// 以下の ifdef ブロックは DLL からのエクスポートを容易にするマクロを作成するための 
// 一般的な方法です。この DLL 内のすべてのファイルは、コマンド ラインで定義された FOVEDLL_EXPORTS
// シンボルを使用してコンパイルされます。このシンボルは、この DLL を使用するプロジェクトでは定義できません。
// ソースファイルがこのファイルを含んでいる他のプロジェクトは、 
// FOVEDLL_API 関数を DLL からインポートされたと見なすのに対し、この DLL は、このマクロで定義された
// シンボルをエクスポートされたと見なします。
#ifdef FOVEDLL_EXPORTS
#define FOVEDLL_API __declspec(dllexport)
#else
#define FOVEDLL_API __declspec(dllimport)
#endif

EXTERN_C 
{
	FOVEDLL_API void Initialize();
	FOVEDLL_API void Finalize();
	FOVEDLL_API void GetEyeVector(double* leftPoint, double* rightPoint);
}
