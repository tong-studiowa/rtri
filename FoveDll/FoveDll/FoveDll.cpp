// FoveDll.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include "FoveDll.h"

#include "fove/IFVRHeadset.h"

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <thread>

// Use std namespace for convenience
using namespace std;

unique_ptr<Fove::IFVRHeadset> gHeadset;
// これは、エクスポートされた関数の例です。
EXTERN_C
{
	void Initialize()
	{
		gHeadset.reset();
		gHeadset = std::move(unique_ptr<Fove::IFVRHeadset>{Fove::GetFVRHeadset()});
		gHeadset->Initialise(Fove::EFVR_ClientCapabilities::Gaze);
	}

	void Finalize()
	{
		gHeadset.release();
	}

	void GetEyeVector(double* leftPoint, double* rightPoint)
	{
		Fove::SFVR_GazeVector leftGaze, rightGaze;
		const Fove::EFVR_ErrorCode error = gHeadset->GetGazeVectors(&leftGaze, &rightGaze);

		if (error == Fove::EFVR_ErrorCode::None)
		{
			leftPoint[0] = leftGaze.vector.x;
			leftPoint[1] = leftGaze.vector.y;
			leftPoint[2] = leftGaze.vector.z;

			rightPoint[0] = rightGaze.vector.x;
			rightPoint[1] = rightGaze.vector.y;
			rightPoint[2] = rightGaze.vector.z;
		}
		else
		{
			leftPoint[0] = 0.0;
			leftPoint[1] = 0.0;
			leftPoint[2] = 1.0;

			rightPoint[0] = 0.0;
			rightPoint[1] = 0.0;
			rightPoint[2] = 1.0;
		}
	}
}
