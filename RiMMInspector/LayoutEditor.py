﻿# LayoutEditor
# Develop       : Python v2.7.2 / Vizard4.06.0138
# LastUpdate    : 2013/02/21
# Programer     : nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizact
import vizmat
import vizinfo
import vizdlg
import vizshape

import ConfigParser
import os
import sys
from os import path
import codecs
from win32api import OutputDebugString

import F3DObjectClass
import FItemClass
import FCameraClass
import ParameterList as ENUM

def GetAbsPath(iPath):
	if iPath==None or iPath=="":
		return ""
	absPath = iPath
	if path.exists(absPath)!=True:		#絶対パスかどうか
		absPath = ENUM.ROOT_PATH+iPath	#絶対パスの生成
		if path.exists(absPath)!=True:
			return ""
	return absPath

class LoadLayoutDataClass():
    def __init__(self,inPath):
        self.mode                = 0         #0=レイアウト編集、1=ブロック編集
        self.isPopMode           = False     #False=通常モード（モデルの回転）、True=ポップモード（ポップアップの追加）
    
        self.layout              = None
        self.layoutModel         = None
        self.boxList             = []
        self.mousePositionX      = 0
        self.mousePositionY      = 0
        self.mouseLeftState      = False
    
        self.modelOriginalList   = dict()    #モデルのコピー元
        self.blockName           = None
        
        OutputDebugString("[StudioWa][INFO][Layout] Layout : __init__")
        self.LoadLayoutIniFile(inPath)
        self.LoadLayoutModel()
        
    def __del__(self):
        OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__")
        
        boxListCount = int(len(self.boxList))
        OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ boxList Count="+str(boxListCount))
        try:
            for data in self.boxList:
                if data!=None:
                    OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ remove from boxlist data="+str(data))
                    del data
        except:
            pass
            
       
        layoutModelCount = int(len(self.layoutModel))
        OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ LayoutModel ListCount="+str(layoutModelCount))
        try:
            for data in self.layoutModel:
                if data!=None:
                    OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ remove from layoutmodel data="+str(data))
                    del data
        except:
            pass
            
        #modelOriginalListCount = int(len(self.modelOriginalList))
        #OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ modelOriginalList Count="+str(modelOriginalListCount))
        #try:
        #    for x in range(len(self.modelOriginalList)):
        #        del x
        #        OutputDebugString("[StudioWa][INFO][Layout] Layout : __del__ remove from modeloriginallist data="+str(x))
        #        #data = self.modelOriginalList.values()[x]
        #        #if data!= None:
        #        #    OutputDebugString("[StudioWa][INFO] Layout : __del__ remove from modeloriginallist data="+str(data))
        #        #    del data
        #except:
        #    pass
        
        for x in range(len(self.modelOriginalList)):
            data = self.modelOriginalList.values()[x]
            if data:
                data.remove()
        self.modelOriginalList.clear()
        
    def GetLayoutModelList(self):
        return self.layoutModel
        
    # - - - - - - - - - - - - - - - - - - - - - - - - -
    #定数の宣言（pythonでは定数を定義できないので、ただのグローバル変数）
    PACK_SIZE  = 100
    BLOCK_SIZE = 500
    
    CAMERA_TARGET_POSITION          = [0,0,0]
    CAMERA_DISTANCE_MODE_0_START    = 2250
    CAMERA_DISTANCE_MODE_1_START    = 600
    CAMERA_DISTANCE_MODE_0          = 2250
    CAMERA_DISTANCE_MODE_1          = 600
    CAMERA_MOVE_SPEED_MODE_0        = 3800
    CAMERA_MOVE_SPEED_MODE_1        = 1000
    CAMERA_POSITION_MODE_0          = [-1500,750,-1500]
    CAMERA_POSITION_MODE_1          = [-400,200,-400]
    
    TURN_SPEED                  = 0.5   #秒
    DOUBLE_CLICK_INTERBAL       = 0.25
    
    #2013-7-26
    SETTING_FOLDER_PATH         = '../Settings/'
    
    # - - - - - - - - - - - - - - - - - - - - - - - - -
    #グローバル変数の宣言
#    mode                = 0         #0=レイアウト編集、1=ブロック編集
#    isPopMode           = False     #False=通常モード（モデルの回転）、True=ポップモード（ポップアップの追加）
    
#    layout              = None
#    layoutModel         = None
#    boxList             = []
#    mousePositionX      = 0
#    mousePositionY      = 0
#    mouseLeftState      = False
    
#    modelOriginalList   = dict()    #モデルのコピー元
#    blockName           = None
    
    
    #ブロック、パックモデルを読み込む
    def LoadModel(self,iFileName):
        fileName = iFileName
        model = None
        
        for x in range(len(self.modelOriginalList)):
            if iFileName == self.modelOriginalList.keys()[x]:    #既にモデルを読み込み済みの場合はコピー
                model = self.modelOriginalList[fileName].clone()
                OutputDebugString("[StudioWa][INFO][Layout] LoadModel : Clone="+str(fileName))
                model.enable(viz.CULLING)   #　！！！追加！！！
                model.visible(True)
                break
        
        if model == None:                                   #モデルが無い場合はファイルを読み込み
            if str(iFileName[1:3]) == ':/':
                filePath = fileName
            elif str(iFileName[1:3]) == ':\\':
                filePath = fileName
            else:
                filePath = viz.res.getPublishedPath(self.SETTING_FOLDER_PATH+fileName)
                #filePath = viz.res.getPublishedPath(GetAbsPath(fileName))
            
            loadedModel = viz.addChild(filePath)
            
            OutputDebugString("[StudioWa][INFO][Layout] LoadModel : Load="+str(filePath))
            
            loadedModel.visible(False)
            loadedModel.appearance(viz.TEXNEAREST)
            self.modelOriginalList[fileName] = loadedModel
            
            model = loadedModel.clone()
            OutputDebugString("[StudioWa][INFO][Layout] LoadModel : Clone="+str(model))
            model.enable(viz.CULLING)   #　！！！追加！！！
            model.visible(True)
            loadedModel = None
        
        instance = FItemClass.FItemClass()          #FItemClassのインスタンスを作成
        instance.SetModel(model)
        
        OutputDebugString("[StudioWa][INFO][Layout] LoadModel : pos="+str(model.getPosition())+",eul="+str(model.getEuler()))
        return instance
    
    #iniファイルを読み込む
    def LoadIni(self,iFileName):
        if str(iFileName[1:3]) == ':/':
            fileName = iFileName
        elif str(iFileName[1:3]) == ':\\':
            fileName = iFileName
        else:
            fileName = viz.res.getPublishedPath(self.SETTING_FOLDER_PATH+iFileName)
            #fileName = viz.res.getPublishedPath(GetAbsPath(iFileName))
        print fileName
        #iniFile = ConfigParser.RawConfigParser()    #RawConfigParserのインスタンスを作成
        #iniFile.read(fileName)
        
        iniFile = ConfigParser.SafeConfigParser()
        f = codecs.open(fileName, 'r', 'shift-jis')
        
        OutputDebugString("[StudioWa][INFO][Layout] LoadIni : load ini="+str(fileName))
        
        iniFile.readfp(f)
        f.close()
        
        return iniFile
    
    #ダミーモデルを作成する
    def CreateDummyModel(self):
        modelSize = None
    
        #サイズ設定
        if self.mode == 0:
            modelSize = [self.BLOCK_SIZE,1,self.BLOCK_SIZE]
        elif self.mode == 1:
            modelSize = [self.PACK_SIZE,1,self.PACK_SIZE]
        
        model = FItemClass.FItemClass()                 #FItemClassのインスタンスを作成
        model.SetModel(vizshape.addBox(size=modelSize)) #ボックスモデルを作成
        model.GetModel().alpha(0)                       #ボックスモデルを非表示に設定
        OutputDebugString("[StudioWa][INFO][Layout] CreateDummyModel : create dummy")
        
        return model
    
    #レイアウト定義ファイルを基にブロック定義ファイル、ブロックモデルを読み込み、配置
    def SetLayout(self,iLayout):
        list = iLayout                              #レイアウト定義ファイル
        modelList = []                              #読み込んだモデルのリスト 
        numOfBlock = int(list.get('Define','NumOfBlock'))       #ブロックの数(香川大では4x4)
        numOfPack = int(list.get('Define','NumOfPack'))         #パックの数(香川大では5x5)
    
        for z in range(numOfBlock):
            for x in range(numOfBlock):
                option = str(z) + '_' + str(x)
                if list.has_option('Whole',option) == True:     #値がある場合読み込みを行う
                    
                    #OutputDebugString("[StudioWa][INFO][Layout] SetLayout : has option ="+str(option))
                    
                    fileName = list.get('Whole',option)
                    fileName = fileName.split(',')
                    
                    dummy = self.CreateDummyModel()
    
                    if list.has_section(fileName[0]) == True:
                        self.SetBlockFromLayout(list,fileName[0],dummy)
                        
                        #OutputDebugString("[StudioWa][INFO] SetLayout : pack list ="+str(fileName[0]))
                    else:
                        model = self.LoadModel(fileName[0])
                        
                        model.GetModel().setPosition([0,0,0])
                        model.SetPosition([0,0,0])
                        model.GetModel().setQuat([0,0,0,1])
                        model.SetQuaternion([0,0,0,1])
                        
                        model.SetParent(dummy.GetModel())
                        #OutputDebugString("[StudioWa][INFO] SetLayout : setparent : model="+str(model)+" parent="+str(dummy.GetModel()))
                        model.GetModel().disable(viz.PICKING)
                        self.boxList.append(model)
                        bCount=int(len(self.boxList))
                        #OutputDebugString("[StudioWa][INFO] SetLayout : boxListCount="+str(bCount))
                        
                    modelQuaternion = self.CheckAngle(int(fileName[1]))
                    dummy.SetQuaternion(modelQuaternion)
    
                    posX = (x * self.BLOCK_SIZE) - (self.BLOCK_SIZE * numOfBlock / 2) + (self.BLOCK_SIZE / 2)
                    posZ = (z * self.BLOCK_SIZE) - (self.BLOCK_SIZE * numOfBlock / 2) + (self.BLOCK_SIZE / 2)
                    dummy.SetPosition([posX,-0.01,posZ])
                    pos = [posX,-0.01,posZ]
                    OutputDebugString("[StudioWa][INFO][Layout] SetLayout : position=X:"+str(posX)+",Z:"+str(posZ))
                    
                    modelList.append(dummy)
                    mCount=int(len(modelList))
                    #OutputDebugString("[StudioWa][INFO][Layout] SetLayout : modelListCount="+str(mCount))

        return modelList
        
    #レイアウト定義ファイル内のブロック定義を基にパックモデルを読み込み、配置（レイアウト編集用）
    def SetBlockFromLayout(self,iBlock,iFileName,iDummy):
        list = iBlock                               #ブロック定義ファイル
        modelList = []                              #読み込んだモデルのリスト    
        numOfBlock = int(list.get('Define','NumOfBlock'))       #ブロックの数(香川大では4x4)
        numOfPack = int(list.get('Define','NumOfPack'))         #パックの数(香川大では5x5)
        dummy = iDummy
    
        for z in range(numOfPack):
            for x in range(numOfPack):
                option = str(z) + '_' + str(x)
                if list.has_option(iFileName,option) == True:   #値がある場合読み込みを行う
                    fileName = list.get(iFileName,option)
                    fileName = fileName.split(',')
                    
                    model = self.LoadModel(fileName[0])
                    
                    model.SetParent(dummy.GetModel())
                    #model.GetModel().disable(viz.PICKING)
                    
                    modelQuaternion = self.CheckAngle(int(fileName[1]))
                    model.SetQuaternion(modelQuaternion)
    
                    posX = (x * self.PACK_SIZE) - (self.PACK_SIZE * numOfPack / 2) + (self.PACK_SIZE / 2)
                    posZ = (z * self.PACK_SIZE) - (self.PACK_SIZE * numOfPack / 2) + (self.PACK_SIZE / 2)
                    model.SetPosition([posX,0,posZ])
                    self.boxList.append(model)
                    bCount=int(len(self.boxList))
                    OutputDebugString("[StudioWa][INFO] SetLayout : boxListCount="+str(bCount))
                    
    #回転の値を確認
    def CheckAngle(self,iAngle):
        modelQuaternion = None
        if iAngle == 0:
            modelQuaternion = [0,0,0,1]
        elif iAngle == 1:
            modelQuaternion = [0,0.707,0,0.707]
        elif iAngle == 2:
            modelQuaternion = [0,1,0,0]
        elif iAngle == 3:
            modelQuaternion = [0,-0.707,0,0.707]
        return modelQuaternion

         
    #レイアウト設定ファイル読み込み
    def LoadLayoutIniFile(self,inPath):
        self.layout = self.LoadIni(inPath)          #レイアウト定義のiniファイルを読み込む
        
    #レイアウト設定ファイルを基にレイアウト用モデルを読み込み、配置
    def LoadLayoutModel(self):
        self.layoutModel = self.SetLayout(self.layout)
        


    # - - - - - - - - - - - - - - - - - - - - - - - - -
    #初期化処理（起動時のみ）
    def Initialize(self):
        #iniファイルの読み込み
        self.LoadLayoutIniFile()                 #レイアウト定義用iniファイルの読み込み
        #レイアウト編集を開始
        self.LoadLayoutModel()                   #レイアウト用モデルを読み込み、配置
        try:
            for data in self.modelOriginalList:
                if data!=None:
                    data.remove()
        except:
            pass
        self.layout = None
        
#QUEST_PATH		= viz.res.getPublishedPath("..\..\Settings\Layout.ini")
#
#edi = LoadLayoutDataClass(QUEST_PATH)
#mainView = viz.MainView
#mainView.eyeheight(1.6)
#
#viz.MainWindow.clip(0.1, 1000)
#mylight = viz.addLight()		#太陽光ダミー
#mylight.enable()				#ライトのアクティブ化
#mylight.position(0, 1000, 0)
#mylight.spread(180)
#mylight.intensity(2)
#
#viz.go()