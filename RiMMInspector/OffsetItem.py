﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
"""
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

offsetTemp = iniFile.GetItemData('Setting','Offset')
offset = float(offsetTemp)
"""
offset = 0.0

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム移動制御のクラス定義
class OffsetItem(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][OffsetItem] : Init OffsetItem')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._SelectModelClass = None
		
		self._Offset = offset
		
		self._KeyState = [False,False,False,False]
		self._PreKeyState = [False,False,False,False]
	
		self._SelectedItemList = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,selectModelClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._SelectModelClass = selectModelClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][OffsetItem] : Start OffsetItem Update')
		
		self._KeyState = self._InputClass.GetOffsetKeyState()
		
		for x in range(4):
			if self._KeyState[x] == False and self._PreKeyState[x] == True:
				list = self._SelectModelClass.GetSelectedModelList()
				if list != []:
					self._SelectedItemList = self._SelectModelClass.GetSelectedModelList()
					
				self.Offset(x)
				self._SelectModelClass.UnSelectItem()
		
		self._PreKeyState = self._KeyState
		
	#オフセット
	def Offset(self,dir):
		offsetVal = [0,0]
		if dir == 0:	#上
			offsetVal[1] = self._Offset
		elif dir == 1:	#下
			offsetVal[1] = -self._Offset
		elif dir == 2:	#左
			offsetVal[0] = -self._Offset
		elif dir == 3:	#右
			offsetVal[0] = self._Offset
		
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		
		for itemNum in self._SelectedItemList:
			itemPosition = self._ItemControlClass.GetPosition(itemNum)

			newItemPositin = [0,0,0]
			newItemPositin[0] = itemPosition[0] + offsetVal[0]
			newItemPositin[1] = itemPosition[1]
			newItemPositin[2] = itemPosition[2] + offsetVal[1]
			
			self._ItemControlClass.SetPosition(itemNum,newItemPositin)
		