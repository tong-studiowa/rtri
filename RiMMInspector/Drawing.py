﻿# coding: utf-8

import viz
import vizshape
import Interface
import LoadIniFile
from os import path
from win32api import OutputDebugString

#ファイルへのパス
RESOURCE_FOLDER_PATH = '..\\Resource\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面制御のクラス定義
class Drawing(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Init Drawing')
		
		self._Drawing = None
		self._DrawingName = ""
		
		self._TextureSize = [1.0,1.0]
		self._Scale = 50.0
		self._DrawingScale = [1.0,1.0,1.0]
		
		self._IsVisible = True
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Start Drawing Update')
		pass
		
	#追加
	def Add(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Add')
		
		loadState = False
		
		if fileName != '':
			#図面の読み込み
			tex = None
			
			if fileName[:2] == '..':
				fileName = viz.res.getPublishedPath(fileName)
				
			#ローカルフォルダを確認
			localFileName = fileName.split('/')
			if len(localFileName) == 1:
				localFileName = fileName.split('\\')
			localFileName = localFileName[-1]
			#print 'localFileName=',localFileName
			localFilePath = viz.res.getPublishedPath(RESOURCE_FOLDER_PATH + 'Drawing\\' + localFileName)
			
			#図面の読み込み
			if path.exists(localFilePath) == True:	#ローカルフォルダにファイルがあるか
				try:
					tex = viz.addTexture(localFilePath)
				except:
					tex = None
			else:
				try:
					tex = viz.addTexture(fileName)
				except:
					tex = None
					
			if tex:
				#既にある図面を削除
				if self._DrawingName != "":
					self.Delete()
					
				#モデル作成
				texSize = tex.getSize()
				
				plane = vizshape.addPlane([1.0,1.0])
				plane.texture(tex)
				
				plane.disable(viz.PICKING)
				plane.disable(viz.LIGHTING)
				plane.alpha(0.5)
				
				plane.disable(viz.DEPTH_WRITE)
				plane.drawOrder(-10)
				plane.setPosition(0,0.1,0)
				
				self._Drawing = plane
				self._DrawingName = fileName
				self._TextureSize = [texSize[0],texSize[1]]
				
				self.SetScale(self._Scale)
				self.SetVisible(self._IsVisible)
				
				loadState = True
		
		return loadState
		
	#削除
	def Delete(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Delete')
		
		if self._DrawingName != "":
			self._Drawing.remove()
			del self._Drawing
			
			self._Drawing = None
			self._DrawingName = ""
			self._Scale = 50.0
	
	#サイズ設定
	def SetScale(self,scale):
		self._Scale = scale
		
		aspectratio = 1.0
		if self._TextureSize[0] != 0:
			aspectratio = float(self._TextureSize[1]) / float(self._TextureSize[0])
		
		if self._Drawing:
			self._DrawingScale = [self._Scale,1.0,self._Scale*aspectratio]
			self._Drawing.setScale(self._DrawingScale)
			
	#サイズ取得
	def GetScale(self):
		scale = self._Scale
		return scale
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.ON
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		if self._Drawing != None:
			self._Drawing.visible(val)
			
	#表示状態取得
	def GetVisible(self):
		return self._IsVisible
		
	#ファイル名取得
	def GetFileName(self):
		fileName = self._DrawingName
		return fileName
		
	#図面のモデルを取得
	def GetDrawingModel(self):
		return self._Drawing
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		return 	self._DrawingScale
		