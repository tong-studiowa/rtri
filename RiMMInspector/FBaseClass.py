﻿# FBaseClass
# Develop       : Python v2.7.2 / Vizard4.05.0095
# LastUpdate    : 2012/11/22
# Programer     : nishida

import viz

class FBaseClass:

# member

# method
	# called method at first
	def __init__(self):
		pass

	# called method at delete
	def __del__(self):
		pass
