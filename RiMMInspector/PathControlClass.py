﻿# coding: utf-8
#PathSimulator

import viz
import vizinput
import vizmat
import vizact
import vizshape

import LoadIniFile

import os
import sys
from os import path
import codecs
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

def GetAbsPath(iPath):
	if iPath==None or iPath=="":
		return ""
	absPath = iPath
	if path.exists(absPath)!=True:	#絶対パスかどうか
		absPath = SETTING_FOLDER_PATH+iPath	#絶対パスの生成
		if path.exists(absPath)!=True:
			return ""
	return absPath

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス設定用クラス定義
class PathControlClass():
	#コンストラクタ
	def __init__(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Init PathControlClass')
		self.pathControl = PathControlClass2(fileName)
	
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Del PathControlClass')
		self.pathControl.Delete()
		del self.pathControl
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Start PathControlClass Update')
		pass
		
	#設定ファイルを読み込み、キャラクターとパスを配置
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Load')
		self.pathControl.Load(fileName)
		
	#キャラクターとパスを削除
	def Delete(self):
		self.pathControl.Delete()
		
	#読込みに成功したかを判定する
	def IsSuccessLoading(self):
		flag = self.pathControl.IsSuccessLoading()
		return flag
	
	#キャラクターを設定
	def SetAvatar(self,id,fileName):
		self.pathControl.SetAvatar(id,fileName)
		
	#パスを設定
	def SetPath(self,id,fileName):
		self.pathControl.SetPath(id,fileName)
		
	#パスを作成
	def CreatePath(self,id):
		#パスの設定ファイルを読み込み
		path,line = self.pathControl.CreatePath(id)
		return path,line
		
	#立ちモーションを設定
	def SetIdleAnimation(self,id):
		self.pathControl.SetIdleAnimation(id)
		
	#歩きモーションを設定
	def SetWalkAnimation(self,id):
		self.pathControl.SetWalkAnimation(id)
		
	#移動の開始
	def Play(self,id):
		self.pathControl.Play(id)
		
	#移動の一時停止
	def Pause(self,id):
		self.pathControl.Pause(id)
		
	#移動の停止、リセット
	def Reset(self,id):
		self.pathControl.Reset(id)
		
	#移動の停止、最終地点にジャンプ
	def End(self,id):
		self.pathControl.End(id)
		
	#キャラクターの総数を取得
	def GetAvatarCount(self):
		count = self.pathControl.GetAvatarCount()
		return count
		
	#キャラクターのリストを取得
	def GetAvatarList(self):
		list = self.pathControl.GetAvatarList()
		return list
		
	#立ちキャラクターのリストを取得
	def GetStandAvatarList(self):
		list = self.pathControl.GetStandAvatarList()
		return list
		
	#キャラクターファイル名のリストを取得
	def GetAvatarFileList(self):
		list = self.pathControl.GetAvatarFileList()
		return list
		
	#パスのリストを取得
	def GetPathList(self):
		list = self.pathControl.GetPathList()
		return list
		
	#パスのファイル名のリストを取得
	def GetPathFileList(self):
		list = self.pathControl.GetPathFileList()
		return list
		
	#再生状態の取得（0：停止、1：再生）
	def GetState(self,id):
		state = self.pathControl.GetState(id)
		return state
		
	#キャラクターの表示を設定
	def SetAvatarVisible(self,id,state):
		self.pathControl.SetAvatarVisible(id,state)
			
	#キャラクターの表示を取得
	def GetAvatarVisible(self,id):
		state = self.pathControl.GetAvatarVisible(id)
		return state
		
	#パスの表示を設定
	def SetPathVisible(self,id,state):
		self.pathControl.SetPathVisible(id,state)
			
	#パスの表示を取得
	def GetPathVisible(self,id):
		state = self.pathControl.GetPathVisible(id)
		return state
		
	#キャラクターの色設定
	def SetAvatarColor(self,id,color):
		self.pathControl.SetAvatarColor(id,color)
		
	#パスの色設定
	def SetPathColor(self,id,color):
		self.pathControl.SetPathColor(id,color)
		
	#視点の高さを設定
	def SetEyeHeight(self,height):
		self.pathControl.SetEyeHeight(height)
		
	#視点の高さを取得
	def GetEyeHeight(self):
		height = self.pathControl.GetEyeHeight()
		return height
		
	#視点用カメラを取得
	def GetEyeViewCamera(self,id):
		viewCamera = self.pathControl.GetEyeViewCamera(id)
		return viewCamera
		
	#視点用カメラを更新
	def CameraUpdate(self):
		self.pathControl.CameraUpdate()
		
	#表示用ラインのリストを取得
	def GetLineList(self):
		list = self.pathControl.GetLineList()
		return list
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス設定用クラス定義2
class PathControlClass2():
	#コンストラクタ
	def __init__(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Init PathControlClass')
		
		self._LoadErrorFlag	= False
		
		self._PathDataFile		= None
		self._PathDataFileName	= ''
		
		self._WalkSpeed		= 7
		self._IdolAnimation	= 1
		self._WalkAnimation	= 2
		self._LineColor		= viz.RED
		self._LineWidth		= 3
		self._EyeHeight		= 1.6
		
		self._AvatarCount = 0
		
		self._AvatarNameList	= []
		self._AvatarList		= []
		self._StandAvatarList	= []
		
		self._ViewCameraList	= []
		self._AvatarStateList	= []
		self._AvatarVisibleList	= []
		
		self._PathNameList	= []
		self._PathList		= []
		self._LineList		= []
		
		self._EndStateList	= []
		
		self.Load(fileName)
		
		for x in range(self._AvatarCount):
			self.Reset(x)
			
		#vizact.ontimer(0,self.CameraUpdate)
		
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Del PathControlClass')
		
		self.Delete()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Start PathControlClass Update')
		pass
		
	#設定ファイルを読み込み、キャラクターとパスを配置
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Load')
		
		filePath = GetAbsPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		total = iniFile.GetItemData('Log','Total')
		
		if total == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはパス定義ファイルではありません。', title='RiMMInspector')
			self._LoadErrorFlag	= True
			del iniFile
		
		else:
			self._PathDataFile		= iniFile
			self._PathDataFileName	= fileName
			print 'PathDataFileName=',fileName
			
			self._AvatarCount = int(total)
			print 'AvatarCount=',self._AvatarCount
			
			#logファイルへのパスを取得
			fileNameData = fileName.split('\\')
			fName = ''
			fState = False
			for x in range(len(fileNameData)):
				data = fileNameData[x]
				if x == len(fileNameData)-1:
					pass
				elif fState:
					fName = fName + data + '\\' 
				if data == 'Settings':
					fState = True
					
			self._AvatarNameList	= []
			self._AvatarList		= []
			self._StandAvatarList	= []
			
			self._ViewCameraList	= []
			self._AvatarStateList	= []
			self._AvatarVisibleList	= []
		
			self._PathNameList	= []
			self._PathList		= []
			self._LineList		= []
				
			self._EndStateList	= []
		
			for x in range(self._AvatarCount):
				self._AvatarNameList.append('')
				self._AvatarList.append(None)
				self._StandAvatarList.append(None)
				
				self._ViewCameraList.append(None)
				self._AvatarStateList.append(False)
				self._AvatarVisibleList.append(True)
			
				self._PathNameList.append('')
				self._PathList.append(None)
				self._LineList.append(None)
				
				self._EndStateList.append(False)
		
			for x in range(self._AvatarCount):
				section = iniFile.GetItemData('Log',str(x))
				
				#キャラクターの作成
				avatarName = iniFile.GetItemData(section,'avatar')
				self.SetAvatar(x,avatarName)
				
				#ログの作成
				pathName = iniFile.GetItemData(section,'log')
				print 'pathName',pathName
				
				logPath = pathName
				logData = pathName.split('\\')
				if len(logData) == 1:
					logPath = fName + pathName
					
				self.SetPath(x,logPath)
				
			print 'AvatarNameList=',self._AvatarNameList
			print 'PathNameList=',self._PathNameList
			
	#キャラクターとパスを削除
	def Delete(self):
		for x in range(self._AvatarCount):
			avatar = self._AvatarList[x]
			if avatar:
				avatar.remove()
				del avatar
			
			standAvatar = self._StandAvatarList[x]
			if standAvatar:
				standAvatar.remove()
				del standAvatar
				
			viewCamera = self._ViewCameraList[x]
			if viewCamera:
				viewCamera.remove()
				del viewCamera
				
		for x in range(self._AvatarCount):
			path = self._PathList[x]
			if path:
				path.remove()
				del path
				
			line = self._LineList[x]
			if line:
				line.remove()
				del line
			
	#読込みに成功したかを判定する
	def IsSuccessLoading(self):
		flag = self._LoadErrorFlag
		return flag
	
	#キャラクターを設定
	def SetAvatar(self,id,fileName):
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		avatar = viz.addAvatar(filePath)
		avatar.disable(viz.LIGHTING)
		avatar.visible(viz.OFF)
		self._AvatarNameList[id] = fileName
		self._AvatarList[id] = avatar
		
		standAvatar = viz.addAvatar(filePath)
		standAvatar.disable(viz.LIGHTING)
		standAvatar.visible(viz.ON)
		self._StandAvatarList[id] = standAvatar
		
		#カメラ追加
		viewCamera = viz.addView()
		viewCamera.getHeadLight().disable()
		self._ViewCameraList[id] = viewCamera
		
	#パスを設定
	def SetPath(self,id,fileName):
		self._PathNameList[id] = fileName
		
		path,line = self.CreatePath(id)
		self._PathList[id] = path
		self._LineList[id] = line
		
	#パスを作成
	def CreatePath(self,id):
		#パスの設定ファイルを読み込み
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + self._PathNameList[id])
		logFile = codecs.open(filePath, 'r', 'shift-jis')
		
		positions = []
		angle = 180
		setAngle = False
		for logData in logFile:
			pos = [0.0,0.0,0.0]
			pos[0] = float(logData.split(',')[1])
			pos[2] = float(logData.split(',')[3])
			positions.append(pos)
			
			if setAngle == False:
				angle = float(logData.split(',')[4])
				setAngle = True
				
		logFile.close()
		print 'positions=',positions
		
		standAvatar = self._StandAvatarList[id]
		standAvatar.setPosition(positions[0])
		standAvatar.setEuler([angle,0,0])
		standAvatar.state(self._IdolAnimation)
		
		self._AvatarStateList[id] = False
		
		path = None
		line = None
		
		if len(positions) != 1:		#パスがある時
			#表示用ラインの作成
			viz.startLayer(viz.LINE_STRIP) 
			for x,pos in enumerate(positions):
				pos[1] = 0.01	#ラインは少し上にずらす
				viz.vertex(pos)
				pos[1] = 0.0

			line = viz.endLayer()
			line.disable(viz.DEPTH_WRITE)
			line.drawOrder(-5)
			#line.setTransparentDrawOrder()
			line.disable(viz.LIGHTING)
			line.color(self._LineColor)
			line.lineWidth(self._LineWidth)

			#アニメーションパスを作成
			positionList = []
			distanceList = []
			allDistance = 0.0
			allDistanceList = []
			
			#長さを確認
			distanceList.append(0.0)
			allDistanceList.append(0.0)
			for x in range(len(positions)-1):
				Pos0 = positions[x]
				Pos1 = positions[x+1]
				
				distance = vizmat.Distance(Pos0,Pos1)
				
				distanceList.append(distance)
				allDistance = allDistance + distance
				allDistanceList.append(allDistance)
				
			#分割数は10cm
			deltaDistance = 0.1
			count = int(allDistance * 10)
			
			dist = 0.0
			for x in range(count):
				dist = dist + deltaDistance
						
				#最初の点を設定
				if x == 0:
					pos = positions[x]
					positionList.append(pos)
			
				#最後の点を設定
				elif x == count-1:
					lastNum = len(positions)-1
					pos = positions[lastNum]
					positionList.append(pos)
					
				elif x < count-1:
					for y in range(len(allDistanceList)):
						p = allDistanceList[y]
						if dist < p or dist == p:
							
							#割合を確認
							distMin = allDistanceList[y-1]
							distMax = allDistanceList[y]
							
							perc = (dist - distMin) / (distMax - distMin)
							
							#位置設定
							pathPosMin = positions[y-1]
							pathPosMax = positions[y]
							
							pos = [0,0,0]
							pos[0] = pathPosMin[0]+(pathPosMax[0]-pathPosMin[0])*perc
							pos[1] = pathPosMax[1]
							pos[2] = pathPosMin[2]+(pathPosMax[2]-pathPosMin[2])*perc
							
							positionList.append(pos)
							
							break
			
			path = viz.addAnimationPath()
			for x,pos in enumerate(positionList):
				path.addControlPoint(x+1,pos=pos)
			
			path.setLoopMode(viz.OFF)
			if len(positionList) > 2:
				path.computeTangents()
				#path.setTranslateMode(viz.CUBIC_BEZIER)
			path.setAutoRotate(viz.ON)
			path.setSpeed(self._WalkSpeed)
			
			path.addEventAtEnd('end')
			
			avatar = self._AvatarList[id]
			viz.link(path, avatar)
			
		return path,line
		
	#立ちモーションを設定
	def SetIdleAnimation(self,id):
		self._IdolAnimation	= id
		
	#歩きモーションを設定
	def SetWalkAnimation(self,id):
		self._WalkAnimation	= id
		
	#移動の開始
	def Play(self,id):
		path = self._PathList[id]
		avatar = self._AvatarList[id]
		standAvatar = self._StandAvatarList[id]
		
		if path != None and self._EndStateList[id] == False:
			path.play()
			avatar.state(self._WalkAnimation)
			
			self._AvatarStateList[id] = True
			if self._AvatarVisibleList[id]:
				avatar.visible(viz.ON)
				standAvatar.visible(viz.OFF)
		
			vizact.onPathEvent(path, 'end', self.End2,id)

	#移動の一時停止
	def Pause(self,id):
		path = self._PathList[id]
		avatar = self._AvatarList[id]
		
		if path != None:
			path.pause()
			avatar.state(self._IdolAnimation)
		
	#移動の停止、リセット
	def Reset(self,id):
		path = self._PathList[id]
		avatar = self._AvatarList[id]
		standAvatar = self._StandAvatarList[id]
		
		if path != None:
			path.pause()
			path.reset()
			avatar.state(self._IdolAnimation)
			
			self._AvatarStateList[id] = False
			if self._AvatarVisibleList[id]:
				avatar.visible(viz.OFF)
				standAvatar.visible(viz.ON)
		
			self._EndStateList[id] = False
			
	#移動の停止、最終地点にジャンプ
	def End(self,id):
		path = self._PathList[id]
		avatar = self._AvatarList[id]
		standAvatar = self._StandAvatarList[id]
		
		if path != None and self._EndStateList[id] == False:
			path.play()
			path.setSpeed(1000000)
			avatar.state(self._IdolAnimation)
			
			self._AvatarStateList[id] = True
			if self._AvatarVisibleList[id]:
				avatar.visible(viz.ON)
				standAvatar.visible(viz.OFF)
				
			vizact.onPathEvent(path, 'end', self.End2,id)

	#移動の停止、最終地点にジャンプ2
	def End2(self,id):
		path = self._PathList[id]
		avatar = self._AvatarList[id]
		
		if path != None:
			path.pause()
			path.setSpeed(self._WalkSpeed)
			avatar.state(self._IdolAnimation)
			
			self._EndStateList[id] = True
		
	#キャラクターの総数を取得
	def GetAvatarCount(self):
		count = self._AvatarCount
		return count
		
	#キャラクターのリストを取得
	def GetAvatarList(self):
		list = self._AvatarList
		return list
		
	#立ちキャラクターのリストを取得
	def GetStandAvatarList(self):
		list = self._StandAvatarList
		return list
		
	#キャラクターファイル名のリストを取得
	def GetAvatarFileList(self):
		list = self._AvatarNameList
		return list
		
	#パスのリストを取得
	def GetPathList(self):
		list = self._PathList
		return list
		
	#パスのファイル名のリストを取得
	def GetPathFileList(self):
		list = self._PathNameList
		return list
		
	#再生状態の取得（0：停止、1：再生）
	def GetState(self,id):
		state = 0
		path = self._PathList[id]
		if path.getPaused() == 0:
			state = 1
		
		return state
		
	#キャラクターの表示を設定
	def SetAvatarVisible(self,id,state):
		self._AvatarVisibleList[id] = state
		avatar = self._AvatarList[id]
		standAvatar = self._StandAvatarList[id]
		
		if state:
			if self._AvatarStateList[id]:
				avatar.visible(viz.ON)
				standAvatar.visible(viz.OFF)
			else:
				avatar.visible(viz.OFF)
				standAvatar.visible(viz.ON)
				
		else:
			avatar.visible(viz.OFF)
			standAvatar.visible(viz.OFF)
			
	#キャラクターの表示を取得
	def GetAvatarVisible(self,id):
		state = self._AvatarVisibleList[id]
		return state
		
	#パスの表示を設定
	def SetPathVisible(self,id,state):
		line = self._LineList[id]
		
		if line != None:
			if state:
				line.visible(viz.ON)
			else:
				line.visible(viz.OFF)
			
	#パスの表示を取得
	def GetPathVisible(self,id):
		line = self._LineList[id]
		state = False
		
		if line != None:
			state = line.getVisible()
		
		return state
		
	#キャラクターの色設定
	def SetAvatarColor(self,id,color):
		avatar = self._AvatarList[id]
		avatar.color(color)
		
	#パスの色設定
	def SetPathColor(self,id,color):
		line = self._LineList[id]
		
		if line != None:
			line.color(color)
		
	#視点の高さを設定
	def SetEyeHeight(self,height):
		self._EyeHeight = height
		
	#視点の高さを取得
	def GetEyeHeight(self):
		height = self._EyeHeight
		return height
		
	#視点用カメラを取得
	def GetEyeViewCamera(self,id):
		viewCamera = self._ViewCameraList[id]
		return viewCamera
		
	#視点用カメラを取得
	def CameraUpdate(self):
		for x in range(self._AvatarCount):
			avatar = self._AvatarList[x]
			if self._AvatarStateList[x] == False:
				avatar = self._StandAvatarList[x]
			viewCamera = self._ViewCameraList[x]
			
			#位置設定
			pos = avatar.getPosition(viz.ABS_GLOBAL)
			pos[1] = pos[1] + self._EyeHeight
			viewCamera.setPosition(pos,mode=viz.ABS_GLOBAL)
			
			#角度設定
			ori = avatar.getEuler()
			viewCamera.setEuler(ori)
			
	#表示用ラインのリストを取得
	def GetLineList(self):
		list = self._LineList
		return list
		