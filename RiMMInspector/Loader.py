﻿
import viz

viz.res.addPublishFileLoader('osg')
viz.res.addPublishFileLoader('osgb')
viz.res.addPublishFileLoader('fbx')
viz.res.addPublishFileLoader('dae')

viz.res.addPublishFileLoader('wav')
viz.res.addPublishFileLoader('wmv')

viz.res.addPublishFileLoader('tga')
viz.res.addPublishFileLoader('ttc')
viz.res.addPublishFileLoader('jpg')
viz.res.addPublishFileLoader('png')
viz.res.addPublishFileLoader('bmp')
viz.res.addPublishFileLoader('tif')
