﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

#areaSelectHeightTemp = iniFile.GetItemData('Setting','AreaSelectHeight')
#areaSelectHeight = float(areaSelectHeightTemp)
areaSelectHeight = 10000.0

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#スタート位置選択制御のクラス定義
class SelectStartPosition(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectStartPosition] : Init SelectStartPosition')
		
		self._InputClass = None
		self._StartPositionClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._SelectWallClass = None
		self._SelectModelClass = None
		self._CheckStateClass = None
		
		self._SelectedState = False
		self._ClickState = False
		
		self._RotateSnap = rotateSnap
		
	#使用するクラスの追加
	def AddClass(self,inputClass,startPositionClass,stencilClass,manipulatorClass,selectWallClass
				,selectModelClass,checkStateClass):
		self._InputClass = inputClass
		self._StartPositionClass = startPositionClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._SelectWallClass = selectWallClass
		self._SelectModelClass = selectModelClass
		self._CheckStateClass = checkStateClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectStartPosition] : Start SelectStartPosition Update')
		
		self._ClickState = False
		state = self._SelectedState
		
		#シングルクリックによる選択
		if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True:
			state = self.Select()
			
			#マニピュレータを操作中
			if self._ManipulatorClass.GetState():
				pass
				
			#モデルをクリックし、選択
			elif state:
				self._ClickState = True
				
				#選択し、モデルをハイライト表示
				self._SelectedState = True
				
				self._StartPositionClass.SetHighlight(True)
				
				self._ManipulatorClass.SetArrowYVisible(1)
				self._ManipulatorClass.SetCircle2Visible(1)
				
				model = self._StartPositionClass.GetModel()
				self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
				
			#選択中にモデル以外をクリック
			else:
				if self._SelectedState == True:
					self._ClickState = True
					
					self._SelectedState = False
					
					#選択を解除し、モデルのハイライト表示をOFF
					self._StartPositionClass.SetHighlight(False)
					
					if self._StartPositionClass.GetModel() == self._ManipulatorClass.GetModel():
						self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
					
		if self._StencilClass.GetClickState():
			if self._SelectedState == True:
				
				#選択を解除し、モデルのハイライト表示をOFF
				self._StartPositionClass.SetHighlight(False)
				self._SelectedState = False
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
		if self._SelectModelClass.GetDragState():
			if self._SelectedState == True:
				
				#選択を解除し、モデルのハイライト表示をOFF
				self._StartPositionClass.SetHighlight(False)
				self._SelectedState = False
				
	#選択
	def Select(self):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		pickState = False
		
		selectedModel = None
		selectedRoom,selectedModel,selectedModelState = self._SelectWallClass.GetSelectedModel()
				
		if selectedModel == None and self._SelectModelClass.GetSelectedState() == False:
			self._StartPositionClass.SetPickable(True)
			state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
			self._StartPositionClass.SetPickable(False)
			
			if pickedModel == self._StartPositionClass.GetModel():		
				pickState = True
			
		return pickState
	
	#選択解除
	def UnSelect(self):
		if self._SelectedState == True:
			self._StartPositionClass.SetHighlight(False)
			self._SelectedState = False
			self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
		
	#選択状態の取得
	def GetSelectedState(self):
		selectedState = self._SelectedState
		
		return selectedState
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
	