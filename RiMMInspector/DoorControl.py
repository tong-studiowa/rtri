﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Interface
import Door
import DoorLine

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドア制御のクラス定義
class DoorControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,doorList,wallHeight):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorControl] : Init DoorControl')
		
		self._WallClass = None
		self._ModelControlClass = None
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._DoorList = doorList
		self._WallHeight = wallHeight
		
		self._ModelList = []
		self._LineModelList = []
		
		self._DoorRef = vizshape.addBox([1,1,1])
		self._DoorRef.visible(viz.OFF)
		self._DoorRef.disable(viz.PICKING)
		
		self._LoadErrorState = False
		
	#使用するクラスの追加
	def AddClass(self,wallClass,modelControlClass):
		self._WallClass = wallClass
		self._ModelControlClass = modelControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorControl] : Start DoorControl Update')
		pass
		
	#iniファイルを基にドアを配置
	def SetDoor(self):
		for data in self._DoorList:
			num = self.Create(data[0],data[1],data[2])
			
		for door in self._ModelList:
			if door.GetLoadErrorState():
				self._LoadErrorState = True
				
	#作成
	def Create(self,fileName,wallNum,percentage):
		#モデル
		door = Door.Door(self._ModelControlClass)
		door.Add(fileName)	#モデルを追加
		
		self._ModelList.append(door)
		
		#ラインモデル
		doorLine = DoorLine.DoorLine()
		doorLine.SetWallHeight(self._WallHeight)
		doorLine.Add(door)	#モデルを追加
		
		self._LineModelList.append(doorLine)
		
		#ID設定
		num = len(self._ModelList) - 1
		
		door.SetId(num)
		doorLine.SetId(num)
		
		#配置
		self.SetPosition(num,wallNum,percentage)
		
		return num
		
	#追加
	def Add(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,wallNum,percentage]
		self._DoorList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,wallNum,percentage)
		
		return num,self._DoorList
		
	#ダミーを追加
	def AddDummy(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,wallNum,percentage]
		self._DoorList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,wallNum,percentage)
		
		door = self._ModelList[num]
		door.SetDummyFlag(True)
		doorLine = self._LineModelList[num]
		doorLine.SetDummyFlag(True)
		
		return num,self._DoorList
		
	#削除
	def Delete(self,num):
		#モデル
		model = self._ModelList[num]
		
		self._ModelList.remove(model)
		
		if model != None:
			model.Delete()	#モデルを削除
			del model
		
		#ラインモデル
		lineModel = self._LineModelList[num]
		
		self._LineModelList.remove(lineModel)
		
		if lineModel != None:
			lineModel.Delete()	#モデルを削除
			del lineModel
			
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
			lineModel = self._LineModelList[x]
			lineModel.SetId(num)
			
		#ドアのリストを更新
		deletedDoor = self._DoorList[num]
		self._DoorList.remove(deletedDoor)
		
		return self._DoorList
			
	#全モデル削除
	def DeleteAll(self):
		#モデル
		for model in self._ModelList:
			model.Delete()	#モデルを削除
			del model
		
		self._ModelList = []
		
		#ラインモデル
		for lineModel in self._LineModelList:
			lineModel.Delete()	#モデルを削除
			del lineModel
		
		self._LineModelList = []
		
	#コピー
	def Copy(self,num,wallSize):
		door = self._DoorList[num]
		
		fileName = door[0]
		wallNum = door[1]
		percentage = door[2]
		
		#オフセット量を算出
		offsetVal=0.1
		if wallSize != 0.0:
			model = self._ModelList[num]
			size = model.GetSize()
			offsetVal = (size[0]+0.1)/wallSize
		
		newPercentage = percentage + offsetVal
		if newPercentage > 0.99:
			newPercentage = 0.99
		
		self.Add(fileName,wallNum,newPercentage)
		
		return self._DoorList
			
	#位置設定
	def SetPosition(self,num,wallNum,percentage):
		model = self._ModelList[num]
		lineModel = self._LineModelList[num]
		
		position = [0,0]
		orientation = 0
		
		if model != None and wallNum != None:	
			point0,point1 = self._WallClass.GetPoint(wallNum)
			position,orientation = self.GetModelPosition(model,point0,point1,percentage)
			
			#床の位置だけオフセット
			position [0] = position[0] + self._FloorPosition[0]
			position [1] = position[1] + self._FloorPosition[1]
			position [2] = position[2] + self._FloorPosition[2]
			
			#モデル
			model.SetOrientation(orientation)
			model.SetPosition(position,self._DoorRef)
	
			#ラインモデル
			lineModel.SetOrientation(orientation)
			lineModel.SetPosition(position)
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
	
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
		lineModel = self._LineModelList[num]
		lineModel.SetHighlight(state)
		
	#ハイライト状態の取得
	def GetHighlight(self,num):
		model = self._ModelList[num]
		state = model.GetHighlight()
		
		return state
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model or self._LineModelList[x]._Model == model:
				num =  x
		
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for x in range(len(self._ModelList)):
			#model = self._ModelList[x]
			#model.SetPickable(state)
			
			lineModel = self._LineModelList[x]
			lineModel.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#ラインモデル取得
	def GetLineModel(self,num):
		lineModel = self._LineModelList[num]
		
		return lineModel
		
	#モデルの位置取得
	def GetModelPosition(self,door,point0,point1,percentage):
		position = [0,0,0]
		orientation = 0
		
		position[0] = point0[0]*(1-percentage)+point1[0]*percentage
		position[1] = 0.0
		position[2] = point0[1]*(1-percentage)+point1[1]*percentage
		
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		
		orientation = wallAngle - 90.0
		
		return position,orientation
		
	#ドアの表示状態を設定
	def SetVisible(self,state):
		for model in self._ModelList:
			model.SetVisible(state)
	
	#ラインの表示状態を設定
	def SetLineVisible(self,state):
		for lineModel in self._LineModelList:
			lineModel.SetVisible(state)
	
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
		for lineModel in self._LineModelList:
			lineModel.SetWallHeight(self._WallHeight)
			
	#壁のポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._DoorList)):
			data = self._DoorList[x]
			self.SetPosition(x,data[1],data[2])
		
	#リストを更新
	def UpdateList(self,num,wallNum,percentage):
		data = self._DoorList[num]
		data[1] = wallNum
		data[2] = percentage
		
		self._DoorList[num] = data
		
		return self._DoorList
		
	#壁のポイントを再設定
	def SetPointList(self,pointList,doorList):
		self._PointList = pointList
		self._DoorList = doorList
		
		self.ReCreate()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = []
		
		for door in self._ModelList:
			model = door.GetModel()
			modelList.append(model)
			
		return modelList
		
	#ドアの数を取得
	def GetCount(self):
		count = len(self._ModelList)
			
		return count
		
	#ドアの情報を取得
	def GetState(self,num):
		data = self._DoorList[num]
			
		return data
		
	#IDを入れ替え
	def ChangeId(self,num):
		#print 'changeID',num
		newDoorNum = len(self._DoorList)-1
		
		#リストを更新
		newDoorList			= []
		newModelList		= []
		newLineModelList	= []
		
		for x in range(len(self._DoorList)):
			door		= self._DoorList[x]
			model		= self._ModelList[x]
			lineModel	= self._LineModelList[x]
			
			if x == num:
				newDoor = self._DoorList[newDoorNum]
				newDoorList.append(newDoor)
				newModel = self._ModelList[newDoorNum]
				newModelList.append(newModel)
				newLineModel = self._LineModelList[newDoorNum]
				newLineModelList.append(newLineModel)
				
			if x != newDoorNum:
				newDoorList.append(door)
				newModelList.append(model)
				newLineModelList.append(lineModel)
				
		self._DoorList		= newDoorList
		self._ModelList		= newModelList
		self._LineModelList	= newLineModelList
		
		return self._DoorList
		
	#読み込みに失敗したかどうかを取得
	def GetLoadErrorState(self):
		return self._LoadErrorState
		