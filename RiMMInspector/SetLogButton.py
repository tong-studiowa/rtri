﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Object2d

import LoadIniFile
import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログファイル制御のクラス定義
class SetLogButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetLogButton] : Init SetLogButton')
		
		self._InputClass = None
		self._LogControlClass = None
		self._RoomControlClass = None
		self._ItemControlClass = None
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		
		self._SelectedFileList = [None,None,None,None]	#1つ目の間取り、アイテム、2つ目の間取り、アイテム
		
		self._IconCount = 3
		
		self._Frame = None
		self._TextList = []
		self._IconList = []
		
		self._Filter = [('INI Files','*.ini')]
		self._Directory = self.GetRootPath() + 'Settings\\Log\\.'
		self._RoomDirectory = self.GetRootPath() + 'Settings\\.'
		self._ItemDirectory = self.GetRootPath() + 'Settings\\.'
		
		#フレーム追加
		self._FramePosition = [0.753,0.204]
		self._FrameScale = [2.1,2.4]
		
		self._Frame = self.CreateFrame(self._FramePosition,self._FrameScale)
		
		#テキスト追加
		self._TextPositionX = 0.753
		self._TextPositionY = [0.232,0.176,0.12]
		self._TextScale = [0.16875,0.3]
		self._TextMessage = ["保存（1画面）","保存（2画面）","削除"]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			message = self._TextMessage[x]
			
			text = self.CreateText(message,position,self._TextScale)
			self._TextList.append(text)
			
		#アイコン追加
		self._IconPositionX = 0.753
		self._IconPositionY = [0.232,0.176, 0.12]
		self._IconScale = [2,0.5]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._IconPositionX
			position[1] = self._IconPositionY[x]
			
			icon = self.CreateIcon(position,self._IconScale)
			self._IconList.append(icon)
			
		#時間設定関係
		self._Title = None
		self._TextBox = None
		
		self._Value = 60	#初期値は60秒
		
		#タイトル追加
		self._TitlePosition = [0.707,0.288]
		self._TitleScale = [0.16875,0.3]
		
		self._Title = self.CreateText("時間(秒)",self._TitlePosition,self._TitleScale)
		
		#テキストボックス追加
		self._TextPosition = [0.785,0.288]
		self._TextScale = [0.4,1.22]
		
		self._TextBox = self.CreateTextBox(self._TextPosition,self._TextScale)
		self.SetText()
		
		#ファイル選択用ウィンドウ設定
		self._SelectFileVisibleState = True
		
		self._SelectFileIconCount = 4
		self._SelectFileIcon2Count = 2
		
		self._SelectFileFrame = None
		self._SelectFileTextList = []
		self._SelectFileIconList = []
		self._SelectFileTitleList = []
		self._SelectFileText2List = []
		self._SelectFileIcon2List = []
		self._SelectFileText3List = []
		self._SelectFileIcon3List = []
		
		#フレーム追加
		self._SelectFileFramePosition = [0.5,0.525]
		self._SelectFileFrameScale = [7.5,4]
		
		self._SelectFileFrame = self.CreateFrame(self._SelectFileFramePosition,self._SelectFileFrameScale)
		
		#テキスト追加
		self._SelectFileTextPositionX = 0.72
		self._SelectFileTextPositionY = [0.65,0.594,0.538,0.482]
		self._SelectFileTextScale = [0.16875,0.3]
		self._SelectFileTextMessage = ["参照","参照","参照","参照"]
		
		for x in range(self._SelectFileIconCount):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileTextPositionX
			position[1] = self._SelectFileTextPositionY[x]
			
			message = self._SelectFileTextMessage[x]
			
			text = self.CreateText(message,position,self._SelectFileTextScale)
			self._SelectFileTextList.append(text)
			
		#アイコン追加
		self._SelectFileIconPositionX = 0.72
		self._SelectFileIconPositionY = [0.65,0.594,0.538,0.482]
		self._SelectFileIconScale = [1,0.5]
		
		for x in range(self._SelectFileIconCount):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileIconPositionX
			position[1] = self._SelectFileIconPositionY[x]
			
			icon = self.CreateIcon(position,self._SelectFileIconScale)
			self._SelectFileIconList.append(icon)
			
		#タイトル追加
		self._SelectFileTitlePositionX = 0.28
		self._SelectFileTitlePositionY = [0.65,0.594,0.538,0.482]
		self._TitleScale = [0.16875,0.3]
		self._SelectFileTitleMessage = ["部屋データ","配置データ","部屋データ","配置データ"]
		
		for x in range(self._SelectFileIconCount):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileTitlePositionX
			position[1] = self._SelectFileTitlePositionY[x]
			
			message = self._SelectFileTitleMessage[x]
			
			title = self.CreateText(message,position,self._SelectFileTextScale)
			self._SelectFileTitleList.append(title)
			
		#ファイル名用テキスト追加
		self._SelectFileText2PositionX = 0.505
		self._SelectFileText2PositionY = [0.65,0.594,0.538,0.482]
		self._SelectFileText2Scale = [0.16875,0.3]
		self._SelectFileText2Message = ["","","",""]
		
		for x in range(self._SelectFileIconCount):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileText2PositionX
			position[1] = self._SelectFileText2PositionY[x]
			
			message = self._SelectFileText2Message[x]
			
			text = self.CreateText(message,position,self._SelectFileText2Scale)
			self._SelectFileText2List.append(text)
			
		#ファイル名用アイコン追加
		self._SelectFileIcon2PositionX = 0.505
		self._SelectFileIcon2PositionY = [0.65,0.594,0.538,0.482]
		self._SelectFileIcon2Scale = [4.3,0.5]
		
		for x in range(self._SelectFileIconCount):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileIcon2PositionX
			position[1] = self._SelectFileIcon2PositionY[x]
			
			icon = self.CreateIcon(position,self._SelectFileIcon2Scale)
			self._SelectFileIcon2List.append(icon)
			
		#決定用テキスト追加
		self._SelectFileText3PositionX = [0.43,0.57]
		self._SelectFileText3PositionY = 0.39
		self._SelectFileText3Scale = [0.16875,0.3]
		self._SelectFileText3Message = ["戻る","決定"]
		
		for x in range(self._SelectFileIcon2Count):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileText3PositionX[x]
			position[1] = self._SelectFileText3PositionY
			
			message = self._SelectFileText3Message[x]
			
			text = self.CreateText(message,position,self._SelectFileText3Scale)
			self._SelectFileText3List.append(text)
			
		#決定用アイコン追加
		self._SelectFileIcon3PositionX = [0.43,0.57]
		self._SelectFileIcon3PositionY = 0.39
		self._SelectFileIcon3Scale = [1,0.5]
		
		for x in range(self._SelectFileIcon2Count):
			
			position = [0.0,0.0]
			position[0] = self._SelectFileIcon3PositionX[x]
			position[1] = self._SelectFileIcon3PositionY
			
			icon = self.CreateIcon(position,self._SelectFileIcon3Scale)
			self._SelectFileIcon3List.append(icon)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,roomControlClass,itemControlClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._RoomControlClass = roomControlClass
		self._ItemControlClass = itemControlClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetLogButton] : Start SetLogButton Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			
			if mousePosition[0] > 0.515 and mousePosition[0] < 0.672:
				num = None
				if mousePosition[1] < 0.09:
					pass
				elif mousePosition[1] < 0.14:
					num = 2
				elif mousePosition[1] < 0.20:
					num = 1
				elif mousePosition[1] < 0.26:
					num = 0
				elif mousePosition[1] < 0.31:
					num = 99
				
				if num != None and num != 99:
					self._OverIconState = num
				
				if num != None:
					self._OverState = True
					
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.SaveLog()		#ログを保存する
						elif num == 1:
							self.SaveLog2()		#ログを保存する（2画面）
						elif num == 2:
							self.DeleteLog()	#ログを削除する
						
			#ロールオーバー
			self.Rollover(self._OverIconState,self._IconList)
				
		#ファイル選択画面のマウスの重なり状態を確認
		self._OverIconState2 = None
		self._OverIconState3 = None
		
		if self._SelectFileVisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			
			if mousePosition[0] > 0.207 and mousePosition[0] < 0.792:
				if mousePosition[1] > 0.33 and mousePosition[1] < 0.72:
					self._OverState = True
					
			if mousePosition[0] > 0.68 and mousePosition[0] < 0.76:
				num = None
				if mousePosition[1] < 0.453:
					pass
				elif mousePosition[1] < 0.51:
					num = 3
				elif mousePosition[1] < 0.566:
					num = 2
				elif mousePosition[1] < 0.62:
					num = 1
				elif mousePosition[1] < 0.677:
					num = 0
				
				if num != None:
					self._OverIconState2 = num
				
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						self.FileSelect(num)		#ファイルを選択する
							
			if mousePosition[1] > 0.363 and mousePosition[1] < 0.416:
				num = None
				if mousePosition[0] < 0.389:
					pass
				elif mousePosition[0] < 0.469:
					num = 0
				elif mousePosition[0] < 0.53:
					pass
				elif mousePosition[0] < 0.61:
					num = 1
				
				if num != None:
					self._OverIconState3 = num
				
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.CancelFileSelect()		#保存をキャンセル
						elif num == 1:
							self.FinFileSelect()		#ファイルの選択を完了
							
			#ロールオーバー
			self.Rollover(self._OverIconState2,self._SelectFileIconList)
			self.Rollover(self._OverIconState3,self._SelectFileIcon3List)
			
		self.InputVal()
		
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
		self.SetValue(self._LogControlClass.GetTime())
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		
		for text in self._TextList:
			text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
		
		self._Title.visible(val)
		
		if state == True:
			self.SetValue(self._LogControlClass.GetTime())
			
		self._TextBox.visible(val)
		
		self.SetSelectFileVisible(False)
		
	#ファイル選択画面の表示状態設定
	def SetSelectFileVisible(self,state):
		val = viz.OFF
		if state == True:
			self._SelectFileVisibleState = True
			val = viz.ON
		else:
			self._SelectFileVisibleState = False
			val = viz.OFF
			
		self._SelectFileFrame.visible(val)
		
		for text in self._SelectFileTextList:
			text.visible(val)
		
		for icon in self._SelectFileIconList:
			icon.visible(val)
		
		for title in self._SelectFileTitleList:
			title.visible(val)
		
		for text2 in self._SelectFileText2List:
			text2.visible(val)
		
		for icon2 in self._SelectFileIcon2List:
			icon2.visible(val)
		
		for text3 in self._SelectFileText3List:
			text3.visible(val)
		
		for icon3 in self._SelectFileIcon3List:
			icon3.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#フレームの作成
	def CreateFrame(self,position,scale):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.setPosition(position[0],position[1])
		frame.setScale(scale[0],scale[1])
		frame.color(viz.GRAY)
		frame.drawOrder(1)
		frame.alpha(0.75)
		
		return frame
		
	#テキストの作成
	def CreateText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		text.setPosition(position[0],position[1])
		text.setScale(scale[0],scale[1])
		text.color(viz.WHITE)
		text.drawOrder(2)
		
		return text
		
	#アイコンの作成
	def CreateIcon(self,position,scale):
		icon = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		icon.setPosition(position[0],position[1])
		icon.setScale(scale[0],scale[1])
		icon.color(viz.GRAY)
		icon.drawOrder(1)
		
		return icon
		
	#テキストボックスの作成
	def CreateTextBox(self,position,scale):
		textBox = viz.addTextbox()
		textBox.setPosition(position[0],position[1])
		textBox.setScale(scale[0],scale[1])
		textBox.length(0.545)
		textBox.color(viz.GRAY)
		textBox.drawOrder(1)
		textBox.overflow(viz.OVERFLOW_GROW)
		
		return textBox
		
	#数値を設定
	def SetValue(self,val):
		self._Value = val
		self.SetText()
		
	#数値を取得
	def GetValue(self):
		val = self._Value
		
		return val
		
	#数値の表示を更新
	def SetText(self):
		val = self._Value
		message = str(val)
		self._TextBox.message(message)
	
	#文字入力
	def InputVal(self):
		if self._VisibleState == True:
			if viz.key.isDown(viz.KEY_RETURN):
				inputVal = self._TextBox.getMessage()
				preVal = self.GetValue()
				val = 1
				
				try:
					val = int(inputVal)
				except:
					val = preVal
				
				if val != preVal:
					self.SetValue(val)
					self._LogControlClass.SetTime(val)
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#ログを読み込む
	def LoadLog(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetLogButton] : LoadLog , directory='+self._Directory)
		filePath = vizinput.fileOpen(filter=self._Filter,file=self._Directory)
		
		if filePath != "":
			self._LogControlClass.Load(filePath)
		
		self.SetVisible(False)
			
	#ログを保存
	def SaveLog(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetLogButton] : SaveLog , directory='+self._Directory)
		
		if self._LogControlClass.GetLogCount() < 2:
			vizinput.message('ログが作成されていません', title='RiMMInspector')
			return
			
		filePath = vizinput.fileSave(file=self._Directory)
		
		if filePath != "":
			if filePath.endswith('.ini') == False:
				filePath = filePath + '.ini'

			self._LogControlClass.Save(filePath)
		
		self.SetVisible(False)
		
	#ログを保存（2画面）
	def SaveLog2(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetLogButton] : SaveLog2 , directory='+self._Directory)
		
		if self._LogControlClass.GetLogCount() < 2:
			vizinput.message('ログが作成されていません', title='RiMMInspector')
			return
			
		#間取りを設定
		self._SelectedFileList = [None,None,None,None]
		self._SelectFileText2Message = ["","","",""]
		
		#間取りを設定
		roomFileName = self._RoomControlClass.GetIniFileName()
		self._SelectedFileList[0] = roomFileName
		self._SelectFileText2Message[0] = self.GetFileName(roomFileName)
		
		#アイテムを設定
		itemFileName = self._ItemControlClass.GetIniFileName()
		self._SelectedFileList[1] = itemFileName
		self._SelectFileText2Message[1] = self.GetFileName(itemFileName)
		
		self.SetFileNameText()
		self.SetSelectFileVisible(True)
		
	#数値の表示を更新
	def SetFileNameText(self):
		for x in range(len(self._SelectFileText2List)):
			text = self._SelectFileText2List[x]
			text.message(self._SelectFileText2Message[x])
			
	#ファイル選択
	def FileSelect(self,id):
		path = ''
		
		directory = self._RoomDirectory
		if id == 1 or id == 3:
			directory = self._ItemDirectory
			
		selectedPath = vizinput.fileOpen(filter=self._Filter,file=directory)
		
		if selectedPath != '':
			iniFile = LoadIniFile.LoadIniFileClass()
			iniFile.LoadIniFile(selectedPath)
			
			if id == 0 or id == 2:	#間取り
				room0 = iniFile.GetItemData('Rooms','0')
				
				if room0 == None:
					vizinput.message('ファイル読み込みエラー!\n指定のINIファイルは間取り定義ファイルではありません。', title='RiMMInspector')
				else:
					path = selectedPath
		
			elif id == 1 or id == 3:	#アイテム
				total = iniFile.GetItemData('Items','Total')
				
				if total == None:
					vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはアイテム定義ファイルではありません。', title='RiMMInspector')
				else:
					path = selectedPath
		
			del iniFile
		
		self._SelectedFileList[id] = path
		self._SelectFileText2Message[id] = self.GetFileName(path)
		
		self.SetFileNameText()
		
	#ファイル選択完了
	def FinFileSelect(self):
		#間取りやアイテムを選択しているか確認
		selectedState = True
		for fileName in self._SelectedFileList:
			if fileName == None:
				selectedState = False
		
		#選択していたら保存
		if selectedState:
			filePath = vizinput.fileSave(file=self._Directory)
			
			if filePath != "":
				if filePath.endswith('.ini') == False:
					filePath = filePath + '.ini'

				self._LogControlClass.Save2(filePath,self._SelectedFileList)
				
			self.SetVisible(False)
		
		else:
			vizinput.message('間取りとアイテムを選択して下さい')
			
	#ファイル選択をキャンセル
	def CancelFileSelect(self):
		self.SetSelectFileVisible(False)
		
	#ログを削除
	def DeleteLog(self):
		self._LogControlClass.DeleteAll()
		
		self.SetVisible(False)
	
	#ロールオーバー処理
	def Rollover(self,selectedIcon,iconList):
		for x in range(len(iconList)):
			icon = iconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
				
	#ルートフォルダの取得
	def GetRootPath(self):
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
			exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
			
		return rootDir
		
	#ファイルパスからファイル名を取得
	def GetFileName(self,filePath):
		fileNameTemp = filePath.split('\\')
		fileNameText = ''
		for text in fileNameTemp:
			if text[-4:] == '.ini':
				fileNameText = text
			
		return fileNameText