﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

#---------------------------------------------------------
from ctypes import *

#---------------------------------------------------------
class Fove():
	"""
	Foveを制御くするクラス
	"""
	
	#---------------------------------------------------------
	def __init__(self):
		"""
		コンストラクタ
		"""		
		self.__mFove = cdll.LoadLibrary("Fove/FoveDll.dll")
		self.__mFove.Initialize()
	
	#---------------------------------------------------------	
	def __del__(self):
		"""
		デストラクタ
		"""
		self.Finalize()
	
	#---------------------------------------------------------	
	def Finalize(self):
		"""
		終了化
		"""
		if self.__mFove:
			self.__mFove.Finalize()
			self.__mFove = None
	
	#---------------------------------------------------------	
	def GetEyePos(self):
		"""
		左の眼の座標を取得
		"""
		lPoint = [0.0] * 3
		rPoint = [0.0] * 3
		lBuf = cast((c_double * 3)(), POINTER(c_double))
		rBuf = cast((c_double * 3)(), POINTER(c_double))
		self.__mFove.GetEyeVector(lBuf, rBuf)
		for i in range(3):
			lPoint[i] = lBuf[i]
			rPoint[i] = rBuf[i]
		return [lPoint, rPoint]
	