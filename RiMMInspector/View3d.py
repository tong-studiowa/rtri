﻿# coding: utf-8

import viz
import vizact
import vizshape
import vizmat
import Object3d
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

import GetCameraFovFunction
fov = GetCameraFovFunction.GetCameraFov('Settings\\RiMMInspector_Settings.ini')
#print 'fov=',fov

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

moveSpeedTemp	= iniFile.GetItemData('Walkthrough','movespeed')
moveSpeed = 2.0
try:
	moveSpeed = float(moveSpeedTemp)
except:
	moveSpeed = 2.0
	
rotateSpeedTemp = iniFile.GetItemData('Walkthrough','rotatespeed')
rotateSpeed = 20.0
try:
	rotateSpeed = float(rotateSpeedTemp)
except:
	rotateSpeed = 20.0
	
zoomModeTemp = iniFile.GetItemData('View','ZoomMode')
zoomMode = 1
try:
	zoomMode = int(zoomModeTemp)
except:
	zoomMode = 1
	
zoomWheelSpeedTemp = iniFile.GetItemData('View','ZoomWheelSpeed')
zoomWheelSpeed = 1.0
try:
	zoomWheelSpeed = float(zoomWheelSpeedTemp)
except:
	zoomWheelSpeed = 1.0
	
zoomDrugSpeedTemp = iniFile.GetItemData('View','ZoomDrugSpeed')
zoomDrugSpeed = 1.0
try:
	zoomDrugSpeed = float(zoomDrugSpeedTemp)
except:
	zoomDrugSpeed = 1.0
	
zoomDistVal = 1.0
"""
zoomDistValTemp = iniFile.GetItemData('View','ZoomDistVal')
try:
	zoomDistVal = float(zoomDistValTemp)
except:
	zoomDistVal = 1.0
"""

# - - - - - - - - - - - - - - - - - - - - - - - - -
#3D視点制御のクラス定義
class View3d(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][View3d] : Init View3d')
		
		self._InputClass = None
		
		self._DispSize = viz.window.getSize()
		self._AspectRatio = float(self._DispSize[1]) / float(self._DispSize[0])
		
		#self._Fov = 65
		self._Fov = fov
		
		self._TranslateSpeed = 10.0
		self._SpinSpeed = 100.0
		
		self._ZoomMode			= zoomMode
		self._ZoomWheelSpeed	= zoomWheelSpeed * 0.1
		self._ZoomDrugSpeed		= zoomDrugSpeed * -1
		self._ZoomDistVal		= zoomDistVal
		
		#ターゲットの作成
		self._CameraTarget = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget.visible(viz.OFF)
		self._CameraTarget.disable(viz.PICKING)
		
		self._CameraTarget2 = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget2.visible(viz.OFF)
		self._CameraTarget2.disable(viz.PICKING)
		self._CameraTarget2.setParent(self._CameraTarget)
		
		self._CameraTarget3 = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget3.visible(viz.OFF)
		self._CameraTarget3.disable(viz.PICKING)
		self._CameraTarget3.setParent(self._CameraTarget2)
		self._CameraTarget3.setPosition([0.0,0.0,-10.0])
		
		self._CameraTarget2.setEuler([0,45,0])
		
		self._CameraPos = self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL)
		self._CameraTargetPos = [0,0,0]
		
		self._MoveViewKeyState = [False,False,False,False,False,False]
		self._RotateViewKeyState = [False,False,False,False]
		self._ResetViewKeyState = False
		
		self._MoveViewJoyState = [0.0,0.0,0.0,0.0,False,False]
		self._RotateViewJoyState = [0.0,0.0,0.0,0.0]
		self._ResetViewJoyState = False
		
		#向き確認用ダミー
		self._DummyA = vizshape.addPlane([0.001,0.001,0.001])
		self._DummyA.visible(viz.OFF)
		self._DummyA.disable(viz.PICKING)
		
		self._DummyB = vizshape.addPlane([0.001,0.001,0.001])
		self._DummyB.visible(viz.OFF)
		self._DummyB.disable(viz.PICKING)
		
		self._DummyB.setParent(self._DummyA)
		self._DummyB.setPosition([0,100,0])
		
		self._DummyC = vizshape.addPlane([0.001,0.001,0.001])
		self._DummyC.visible(viz.OFF)
		self._DummyC.disable(viz.PICKING)
		
		self._DummyC.setParent(self._DummyA)
		self._DummyC.setPosition([0,-100,0])
		
		#ジョイスティックの設定
		self._mMoveVal	 = moveSpeed
		self._mRotateVal = rotateSpeed
		
		self._EyeHeight = [1.5,25.0]	#ウォークスルーの高さ、上空から見下ろす高さ

	#使用するクラスの追加
	def AddClass(self,inputClass):
		self._InputClass = inputClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数	
	def Update(self,clickState):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][View3d] : Start View3d Update')
		
		#視点操作
		moveState = False
		if self._InputClass.GetAltKeyState():
			
			#マウス左ボタン操作（回転）
			if self._InputClass.GetShiftKeyState() == False and self._InputClass.GetMouseDragState() and clickState == False:
				self.Spin(self._InputClass.GetMouseDisplacement())
				moveState = True
				
			#マウス中ボタン操作（移動）
			if self._InputClass.GetMouseMiddleButtonState():
				self.Translate(self._InputClass.GetMouseDisplacement())
				moveState = True
				
			#マウス右ボタン操作（ズーム）
			if self._InputClass.GetMouseRightButtonState():
				zoomVal = self._InputClass.GetMouseDisplacement()[1] * self._ZoomDrugSpeed
				self.Zoom(zoomVal)
				moveState = True
			
		#マウスホイール操作
		mouseWheelState = self._InputClass.GetMouseWheelState()
		if mouseWheelState != 0:
			zoomVal = mouseWheelState * self._ZoomWheelSpeed
			self.Zoom(zoomVal)
			moveState = True
		
		#キーボードによるウォークスルー操作
		self._MoveViewKeyState = self._InputClass.GetMoveViewKeyState()
		self._RotateViewKeyState = self._InputClass.GetRotateViewKeyState()
		self._ResetViewKeyState = self._InputClass.GetResetViewKeyState()
		
		if self._MoveViewKeyState[0]:
			self._Move(0,0,self._mMoveVal*1.0,True)
		if self._MoveViewKeyState[3]:
			self._Move(self._mMoveVal*1.0,0,0)
		if self._MoveViewKeyState[1]:
			self._Move(0,0,self._mMoveVal*-1.0,True)
		if self._MoveViewKeyState[2]:
			self._Move(self._mMoveVal*-1.0,0,0)
		if self._MoveViewKeyState[4]==1:
			self._Move(0,self._mMoveVal*1.0,0)
		if self._MoveViewKeyState[5]==1:
			self._Move(0,self._mMoveVal*-1.0,0)
		
		if self._RotateViewKeyState[0]:
			self._Pan(self._mRotateVal*-1.0,0,0)
		if self._RotateViewKeyState[3]:
			self._Rotate(0,self._mRotateVal*1.0,0)
		if self._RotateViewKeyState[1]:
			self._Pan(self._mRotateVal*1.0,0,0)
		if self._RotateViewKeyState[2]:
			self._Rotate(0,self._mRotateVal*-1.0,0)
			
		#ジョイスティックによるウォークスルー操作
		self._MoveViewJoyState = self._InputClass.GetMoveViewJoyState()
		self._RotateViewJoyState = self._InputClass.GetRotateViewJoyState()
		self._ResetViewJoyState = self._InputClass.GetResetViewJoyState()
		#print 'self._MoveViewJoyState',self._MoveViewJoyState
		#print 'self._RotateViewJoyState',self._RotateViewJoyState
		
		#移動
		if self._MoveViewJoyState[0]>0.2:
			self._Move(0,0,self._mMoveVal*self._MoveViewJoyState[0],True)
		if self._MoveViewJoyState[1]>0.2:
			self._Move(0,0,self._mMoveVal*self._MoveViewJoyState[1]*-1,True)
		if self._MoveViewJoyState[2]>0.2:
			self._Move(self._mMoveVal*self._MoveViewJoyState[2]*-1,0,0)
		if self._MoveViewJoyState[3]>0.2:
			self._Move(self._mMoveVal*self._MoveViewJoyState[3],0,0)
			
		#視点操作
		if self._RotateViewJoyState[3]>0.2:
			self._Rotate(0,self._mRotateVal*self._RotateViewJoyState[3],0)
			self._Pan(0,self._mRotateVal*self._RotateViewJoyState[3],0)
		if self._RotateViewJoyState[2]>0.2:
			self._Rotate(0,self._mRotateVal*self._RotateViewJoyState[2]*-1,0)
			self._Pan(0,self._mRotateVal*self._RotateViewJoyState[2]*-1,0)
		if self._RotateViewJoyState[0]>0.2:
			self._Pan(self._mRotateVal*self._RotateViewJoyState[0],0,0)
		if self._RotateViewJoyState[1]>0.2:
			self._Pan(self._mRotateVal*self._RotateViewJoyState[1]*-1,0,0)
			
		#視点リセット
		if self._ResetViewKeyState:
			self._ResetRotationOfEyeView()
			
		#self.WalkTranslate()
		#self.WalkSpin()
		#self.WalkReset()
		
		#カメラ位置を更新
		if moveState:
			viz.MainView.setPosition(self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL))
			viz.MainView.setEuler(self._CameraTarget3.getEuler(mode=viz.ABS_GLOBAL))
			
	#リセットするための関数
	def Reset(self):
		self._CameraTarget.setPosition(self._CameraTargetPos[0],self._CameraTargetPos[1],self._CameraTargetPos[2])
		viz.MainView.setPosition(self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL))
		viz.MainView.setEuler(self._CameraTarget3.getEuler(mode=viz.ABS_GLOBAL))
		
		self.SetAspectRatio()
	
	#移動
	def Translate(self,displacementVal):
		dist = self.GetDistance()
		
		x = displacementVal[0] * -self._TranslateSpeed
		y = displacementVal[1] * -self._TranslateSpeed
		
		viz.MainView.move(x,y,dist)
		newPos = viz.MainView.getPosition()
		self._CameraTarget.setPosition(newPos)
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#回転
	def Spin(self,displacementVal):
		x = displacementVal[0] * self._SpinSpeed
		y = displacementVal[1] * -self._SpinSpeed
		
		#横方向
		angleX = self._CameraTarget.getEuler()
		angleX[0] = angleX[0] + x
		self._CameraTarget.setEuler(angleX)
		
		#縦方向
		angleY = self._CameraTarget2.getEuler()
		angleY[0] = 0.0
		angleY[1] = angleY[1] + y
		angleY[2] = 0.0
		
		#真上、真下を超えないよう制限
		if angleY[1] > 89.0:
			angleY[1] = 89.0
		elif angleY[1] < -89.0:
			angleY[1] = -89.0
			
		self._CameraTarget2.setEuler(angleY)
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#ズーム
	def Zoom(self,wheelVal):
		dist = self.GetDistance()
		if self._ZoomMode == 0:
			dist = 1.0
			wheelVal *= 10.0
			
		z = wheelVal * (dist * self._ZoomDistVal)
		
		pos = self._CameraTarget3.getPosition()
		pos[2] = pos[2] + z
		
		self._CameraTarget3.setPosition(pos)
		
		#回転の中心からの距離が1m以下の場合、中心をずらす
		dist = self.GetDistance()
		if dist < 1.0:
			offset = 1.0 - dist
			pos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
			
			self._CameraTarget3.setPosition([0.0,0.0,offset])
			newPos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
			
			self._CameraTarget.setPosition(newPos)
			self._CameraTarget3.setPosition([0.0,0.0,-1.0])
			
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#回転の中心から視点までの距離を取得
	def GetDistance(self):
		pos = self._CameraTarget3.getPosition()
		distance = pos[2] * -1
		return distance
		
	#カメラターゲットの位置を設定
	def SetCameraTargetPos(self,pos):
		distance = [0,0,0]
		distance[0] = self._CameraPos[0] - self._CameraTargetPos[0]
		distance[1] = self._CameraPos[1] - self._CameraTargetPos[1]
		distance[2] = self._CameraPos[2] - self._CameraTargetPos[2]
		
		camTargetPos = [0,0,0]
		camTargetPos[0] = pos[0]
		camTargetPos[1] = self._CameraTargetPos[1]
		camTargetPos[2] = pos[2]
		
		self._CameraTargetPos = camTargetPos
		
		camPos = [0,0,0]
		camPos[0] = pos[0] + distance[0]
		camPos[1] = self._CameraPos[1]
		camPos[2] = pos[2] + distance[2]
		
		self._CameraPos = camPos
		
	#カメラターゲットの位置を設定
	def SetCameraTargetPos2(self):
		#位置設定
		pos = viz.MainView.getPosition()
		
		targetpos = self._CameraTarget.getPosition(viz.ABS_GLOBAL)
		target3pos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
		
		offsetPos = [0,0,0]
		offsetPos[0] = targetpos[0] - target3pos[0]
		offsetPos[1] = targetpos[1] - target3pos[1]
		offsetPos[2] = targetpos[2] - target3pos[2]
		
		newPos = [0,0,0]
		newPos[0] = pos[0] + offsetPos[0]
		newPos[1] = pos[1] + offsetPos[1]
		newPos[2] = pos[2] + offsetPos[2]
		
		self._CameraTarget.setPosition(newPos)
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
		#角度設定
		angle = viz.MainView.getEuler()
		targetAngle = [0,0,0]
		targetAngle[0] = angle[0]
		self._CameraTarget.setEuler(targetAngle)
		
		targetAngle2 = [0,0,0]
		targetAngle2[1] = angle[1]
		self._CameraTarget2.setEuler(targetAngle2)
		
	#カメラターゲットの位置を取得
	def GetCameraTargetPos(self):
		pos = self._CameraTargetPos
		return pos
		
	#キーボード、ジョイスティック操作による移動
	def WalkTranslate(self):
		#視点を移動
		moveVal = [0.0,0.0,0.0]
		
		#キーボード
		if self._MoveViewKeyState[0]:
			moveVal[2] = self._TranslateSpeed
		if self._MoveViewKeyState[1]:
			moveVal[2] = moveVal[2] + self._TranslateSpeed * -1
			
		if self._MoveViewKeyState[2]:
			moveVal[0] = self._TranslateSpeed * -1
		if self._MoveViewKeyState[3]:
			moveVal[0] = moveVal[0] + self._TranslateSpeed
			
		if self._MoveViewKeyState[4]:
			moveVal[1] = self._TranslateSpeed
		if self._MoveViewKeyState[5]:
			moveVal[1] = moveVal[1] + self._TranslateSpeed * -1
		
		#ジョイスティック
		if self._MoveViewJoyState[0] != 0.0:
			moveVal[2] = moveVal[2] + self._TranslateSpeed * self._MoveViewJoyState[0]
		if self._MoveViewJoyState[1] != 0.0:
			moveVal[2] = moveVal[2] + self._TranslateSpeed * -1 * self._MoveViewJoyState[1]
			
		if self._MoveViewJoyState[2] != 0.0:
			moveVal[0] = moveVal[0] + self._TranslateSpeed * -1 * self._MoveViewJoyState[2]
		if self._MoveViewJoyState[3] != 0.0:
			moveVal[0] = moveVal[0] + self._TranslateSpeed * self._MoveViewJoyState[3]
			
		if self._MoveViewJoyState[4]:
			moveVal[1] = moveVal[1] + self._TranslateSpeed
		if self._MoveViewJoyState[5]:
			moveVal[1] = moveVal[1] + self._TranslateSpeed * -1
		
		#移動量の調整
		moveVal[0] = moveVal[0] * 0.0002
		moveVal[1] = moveVal[1] * 0.0002
		moveVal[2] = moveVal[2] * 0.0002
		
		targetLocalPos = [0.0,0.0,0.0]
		targetLocalPos[0] = self._CameraTargetPos[0] - self._CameraPos[0]
		targetLocalPos[1] = self._CameraTargetPos[1] - self._CameraPos[1]
		targetLocalPos[2] = self._CameraTargetPos[2] - self._CameraPos[2]
		
		viz.MainView.move(moveVal)
		
		#移動制限（床より下に行かないよう制限）
		newmoveVal = viz.MainView.getPosition()
		if newmoveVal[1] < 0:
			newmoveVal[1] = 0.0
			viz.MainView.setPosition(newmoveVal)
		
		self._CameraPos = viz.MainView.getPosition()
		
		#ターゲットを移動
		targetPosition = [0.0,0.0,0.0]
		targetPosition[0] = self._CameraPos[0] + targetLocalPos[0]
		targetPosition[1] = self._CameraPos[1] + targetLocalPos[1]
		targetPosition[2] = self._CameraPos[2] + targetLocalPos[2]
		
		self._CameraTarget.setPosition(targetPosition)
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#キーボード、ジョイスティック操作による回転
	def WalkSpin(self):
		preDistance = self.GetDistance()
		
		spinSpeedY = 0
		spinSpeedX = 0
		
		#キーボード
		if self._RotateViewKeyState[0]:
			spinSpeedY = self._SpinSpeed * -1
		if self._RotateViewKeyState[1]:
			spinSpeedY = spinSpeedY + self._SpinSpeed
			
		if self._RotateViewKeyState[2]:
			spinSpeedX = self._SpinSpeed
		if self._RotateViewKeyState[3]:
			spinSpeedX = spinSpeedX + self._SpinSpeed * -1
		
		#ジョイスティック
		if self._RotateViewJoyState[0] != 0.0:
			spinSpeedY = spinSpeedY + self._SpinSpeed * self._RotateViewJoyState[0] * -1
		if self._RotateViewJoyState[1] != 0.0:
			spinSpeedY = spinSpeedY + self._SpinSpeed * self._RotateViewJoyState[1]
			
		if self._RotateViewJoyState[2] != 0.0:
			spinSpeedX = spinSpeedX + self._SpinSpeed * self._RotateViewJoyState[2]
		if self._RotateViewJoyState[3] != 0.0:
			spinSpeedX = spinSpeedX + self._SpinSpeed * self._RotateViewJoyState[3] * -1
		
		#回転量の調整
		moveVal = [0.0,0.0,0.0]
		moveVal[0] = spinSpeedX * preDistance * -0.00005
		moveVal[1] = spinSpeedY * preDistance * -0.00005
		
		preViewPos = viz.MainView.getPosition()
		viz.MainView.move(moveVal)
		
		viz.lookAt(self._CameraTarget.getPosition())
		
		distance = self.GetDistance()
		viz.MainView.move(0,0,distance - preDistance)
		
		viewPos = viz.MainView.getPosition()
		
		disp = [0.0,0.0,0.0]
		disp[0] = preViewPos[0] - viewPos[0]
		disp[1] = preViewPos[1] - viewPos[1]
		disp[2] = preViewPos[2] - viewPos[2]
		
		viz.MainView.setPosition(preViewPos)
		
		targetPos = self._CameraTarget.getPosition()
		targetPos[0] = targetPos[0] - disp[0]
		targetPos[1] = targetPos[1] - disp[1]
		targetPos[2] = targetPos[2] - disp[2]
		
		self._CameraTarget.setPosition(targetPos)
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#キーボード、ジョイスティック操作による視点位置リセット
	def WalkReset(self):
		if self._ResetViewKeyState or self._ResetViewJoyState:
			preDistance = self.GetDistance()
			
			targetPos = [0.0,0.0,0.0]
			targetPos[0] = self._CameraTargetPos[0]
			targetPos[1] = self._CameraPos[1]
			targetPos[2] = self._CameraTargetPos[2]
			
			self._CameraTarget.setPosition(targetPos)
			viz.lookAt(self._CameraTarget.getPosition())
			
			distance = self.GetDistance()
			
			preCameraPos = viz.MainView.getPosition()
			
			moveVal = [0.0,0.0,0.0]
			moveVal[2] = distance - preDistance
			viz.MainView.move(moveVal)
			
			cameraPos = viz.MainView.getPosition()
			
			targetPos = self._CameraTarget.getPosition()
			targetPos[0] = targetPos[0] - (cameraPos[0] - preCameraPos[0])
			targetPos[2] = targetPos[2] - (cameraPos[2] - preCameraPos[2])
			
			viz.MainView.setPosition(preCameraPos)
			self._CameraTarget.setPosition(targetPos)
			
			viz.lookAt(self._CameraTarget.getPosition())
			
			self._CameraPos = viz.MainView.getPosition()
			self._CameraTargetPos = self._CameraTarget.getPosition()
			
	#パスシムから受け取った情報を設定
	def SetViewPos(self,data):
		self._CameraTarget.setPosition(data[0][0])
		self._CameraTarget.setEuler(data[0][1])
		
		self._CameraTarget2.setPosition(data[1][0])
		self._CameraTarget2.setEuler(data[1][1])
		
		self._CameraTarget3.setPosition(data[2][0])
		self._CameraTarget3.setEuler(data[2][1])
		
		viz.MainView.setPosition(data[3][0])
		viz.MainView.setEuler(data[3][1])
		
		self._CameraPos = data[3][0]
		self._CameraTargetPos = data[0][0]
		
	#パスシムへ渡す情報を設定
	def GetViewPos(self):
		target1Pos	 = self._CameraTarget.getPosition()
		target1Euler = self._CameraTarget.getEuler()
		
		target2Pos	 = self._CameraTarget2.getPosition()
		target2Euler = self._CameraTarget2.getEuler()
		
		target3Pos	 = self._CameraTarget3.getPosition()
		target3Euler = self._CameraTarget3.getEuler()
		
		viewPos	 	 = viz.MainView.getPosition()
		viewEuler	 = viz.MainView.getEuler()
		
		return [[target1Pos,target1Euler],[target2Pos,target2Euler],[target3Pos,target3Euler],[viewPos,viewEuler]]
		
	def _Move(self,iX,iY,iZ,keepHeight=False):
		prePos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
		elapsedTime = viz.getFrameElapsed()
		moveVal = [iX*elapsedTime,iY*elapsedTime,iZ*elapsedTime]
		viz.MainView.move(moveVal)
		if keepHeight:
			newPos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			newPos[1] = prePos[1]
			viz.MainView.setPosition(newPos,mode=viz.ABS_GLOBAL)
		self.SetCameraTargetPos2()
		
	def _Rotate(self,iX,iY,iZ):
		currentEuler = viz.MainView.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,currentEuler[1],currentEuler[2]]
		viz.MainView.setEuler(rotateVal)
		self.SetCameraTargetPos2()
		
	def _Pan(self,iX,iY,iZ):
		currentEuler = viz.MainView.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,currentEuler[1]+iX*elapsedTime,currentEuler[2]]
		viz.MainView.setEuler(rotateVal)
		self.SetCameraTargetPos2()
		
	def _ResetRotationOfEyeView(self):
		if self._InputClass.GetShiftKeyState():
			ang1=viz.MainView.getEuler()
			ang2=viz.MainView.getEuler()
			viz.MainView.setEuler(0,90,0)
			viz.MainView.setEuler(0,90,0)
			pos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			viz.MainView.setPosition([pos[0],self._EyeHeight[1],pos[2]],mode=viz.ABS_GLOBAL)
		else:
			ang1=viz.MainView.getEuler()
			ang2=viz.MainView.getEuler()
			viz.MainView.setEuler(ang1[0],0,0)
			viz.MainView.setEuler(ang2[0],0,0)
			pos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			viz.MainView.setPosition([pos[0],self._EyeHeight[0],pos[2]],mode=viz.ABS_GLOBAL)
		self.SetCameraTargetPos2()
		
	def SetEyeHeight(self,num,val):
		self._EyeHeight[num] = val * 0.001
		
	def GetEyeHeight(self,num):
		val = self._EyeHeight[num] * 1000.0
		return val
		
	def SetEyeHeightList(self,list):
		for x in range(2):
			self._EyeHeight[x] = list[x] * 0.001
			
	#画角を設定
	def SetAspectRatio(self):
		self._DispSize = viz.window.getSize()
		if self._DispSize[1] != 0:
			self._AspectRatio = float(self._DispSize[0]) / float(self._DispSize[1])
		else:
			self._AspectRatio = float(self._DispSize[0]) / 1.0
			
		viz.fov(self._Fov, self._AspectRatio)
		