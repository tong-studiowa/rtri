﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat
import vizfx

import math
import Object2d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面の表示状態設定クラス定義
class ShowPath(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ShowPath] : Init ShowPath')
		
		self._InputClass = None
		self._PathControlClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		
		#フレーム追加
		self._Position = [226,362]
		self._PositionX = [226,63]
		self._Scale = [100,36]
		self._Frame = self.CreateFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._TextScale = [18.6,18.6]
		self._Text = self.CreateText("",self._Position,self._TextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,pathControlClass,stencilClass,manipulatorClass):
		self._InputClass = inputClass
		self._PathControlClass = pathControlClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ShowPath] : Start ShowPath Update')

		self._ClickState = False
		self._OverState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
				
		if self._OverState:
			if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
				self._ClickState = True
				
				if self._IsVisible == True:
					self._IsVisible = False
				else:
					self._IsVisible = True
					
				self.Set()
			
		#ロールオーバー
		self.Rollover(self._OverState)

	#リセット処理
	def Reset(self):
		self._ClickState = False
		self._IsVisible = True
		self.SetText(True)
		self.SetPathVisible(True)
		
		self.SetVisible(False)
		self.Set()
		
	#設定を反映
	def Set(self):
		self.SetText(False)
		self.SetPathVisible(False)
		"""
		if self._IsVisible == False:
			self.SetText(False)
			self.SetPathVisible(False)
		else:
			self.SetText(True)
			self.SetPathVisible(True)
		"""
			
	#表示状態設定
	def SetVisible(self,state):
		self._Frame.visible(viz.OFF)
		self._Text.visible(viz.OFF)
		"""
		if state:
			self._Frame.visible(viz.ON)
			self._Text.visible(viz.ON)
		else:
			self._Frame.visible(viz.OFF)
			self._Text.visible(viz.OFF)
		"""
		
	#パスの表示状態設定
	def SetPathVisible(self,state):
		if state == True:
			self._IsVisible = True
		else:
			self._IsVisible = False
			
		self._PathControlClass.SetVisible(self._IsVisible)
		
	#パスの表示状態取得
	def GetPathVisible(self):
		return self._IsVisible
		
	#テキスト設定
	def SetText(self,state):
		if state == False:
			self._Text.message("パス有")
		else:
			self._Text.message("パス無")
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#表示状態の設定
	def SetViewState(self,state):
		if state:
			self._Position[0] = self._PositionX[1]
		else:
			self._Position[0] = self._PositionX[0]
		
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Text,self._Position,self._TextScale)
		