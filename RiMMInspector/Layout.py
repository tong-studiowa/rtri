﻿# coding: utf-8
#PathSimulator

import viz
import viztask
import vizmat
import vizinput
import vizshape
import LoadIniFile
import PathControlClass
import LoadItemDataClass
import LoadRoomDataClass
#import GridControlClass	#不要
#import SSAO	#不要
import Map
import PathControlClassLayouter
import os.path

from threading import Thread
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

#モード切り替え関係
mode = False
def SetMode(state):
	global mode
	global mapData
	global mapClass
	
	mode = state
	
	mapVisible = mapClass.GetVisible()
	if mode and mapVisible:
		mapClass.SetVisible(viz.ON)
	else:
		mapClass.SetVisible(viz.OFF,state=False)
	
MODE_PLAY  = 1
MODE_STOP  = 2
MODE_PAUSE = 3

selectMode	= MODE_STOP
roomClass   = None
itemClass   = None
pathData    = None
pathClass = None
collideTask = None

roomData  	  = []
itemData  	  = []
doorData  	  = []
lightData 	  = []
collideData   = []
CheckDoorTask = []
drewingData   = None
mapData		  = None

CameraHandleObject = None
def setCameraObject(camObj):
	global CameraHandleObject
	CameraHandleObject = camObj
		
def CreateMap():
	map=Map.MapClass()
	map.SetTarget(viz.MainView)
	iniFile = LoadIniFile.LoadIniFileClass()
	iniFile.LoadIniFile(viz.res.getPublishedPath('Settings\\RiMMInspector_Settings.ini'))
	height	= iniFile.GetItemData('Map','height')
	size	= iniFile.GetItemData('Map','size')
	pos		= iniFile.GetItemData('Map','position')
	mode	= iniFile.GetItemData('Map','mode')
	#マップの引きを設定
	try:	map.SetOffset([0,float(height),0])
	except:	map.SetOffset([0,10.0,0])
	#マップのサイズを設定
	try:	mapSize = map.SetSize([float(size.split(',')[0]),float(size.split(',')[1])])
	except:	mapSize = map.SetSize([0.24,0.32])
	#マップの位置を設定
	try:	map.SetPosition(pos)
	except:	map.SetPosition([0.7,1.0])
	#マップのモードを設定
	print 'mode',mode
	try:	map.SetMode(int(mode))
	except:	map.SetMode(0)
	return map
def SetNotRenderToMapWindow(list):
	global mapClass
	datalist=[]
	for data in list:
		try:
			if data: datalist.append(data.GetModel())
		except:
			datalist.append(data)
	mapClass.SetNotRenderToMapWindow(datalist)
def SetRenderToMapWindow(list):
	global mapClass
	datalist=[]
	for data in list:
		try:
			if data: datalist.append(data.GetModel())
		except:
			datalist.append(data)
	mapClass.SetRenderToMapWindow(datalist)
mapClass = CreateMap()

drewingFlag		 = True
roomFlag		 = False
equipmentFlag	 = False
lightFlag		 = False
collideFlag		 = False
callPauseFlag	 = False
collideStopFlag	 = False
pathVisibleFlag	 = True
autoDoorFlag	 = True
avatarCheckBoxListCount = 20
#grid = GridControlClass.GridControlClass()
grid = None
def SetGrid(bgData):
	global grid
	grid = bgData
	SetNotRenderToMapWindow([grid])

#自動ドア開閉の設定
def SetAutoDoorFlag():
	iniFile = LoadIniFile.LoadIniFileClass()
	iniFile.LoadIniFile(viz.res.getPublishedPath('Settings\\RiMMInspector_Settings.ini'))
	val = iniFile.GetItemData('PathSimulator_Setting','autoDoor')
	if val == '0':
		global autoDoorFlag
		autoDoorFlag = False
SetAutoDoorFlag()
		
#背景モデルの作成
def SetBGModel():
	sky=viz.add(viz.res.getPublishedPath("Resource\\Bg\\sky.osgb"))
	if sky:
		sky.setPosition(0,-10,0)
		sky.setScale([100,100,100])
		sky.disable(viz.LIGHTING)
	ground=vizshape.addPlane()
	if ground:
		ground.setPosition(0,-0.1,0)
		ground.color(0.3,0.3,0.3)
		ground.setScale(100,1,100)
		ground.disable(viz.LIGHTING)
	return [sky,ground]
#bg=SetBGModel()
bg = None
def SetBg(bgData):
	global bg
	bg = bgData
	SetNotRenderToMapWindow(bg)

#アイテム、間取り、パスデータの読み込み
def LoadData(pathList):
	
	global roomClass
	global itemClass
	global roomData
	global itemData
	global pathData
	global doorData
	global drewingData
	global collideData
	global mapData
	global lightData
	global Thread_1
	
	global isLoadingFlag
	global roomFlag
	global equipmentFlag
	global lightFlag
	global collideFlag
	global collideStopFlag
	global drewingFlag
	global threadAliveFlag
	
	roomFlag=True
	lightFlag=True
	collideFlag=False
	collideStopFlag=False
	selectAvatarFlag=False
	
	#データのリセット
	ResetData()
	
	#間取りデータの読み込み
	roomClass	= LoadRoomDataClass.LoadRoomDataClass(pathList[0])
	if roomClass.IsSuccessLoading():
		ResetData()
		Debug(' Loading Error -> Room Data')
		return
	
	doorData = roomClass.GetDoorModelList()
	roomData = roomClass.GetModelList()
	collideData = roomClass.GetCollisionModelList()
	
	SetNotRenderToMapWindow(roomData)
	SetNotRenderToMapWindow(collideData)
	
	SetCollideModel(doorData,True)
	SetCollideModel(collideData,True)
	SetCollideModel(roomData,False)
	
	#図面データの取得
	drewingData = roomClass.GetDrawingModel()
	#if drewingData:
	#	drewingData.drawOrder(10)
	if not(drewingFlag):
		if drewingData:
			#drewingData.disable(viz.RENDERING)
			drewingData.visible(viz.OFF)
	SetNotRenderToMapWindow([drewingData])
	
	if drewingData:
		scale = roomClass.GetDrawingScale()
		mapData = drewingData.copy()
		mapData.enable(viz.RENDERING)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setScale(scale)
		mapData.setPosition([0,2,0])
		mapData.alpha(1.0)
		SetRenderToMapWindow([mapData])
	else:
		mapData=vizshape.addPlane(size=[1000,1000])
		mapData.disable(viz.LIGHTING)
		mapData.color(viz.BLACK)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setPosition([0,2,0])
		SetRenderToMapWindow([mapData])
	
	#アイテムデータの読み込み
	itemClass	= LoadItemDataClass.LoadItemDataClass(pathList[1])
	if itemClass.IsSuccessLoading():
		ResetData()
		Debug(' Loading Error -> Item Data')
		return
	itemData	= itemClass.GetModelWithoutLightList()
	lightData	= itemClass.GetLightModelList()
	SetCollideModel(itemData,True)
	equipmentFlag=True
	
	SetNotRenderToMapWindow(itemData)
	SetNotRenderToMapWindow(lightData)
	
	#視点位置のセット
	startPos,startAng=roomClass.GetStartPosition()
	try:
		viz.MainView.setPosition(startPos)
		viz.MainView.setEuler(startAng)
		viz.MainView.move([0,0,10])
		CameraHandleObject.SetStartPos(viz.MainView.getPosition(),viz.MainView.getEuler())
	except:
		pass
	
	#パスデータの読み込み
	pathData = PathControlClass.PathControlClass(pathList[2])     #パス設定ファイルの読み込み
	if pathData.IsSuccessLoading():
		# commentout takahashi 2014/10/22
		#ResetData()
		#Debug(' Loading Error -> Path Data')
		#return
		avatarlist=[]
		pathlist=[]
	else:
		avatarlist=pathData.GetAvatarList()
		SetCollideModel(avatarlist,True)
		SetNotRenderToMapWindow(avatarlist)
		
		standAvatarlist=pathData.GetStandAvatarList()
		SetNotRenderToMapWindow(standAvatarlist)
		
		lineList=pathData.GetLineList()
		SetNotRenderToMapWindow(lineList)
	
	if autoDoorFlag:
		CheckDoor()

#アイテム、間取り、パスデータの読み込み
def LoadDataFromLayouter(data):
	
	global roomClass
	global itemClass
	global roomData
	global itemData
	global pathData
	global pathClass
	global doorData
	global drewingData
	global collideData
	global mapData
	global lightData
	global Thread_1
	
	global isLoadingFlag
	global roomFlag
	global equipmentFlag
	global lightFlag
	global collideFlag
	global collideStopFlag
	global drewingFlag
	global threadAliveFlag
	
	roomFlag=True
	lightFlag=True
	collideFlag=False
	collideStopFlag=False
	selectAvatarFlag=False
	
	#データのリセット
	#ResetData()
	
	#間取りデータの読み込み
	roomClass	= data[0]
	#if roomClass.IsSuccessLoading():
	#	ResetData()
	#	Debug(' Loading Error -> Room Data')
	#	return
	
	doorData = roomClass.GetDoorModelList()
	roomData = roomClass.GetModelList()
	collideData = roomClass.GetCollisionModelList()
	
	SetNotRenderToMapWindow(roomData)
	SetNotRenderToMapWindow(collideData)
	
	SetCollideModel(doorData,True)
	SetCollideModel(collideData,True)
	SetCollideModel(roomData,False)
	
	#図面データの取得
	drewingData = roomClass.GetDrawingModel()
	#if drewingData:
	#	drewingData.drawOrder(10)
	#if not(drewingFlag):
	#	if drewingData:
	#		drewingData.disable(viz.RENDERING)
	#		drewingData.visible(viz.OFF)
	SetNotRenderToMapWindow([drewingData])
	
	if drewingData:
		scale = roomClass.GetDrawingScale()
		mapData = drewingData.copy()
		mapData.enable(viz.RENDERING)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setScale(scale)
		mapData.setPosition([0,2,0])
		mapData.alpha(1.0)
		SetRenderToMapWindow([mapData])
	else:
		mapData=vizshape.addPlane(size=[1000,1000])
		mapData.disable(viz.LIGHTING)
		mapData.color(viz.BLACK)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setPosition([0,2,0])
		SetRenderToMapWindow([mapData])
	
	#アイテムデータの読み込み
	itemClass	= data[1]
	#if itemClass.IsSuccessLoading():
	#	ResetData()
	#	Debug(' Loading Error -> Item Data')
	#	return
	itemData	 = itemClass.GetModelWithoutLightList()
	lightData	 = itemClass.GetLightModelList()
	itemLineData = itemClass.GetLineModelList()
	SetCollideModel(itemData,True)
	equipmentFlag=True
	
	SetNotRenderToMapWindow(itemData)
	SetNotRenderToMapWindow(lightData)
	SetNotRenderToMapWindow(itemLineData)
	
	#視点位置のセット
	#startPos,startAng=roomClass.GetStartPosition()
	CameraHandleObject.SetViewPos(data[4])
	
	#パスデータの読み込み
	pathData = PathControlClassLayouter.PathControlClassLayouter(data[3])     #パス設定ファイルの読み込み
	pathClass = data[2]
	if pathData.IsSuccessLoading():
		# commentout takahashi 2014/10/22
		#ResetData()
		#Debug(' Loading Error -> Path Data')
		#return
		avatarlist=[]
		pathlist=[]
	else:
		avatarlist=pathData.GetAvatarList()
		SetCollideModel(avatarlist,True)
		SetNotRenderToMapWindow(avatarlist)
		
		standAvatarlist=pathData.GetStandAvatarList()
		SetNotRenderToMapWindow(standAvatarlist)
		
		lineList=pathData.GetLineList()
		SetNotRenderToMapWindow(lineList)
		
	if autoDoorFlag:
		CheckDoor()

#アイテム、間取り、パスデータの読み込み
def LoadDataInPathSim(pathList):
	
	global roomClass
	global itemClass
	global roomData
	global itemData
	global pathData
	global pathClass
	global doorData
	global drewingData
	global collideData
	global mapData
	global lightData
	global Thread_1
	
	global isLoadingFlag
	global roomFlag
	global equipmentFlag
	global lightFlag
	global collideFlag
	global collideStopFlag
	global drewingFlag
	global threadAliveFlag
	
	roomFlag=True
	lightFlag=True
	collideFlag=False
	collideStopFlag=False
	selectAvatarFlag=False
	
	#データのリセット
	ResetData2()
	
	#間取りデータの読み込み
	roomClass.Load(pathList[0])
	if roomClass.IsSuccessLoading():
		#ResetData()
		Debug(' Loading Error -> Room Data')
		return
	
	doorData = roomClass.GetDoorModelList()
	roomData = roomClass.GetModelList()
	collideData = roomClass.GetCollisionModelList()
	
	SetNotRenderToMapWindow(roomData)
	SetNotRenderToMapWindow(collideData)
	
	SetCollideModel(doorData,True)
	SetCollideModel(collideData,True)
	SetCollideModel(roomData,False)
	
	#図面データの取得
	drewingData = roomClass.GetDrawingModel()
	#if drewingData:
	#	drewingData.drawOrder(10)
	if not(drewingFlag):
		if drewingData:
			#drewingData.disable(viz.RENDERING)
			drewingData.visible(viz.OFF)
	SetNotRenderToMapWindow([drewingData])
	
	if drewingData:
		scale = roomClass.GetDrawingScale()
		mapData = drewingData.copy()
		mapData.enable(viz.RENDERING)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setScale(scale)
		mapData.setPosition([0,2,0])
		mapData.alpha(1.0)
		SetRenderToMapWindow([mapData])
	else:
		mapData=vizshape.addPlane(size=[1000,1000])
		mapData.disable(viz.LIGHTING)
		mapData.color(viz.BLACK)
		mapData.disable(viz.DEPTH_WRITE)
		mapData.drawOrder(16)
		mapData.setPosition([0,2,0])
		SetRenderToMapWindow([mapData])
	
	#アイテムデータの読み込み
	itemClass.Load(pathList[1])
	if itemClass.IsSuccessLoading():
		#ResetData()
		Debug(' Loading Error -> Item Data')
		return
	itemData	 = itemClass.GetModelWithoutLightList()
	lightData	 = itemClass.GetLightModelList()
	itemLineData = itemClass.GetLineModelList()
	SetCollideModel(itemData,True)
	
	#ダミーボックスのラインを表示
	itemClass.SetDummyBoxLineVisible(False)
	
	equipmentFlag=True
	
	SetNotRenderToMapWindow(itemData)
	SetNotRenderToMapWindow(lightData)
	SetNotRenderToMapWindow(itemLineData)
	
	#視点位置のセット
	startPos,startAng=roomClass.GetStartPosition()
	try:
		viz.MainView.setPosition(startPos)
		viz.MainView.setEuler(startAng)
		viz.MainView.move([0,0,10])
		CameraHandleObject.SetStartPos(viz.MainView.getPosition(),viz.MainView.getEuler())
	except:
		pass
	
	#パスデータの読み込み
	pathClass.Load(pathList[2])
	pathData = PathControlClass.PathControlClass(pathList[2])     #パス設定ファイルの読み込み
	if pathData.IsSuccessLoading():
		# commentout takahashi 2014/10/22
		#ResetData()
		#Debug(' Loading Error -> Path Data')
		#return
		avatarlist=[]
		pathlist=[]
	else:
		avatarlist=pathData.GetAvatarList()
		SetCollideModel(avatarlist,True)
		SetNotRenderToMapWindow(avatarlist)
		
		standAvatarlist=pathData.GetStandAvatarList()
		SetNotRenderToMapWindow(standAvatarlist)
		
		lineList=pathData.GetLineList()
		SetNotRenderToMapWindow(lineList)
	
	if autoDoorFlag:
		CheckDoor()

	itemClass.SetAllItemLineVisible(True)
	itemClass.SetAllItemLineColor(False)
	itemClass.CreateItemAnimationPath()
	
#アバター数に応じたチェックボックスリストの生成
def GetAvatarCheckBoxList():
	global pathData
	list=[False,False,False,False,False,False,False,False,False,False,
		  False,False,False,False,False,False,False,False,False,False]
	count = pathData.GetAvatarCount()
	if count > avatarCheckBoxListCount:
		count = avatarCheckBoxListCount
	if pathData:
		for index in range(count):
			list[index]=True
	return list

#アバター＆パスの表示制御
def SetVisibleAvatarModelAndPath(index,state):
	global pathData
	global pathVisibleFlag
	try:
		if pathData:
			pathData.SetAvatarVisible(index,state)
			if pathVisibleFlag:
				pathData.SetPathVisible(index,state)
	except:
		pass

#マウスでクリックしたキャラクター情報の取得
def CheckClickAvatar(model):
	global selectAvatarFlag
	global selectAvatarNum
	if pathData:
		for index,avatar in enumerate(pathData.GetAvatarList()):
			if model==avatar:
				if selectAvatarFlag==True:
					if selectAvatarNum==index:
						selectAvatarFlag=False
				else:
					selectAvatarFlag=True
					selectAvatarNum=index
				break

#モデルのコリジョン一括指定
def SetCollideModel(list,collide):
	if list != None:
		for data in list:
			if collide==True and data!=None:
				data.collideMesh()
				data.disable(viz.DYNAMICS)
			else:
				data.collideNone()
				
#モデルのコリジョン一括削除
def ResetCollideModel():
	global roomData
	global itemData
	global pathData
	global doorData
	global lightData
	global collideData
	
	avatarlist=pathData.GetAvatarList()
	
	SetCollideModel(roomData,False)
	SetCollideModel(itemData,False)
	SetCollideModel(avatarlist,False)
	SetCollideModel(doorData,False)
	SetCollideModel(lightData,False)
	SetCollideModel(collideData,False)

#読込データの一括削除
def ResetData(state=True):
	global roomClass
	global itemClass
	global roomData
	global itemData
	global drewingData
	global mapData
	global mapClass
	global pathData
	global pathClass
	global doorData
	global lightData
	global collideData
	global collideFlag
	#global collideTask
	#if collideTask:
	#	collideTask.kill()
	collideFlag=False
	ResetDoorTask()
	roomData	=[]
	itemData	=[]
	doorData	=[]
	lightData	=[]
	collideData	=[]
	roomClass	=None
	itemClass	=None
	pathData	=None
	pathClass = None
	if mapData: mapData.remove()
	if state:
		if drewingData: drewingData.remove()
		mapClass.SetTarget(viz.MainView)

#読込データの一括削除
def ResetData2():
	global roomClass
	global itemClass
	global roomData
	global itemData
	global drewingData
	global mapData
	global mapClass
	global pathData
	global doorData
	global lightData
	global collideData
	global collideFlag
	#global collideTask
	#if collideTask:
	#	collideTask.kill()
	collideFlag=False
	ResetDoorTask()
	roomData	=[]
	itemData	=[]
	doorData	=[]
	lightData	=[]
	collideData	=[]
	#roomClass	=None
	#itemClass	=None
	#pathData	=None
	#pathClass = None
	if mapData: mapData.remove()
	if drewingData: drewingData.remove()
	mapClass.SetTarget(viz.MainView)

#再生
def Play(clickList):
	global itemClass
	global pathData
	global selectMode
	global MODE_PLAY
	selectMode=MODE_PLAY
	if itemClass:
		itemClass.Play()
	if pathData:
		count=pathData.GetAvatarCount()
		if count > avatarCheckBoxListCount:
			count = avatarCheckBoxListCount
		for num in range(count):
			if clickList[num]:
				pathData.Play(num)

#一時停止
def Pause():
	global itemClass
	global pathData
	global selectMode
	global MODE_PAUSE
	selectMode=MODE_PAUSE
	if itemClass:
		itemClass.Pause()
	if pathData:
		count=pathData.GetAvatarCount()
		if count > avatarCheckBoxListCount:
			count = avatarCheckBoxListCount
		for num in range(count):
			pathData.Pause(num)

#停止
def Stop():
	global itemClass
	global pathData
	global selectMode
	global MODE_STOP
	selectMode=MODE_STOP
	if itemClass:
		itemClass.Reset()
	if pathData:
		count=pathData.GetAvatarCount()
		if count > avatarCheckBoxListCount:
			count = avatarCheckBoxListCount
		for num in range(count):
			pathData.Reset(num)

#終わり出し
def SetFinPos():
	global pathData
	global selectMode
	global MODE_STOP
	selectMode=MODE_STOP
	if itemClass:
		itemClass.End()
	if pathData:
		count=pathData.GetAvatarCount()
		for num in range(count):
			pathData.End(num)

#衝突判定フラグの反転
def SetCollideStop():
	global collideFlag
	global collideStopFlag
	collideStopFlag=not(collideStopFlag)
	return collideStopFlag,collideFlag

#衝突判定処理のON/OFF切り替え
def SetCollide(AvatarList):
	global collideTask
	global collideFlag
	global collideStopFlag
	global itemClass
	collideFlag=not(collideFlag)
	
	itemList = itemClass.GetItemWithPathList()
	
	if collideFlag:
		collideTask=viztask.schedule(CheckCollideFunction(AvatarList,itemList))
	else:
		ResetColorAllModel()#カラーのリセット
		if collideTask:
			collideTask.kill()
	return collideFlag,collideStopFlag

#衝突判定処理のON/OFF切り替え
checkAvatarList=[False,False,False,False,False,False,False,False,False,False,
				 False,False,False,False,False,False,False,False,False,False]
Thread_1=None
threadAliveFlag=False
'''
def SetCollide(AvatarList):
	global collideFlag
	global checkAvatarList
	global Thread_1
	collideFlag=not(collideFlag)
	if collideFlag:
		checkAvatarList=AvatarList
		Thread_1=Thread(target=CheckCollideFunction2)
		Thread_1.daemon = True
		Thread_1.start()
	else:
		ResetColorAllModel()#カラーのリセット
		Thread_1=None
	return collideFlag
'''
def StopCollide():
	global checkAvatarList
	global collideFlag
	collideFlag=False
	checkAvatarList=[False,False,False,False,False,False,False,False,False,False,
					 False,False,False,False,False,False,False,False,False,False]

#衝突判定用、デフォルトカラーへの一括変更
def ResetColorAllModel():
	global doorData
	global roomData
	global itemData
	global pathData
	
	for data in doorData:
		if data: data.color(1,1,1)
	#for data in roomData:
	#	if data: data.color(1,1,1)
	for data in itemData:
		if data:
			data.color(1,1,1)
			data.emissive(0,0,0)
			data.disable(viz.LIGHTING)
	if pathData:
		for data in pathData.GetAvatarList():
			if data: data.color(1,1,1)

#衝突判定処理（Loop処理）
def CheckCollideFunction(AvatarList,itemList=[]):
	global pathData
	global selectMode
	global collideStopFlag
	global callPauseFlag
	
	if not(pathData):
		return
	
	avatarList=pathData.GetAvatarList()
	preAvatarList=[]
	
	while True:
		if selectMode!=MODE_PAUSE:
			
			#元のデータの色をリセット
			for data in preAvatarList:
				if data:
					data.color(1,1,1)
					data.emissive(0,0,0)
					data.disable(viz.LIGHTING)
			preAvatarList=[]
			
			#アバターの衝突判定
			for index,avatar in enumerate(avatarList):
				if index > avatarCheckBoxListCount-1:
					break
				
				elif AvatarList[index]==True:
					
					avatar.collideNone() #衝突判定メッシュの更新
					avatar.collideBox()
					
					intersectNode=viz.phys.intersectNode(avatar)
					
					collideModel=False
					for data in intersectNode:
						isavatar=False
						for avatar2 in avatarList:
							if data==avatar2:
								isavatar=True
						for avatar2 in itemList:
							if data==avatar2:
								isavatar=True
						if not(isavatar):
							collideModel=True
							break
					
					if collideModel:
						
						avatar.collideNone() #衝突判定メッシュの更新
						avatar.collideMesh()
						
						intersectNode=viz.phys.intersectNode(avatar)
						
						for data in intersectNode:
						
							#アバター同士の接触判定はなし
							isavatar=False
							for avatar2 in avatarList:
								if data==avatar2:
									isavatar=True
							for avatar2 in itemList:
								if data==avatar2:
									isavatar=True
							
							#衝突したモデルに色を付ける
							if not(isavatar):
								data.color(1,0,0)
								data.emissive(1,0,0)
								data.enable(viz.LIGHTING)
								preAvatarList.append(data)
								
								if collideStopFlag:
									callPauseFlag=True
			
			#アイテムの衝突判定
			for item in itemList:
				
				item.collideNone() #衝突判定メッシュの更新
				item.collideBox()
				
				intersectNode=viz.phys.intersectNode(item)
				
				item.collideNone() #衝突判定メッシュの更新
				item.collideMesh()
				
				for data in intersectNode:
					#アバター同士の接触判定はなし
					isavatar=False
					for avatar2 in avatarList:
						if data==avatar2:
							isavatar=True
					for avatar2 in itemList:
						if data==avatar2:
							isavatar=True
					
					#衝突したモデルに色を付ける
					if not(isavatar):
						data.color(1,0,0)
						data.emissive(1,0,0)
						data.enable(viz.LIGHTING)
						preAvatarList.append(data)
						
						if collideStopFlag:
							callPauseFlag=True
							
		yield viztask.waitFrame(1)

#衝突判定処理（Thread処理）
'''
def CheckCollideFunction2():
	global pathData
	global selectMode
	global collideStopFlag
	global collideFlag
	global callPauseFlag
	global checkAvatarList
	global threadAliveFlag
	
	start=0
	threadAliveFlag=True
	avatarList=checkAvatarList
	preAvatarList=[]
	
	while collideFlag and pathData:
		
		if start==0:
			avatarList=pathData.GetAvatarList()
			preAvatarList=[]
			start=1
		
		if selectMode!=MODE_PAUSE:
			
			#元のデータの色をリセット
			if callPauseFlag==True:
				for data in preAvatarList:
					if data: data.color(1,1,1)
				preAvatarList=[]
			
			#アバターの衝突判定
			for index,avatar in enumerate(avatarList):
				
				if checkAvatarList[index]==True:
					
					intersectNode=[]
					
					if avatar:
						avatar.collideNone() #衝突判定メッシュの更新
						avatar.collideMesh()
						intersectNode=viz.phys.intersectNode(avatar)
					
					for data in intersectNode:
						
						#アバター同士の接触判定はなし
						isavatar=False
						for avatar2 in avatarList:
							if data and avatar2:
								if data==avatar2:
									isavatar=True
						
						#衝突したモデルに色を付ける
						if not(isavatar):
							if data:
								data.color(1,0,0)
								preAvatarList.append(data)
							
							if collideStopFlag:
								callPauseFlag=True
				else:
					if avatar:
						avatar.collideNone() #衝突判定メッシュの更新
	
	threadAliveFlag=False
'''

#一時停止フラグの呼び出し
def CallPause():
	global callPauseFlag
	if callPauseFlag:
		callPauseFlag=False
		return True
	return False

#グリッドの表示切り替え
def SwitchGrid():
	global grid
	visible=grid.GetGridVisible()
	grid.SetGridVisible(not(visible))	#反転
	return not(visible)

#パスの表示切り替え
def SwitchPath():
	global itemClass
	global pathData
	global pathVisibleFlag
	
	pathVisibleFlag=not(pathVisibleFlag)
	
	if itemClass != None:
		itemClass.ChangeLineVisible(pathVisibleFlag)
		
	state = False
	if pathData != None:
		count=pathData.GetAvatarCount()
		if count > avatarCheckBoxListCount:
			count = avatarCheckBoxListCount
		try:
			for x in range(count):
				#表示状態を取得
				if pathData.GetAvatarVisible(x):
					pathData.SetPathVisible(x,pathVisibleFlag)
			state = True
		except:
			pass
			
	"""
	pathVisibleFlag=False
	
	state = False
	if pathData != None:
		count=pathData.GetAvatarCount()
		if count > avatarCheckBoxListCount:
			count = avatarCheckBoxListCount
		try:
			visible=pathData.GetPathVisible(0)
			for x in range(count):
				#表示状態を取得
				pathData.SetPathVisible(x,not(visible)) #反転
			pathVisibleFlag=not(visible)
			state = True
		except:
			pass
			
	if itemClass != None:
		pathVisibleFlag=itemClass.ChangeLineVisible(state,pathVisibleFlag)
	"""
	return pathVisibleFlag

#パスの表示切り替え
def SwitchAvatarVisible(index,isVisible):
	global pathData
	pathData.SetAvatarVisible(index,isVisible)

#アイテムの表示切り替え
def SwitchEquipment():
	global equipmentFlag
	global itemData
	global itemClass
	equipmentFlag=not(equipmentFlag)
	
	for data in itemData:
		if data:
			if equipmentFlag:
				#data.enable(viz.RENDERING)
				data.visible(viz.ON)
			else:
				#data.disable(viz.RENDERING)
				data.visible(viz.OFF)
	
	itemClass.SetAllItemLineVisible(pathVisibleFlag)
	
	return equipmentFlag

#間取りモデルの表示切り替え
def SwitchBuilding():
	global roomFlag
	global roomData
	roomFlag=not(roomFlag)
	
	for data in roomData:
		if data:
			if roomFlag:
				data.enable(viz.RENDERING)
			else:
				data.disable(viz.RENDERING)
	
	return roomFlag

#照明モデルの表示切り替え
def SwitchLight():
	global lightData
	global lightFlag
	global itemClass
	lightFlag=not(lightFlag)
	for data in lightData:
		if data:
			if lightFlag:
				#data.enable(viz.RENDERING)
				data.visible(viz.ON)
			else:
				#data.disable(viz.RENDERING)
				data.visible(viz.OFF)
	
	itemClass.SetAllItemLineVisible(pathVisibleFlag)
	
	return lightFlag

def SwitchMap():
	global mapClass
	
	flag=mapClass.GetVisible()
	mapClass.SetVisible(not(flag))
	
	return mapClass.GetVisible()

def SwitchDrawing():
	global drewingData
	global drewingFlag
	
	drewingFlag=not(drewingFlag)
	
	if drewingData:
		if drewingFlag:
			#drewingData.enable(viz.RENDERING)
			drewingData.visible(viz.ON)
		else:
			#drewingData.disable(viz.RENDERING)
			drewingData.visible(viz.OFF)
	
	return drewingFlag

#ドアの開閉処理
def CheckDoor():
	global CheckDoorTask
	global doorData	
	for door in doorData:
		CheckDoorTask.append(viztask.schedule(_CheckDoor(door)))
def _CheckDoor(door):
	
	global pathData
	
	if not(door):
		return
	
	doorState = False
	doorThreshold = 1.0
	avatarList=pathData.GetAvatarList()
	
	while True:
		
		distance = 10
		doorPos	 = door.getPosition()
		
		for avatar in avatarList:
			
			viewPos	 = avatar.getPosition()
			_distance = vizmat.Distance(viewPos,doorPos)
			if _distance<distance:
				distance=_distance
		
		viewPos	 = viz.MainView.getPosition()
		if viewPos[1] > 1.0:
			viewPos[1] = viewPos[1] - 1.5
		_distance = vizmat.Distance(viewPos,doorPos)
		if _distance<distance:
			distance=_distance
				
		if doorState==False and distance<doorThreshold:#近づいたら開ける
			OpenAndCloseDoor(door,0)
			doorState = True
			
		elif doorState==True and distance>doorThreshold:#離れたら閉める
			OpenAndCloseDoor(door,1)
			doorState = False
		
		yield viztask.waitFrame(1)
def OpenAndCloseDoor(obj,state): 
	if state == 0:#開ける
		obj.disable(viz.ANIMATIONS)
		obj.setAnimationFrame(0)
		obj.setAnimationSpeed(1)
		obj.setAnimationLoopMode(0)
		obj.enable(viz.ANIMATIONS)
	if state == 1:#閉める
		obj.disable(viz.ANIMATIONS)
		obj.setAnimationFrame(30)
		obj.setAnimationSpeed(-1)
		obj.setAnimationLoopMode(0)
		obj.enable(viz.ANIMATIONS)
def ResetDoorTask():
	global CheckDoorTask
	global doorData	
		
	for task in CheckDoorTask:
		if task:
			task.kill()

	for obj in doorData:
		obj.disable(viz.ANIMATIONS)
		obj.setAnimationFrame(0)
		obj.setAnimationSpeed(1)
		obj.setAnimationLoopMode(0)
		
def SetAvatarModelCamera(index):
	global pathData
	global mapClass
	nextView=viz.MainView
	if index<0:
		
		#視点位置のセット
		avatarView = viz.MainWindow.getView()
		if avatarView != viz.MainView:
			viz.MainView.setPosition(avatarView.getPosition(viz.ABS_GLOBAL))
			viz.MainView.setEuler(avatarView.getEuler(viz.ABS_GLOBAL))
			viz.MainView.move([0,0,10])
			CameraHandleObject.SetStartPos(viz.MainView.getPosition(),viz.MainView.getEuler())
		
		nextView=viz.MainView
	else:
		nextView=pathData.GetEyeViewCamera(index)
	viz.MainWindow.setView(nextView)
	mapClass.SetTarget(nextView)

def SetAvatarEyeHeight(height):
	global pathData
	if pathData:
		pathData.SetEyeHeight(height)

def Debug(message):
	OutputDebugString('[STUDIOWA][PATH-SIM][LAYOUT]'+str(message))

#編集中のデータ保存を促す処理
def SaveRemind():
	global roomClass
	global itemClass
	global pathClass

	roomCount = roomClass.GetRoomCount()
	itemCount = itemClass.GetItemCount()
	characterCount = pathClass.GetCharacterCount()
	
	#テキストの作成
	questionText = ''
	if roomCount != 0 and itemCount == 0 and characterCount == 0:
		questionText = '間取りの状態を保存しますか？'
	elif roomCount == 0 and itemCount != 0 and characterCount == 0:
		questionText = 'アイテムの状態を保存しますか？'
	elif roomCount != 0 and itemCount != 0 and characterCount == 0:
		questionText = '間取り、アイテムの状態を保存しますか？'
	elif roomCount == 0 and itemCount == 0 and characterCount != 0:
		questionText = 'パスの状態を保存しますか？'
	elif roomCount != 0 and itemCount == 0 and characterCount != 0:
		questionText = '間取り、パスの状態を保存しますか？'
	elif roomCount == 0 and itemCount != 0 and characterCount != 0:
		questionText = 'アイテム、パスの状態を保存しますか？'
	elif roomCount != 0 and itemCount != 0 and characterCount != 0:
		questionText = '間取り、アイテム、パスの状態を保存しますか？'
		
	if questionText != '':
		answer = vizinput.ask(questionText,title='PathSimulator')
		
		if answer == 1:		#「はい」を選択したらファイル名を付けて保存
			if roomCount != 0:
				SaveRoom()
			if itemCount != 0:
				SaveItem()
			if characterCount != 0:
				SavePath()
				
#部屋を保存
def SaveRoom():
	global roomClass
	
	directory = GetRootPath() + 'Settings\\MedicalSim\\Room\\.'
	filePath = vizinput.fileSave(file=directory)
	
	if filePath != "":
		if filePath.endswith('.ini') == False:
			filePath = filePath + '.ini'

		roomClass.Save(filePath)
		
#アイテムを保存
def SaveItem():
	global itemClass
	
	directory = GetRootPath() + 'Settings\\MedicalSim\\Item\\.'
	filePath = vizinput.fileSave(file=directory)
	
	if filePath != "":
		if filePath.endswith('.ini') == False:
			filePath = filePath + '.ini'

		itemClass.Save(filePath)
	
#ログを保存
def SavePath():
	global pathClass
	
	if pathClass.GetCharacterCount() == 0:
		vizinput.message('パスが作成されていません', title='PathSimulator')
		return
		
	directory = GetRootPath() + 'Settings\\MedicalSim\\Log\\.'
	filePath = vizinput.fileSave(file=directory)
	
	if filePath != "":
		if filePath.endswith('.ini') == False:
			filePath = filePath + '.ini'

		pathClass.Save(filePath)
	
#ルートフォルダの取得
def GetRootPath():
	exeDir = viz.res.getPublishedPath('')
	if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
		exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
	rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
		
	return rootDir
	