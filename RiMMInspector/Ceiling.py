﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizmat

import Object3d

from os import path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#天井制御のクラス定義
class Ceiling(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,textureName,textureSize,height):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Ceiling] : Init Ceiling')
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._TextureFile = textureName
		self._TextureSize = textureSize
		self._Height = height
		
		self._Model = None
		self._Texture = None
		self._TextureName = textureName
		
		self.Create()
		self.SetVisible(False)
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Ceiling] : Start Ceiling Ceiling')
		pass
		
	#天井モデルの作成
	def Create(self):
		self._Texture = self.LoadTexture(self._TextureFile)
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)

		self._Model = None
		
		#板ポリゴン作成
		viz.startLayer(viz.TRIANGLE_FAN)
		for position in self._PointList: 
			viz.texCoord(0.0,0.0)
			viz.vertex(position[0],0,position[1])
		model = viz.endLayer()
		
		model.drawOrder(-50)
		
		#model.enable(viz.LIGHTING)
		model.color(viz.WHITE)
		model.emissive(viz.GRAY)
		model.ambient(viz.GRAY)
		
		model.texture(self._Texture)
			
		self._Model = model
		
		self.SetPickable(False)
		self.SetCeiling()
		
	#天井モデルの削除
	def Delete(self):
		self._Model.remove()	#モデルを削除
		del self._Model
				
		self._Model = None
		
	#天井モデルの配置
	def SetCeiling(self):
		self._Model.setPosition([0.0,0.0,0.0])
		
		matrix = vizmat.Transform()
		matrix.setPosition([0.0,0.0,0.0])
		matrix.setScale([1/self._TextureSize,1/self._TextureSize,1])
		
		for x in range(len(self._PointList)):
			position = self._PointList[x]
			self._Model.setVertex(x,[position[0],0,position[1]],viz.ABS_GLOBAL)
			self._Model.setTexCoord(x,position[0],position[1])
			
		#サイズによって少しずらす（ちらつき防止）
		box = self._Model.getBoundingBox()
		scale = box.getSize()
		heightOffset = scale[0]*scale[2]*0.000005
		
		ceilingPosition = [0,0,0]
		ceilingPosition[0] = self._FloorPosition[0]
		ceilingPosition[1] = self._Height - 0.002 + heightOffset
		ceilingPosition[2] = self._FloorPosition[2]
		
		self._Model.setPosition(ceilingPosition)
		self._Model.texmat(matrix)

	#テクスチャ画像の読み込み
	def LoadTexture(self,textureName):
		filePath = textureName
		"""
		if path.exists(textureName)!=True:		#絶対パスかどうか
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		"""
		if textureName[:2] == '..':
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		
		texture = viz.addTexture(filePath)
		
		return texture

	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
				
	#表示状態を設定
	def SetVisible(self,state):
		if state == True:
			self._Model.visible(viz.ON)
		else:
			self._Model.visible(viz.OFF)
			
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
	
		self.SetCeiling()
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.SetCeiling()
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#テクスチャ設定
	def SetTexture(self,texture):
		self._Texture = texture
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		self._Model.texture(self._Texture)
		
	#テクスチャ取得
	def GetTexture(self):
		texture = self._Texture
		return texture
		
	#テクスチャ名取得
	def GetTextureName(self):
		textureName = self._TextureName
		return textureName
		
	#壁のポイントを再設定
	def SetPointList(self,pointList):
		self._PointList = pointList
		
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.SetCeiling()
		