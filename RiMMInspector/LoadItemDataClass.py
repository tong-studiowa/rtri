﻿# coding: utf-8
#PathSimulator

import viz
import vizinput
import vizfx
import LoadIniFile
from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = ''

# - - - - - - - - - - - - - - - - - - - - - - - - -
#間取り読み込み用クラス定義
class LoadItemDataClass():
	#コンストラクタ
	def __init__(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][LoadItemDataClass] : Init LoadItemDataClass')
		
		self._ItemControlClass = ItemControl()
		self._ItemControlClass.Load(SETTING_FOLDER_PATH + fileName)
	
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][LoadItemDataClass] : del LoadItemDataClass')
		
		modelCount = self._ItemControlClass.GetItemCount()
		for x in range(modelCount):
			self._ItemControlClass.Delete(0)
			
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadItemDataClass] : Start LoadItemDataClass Update')
		pass
		
	#アイテムモデルのリストを取得
	def GetModelList(self):
		modelList = self._ItemControlClass.GetModelList()
		return modelList
	
	#読み込みに成功したかどうかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._ItemControlClass.IsSuccessLoading()
	
	#照明モデルのリストを取得
	def GetLightModelList(self):
		modelList = self._ItemControlClass.GetLightModelList()
		return modelList
		
	#照明以外のモデルのリストを取得
	def GetModelWithoutLightList(self):
		modelList = self._ItemControlClass.GetModelWithoutLightList()
		return modelList
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Interface:
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass

# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Object3d(Interface):
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム制御のクラス定義
class ItemControl(Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Init ItemControl')
		
		self._ItemIniFileName = ""
		self._ItemIniFile = None
		self._ItemIniDataCount = 0
		self._ItemIniDataList = []
		
		self._SaveIniFile = None
		self._LoadErrorFlag=False	#takahashi 2014/8/1
		
		self._ModelList = []
		self._ModelNameList = []
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Start ItemControl Update')
		pass
		
	#INIファイルを基にアイテムを読み込む
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Load')
		
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		total = iniFile.GetItemData('Items','Total')
		
		if total == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはアイテム定義ファイルではありません。', title='RiMMInspector')
			self._LoadErrorFlag=True	#takahashi 2014/8/1
		
		else:
			self._ItemIniFileName = fileName
			
			#読み込み用iniファイルの作成
			self._ItemIniFile = iniFile
			
			#情報をリストに設定
			total = self._ItemIniFile.GetItemData('Items','Total')
			self._ItemIniDataCount = int(total)
			
			self._ItemIniDataList = []
			for x in range(self._ItemIniDataCount):
				
				#name
				name = self._ItemIniFile.GetItemData('Items',str(x))
				
				#model
				model = self._ItemIniFile.GetItemData(name,'model')
				
				#collision
				collisionTemp = self._ItemIniFile.GetItemData(name,'collision')
				collision = [0,0,0,1,1,1]
				collision[0] = float(collisionTemp.split(',')[0])
				collision[1] = float(collisionTemp.split(',')[1])
				collision[2] = float(collisionTemp.split(',')[2])
				collision[3] = float(collisionTemp.split(',')[3])
				collision[4] = float(collisionTemp.split(',')[4])
				collision[5] = float(collisionTemp.split(',')[5])
				
				#position
				positionTemp = self._ItemIniFile.GetItemData(name,'position')
				position = [0,0,0]
				position[0] = float(positionTemp.split(',')[0])
				position[1] = float(positionTemp.split(',')[1])
				position[2] = float(positionTemp.split(',')[2])
				
				#angle
				angleTemp = self._ItemIniFile.GetItemData(name,'angle')
				angle = [0,0,0,0]
				angle[0] = float(angleTemp.split(',')[0])
				angle[1] = float(angleTemp.split(',')[1])
				angle[2] = float(angleTemp.split(',')[2])
				angle[3] = float(angleTemp.split(',')[3])
				
				#animation
				animationTemp = self._ItemIniFile.GetItemData(name,'animation')
				animation = ""
				if animationTemp != None:
					animation = animationTemp
					
				#in
				pInTemp = self._ItemIniFile.GetItemData(name,'in')
				pIn = ""
				if pInTemp != None:
					pIn = pInTemp
				
				#out
				pOutTemp = self._ItemIniFile.GetItemData(name,'out')
				pOut = ""
				if pOutTemp != None:
					pOut = pOutTemp
				
				#Motion
				motionTemp = self._ItemIniFile.GetItemData(name,'motion')
				motion = ""
				if motionTemp != None:
					motion = motionTemp
				
				#target
				targetTemp = self._ItemIniFile.GetItemData(name,'target')
				target = 0
				if targetTemp != None:
					target = int(targetTemp)
				
				#size
				sizeTemp = self._ItemIniFile.GetItemData(name,'size')
				size = [0.0,0.0,0.0]
				if sizeTemp != None:
					size[0] = float(sizeTemp.split(',')[0])
					size[1] = float(sizeTemp.split(',')[1])
					size[2] = float(sizeTemp.split(',')[2])
				
				#texture
				textureList = []
				for x in range(100):
					textureName = "Texture"+str(x)
					textureTemp = self._ItemIniFile.GetItemData(name,textureName)
					if textureTemp != None:
						nodeName = textureTemp.split(',')[0]
						textureFile = textureTemp.split(',')[1]
						texture = [nodeName,textureFile]
						textureList.append(texture)
					else:
						break
				
				#モデルを読み込み、配置
				modelNum = self.Add(model)
				self.SetPosition(modelNum,position)
				self.SetAxisAngle(modelNum,angle)
				if model[-13:] == 'DummyBox.osgb': 		#ダミーボックスのみサイズ設定を反映
					self.SetSize(modelNum,size)
				#for texture in textureList:
				#	nodeName = texture[0]
				#	textureFile = texture[1]
				#	self.SetTexture(modelNum,nodeName,textureFile)
				
				item = self.GetModel(modelNum)
				
				#情報をリストに追加
				data = [name,model,collision,position,angle,animation,pIn,pOut,motion,target,modelNum,item]
				self._ItemIniDataList.append(data)
				
	#INIファイルにアイテムを保存する
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Save')
		
		#保存用iniファイルの作成
		self._SaveIniFile = LoadIniFile.LoadIniFileClass()
		self._SaveIniFile.ResetIniData()
		
		self._SaveIniFile.AddSection('Items')
		self._SaveIniFile.AddItem('Items','Total',str(self._ItemIniDataCount))
		
		for x in range(self._ItemIniDataCount):
			data = self._ItemIniDataList[x]
			
			self._SaveIniFile.AddItem('Items',str(x),data[0])
			
			#各モデルの情報を追加
			modelNum = data[10]
			item = data[11]
			
			#name
			self._SaveIniFile.AddSection(data[0])
			
			#model
			self._SaveIniFile.AddItem(data[0],'model',data[1])
			
			#collision
			collisionTemp = data[2]
			collision = str(collisionTemp[0])
			for x in range(5):
				collision = collision + ',' + str(collisionTemp[x+1])
			self._SaveIniFile.AddItem(data[0],'collision',collision)
			
			#position
			positionTemp = item.GetPosition()
			position = str(positionTemp[0])
			for x in range(2):
				position = position + ',' + str(positionTemp[x+1])
			self._SaveIniFile.AddItem(data[0],'position',position)
			
			#angle
			angleTemp = item.GetAxisAngle()
			angle = str(angleTemp[0])
			for x in range(3):
				angle = angle + ',' + str(angleTemp[x+1])
			self._SaveIniFile.AddItem(data[0],'angle',angle)
			
			#animation
			self._SaveIniFile.AddItem(data[0],'animation',data[5])
		
			#in
			self._SaveIniFile.AddItem(data[0],'in',data[6])
			
			#out
			self._SaveIniFile.AddItem(data[0],'out',data[7])
			
			#motion
			self._SaveIniFile.AddItem(data[0],'motion',data[8])
			
			#target
			self._SaveIniFile.AddItem(data[0],'target',str(data[9]))
			
			#size
			sizeTemp = item.GetSize()
			size = str(sizeTemp[0])
			for x in range(2):
				size = size + ',' + str(sizeTemp[x+1])
			self._SaveIniFile.AddItem(data[0],'size',size)
			
			#texture
			textureList = item.GetTextureList()
			for x in range(len(textureList)):
				textureName = 'texture' + str(x)
				textureTemp = textureList[x]
				texture = str(textureTemp[0]) + ',' + str(textureTemp[1])
				self._SaveIniFile.AddItem(data[0],textureName,texture)
			
		self._SaveIniFile.WriteIniData(fileName)
	
	#読込みに成功したかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._LoadErrorFlag
	
	#追加
	def Add(self,fileName):
		model = Item()
		model.Add(fileName)	#モデルを追加
		
		self._ModelList.append(model)
		self._ModelNameList.append(fileName)
		
		num = len(self._ModelList) - 1
		
		model.SetId(num)
		
		return num
	
	#新規モデルをリストに追加
	def AddNewItemToList(self,modelNum):
		item = self.GetModel(modelNum)
		
		name = 'NewItem' + str(modelNum)
		model = self._ModelNameList[modelNum]
		collision = [0,0,0,1,1,1]
		position = item.GetPosition()
		angle = item.GetAxisAngle()
		animation = ""
		pIn = ""
		pOut = ""
		motion = ""
		target = 0
		
		#情報をリストに追加
		data = [name,model,collision,position,angle,animation,pIn,pOut,motion,target,modelNum,item]
		self._ItemIniDataList.append(data)
		self._ItemIniDataCount = len(self._ItemIniDataList)
		
	#削除
	def Delete(self,num):
		deleteModel = self._ModelList[num]
		
		#モデルリストを更新
		newModelLiset = []
		newModelNameList = []
		
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			if model != deleteModel:
				fileName = self._ModelNameList[x]
				
				newModelLiset.append(model)
				newModelNameList.append(fileName)
						
		self._ModelList = newModelLiset
		self._ModelNameList = newModelNameList
		
		#データリストを更新
		newItemIniDataList = []
		changeRowState = False
		for x in range(len(self._ItemIniDataList)):
			data = self._ItemIniDataList[x]
			
			if data[11] == deleteModel:
				changeRowState = True
			else:
				if changeRowState == True:
					data[10] = data[10] - 1
					
					if data[0].startswith('NewItem'):
						name = data[0]
						num = int(name[7:])
						newName = 'NewItem' + str(num-1)
						data[0] = newName
						
				newItemIniDataList.append(data)
				
		self._ItemIniDataList = newItemIniDataList
		self._ItemIniDataCount = len(self._ItemIniDataList)
		
		#モデルを削除
		if deleteModel != None:
			deleteModel.Delete()
			del deleteModel
		
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
		
	#コピー
	def Copy(self,num):
		model = self._ModelList[num]
		modelName = self._ModelNameList[num]
		
		#追加
		modelNum = self.Add(modelName)
		
		#少しずらして配置
		originalPosition = model.GetPosition()
		position = [0,0,0]
		position[0] = originalPosition[0] + 0.5
		position[1] = originalPosition[1]
		position[2] = originalPosition[2] + 0.5
		
		self.SetPosition(modelNum,position)
		
		#角度は同じ
		originalOrientation = model.GetOrientation()
		self.SetOrientation(modelNum,originalOrientation)
		
		self.AddNewItemToList(modelNum)
		
	#全モデル削除
	def DeleteAll(self):
		for model in self._ModelList:
			model.remove()	#モデルを削除
		
		self._ModelList = []
		self._ModelNameList = []
		
	#位置設定
	def SetPosition(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPosition(position)
	
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
	
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#角度設定(INIファイル用)
	def SetAxisAngle(self,num,axisAngle):
		model = self._ModelList[num]
		
		if model != None and axisAngle != None:	
			model.SetAxisAngle(axisAngle)
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self,num):
		model = self._ModelList[num]
		axisAngle = [0,0,0,0]
		
		if model != None:
			axisAngle = model.GetAxisAngle()
		
		return axisAngle
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model:
				num =  x
		
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for model in self._ModelList:
			model.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#サイズ設定
	def SetSize(self,num,size):
		model = self._ModelList[num]
		if model != None:
			model.SetSize(size)
		
	#サイズ取得
	def GetSize(self,num):
		model = self._ModelList[num]
		size = [1,1,1]
		
		if model != None:
			size = model.GetSize()
		
		return size
		
	#テクスチャ設定
	def SetTexture(self,num,node,filePath):
		model = self._ModelList[num]
		if model != None:
			model.SetTexture(node,filePath)
		
	#テクスチャ取得
	def GetTexture(self,num,node):
		model = self._ModelList[num]
		texture = None
		
		if model != None:
			texture = model.GetTexture(node)
		
		return texture
		
	#アイテム数を取得
	def GetItemCount(self):
		count = len(self._ModelList)
		return count
		
	#モデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			itemModel = item.GetModel()
			modelList.append(itemModel)
		
		return modelList
		
	#照明モデルのリストを取得
	def GetLightModelList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			fileName = item.GetFileName()
			if fileName[-15:-10] == 'light':	#ライトモデルかどうかファイル名で判定
				itemModel = item.GetModel()
				modelList.append(itemModel)
				
		return modelList
		
	#照明以外のモデルのリストを取得
	def GetModelWithoutLightList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			fileName = item.GetFileName()
			if fileName[-15:-10] != 'light':	#ライトモデルかどうかファイル名で判定
				itemModel = item.GetModel()
				modelList.append(itemModel)
				
		return modelList
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム制御のクラス定義
class Item(Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Init Item')
		
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Texture = None
		self._TextureList = []
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		self._Size = [1.0,1.0,1.0]
		self._SizeOriginal = [1.0,1.0,1.0]
		
		self._Id = None
		self._FileName = None
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Start Item Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = None
		if path.exists(fileName) == True:		#絶対パスかどうか
			filePath = fileName
		else:
			filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
			
		model = vizfx.addChild(filePath)	#モデルを追加
		#model = viz.addChild(filePath)
		
		self._Model = model
		self._Model.color([1,1,1])
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Texture = self._Model.getTexture()
		self._TextureList = []
		
		#半透明モデルのオーダーを設定
		nodeNameList = self._Model.getNodeNames()
		for name in nodeNameList:
			if name[-2:] == '_A':
				self._Model.drawOrder(10,name)
			if name == 'sky':
				self._Model.drawOrder(-300)
		
		"""
		#ミップマップをOFF
		nodeNameList = model.getNodeNames()
		for nodeName in nodeNameList:
			texture = model.getTexture(node=nodeName)
			if texture != None:
				texture.filter(viz.MIN_FILTER,viz.NEAREST)
				texture.filter(viz.MAG_FILTER,viz.NEAREST)
		"""
		
		#サイズ設定
		box = self._Model.getBoundingBox()
		modelSize = box.size
		self._Size = modelSize
		self._SizeOriginal = self._Size
		
		self.SetPickable(False)
		
		self._FileName = filePath
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			position = iPosition
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = position[1] + self._PositionOffset[1]
			position[2] = position[2] + self._PositionOffset[2]
			
			self._Model.setPosition(position)
			self._Position = position
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#角度設定(INIファイル用)
	def SetAxisAngle(self,axisAngle):
		if self._Model != None and axisAngle != None:
			self._Model.setAxisAngle(axisAngle)
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self):
		axisAngle = self._Model.getAxisAngle(viz.ABS_GLOBAL)
		return axisAngle
	
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
		
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#サイズ設定
	def SetSize(self,size):
		if size[0] == 0.0 and size[1] == 0.0 and  size[2] == 0.0:
			pass
		else:
			self._Size = size
			
			newSize = [1.0,1.0,1.0]
			if self._SizeOriginal[0] != 0:
				newSize[0] = self._Size[0] / self._SizeOriginal[0]
			if self._SizeOriginal[1] != 0:
				newSize[1] = self._Size[1] / self._SizeOriginal[1]
			if self._SizeOriginal[2] != 0:
				newSize[2] = self._Size[2] / self._SizeOriginal[2]
			
			self._Model.setScale(newSize)
		
	#サイズ取得
	def GetSize(self):
		size = self._Size
		
		return size
		
	#テクスチャ設定
	def SetTexture(self,node,filePath):
		newTexture = viz.addTexture(filePath)
		if newTexture != None:
			self._Model.texture(newTexture,node=node)
			
			texture = [node,filePath]
			self._TextureList.append(texture)
		
	#テクスチャ取得
	def GetTexture(self,node):
		texture = self._Model.getTexture(node=node)
		
		return texture
		
	#テクスチャリスト取得
	def GetTextureList(self):
		textureList = self._TextureList
		
		return textureList
		
	#モデル名を取得
	def GetFileName(self):
		fileName = self._FileName
		
		return fileName
		