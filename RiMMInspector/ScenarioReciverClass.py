﻿# ScenarioReciverClass
# 担当			： 高橋
# 最終更新日	： 2013.1.21

import viz
import time
from ctypes import *
from win32api import OutputDebugString

#=========================================================
# コンテンツプレイヤーの使用可能なキーコード
#	0x80000000~0x800FFFFF
#=========================================================
class KeyCode():
	CONTENT_3D_PLAY			= 0x80000001	#3Dコンテンツ再生開始時
	CONTENT_3D_FINISH		= 0x80000002	#3Dコンテンツ再生終了時
	CONTENT_2D_PLAY			= 0x80010001	#2Dコンテンツ再生開始時
	CONTENT_2D_FINISH		= 0x80010002	#2Dコンテンツ再生終了時
	QUEST_COUNTDOWN_START	= 0x80020001	#質問文カウントダウン開始時
	QUEST_COUNTDOWN_FINISH	= 0x80020002	#質問文カウントダウン終了時

#=========================================================
# キーコードの送信
#=========================================================
def SendKeycodeToInputmanager(key):
	dll = cdll.LoadLibrary("ContentsPlayer2InputManager.dll")
	dll.SendKeycode(key)
	OutputDebugString("[StudioWa][INFO][ContentsToInput] Send Keycode "+str(key))

#=========================================================
# InputManagerとの通信用クラス
#=========================================================
class InputManagerReciverClass():
	def __init__(self):
		self._mInputManagerDataReciever	= cdll.LoadLibrary("InputController2ContentsPlayerDLL.dll")			#dllの読み込み
		self._mInputManagerDataReciever.GetLookPositionX.restype = c_float	#受信する型の定義（float）
		self._mInputManagerDataReciever.GetLookPositionY.restype = c_float	#受信する型の定義（float）
		self._mReciever					= self._mInputManagerDataReciever.CreateScenarioDataReciever()		#受信クラスの作成
		self._mRecievedData				= self._mInputManagerDataReciever.CreateRecievedScenarioData()	#データバッファの作成
		self._mAddress					= None
	
	def __del__(self):
		self._UnConect()
		self._mInputManagerDataReciever	= None
		self._mReciever					= None
		self._mRecievedData				= None
		self._mAddress					= None
	
	def SetAddress(self,inAddress):
		self._mAddress=c_wchar_p(inAddress);
	
	def Conect(self):
		return self._mInputManagerDataReciever.ConnectToTracker(self._mReciever,self._mAddress.value)
	
	def CheckUpdate(self):
		return self._mInputManagerDataReciever.GetRecievedToTrackerData(self._mRecievedData,self._mReciever)
	
	def GetKeycode(self):
		return self._mInputManagerDataReciever.GetKey(self._mRecievedData)
	
	def GetLookPositionX(self):
		return self._mInputManagerDataReciever.GetLookPositionX()
	
	def GetLookPositionY(self):
		return self._mInputManagerDataReciever.GetLookPositionY()
	
	def _UnConect(self):
		self._mInputManagerDataReciever.DeleteScenarioDataReciever(self._mReciever)
		self._mInputManagerDataReciever.DeleteRecievedScenarioData(self._mReciever)


#=========================================================
# ScenarioManagerとの通信用クラス
#=========================================================
class ScenarioReciverClass():
	def __init__(self):
		self._mScenarioDataReciever = cdll.LoadLibrary("VizardReciever.dll")			#dllの読み込み
		self._mReciever = self._mScenarioDataReciever.CreateScenarioDataReciever()		#受信クラスの作成
		self._mRecievedData = self._mScenarioDataReciever.CreateRecievedScenarioData()	#データバッファの作成
		self._mAddress = None
		
	def __del__(self):
		self._UnConect()
		self._mScenarioDataReciever	= None
		self._mReciever				= None
		self._mRecievedData			= None
		self._mAddress				= None
		
	def SetAddress(self,inAddress):
		self._mAddress=c_wchar_p(inAddress);
		
	def Conect(self):
		return self._mScenarioDataReciever.ConnectToScenarioEditor(self._mReciever,self._mAddress.value)
		
	def CheckRecieveThread(self):
		#受信スレッドが終了しているかどうか確認
		if self._mScenarioDataReciever.IsScenarioEditorDataRecieverThreadEnd(self._mReciever):
			return False	#終了
		return True			#通信中
		
	def GetDataType(self):
		return self._mScenarioDataReciever.GetScenarioDataType(self._mRecievedData)#受信データのうちデータタイプの収得
		
	def GetDataSize(self):
		return self._mScenarioDataReciever.GetScenarioDataSize(self._mRecievedData)	#受信データの文字列サイズを収得
	
	def GetDataText(self):
		name=c_wchar_p(self._mScenarioDataReciever.GetScenarioDataName(self._mRecievedData))#受信データの文字列を収得 Wchar
		return name.value	#実際の値を得るときはvalueを使う
	
	def GetDataDouble(self):#2013/07/18 doubleのポインタを利用するように変更
		dp=cast(self._mScenarioDataReciever.GetScenarioDataDoublePointer(self._mRecievedData),POINTER(c_double))
		return dp[0]
		
	def GetDataDoubleArrayData(self,index):#2013/07/18　追加
		dp=cast(self._mScenarioDataReciever.GetScenarioDataDoublePointer(self._mRecievedData),POINTER(c_double))
		return dp[index]
	
	def GetDataDoublePointer(self):#2013/07/18　追加
		return self._mScenarioDataReciever.GetScenarioDataDoublePointer(self._mRecievedData)

	def GetDataInt(self):#2013/07/19　追加
		return self._mScenarioDataReciever.GetScenarioDataInt(self._mRecievedData)

	def CheckReadScenarioData(self):	#2013/07/25　追加
		#データ収得用にデータを移動する
		if 0<self._mScenarioDataReciever.GetRecievedScenarioEditorData(self._mRecievedData, self._mReciever):
			return True
		return False
	
	def CheckUpdate(self):
		#データがあるかどうか （イベント）
		if 0<self._mScenarioDataReciever.CheckSetScenarioDataRecieveEvent(self._mReciever):
			return True
		return False
	
	def IncrementReadDataPosition(self):	#2013/07/25　追加
		#読み出し位置を一つ次に進める
		self._mScenarioDataReciever.IncrementReadPosition(self._mReciever)
		
	def SendPositionPointer(self,type,data):
		self._mScenarioDataReciever.SendScenarioDataPositionPointer(self._mReciever,type,data)
	
	def SendPosition(self,type,x,y,z):
		dx=c_double(0.0)
		dy=c_double(0.0)
		dz=c_double(0.0)
		dx.value=x
		dy.value=y
		dz.value=z
		self._mScenarioDataReciever.SendScenarioDataPosition(self._mReciever,type,dx,dy,dz)
		
	def SendAngle(self,type,x,y,z,angle):
		dx=c_double(0.0)
		dy=c_double(0.0)
		dz=c_double(0.0)
		dangle=c_double(0.0)
		dx.value=x
		dy.value=y
		dz.value=z
		dangle=angle;
		self._mScenarioDataReciever.SendScenarioDataAngle(self._mReciever,type,dx,dy,dz,dangle)
		
	def SendCollision(self,type,x,y,z,witdh,height,depth):
		dx=c_double(0.0)
		dy=c_double(0.0)
		dz=c_double(0.0)
		dwight=c_double(0.0)
		dheight=c_double(0.0)
		ddepth=c_double(0.0)
		dx.value=x
		dy.value=y
		dz.value=z
		dwidth.value=witdh
		dheight.value=height
		ddepth.value=depth
		self._mScenarioDataReciever.SendScenarioDataCollision(self._mReciever,type,dx,dy,dz,dwitdh,dheight,ddepth)
	
	def UseFirstTypeFootPrint(self):
		self._mScenarioDataReciever.UseScenarioDataFirstTypeFootPrint(self._mReciever)
		
	def UseSecondTypeFootPrint(self):
		self._mScenarioDataReciever.UseScenarioDataSecondTypeFootPrint(self._mReciever)
		
	def SetDeviceType(self,typeData):
		self._mScenarioDataReciever.SetScenarioDataDeviceType(self._mReciever,typeData)
		
	def _UnConect(self):
		self._mScenarioDataReciever.DeleteScenarioDataReciever(self._mReciever)
		self._mScenarioDataReciever.DeleteRecievedScenarioData(self._mReciever)
		