﻿# coding: utf-8

import viz
import vizinput
import vizmat
import vizact
import vizshape

import LoadIniFile

import os
import sys
from os import path
import codecs
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

def GetAbsPath(iPath):
	if iPath==None or iPath=="":
		return ""
	absPath = iPath
	if path.exists(absPath)!=True:	#絶対パスかどうか
		absPath = SETTING_FOLDER_PATH+iPath	#絶対パスの生成
		if path.exists(absPath)!=True:
			return ""
	return absPath

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス設定用クラス定義
class PathData():
	#コンストラクタ
	def __init__(self,charaControl):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Init PathControlClass')
		self.pathControl = charaControl
	
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][PathControlClass] : Del PathControlClass')
		self.pathControl.DeleteAll()
		del self.pathControl
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Start PathControlClass Update')
		pass
		
	#設定ファイルを読み込み、キャラクターとパスを配置
	def Load(self,fileName):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControlClass] : Load')
		self.pathControl.Load(fileName)
		self.pathControl.SetVisibleCharacterAndPath(False)
		
	#INIファイルにキャラクターとパスを保存する
	def Save(self,fileName):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : Save')
		self.pathControl.Save(fileName)
		
	#キャラクターとパスを削除
	def Delete(self):
		self.pathControl.Delete()
		
	#読込みに成功したかを判定する
	def IsSuccessLoading(self):
		flag = self.pathControl.IsSuccessLoading()
		return flag
	
	#キャラクター数を取得
	def GetCharacterCount(self):
		count = self.pathControl.GetCharacterCount()
		return count
		
	#キャラクターを設定
	def SetAvatar(self,id,fileName):
		self.pathControl.SetAvatar(id,fileName)
		
	#パスを設定
	def SetPath(self,id,fileName):
		self.pathControl.SetPath(id,fileName)
		
	#パスを作成
	def CreatePath(self,id):
		#パスの設定ファイルを読み込み
		path,line = self.pathControl.CreatePath(id)
		return path,line
		
	#立ちモーションを設定
	def SetIdleAnimation(self,id):
		self.pathControl.SetIdleAnimation(id)
		
	#歩きモーションを設定
	def SetWalkAnimation(self,id):
		self.pathControl.SetWalkAnimation(id)
		
	#移動の開始
	def Play(self,id):
		self.pathControl.Play(id)
		
	#移動の一時停止
	def Pause(self,id):
		self.pathControl.Pause(id)
		
	#移動の停止、リセット
	def Reset(self,id):
		self.pathControl.Reset(id)
		
	#移動の停止、最終地点にジャンプ
	def End(self,id):
		self.pathControl.End(id)
		
	#キャラクターの総数を取得
	def GetAvatarCount(self):
		count = self.pathControl.GetAvatarCount()
		return count
		
	#キャラクターのリストを取得
	def GetAvatarList(self):
		list = self.pathControl.GetAvatarList()
		return list
		
	#キャラクターファイル名のリストを取得
	def GetAvatarFileList(self):
		list = self.pathControl.GetAvatarFileList()
		return list
		
	#パスのリストを取得
	def GetPathList(self):
		list = self.pathControl.GetPathList()
		return list
		
	#パスのファイル名のリストを取得
	def GetPathFileList(self):
		list = self.pathControl.GetPathFileList()
		return list
		
	#再生状態の取得（0：停止、1：再生）
	def GetState(self,id):
		state = self.pathControl.GetState(id)
		return state
		
	#キャラクターの表示を設定
	def SetAvatarVisible(self,id,state):
		self.pathControl.SetAvatarVisible(id,state)
			
	#キャラクターの表示を取得
	def GetAvatarVisible(self,id):
		state = self.pathControl.GetAvatarVisible(id)
		return state
		
	#パスの表示を設定
	def SetPathVisible(self,id,state):
		self.pathControl.SetPathVisible(id,state)
		
	#パスの表示を取得
	def GetPathVisible(self,id):
		state = self.pathControl.GetPathVisible(id)
		return state
		
	#キャラクターの色設定
	def SetAvatarColor(self,id,color):
		self.pathControl.SetAvatarColor(id,color)
		
	#パスの色設定
	def SetPathColor(self,id,color):
		self.pathControl.SetPathColor(id,color)
		
	#視点の高さを設定
	def SetEyeHeight(self,height):
		self.pathControl.SetEyeHeight(height)
		
	#視点の高さを取得
	def GetEyeHeight(self):
		height = self.pathControl.GetEyeHeight()
		return height
		
	#視点用カメラを取得
	def GetEyeViewCamera(self,id):
		viewCamera = self.pathControl.GetEyeViewCamera(id)
		return viewCamera
		
	#アニメーションパスを作成
	def CreateAnimationPath(self):
		pass
		
	#アニメーションパスを削除
	def DeleteAnimationPath(self):
		pass
		
	#視点用カメラを更新
	def CameraUpdate(self):
		self.pathControl.CameraUpdate()
		