﻿# coding: utf-8
#PathSimulator

import viz

class DisplayTextBox():
	def __init__(self):
		#画像の表示用オブジェクトを作成する
		self._mObjectTextBox = viz.addTextbox()
		self._mObjectTextBox.overflow(viz.OVERFLOW_GROW)
		self._mObjectTextBox.disable(viz.PICKING)
		self._mObjectTextBox.setScale(1.0,1.0)
		self._mObjectTextBox.resolution(1)
		self._mPosition = [0,0]
		self._mWindowSize = [0,0]
		self._mTextSize = [0.7,1.0]	#画像の表示用オブジェクトのサイズ(pixel)
		self._mTextLine = 1
		
		self._mValue = 0.0
		self._mPreKeyState = False
	
	def __del__(self):
		if self._mObjectTextBox:
			self._mObjectTextBox.remove()	#画像の表示用オブジェクトを削除
	
	def SetFont(self,font):
		self._mObjectTextBox.font(font)
	
	def SetAlignment(self,alignment):
		self._mObjectTextBox.alignment(alignment)
	
	def SetFontColor(self,color):
		try:
			self._mObjectTextBox.color(color)
		except:
			try:
				list = color.split(',')
				r=int(list[0])
				g=int(list[1])
				b=int(list[2])
				self._mObjectTextBox.color(r,g,b)
			except:
				pass
	
	def SetRenderToAllWindowsExcept(self,list):
		if self._mObjectTextBox:
			self._mObjectTextBox.renderToAllWindowsExcept(list)
	
	def SetText(self,text):
		self._mTextLine = 1
		if text=='' or text==None:
			self._mObjectTextBox.message(str(''))
		if self._mObjectTextBox:
			try:
				list = text.split('\\n')
				newtext=''
				for listtext in list:
					newtext = newtext + listtext + '\n'
					self._mTextLine = self._mTextLine + 1
				self._mObjectTextBox.message(str(newtext))
			except:
				self._mObjectTextBox.message(str(text))
	
	def GetText(self):
		if self._mObjectTextBox:
			return self._mObjectTextBox.getMessage()
		return ''
	
	def SetPosition(self,x,y):
	#Windowのサイズを取得
		self._mWindowSize = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		#オブジェクトの表示位置を設定する
		#　---Vizardは左下が(0,0)なので注意
		if self._mObjectTextBox:
			self._mObjectTextBox.setPosition(x/self._mWindowSize[0],1.0-y/self._mWindowSize[1])
		self._mPosition = [x,y]
	
	def SetSize(self,width,height):
		WINDOW_SIZE = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		self._ResetSize()	#画像のサイズをリセットする
		self._mTextureSize=[width*(1280.0/WINDOW_SIZE[0]),
							height*(1024.0/WINDOW_SIZE[1])]
		if self._mObjectTextBox:
			self._mObjectTextBox.setScale(self._mTextureSize[0],
									 self._mTextureSize[1])	#サイズを設定
		
	def SetFontSize(self,size):
		self._mObjectTextBox.fontSize(size)
	
	def GetTextLineNum(self):
		return self._mTextLine
	
	def _ResetSize(self):
		SizeX = 1.0/self._mTextSize[0]	#現在のサイズから、1pixel*1pixelに戻す
		SizeY = 1.0/self._mTextSize[1]
		if self._mObjectTextBox:
			self._mObjectTextBox.setScale(SizeX,SizeY)
	
	def SetRenderOrder(self,orderVal):
	#レンダーオーダーを設定する
	#　---高い値になるほど、後に描画される（2Dモデルを最前面に表示したければこの値を高くする）
		if self._mObjectTextBox:
			self._mObjectTextBox.drawOrder(orderVal)
	
	def SetVisible(self,isVisible):
		if self._mObjectTextBox:
			if isVisible:
				self._mObjectTextBox.enable(viz.RENDERING)
				self._mObjectTextBox.visible(viz.ON)
			else:
				self._mObjectTextBox.disable(viz.RENDERING)
				self._mObjectTextBox.visible(viz.OFF)
	
	def GetModel(self):
		return self._mObjectTextBox
	
	def SetRatio(self,per):
		WINDOW_SIZE = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		OFFSET = [WINDOW_SIZE[0]-self._mWindowSize[0],WINDOW_SIZE[1]-self._mWindowSize[1]]
		self._ResetSize()
		if self._mObjectTextBox:
			self._mObjectTextBox.setScale(	self._mTextSize[0]*per[0],
										self._mTextSize[1]*per[1])
			self._mObjectTextBox.setPosition(	self._mPosition[0]/WINDOW_SIZE[0],
											1.0-(self._mPosition[1]+OFFSET[1])/WINDOW_SIZE[1])
											
	#数値を設定
	def SetValue(self,val):
		self._mValue = val
		self._mObjectTextBox.message(str(self._mValue))
		
	#数値を取得
	def GetValue(self):
		val = self._mValue
		return val
		
	def Update(self):
		state,val = self.InputVal()
		return state,val
		
	#文字入力
	def InputVal(self):
		state = False
		val = self._mValue
		
		keyState = False
		
		if self._mObjectTextBox.getVisible():
			if viz.key.isDown(viz.KEY_RETURN):
				keyState = True
			if viz.key.isDown(viz.KEY_KP_ENTER):
				keyState = True
				
			if keyState and self._mPreKeyState == False:
				inputVal = self._mObjectTextBox.getMessage()
				preVal = self._mValue
				val = 1.0
				
				try:
					val = float(inputVal)
				except:
					val = preVal
				
				if val != preVal:
					state = True
					self._mValue = val
					
				self.SetValue(self._mValue)
					
		self._mPreKeyState = keyState
		
		return state,self._mValue
		