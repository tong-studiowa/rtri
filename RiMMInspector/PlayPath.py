﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat
#import vizfx

import Object2d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス再生制御のクラス定義
class PlayPath(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PlayPath] : Init PlayPath')
		
		self._InputClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._PathControlClass = None
		self._CheckStateClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._AddPathClass = None
		self._FunctionButtonAreaClass = None
		self._ItemControlClass = None
		
		self._PlayState = False
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		
		self._SelectedCharacter = None
		self._SelectedPathData = None
		self._IsItem = False
		
		self._Character = None
		self._CharacterPosition = [0,0,0]
		self._CharacterOrientation = 180.0
		self._PathData = None
		self._AnimationPath = None
		
		self._Dummy = vizshape.addPlane([0.01,0.01])
		self._Dummy.disable(viz.PICKING)
		self._Dummy.disable(viz.RENDERING)
		self._Dummy.visible(viz.OFF)
		self._Dummy.alpha(0)
		self._Dummy.setPosition(0,0,0)
		self._Dummy.setEuler(0,0,0)
		
		#フレーム追加
		self._Position = [226,460]
		self._Position2d = [226,460]
		self._Position3d = [63,411]
		self._Scale = [100,36]
		self._Frame = self.CreateFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._TextScale = [18.6,18.6]
		self._Text = self.CreateText("",self._Position,self._TextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,characterControlClass,selectModelClass,pathControlClass,checkStateClass
				,stencilClass,manipulatorClass,addPathClass,functionButtonAreaClass,itemControlClass):
		self._InputClass = inputClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._PathControlClass = pathControlClass
		self._CheckStateClass = checkStateClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._AddPathClass = addPathClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._ItemControlClass = itemControlClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PlayPath] : Start PlayPath Update')
		
		#キャラクター選択中は表示
		selectedCharacter		= self._SelectModelClass.GetSelectedCharacter()
		selectedCharacterList	= self._SelectModelClass.GetSelectedCharacterList()
		selectedItem			= self._SelectModelClass.GetSelectedModel()
		selectedItemList		= self._SelectModelClass.GetSelectedModelList()
		selectedRoomList		= self._SelectModelClass.GetSelectedRoomList()
		selectedDoorList = self._SelectModelClass.GetSelectedDoorList()
		selectedWindowList = self._SelectModelClass.GetSelectedWindowList()
		
		self._SelectedCharacter = None
		self._SelectedPathData = None
		if selectedCharacter != None and len(selectedCharacterList) == 1 and  selectedItemList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == []:
			self._SelectedCharacter = selectedCharacter
			self._SelectedPathData = self._PathControlClass.GetPathData(selectedCharacter)
			self._IsItem = False
		elif selectedItem != None and len(selectedItemList) == 1 and  selectedCharacterList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == []:
			self._SelectedCharacter = selectedItem
			self._SelectedPathData = self._PathControlClass.GetPathData(selectedItem,True)
			self._IsItem = True
			
		if selectedCharacter != None and len(selectedCharacterList) == 1 and self._SelectedPathData != None and len(self._SelectedPathData) > 1 and  selectedItemList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == [] and self._AddPathClass.GetAddState() == False:
			self.SetVisible(True)
		elif selectedItem != None and len(selectedItemList) == 1 and self._SelectedPathData != None and len(self._SelectedPathData) > 1 and  selectedCharacterList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == [] and self._AddPathClass.GetAddState() == False:
			self.SetVisible(True)
		else:
			self.SetVisible(False)
			
		self._ClickState = False
		self._OverState = False
		
		if self._IsVisible and self._CheckStateClass.GetModeState() != 2:
			mousePosition = self._InputClass.GetMousePosition()
			
			#ボタンをクリックで再生状態を切り替え
			self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
				
			if self._OverState:
				if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
					self._ClickState = True
					
					if self._PlayState == True:
						self._PlayState = False
						self.Stop()					#停止
					else:
						self._PlayState = True
						self.Play()					#再生開始
						
					self.Set()
			
			if self._PlayState == True and self._CheckStateClass.GetOverState() == False:	#再生中に他ので停止
				if self._OverState == False:
					if self._InputClass.GetMouseStartDragState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() == False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
						self._PlayState = False
						self.Stop()					#停止
						self.Set()
					
					if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
						self._PlayState = False
						self.Stop()					#停止
						self.Set()
					
				#範囲選択をしたら停止
				if self._InputClass.GetMouseStartDragState() and self._InputClass.GetShiftKeyState():
					self._PlayState = False
					self.Stop()					#停止
					self.Set()
					
			#ステンシルを操作したら停止
			elif self._PlayState == True and self._StencilClass.GetClickState():
				self._PlayState = False
				self.Stop()					#停止
				self.Set()
				
		#ロールオーバー
		self.Rollover(self._OverState)
		
		if self._PlayState:
			self.Move()		#移動
			
	#リセット処理
	def Reset(self):
		self._PlayState = False
		self._ClickState = False
		self._IsVisible = False
		self.SetText(False)
		self.SetVisible(False)
		
	#リセット処理
	def ResetParameters(self):
		self._Character = None
		self._CharacterPosition = [0,0,0]
		self._CharacterOrientation = 180.0
		self._PathData = None
		
		if self._AnimationPath:
			self._AnimationPath.remove()
			del self._AnimationPath
		self._AnimationPath = None
		
		self._Dummy.setPosition(0,0,0)
		self._Dummy.setEuler(0,0,0)
		
	#設定を反映
	def Set(self):
		if self._PlayState == False:
			self.SetText(False)
		else:
			self.SetText(True)
			
		self._FunctionButtonAreaClass.SetIconVisible()
		
	#再生
	def Play(self):
		if self._IsItem:
			self._Character = self._SelectModelClass.GetSelectedModel()
			self._CharacterPosition = self._ItemControlClass.GetPosition(self._Character)
			self._CharacterOrientation =  self._ItemControlClass.GetOrientation(self._Character)
			
			self._SelectModelClass.UnSelectItem2()

		else:
			self._Character = self._SelectModelClass.GetSelectedCharacter()
			self._CharacterPosition = self._CharacterControlClass.GetPosition(self._Character)
			self._CharacterOrientation =  self._CharacterControlClass.GetOrientation(self._Character)
			
			self._CharacterControlClass.SetAnimation(self._Character,2)
			self._SelectModelClass.UnSelectCharacter2()
		
		self._PathData = self._PathControlClass.GetPathData(self._Character,self._IsItem)

		self.CreateAnimationPath()
		self._AnimationPath.play()
		
	#停止
	def Stop(self):
		self._AnimationPath.pause()
		self._AnimationPath.reset()
		
		if self._IsItem:
			self._ItemControlClass.SetPosition(self._Character,self._CharacterPosition)
			self._ItemControlClass.SetOrientation(self._Character,self._CharacterOrientation)
			
			selectedCharacterList = [self._Character]
			self._SelectModelClass.ReSelect(selectedCharacterList)
			
		else:
			self._CharacterControlClass.SetPosition(self._Character,self._CharacterPosition)
			self._CharacterControlClass.SetOrientation(self._Character,self._CharacterOrientation)
			
			self._CharacterControlClass.SetAnimation(self._Character,1)
			selectedCharacterList = [self._Character]
			self._SelectModelClass.ReSelectCharacter(selectedCharacterList)
		
		self.ResetParameters()
		
	#移動
	def Move(self):
		if self._IsItem:
			self._ItemControlClass.SetPosition(self._Character,self._Dummy.getPosition(),False)
			#self._ItemControlClass.SetAxisAngle(self._Character,self._Dummy.getAxisAngle())
			
			euler = [0,0,0]
			dummyEuler = self._Dummy.getEuler()
			euler[0] = dummyEuler[0] + 180.0
			self._ItemControlClass.SetOrientation(self._Character,euler[0],snap=False)
		else:
			self._CharacterControlClass.SetPosition(self._Character,self._Dummy.getPosition(),False)
			self._CharacterControlClass.SetAxisAngle(self._Character,self._Dummy.getAxisAngle())
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		"""
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
		"""
		self._Frame.visible(val)
		self._Text.visible(val)
		
	#テキスト設定
	def SetText(self,state):
		if state == False:
			self._Text.message("パス再生")
		else:
			self._Text.message("パス停止")
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#選択状態の取得
	def GetPlayState(self):
		state = self._PlayState
		
		return state
		
	#選択状態の設定
	def SetPlayState(self,state):
		self._PlayState = state
		
		self.Set()
		
	#アニメーションパスを作成
	def CreateAnimationPath(self):
		#位置のリストを作成
		positionList = []
		distanceList = []
		allDistance = 0.0
		allDistanceList = []
		
		#長さを確認
		distanceList.append(0.0)
		allDistanceList.append(0.0)
		for x in range(len(self._PathData)-1):
			path0 = self._PathData[x]
			pathPos0 = path0.GetPosition()
			pathPos0[1] = 0
			
			path1 = self._PathData[x+1]
			pathPos1 = path1.GetPosition()
			pathPos1[1] = 0
			
			distance = vizmat.Distance(pathPos0,pathPos1)
			
			distanceList.append(distance)
			allDistance = allDistance + distance
			allDistanceList.append(allDistance)
			
		#分割数は10cm
		deltaDistance = 0.1
		count = int(allDistance * 10)
		
		dist = 0.0
		for x in range(count):
			dist = dist + deltaDistance
					
			#最初の点を設定
			if x == 0:
				path = self._PathData[x]
				positionList.append(path.GetPosition())
		
			#最後の点を設定
			elif x == count-1:
				lastNum = len(self._PathData)-1
				path = self._PathData[lastNum]
				positionList.append(path.GetPosition())
				
			elif x < count-1:
				for y in range(len(allDistanceList)):
					p = allDistanceList[y]
					if dist < p or dist == p:
						
						#割合を確認
						distMin = allDistanceList[y-1]
						distMax = allDistanceList[y]
						
						pathMin = self._PathData[y-1]
						pathMax = self._PathData[y]
						
						perc = (dist - distMin) / (distMax - distMin)
						
						#位置設定
						pathPosMin = pathMin.GetPosition()
						pathPosMax = pathMax.GetPosition()
						
						pos = [0,0,0]
						pos[0] = pathPosMin[0]+(pathPosMax[0]-pathPosMin[0])*perc
						pos[1] = pathPosMax[1]
						pos[2] = pathPosMin[2]+(pathPosMax[2]-pathPosMin[2])*perc
						
						positionList.append(pos)
						
						break
		
		#パス作成
		path = viz.addAnimationPath()

		for x in range(len(positionList)):
			path.addControlPoint(x+1,pos=positionList[x])

		path.setLoopMode(viz.CIRCULAR)
		path.computeTangents()
		#path.setTranslateMode(viz.CUBIC_BEZIER)
		path.setAutoRotate(viz.ON)
		viz.link(path, self._Dummy)

		path.setSpeed(7)	#移動速度
		path.reset()

		self._AnimationPath = path
		
	#表示状態の設定
	def SetViewState(self,state):
		if state:
			self._Position = self._Position3d
		else:
			self._Position = self._Position2d
		
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Text,self._Position,self._TextScale)
		