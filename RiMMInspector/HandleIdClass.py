class HandleIdClass(object):

# member
    mSingletone = None     # create at first
    
# method    
    # create singletone method
    def __new__(self,*args,**kwargs):
        if self.mSingletone == None:                 # first call
            self.mSingletone = object.__new__(self)  # create instance
            self.mHandleId = 0                       # initialized ID
            #self.mSemaphore = Semaphore(1)           # create semaphore
        return self.mSingletone                      # retrun instance

    # return Handle ID method
    def GetHandleId(self):
        #self.mSemaphore.acquire()   # get semaphore
        self.mHandleId += 1         # increment ID
        #self.mSemaphore.release()   # release semaphore
        return self.mHandleId       # retrun ID

    def __init__(self):
        pass
    
    # called method at delete
    def __del__(self):
        pass

# ================================================
#    debug
# ================================================

def debugClass():
    aaa = HandleIdClass()
    print (aaa.GetHandleId())
    print (aaa.GetHandleId())

    bbb = HandleIdClass()
    print (bbb.GetHandleId())
    print (aaa.GetHandleId())
    print (bbb.GetHandleId())
    
    ccc = HandleIdClass()
    print (ccc.GetHandleId())
    print (ccc.GetHandleId())
    print (aaa.GetHandleId())
    print (bbb.GetHandleId())

