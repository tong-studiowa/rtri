﻿# coding: utf-8

import viz
import vizinput

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#一般関係のファンクション制御のクラス定義
class GeneralFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][GeneralFunction] : Init GeneralFunction')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._RoomControlClass = None
		self._SetItemButtonClass = None
		self._SetEnvironmentButtonClass = None
		self._StartPositionClass = None
		self._EnvironmentControlClass = None
		self._LogControlClass = None
		self._SetLogButtonClass = None
		self._DrawingClass = None
		self._SetDrawingButtonClass = None
		self._SetPathButtonClass = None
		self._CharacterControlClass = None
		self._PathControlClass = None
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,roomControlClass,setItemButtonClass,setRoomButtonClass
				,setEnvironmentButtonClass,startPositionClass,environmentControlClass,logControlClass
				,setLogButtonClass,drawingClass,setDrawingButtonClass,setPathButtonClass,characterControlClass
				,pathControlClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._SetItemButtonClass = setItemButtonClass
		self._SetRoomButtonClass = setRoomButtonClass
		self._SetEnvironmentButtonClass = setEnvironmentButtonClass
		self._StartPositionClass = startPositionClass
		self._EnvironmentControlClass = environmentControlClass
		self._LogControlClass = logControlClass
		self._SetLogButtonClass = setLogButtonClass
		self._DrawingClass = drawingClass
		self._SetDrawingButtonClass = setDrawingButtonClass
		self._SetPathButtonClass = setPathButtonClass
		self._CharacterControlClass = characterControlClass
		self._PathControlClass = pathControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][GeneralFunction] : Start GeneralFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.DeleteAll()
			self.Hide()
		elif num == 1:
			self.SetItem()
			self._SetRoomButtonClass.SetVisible(False)
			self._SetEnvironmentButtonClass.SetVisible(False)
			self._SetDrawingButtonClass.SetVisible(False)
			self._SetLogButtonClass.SetVisible(False)
			self._SetPathButtonClass.SetVisible(False)
		else:
			self.Hide()
			
			"""
			self.SetDrawing()
			self._SetRoomButtonClass.SetVisible(False)
			self._SetItemButtonClass.SetVisible(False)
			self._SetEnvironmentButtonClass.SetVisible(False)
			self._SetLogButtonClass.SetVisible(False)
			self._SetPathButtonClass.SetVisible(False)
			
			self.SetPath()
			self._SetItemButtonClass.SetVisible(False)
			self._SetRoomButtonClass.SetVisible(False)
			self._SetEnvironmentButtonClass.SetVisible(False)
			self._SetDrawingButtonClass.SetVisible(False)
			self._SetLogButtonClass.SetVisible(False)
			
			self.SetEnvironment()
			self._SetItemButtonClass.SetVisible(False)
			self._SetRoomButtonClass.SetVisible(False)
			self._SetDrawingButtonClass.SetVisible(False)
			self._SetLogButtonClass.SetVisible(False)
			self._SetPathButtonClass.SetVisible(False)
			
			self.SetLog()
			self._SetItemButtonClass.SetVisible(False)
			self._SetRoomButtonClass.SetVisible(False)
			self._SetEnvironmentButtonClass.SetVisible(False)
			self._SetDrawingButtonClass.SetVisible(False)
			self._SetPathButtonClass.SetVisible(False)
			"""
			
	#選択が解除された
	def Hide(self):
		self._SetItemButtonClass.SetVisible(False)
		self._SetRoomButtonClass.SetVisible(False)
		self._SetEnvironmentButtonClass.SetVisible(False)
		self._SetDrawingButtonClass.SetVisible(False)
		self._SetLogButtonClass.SetVisible(False)
		self._SetPathButtonClass.SetVisible(False)
		
	#アイテム設定
	def SetItem(self):
		if self._SetItemButtonClass.GetVisible():
			self._SetItemButtonClass.SetVisible(False)
		else:
			self._SetItemButtonClass.SetVisible(True)
			
	#間取り設定
	def SetRoom(self):
		if self._SetRoomButtonClass.GetVisible():
			self._SetRoomButtonClass.SetVisible(False)
		else:
			self._SetRoomButtonClass.SetVisible(True)
			
	#地形設定
	def SetEnvironment(self):
		if self._SetEnvironmentButtonClass.GetVisible():
			self._SetEnvironmentButtonClass.SetVisible(False)
		else:
			self._SetEnvironmentButtonClass.SetVisible(True)
		
	#図面設定
	def SetDrawing(self):
		if self._SetDrawingButtonClass.GetVisible():
			self._SetDrawingButtonClass.SetVisible(False)
		else:
			self._SetDrawingButtonClass.SetVisible(True)
		
	#ログ設定
	def SetLog(self):
		if self._SetLogButtonClass.GetVisible():
			self._SetLogButtonClass.SetVisible(False)
		else:
			self._SetLogButtonClass.SetVisible(True)
		
	#パス設定
	def SetPath(self):
		if self._SetPathButtonClass.GetVisible():
			self._SetPathButtonClass.SetVisible(False)
		else:
			self._SetPathButtonClass.SetVisible(True)
		
	#新規作成
	def DeleteAll(self):
		answer = vizinput.ask('全てを削除して初期化します。よろしいですか？')
		
		if answer:
			self._ItemControlClass.DeleteAll()
			self._RoomControlClass.DeleteAll()
			self._EnvironmentControlClass.Delete()
			self._StartPositionClass.Reset()
			self._DrawingClass.Delete()
			self._LogControlClass.DeleteAll()
			self._CharacterControlClass.DeleteAll()
			self._PathControlClass.DeleteAllCharacter()
			