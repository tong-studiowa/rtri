﻿# coding: utf-8

import viz
import Object2d
import LoadIniFile

from os import path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

undoCountTemp = iniFile.GetItemData('Setting','UndoCount')
undoCount = int(undoCountTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アンドゥ制御のクラス定義
class Redo(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Redo] : Init Redo')
		
		self._InputClass = None
		self._LogControlClass = None
		self._StencilClass = None
		self._FunctionButtonAreaClass = None
		self._ChangeViewClass = None
		self._BackgroundClass = None
		self._SetDirectionClass = None
		self._ItemControlClass = None
		self._RoomControlClass = None
		self._SelectModelClass = None
		self._SelectWallClass = None
		self._SelectLogClass = None
		self._SelectStartPositionClass = None
		self._UndoClass = None
		self._CharacterControlClass = None
		self._PathControlClass = None
		self._SelectPathClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._RedoCount = undoCount
		self._RedoList = []
		
		self._UndoData = []
		
		self._Is3d = False
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._Position = [243,windowSize[1]-92]
		self._PositionX = [246,80]
		self._Scale = [36,36]
		self._Frame = self.CreateFrame(self._Position,self._Scale,order=2)
		
		#アイコン追加
		texturePath = viz.res.getPublishedPath('Resource\\Interface\\Icon_Redo.tga')
		self._Icon = self.CreateTexIcon(texturePath,self._Position,self._Scale,order=3)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,stencilClass,functionButtonAreaClass
				,changeViewClass,backgroundClass,setDirectionClass,itemControlClass
				,roomControlClass,selectModelClass,selectWallClass
				,selectLogClass,selectStartPositionClass
				,undoClass,characterControlClass,pathControlClass,selectPathClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._StencilClass = stencilClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._ChangeViewClass = changeViewClass
		self._BackgroundClass = backgroundClass
		self._SetDirectionClass = setDirectionClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._SelectModelClass = selectModelClass
		self._SelectWallClass = selectWallClass
		self._SelectLogClass = selectLogClass
		self._SelectStartPositionClass = selectStartPositionClass
		self._UndoClass = undoClass
		self._CharacterControlClass = characterControlClass
		self._PathControlClass = pathControlClass
		self._SelectPathClass = selectPathClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Redo] : Start Redo Update')
		
		self._ClickState = False
		self._OverState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
		
		if self._OverState:
			if self._InputClass.GetMouseClickState():
				self._ClickState = True
				self.Redo()
			
		#ロールオーバー
		self.Rollover(self._OverState)
		
	#リセット処理
	def Reset(self):
		self._ClickState = False
		
	#リドゥ処理
	def Redo(self):
		#print 'Redo'
		data = self.GetFromList()
		
		if data != None:
			self.UnSelect()
			
			if data[0] == 0:	#アイテムの移動
				self.TranslateItem(data)
			elif data[0] == 1:	#アイテムの回転
				self.RotateItem(data)
			elif data[0] == 2:	#アイテムのサイズ変更
				self.SetItemSize(data)
			elif data[0] == 3:	#アイテムのテクスチャ変更
				self.SetItemTexture(data)
			elif data[0] == 4:	#間取りの移動
				self.TranslateRoom(data)
			elif data[0] == 5:	#床のテクスチャ変更
				self.SetFloorTexture(data)
			elif data[0] == 6:	#壁のテクスチャ変更
				self.SetWallTexture(data)
			elif data[0] == 7:	#天井のテクスチャ変更
				self.SetCeilingTexture(data)
			elif data[0] == 8:	#壁の高さ変更
				self.SetWallHeight(data)
			elif data[0] == 9:	#壁の移動
				self.TranslateWall(data)
			elif data[0] == 10:	#ドアの移動
				self.TranslateDoor(data)
			elif data[0] == 11:	#窓の移動
				self.TranslateWindow(data)
			elif data[0] == 12:	#窓の高さ変更
				self.SetWindowHeight(data)
			elif data[0] == 13:	#アイテムの追加（削除する）
				self.AddItem(data)
			elif data[0] == 14:	#アイテムの削除（追加する）
				self.DeleteItem(data)
			elif data[0] == 15:	#間取りの追加（削除する）
				self.AddRoom(data)
			elif data[0] == 16:	#間取りの削除（追加する）
				self.DeleteRoom(data)
			elif data[0] == 17:	#壁の接合点の追加（削除する）
				self.AddWallPoint(data)
			elif data[0] == 18:	#壁の接合点の削除（追加する）
				self.DeleteWallPoint(data)
			elif data[0] == 19:	#ドアの追加（削除する）
				self.AddDoor(data)
			elif data[0] == 20:	#ドアの削除（追加する）
				self.DeleteDoor(data)
			elif data[0] == 21:	#窓の追加（削除する）
				self.AddWindow(data)
			elif data[0] == 22:	#窓の削除（追加する）
				self.DeleteWindow(data)
			elif data[0] == 23:	#アイテムのドラッグによるサイズ変更
				self.SetItemSizeAndPos(data)
			elif data[0] == 24:	#ショートカットキーによる追加（削除する）
				self.AddShortcutKey(data)
			elif data[0] == 25:	#ショートカットキーによる削除（追加する）
				self.DeleteShortcutKey(data)
			elif data[0] == 26:	#キャラクターの追加（削除する）
				self.AddCharacter(data)
			elif data[0] == 27:	#キャラクターの削除（追加する）
				self.DeleteCharacter(data)
			elif data[0] == 28:	#キャラクターの移動
				self.TranslateCharacter(data)
			elif data[0] == 29:	#キャラクターの回転
				self.RotateCharacter(data)
			elif data[0] == 30:	#パスの追加（削除する）
				self.AddPath(data)
			elif data[0] == 31:	#パスの削除（追加する）
				self.DeletePath(data)
			elif data[0] == 32:	#パスの移動
				self.TranslatePath(data)
			elif data[0] == 33:	#パス全体の追加（削除する）
				self.AddAllPath(data)
			elif data[0] == 34:	#パス全体の削除（追加する）
				self.DeleteAllPath(data)
			elif data[0] == 35:	#壁の表示設定
				self.SetWallVisible(data)
			elif data[0] == 36:	#アイテム、キャラクター、間取りの移動
				self.TranslateModel(data)
			elif data[0] == 37:	#アイテム、キャラクターの回転
				self.RotateModel(data)
				
	#リストに追加
	def AddToList(self,data):
		#print 'data=',data
		self._RedoList.append(data)
		
		if len(self._RedoList) > self._RedoCount:
			newList = []
			for x in range(len(self._RedoList)):
				if x != 0:
					data = self._RedoList[x]
					newList.append(data)
			self._RedoList = newList
			
	#リストから取得
	def GetFromList(self):
		data = None
		if len(self._RedoList) == 1:
			data = self._RedoList[0]
			self._RedoList = []
		elif len(self._RedoList) > 1:
			data = self._RedoList[len(self._RedoList)-1]
			
			newList = []
			for x in range(len(self._RedoList)):
				if x != len(self._RedoList)-1:
					newData = self._RedoList[x]
					newList.append(newData)
			self._RedoList = newList
			
		return data
		
	#リストをクリア
	def ClearList(self):
		self._RedoList = []
		
	#選択を解除
	def UnSelect(self):
		self._SelectModelClass.UnSelect()
		self._SelectWallClass.UnSelect()
		self._SelectLogClass.UnSelect()
		self._SelectStartPositionClass.UnSelect()
		self._SelectPathClass.UnSelect()
		
	#アイテムの移動処理
	def TranslateItem(self,data):
		self._UndoData = []
		self._UndoData.append(0)
		
		for x in range(len(data)):
			if x != 0:
				itemNum = data[x][0]
				itemPos = data[x][1]
				
				prePos = self._ItemControlClass.GetPosition(itemNum)
				self._UndoData.append([itemNum,prePos])
				
				self._ItemControlClass.SetPosition(itemNum,itemPos)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムの回転処理
	def RotateItem(self,data):
		self._UndoData = []
		self._UndoData.append(1)
		
		for x in range(len(data)):
			if x != 0:
				itemNum		= data[x][0]
				itemPos		= data[x][1]
				itemAngle	= data[x][2]
				
				prePos		= self._ItemControlClass.GetPosition(itemNum)
				preAngle	= self._ItemControlClass.GetAxisAngle(itemNum)
				self._UndoData.append([itemNum,prePos,preAngle])
				
				self._ItemControlClass.SetPosition(itemNum,itemPos)
				self._ItemControlClass.SetAxisAngle(itemNum,itemAngle)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムのサイズ変更処理
	def SetItemSize(self,data):
		self._UndoData = []
		self._UndoData.append(2)
		
		itemNum  = data[1][0]
		itemSize = data[1][1]
		
		preSize = self._ItemControlClass.GetSize(itemNum)
		self._UndoData.append([itemNum,preSize])
		
		self._ItemControlClass.SetSize(itemNum,itemSize)
		
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムのテクスチャ変更処理
	def SetItemTexture(self,data):
		self._UndoData = []
		self._UndoData.append(3)
		
		itemNum		= data[1][0]
		itemNode	= data[1][1]
		itemTexture	= data[1][2]
		itemFile	= data[1][3]
		
		preTexture = self._ItemControlClass.GetTexture(itemNum,itemNode)
		preTexFile = self._ItemControlClass.GetTextureFilePath(itemNum,itemNode)
		self._UndoData.append([itemNum,itemNode,preTexture,preTexFile])

		self._ItemControlClass.ResetTexture(itemNum,itemNode,itemTexture,itemFile)
		
		self._UndoClass.AddToList(self._UndoData)
		
	#間取りの移動処理
	def TranslateRoom(self,data):
		self._UndoData = []
		self._UndoData.append(4)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum = data[x][0]
				roomPos = data[x][1]
				
				prePos = self._RoomControlClass.GetFloorPosition(roomNum)
				self._UndoData.append([roomNum,prePos])
				
				self._RoomControlClass.SetFloorPosition(roomNum,roomPos)
				
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#床のテクスチャ変更処理
	def SetFloorTexture(self,data):
		self._UndoData = []
		self._UndoData.append(5)
		
		for x in range(len(data)):
			if x != 0:
				roomNum				= data[x][0]
				roomFloorTex		= data[x][1]
				roomFloorTexFile	= data[x][2]
				
				preTexture = self._RoomControlClass.GetFloorTexture(roomNum)
				preTexFile,preTexSize = self._RoomControlClass.GetFloorTextureSetting(roomNum)
				self._UndoData.append([roomNum,preTexture,preTexFile])
				
				self._RoomControlClass.SetFloorTexture(roomNum,roomFloorTex,roomFloorTexFile)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#壁のテクスチャ変更処理
	def SetWallTexture(self,data):
		self._UndoData = []
		self._UndoData.append(6)
		
		for x in range(len(data)):
			if x != 0:
				roomNum			= data[x][0]
				roomWallTex		= data[x][1]
				roomWallTexFile	= data[x][2]
				
				preTexture = self._RoomControlClass.GetWallTexture(roomNum)
				preTexFile,preTexSize = self._RoomControlClass.GetWallTextureSetting(roomNum)
				self._UndoData.append([roomNum,preTexture,preTexFile])
				
				self._RoomControlClass.SetWallTexture(roomNum,roomWallTex,roomWallTexFile)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#天井のテクスチャ変更処理
	def SetCeilingTexture(self,data):
		self._UndoData = []
		self._UndoData.append(7)
		
		for x in range(len(data)):
			if x != 0:
				roomNum				= data[x][0]
				roomCeilingTex		= data[x][1]
				roomCeilingTexFile	= data[x][2]
				
				preTexture = self._RoomControlClass.GetCeilingTexture(roomNum)
				preTexFile,preTexSize = self._RoomControlClass.GetCeilingTextureSetting(roomNum)
				self._UndoData.append([roomNum,preTexture,preTexFile])
				
				self._RoomControlClass.SetCeilingTexture(roomNum,roomCeilingTex,roomCeilingTexFile)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#壁の高さ変更処理
	def SetWallHeight(self,data):
		self._UndoData = []
		self._UndoData.append(8)
		
		self.SetHole(False)
				
		for x in range(len(data)):
			if x != 0:
				roomNum		= data[x][0]
				wallHeight	= data[x][1]
				
				preWallHeight = self._RoomControlClass.GetWallHeight(roomNum)
				self._UndoData.append([roomNum,preWallHeight])
				
				self._RoomControlClass.SetWallHeight(roomNum,wallHeight)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#壁の移動処理
	def TranslateWall(self,data):
		self._UndoData = []
		self._UndoData.append(9)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				modelState	= data[x][0]
				roomNum		= data[x][1]
				wallNum		= data[x][2]
				wallPos		= data[x][3]
				#print 'undoData=',9,modelState,roomNum,wallNum,wallPos
				
				prePos = None
				if modelState == 1:
					model = self._RoomControlClass.GetWallLineModel(roomNum,wallNum)
					prePos = model.getPosition(mode=viz.ABS_GLOBAL)
				elif modelState == 2:
					model = self._RoomControlClass.GetWallPointModel(roomNum,wallNum)
					prePos = model.getPosition(mode=viz.ABS_GLOBAL)
				self._UndoData.append([modelState,roomNum,wallNum,prePos])
				
				floorPosition = self._RoomControlClass.GetFloorPosition(roomNum)
				wallPos[0] = wallPos[0] - floorPosition[0]
				wallPos[1] = wallPos[1] - floorPosition[1]
				wallPos[2] = wallPos[2] - floorPosition[2]
				
				if modelState == 1:
					self._RoomControlClass.SetWallLinePosition(roomNum,wallNum,wallPos)
					
				elif modelState == 2:
					self._RoomControlClass.SetWallPointPosition(roomNum,wallNum,wallPos)
					
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#ドアの移動処理
	def TranslateDoor(self,data):
		self._UndoData = []
		self._UndoData.append(10)
		
		roomNum		= data[1][0]
		doorNum		= data[1][1]
		wallNum		= data[1][2]
		percentage	= data[1][3]
		
		doorState = self._RoomControlClass.GetDoorState(roomNum,doorNum)
		self._UndoData.append([roomNum,doorNum,doorState[1],doorState[2]])
		
		self._RoomControlClass.SetDoorPosition(roomNum,doorNum,wallNum,percentage)
		self._RoomControlClass.UpdateDoorList(roomNum,doorNum,wallNum,percentage)
		
		self.SetHole(False)
		self._RoomControlClass.ReCreateWall(roomNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#窓の移動処理
	def TranslateWindow(self,data):
		self._UndoData = []
		self._UndoData.append(11)
		
		roomNum			= data[1][0]
		windowNum		= data[1][1]
		wallNum			= data[1][2]
		percentage		= data[1][3]
		
		windowState = self._RoomControlClass.GetWindowState(roomNum,windowNum)
		self._UndoData.append([roomNum,windowNum,windowState[2],windowState[3]])
		
		self._RoomControlClass.SetWindowPosition(roomNum,windowNum,wallNum,percentage)
		self._RoomControlClass.UpdateWindowList(roomNum,windowNum,2,0,wallNum,percentage)
		
		self.SetHole(False)
		
		self._RoomControlClass.ReCreateWall(roomNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#窓の高さ変更処理
	def SetWindowHeight(self,data):
		self._UndoData = []
		self._UndoData.append(12)
		
		for x in range(len(data)):
			if x != 0:
				roomNum			= data[x][0]
				windowNum		= data[x][1]
				windowHeight	= data[x][2]
				
				preWindowHeight = self._RoomControlClass.GetWindowHeight(roomNum,windowNum)
				self._UndoData.append([roomNum,windowNum,preWindowHeight])
				
				self._RoomControlClass.SetWindowHeight(roomNum,windowNum,windowHeight)
				self._RoomControlClass.UpdateWindowList(roomNum,windowNum,1,windowHeight,0,0)
				
		self.SetHole(False)
		
		self._RoomControlClass.ReCreateWall(roomNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムの追加処理（削除する）
	def AddItem(self,data):
		self._UndoData = []
		self._UndoData.append(14)
		
		self._SelectModelClass.UnSelectItem()
		
		for x in range(len(data)):
			if x != 0:
				itemNum = data[x][0]
				
				preFile		= self._ItemControlClass.GetFileName(itemNum)
				prePos		= self._ItemControlClass.GetPosition(itemNum)
				preOri		= self._ItemControlClass.GetOrientation(itemNum)
				preSize		= self._ItemControlClass.GetSize(itemNum)
				preMoveY	= self._ItemControlClass.GetMoveYState(itemNum)
				preTexList	= self._ItemControlClass.GetTextureList(itemNum)
				pathPosList	= self._ItemControlClass.GetPathPositionList(itemNum)

				#print 'redoAddItem',itemNum
				
				self._UndoData.append([itemNum,preFile,prePos,preOri,preSize,preMoveY,preTexList,pathPosList])
				
		for x in range(len(data)):
			if x != 0:
				itemNum = data[x][0] - x + 1
				self._ItemControlClass.Delete(itemNum)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムの削除処理（追加する）
	def DeleteItem(self,data):
		self._UndoData = []
		self._UndoData.append(13)
		
		for x in range(len(data)):
			if x != 0:
				itemNum		= data[x][0]
				file		= data[x][1]
				pos			= data[x][2]
				ori			= data[x][3]
				size		= data[x][4]
				moveY		= data[x][5]
				texList		= data[x][6]
				pathPosList	= data[x][7]
				
				#print 'redoDeleteItem',itemNum
				
				newItemNum = self._ItemControlClass.Add(file)
				self._ItemControlClass.AddNewItemToList(newItemNum)
				
				self._ItemControlClass.SetPosition(newItemNum,pos)
				self._ItemControlClass.SetOrientation(newItemNum,ori)
				self._ItemControlClass.SetSize(newItemNum,size)
				self._ItemControlClass.SetMoveYState(newItemNum,moveY)
				if texList != []:
					for texData in texList:
						self._ItemControlClass.SetTexture(newItemNum,texData[0],texData[1])
				self._ItemControlClass.SetPathPositionList(newItemNum,pathPosList)
		
				if newItemNum != 0 and itemNum != newItemNum:
					#print 'changeID',newItemNum,itemNum
					self._ItemControlClass.ChangeId(itemNum)
					
				self._UndoData.append([itemNum])
		
		self._UndoClass.AddToList(self._UndoData)
		
	#間取りの追加処理（削除する）
	def AddRoom(self,data):
		self._UndoData = []
		self._UndoData.append(16)
		
		self.SetHole(False)
		
		roomNum = data[1][0]
		file	= data[1][1]
		pos		= data[1][2]
		
		preFile,roomName= self._RoomControlClass.GetFileName(roomNum)
		prePos			= self._RoomControlClass.GetFloorPosition(roomNum)
		prePointData	= self._RoomControlClass.GetWallPointList(roomNum)
		preWallheight	= self._RoomControlClass.GetWallHeight(roomNum)
		preTexData		= self._RoomControlClass.GetTextureData(roomNum)
		preDoorData		= self._RoomControlClass.GetDoorData(roomNum)
		preWindowData	= self._RoomControlClass.GetWindowData(roomNum)
		preWallVisible	= self._RoomControlClass.GetWallVisibleList(roomNum)
		preIsOpRoom		= self._RoomControlClass.GetIsOpRoom(roomNum)
		self._UndoData.append([roomNum,preFile,prePos,prePointData,preWallheight,preTexData,preDoorData,preWindowData,preWallVisible,preIsOpRoom])
		
		self._SelectModelClass.UnSelectRoom()
		self._SelectModelClass.UnSelectDoor()
		self._SelectModelClass.UnSelectWindow()
		self._SelectWallClass.UnSelect()
		self._RoomControlClass.Delete(roomNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#間取りの削除処理（追加する）
	def DeleteRoom(self,data):
		self._UndoData = []
		self._UndoData.append(15)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum		= data[x][0]
				file		= data[x][1]
				pos			= data[x][2]
				pointData	= data[x][3]
				wallheight	= data[x][4]
				texData		= data[x][5]
				doorData	= data[x][6]
				windowData	= data[x][7]
				wallVisible	= data[x][8]
				isOpRoom	= data[x][9]
				
				#print 'roomNum',roomNum
				#print 'file',file
				#print 'pos',pos
				#print 'pointData',pointData
				#print 'wallheight',wallheight
				#print 'texData',texData
				#print 'doorData',doorData
				#print 'windowData',windowData
				
				roomData = []
				for y in range(len(data[x])):
					roomData.append(data[x][y])
					
				newRoomNum = self._RoomControlClass.ReCreate(roomData)
				self._RoomControlClass.AddNewRoomToList(newRoomNum)
				
				if newRoomNum != 0 and roomNum != newRoomNum:
					self._RoomControlClass.ChangeId(roomNum)
			
				self._UndoData.append([roomNum,file,pos])
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#壁の接合点の追加処理（削除する）
	def AddWallPoint(self,data):
		self._UndoData = []
		self._UndoData.append(18)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum = data[x][0]
				wallNum	= data[x][1]
				
				#print 'roomNum',roomNum
				#print 'wallNum',wallNum
				
				pointDataTemp	= self._RoomControlClass.GetWallPointList(roomNum)
				pointData = []
				for point in pointDataTemp:
					pointData.append(point)
					
				wallVisibleListTemp	= self._RoomControlClass.GetWallVisibleList(roomNum)
				wallVisibleList = []
				for visible in wallVisibleListTemp:
					wallVisibleList.append(visible)
					
				floorPosTemp = self._RoomControlClass.GetFloorPosition(roomNum)
				floorPos = []
				for pos in floorPosTemp:
					floorPos.append(pos)
					
				self._UndoData.append([roomNum,wallNum+1,pointData,wallVisibleList,floorPos])
				
				self._SelectWallClass.UnSelect()
				self._RoomControlClass.RemoveWallPoint(roomNum,wallNum+1)
				
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#壁の接合点の削除処理（追加する）
	def DeleteWallPoint(self,data):
		self._UndoData = []
		self._UndoData.append(17)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum			= data[x][0]
				wallNum			= data[x][1]
				pointData		= data[x][2]
				wallVisibleList	= data[x][3]
				floorPos		= data[x][4]
				
				#print 'roomNum',roomNum
				#print 'wallNum',wallNum
				#print 'pointData',pointData
				#print 'wallVisibleList',wallVisibleList
				#print 'floorPos',floorPos
				
				self._UndoData.append([roomNum,wallNum-1])
				
				self._RoomControlClass.AddWallPoint(roomNum,wallNum-1,pointData,wallVisibleList,floorPos)
				
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#ドアの追加（削除する）
	def AddDoor(self,data):
		self._UndoData = []
		self._UndoData.append(20)
		
		self.SetHole(False)
		
		self._SelectModelClass.UnSelectDoor()
		
		for x in range(len(data)):
			if x != 0:
				roomNum = data[x][0]
				doorNum = data[x][1]
				
				doorDataTemp = self._RoomControlClass.GetDoorData(roomNum)
				doorData = ['',0,0.0]
				doorData[0] = doorDataTemp[doorNum][0]
				doorData[1] = doorDataTemp[doorNum][1]
				doorData[2] = doorDataTemp[doorNum][2]
				
				self._UndoData.append([roomNum,doorNum,doorData])
				
				self._RoomControlClass.DeleteDoor(roomNum,doorNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#ドアの削除（追加する）
	def DeleteDoor(self,data):
		self._UndoData = []
		self._UndoData.append(19)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum		= data[x][0]
				doorNum		= data[x][1]
				doorData	= data[x][2]
				
				#print 'roomNum',roomNum
				#print 'doorNum',doorNum
				#print 'doorData',doorData
				
				newDoorNum = self._RoomControlClass.AddDoor(roomNum,doorData[0],doorData[1],doorData[2])
				
				if newDoorNum != 0 and doorNum != newDoorNum:
					self._RoomControlClass.ChangeDoorId(roomNum,doorNum)
					
				self._UndoData.append([roomNum,doorNum])
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#窓の追加（削除する）
	def AddWindow(self,data):
		self._UndoData = []
		self._UndoData.append(22)
		
		self.SetHole(False)
		
		self._SelectModelClass.UnSelectWindow()
		
		for x in range(len(data)):
			if x != 0:
				roomNum		= data[x][0]
				windowNum	= data[x][1]
				
				windowDataTemp = self._RoomControlClass.GetWindowData(roomNum)
				windowData = ['',0.0,0,0.0]
				windowData[0] = windowDataTemp[windowNum][0]
				windowData[1] = windowDataTemp[windowNum][1]
				windowData[2] = windowDataTemp[windowNum][2]
				windowData[3] = windowDataTemp[windowNum][3]
				
				self._UndoData.append([roomNum,windowNum,windowData])
				
				self._RoomControlClass.DeleteWindow(roomNum,windowNum)
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#窓の削除（追加する）
	def DeleteWindow(self,data):
		self._UndoData = []
		self._UndoData.append(21)
		
		self.SetHole(False)
		
		for x in range(len(data)):
			if x != 0:
				roomNum		= data[x][0]
				windowNum	= data[x][1]
				windowData	= data[x][2]
				
				#print 'roomNum',roomNum
				#print 'windowNum',windowNum
				#print 'windowData',windowData
				
				newWindowNum = self._RoomControlClass.AddWindow(roomNum,windowData[0],windowData[2],windowData[3],windowData[1])
				
				if newWindowNum != 0 and windowNum != newWindowNum:
					self._RoomControlClass.ChangeWindowId(roomNum,windowNum)
					
				self._UndoData.append([roomNum,windowNum])
		
		self.SetHole(True)
		
		#表示関係を設定
		self._RoomControlClass.SetRoomVisibleState()
		
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテムのドラッグによるサイズ変更
	def SetItemSizeAndPos(self,data):
		self._UndoData = []
		self._UndoData.append(23)
		
		itemNum  = data[1][0]
		itemPos  = data[1][1]
		itemSize = data[1][2]
		
		prePos = self._ItemControlClass.GetPosition(itemNum)
		preSize = self._ItemControlClass.GetSize(itemNum)
		self._UndoData.append([itemNum,prePos,preSize])
		
		self._ItemControlClass.SetPosition(itemNum,itemPos)
		self._ItemControlClass.SetSize(itemNum,itemSize)
		
		self._UndoClass.AddToList(self._UndoData)
		
	#ショートカットキーによる追加（削除する）
	def AddShortcutKey(self,data):
		self._UndoData = []
		self._UndoData.append(25)
		
		self._SelectModelClass.UnSelect()
		self._SelectWallClass.UnSelect()
		
		#間取り、ドア、窓を含む場合は壁を再作成
		roomState = False
		for x in range(len(data)):
			if x != 0:
				if data[x][0] == 1 or data[x][0] == 3 or data[x][0] == 4:
					roomState = True
					
		if roomState:
			self.SetHole(False)
			
		itemList   = []
		roomList   = []
		charaList  = []
		doorList   = []
		windowList = []
		
		for x in range(len(data)):
			if x != 0:
				category = data[x][0]
				
				#アイテム
				if category == 0:
					itemNum = data[x][1]
					
					preFile		= self._ItemControlClass.GetFileName(itemNum)
					prePos		= self._ItemControlClass.GetPosition(itemNum)
					preOri		= self._ItemControlClass.GetOrientation(itemNum)
					preSize		= self._ItemControlClass.GetSize(itemNum)
					preMoveY	= self._ItemControlClass.GetMoveYState(itemNum)
					preTexList	= self._ItemControlClass.GetTextureList(itemNum)
					pathPosList	= self._ItemControlClass.GetPathPositionList(itemNum)

					#print 'undoAddItem',itemNum
					
					self._UndoData.append([category,itemNum,preFile,prePos,preOri,preSize,preMoveY,preTexList,pathPosList])
					itemList.append(itemNum)
					
				#間取り
				if category == 1:
					roomNum = data[x][1]
					
					preFile,roomName	= self._RoomControlClass.GetFileName(roomNum)
					prePos				= self._RoomControlClass.GetFloorPosition(roomNum)
					prePointData		= self._RoomControlClass.GetWallPointList(roomNum)
					preWallheight		= self._RoomControlClass.GetWallHeight(roomNum)
					preTexData			= self._RoomControlClass.GetTextureData(roomNum)
					preDoorData			= self._RoomControlClass.GetDoorData(roomNum)
					preWindowData		= self._RoomControlClass.GetWindowData(roomNum)
					preWallVisible		= self._RoomControlClass.GetWallVisibleList(roomNum)
					preIsOpRoom			= self._RoomControlClass.GetIsOpRoom(roomNum)
					
					self._UndoData.append([category,roomNum,preFile,prePos,prePointData,preWallheight,preTexData,preDoorData,preWindowData,preWallVisible,preIsOpRoom])
					roomList.append(roomNum)
					
				#キャラクター
				if category == 2:
					charaNum = data[x][1]
					
					preFile		= self._CharacterControlClass.GetFileName(charaNum)
					prePos		= self._CharacterControlClass.GetPosition(charaNum)
					preOri		= self._CharacterControlClass.GetOrientation(charaNum)
					pathPosList	= self._CharacterControlClass.GetPathPositionList(charaNum)
					
					self._UndoData.append([category,charaNum,preFile,prePos,preOri,pathPosList])
					charaList.append(charaNum)
					
				#ドア
				if category == 3:
					roomNum = data[x][1]
					doorNum = data[x][2]
					
					doorDataTemp = self._RoomControlClass.GetDoorData(roomNum)
					doorData = ['',0,0.0]
					doorData[0] = doorDataTemp[doorNum][0]
					doorData[1] = doorDataTemp[doorNum][1]
					doorData[2] = doorDataTemp[doorNum][2]
				
					self._UndoData.append([category,roomNum,doorNum,doorData])
					doorList.append([roomNum,doorNum])
					
				#窓
				if category == 4:
					roomNum = data[x][1]
					windowNum = data[x][2]
					
					windowDataTemp = self._RoomControlClass.GetWindowData(roomNum)
					windowData = ['',0.0,0,0.0]
					windowData[0] = windowDataTemp[windowNum][0]
					windowData[1] = windowDataTemp[windowNum][1]
					windowData[2] = windowDataTemp[windowNum][2]
					windowData[3] = windowDataTemp[windowNum][3]
					
					self._UndoData.append([category,roomNum,windowNum,windowData])
					windowList.append([roomNum,windowNum])
					
		#アイテム
		for x in range(len(itemList)):
			item = itemList[x] - x
			self._ItemControlClass.Delete(item)
			
		#間取り
		for x in range(len(roomList)):
			room = roomList[x] - x
			self._RoomControlClass.Delete(room)
			
		#キャラクター
		for x in range(len(charaList)):
			charaNum = charaList[x] - x
			self._CharacterControlClass.Delete(charaNum)
			
		#ドア
		roomCount = self._RoomControlClass.GetRoomCount()
		delDoorList = []
		for x in range(roomCount):
			delDoorList.append([])
			
		for x in range(len(doorList)):
			doorData = doorList[x]
			delDoorList[doorData[0]].append(doorData[1])
			
		for x in range(len(delDoorList)):
			delDoorList2 = delDoorList[x]
			for y in range(len(delDoorList2)):
				doorNum = delDoorList2[y] - y
				self._RoomControlClass.DeleteDoor(x,doorNum)
				
		#窓
		roomCount = self._RoomControlClass.GetRoomCount()
		delWindowList = []
		for x in range(roomCount):
			delWindowList.append([])
			
		for x in range(len(windowList)):
			windowData = windowList[x]
			delWindowList[windowData[0]].append(windowData[1])
			
		for x in range(len(delWindowList)):
			delWindowList2 = delWindowList[x]
			for y in range(len(delWindowList2)):
				windowNum = delWindowList2[y] - y
				self._RoomControlClass.DeleteWindow(x,windowNum)
				
		if roomState:
			self.SetHole(True)
			
			#表示関係を設定
			self._RoomControlClass.SetRoomVisibleState()
			
		self._UndoClass.AddToList(self._UndoData)
		
	#ショートカットキーによる削除（追加する）
	def DeleteShortcutKey(self,data):
		self._UndoData = []
		self._UndoData.append(24)
		
		#間取り、ドア、窓を含む場合は壁を再作成
		roomState = False
		for x in range(len(data)):
			if x != 0:
				if data[x][0] == 1 or data[x][0] == 3 or data[x][0] == 4:
					roomState = True
					
		if roomState:
			self.SetHole(False)
			
		for x in range(len(data)):
			if x != 0:
				category = data[x][0]
				
				#アイテム
				if category == 0:
					itemNum 	= data[x][1]
					file		= data[x][2]
					pos			= data[x][3]
					ori			= data[x][4]
					size		= data[x][5]
					moveY		= data[x][6]
					texList		= data[x][7]
					pathPosList	= data[x][8]
					
					#print 'undoDeleteItem',itemNum
					
					newItemNum = self._ItemControlClass.Add(file)
					self._ItemControlClass.AddNewItemToList(newItemNum)
					
					self._ItemControlClass.SetPosition(newItemNum,pos)
					self._ItemControlClass.SetOrientation(newItemNum,ori)
					self._ItemControlClass.SetSize(newItemNum,size)
					self._ItemControlClass.SetMoveYState(newItemNum,moveY)
					if texList != []:
						for texData in texList:
							self._ItemControlClass.SetTexture(newItemNum,texData[0],texData[1])
					self._ItemControlClass.SetPathPositionList(newItemNum,pathPosList)

					if newItemNum != 0 and itemNum != newItemNum:
						#print 'changeID',newItemNum,itemNum
						self._ItemControlClass.ChangeId(itemNum)
						
					self._UndoData.append([category,itemNum])
					
				#間取り
				if category == 1:
					roomNum		= data[x][1]
					file		= data[x][2]
					pos			= data[x][3]
					pointData	= data[x][4]
					wallheight	= data[x][5]
					texData		= data[x][6]
					doorData	= data[x][7]
					windowData	= data[x][8]
					
					#print 'roomNum',roomNum
					#print 'file',file
					#print 'pos',pos
					#print 'pointData',pointData
					#print 'wallheight',wallheight
					#print 'texData',texData
					#print 'doorData',doorData
					#print 'windowData',windowData
					
					roomData = []
					for y in range(len(data[x])):
						if y != 0:
							roomData.append(data[x][y])
						
					newRoomNum = self._RoomControlClass.ReCreate(roomData)
					self._RoomControlClass.AddNewRoomToList(newRoomNum)
					
					if newRoomNum != 0 and roomNum != newRoomNum:
						self._RoomControlClass.ChangeId(roomNum)
						
					self._UndoData.append([category,roomNum])
					
				#キャラクター
				if category == 2:
					charaNum	= data[x][1]
					file		= data[x][2]
					pos			= data[x][3]
					ori			= data[x][4]
					pathPosList	= data[x][5]
					#print 'undoDeleteCharacter',charaNum
					
					newCharaNum = self._CharacterControlClass.Add(file)
					
					self._CharacterControlClass.SetPosition(newCharaNum,pos)
					self._CharacterControlClass.SetOrientation(newCharaNum,ori)
					self._CharacterControlClass.SetPathPositionList(newCharaNum,pathPosList)
					
					if newCharaNum != 0 and charaNum != newCharaNum:
						#print 'changeID',newCharaNum,charaNum
						self._CharacterControlClass.ChangeId(charaNum)
						
					self._UndoData.append([category,charaNum])
					
				#ドア
				if category == 3:
					roomNum		= data[x][1]
					doorNum		= data[x][2]
					doorData	= data[x][3]
					
					newDoorNum = self._RoomControlClass.AddDoor(roomNum,doorData[0],doorData[1],doorData[2])
					
					if newDoorNum != 0 and doorNum != newDoorNum:
						self._RoomControlClass.ChangeDoorId(roomNum,doorNum)
						
					if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
						self._RoomControlClass.SetDoorVisible(roomNum,False)
						
					self._UndoData.append([category,roomNum,doorNum])
					
				#窓
				if category == 4:
					roomNum		= data[x][1]
					windowNum	= data[x][2]
					windowData	= data[x][3]
					
					newWindowNum = self._RoomControlClass.AddWindow(roomNum,windowData[0],windowData[2],windowData[3],windowData[1])
					
					if newWindowNum != 0 and windowNum != newWindowNum:
						self._RoomControlClass.ChangeWindowId(roomNum,windowNum)
						
					if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
						self._RoomControlClass.SetWindowVisible(roomNum,False)
						
					self._UndoData.append([category,roomNum,windowNum])
					
		if roomState:
			self.SetHole(True)
			
			#表示関係を設定
			self._RoomControlClass.SetRoomVisibleState()
			
		self._UndoClass.AddToList(self._UndoData)
		
	#キャラクターの追加処理（削除する）
	def AddCharacter(self,data):
		self._UndoData = []
		self._UndoData.append(27)
		
		self._SelectModelClass.UnSelectCharacter()
		
		charaNum = data[1][0]
		
		preFile		= self._CharacterControlClass.GetFileName(charaNum)
		prePos		= self._CharacterControlClass.GetPosition(charaNum)
		preOri		= self._CharacterControlClass.GetOrientation(charaNum)
		pathPosList	= self._CharacterControlClass.GetPathPositionList(charaNum)
		
		self._CharacterControlClass.Delete(charaNum)
		
		self._UndoData.append([charaNum,preFile,prePos,preOri,pathPosList])
		self._UndoClass.AddToList(self._UndoData)
		
	#キャラクターの削除処理（追加する）
	def DeleteCharacter(self,data):
		self._UndoData = []
		self._UndoData.append(26)
		
		charaNum	= data[1][0]
		file		= data[1][1]
		pos			= data[1][2]
		ori			= data[1][3]
		pathPosList	= data[1][4]
		#print 'undoDeleteCharacter',charaNum
		
		newCharaNum = self._CharacterControlClass.Add(file)
		
		self._CharacterControlClass.SetPosition(newCharaNum,pos)
		self._CharacterControlClass.SetOrientation(newCharaNum,ori)
		self._CharacterControlClass.SetPathPositionList(newCharaNum,pathPosList)
		
		if newCharaNum != 0 and charaNum != newCharaNum:
			#print 'changeID',newCharaNum,charaNum
			self._CharacterControlClass.ChangeId(charaNum)
			
		self._UndoData.append([charaNum])
		self._UndoClass.AddToList(self._UndoData)
		
	#キャラクターの移動処理
	def TranslateCharacter(self,data):
		self._UndoData = []
		self._UndoData.append(28)
		
		for x in range(len(data)):
			if x != 0:
				charaNum = data[x][0]
				charaPos = data[x][1]
				
				prePos = self._CharacterControlClass.GetPosition(charaNum)
				self._UndoData.append([charaNum,prePos])
				
				self._CharacterControlClass.SetPosition(charaNum,charaPos)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#キャラクターの回転処理
	def RotateCharacter(self,data):
		self._UndoData = []
		self._UndoData.append(29)
		
		for x in range(len(data)):
			if x != 0:
				charaNum	= data[x][0]
				charaPos	= data[x][1]
				charaAngle	= data[x][2]
				
				prePos		= self._CharacterControlClass.GetPosition(charaNum)
				preAngle	= self._CharacterControlClass.GetAxisAngle(charaNum)
				self._UndoData.append([charaNum,prePos,preAngle])
				
				self._CharacterControlClass.SetPosition(charaNum,charaPos)
				self._CharacterControlClass.SetAxisAngle(charaNum,charaAngle)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#パスの追加処理（削除する）
	def AddPath(self,data):
		self._UndoData = []
		self._UndoData.append(31)
		
		self._SelectPathClass.UnSelect()
		
		charaNum = data[1][0]
		pathNum  = data[1][1]
		isItem	 = data[1][2]
		
		prePos  = self._PathControlClass.GetPosition(charaNum,pathNum,isItem)
		
		self._PathControlClass.Delete(charaNum,pathNum,isItem)
		
		self._UndoData.append([charaNum,pathNum,prePos,isItem])
		self._UndoClass.AddToList(self._UndoData)
		
	#パスの削除処理（追加する）
	def DeletePath(self,data):
		#print 'DeletePath',data
		
		self._UndoData = []
		self._UndoData.append(30)
		
		charaNum	= data[1][0]
		pathNum		= data[1][1]
		pos			= data[1][2]
		isItem  	= data[1][3]
		#print 'undoDeleteCharacter',charaNum
		#print 'undoDeletePath',pathNum
		#print 'undoDeletePos',pos
		
		newPathNum = self._PathControlClass.Add(charaNum,isItem)
		
		self._PathControlClass.SetPosition(charaNum,newPathNum,pos,isItem)
		
		if newPathNum != 0 and pathNum != newPathNum:
			#print 'changeID',newPathNum,charaNum
			self._PathControlClass.ChangeId2(charaNum,pathNum,isItem)
			
		self._UndoData.append([charaNum,pathNum,isItem])
		self._UndoClass.AddToList(self._UndoData)
		
	#パスの移動処理
	def TranslatePath(self,data):
		self._UndoData = []
		self._UndoData.append(32)
		
		charaNum = data[1][0]
		pathNum  = data[1][1]
		pathPos  = data[1][2]
		isItem 	 = data[1][3]
		
		prePos = self._PathControlClass.GetPosition(charaNum,pathNum,isItem)
		self._UndoData.append([charaNum,pathNum,prePos,isItem])
		
		self._PathControlClass.SetPosition(charaNum,pathNum,pathPos,isItem)
		
		if pathNum == 0:
			if isItem:
				self._ItemControlClass.SetPosition(charaNum,pathPos)
			else:
				self._CharacterControlClass.SetPosition(charaNum,pathPos)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#パス全体の追加処理（削除する）
	def AddAllPath(self,data):
		self._UndoData = []
		self._UndoData.append(34)
		
		self._SelectPathClass.UnSelect()
		
		charaNum = data[1][0]
		isItem = False
		if len(data[1]) == 2:
			isItem = data[1][1]
		
		pathPosList = None
		if isItem:
			pathPosList	= self._ItemControlClass.GetPathPositionList(charaNum)
			self._ItemControlClass.DeletePath(charaNum)
		else:
			pathPosList	= self._CharacterControlClass.GetPathPositionList(charaNum)
			self._CharacterControlClass.DeletePath(charaNum)
		
		self._UndoData.append([charaNum,pathPosList,isItem])
		self._UndoClass.AddToList(self._UndoData)
		
	#パス全体の削除処理（追加する）
	def DeleteAllPath(self,data):
		self._UndoData = []
		self._UndoData.append(33)
		
		charaNum	= data[1][0]
		pathPosList	= data[1][1]
		isItem = False
		if len(data[1]) == 3:
			isItem = data[1][2]
		#print 'undoDeleteCharacter',charaNum
		
		if isItem:
			self._ItemControlClass.SetPathPositionList(charaNum,pathPosList)
		else:
			self._CharacterControlClass.SetPathPositionList(charaNum,pathPosList)
		
		self._UndoData.append([charaNum,isItem])
		self._UndoClass.AddToList(self._UndoData)
		
	#壁の表示設定
	def SetWallVisible(self,data):
		self._UndoData = []
		self._UndoData.append(35)
		
		for x in range(len(data)):
			if x != 0:
				roomNum	= data[x][0]
				wallNum	= data[x][1]
				state	= data[x][2]
				
				preState = self._RoomControlClass.GetWallVisible(roomNum,wallNum)
				self._UndoData.append([roomNum,wallNum,preState])
				
				self._RoomControlClass.SetWallVisible(roomNum,wallNum,state)
				self._RoomControlClass.SetHighlightWallLine(roomNum,wallNum,False)
				
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテム、キャラクター、間取り、ドア、窓の移動
	def TranslateModel(self,data):
		self._UndoData = []
		self._UndoData.append(36)
		
		roomState = False
		for x in range(len(data)):
			if x != 0:
				if data[x][0] == 1 or data[x][0] == 3 or data[x][0] == 4:
					roomState = True
					
		if roomState:
			self.SetHole(False)
			
		for x in range(len(data)):
			if x != 0:
				category = data[x][0]
				
				#アイテム
				if category == 0:
					itemNum = data[x][1]
					itemPos = data[x][2]
					
					prePos = self._ItemControlClass.GetPosition(itemNum)
					self._UndoData.append([category,itemNum,prePos])
					
					self._ItemControlClass.SetPosition(itemNum,itemPos)
					
				#間取り
				if category == 1:
					roomNum = data[x][1]
					roomPos = data[x][2]
					
					prePos = self._RoomControlClass.GetFloorPosition(roomNum)
					self._UndoData.append([category,roomNum,prePos])
					
					self._RoomControlClass.SetFloorPosition(roomNum,roomPos)
					
				#キャラクター
				if category == 2:
					charaNum = data[x][1]
					charaPos = data[x][2]
					
					prePos = self._CharacterControlClass.GetPosition(charaNum)
					self._UndoData.append([category,charaNum,prePos])
					
					self._CharacterControlClass.SetPosition(charaNum,charaPos)
					
				#ドア
				if category == 3:
					roomNum		= data[x][1]
					doorNum		= data[x][2]
					wallNum		= data[x][3]
					percentage	= data[x][4]
					
					doorState = self._RoomControlClass.GetDoorState(roomNum,doorNum)
					self._UndoData.append([category,roomNum,doorNum,doorState[1],doorState[2]])
					
					self._RoomControlClass.SetDoorPosition(roomNum,doorNum,wallNum,percentage)
					self._RoomControlClass.UpdateDoorList(roomNum,doorNum,wallNum,percentage)
					
				#窓
				if category == 4:
					roomNum			= data[x][1]
					windowNum		= data[x][2]
					wallNum			= data[x][3]
					percentage		= data[x][4]
					
					windowState = self._RoomControlClass.GetWindowState(roomNum,windowNum)
					self._UndoData.append([category,roomNum,windowNum,windowState[2],windowState[3]])
					
					self._RoomControlClass.SetWindowPosition(roomNum,windowNum,wallNum,percentage)
					self._RoomControlClass.UpdateWindowList(roomNum,windowNum,2,0,wallNum,percentage)
					
		if roomState:
			self.SetHole(True)
			
			#表示関係を設定
			self._RoomControlClass.SetRoomVisibleState()
			
		self._UndoClass.AddToList(self._UndoData)
		
	#アイテム、キャラクターの回転処理
	def RotateModel(self,data):
		self._UndoData = []
		self._UndoData.append(37)
		
		roomState = False
		for x in range(len(data)):
			if x != 0:
				if data[x][0] == 1:
					roomState = True
					
		if roomState:
			self.SetHole(False)
			
		for x in range(len(data)):
			if x != 0:
				category = data[x][0]
				
				#アイテム
				if category == 0:
					itemNum		= data[x][1]
					itemPos		= data[x][2]
					itemAngle	= data[x][3]
					
					prePos		= self._ItemControlClass.GetPosition(itemNum)
					preAngle	= self._ItemControlClass.GetAxisAngle(itemNum)
					self._UndoData.append([category,itemNum,prePos,preAngle])
					
					self._ItemControlClass.SetPosition(itemNum,itemPos)
					self._ItemControlClass.SetAxisAngle(itemNum,itemAngle)
					
				#キャラクター
				if category == 1:
					charaNum	= data[x][1]
					charaPos	= data[x][2]
					charaAngle	= data[x][3]
					
					prePos		= self._CharacterControlClass.GetPosition(charaNum)
					preAngle	= self._CharacterControlClass.GetAxisAngle(charaNum)
					self._UndoData.append([category,charaNum,prePos,preAngle])
					
					self._CharacterControlClass.SetPosition(charaNum,charaPos)
					self._CharacterControlClass.SetAxisAngle(charaNum,charaAngle)
					
		self._UndoClass.AddToList(self._UndoData)
		
	#ダミードア、窓を設定
	def SetHole(self,isAdd):
		if self._Is3d:
			if isAdd:
				self._RoomControlClass.AddAllDoorHole()			#ダミードアを作成
				self._RoomControlClass.AddAllWindowHole()		#ダミー窓を作成
				
			else:
				self._RoomControlClass.DeleteAllDoorHole()		#ダミードアを削除
				self._RoomControlClass.DeleteAllWindowHole()	#ダミー窓を削除
				
	#表示状態設定
	def SetVisible(self,state):
		if state:
			self._Frame.visible(viz.ON)
			self._Icon.visible(viz.ON)
		else:
			self._Frame.visible(viz.OFF)
			self._Icon.visible(viz.OFF)
			
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		if state:
			self._Position[0] = self._PositionX[1]
		else:
			self._Position[0] = self._PositionX[0]
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._Position[1] = windowSize[1]-92
		
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Icon,self._Position,self._Scale)
		