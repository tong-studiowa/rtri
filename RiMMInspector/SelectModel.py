﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

#areaSelectHeightTemp = iniFile.GetItemData('Setting','AreaSelectHeight')
#areaSelectHeight = float(areaSelectHeightTemp)
areaSelectHeight = 10000.0

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

#pillarFilterTemp = iniFile.GetItemData('Setting','PillarFilter')
pillarFilterTemp = 'pillar,16,10'
pillarFilterText = pillarFilterTemp.split(',')[0]
pillarFilterStart = int(pillarFilterTemp.split(',')[1])
pillarFilterEnd = int(pillarFilterTemp.split(',')[2])

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モデル選択制御のクラス定義
class SelectModel(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectModel] : Init SelectModel')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._SelectWallClass = None
		self._CheckStateClass = None
		self._RoomControlClass = None
		self._CharacterControlClass = None
		self._StartPositionClass = None
		self._SetFileterClass = None
		self._PathControlClass = None
		self._SelectStartPositionClass = None
		self._SelectLogClass = None
		self._SelectPathClass = None
		self._ScaleItemClass = None
		
		#アイテムの選択
		self._SelectedModelList = []
		
		self._SelectedModel = None
		self._SelectedNode = None
		self._SelectedModelMoveYState = False
		
		#間取りの選択
		self._SelectedRoomList = []
		self._SelectedRoom = None
		
		#キャラクターの選択
		self._SelectedCharacterList = []
		self._SelectedCharacter = None
		
		#ドアの選択
		self._SelectedDoorList = []
		self._SelectedDoor = [None,None]
		
		#窓の選択
		self._SelectedWindowList = []
		self._SelectedWindow = [None,None]
		
		#マウス操作関係
		self._ClickState = False
		
		self._DragState = False
		self._PreDragState = False
		self._ClickOtherState = False
		
		self._DragFrame = self.CreateDragFrame()
		self._DragFramePos1X = 0.0
		self._DragFramePos1Y = 0.0
		self._DragFramePos2X = 0.0
		self._DragFramePos2Y = 0.0
		self._MousePosX = 0
		self._MousePosY = 0
		
		self._DragDistance = 0.0
		self._DragThreshold = 0.02
		
		self._AreaSelectHeight = areaSelectHeight
		
		self._Dummy = vizshape.addBox([0.01,0.01,0.01])
		self._Dummy.visible(viz.OFF)
		self._Dummy.disable(viz.PICKING)

		self._RotateSnap = rotateSnap
		self._DummyOrientation = 0.0
		
		self._OrthoViewWindow = None
		self._Is3d = False
		
		self._BaseRoom = None	#最初に選択したドア、窓の部屋番号
		
		self._FilterStateList = []	#間取り、照明、エアコン、アイテム、キャラクター
		
		self._PillarFilterText = pillarFilterText
		self._PillarFilterStart = pillarFilterStart
		self._PillarFilterEnd = pillarFilterEnd
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,stencilClass,manipulatorClass,checkStateClass,roomControlClass
				,characterControlClass,startPositionClass,setFileterClass,backgroundClass,pathControlClass
				,selectLogClass,selectPathClass,selectStartPositionClass,selectWallClass
				,scaleItemClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._CheckStateClass = checkStateClass
		self._RoomControlClass = roomControlClass
		self._CharacterControlClass = characterControlClass
		self._StartPositionClass = startPositionClass
		self._SetFileterClass = setFileterClass
		self._BackgroundClass = backgroundClass
		self._PathControlClass = pathControlClass
		self._SelectLogClass = selectLogClass
		self._SelectPathClass = selectPathClass
		self._SelectStartPositionClass = selectStartPositionClass
		self._SelectWallClass = selectWallClass
		self._ScaleItemClass = scaleItemClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectModel] : Start SelectModel Update')
		
		#フィルターの設定を更新
		self._FilterStateList = self._SetFileterClass.GetFilterStateList()
		
		#パラメータをリセット
		self._ClickState = False
		modelNum	 = self._SelectedModel
		nodeName	 = self._SelectedNode
		roomNum		 = self._SelectedRoom
		characterNum = self._SelectedCharacter
		doorNum		 = self._SelectedDoor
		windowNum	 = self._SelectedWindow
		
		#シングルクリックによる選択
		if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() == False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True:
			modelNum,nodeName,roomNum,characterNum,doorNum,windowNum = self.Select()
			#print 'select',modelNum,nodeName,roomNum,characterNum,doorNum,windowNum
			
			#マニピュレータを操作中
			if self._ManipulatorClass.GetState():
				pass
				
			#ダミーボックスのサイズ変更中
			elif self._ScaleItemClass.GetState():
				pass
				
			#選択中のモデルをクリック
			elif modelNum != None and modelNum == self._SelectedModel:
				pass
				
			#選択中の部屋をクリック
			elif roomNum != None and roomNum == self._SelectedRoom:
				pass
				
			#選択中のキャラクターをクリック
			elif characterNum != None and characterNum == self._SelectedCharacter:
				pass
				
			#選択中のドアをクリック
			elif doorNum != [None,None] and doorNum == self._SelectedDoor:
				pass
				
			#選択中の窓をクリック
			elif windowNum != [None,None] and windowNum == self._SelectedWindow:
				pass
				
			#モデルをクリックし、選択
			elif modelNum != None or roomNum != None or characterNum != None or doorNum != [None,None] or windowNum != [None,None]:
				self._ClickState = True
				
				#選択していたモデルのハイライト表示をOFF
				if self._SelectedModel != None:			#アイテム
					for x in self._SelectedModelList:
						self._ItemControlClass.SetHighlight(x,False)
					self._SelectedModelList = []
					self._SelectedModel = None
					self._SelectedNode = None
					self._SelectedModelMoveYState = False
				
				if self._SelectedRoom != None:			#間取り
					for x in self._SelectedRoomList:
						self._RoomControlClass.SetHighlightFloor(x,False)
					self._SelectedRoomList = []
					self._SelectedRoom = None
				
				if self._SelectedCharacter != None:		#キャラクター
					for x in self._SelectedCharacterList:
						self._CharacterControlClass.SetHighlight(x,False)
					self._SelectedCharacterList = []
					self._SelectedCharacter = None
					
				if self._SelectedDoor != [None,None]:		#ドア
					for doorData in self._SelectedDoorList:
						self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
					self._SelectedDoorList = []
					self._SelectedDoor = [None,None]
					
				if self._SelectedWindow != [None,None]:		#窓
					for windowData in self._SelectedWindowList:
						self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
					self._SelectedWindowList = []
					self._SelectedWindow = [None,None]
					
				#選択し、モデルをハイライト表示
				if modelNum != None:		#アイテム
					self._SelectedModelList.append(modelNum)
					self._SelectedModel = modelNum
					self._SelectedNode = nodeName
					self._SelectedModelMoveYState = self._ItemControlClass.GetMoveYState(self._SelectedModel)
					
					self._ItemControlClass.SetHighlight(self._SelectedModel,True)
					
					self._ManipulatorClass.SetArrowYVisible(self._SelectedModelMoveYState)
					self._ManipulatorClass.SetCircle2Visible(1)
					
					item = self._ItemControlClass.GetModel(self._SelectedModel)
					model = item.GetModel()
					self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
				
				elif roomNum != None:		#部屋
					self._SelectedRoomList.append(roomNum)
					self._SelectedRoom = roomNum
					
					self._RoomControlClass.SetHighlightFloor(self._SelectedRoom,True)
					
					self._ManipulatorClass.SetCircle2Visible(0)
					
					floor = self._RoomControlClass.GetFloorModel(self._SelectedRoom)
					self._ManipulatorClass.SetModel(floor,False)	#マニピュレータ設定
					
				elif characterNum != None:	#キャラクター
					self._SelectedCharacterList = []
					self._SelectedCharacterList.append(characterNum)
					self._SelectedCharacter = characterNum
					
					self._CharacterControlClass.SetHighlight(self._SelectedCharacter,True)
					
					self._ManipulatorClass.SetArrowYVisible(0)
					self._ManipulatorClass.SetCircle2Visible(0)
					
					character = self._CharacterControlClass.GetModel(self._SelectedCharacter)
					model = character.GetModel()
					self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
					
				elif doorNum != [None,None]:		#ドア
					self._SelectedDoorList.append(doorNum)
					self._SelectedDoor = doorNum
					self._BaseRoom = doorNum[0]
					
					self._RoomControlClass.SetHighlightDoor(self._SelectedDoor[0],self._SelectedDoor[1],True)
					
					self._ManipulatorClass.SetCircle2Visible(0)
					
					door = self._RoomControlClass.GetDoorModel(self._SelectedDoor[0],self._SelectedDoor[1])
					model = door.GetModel()
					self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
					
				elif windowNum != [None,None]:		#窓
					self._SelectedWindowList.append(windowNum)
					self._SelectedWindow = windowNum
					self._BaseRoom = windowNum[0]
					
					self._RoomControlClass.SetHighlightWindow(self._SelectedWindow[0],self._SelectedWindow[1],True)
					
					self._ManipulatorClass.SetCircle2Visible(0)
					
					window = self._RoomControlClass.GetWindowModel(self._SelectedWindow[0],self._SelectedWindow[1])
					model = window.GetModel()
					self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
					
			#選択中にモデル以外をクリック
			else:
				if self._SelectedModel != None or self._SelectedRoom != None or self._SelectedCharacter != None or self._SelectedDoor != [None,None] or self._SelectedWindow != [None,None]:
					self._ClickState = True
					#print 'UnSelect',modelNum,nodeName,roomNum,characterNum,doorNum,windowNum
					
					#選択を解除し、モデルのハイライト表示をOFF
					if self._SelectedModel != None:			#アイテム
						self._ItemControlClass.SetHighlight(self._SelectedModel,False)
						
						selectedItem = self._ItemControlClass.GetModel(self._SelectedModel)
						selectedModel = selectedItem.GetModel()
						
						if selectedModel == self._ManipulatorClass.GetModel():
							self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
						for x in self._SelectedModelList:
							self._ItemControlClass.SetHighlight(x,False)
						self._SelectedModelList = []
						self._SelectedModel = None
						self._SelectedNode = None
						
					if self._SelectedRoom != None:			#部屋
						self._RoomControlClass.SetHighlightFloor(self._SelectedRoom,False)
						selectedModel = self._RoomControlClass.GetFloorModel(self._SelectedRoom)
						
						if selectedModel == self._ManipulatorClass.GetModel():
							self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
						for x in self._SelectedRoomList:
							self._RoomControlClass.SetHighlightFloor(x,False)
						self._SelectedRoomList = []
						self._SelectedRoom = None
						
					if self._SelectedCharacter != None:		#キャラクター
						self._CharacterControlClass.SetHighlight(self._SelectedCharacter,False)
						
						selectedCharacter = self._CharacterControlClass.GetModel(self._SelectedCharacter)
						selectedModel = selectedCharacter.GetModel()
						
						if selectedModel == self._ManipulatorClass.GetModel():
							self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
						for x in self._SelectedCharacterList:
							self._CharacterControlClass.SetHighlight(x,False)
						self._SelectedCharacterList = []
						self._SelectedCharacter = None
						
					if self._SelectedDoor != [None,None]:			#ドア
						self._RoomControlClass.SetHighlightDoor(self._SelectedDoor[0],self._SelectedDoor[1],False)
						
						selectedDoor = self._RoomControlClass.GetDoorModel(self._SelectedDoor[0],self._SelectedDoor[1])
						selectedModel = selectedDoor.GetModel()
						
						if selectedModel == self._ManipulatorClass.GetModel():
							self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
						for doorData in self._SelectedDoorList:
							self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
						self._SelectedDoorList = []
						self._SelectedDoor = [None,None]
						self._BaseRoom = None
						
					if self._SelectedWindow != [None,None]:			#窓
						self._RoomControlClass.SetHighlightWindow(self._SelectedWindow[0],self._SelectedWindow[1],False)
						
						selectedWindow = self._RoomControlClass.GetWindowModel(self._SelectedWindow[0],self._SelectedWindow[1])
						selectedModel = selectedWindow.GetModel()
						
						if selectedModel == self._ManipulatorClass.GetModel():
							self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
						for windowData in self._SelectedWindowList:
							self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
						self._SelectedWindowList = []
						self._SelectedWindow = [None,None]
						self._BaseRoom = None
						
					if self._Dummy == self._ManipulatorClass.GetModel():
						self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
							
				elif self._SelectedModelList != [] or self._SelectedRoomList != [] or self._SelectedCharacterList != [] or self._SelectedDoorList != [] or self._SelectedWindowList != []:
					for x in self._SelectedModelList:
						self._ItemControlClass.SetHighlight(x,False)
					self._SelectedModelList = []
					
					for x in self._SelectedRoomList:
						self._RoomControlClass.SetHighlightFloor(x,False)
					self._SelectedRoomList = []
					
					for x in self._SelectedCharacterList:
						self._CharacterControlClass.SetHighlight(x,False)
					self._SelectedCharacterList = []
					
					for doorData in self._SelectedDoorList:
						self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
					self._SelectedDoorList = []
					
					for windowData in self._SelectedWindowList:
						self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
					self._SelectedWindowList = []
					
		#Shift+クリックで追加選択、選択解除
		elif self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() == True and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True:
			if self._DragDistance < self._DragThreshold:
				modelNum,nodeName,roomNum,characterNum,doorNum,windowNum = self.Select(True)
				
				#アイテム
				newSelectedModelList = []
				if modelNum != None:
					state = False
					for x in range(len(self._SelectedModelList)):
						if self._SelectedModelList[x] == modelNum:
							state = True
							
							#選択解除
							for x in range(len(self._SelectedModelList)):
								if self._SelectedModelList[x] == modelNum:
									self._ItemControlClass.SetHighlight(modelNum,False)	#選択を解除
								else:
									newSelectedModelList.append(self._SelectedModelList[x])
									
					if state:
						self._SelectedModelList = newSelectedModelList
						
					else:		#追加選択
						newSelectedModelList = []
						state = False
						for x in range(len(self._SelectedModelList)):
							if self._SelectedModelList[x] > modelNum and state == False:
								state = True
								newSelectedModelList.append(modelNum)
								
							newSelectedModelList.append(self._SelectedModelList[x])
							
						if state == False:
							newSelectedModelList.append(modelNum)
							
						self._SelectedModelList = newSelectedModelList
						
						self._ItemControlClass.SetHighlight(modelNum,True)	#選択
						
				#間取り
				newSelectedRoomList = []
				if roomNum != None:
					state = False
					for x in range(len(self._SelectedRoomList)):
						if self._SelectedRoomList[x] == roomNum:
							state = True
							
							#選択解除
							for x in range(len(self._SelectedRoomList)):
								if self._SelectedRoomList[x] == roomNum:
									self._RoomControlClass.SetHighlightFloor(roomNum,False)	#選択を解除
								else:
									newSelectedRoomList.append(self._SelectedRoomList[x])
									
					if state:
						self._SelectedRoomList = newSelectedRoomList
						
					else:		#追加選択
						newSelectedRoomList = []
						state = False
						for x in range(len(self._SelectedRoomList)):
							if self._SelectedRoomList[x] > roomNum and state == False:
								state = True
								newSelectedRoomList.append(roomNum)
								
							newSelectedRoomList.append(self._SelectedRoomList[x])
							
						if state == False:
							newSelectedRoomList.append(roomNum)
							
						self._SelectedRoomList = newSelectedRoomList
						
						self._RoomControlClass.SetHighlightFloor(roomNum,True)	#選択
						
				#キャラクター
				newSelectedCharacterList = []
				if characterNum != None:
					state = False
					for x in range(len(self._SelectedCharacterList)):
						if self._SelectedCharacterList[x] == characterNum:
							state = True
							
							#選択解除
							for x in range(len(self._SelectedCharacterList)):
								if self._SelectedCharacterList[x] == characterNum:
									self._CharacterControlClass.SetHighlight(characterNum,False)	#選択を解除
								else:
									newSelectedCharacterList.append(self._SelectedCharacterList[x])
									
					if state:
						self._SelectedCharacterList = newSelectedCharacterList
						
					else:		#追加選択
						newSelectedCharacterList = []
						state = False
						for x in range(len(self._SelectedCharacterList)):
							if self._SelectedCharacterList[x] > characterNum and state == False:
								state = True
								newSelectedCharacterList.append(characterNum)
						
							newSelectedCharacterList.append(self._SelectedCharacterList[x])
						
						if state == False:
							newSelectedCharacterList.append(characterNum)
							
						self._SelectedCharacterList = newSelectedCharacterList
						
						self._CharacterControlClass.SetHighlight(characterNum,True)	#選択
						
				#ドア
				newSelectedDoorList = []
				if doorNum != [None,None]:
					state = False
					for x in range(len(self._SelectedDoorList)):
						if self._SelectedDoorList[x] == doorNum:
							state = True
							
							#選択解除
							for x in range(len(self._SelectedDoorList)):
								if self._SelectedDoorList[x] == doorNum:
									self._RoomControlClass.SetHighlightDoor(doorNum[0],doorNum[1],False)	#選択を解除
								else:
									newSelectedDoorList.append(self._SelectedDoorList[x])
									
					if state:
						self._SelectedDoorList = newSelectedDoorList
						
					else:		#追加選択
						newSelectedDoorList = []
						state = False
						for x in range(len(self._SelectedDoorList)):
							doorData = self._SelectedDoorList[x]
							if doorData[0] > doorNum[0] and doorData[1] > doorNum[1] and state == False:
								state = True
								newSelectedDoorList.append(doorNum)
								
							newSelectedDoorList.append(self._SelectedDoorList[x])
							
						if state == False:
							newSelectedDoorList.append(doorNum)
							
						self._SelectedDoorList = newSelectedDoorList
						
						self._RoomControlClass.SetHighlightDoor(doorNum[0],doorNum[1],True)	#選択
						
				#窓
				newSelectedWindowList = []
				if windowNum != [None,None]:
					state = False
					for x in range(len(self._SelectedWindowList)):
						if self._SelectedWindowList[x] == windowNum:
							state = True
							
							#選択解除
							for x in range(len(self._SelectedWindowList)):
								if self._SelectedWindowList[x] == windowNum:
									self._RoomControlClass.SetHighlightWindow(windowNum[0],windowNum[1],False)	#選択を解除
								else:
									newSelectedWindowList.append(self._SelectedWindowList[x])
									
					if state:
						self._SelectedWindowList = newSelectedWindowList
						
					else:		#追加選択
						newSelectedWindowList = []
						state = False
						for x in range(len(self._SelectedWindowList)):
							windowData = self._SelectedWindowList[x]
							if windowData[0] > windowNum[0] and windowData[1] > windowNum[1] and state == False:
								state = True
								newSelectedWindowList.append(windowNum)
								
							newSelectedWindowList.append(self._SelectedWindowList[x])
							
						if state == False:
							newSelectedWindowList.append(windowNum)
							
						self._SelectedWindowList = newSelectedWindowList
						
						self._RoomControlClass.SetHighlightWindow(windowNum[0],windowNum[1],True)	#選択
						
				#マニピュレータを設定
				if len(self._SelectedModelList) != 0 or len(self._SelectedRoomList) != 0 or len(self._SelectedCharacterList) != 0 or len(self._SelectedDoorList) != 0 or len(self._SelectedWindowList) != 0:	#1つ以上選択
					moveYState = 1
					
					if len(self._SelectedModelList) != 0:
						self._SelectedModel = self._SelectedModelList[0]
						self._SelectedNode = None
						self._BaseRoom = None
						
						moveYState = 1
						for itemNum in self._SelectedModelList:
							itemYstate = self._ItemControlClass.GetMoveYState(itemNum)
							if itemYstate == 0:
								moveYState = 0
								
					if len(self._SelectedRoomList) != 0:
						self._SelectedRoom = self._SelectedRoomList[0]
						
					if len(self._SelectedCharacterList) != 0:
						self._SelectedCharacter = self._SelectedCharacterList[0]
						moveYState = 0
						
					if len(self._SelectedDoorList) != 0:
						self._SelectedDoor = self._SelectedDoorList[0]
						
					if len(self._SelectedWindowList) != 0:
						self._SelectedWindow = self._SelectedWindowList[0]
						
					self._ManipulatorClass.SetArrowYVisible(moveYState)
					self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
					
				else:	#未選択
					self._SelectedModel = None
					self._SelectedNode = None
					self._SelectedRoom = None
					self._SelectedCharacter = None
					self._SelectedDoor = [None,None]
					self._SelectedWindow = [None,None]
					self._BaseRoom = None
					
					self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
					
		#ステンシルの操作で選択解除
		if self._StencilClass.GetClickState():
			if self._SelectedModel != None or self._SelectedRoom != None or self._SelectedCharacter != None or self._SelectedDoor == [None,None] or self._SelectedWindow == [None,None]:
				#選択を解除し、モデルのハイライト表示をOFF
				if self._SelectedModel != None:			#アイテム
					self._ItemControlClass.SetHighlight(self._SelectedModel,False)
					for x in self._SelectedModelList:
						self._ItemControlClass.SetHighlight(x,False)
					self._SelectedModelList = []
					self._SelectedModel = None
					self._SelectedNode = None
					
				if self._SelectedRoom != None:			#部屋
					self._RoomControlClass.SetHighlightFloor(self._SelectedRoom,False)
					for x in self._SelectedRoomList:
						self._RoomControlClass.SetHighlightFloor(x,False)
					self._SelectedRoomList = []
					self._SelectedRoom = None
					
				if self._SelectedCharacter != None:		#キャラクター
					self._CharacterControlClass.SetHighlight(self._SelectedCharacter,False)
					for x in self._SelectedCharacterList:
						self._CharacterControlClass.SetHighlight(x,False)
					self._SelectedCharacterList = []
					self._SelectedCharacter = None
					
				if self._SelectedDoor != [None,None]:			#ドア
					self._RoomControlClass.SetHighlightDoor(self._SelectedDoor[0],self._SelectedDoor[1],False)
					for doorData in self._SelectedDoorList:
						self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
					self._SelectedDoorList = []
					self._SelectedDoor = [None,None]
					self._BaseRoom = None
					
				if self._SelectedWindow != [None,None]:			#窓
					self._RoomControlClass.SetHighlightWindow(self._SelectedWindow[0],self._SelectedWindow[1],False)
					for windowData in self._SelectedWindowList:
						self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
					self._SelectedWindowList = []
					self._SelectedWindow = [None,None]
					self._BaseRoom = None
					
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
		#Shift+マウス左ドラッグによる複数選択
		mousePos = self._InputClass.GetMousePosition()
		self._MousePosX = mousePos[0]
		self._MousePosY = mousePos[1]
		
		if self._InputClass.GetMouseStartDragState():
			if self._CheckStateClass.GetOverState():
				if self._DragState == False:
					self._ClickOtherState = True
			
			elif self._InputClass.GetShiftKeyState() and self._DragState == False and self._ClickOtherState == False:
				#print 'ドラッグ開始'
				self._DragState = True
				self.SetDragFrameVisible(True)
				
				x = self._MousePosX
				self._DragFramePos1X = x
				y = self._MousePosY
				self._DragFramePos1Y = y
				
				self._BaseRoom = None
					
		if self._DragState == True:	#ドラッグ中
			#print 'ドラッグ中'
			x = self._MousePosX
			self._DragFramePos2X = x
			y = self._MousePosY
			self._DragFramePos2Y = y
			
			framePosX = (self._DragFramePos1X + self._DragFramePos2X) * 0.5
			framePosY = (self._DragFramePos1Y + self._DragFramePos2Y) * 0.5
			
			self._DragFrame.setPosition(framePosX,framePosY)
			
			frameScaleX = self._DragFramePos2X - self._DragFramePos1X
			if self._DragFramePos2X < self._DragFramePos1X:
				frameScaleX = self._DragFramePos1X - self._DragFramePos2X
				
			frameScaleY = self._DragFramePos2Y - self._DragFramePos1Y
			if self._DragFramePos2Y < self._DragFramePos1Y:
				frameScaleY = self._DragFramePos1Y - self._DragFramePos2Y
			
			self._DragFrame.setScale(frameScaleX*12.8,frameScaleY*10.25)
			
			self._DragDistance = vizmat.Distance([self._DragFramePos1X,0,self._DragFramePos1Y],[self._DragFramePos2X,0,self._DragFramePos2Y])
			#print 'dist',self._DragDistance
			
			if self._DragDistance > self._DragThreshold:
				#アイテムの選択
				self._SelectedModelList = []
				itemCount = self._ItemControlClass.GetItemCount()
				
				if self._FilterStateList[1] or self._FilterStateList[2] or self._FilterStateList[3] or self._FilterStateList[4]:
					for x in range(itemCount):
						item = self._ItemControlClass.GetModel(x)
						model = item.GetModel()
						
						modelPos3d = model.getPosition(viz.ABS_GLOBAL)
						
						modelPos = None
						if self._Is3d == True:
							modelPos = viz.MainWindow.worldToScreen( modelPos3d )
						else:
							modelPos = self._OrthoViewWindow.worldToScreen( modelPos3d )
						
						xRes = False
						yRes = False
						
						if modelPos3d[1] < self._AreaSelectHeight:	#高いところのは選択しない
							if self._DragFramePos2X > self._DragFramePos1X:
								if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
									xRes = True
							else:
								if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
									xRes = True
							
							if self._DragFramePos2Y > self._DragFramePos1Y:
								if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
									yRes = True
							else:
								if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
									yRes = True
						
						#フィルター設定
						filterState = False
						itemName = self._ItemControlClass.GetFileName(x)
						if itemName[-self._PillarFilterStart:-self._PillarFilterEnd] == self._PillarFilterText:	#柱
							if self._FilterStateList[1]:
								filterState = True
						elif itemName[-23:-10] == 'ceiling_light':		#照明
							if self._FilterStateList[2]:
								filterState = True
						elif itemName[-22:-10] == 'aircondition':		#エアコン
							if self._FilterStateList[3]:
								filterState = True
						else:
							if self._FilterStateList[4]:
								filterState = True
						
						if xRes and yRes and filterState:	#選択した
							self._SelectedModelList.append(x)
							if self._ItemControlClass.GetHighlightState(x) == False:
								self._ItemControlClass.SetHighlight(x,True)
							
						else:	#選択していない
							if self._ItemControlClass.GetHighlightState(x) == True:
								self._ItemControlClass.SetHighlight(x,False)
				
				else:
					for x in range(itemCount):
						if self._ItemControlClass.GetHighlightState(x) == True:
							self._ItemControlClass.SetHighlight(x,False)
					
				#部屋の選択
				self._SelectedRoomList = []
				roomCount = self._RoomControlClass.GetRoomCount()
				
				if self._Is3d ==False and self._FilterStateList[0]:
					for x in range(roomCount):
						res = False
						floorPosition = self._RoomControlClass.GetFloorPosition(x)
						
						#部屋の中心が含まれるか確認
						modelPos = self._OrthoViewWindow.worldToScreen( floorPosition )
						
						xRes = False
						yRes = False
						
						if self._DragFramePos2X > self._DragFramePos1X:
							if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
								xRes = True
						else:
							if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
								xRes = True
						
						if self._DragFramePos2Y > self._DragFramePos1Y:
							if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
								yRes = True
						else:
							if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
								yRes = True
								
						if xRes and yRes:	#選択した
							res = True
						
						if res == False:
							#部屋の頂点が含まれるか確認
							room = self._RoomControlClass.GetRoom(x)
							floorPointList = room.GetWallPointList()
							
							for y in range(len(floorPointList)):
								floorPoint = floorPointList[y]
								
								pos3d = [0,0,0]
								pos3d[0] = floorPoint[0] + floorPosition[0]
								pos3d[2] = floorPoint[1] + floorPosition[2]
								
								modelPos = self._OrthoViewWindow.worldToScreen( pos3d )
								
								xRes = False
								yRes = False
								
								if self._DragFramePos2X > self._DragFramePos1X:
									if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
										xRes = True
								else:
									if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
										xRes = True
								
								if self._DragFramePos2Y > self._DragFramePos1Y:
									if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
										yRes = True
								else:
									if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
										yRes = True
										
								if xRes and yRes:	#頂点が入っている
									res = True
									break
						
						if res:	#選択した
							self._SelectedRoomList.append(x)
							if self._RoomControlClass.GetHighlightFloorState(x) == False:
								self._RoomControlClass.SetHighlightFloor(x,True)
							
						else:	#選択していない
							if self._RoomControlClass.GetHighlightFloorState(x) == True:
								self._RoomControlClass.SetHighlightFloor(x,False)
						
				else:
					for x in range(roomCount):
						if self._RoomControlClass.GetHighlightFloorState(x) == True:
							self._RoomControlClass.SetHighlightFloor(x,False)
							
				#キャラクターの選択
				self._SelectedCharacterList = []
				characterCount = self._CharacterControlClass.GetCharacterCount()
				
				if self._FilterStateList[5]:
					for x in range(characterCount):
						character = self._CharacterControlClass.GetModel(x)
						model = character.GetModel()
						
						modelPos3d = model.getPosition()
						
						modelPos = None
						if self._Is3d == True:
							modelPos = viz.MainWindow.worldToScreen( modelPos3d )
						else:
							modelPos = self._OrthoViewWindow.worldToScreen( modelPos3d )
						
						xRes = False
						yRes = False
						
						if self._DragFramePos2X > self._DragFramePos1X:
							if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
								xRes = True
						else:
							if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
								xRes = True
						
						if self._DragFramePos2Y > self._DragFramePos1Y:
							if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
								yRes = True
						else:
							if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
								yRes = True
						
						if xRes and yRes:	#選択した
							self._SelectedCharacterList.append(x)
							if self._CharacterControlClass.GetHighlightState(x) == False:
								self._CharacterControlClass.SetHighlight(x,True)
							
						else:	#選択していない
							if self._CharacterControlClass.GetHighlightState(x) == True:
								self._CharacterControlClass.SetHighlight(x,False)
								
				else:
					for x in range(characterCount):
						if self._CharacterControlClass.GetHighlightState(x) == True:
							self._CharacterControlClass.SetHighlight(x,False)
							
				#ドアの選択
				self._SelectedDoorList = []
				roomCount = self._RoomControlClass.GetRoomCount()
				
				if self._Is3d ==False and self._FilterStateList[6]:
					for x in range(roomCount):
						doorConnt = self._RoomControlClass.GetDoorCount(x)
						floorPosition = self._RoomControlClass.GetFloorPosition(x)
						
						for y in range(doorConnt):
							modelPos3d = self._RoomControlClass.GetDoorPosition(x,y)
							
							modelPos = self._OrthoViewWindow.worldToScreen( modelPos3d )
							
							xRes = False
							yRes = False
							
							if self._DragFramePos2X > self._DragFramePos1X:
								if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
									xRes = True
							else:
								if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
									xRes = True
							
							if self._DragFramePos2Y > self._DragFramePos1Y:
								if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
									yRes = True
							else:
								if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
									yRes = True
							
							if xRes and yRes:	#選択した
								self._SelectedDoorList.append([x,y])
								if self._RoomControlClass.GetHighlightDoorState(x,y) == False:
									self._RoomControlClass.SetHighlightDoor(x,y,True)
									if self._BaseRoom == None:
										self._BaseRoom = x
										
							else:	#選択していない
								if self._RoomControlClass.GetHighlightDoorState(x,y) == True:
									self._RoomControlClass.SetHighlightDoor(x,y,False)
									
				else:
					for x in range(roomCount):
						doorConnt = self._RoomControlClass.GetDoorCount(x)
						for y in range(doorConnt):
							if self._RoomControlClass.GetHighlightDoorState(x,y) == True:
								self._RoomControlClass.SetHighlightDoor(x,y,False)
								
				#窓の選択
				self._SelectedWindowList = []
				roomCount = self._RoomControlClass.GetRoomCount()
				
				if self._Is3d ==False and self._FilterStateList[7]:
					for x in range(roomCount):
						windowConnt = self._RoomControlClass.GetWindowCount(x)
						floorPosition = self._RoomControlClass.GetFloorPosition(x)
						
						for y in range(windowConnt):
							modelPos3d = self._RoomControlClass.GetWindowPosition(x,y)
							
							modelPos = self._OrthoViewWindow.worldToScreen( modelPos3d )
							
							xRes = False
							yRes = False
							
							if self._DragFramePos2X > self._DragFramePos1X:
								if modelPos[0] < self._DragFramePos2X and modelPos[0] > self._DragFramePos1X:
									xRes = True
							else:
								if modelPos[0] < self._DragFramePos1X and modelPos[0] > self._DragFramePos2X:
									xRes = True
							
							if self._DragFramePos2Y > self._DragFramePos1Y:
								if modelPos[1] < self._DragFramePos2Y and modelPos[1] > self._DragFramePos1Y:
									yRes = True
							else:
								if modelPos[1] < self._DragFramePos1Y and modelPos[1] > self._DragFramePos2Y:
									yRes = True
							
							if xRes and yRes:	#選択した
								self._SelectedWindowList.append([x,y])
								if self._RoomControlClass.GetHighlightWindowState(x,y) == False:
									self._RoomControlClass.SetHighlightWindow(x,y,True)
								
									if self._BaseRoom == None:
										self._BaseRoom = x
										
							else:	#選択していない
								if self._RoomControlClass.GetHighlightWindowState(x,y) == True:
									self._RoomControlClass.SetHighlightWindow(x,y,False)
									
				else:
					for x in range(roomCount):
						windowConnt = self._RoomControlClass.GetWindowCount(x)
						for y in range(windowConnt):
							if self._RoomControlClass.GetHighlightWindowState(x,y) == True:
								self._RoomControlClass.SetHighlightWindow(x,y,False)
								
				#マニピュレータの設定など
				if len(self._SelectedModelList) != 0 or len(self._SelectedRoomList) != 0 or len(self._SelectedCharacterList) != 0 or len(self._SelectedDoorList) != 0 or len(self._SelectedWindowList) != 0:	#1つ以上選択
					moveYState = 1
					
					if len(self._SelectedModelList) != 0:
						self._SelectedModel = self._SelectedModelList[0]
						self._SelectedNode = None
						
						moveYState = 1
						for itemNum in self._SelectedModelList:
							itemYstate = self._ItemControlClass.GetMoveYState(itemNum)
							if itemYstate == 0:
								moveYState = 0
								
					if len(self._SelectedRoomList) != 0:
						self._SelectedRoom = self._SelectedRoomList[0]
							
					if len(self._SelectedCharacterList) != 0:
						self._SelectedCharacter = self._SelectedCharacterList[0]
						moveYState = 0
						
					if len(self._SelectedDoorList) != 0:
						self._SelectedDoor = self._SelectedDoorList[0]
							
					if len(self._SelectedWindowList) != 0:
						self._SelectedWindow = self._SelectedWindowList[0]
							
					self._ManipulatorClass.SetArrowYVisible(moveYState)
					self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
					
				else:	#未選択
					self._SelectedModel		= None
					self._SelectedNode		= None
					self._SelectedRoom		= None
					self._SelectedCharacter	= None
					self._SelectedDoor		= [None,None]
					self._SelectedWindow	= [None,None]
					self._BaseRoom = None
					
					self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
					
		if self._InputClass.GetMouseDragState() == False:
			self._ClickOtherState = False
			
			if self._DragState == True:
				#print 'ドラッグ終了'
				self._DragState = False
				self.SetDragFrameVisible(False)
				
		if len(self._SelectedModelList) != 0 or len(self._SelectedRoomList) != 0 or len(self._SelectedCharacterList) != 0 or len(self._SelectedDoorList) != 0 or len(self._SelectedWindowList) != 0:	#ダミーの位置を設定
			dummyPos = [0,0,0]
			
			#アイテム
			itemCount = len(self._SelectedModelList)
			for itemNum in self._SelectedModelList:
				itemPos = self._ItemControlClass.GetPosition(itemNum)
				dummyPos[0] = dummyPos[0] + itemPos[0]
				dummyPos[1] = dummyPos[1] + itemPos[1]
				dummyPos[2] = dummyPos[2] + itemPos[2]
				
			#部屋
			roomCount = len(self._SelectedRoomList)
			for roomNum in self._SelectedRoomList:
				roomPos = self._RoomControlClass.GetFloorPosition(roomNum)
				dummyPos[0] = dummyPos[0] + roomPos[0]
				dummyPos[1] = dummyPos[1] + roomPos[1]
				dummyPos[2] = dummyPos[2] + roomPos[2]
				
			#キャラクター
			characterCount = len(self._SelectedCharacterList)
			for characterNum in self._SelectedCharacterList:
				characterPos = self._CharacterControlClass.GetPosition(characterNum)
				dummyPos[0] = dummyPos[0] + characterPos[0]
				dummyPos[1] = dummyPos[1] + characterPos[1]
				dummyPos[2] = dummyPos[2] + characterPos[2]
				
			#ドア
			doorCount = len(self._SelectedDoorList)
			for doorData in self._SelectedDoorList:
				doorPos = self._RoomControlClass.GetDoorPosition(doorData[0],doorData[1])
				dummyPos[0] = dummyPos[0] + doorPos[0] 
				dummyPos[1] = dummyPos[1] + doorPos[1] 
				dummyPos[2] = dummyPos[2] + doorPos[2] 
				
			#窓
			windowCount = len(self._SelectedWindowList)
			for windowData in self._SelectedWindowList:
				windowPos = self._RoomControlClass.GetWindowPosition(windowData[0],windowData[1])
				dummyPos[0] = dummyPos[0] + windowPos[0] 
				dummyPos[1] = dummyPos[1] + windowPos[1] 
				dummyPos[2] = dummyPos[2] + windowPos[2] 
				
			dummyPos[0] = dummyPos[0] / (itemCount+roomCount+characterCount+doorCount+windowCount)
			dummyPos[1] = dummyPos[1] / (itemCount+roomCount+characterCount+doorCount+windowCount)
			dummyPos[2] = dummyPos[2] / (itemCount+roomCount+characterCount+doorCount+windowCount)
			
			self._Dummy.setPosition(dummyPos)
			
		self._PreDragState = self._DragState
		
		#print 'SelectedModelList=',self._SelectedModelList
		#print 'SelectedModel=',self._SelectedModel
		#print 'SelectedNode=',self._SelectedNode
		#print 'SelectedRoomList=',self._SelectedRoomList
		#print 'SelectedRoom=',self._SelectedRoom
		
	#選択
	def Select(self,shift=False):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		modelNum	 = None
		nodeName	 = None
		roomNum		 = None
		characterNum = None
		doorData	 = [None,None]
		windowData	 = [None,None]
		
		self._BackgroundClass.SetPickable(False)
		self._ItemControlClass.SetPickableForAllModel(True)
		self._RoomControlClass.SetPickableForAllModel(True)
		self._CharacterControlClass.SetPickableForAllModel(True)
		self._StartPositionClass.SetPickable(True)
		self._PathControlClass.SetPickableForAllPath(True)
		
		if shift:
			self._ManipulatorClass.SetPickable(False)
			
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		
		self._BackgroundClass.SetPickable(True)
		self._ItemControlClass.SetPickableForAllModel(False)
		self._RoomControlClass.SetPickableForAllModel(False)
		self._CharacterControlClass.SetPickableForAllModel(False)
		self._StartPositionClass.SetPickable(False)
		self._PathControlClass.SetPickableForAllPath(False)
		
		if shift:
			self._ManipulatorClass.SetPickable(True)
			
		if state:
			modelNum = self._ItemControlClass.GetModelNum(pickedModel)
			
			if modelNum != None:
				nodeName = pickedModelName
				
			if self._Is3d == False:
				roomNum = self._RoomControlClass.GetRoomNum(pickedModel)
				doorData[0],doorData[1]	= self._RoomControlClass.GetDoorNum(pickedModel)
				windowData[0],windowData[1]	= self._RoomControlClass.GetWindowNum(pickedModel)
			
			characterNum = self._CharacterControlClass.GetModelNum(pickedModel)
			
		return modelNum,nodeName,roomNum,characterNum,doorData,windowData
	
	#全モデルの選択解除
	def UnSelect(self):
		self.UnSelectItem()
		self.UnSelectRoom()
		self.UnSelectCharacter()
		self.UnSelectDoor()
		self.UnSelectWindow()
		
	#アイテムの選択解除
	def UnSelectItem(self):
		if self._SelectedModelList != []:
			for itemNum in self._SelectedModelList:
				self._ItemControlClass.SetHighlight(itemNum,False)
			self._SelectedModelList = []
			self._SelectedModel = None
			self._SelectedNode = None
		
			if self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
				
	#アイテムの選択解除2
	def UnSelectItem2(self):
		self._ItemControlClass.SetHighlight(self._SelectedModel,False)
		self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
		
	#間取りの選択解除
	def UnSelectRoom(self):
		if self._SelectedRoomList != []:
			for roomNum in self._SelectedRoomList:
				self._RoomControlClass.SetHighlightFloor(roomNum,False)
			self._SelectedRoomList = []
			self._SelectedRoom = None
			
			if self._SelectedModelList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
	#キャラクターの選択解除
	def UnSelectCharacter(self):
		if self._SelectedCharacterList != []:
			for characterNum in self._SelectedCharacterList:
				self._CharacterControlClass.SetHighlight(characterNum,False)
			self._SelectedCharacterList = []
			self._SelectedCharacter = None
			
			if self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
				
	#キャラクターの選択解除2
	def UnSelectCharacter2(self):
		self._CharacterControlClass.SetHighlight(self._SelectedCharacter,False)
		self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
		
	#ドアの選択解除
	def UnSelectDoor(self):
		if self._SelectedDoorList != []:
			for doorData in self._SelectedDoorList:
				self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
			self._SelectedDoorList = []
			self._SelectedDoor = [None,None]
			
			if self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedWindowList == []:
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
				
	#窓の選択解除
	def UnSelectWindow(self):
		if self._SelectedWindowList != []:
			for windowData in self._SelectedWindowList:
				self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
			self._SelectedWindowList = []
			self._SelectedWindow = [None,None]
			
			if self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == []:
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
				
	#アイテムの再選択
	def ReSelect(self,list):
		if self._SelectedModelList != []:
			for itemNum in self._SelectedModelList:
				self._ItemControlClass.SetHighlight(itemNum,False)
			self._SelectedModelList = []
			
		self._SelectedModel = list[0]
		for itemNum in list:
			self._ItemControlClass.SetHighlight(itemNum,True)
			self._SelectedModelList.append(itemNum)
			
		if len(self._SelectedModelList) == 1 and self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
			item = self._ItemControlClass.GetModel(self._SelectedModel)
			model = item.GetModel()
			self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
		
		else:
			self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
			
	#間取りの再選択
	def ReSelectRoom(self,list):
		if self._SelectedRoomList != []:
			for roomNum in self._SelectedRoomList:
				self._RoomControlClass.SetHighlightFloor(roomNum,False)
			self._SelectedRoomList = []
			
		self._SelectedRoom = list[0]
		for roomNum in list:
			self._RoomControlClass.SetHighlightFloor(roomNum,True)
			self._SelectedRoomList.append(roomNum)
			
		if len(self._SelectedRoomList) == 1 and self._SelectedModelList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
			room = self._RoomControlClass.GetRoom(self._SelectedRoom)
			floorModel = room.GetFloorModel()
			self._ManipulatorClass.SetModel(floorModel,False)	#マニピュレータ設定
			
		else:
			self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
			
	#キャラクターの再選択
	def ReSelectCharacter(self,list):
		if self._SelectedCharacterList != []:
			for roomNum in self._SelectedCharacterList:
				self._CharacterControlClass.SetHighlight(roomNum,False)
			self._SelectedCharacterList = []
			
		self._SelectedCharacter = list[0]
		for characterNum in list:
			self._CharacterControlClass.SetHighlight(characterNum,True)
			self._SelectedCharacterList.append(characterNum)
			
		if len(self._SelectedCharacterList) == 1 and self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedDoorList == [] and self._SelectedWindowList == []:
			character = self._CharacterControlClass.GetModel(self._SelectedCharacter)
			model = character.GetModel()
			self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
			
		else:
			self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
			
	#ドアの再選択
	def ReSelectDoor(self,list):
		if self._SelectedDoorList != []:
			for doorData in self._SelectedDoorList:
				self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],False)
			self._SelectedDoorList = []
			
		self._SelectedDoor = list[0]
		for doorData in list:
			self._RoomControlClass.SetHighlightDoor(doorData[0],doorData[1],True)
			self._SelectedDoorList.append(doorData)
			
		if len(self._SelectedDoorList) == 1 and self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedWindowList == []:
			door = self._RoomControlClass.GetDoorModel(self._SelectedDoor[0],self._SelectedDoor[1])
			model = door.GetModel()
					
			self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
			
		else:
			self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
			
	#窓の再選択
	def ReSelectWindow(self,list):
		if self._SelectedWindowList != []:
			for windowData in self._SelectedWindowList:
				self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],False)
			self._SelectedWindowList = []
			
		self._SelectedWindow = list[0]
		for windowData in list:
			self._RoomControlClass.SetHighlightWindow(windowData[0],windowData[1],True)
			self._SelectedWindowList.append(windowData)
			
		if len(self._SelectedWindowList) == 1 and self._SelectedModelList == [] and self._SelectedRoomList == [] and self._SelectedCharacterList == [] and self._SelectedDoorList == []:
			window = self._RoomControlClass.GetWindowModel(self._SelectedWindow[0],self._SelectedWindow[1])
			model = window.GetModel()
					
			self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
			
		else:
			self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
			
	#選択しているアイテムの取得
	def GetSelectedModel(self):
		selectedModel = self._SelectedModel
		return selectedModel
		
	#選択しているノードの取得
	def GetSelectedNode(self):
		selectedNode = self._SelectedNode
		return selectedNode
		
	#選択している部屋の取得
	def GetSelectedRoom(self):
		selectedRoom = self._SelectedRoom
		return selectedRoom
		
	#選択しているキャラクターの取得
	def GetSelectedCharacter(self):
		selectedCharacter = self._SelectedCharacter
		return selectedCharacter
		
	#選択しているドアの取得
	def GetSelectedDoor(self):
		selectedDoor = self._SelectedDoor
		return selectedDoor[0],selectedDoor[1]
		
	#選択している窓の取得
	def GetSelectedWindow(self):
		selectedWindow = self._SelectedWindow
		return selectedWindow[0],selectedWindow[1]
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
	
	#フレームの作成
	def CreateDragFrame(self):
		frame = viz.addTexQuad(parent=viz.SCREEN)
		frame.setScale(1,1)
		frame.color(viz.BLUE)
		frame.drawOrder(1)
		frame.alpha(0.2)
		frame.visible(viz.OFF)
		
		return frame
		
	#フレームの表示状態の設定
	def SetDragFrameVisible(self,state):
		if state:
			self._DragFrame.visible(viz.ON)
		else:
			self._DragFrame.visible(viz.OFF)
			
	#選択しているモデルのリストの取得
	def GetSelectedModelList(self):
		selectedModelList = self._SelectedModelList
		return selectedModelList
		
	#選択している部屋のリストの取得
	def GetSelectedRoomList(self):
		selectedRoomList = self._SelectedRoomList
		return selectedRoomList
		
	#選択しているキャラクターのリストの取得
	def GetSelectedCharacterList(self):
		selectedCharacterList = self._SelectedCharacterList
		return selectedCharacterList
		
	#選択しているドアのリストの取得
	def GetSelectedDoorList(self):
		selectedDoorList = self._SelectedDoorList
		return selectedDoorList
		
	#選択している窓のリストの取得
	def GetSelectedWindowList(self):
		selectedWindowList = self._SelectedWindowList
		return selectedWindowList
		
	#ドラッグ状態の取得
	def GetDragState(self):
		state = self._DragState
		return state
	
	#ダミーモデルの取得
	def GetDummyModel(self):
		dummyModel = self._Dummy
		return dummyModel
		
	#ダミーモデルの角度設定
	def SetDummyOrientation(self,iOrientation):
		if iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Dummy.setEuler(eulerAngle)
			
			self._DummyOrientation = orientation
			
			#スナップ
			snapOrientation = 0
			count = 360 / self._RotateSnap
			for x in range(count):
				angle = self._RotateSnap * x + self._RotateSnap / 2
				if self._DummyOrientation < angle:
					snapOrientation = self._RotateSnap * x
					break
			
			self._Dummy.setEuler(snapOrientation)
			
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		if self._Is3d:
			self.UnSelectRoom()
			self.UnSelectDoor()
			self.UnSelectWindow()
			
	#2D表示のウィンドウの設定
	def SetOrthoViewWindow(self,window):
		self._OrthoViewWindow = window
		
	#縦方向の移動制限を取得
	def GetMoveYState(self):
		state = self._SelectedModelMoveYState
			
		return state
	
	#選択状態の取得
	def GetSelectedState(self):
		state = False
		if self._SelectedModel != None or self._SelectedRoom != None or self._SelectedCharacter != None or self._SelectedDoor != [None,None] or self._SelectedWindow != [None,None]:
			state = True
			
		return state
		
	#新規間取りの選択
	def SelectNewestRoom(self):
		roomCount = self._RoomControlClass.GetRoomCount()
		if roomCount != 0 and self._Is3d == False:
			#全選択状態の解除
			self.UnSelect()
			self._SelectLogClass.UnSelect()
			self._SelectPathClass.UnSelect()
			self._SelectStartPositionClass.UnSelect()
			self._SelectWallClass.UnSelect()
		
			#最新の間取りをリストに追加
			list = [roomCount-1]
			
			if self._SelectedRoomList != []:
				for roomNum in self._SelectedRoomList:
					self._RoomControlClass.SetHighlightFloor(roomNum,False)
				self._SelectedRoomList = []
				
			self._SelectedRoom = list[0]
			for roomNum in list:
				self._RoomControlClass.SetHighlightFloor(roomNum,True)
				self._SelectedRoomList.append(roomNum)
				
			if len(self._SelectedRoomList) == 1 and self._SelectedModelList == [] and self._SelectedCharacterList == []:
				room = self._RoomControlClass.GetRoom(self._SelectedRoom)
				floorModel = room.GetFloorModel()
				self._ManipulatorClass.SetModel(floorModel,False)	#マニピュレータ設定
			
			else:
				self._ManipulatorClass.SetModel(self._Dummy,False)	#マニピュレータ設定
				
	#最初に選択したドア、窓の部屋番号を取得
	def GetBaseRoom(self):
		num = self._BaseRoom
		return num
		