﻿# EnvironmentControl
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/2
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Interface
import LayoutEditor

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#地形制御のクラス定義
class EnvironmentControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][EnvironmentControl] : Init EnvironmentControl')
		
		self._Environment = None
		self._EnvironmentName = ""
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][EnvironmentControl] : Start EnvironmentControl Update')
		pass
		
	#追加
	def Add(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][EnvironmentControl] : Add')
		
		loadState = False
		
		#チェック
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(fileName)
		wholeSize = iniFile.GetItemData('Define','WholeSize')
		
		if wholeSize == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルは地形定義ファイルではありません。', title='RiMMInspector')
			loadState = False
		
		else:
			#既にある地形を削除
			if self._EnvironmentName != "":
				self.Delete()
			
			#地形の読み込み
			model = LayoutEditor.LoadLayoutDataClass(fileName)
			
			self._Environment = model
			self._EnvironmentName = fileName
			
			loadState = True
			
		return loadState
		
	#削除
	def Delete(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][EnvironmentControl] : Delete')
		
		if self._EnvironmentName != "":
			del self._Environment
			
			self._Environment = None
			self._EnvironmentName = ""
	
		