﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d

import math
from os import path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁制御のクラス定義
class Wall(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,doorList,windowList,textureName,textureSize,height,visibleList,isOpeRoom):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Wall] : Init Wall')
		
		self._FloorPosition = floorPositoin
		
		self._DoorControlClass = None
		self._WindowControlClass = None
		
		self._PointList = pointList
		self._DoorList = doorList
		self._WindowList = windowList
		self._TextureName = textureName
		self._TextureSize = textureSize
		self._Height = height
		self._VisibleList = visibleList
		
		self._ModelCount = 5
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		self._Texture = None
		self._TextureMatrix = None
		
		self.SetTextureMatrix()
		
		#壁の端は斜めに面を作成（正方形の部屋の四隅）
		self._OffsetVal = 0.075
		self._OpeRoomOffsetVal = 0.031
		
		#オペ室かどうか
		self._IsOpRoom = isOpeRoom
		
	#使用するクラスの追加
	def AddClass(self,doorControlClass,windowControlClass):
		self._DoorControlClass = doorControlClass
		self._WindowControlClass = windowControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Wall] : Start Wall Update')
		pass
		
	#壁モデルの作成
	def Create(self):
		#print 'CreateWall'
		
		self._Texture = self.LoadTexture(self._TextureName)
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		
		self.SetWall(True)
		self.SetPickable(False)
		self.SetVisible()
		
	#壁モデルの削除
	def Delete(self):
		#print 'DeleteWall'
		
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				"""
				childModelList = model.getChildren()
				for childModel in childModelList:
					childModel.remove()	#モデルを削除
					del childModel
				"""
				
				model.remove()	#モデルを削除
				del model
				
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		
	#壁モデルの配置
	def SetWall(self,isCreate=False):
		#print 'SetWall'
		
		#部屋にある壁の数だけループ
		for pointNum in range(len(self._PointList)):
			#print 'pointNum',pointNum
			
			#壁の両端の位置と距離を取得
			point0,point1 = self.GetPoint(pointNum)
			pointDistance = vizmat.Distance(point0,point1)
			
			#壁に配置されているドアと窓の情報を取得
			doorDataList	= self.GetDoorData(pointNum)
			windowDataList	= self.GetWindowData(pointNum)
			
			#ドアと窓が無い場合（壁のみ）
			if len(doorDataList) == 0 and len(windowDataList) == 0:
				for modelNum in range(self._ModelCount):		#5面作成（表裏左右上）
					rootModel = self.GetRootModel(isCreate,modelNum,pointNum)	#ルートモデルの設定
					wallModel = self.GetModel(isCreate,rootModel,modelNum,0)	#壁モデルの設定
					
					percentage0 = 0.0
					percentage1 = 1.0
					positionYMin = 0.0
					positionYMax = self._Height
					
					#モデルを配置
					self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
					
					#表示状態の設定
					visibleState = self._VisibleList[pointNum]
					self.SetModelPosition(rootModel,point0,point1,visibleState)
					
			#ドアと窓が有る場合
			else:
				#ドア、窓を位置が小さい順に並べる
				dataList = self.SortData(doorDataList,windowDataList)
				#print 'dataList',dataList
				#print 'doorDataList',doorDataList
				#print 'windowDataList',windowDataList
				
				for modelNum in range(self._ModelCount):		#5面作成（表裏左右上）
					rootModel = self.GetRootModel(isCreate,modelNum,pointNum)	#ルートモデルの設定
					
					modelCount = 0
					
					#ドア又は窓の数だけループ
					#print 'dataCount=',len(dataList)
					for count in range(len(dataList)):
						data = dataList[count]
						#print 'data',data
						
						#最初は壁を作成
						if count == 0:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage1 = posXMin
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#ドア又は窓の上に壁を作成
						wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
						modelCount = modelCount + 1
						
						percentage0 = 0.0
						percentage1 = 1.0
						positionYMin = 0.0
						positionYMax = self._Height
						
						posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
						percentage0 = posXMin
						percentage1 = posXMax
						positionYMin = posYMax
						
						#モデルを配置
						self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
						
						#ドア又は窓の上に壁を作成
						if data[0] == 1:	#窓の場合
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMin
							percentage1 = posXMax
							positionYMax = posYMin
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#最後は壁を作成
						if count == len(dataList) - 1:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMax
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#次のドア又は窓まで壁を作成
						else:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMax
							
							nextData = dataList[count+1]
							#print 'nextData',nextData
							posXMin2,posXMax2,posYMin2,posYMax2 = self.GetPositionOnWall(nextData,pointNum,pointDistance)
							percentage1 = posXMin2
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
							#隙間がない時は壁を非表示
							dist = pointDistance * (percentage1-percentage0)
							
							if dist > 0.01:	#今の窓、ドアの終端と次の窓、ドアの始端の位置関係が1cm未満の場合は非表示
								wallModel.visible(viz.ON)
							else:
								wallModel.visible(viz.OFF)
								
					#表示状態の設定
					visibleState = self._VisibleList[pointNum]
					self.SetModelPosition(rootModel,point0,point1,visibleState)
					
	#ルートモデルを作成
	def CreateRootModel(self):
		model = vizshape.addPlane([1,1])
		model.disable(viz.PICKING)
		model.disable(viz.RENDERING)
		
		return model
		
	#壁モデルを作成
	def CreateWallModel(self,rootModel):
		#板ポリゴン作成
		viz.startLayer(viz.QUADS)
		viz.texCoord(0.0,0.0)
		viz.vertex(0.0,0.0,0.0)
		viz.normal(0,1,0)
		viz.texCoord(1.0,0.0)
		viz.vertex(1.0,0.0,0.0)
		viz.normal(0,1,0)
		viz.texCoord(1.0,1.0)
		viz.vertex(1.0,0.0,1.0)
		viz.normal(0,1,0)
		viz.texCoord(0.0,1.0)
		viz.vertex(0.0,0.0,1.0)
		viz.normal(0,1,0)
		model = viz.endLayer()
		
		model.setParent(rootModel)
		model.texture(self._Texture)
		
		model.enable(viz.LIGHTING)
		model.color(viz.WHITE)
		model.emissive(viz.GRAY)
		model.ambient(viz.GRAY)
		
		model.enable(viz.SAMPLE_ALPHA_TO_COVERAGE)
		model.disable(viz.BLEND)
		
		return model
		
	#モデル位置をリセット
	def ResetModel(self,model):
		model.setPosition(0,0,0)
		model.setEuler(0,0,0)
		
	#ルートモデルを取得
	def GetRootModel(self,isCreate,modelNum,pointNum):
		rootModel = None
		
		if isCreate:	#作成
			rootModel = self.CreateRootModel()
			self.AddToWallList(modelNum,rootModel)
			
		else:			#取得
			rootModel = self.GetFromWallList(modelNum,pointNum)
			self.ResetModel(rootModel)
			
		return rootModel
		
	#モデルを取得
	def GetModel(self,isCreate,rootModel,modelNum,count):
		wallModel = None
		
		if isCreate:	#作成
			wallModel = self.CreateWallModel(rootModel)
			
		else:			#取得
			modelList = rootModel.getChildren()
			wallModel = modelList[count]
			self.ResetModel(wallModel)
			
		return wallModel
		
	#壁をリストに追加
	def AddToWallList(self,modelNum,rootModel):
		if modelNum == 0:
			self._ModelList.append(rootModel)
		elif modelNum == 1:
			self._ModelBackSideList.append(rootModel)
		elif modelNum == 2:
			self._ModelTopSideList.append(rootModel)
		elif modelNum == 3:
			self._ModelLeftSideList.append(rootModel)
		elif modelNum == 4:
			self._ModelRightSideList.append(rootModel)
			
	#壁をリストから取得
	def GetFromWallList(self,modelNum,wallNum):
		rootModel = None
		if modelNum == 0:
			rootModel = self._ModelList[wallNum]
		elif modelNum == 1:
			rootModel = self._ModelBackSideList[wallNum]
		elif modelNum == 2:
			rootModel = self._ModelTopSideList[wallNum]
		elif modelNum == 3:
			rootModel = self._ModelLeftSideList[wallNum]
		elif modelNum == 4:
			rootModel = self._ModelRightSideList[wallNum]
			
		return rootModel
		
	#壁にあるドアの情報を取得
	def GetDoorData(self,wallNum):
		dataList = []
		for x in range(len(self._DoorList)):
			data = self._DoorList[x]
			if data[1] == wallNum:
				if len(data) == 4:
					data[3] = x
				else:
					data.append(x)
					
				dataList.append(data)
				
		return dataList
		
	#壁にある窓の情報を取得
	def GetWindowData(self,wallNum):
		dataList = []
		for x in range(len(self._WindowList)):
			data = self._WindowList[x]
			if data[2] == wallNum:
				if len(data) == 5:
					data[4] = x
				else:
					data.append(x)
					
				dataList.append(data)
				
		return dataList
		
	#壁のモデルを配置
	def SetWallModel(self,wallModel,modelNum,percentage0,percentage1,posYMin,posYMax,pointDistance):
		position0 = (pointDistance * (1-percentage1)) - (pointDistance / 2)
		position1 = (pointDistance * (1-percentage0)) - (pointDistance / 2)
		
		positionYMin = posYMin
		positionYMax = posYMax
		
		posZ = 0.0
		if modelNum == 0:
			posZ = 0.075 + 0.001	#壁は内側に1mmずらす
		elif modelNum == 1:
			posZ = -0.075 + 0.001
			
		if percentage0 == 0:	#最初の位置
			if modelNum != 0:	
				if self._IsOpRoom == 1:
					position1 = position1 + self._OpeRoomOffsetVal
				elif self._IsOpRoom == 2:
					pass
				else:
					position1 = position1 + self._OffsetVal
				
		if percentage1 == 1:	#最後の位置
			if modelNum != 0:
				if self._IsOpRoom == 1:
					position0 = position0 - self._OpeRoomOffsetVal
				elif self._IsOpRoom == 2:
					pass
				else:
					position0 = position0 - self._OffsetVal
					
		#オペ室通路用間取りの壁は幅を縮める
		if self._IsOpRoom == 2:
			position1 = position1 - 0.035
			position0 = position0 + 0.035
			
		wallModel.setVertex(0,[position0,posZ,positionYMin])
		wallModel.setTexCoord(0,position0,positionYMin)
		wallModel.setVertex(1,[position1,posZ,positionYMin])
		wallModel.setTexCoord(1,position1,positionYMin)
		wallModel.setVertex(2,[position1,posZ,positionYMax])
		wallModel.setTexCoord(2,position1,positionYMax)
		wallModel.setVertex(3,[position0,posZ,positionYMax])
		wallModel.setTexCoord(3,position0,positionYMax)
		
		if modelNum == 2:	#天板
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position0,posZ,positionYMax])
			wallModel.setVertex(1,[position1,posZ,positionYMax])
			wallModel.setVertex(2,[position1,posZ2,positionYMax])
			wallModel.setVertex(3,[position0,posZ2,positionYMax])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		if modelNum == 3:	#左側面
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position0,posZ,positionYMin])
			wallModel.setVertex(1,[position0,posZ,positionYMax])
			wallModel.setVertex(2,[position0+self._OffsetVal,posZ2,positionYMax])
			wallModel.setVertex(3,[position0+self._OffsetVal,posZ2,positionYMin])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		if modelNum == 4:	#右側面
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position1,posZ,positionYMin])
			wallModel.setVertex(1,[position1,posZ,positionYMax])
			wallModel.setVertex(2,[position1-self._OffsetVal,posZ2,positionYMax])
			wallModel.setVertex(3,[position1-self._OffsetVal,posZ2,positionYMin])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		#テクスチャ設定
		wallModel.texmat(self._TextureMatrix)
		
	#ドア、窓を位置が小さい順に並べる
	def SortData(self,doorDataList,windowDataList):
		targetList = []		#比較対象
		for x in range(len(doorDataList)):
			targetList.append([0,x,doorDataList[x][2]])
		for x in range(len(windowDataList)):
			targetList.append([1,x,windowDataList[x][3]])
		#print 'targetList',targetList
		
		sortedList = sorted(targetList,key=lambda x: x[2])		#3つ目の値でソート
		
		resDataList = []	#並べ替えた結果
		for data in sortedList:
			resDataList.append([data[0],data[1]])
		#print 'resDataList',resDataList
		
		return resDataList
		
	#ドア、窓の上下左右の位置を取得
	def GetPositionOnWall(self,data,pointNum,pointDistance):
		percentage0 = 0.0
		percentage1 = 1.0
		posYMin = 0.0
		posYMax = 1.0
		
		if data[0] == 0:	#ドアの場合
			doorDataList = self.GetDoorData(pointNum)
			doorData = doorDataList[data[1]]
			#print 'doorData',doorData
			door = self._DoorControlClass.GetModel(doorData[3])
			doorSize = door.GetSize()
			doorSizePercentageX = doorSize[0] / pointDistance
			percentage0 = doorData[2] - doorSizePercentageX / 2
			percentage1 = doorData[2] + doorSizePercentageX / 2
			posYMax = doorSize[1]
			
		elif data[0] == 1:	#窓の場合
			windowDataList = self.GetWindowData(pointNum)
			windowData = windowDataList[data[1]]
			#print 'windowData',windowData
			window = self._WindowControlClass.GetModel(windowData[4])
			windowSize = window.GetSize()
			windowSizePercentageX = windowSize[0] / pointDistance
			percentage0 = windowData[3] - windowSizePercentageX / 2
			percentage1 = windowData[3] + windowSizePercentageX / 2
			posYMin = windowData[1]
			posYMax = windowData[1] + windowSize[1]
			
		return percentage0,percentage1,posYMin,posYMax
		
	#壁のUV設定
	def SetTextureMatrix(self):
		#テクスチャのUV設定
		self._TextureMatrix = vizmat.Transform()
		self._TextureMatrix.setPosition([0.0,0.0,0.0])
		self._TextureMatrix.setScale([1/self._TextureSize,1/self._TextureSize,1])
		
	#テクスチャ画像の読み込み
	def LoadTexture(self,textureName):
		filePath = textureName
		"""
		if path.exists(textureName)!=True:		#絶対パスかどうか
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		"""
		if textureName[:2] == '..':
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		
		texture = viz.addTexture(filePath)
		
		return texture
		
	#選択可能状態の設定
	def SetPickable(self,state):
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				childModelList = model.getChildren()
				for childModel in childModelList:
					if state == True:
						childModel.enable(viz.PICKING)
					elif state == False:
						childModel.disable(viz.PICKING)
						
	#ポイントを取得
	def GetPoint(self,num):
		point0 = self._PointList[num]
		num2 = num + 1
		if num2 == len(self._PointList):
			num2 = 0
		point1 = self._PointList[num2]
		
		return point0,point1
		
	#ポイント番号を取得
	def GetPointNum(self,num):
		num2 = num + 1
		if num2 == len(self._PointList):
			num2 = 0
			
		return num,num2
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		#self.SetWall()
		
	#位置設定
	def SetModelPosition(self,model,point0,point1,visibleState):
		#位置
		position = [0.0,0.0,0.0]
		position[0] = (point0[0] + point1[0]) / 2 + self._FloorPosition[0]
		position[1] = 0.0 + self._FloorPosition[1]
		position[2] = (point0[1] + point1[1]) / 2 + self._FloorPosition[2]
		
		#非表示の時はずらす
		if visibleState == False:
			position[1] = -15.0
			
		model.setPosition(position)
		
		#角度
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		model.setEuler(wallAngle-90.0,-90.0,0.0)
		
	#ドアを設定
	def SetDoor(self,doorList):
		self._DoorList = doorList
		
		"""
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		"""
		
	#窓を設定
	def SetWindow(self,windowList):
		self._WindowList = windowList
		
		"""
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		"""
		
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
		
	#テクスチャ設定
	def SetTexture(self,texture):
		self._Texture = texture
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				childModelList = model.getChildren()
				for childModel in childModelList:
					childModel.texture(self._Texture)
					
	#テクスチャ取得
	def GetTexture(self):
		texture = self._Texture
		return texture
		
	#テクスチャ名取得
	def GetTextureName(self):
		textureName = self._TextureName
		return textureName
		
	#壁のポイントを追加
	def AddPoint(self,num,pointData=None,wallVisibleList=None):
		#ポイントリストを更新
		point0,point1 = self.GetPoint(num)
		newPoint = [0,0]
		newPoint[0] = (point0[0] + point1[0]) / 2
		newPoint[1] = (point0[1] + point1[1]) / 2
		
		self._PointList.insert(num+1,newPoint)
		
		#表示状態リストを更新
		visibleState = self._VisibleList[num]
		self._VisibleList.insert(num+1,visibleState)
		
		#データがある時
		if pointData != None:
			self._PointList		= pointData
			self._VisibleList	= wallVisibleList
			
		#ドアリストを更新
		for x in range(len(self._DoorList)):
			door = self._DoorList[x]
			if door[1] == num:
				if door[2] < 0.5:
					door[2] = door[2] * 2
				else:
					door[1] = door[1] + 1
					door[2] = door[2] * 2 - 1
				self._DoorList[x] = door
				
			elif door[1] > num:
				door[1] = door[1] + 1
				self._DoorList[x] = door
				
		#窓リストを更新
		for x in range(len(self._WindowList)):
			widnow = self._WindowList[x]
			if widnow[2] == num:
				if widnow[3] < 0.5:
					widnow[3] = widnow[3] * 2
				else:
					widnow[2] = widnow[2] + 1
					widnow[3] = widnow[3] * 2 - 1
				self._WindowList[x] = widnow
				
			elif widnow[2] > num:
				widnow[2] = widnow[2] + 1
				self._WindowList[x] = widnow
				
		"""
		#再作成
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		"""
		
		floorPointList = self._PointList
		doorList = self._DoorList
		windowList = self._WindowList
		
		return floorPointList,doorList,windowList
		
	#壁のポイントを削除
	def RemovePoint(self,num):
		#ポイントリストを更新
		point = self._PointList[num]
		self._PointList.remove(point)
		
		#表示状態リストを更新
		newVisibleList = []
		for x in range(len(self._VisibleList)):
			if x != num:
				newVisibleList.append(self._VisibleList[x])
				
		self._VisibleList = newVisibleList
		
		#ドアリストを更新
		for x in range(len(self._DoorList)):
			door = self._DoorList[x]
			if num == 0:
				if door[1] == len(self._PointList):
					door[1] = door[1] - 1
					door[2] = door[2] / 2
					self._DoorList[x] = door
				elif door[1] == num:
					door[1] = len(self._PointList) - 1
					door[2] = door[2] / 2 + 0.5
					self._DoorList[x] = door
				elif door[1] > num:
					door[1] = door[1] - 1
					self._DoorList[x] = door
			else:
				if door[1] == num - 1:
					door[2] = door[2] / 2
					self._DoorList[x] = door
				elif door[1] == num:
					door[1] = door[1] - 1
					door[2] = door[2] / 2 + 0.5
					self._DoorList[x] = door
				elif door[1] > num:
					door[1] = door[1] - 1
					self._DoorList[x] = door
					
		#窓リストを更新
		for x in range(len(self._WindowList)):
			widnow = self._WindowList[x]
			if num == 0:
				if widnow[2] == len(self._PointList):
					widnow[2] = widnow[2] - 1
					widnow[3] = widnow[3] / 2
					self._WindowList[x] = widnow
				elif widnow[2] == num:
					widnow[2] = len(self._PointList) - 1
					widnow[3] = widnow[3] / 2 + 0.5
					self._WindowList[x] = widnow
				elif widnow[2] > num:
					widnow[2] = widnow[2] - 1
					self._WindowList[x] = widnow
			else:
				if widnow[2] == num - 1:
					widnow[3] = widnow[3] / 2
					self._WindowList[x] = widnow
				elif widnow[2] == num:
					widnow[2] = widnow[2] - 1
					widnow[3] = widnow[3] / 2 + 0.5
					self._WindowList[x] = widnow
				elif widnow[2] > num:
					widnow[2] = widnow[2] - 1
					self._WindowList[x] = widnow
					
		"""
		#再作成	
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		"""
		
		floorPointList = self._PointList
		doorList = self._DoorList
		windowList = self._WindowList
		
		return floorPointList,doorList,windowList
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		#self.SetWall()
		
	#モデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		for wallModel in self._ModelList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		return modelList
		
	#全モデルのリストを取得
	def GetAllModelList(self):
		modelList = []
		
		for wallModel in self._ModelList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelBackSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelTopSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelLeftSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelRightSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		return modelList
		
	#モデルの表示状態を設定
	def SetVisible(self):
		#print 'self._VisibleList',self._VisibleList
		#print 'self._ModelCount',self._ModelCount
		#print 'self._ModelList',self._ModelList
		
		for x in range(len(self._ModelList)):
			model = None
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				visibleState = self._VisibleList[x]
				
				if visibleState:
					model.visible(viz.ON)
					
					position = model.getPosition()
					position[1] = 0.0 + self._FloorPosition[1]
					model.setPosition(position)
					
				else:
					model.visible(viz.OFF)
					
					position = model.getPosition()
					position[1] = -15.0
					model.setPosition(position)
					
	#モデルの表示状態のリストを設定
	def SetVisibleList(self,visibleList):
		self._VisibleList = visibleList
		
		self.SetVisible()
		
	#モデルの表示状態のリスト取得
	def GetVisibleList(self):
		list = self._VisibleList
		return list
		
	#壁を非表示に設定
	def Hide(self,state):
		if state:
			for x in range(len(self._ModelList)):
				for y in range(self._ModelCount):
					model = None
					if y == 0:
						model = self._ModelList[x]
					elif y == 1:
						model = self._ModelBackSideList[x]
					elif y == 2:
						model = self._ModelTopSideList[x]
					elif y == 3:
						model = self._ModelLeftSideList[x]
					elif y == 4:
						model = self._ModelRightSideList[x]
						
					model.visible(viz.OFF)
					
		else:
			self.SetVisible()
			
	#壁モデルを作成
	def SetAllWallModel(self):
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		