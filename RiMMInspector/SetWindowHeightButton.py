﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object2d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓の高さ設定ボタン制御のクラス定義
class SetWindowHeightButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetWindowHeightButton] : Init SetWindowHeightButton')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectModelClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._OverState = False
		
		self._VisibleState = True
		
		self._Frame = None
		self._Title = None
		self._TextBox = None
		self._DummyText = None
		
		self._SelectedWindowList = []
		self._Value = 0.0
		
		self._PreKeyState = False
		self._PreForcus = False
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [233,windowSize[1]-86]
		self._FrameScale = [290,45]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#タイトル追加
		self._TitlePosition = [132,windowSize[1]-86]
		self._TitleScale = [18.6,18.6]
		
		self._Title = self.CreateText('高さ',self._TitlePosition,self._TitleScale)
		
		#テキストボックス追加
		self._TextBoxPosition = [275,windowSize[1]-86]
		self._TextBoxScale = [200,36]
		
		self._TextBox = self.CreateTextBox(self._TextBoxPosition,self._TextBoxScale)
		self.SetText()
		
		#ダミーテキスト追加
		self._DummyTextPosition = [185,windowSize[1]-86]
		self._DummyTextScale = [20,27]
		
		self._DummyText = self.CreateDummyText('',self._DummyTextPosition,self._DummyTextScale)
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectModelClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectModelClass = selectModelClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetWindowHeightButton] : Start SetWindowHeightButton Update')
		
		self._OverState = False
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
					
		self.InputVal()
	
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
			
			#表示したらフォーカス
			self.SetTextBoxForcus()
			
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		self._Title.visible(val)
		self._TextBox.visible(val)
		self._DummyText.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#数値を設定
	def SetValue(self,windowList,val):
		self._SelectedWindowList = windowList
		self._Value = val
		self.SetText()
		
	#数値を取得
	def GetValue(self):
		windowList = self._SelectedWindowList
		val = self._Value
		
		return windowList,val
		
	#数値の表示を更新
	def SetText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(round(val,3))
		self._TextBox.message(message)
	
	#ダミーテキストを表示
	def SetDummyText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(round(val,3))
		self._DummyText.message(message)
	
		self._TextBox.message('')
		
	#文字入力
	def InputVal(self):
		keyState = False
		forcus = False
		
		if self._VisibleState == True:
			keyState = False
			if viz.key.isDown(viz.KEY_RETURN):
				keyState = True
			if viz.key.isDown(viz.KEY_KP_ENTER):
				keyState = True
				
			if keyState:
				inputVal = self._TextBox.getMessage()
				preWindowList,preVal = self.GetValue()
				
				try:
					val = float(inputVal) * 0.001	#入力時はミリメートル → メートル
				except:
					val = preVal
				
				if val != preVal:
					self._SelectedWindowList = self._SelectModelClass.GetSelectedWindowList()
					self.SetValue(preWindowList,val)
					
					self._UndoData = []
					self._UndoData.append(12)
					
					for windowData in self._SelectedWindowList:
						preHeight = self._RoomControlClass.GetWindowHeight(windowData[0],windowData[1])
						
						self._RoomControlClass.SetWindowHeight(windowData[0],windowData[1],self._Value)
					
						self._UndoData.append([windowData[0],windowData[1],preHeight])
						
					self._UndoClass.AddToList(self._UndoData)
					self._RedoClass.ClearList()
					
			forcus = self.GetTextBoxForcus()
			if forcus != self._PreForcus and keyState == 0:
				preItem,preVal = self.GetValue()
				self.SetValue(preItem,preVal)
				
				if forcus:
					self.SetDummyText()
					
			inputValTemp = self._TextBox.getMessage()
			if len(inputValTemp) != 0:
				self._DummyText.message('')
				
		self._PreKeyState = keyState
		self._PreForcus = self.GetTextBoxForcus()
		
	#モデルを設定
	def SetModel(self,val):
		self._SelectedWindowList = val
		
	#モデルを取得
	def GetModel(self):
		val = self._SelectedWindowList
		
		return val
		
	#テキストボックスのフォーカスを設定
	def SetTextBoxForcus(self):
		self.SetDummyText()
		self._TextBox.setFocus(viz.ON)
			
	def GetTextBoxForcus(self):
		state = self._TextBox.getFocus()
		return state
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [233,windowSize[1]-86]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TitlePosition = [132,windowSize[1]-86]
		self.SetIconPosition(self._Title,self._TitlePosition,self._TitleScale)
		
		self._TextBoxPosition = [275,windowSize[1]-86]
		self.SetIconPosition(self._TextBox,self._TextBoxPosition,self._TextBoxScale,True)
		
		self._DummyTextPosition = [185,windowSize[1]-86]
		self.SetIconPosition(self._DummyText,self._DummyTextPosition,self._DummyTextScale)
		