﻿# coding: utf-8

import viz
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#Callback関数制御のクラス定義
class CallbackContorl():
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CallbackContorl] : Init CallbackContorl')
		
		self._InputClass 			= None
		self._CameraControlClass	= None
		
		#処理開始
		viz.callback(viz.MOUSEDOWN_EVENT,self.MouseDownEvent)	#マウスのボタンが押された
		viz.callback(viz.MOUSEUP_EVENT,self.MouseUpEvent)		#マウスのボタンが離された
		viz.callback(viz.MOUSEWHEEL_EVENT,self.MouseWheelEvent)	#マウスのホイールが操作された
		
		viz.callback(viz.KEYDOWN_EVENT,self.OnKeyDown)
		viz.callback(viz.KEYUP_EVENT,self.OnKeyUp)
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
		
	#マウスのボタンが離された状態から押された状態に切り替わったタイミングで呼び出される関数	
	def MouseDownEvent(self,button):
		self._InputClass.MouseDownEvent(button)
		self._CameraControlClass.MouseDownEvent(button)
	
	#マウスのボタンが押された状態から離された状態に切り替わったタイミングで呼び出される関数	
	def MouseUpEvent(self,button):
		self._InputClass.MouseUpEvent(button)
		self._CameraControlClass.MouseUpEvent(button)
	
	#マウスのホイールが操作されたときに呼び出される関数
	def MouseWheelEvent(self,val):
		self._InputClass.MouseWheelEvent(val)
		self._CameraControlClass.MouseWheelEvent(val)
		
	#キーが押されたときに呼び出される関数
	def OnKeyDown(self,key):
		self._InputClass.OnKeyDown(key)
		self._CameraControlClass.OnKeyDown(key)
	
	#キーが離れたときに呼び出される関数
	def OnKeyUp(self,key):
		self._InputClass.OnKeyUp(key)
		self._CameraControlClass.OnKeyUp(key)
		
	#使用するクラスを設定
	def SetTargetClass(self,input,cam):
		self._InputClass			= input
		self._CameraControlClass	= cam
		