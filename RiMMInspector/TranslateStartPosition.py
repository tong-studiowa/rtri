﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#スタート位置の移動制御のクラス定義
class TranslateStartPosition(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateStartPosition] : Init TranslateStartPosition')
		
		self._InputClass = None
		self._StartPositionClass = None
		self._SelectStartPositionClass = None
		self._ManipulatorClass = None
		
		self._SelectedState = False
		
		self._ManipulatorState = 0
		
		self._TranslateSnap = translateSnap
		
	#使用するクラスの追加
	def AddClass(self,inputClass,startPositionClass,selectStartPositionClass,manipulatorClass):
		self._InputClass = inputClass
		self._StartPositionClass = startPositionClass
		self._SelectStartPositionClass = selectStartPositionClass
		self._ManipulatorClass = manipulatorClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateStartPosition] : Start TranslateStartPosition Update')
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState != 0 and manipulatorState != 2 :
			self._SelectedState = self._SelectStartPositionClass.GetSelectedState()
			
			if self._SelectedState:
				self.Translate(manipulatorState)
			
	#移動
	def Translate(self,manipulatorState):
		prePosition = self._StartPositionClass.GetPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = prePosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = prePosition[1]
			position[2] = prePosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = prePosition[0]
			position[1] = pickingPosition[1]
			position[2] = prePosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = prePosition[0]
			position[1] = prePosition[1]
			position[2] = pickingPosition[2]
			
		self._StartPositionClass.SetPosition(position)
		
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
