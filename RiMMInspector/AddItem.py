﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
#import vizfx
import vizshape

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

#addItemHeightTemp = iniFile.GetItemData('Setting','AddItemHeight')
#addItemHeight = float(addItemHeightTemp) * 0.001	#入力時はミリメートル → メートル
addItemHeight = 3000.0

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム追加時のドラッグ用モデル制御のクラス定義
class AddItem(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddModel] : Init AddItem')
		
		self._ModelControlClass = None
		
		self._FileName = None
		self._Model = None
		self._Picking = False
		self._Texture = None
		
		self._TranslateSnap = translateSnap
		self._AddItemHeight = addItemHeight
		
		self._IsDummyBox = False
		self._DummyBoxRoot = None
		
		self._DummyBoxLineSize = 0.1		#各辺のラインの幅
		self._DummyBoxLineList = []
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddItem] : Start AddItem Update')
		pass
	
	#使用するクラスの追加
	def AddClass(self,modelControlClass):
		self._ModelControlClass = modelControlClass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		self._IsDummyBox = False
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = self._ModelControlClass.Add(filePath)	#モデルを追加
		
		self._Model = model
		self._Texture = self._Model.getTexture()
		self.SetPickable(False)
		self.SetHighlight(True)
		
		#ダミーボックスの設定
		#print 'filePath',filePath
		if filePath[-13:] == 'DummyBox.osgb':
			#print 'dummyBox'
			self._IsDummyBox = True
			self.InitDummyBox(model)
			
		self._FileName = filePath
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
			
			if self._IsDummyBox:
				self._DummyBoxRoot.remove()
				for line in self._DummyBoxLineList:
					line.remove()
					
		self._Model = None
		
		self._IsDummyBox = False
		self._DummyBoxRoot = None
		self._DummyBoxLineList = []
		
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			position = iPosition
			
			#スナップ
			position[0] = self.Snap(position[0])
			position[2] = self.Snap(position[2])
			
			if self._IsDummyBox:
				self._DummyBoxRoot.setPosition(position)
			else:
				self._Model.setPosition(position)
				
			#高さを床面に揃える
			box = self._Model.getBoundingBox()
			ymin = box.getYmin()
			position[1] = position[1] - ymin + 0.01
			
			#ダミーボックスはラインの分下げる
			if self._FileName[-13:] == 'DummyBox.osgb':
				position[1] = position[1] - 0.01
			
			#蛍光灯、エアコンは天井に配置
			if self._FileName != None and self._FileName != '':
				if self._FileName[-23:-10] == 'ceiling_light' or self._FileName[-22:-10] == 'aircondition':
					position[1] = self._AddItemHeight - 0.01
					
			if self._IsDummyBox:
				self._DummyBoxRoot.setPosition(position)
			else:
				self._Model.setPosition(position)
				
	#位置取得
	def GetPosition(self):
		position = [0,0,0]
		
		if self._Model != None:
			if self._IsDummyBox:
				position = self._DummyBoxRoot.getPosition()
			else:
				position = self._Model.getPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			if self._IsDummyBox:
				self._DummyBoxRoot.setEuler(eulerAngle)
			else:
				self._Model.setEuler(eulerAngle)
				
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.enable(viz.LIGHTING)
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.specular(viz.RED)
		else:
			self._Model.disable(viz.LIGHTING)
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.specular(self._Specular)
		
	#選択可能かの設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
		
	#ダミーボックスの設定
	def InitDummyBox(self,model):
		root = vizshape.addBox()
		root.disable(viz.RENDERING)
		root.disable(viz.INTERSECTION)
		root.disable(viz.PICKING)
		self._DummyBoxRoot = root
		
		model.setParent(root)

		for x in range(12):
			line = vizshape.addBox()
			line.color(viz.GRAY)
			line.disable(viz.INTERSECTION)
			line.disable(viz.PICKING)
			line.setParent(root)
			self._DummyBoxLineList.append(line)
			
		self.SetDummyBoxLinePosition()

	#ダミーボックスの枠線を配置
	def SetDummyBoxLinePosition(self):
		box = self._Model.getBoundingBox()
		modelSize = box.size
		#print 'modelSize=',modelSize
		
		lineSize = self._DummyBoxLineSize
		
		for x in range(len(self._DummyBoxLineList)):
			line = self._DummyBoxLineList[x]
			
			scale = [lineSize,lineSize,lineSize]
			if x < 4:
				scale[0] = (modelSize[0]+0.02)
			elif x < 8:
				scale[1] = (modelSize[1]+0.02)
			else:
				scale[2] = (modelSize[2]+0.02)
			line.setScale(scale)
			
			pos = [0,modelSize[1]*0.5,0]
			
			if   x == 0:
				pos[1] = (lineSize*0.5)-0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 1:
				pos[1] = (lineSize*0.5)-0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 2:
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 3:
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
				
			elif x == 4:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 5:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 6:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 7:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
				
			elif x == 8:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[1] = (lineSize*0.5)-0.01
			elif x == 9:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[1] = (lineSize*0.5)-0.01
			elif x == 10:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
			elif x == 11:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				
			line.setPosition(pos)
			