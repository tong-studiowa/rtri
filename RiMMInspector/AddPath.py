﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
#import vizfx

import Object2d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス追加制御のクラス定義
class AddPath(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddPath] : Init AddPath')
		
		self._InputClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._PathControlClass = None
		self._CheckStateClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._FunctionButtonAreaClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._ItemControlClass = None
		
		self._AddState = False
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		self._Is3d = False
		
		self._SelectedCharacter = None
		self._SelectedItem = None
		
		self._Height = 0
		
		self._TranslateSnap = translateSnap
		
		self._UndoData = []
		
		#フレーム追加
		self._Position = [226,411]
		self._Scale = [100,36]
		self._Frame = self.CreateFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._TextScale = [18.6,18.6]
		self._Text = self.CreateText("",self._Position,self._TextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,characterControlClass,selectModelClass,pathControlClass,checkStateClass
				,stencilClass,manipulatorClass,functionButtonAreaClass,undoClass,redoClass,itemControlClass):
		self._InputClass = inputClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._PathControlClass = pathControlClass
		self._CheckStateClass = checkStateClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._ItemControlClass = itemControlClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddPath] : Start AddPath Update')
		
		self._SelectedCharacter = self._SelectModelClass.GetSelectedCharacter()
		selectedCharacterList = self._SelectModelClass.GetSelectedCharacterList()
		self._SelectedItem = self._SelectModelClass.GetSelectedModel()
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
		selectedDoorList = self._SelectModelClass.GetSelectedDoorList()
		selectedWindowList = self._SelectModelClass.GetSelectedWindowList()
		
		isItem = False
		if self._SelectedCharacter != None and self._Is3d == False and len(selectedCharacterList) == 1 and  selectedItemList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == []:
			self.SetVisible(True)
		elif self._SelectedItem != None and self._Is3d == False and len(selectedItemList) == 1 and  selectedCharacterList == [] and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == []:
			self.SetVisible(True)
			isItem = True
		else:
			self.SetVisible(False)
			
		self._ClickState = False
		self._OverState = False
		
		if self._IsVisible and self._CheckStateClass.GetModeState() != 3:
			mousePosition = self._InputClass.GetMousePosition()
			
			#ボタンをクリックでモード切り替え
			self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
				
			if self._OverState:
				if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
					self._ClickState = True
					
					if self._AddState == True:
						self._AddState = False
					else:
						self._AddState = True
						
					self.Set()
			
			#画面上をクリックで追加
			if self._AddState == True and self._OverState == False and self._IsVisible == True:
				if self._InputClass.GetMouseClickState():
					if self._CheckStateClass.GetOverState() != True:
						self.Add(isItem)
					
					else:
						if self._AddState == True:
							self._AddState = False
							self.Set()
						
				elif self._InputClass.GetMouseStartDragState() and self._CheckStateClass.GetOverState():	#リストからドラッグなどで解除
					if self._AddState == True:
						self._AddState = False
						self.Set()
						
				elif self._InputClass.GetMouseStartDragState() and self._InputClass.GetShiftKeyState():	#複数選択で解除
					if self._AddState == True:
						self._AddState = False
						self.Set()
				
			#ロールオーバー
			self.Rollover(self._OverState)
			
	#リセット処理
	def Reset(self):
		self._AddState = False
		self._ClickState = False
		self._IsVisible = False
		self.SetText(False)
		self.SetVisible(False)
		
	#設定を反映
	def Set(self):
		if self._AddState == False:
			self.SetText(False)
		else:
			self.SetText(True)
			
		self._FunctionButtonAreaClass.SetIconVisible()
		
	#追加
	def Add(self,isItem=False):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self._Height
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		selectedModelNum = 0
		if isItem:
			selectedModelNum = self._SelectedItem
		else:
			selectedModelNum = self._SelectedCharacter
			
		pathNum = self._PathControlClass.Add(selectedModelNum,isItem)
		self._PathControlClass.SetPosition(selectedModelNum,pathNum,pickingPosition,isItem)
		
		self._UndoData = []
		self._UndoData.append(30)
		self._UndoData.append([selectedModelNum,pathNum,isItem])
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res

	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		self._Frame.visible(val)
		self._Text.visible(val)
		
	#テキスト設定
	def SetText(self,state):
		if state == False:
			self._Text.message("パス追加")
		else:
			self._Text.message("追加終了")
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#選択状態の取得
	def GetAddState(self):
		state = self._AddState
		
		return state
		
	#選択状態の設定
	def SetAddState(self,state):
		self._AddState = state
		
		self.Set()
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Text,self._Position,self._TextScale)
		