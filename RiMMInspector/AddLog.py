﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
#import vizfx

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログ追加制御のクラス定義
class AddLog(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddLog] : Init AddLog')
		
		self._InputClass = None
		self._LogControlClass = None
		self._StencilClass = None
		self._FunctionButtonAreaClass = None
		self._ChangeViewClass = None
		self._BackgroundClass = None
		self._SetDirectionClass = None
		
		self._AddState = False
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		
		self._Height = 1.4
		self._LogAngle = 0
		
		self._TranslateSnap = translateSnap
		
		#フレーム追加
		self._Position = [0.049,0.682]
		self._Scale = [1.0,0.5]
		self._Frame = self.CreateFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._TextScale = [0.16875,0.3]
		self._Text = self.CreateText("",self._Position,self._TextScale)
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddLog] : Start AddLog Update')

		self._ClickState = False
		self._OverState = False
		
		
		if self._IsVisible:
			mousePosition = self._InputClass.GetMousePosition()
			
			if mousePosition[1] > 0.657 and mousePosition[1] < 0.706:
				if mousePosition[0] > 0.01 and mousePosition[0] < 0.0885:
					self._OverState = True
					
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						
						if self._AddState == True:
							self._AddState = False
						else:
							self._AddState = True
							
						self.Set()
			
			if self._AddState == True and self._OverState == False and self._IsVisible == True:
				if self._InputClass.GetMouseClickState():
					if self._StencilClass.GetOverState() != True and self._FunctionButtonAreaClass.GetOverState() != True and self._ChangeViewClass.GetOverState() != True and self._FunctionButtonAreaClass.GetOverState() != True and self._ChangeViewClass.GetOverState() != True and self._BackgroundClass.GetOverState() != True and self._SetDirectionClass.GetOverState() == -1 and self.GetOverState() != True:
					
						self.Add()
					
					else:
						if self._AddState == True:
							self._AddState = False
							self.Set()
				
				elif self._InputClass.GetMouseStartDragState() and self._StencilClass.GetOverState():
					if self._AddState == True:
						self._AddState = False
						self.Set()
				
			#ロールオーバー
			self.Rollover(self._OverState)
			
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,stencilClass,functionButtonAreaClass
				,changeViewClass,backgroundClass,setDirectionClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._StencilClass = stencilClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._ChangeViewClass = changeViewClass
		self._BackgroundClass = backgroundClass
		self._SetDirectionClass = setDirectionClass
		
		self.Reset()
		
	#リセット処理
	def Reset(self):
		self._AddState = False
		self._ClickState = False
		self._IsVisible = True
		self.SetText(False)
		self.SetVisible(True)
		
		self.SetVisible(False)
		
	#設定を反映
	def Set(self):
		if self._AddState == False:
			self.SetText(False)
		else:
			self.SetText(True)
			
	#追加
	def Add(self):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self._Height
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		logNum = self._LogControlClass.Add()
		self._LogControlClass.SetPosition(logNum,pickingPosition)
		self._LogControlClass.SetOrientation(logNum,self._LogAngle)
		
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res

	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		self._Text.visible(val)
		
	#テキスト設定
	def SetText(self,state):
		if state == False:
			self._Text.message("ログ追加")
		else:
			self._Text.message("追加終了")
		
	#フレームの作成
	def CreateFrame(self,position,scale):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.setPosition(position[0],position[1])
		frame.setScale(scale[0],scale[1])
		frame.color(viz.GRAY)
		
		return frame
		
	#テキストの作成
	def CreateText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		text.setPosition(position[0],position[1])
		text.setScale(scale[0],scale[1])
		text.color(viz.WHITE)
		text.drawOrder(1)
		
		return text
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#選択状態の取得
	def GetAddState(self):
		state = self._AddState
		
		return state
		
	#選択状態の設定
	def SetAddState(self,state):
		self._AddState = state
		
		self.Set()
		
	#ログの向き設定
	def SetLogAngle(self,angle):
		self._LogAngle = angle
		