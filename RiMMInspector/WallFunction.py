﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁関係のファンクション制御のクラス定義
class WallFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallFunction] : Init WallFunction')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectWallClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectWallClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectWallClass = selectWallClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallFunction] : Start WallFunction Update')
		pass
		
	#クリック
	def Click(self,num):
		if num == 0:
			self.AddPoint()
		elif num == 1:
			self.SetVisible()
			
	#選択が解除された
	def Hide(self):
		pass
		
	#接合点の追加
	def AddPoint(self):
		roomNum = None
		wallNum = None
		wallState = 0
		
		wallList = []
		
		roomNum,wallNum,wallState = self._SelectWallClass.GetSelectedModel()
		wallList = self._SelectWallClass.GetSelectedModel2()
		
		self._UndoData = []
		self._UndoData.append(17)
		self._UndoData.append([roomNum,wallNum])
		
		self._SelectWallClass.UnSelect()
		self._RoomControlClass.AddWallPoint(roomNum,wallNum)
		
		if wallList != []:
			for wall in wallList:
				self._UndoData.append([wall[0],wall[1]])
				
				self._RoomControlClass.AddWallPoint(wall[0],wall[1])
				
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		#print 'undoData=',self._UndoData
		
	#表示状態の切り替え
	def SetVisible(self):
		roomNum = None
		wallNum = None
		wallState = 0
		
		wallList = []
		
		roomNum,wallNum,wallState = self._SelectWallClass.GetSelectedModel()
		wallList = self._SelectWallClass.GetSelectedModel2()
		
		visibleState = self._RoomControlClass.GetWallVisible(roomNum,wallNum)
		
		self._UndoData = []
		self._UndoData.append(35)
		
		preVisibleState = self._RoomControlClass.GetWallVisible(roomNum,wallNum)
		self._UndoData.append([roomNum,wallNum,preVisibleState])
		
		if visibleState:
			visibleState = False
		else:
			visibleState = True
			
		self._RoomControlClass.SetWallVisible(roomNum,wallNum,visibleState)
		
		if wallList != []:
			for wall in wallList:
				preVisibleState = self._RoomControlClass.GetWallVisible(wall[0],wall[1])
				self._UndoData.append([wall[0],wall[1],preVisibleState])
				
				self._RoomControlClass.SetWallVisible(wall[0],wall[1],visibleState)
				
		self._UndoClass.AddToList(self._UndoData)
		
				