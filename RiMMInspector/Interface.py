﻿# Interface
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2013/10/07
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Interface:
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass
