﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

#DefaultRoomTemp = iniFile.GetItemData('Setting','DefaultRoom')
#DefaultRoomCategory = DefaultRoomTemp.split(',')[0]
#DefaultRoomModel = DefaultRoomTemp.split(',')[1]
DefaultRoomCategory	= 1
DefaultRoomModel	= 5

stencilIniFilePath = iniFile.GetItemData('Setting','Stencil')
stencilIniFile = LoadIniFile.LoadIniFileClass()
stencilIniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+stencilIniFilePath))

#CategoryNameTemp = stencilIniFile.GetItemData('Category',DefaultRoomCategory)
#CategoryName = CategoryNameTemp.split(',')[0]

#DefaultRoomData = stencilIniFile.GetItemData(CategoryName,DefaultRoomModel)
DefaultRoomData = 0

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ショートカットキー制御のクラス定義
class ShortcutKeyControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self,list):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ShortcutKeyControl] : Init ShortcutKeyControl')
		
		self._InputClass = None
		self._ManipulatorClass = None
		self._ItemControlClass = None
		self._RoomControlClass = None
		self._CharacterControlClass = None
		self._PathControlClass = None
		self._SelectModelClass = None
		self._SelectWallClass = None
		self._SelectPathClass = None
		self._AddPathClass = None
		self._PlayPathClass = None
		self._ChangeViewClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._StencilClass = None
		self._TranslateModelClass = None
		self._RotateModelClass = None
		self._ScaleItemClass = None
		
		self._FunctionState = 0
		self._FunctionClassList = list
		
		self._DeleteKeyState = False
		self._PreDeleteKeyState = False
		
		self._CopyKeyState = False
		self._PreCopyKeyState = False
		
		self._PasteKeyState = False
		self._PrePasteKeyState = False
		
		self._UndoKeyState = False
		self._PreUndoKeyState = False
		
		self._RedoKeyState = False
		self._PreRedoKeyState = False
		
		self._VKeyState = False
		self._PreVKeyState = False
		
		self._CKeyState = False
		self._PreCKeyState = False
		
		self._HKeyState = False
		self._PreHKeyState = False
		
		self._NumKeyState = []
		self._PreNumKeyState = []
		for x in range(10):
			self._NumKeyState.append(False)
			self._PreNumKeyState.append(False)
			
		self._CopyItemList = []
		self._CopyRoomList = []
		self._CopyCharacterList = []
		self._CopyDoorList = []
		self._CopyWindowList = []
		
		self._TranslateSnap = translateSnap
		
	#使用するクラスの追加
	def AddClass(self,inputClass,manipulatorClass,itemControlClass,roomControlClass,characterControlClass
				,pathControlClass,selectModelClass,selectWallClass
				,selectPathClass,addPathClass,playPathClass,changeViewClass,undoClass,redoClass
				,stencilClass,translateModelClass,rotateModelClass,scaleItemClass):
		self._InputClass = inputClass
		self._ManipulatorClass = manipulatorClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._CharacterControlClass = characterControlClass
		self._PathControlClass = pathControlClass
		self._SelectModelClass = selectModelClass
		self._SelectWallClass = selectWallClass
		self._SelectPathClass = selectPathClass
		self._AddPathClass = addPathClass
		self._PlayPathClass = playPathClass
		self._ChangeViewClass = changeViewClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._StencilClass = stencilClass
		self._TranslateModelClass = translateModelClass
		self._RotateModelClass = rotateModelClass
		self._ScaleItemClass = scaleItemClass
		
	#毎フレーム呼び出されるための関数
	def Update(self,state):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ShortcutKeyControl] : Start ShortcutKeyControl Update')
		
		self._FunctionState = state
		
		#削除
		self._DeleteKeyState = self._InputClass.GetDeleteKeyState()
		if self._DeleteKeyState == False and self._PreDeleteKeyState == True:
			if self.CanUseShortcutKey():
				self.Delete()
				
		#コピー
		self._CopyKeyState = self._InputClass.GetCKeyState()
		if self._CopyKeyState == True and self._PreCopyKeyState == False and self._InputClass.GetCtrlKeyState():
			if self.CanUseShortcutKey():
				self.Copy()
				
		#ペースト
		self._PasteKeyState = self._InputClass.GetVKeyState()
		if self._PasteKeyState == True and self._PrePasteKeyState == False and self._InputClass.GetCtrlKeyState():
			if self.CanUseShortcutKey():
				self.Paste()
				
		"""
		#接合点追加
		if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
			self._VKeyState = self._InputClass.GetVKeyState()
			if self._VKeyState == True and self._PreVKeyState == False and self._InputClass.GetCtrlKeyState() == False:
				self.AddPoint()
					
		#間取り追加（間取りの左上がマウスの位置に来るように配置）
		if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
			self._CKeyState = self._InputClass.GetCKeyState()
			if self._CKeyState == True and self._PreCKeyState == False and self._InputClass.GetCtrlKeyState() == False:
				if self.CanUseShortcutKey():
					self.AddRoom()
					
		#壁表示設定
		if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
			self._HKeyState = self._InputClass.GetHKeyState()
			if self._HKeyState == True and self._PreHKeyState == False and self._InputClass.GetCtrlKeyState() == False:
				self.SetWallVisible()
		"""
		
		#アンドゥ
		self._UndoKeyState = self._InputClass.GetZKeyState()
		if self._UndoKeyState == True and self._PreUndoKeyState == False and self._InputClass.GetCtrlKeyState():
			if self.CanUseShortcutKey():
				self._UndoClass.Undo()
				
		#リドゥ
		self._RedoKeyState = self._InputClass.GetYKeyState()
		if self._RedoKeyState == True and self._PreRedoKeyState == False and self._InputClass.GetCtrlKeyState():
			if self.CanUseShortcutKey():
				self._RedoClass.Redo()
				
		#ステンシルのカテゴリ切り替え
		if self._ChangeViewClass.GetViewState() == False and self._StencilClass.GetClickState() == False:	#上面図視点で追加中でない時のみ
			self._NumKeyState = self._InputClass.GetNumKeyState()
			if self._NumKeyState != self._PreNumKeyState and self._InputClass.GetCtrlKeyState() == False:
				self.SetStencilCategory()
				
		self._PreDeleteKeyState	= self._DeleteKeyState
		self._PreCopyKeyState	= self._CopyKeyState
		self._PrePasteKeyState	= self._PasteKeyState
		self._PreUndoKeyState	= self._UndoKeyState
		self._PreRedoKeyState	= self._RedoKeyState
		self._PreVKeyState		= self._VKeyState
		self._PreCKeyState		= self._CKeyState
		self._PreHKeyState		= self._HKeyState
		self._PreNumKeyState	= self._NumKeyState
		
	#操作できるか確認
	def CanUseShortcutKey(self):
		if self._SelectModelClass.GetDragState():
			return False
		elif self._AddPathClass.GetAddState():
			return False
		elif self._PlayPathClass.GetPlayState():
			return False
		elif self._TranslateModelClass.GetTranslateState():
			return False
		elif self._RotateModelClass.GetState():
			return False
		elif self._ScaleItemClass.GetState():
			return False

		return True
		
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(25)
			
		itemList	= self._SelectModelClass.GetSelectedModelList()
		roomList	= self._SelectModelClass.GetSelectedRoomList()
		doorList	= self._SelectModelClass.GetSelectedDoorList()
		windowList	= self._SelectModelClass.GetSelectedWindowList()
		characterList = self._SelectModelClass.GetSelectedCharacterList()
		
		function = self._FunctionClassList[self._FunctionState]
		if self._FunctionState == 1:		#アイテム
			function.Click(0)
		elif self._FunctionState == 3:		#柱
			function.Click(0)
		elif self._FunctionState == 4:		#ドア
			function.Click(0)
		elif self._FunctionState == 5:		#窓
			function.Click(1)
		elif self._FunctionState == 6 and len(roomList) == 1 and itemList == [] and doorList == [] and windowList == [] and characterList == []:	#間取り
			function.Click(4)
		elif self._FunctionState == 8:		#キャラクター
			function.Click(0)
		elif self._FunctionState == 9:		#パス
			function.Click(0)
			
		#複数選択中
		itemList	= self._SelectModelClass.GetSelectedModelList()
		roomList	= self._SelectModelClass.GetSelectedRoomList()
		doorList	= self._SelectModelClass.GetSelectedDoorList()
		windowList	= self._SelectModelClass.GetSelectedWindowList()
		characterList = self._SelectModelClass.GetSelectedCharacterList()
		
		#間取りを削除する時はドア、窓の選択を解除
		doorListTemp = []
		for doorData in doorList:
			state = True
			for room in roomList:
				if room == doorData[0]:
					state = False
			if state:
				doorListTemp.append(doorData)
		doorList = doorListTemp
		
		windowListTemp = []
		for windowData in windowList:
			state = True
			for room in roomList:
				if room == windowData[0]:
					state = False
			if state:
				windowListTemp.append(windowData)
		windowList = windowListTemp
		
		#選択を解除
		if itemList != [] or roomList != [] or doorList != [] or windowList != [] or characterList != []:
			self._SelectModelClass.UnSelect()
			
		if itemList != []:
			category = 0
			
			for x in range(len(itemList)):
				itemNum = itemList[x]
				
				file		= self._ItemControlClass.GetFileName(itemNum)
				pos			= self._ItemControlClass.GetPosition(itemNum)
				ori			= self._ItemControlClass.GetOrientation(itemNum)
				size		= self._ItemControlClass.GetSize(itemNum)
				moveY		= self._ItemControlClass.GetMoveYState(itemNum)
				texList		= self._ItemControlClass.GetTextureList(itemNum)
				pathPosList	= self._ItemControlClass.GetPathPositionList(itemNum)

				self._UndoData.append([category,itemNum,file,pos,ori,size,moveY,texList,pathPosList])
				
			for x in range(len(itemList)):
				item = itemList[x] - x
				self._ItemControlClass.Delete(item)
				
		if roomList != []:
			category = 1
			
			for x in range(len(roomList)):
				roomNum = roomList[x]
				
				file,roomName	= self._RoomControlClass.GetFileName(roomNum)
				pos				= self._RoomControlClass.GetFloorPosition(roomNum)
				pointData		= self._RoomControlClass.GetWallPointList(roomNum)
				wallheight		= self._RoomControlClass.GetWallHeight(roomNum)
				texData			= self._RoomControlClass.GetTextureData(roomNum)
				doorData		= self._RoomControlClass.GetDoorData(roomNum)
				windowData		= self._RoomControlClass.GetWindowData(roomNum)
				wallVisible		= self._RoomControlClass.GetWallVisibleList(roomNum)
				isOpRoom		= self._RoomControlClass.GetIsOpRoom(roomNum)
				
				self._UndoData.append([category,roomNum,file,pos,pointData,wallheight,texData,doorData,windowData,wallVisible,isOpRoom])
				
			for x in range(len(roomList)):
				room = roomList[x] - x
				self._RoomControlClass.Delete(room)
				
		if characterList != []:
			category = 2
			
			for x in range(len(characterList)):
				charaNum = characterList[x]
				
				preFile		= self._CharacterControlClass.GetFileName(charaNum)
				prePos		= self._CharacterControlClass.GetPosition(charaNum)
				preOri		= self._CharacterControlClass.GetOrientation(charaNum)
				pathPosList	= self._CharacterControlClass.GetPathPositionList(charaNum)
				
				self._UndoData.append([category,charaNum,preFile,prePos,preOri,pathPosList])
				
			for x in range(len(characterList)):
				character = characterList[x] - x
				self._CharacterControlClass.Delete(character)
				
		if doorList != []:
			category = 3
			
			for x in range(len(doorList)):
				doorData = doorList[x]
				
				doorStateTemp = self._RoomControlClass.GetDoorData(doorData[0])
				doorState = ['',0,0.0]
				doorState[0] = doorStateTemp[doorData[1]][0]
				doorState[1] = doorStateTemp[doorData[1]][1]
				doorState[2] = doorStateTemp[doorData[1]][2]
				
				self._UndoData.append([category,doorData[0],doorData[1],doorState])
			
			roomCount = self._RoomControlClass.GetRoomCount()
			delDoorList = []
			for x in range(roomCount):
				delDoorList.append([])
				
			for x in range(len(doorList)):
				doorData = doorList[x]
				delDoorList[doorData[0]].append(doorData[1])
				
			for x in range(len(delDoorList)):
				delDoorList2 = delDoorList[x]
				for y in range(len(delDoorList2)):
					doorNum = delDoorList2[y] - y
					self._RoomControlClass.DeleteDoor(x,doorNum)
				
		if windowList != []:
			category = 4
			
			for x in range(len(windowList)):
				windowData = windowList[x]
		
				windowStateTemp = self._RoomControlClass.GetWindowData(windowData[0])
				windowState = ['',0.0,0,0.0]
				windowState[0] = windowStateTemp[windowData[1]][0]
				windowState[1] = windowStateTemp[windowData[1]][1]
				windowState[2] = windowStateTemp[windowData[1]][2]
				windowState[3] = windowStateTemp[windowData[1]][3]
				
				self._UndoData.append([category,windowData[0],windowData[1],windowState])
				
			roomCount = self._RoomControlClass.GetRoomCount()
			delWindowList = []
			for x in range(roomCount):
				delWindowList.append([])
				
			for x in range(len(windowList)):
				windowData = windowList[x]
				delWindowList[windowData[0]].append(windowData[1])
				
			for x in range(len(delWindowList)):
				delWindowList2 = delWindowList[x]
				for y in range(len(delWindowList2)):
					windowNum = delWindowList2[y] - y
					self._RoomControlClass.DeleteWindow(x,windowNum)
				
		if itemList != [] or roomList != [] or doorList != [] or windowList != [] or characterList != []:
			print 'self._UndoData',self._UndoData
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
	#コピー
	def Copy(self):
		self.CopyItem()
		self.CopyRoom()
		self.CopyCharacter()
		self.CopyDoor()
		self.CopyWindow()
		
	#アイテムをコピー
	def CopyItem(self):
		self._CopyItemList = []
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		
		for x in range(len(selectedItemList)):
			item = selectedItemList[x]
			
			file	 = self._ItemControlClass.GetFileName(item)
			pos		 = self._ItemControlClass.GetPosition(item)
			ori		 = self._ItemControlClass.GetOrientation(item)
			size	 = self._ItemControlClass.GetSize(item)
			moveY	 = self._ItemControlClass.GetMoveYState(item)
			
			tex = []
			texList = self._ItemControlClass.GetTextureList(item)
			for texData in texList:
				tex.append(texData)
				
			pathPointList = []
			pathData = self._PathControlClass.GetPathData(item,True)
			for x in range(len(pathData)):
				path = self._PathControlClass.GetPath(item,x,True)
				pathPos = path.GetPosition()
				pathPointList.append(pathPos)
				
			self._CopyItemList.append([file,pos,ori,size,moveY,tex,pathPointList])
			
	#間取りをコピー
	def CopyRoom(self):
		self._CopyRoomList = []
		selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
		
		for x in range(len(selectedRoomList)):
			room = selectedRoomList[x]
			
			fileName,roomName	= self._RoomControlClass.GetFileName(room)
			floorPos			= self._RoomControlClass.GetFloorPosition(room)
			wallPointList		= self._RoomControlClass.GetWallPointList(room)
			wallVisibleList		= self._RoomControlClass.GetWallVisibleList(room)
			wallHeight			= self._RoomControlClass.GetWallHeight(room)
			floorTex			= self._RoomControlClass.GetFloorTextureName(room)
			wallTex 			= self._RoomControlClass.GetWallTextureName(room)
			ceilingTex			= self._RoomControlClass.GetCeilingTextureName(room)
			doorList			= self._RoomControlClass.GetDoorList(room)
			windowList			= self._RoomControlClass.GetWindowList(room)
			isOpRoom			= self._RoomControlClass.GetOpRoomState(room)
			
			self._CopyRoomList.append([fileName,roomName,floorPos,wallPointList,wallVisibleList,wallHeight,floorTex,wallTex,ceilingTex,doorList,windowList,isOpRoom])
			
	#キャラクターをコピー
	def CopyCharacter(self):
		self._CopyCharacterList = []
		selectedCharacterList = self._SelectModelClass.GetSelectedCharacterList()
		
		for x in range(len(selectedCharacterList)):
			character = selectedCharacterList[x]
			
			file		= self._CharacterControlClass.GetFileName(character)
			pos			= self._CharacterControlClass.GetPosition(character)
			ori			= self._CharacterControlClass.GetOrientation(character)
			
			pathPointList = []
			pathData	= self._PathControlClass.GetPathData(character)
			for x in range(len(pathData)):
				path = self._PathControlClass.GetPath(character,x)
				pathPos = path.GetPosition()
				pathPointList.append(pathPos)
				
			self._CopyCharacterList.append([file,pos,ori,pathPointList])
			
	#ドアをコピー
	def CopyDoor(self):
		self._CopyDoorList = []
		selectedDoorList = self._SelectModelClass.GetSelectedDoorList()
		
		#間取りをコピーする時はドア、窓の選択を解除
		roomList = self._SelectModelClass.GetSelectedRoomList()
		doorListTemp = []
		for doorData in selectedDoorList:
			state = True
			for room in roomList:
				if room == doorData[0]:
					state = False
			if state:
				doorListTemp.append(doorData)
		selectedDoorList = doorListTemp
		
		for x in range(len(selectedDoorList)):
			doorState = selectedDoorList[x]
			
			doorDataTemp = self._RoomControlClass.GetDoorData(doorState[0])
			doorData = ['',0,0.0]
			doorData[0] = doorDataTemp[doorState[1]][0]
			doorData[1] = doorDataTemp[doorState[1]][1]
			doorData[2] = doorDataTemp[doorState[1]][2]
			
			self._CopyDoorList.append([doorState[0],doorState[1],doorData])
			
	#窓をコピー
	def CopyWindow(self):
		self._CopyWindowList = []
		selectedWindowList = self._SelectModelClass.GetSelectedWindowList()
		
		#間取りをコピーする時はドア、窓の選択を解除
		roomList = self._SelectModelClass.GetSelectedRoomList()
		windowListTemp = []
		for windowData in selectedWindowList:
			state = True
			for room in roomList:
				if room == windowData[0]:
					state = False
			if state:
				windowListTemp.append(windowData)
		selectedWindowList = windowListTemp
		
		for x in range(len(selectedWindowList)):
			windowState = selectedWindowList[x]
			
			windowDataTemp = self._RoomControlClass.GetWindowData(windowState[0])
			windowData = ['',0,0.0]
			windowData[0] = windowDataTemp[windowState[1]][0]
			windowData[1] = windowDataTemp[windowState[1]][1]
			windowData[2] = windowDataTemp[windowState[1]][2]
			
			self._CopyWindowList.append([windowState[0],windowState[1],windowData])
			
	#ペースト
	def Paste(self,mode=False):
		state = 0
		
		if self._CopyItemList != [] or self._CopyRoomList != [] or self._CopyDoorList != [] or self._CopyWindowList != [] or self._CopyCharacterList != []:
			if self._CopyItemList == [] and self._CopyRoomList == [] and self._CopyCharacterList == []:
				roomList = self._SelectModelClass.GetSelectedRoomList()
				if len(roomList) == 1:
					baseRoomNum = self._SelectModelClass.GetBaseRoom()
					roomWallCont = None
					if baseRoomNum != None:
						roomWallCont = self._RoomControlClass.GetWallPointCount(baseRoomNum)
					newRoomWallCount = self._RoomControlClass.GetWallPointCount(roomList[0])
					
					if baseRoomNum != roomList[0] and baseRoomNum != None:
						if roomWallCont == newRoomWallCount:	#他の間取りへコピー
							print 'PastDoorWindowToOtherRoom'
							
							newCopyDoorList =[]
							for x in range(len(self._CopyDoorList)):
								doorState = self._CopyDoorList[x]
								if doorState[0] == baseRoomNum:
									newCopyDoorList.append(doorState)
							self._CopyDoorList = newCopyDoorList
							
							newCopyWindowList =[]
							for x in range(len(self._CopyWindowList)):
								windowState = self._CopyWindowList[x]
								if windowState[0] == baseRoomNum:
									newCopyWindowList.append(windowState)
							self._CopyWindowList = newCopyWindowList
							
							self._UndoData = []
							self._UndoData.append(24)
							
							self.PastDoorWindowToOtherRoom()
							
							self._UndoClass.AddToList(self._UndoData)
							self._RedoClass.ClearList()
							
							state = 1
							
						else:		#何もしない
							state = 2
							
			if state == 0:		#通常のコピー
				print 'Paste'
				self._UndoData = []
				self._UndoData.append(24)
				
				self.PasteItem(mode)
				self.PasteRoom(mode)
				self.PasteCharacter(mode)
				self.PasteDoor(mode)
				self.PasteWindow(mode)
				
				self._UndoClass.AddToList(self._UndoData)
				self._RedoClass.ClearList()
				
	#アイテムをペースト
	def PasteItem(self,mode=False):
		if self._CopyItemList != []:
			category = 0
			
			count = len(self._CopyItemList)
			for x in range(len(self._CopyItemList)):
				itemData = self._CopyItemList[x]
				fileName = itemData[0]
				modelNum = self._ItemControlClass.Add(fileName)
				
				pos = itemData[1]
				newPos = [0,0,0]
				newPos[0] = pos[0]
				newPos[1] = pos[1]
				newPos[2] = pos[2]
				
				if mode == False:	#位置を少しずらす
					newPos[0] = pos[0] + 1.0
					newPos[2] = pos[2] + 1.0
					itemData[1] = newPos
					self._CopyItemList[x] = itemData
					
				self._ItemControlClass.SetPosition(modelNum,newPos)
				self._ItemControlClass.SetOrientation(modelNum,itemData[2])
				"""
				if fileName[-13:] == 'DummyBox.osgb': 		#ダミーボックスのみサイズ設定を反映
					self._ItemControlClass.SetSize(modelNum,itemData[3])
				"""
				self._ItemControlClass.SetSize(modelNum,itemData[3])
				self._ItemControlClass.SetMoveYState(modelNum,itemData[4])
				self._ItemControlClass.AddNewItemToList(modelNum)
				
				newItemNum = self._ItemControlClass.GetItemCount() - 1
				self._UndoData.append([category,newItemNum])
				
				"""
				if fileName[-13:] == 'DummyBox.osgb': 		#ダミーボックスのみサイズ設定を反映
					tex = itemData[5]
					if tex != []:
						for texData in tex:
							self._ItemControlClass.SetTexture(modelNum,texData[0],texData[1])
				"""
				tex = itemData[5]
				if tex != []:
					for texData in tex:
						self._ItemControlClass.SetTexture(modelNum,texData[0],texData[1])
							
				pathPointList = itemData[6]
				for y in range(len(pathPointList)):
					if y != 0:
						path = self._PathControlClass.Add(modelNum,True)
						pos = pathPointList[y]	#位置を少しずらす
						newPos = [0,0,0]
						newPos[0] = pos[0] + 1.0
						newPos[1] = pos[1]
						newPos[2] = pos[2] + 1.0
						self._CopyItemList[x][6][y] = newPos 
						self._PathControlClass.SetPosition(modelNum,path,pathPointList[y],True)
						
			#コピー先を選択
			newItemList = []
			for x in range(count):
				itemCount = self._ItemControlClass.GetItemCount()
				newItemNum = (itemCount)-(count-x)
				newItemList.append(newItemNum)
			self._SelectModelClass.ReSelect(newItemList)
			
	#間取りをペースト
	def PasteRoom(self,mode=False):
		if self._CopyRoomList != []:
			category = 1
			
			count = len(self._CopyRoomList)
			for x in range(len(self._CopyRoomList)):
				roomData = self._CopyRoomList[x]
				fileName = roomData[0]
				
				pos = roomData[2]
				newPos = [0,0,0]
				newPos[0] = pos[0]
				newPos[1] = pos[1]
				newPos[2] = pos[2]
				
				if mode == False:	#位置を少しずらす
					newPos[0] = pos[0] + 1.0
					newPos[2] = pos[2] + 1.0
					roomData[2] = newPos
					
				floorPointList = []
				floorPointListTemp = roomData[3]
				for floorPointTemp in floorPointListTemp:
					floorPoint = [0,0]
					floorPoint[0] = floorPointTemp[0]
					floorPoint[1] = floorPointTemp[1]
					floorPointList.append(floorPoint)
				roomData[3] = floorPointList
				
				wallVisibleList = []
				fwallVisibleListTemp = roomData[4]
				for floorPointTemp in fwallVisibleListTemp:
					visibleState = floorPointTemp
					wallVisibleList.append(visibleState)
				roomData[4] = wallVisibleList
				
				self._CopyRoomList[x] = roomData
				
				roomNum = self._RoomControlClass.Copy(fileName,roomData)
				self._RoomControlClass.AddNewRoomToList(roomNum)
				
				newRoomNum = self._RoomControlClass.GetRoomCount() - 1
				self._UndoData.append([category,newRoomNum])
				
			#コピー先を選択
			if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
				newRoomList = []
				for x in range(count):
					roomCount = self._RoomControlClass.GetRoomCount()
					newRoomNum = (roomCount)-(count-x)
					newRoomList.append(newRoomNum)
				self._SelectModelClass.ReSelectRoom(newRoomList)
				self._SelectModelClass.UnSelectDoor()
				self._SelectModelClass.UnSelectWindow()
				
	#キャラクターをペースト
	def PasteCharacter(self,mode=False):
		if self._CopyCharacterList != []:
			category = 2
			
			count = len(self._CopyCharacterList)
			for x in range(len(self._CopyCharacterList)):
				characterData = self._CopyCharacterList[x]
				fileName = characterData[0]
				modelNum = self._CharacterControlClass.Add(fileName)
				
				pos = characterData[1]
				newPos = [0,0,0]
				newPos[0] = pos[0]
				newPos[1] = pos[1]
				newPos[2] = pos[2]
				
				if mode == False:	#位置を少しずらす
					newPos[0] = pos[0] + 1.0
					newPos[2] = pos[2] + 1.0
					characterData[1] = newPos
					self._CopyCharacterList[x] = characterData
					
				self._CharacterControlClass.SetPosition(modelNum,newPos)
				self._CharacterControlClass.SetOrientation(modelNum,characterData[2])
				
				newCharaNum = self._CharacterControlClass.GetCharacterCount() - 1
				self._UndoData.append([category,newCharaNum])
				
				pathPointList = characterData[3]
				for y in range(len(pathPointList)):
					if y != 0:
						path = self._PathControlClass.Add(modelNum)
						pos = pathPointList[y]	#位置を少しずらす
						newPos = [0,0,0]
						newPos[0] = pos[0] + 1.0
						newPos[1] = pos[1]
						newPos[2] = pos[2] + 1.0
						self._CopyCharacterList[x][3][y] = newPos 
						self._PathControlClass.SetPosition(modelNum,path,pathPointList[y])
						
			#コピー先を選択
			newCharacterList = []
			for x in range(count):
				characterCount = self._CharacterControlClass.GetCharacterCount()
				newCharacterNum = (characterCount)-(count-x)
				newCharacterList.append(newCharacterNum)
			self._SelectModelClass.ReSelectCharacter(newCharacterList)
			
	#ドアをペースト
	def PasteDoor(self,mode=False):
		if self._CopyDoorList != []:
			category = 3
			
			self.SetHole(False)
			
			count = len(self._CopyDoorList)
			for x in range(len(self._CopyDoorList)):
				doorState = self._CopyDoorList[x]
				roomNum		= doorState[0]
				doorNum		= doorState[1]
				doorData	= doorState[2]
				
				self._CopyDoorList[x] = [roomNum,doorNum,doorData]
				
				self._RoomControlClass.CopyDoor(roomNum,doorNum)
				
				doorCount = self._RoomControlClass.GetDoorCount(roomNum)
				newDoorNum = doorCount - 1
				self._UndoData.append([category,roomNum,newDoorNum])
				
			self.SetHole(True)
			
			#コピー先を選択
			if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
				newDoorList = []
				for x in range(len(self._UndoData)):
					if x != 0:
						if self._UndoData[x][0] == 3:
							newDoorList.append([self._UndoData[x][1],self._UndoData[x][2]])
						
							self._RoomControlClass.SetDoorVisible(self._UndoData[x][1],False)
						
				self._SelectModelClass.ReSelectDoor(newDoorList)
				
	#窓をペースト
	def PasteWindow(self,mode=False):
		if self._CopyWindowList != []:
			category = 4
			
			self.SetHole(False)
			
			count = len(self._CopyWindowList)
			for x in range(len(self._CopyWindowList)):
				windowState = self._CopyWindowList[x]
				roomNum		= windowState[0]
				windowNum	= windowState[1]
				windowData	= windowState[2]
				
				self._CopyWindowList[x] = [roomNum,windowNum,windowData]
				
				self._RoomControlClass.CopyWindow(roomNum,windowNum)
				
				windowCount = self._RoomControlClass.GetWindowCount(roomNum)
				newWindowNum = windowCount - 1
				self._UndoData.append([category,roomNum,newWindowNum])
				
			self.SetHole(True)
			
			#コピー先を選択
			if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
				newWindowList = []
				for x in range(len(self._UndoData)):
					if x != 0:
						if self._UndoData[x][0] == 4:
							newWindowList.append([self._UndoData[x][1],self._UndoData[x][2]])
						
							self._RoomControlClass.SetWindowVisible(self._UndoData[x][1],False)
						
				self._SelectModelClass.ReSelectWindow(newWindowList)
				
	#ドア、窓を他の間取りにペースト
	def PastDoorWindowToOtherRoom(self):
		roomList = self._SelectModelClass.GetSelectedRoomList()
		newRoomNum = roomList[0]
		
		self.SetHole(False)
			
		#ドア
		if self._CopyDoorList != []:
			category = 3
			
			count = len(self._CopyDoorList)
			for x in range(len(self._CopyDoorList)):
				doorState = self._CopyDoorList[x]
				roomNum		= doorState[0]
				doorNum		= doorState[1]
				doorData	= doorState[2]
				
				self._CopyDoorList[x] = [roomNum,doorNum,doorData]
				
				self._RoomControlClass.CopyDoorToOtherRoom(roomNum,doorNum,newRoomNum)
				
				doorCount = self._RoomControlClass.GetDoorCount(newRoomNum)
				newDoorNum = doorCount - 1
				self._UndoData.append([category,newRoomNum,newDoorNum])
				
			#コピー先を選択
			if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
				newDoorList = []
				for x in range(len(self._UndoData)):
					if x != 0:
						if self._UndoData[x][0] == 3:
							newDoorList.append([self._UndoData[x][1],self._UndoData[x][2]])
		
							self._RoomControlClass.SetDoorVisible(self._UndoData[x][1],False)
				
				self._SelectModelClass.ReSelectDoor(newDoorList)
				
		#窓
		if self._CopyWindowList != []:
			category = 4
			
			count = len(self._CopyWindowList)
			for x in range(len(self._CopyWindowList)):
				windowState = self._CopyWindowList[x]
				roomNum		= windowState[0]
				windowNum	= windowState[1]
				windowData	= windowState[2]
				
				self._CopyWindowList[x] = [roomNum,windowNum,windowData]
				
				self._RoomControlClass.CopyWindowToOtherRoom(roomNum,windowNum,newRoomNum)
				
				windowCount = self._RoomControlClass.GetWindowCount(newRoomNum)
				newWindowNum = windowCount - 1
				self._UndoData.append([category,newRoomNum,newWindowNum])
				
			#コピー先を選択
			if self._ChangeViewClass.GetViewState() == False:	#上面図視点時のみ
				newWindowList = []
				for x in range(len(self._UndoData)):
					if x != 0:
						if self._UndoData[x][0] == 4:
							newWindowList.append([self._UndoData[x][1],self._UndoData[x][2]])
						
							self._RoomControlClass.SetWindowVisible(self._UndoData[x][1],False)
				
				self._SelectModelClass.ReSelectWindow(newWindowList)
				
		self.SetHole(True)
		
		if self._CopyDoorList != [] or self._CopyWindowList != []:
			self._SelectModelClass.UnSelectRoom()
				
	#接合点追加
	def AddPoint(self):
		roomNum,wallNum,wallState = self._SelectWallClass.GetSelectedModel()
		if wallNum != None:
			isOpRoom = self._RoomControlClass.GetOpRoomState(roomNum)
			if isOpRoom == 0:
				function = self._FunctionClassList[self._FunctionState]
				if self._FunctionState == 2:		#壁
					function.Click(0)
					
	#間取り追加（間取りの左上がマウスの位置に来るように配置）
	def AddRoom(self):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		
		modelName = DefaultRoomData.split(',')[0]
		
		roomNum = self._RoomControlClass.Add(modelName)
		floorModel = self._RoomControlClass.GetFloorModel(roomNum)
		box = floorModel.getBoundingBox()
		modelSize = box.size
		
		pos = [0,0,0]
		pos[0] = pickingPosition[0] + modelSize[0] * 0.5
		pos[1] = pickingPosition[1]
		pos[2] = pickingPosition[2] - modelSize[2] * 0.5
		
		#スナップ
		pos[0] = self.Snap(pos[0])
		pos[2] = self.Snap(pos[2])
			
		self._RoomControlClass.SetFloorPosition(roomNum,pos)
		self._RoomControlClass.AddNewRoomToList(roomNum)
		
		self._SelectModelClass.SelectNewestRoom()
		
		self._UndoData = []
		self._UndoData.append(15)
		self._UndoData.append([roomNum,modelName,pos])
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#壁表示設定
	def SetWallVisible(self):
		roomNum,wallNum,wallState = self._SelectWallClass.GetSelectedModel()
		if wallNum != None:
			function = self._FunctionClassList[self._FunctionState]
			if self._FunctionState == 2:		#壁
				function.Click(1)
				
	#ステンシルのカテゴリ設定
	def SetStencilCategory(self):
		num = -1
		for x in range(10):
			state = self._NumKeyState[x]
			preState = self._PreNumKeyState[x]
			if state == True and preState == False:
				num = x
			
		if num != -1:
			self._StencilClass.SetCategory(num)
		
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
		
	#ダミードア、窓を設定
	def SetHole(self,isAdd):
		if self._ChangeViewClass.GetViewState():
			if isAdd:
				self._RoomControlClass.AddAllDoorHole()			#ダミードアを作成
				self._RoomControlClass.AddAllWindowHole()		#ダミー窓を作成
				
			else:
				self._RoomControlClass.DeleteAllDoorHole()		#ダミードアを削除
				self._RoomControlClass.DeleteAllWindowHole()	#ダミー窓を削除
				