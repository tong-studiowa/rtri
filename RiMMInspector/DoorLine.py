﻿# DoorLine
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/1
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドアのライン制御のクラス定義
class DoorLine(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorLine] : Init DoorLine')
		
		self._DoorClass = None
		
		self._Model = None
		self._Picking = False
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		
		self._WallHeight = 3
		
		self._Width = 0.15
		self._Color = [0.50,0.75,0.50]
		
		self._Id = None
		self._DummyFlag = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorLine] : Start DoorLine Update')
		pass
		
	#追加
	def Add(self,doorClass):
		self._DoorClass = doorClass
		
		if self._Model != None:
			self.Delete()
		
		doorModel = self._DoorClass.GetModel()
		box = doorModel.getBoundingBox()
		modelSize = box.size
		modelCenter = box.center
		
		model = vizshape.addPlane([1.0,1.0])
		model.setScale([modelSize[0],1.0,self._Width])
		model.setPosition(0,self._WallHeight+0.2,0)
		model.disable(viz.LIGHTING)
		model.color(self._Color)
		
		self._Model = model
		
		self.SetPickable(False)
		self.SetHighlight(False)
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			
			position = iPosition
			
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = 92.0
			position[2] = position[2] + self._PositionOffset[2]
			
			self._Model.setPosition(position)
			self._Position = position
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		if self._DummyFlag:
			self._Model.visible(False)	#ダミーは非表示
		else:
			self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
		else:
			self._Model.color(self._Color)
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
	
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		