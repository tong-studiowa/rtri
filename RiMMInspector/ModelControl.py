﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モデル読み込み関係の制御用クラス
class ModelControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ModelControl] : Init ModelControl')
		self._ModelNameList = []
		self._ModelList = []
		
		self._CharacterNameList = []
		self._CharacterList = []
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ModelControl] : Start ModelControl Update')
		pass
		
	#リセット
	def Reset(self):
		pass
		
	#追加
	def Add(self,fileName):
		model = None
		
		#読み込み済みの場合はコピー
		for x in range(len(self._ModelNameList)):
			modelOriginalName = self._ModelNameList[x]
			if fileName == modelOriginalName:
				modelOriginal = self._ModelList[x]
				
				model = modelOriginal.copy()
					
		#まだ読み込んでいない場合は読み込む
		if model == None:
			modelOriginal = self.Load(fileName)
			
			if modelOriginal != None:
				modelOriginal.setPosition(0,-1000,0)
				modelOriginal.visible(viz.OFF)
				modelOriginal.disable(viz.PICKING)
				modelOriginal.disable(viz.ANIMATIONS)
				modelOriginal.setAnimationFrame(0)
				
				self._ModelNameList.append(fileName)
				self._ModelList.append(modelOriginal)
				
				model = modelOriginal.copy()
			
		if model == None:
			model.visible(viz.ON)
			modelOriginal.enable(viz.PICKING)
			model.disable(viz.ANIMATIONS)
			model.setAnimationFrame(0)
			
		return model
			
	#読み込み
	def Load(self,fileName):
		model = None
		
		try:
			#model = vizfx.addChild(filePath)	#モデルを追加
			model = viz.addChild(fileName)
		except:
			model = None
			
		return model
		
	#キャラクターの追加
	def AddCharacter(self,fileName):
		character = None
		
		#読み込み済みの場合はコピー
		for x in range(len(self._CharacterNameList)):
			characterOriginalName = self._CharacterNameList[x]
			if fileName == characterOriginalName:
				characterOriginal = self._CharacterList[x]
				
				character = characterOriginal.copy()
					
		#まだ読み込んでいない場合は読み込む
		if character == None:
			characterOriginal = self.Load(fileName)
			
			if characterOriginal != None:
				characterOriginal.setPosition(0,-1000,0)
				characterOriginal.visible(viz.OFF)
				characterOriginal.disable(viz.PICKING)
				characterOriginal.state(0)
				
				self._CharacterNameList.append(fileName)
				self._CharacterList.append(characterOriginal)
				
				character = characterOriginal.copy()
			
		if character == None:
			character.visible(viz.ON)
			character.enable(viz.PICKING)
			character.state(0)
			
		return character
			
	#キャラクターの読み込み
	def LoadCharacter(self,fileName):
		character = None
		
		try:
			character = viz.addAvatar(fileName)
		except:
			character = None
			
		return character
		