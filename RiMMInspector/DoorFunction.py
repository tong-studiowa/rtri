﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドア関係のファンクション制御のクラス定義
class DoorFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorFunction] : Init DoorFunction')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectModelClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectModelClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectModelClass = selectModelClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorFunction] : Start DoorFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.Delete()
		elif num == 1:
			self.Copy()
	
	#選択が解除された
	def Hide(self):
		pass
		
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(20)
		
		doorList = self._SelectModelClass.GetSelectedDoorList()
		
		self._SelectModelClass.UnSelectDoor()
		
		if doorList != []:
			for x in range(len(doorList)):
				doorData = doorList[x]
		
				doorStateTemp = self._RoomControlClass.GetDoorData(doorData[0])
				doorState = ['',0,0.0]
				doorState[0] = doorStateTemp[doorData[1]][0]
				doorState[1] = doorStateTemp[doorData[1]][1]
				doorState[2] = doorStateTemp[doorData[1]][2]
				
				self._UndoData.append([doorData[0],doorData[1],doorState])
				
			roomCount = self._RoomControlClass.GetRoomCount()
			delDoorList = []
			for x in range(roomCount):
				delDoorList.append([])
				
			for x in range(len(doorList)):
				doorData = doorList[x]
				delDoorList[doorData[0]].append(doorData[1])
				
			for x in range(len(delDoorList)):
				delDoorList2 = delDoorList[x]
				for y in range(len(delDoorList2)):
					doorNum = delDoorList2[y] - y
					self._RoomControlClass.DeleteDoor(x,doorNum)
					
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#コピー
	def Copy(self):
		self._UndoData = []
		self._UndoData.append(19)
		
		doorList = self._SelectModelClass.GetSelectedDoorList()
		
		count = len(doorList)
		if doorList != []:
			for x in range(len(doorList)):
				doorData = doorList[x]
				
				self._RoomControlClass.CopyDoor(doorData[0],doorData[1])
				
				doorCount = self._RoomControlClass.GetDoorCount(doorData[0])
				newDoorNum = doorCount - 1
				self._UndoData.append([doorData[0],newDoorNum])
		
				self._RoomControlClass.SetDoorVisible(doorData[0],False)
		
		#コピー先を選択
		newDoorList = []
		for x in range(len(self._UndoData)):
			if x != 0:
				newDoorList.append(self._UndoData[x])
		self._SelectModelClass.ReSelectDoor(newDoorList)
		
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		