﻿# coding: utf-8
#PathSimulator

import viz

class Display2D():
	def __init__(self):
		#画像の表示用オブジェクトを作成する
		self._mObject2D = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_LEFT_TOP,size=1)
		self._mTexture = None			#画像データ
		self._mTextureSize = [1.0,1.0]	#画像の表示用オブジェクトのサイズ(pixel)
		self._mPosition = [0.0,0.0]
		self._mWindowSize = [0,0]
		self._mKeepSize = 0
	
	def __del__(self):
		if self._mObject2D:
			self._mObject2D.remove()	#画像の表示用オブジェクトを削除
		if self._mTexture:
			self._mTexture.remove()		#画像データを削除
	
	def SetRenderToAllWindowsExcept(self,list):
		if self._mObject2D:
			self._mObject2D.renderToAllWindowsExcept(list)
	
	def SetRenderOnlyToWindows(self,list):
		if self._mObject2D:
			self._mObject2D.renderOnlyToWindows(list)
	
	def SetAlignment(self,align):
		if self._mObject2D:
			self._mObject2D.alignment = align
	
	def SetTexture(self,texturePath):
		#古いテクスチャの削除
		if self._mTexture:
			if self._mObject2D:
				self._mObject2D.texture(None)
			self._mTexture.remove()
		#新しいテクスチャの設定
		absPath = viz.res.getPublishedPath(texturePath)
		self._mTexture = viz.addTexture(absPath)
		if self._mObject2D:
			self._mObject2D.texture(self._mTexture)
	
	def SetColor(self,color):
		if color!=None:
			if self._mObject2D:
				self._mObject2D.color(color)
	
	def SetAlpha(self,alpha):
		if alpha!=None:
			if self._mObject2D:
				self._mObject2D.alpha(float(alpha))
	
	def SetPosition(self,x,y):
		self._mWindowSize = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		#オブジェクトの表示位置を設定する
		if self._mObject2D:
			self._mObject2D.setPosition((x)/self._mWindowSize[0],
										1.0-(y)/self._mWindowSize[1])
		self._mPosition = [x,y]
	
	def SetPosion2(self,x,y,windowSize):
		self._mWindowSize = windowSize
		#オブジェクトの表示位置を設定する
		if self._mObject2D:
			self._mObject2D.setPosition((x)/self._mWindowSize[0],
										1.0-(y)/self._mWindowSize[1])
		self._mPosition = [x,y]
	
	def SetSize(self,width,height):
		WINDOW_SIZE = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		self._ResetSize()	#画像のサイズをリセットする
		self._mTextureSize=[width*(1280.0/WINDOW_SIZE[0]),
							height*(1024.0/WINDOW_SIZE[1])]
		if self._mObject2D:
			self._mObject2D.setScale(self._mTextureSize[0],
									 self._mTextureSize[1])	#サイズを設定
									 
	def SetSize2(self,width,height,windowSize):
		WINDOW_SIZE = windowSize
		self._ResetSize()	#画像のサイズをリセットする
		self._mTextureSize=[width *(1280.0/WINDOW_SIZE[0]),
							height*(1024.0/WINDOW_SIZE[1])]
		if self._mObject2D:
			self._mObject2D.setScale(self._mTextureSize[0],
									 self._mTextureSize[1])	#サイズを設定
									 
	def GetModel(self):
		return self._mObject2D
	
	def GetPosition(self):
		return self._mPosition
	
	def GetSize(self):
		return self._mTextureSize
	
	def SetPickable(self,isPicking=True):
		if isPicking:
			self._mObject2D.enable(viz.PICKING)
		else:
			self._mObject2D.disable(viz.PICKING)
	
	def _ResetSize(self):
		SizeX = 1.0/self._mTextureSize[0]	#現在のサイズから、1pixel*1pixelに戻す
		SizeY = 1.0/self._mTextureSize[1]
		if self._mObject2D:
			self._mObject2D.setScale(SizeX,SizeY)
	
	def SetRenderOrder(self,orderVal):
	#レンダーオーダーを設定する
	#　---高い値になるほど、後に描画される（2Dモデルを最前面に表示したければこの値を高くする）
		if orderVal!=None:
			if self._mObject2D:
				self._mObject2D.drawOrder(int(orderVal))
	
	def SetVisible(self,isVisible):
		if self._mObject2D:
			if isVisible:
				self._mObject2D.enable(viz.RENDERING)
				self.SetPickable(True)
			else:
				self._mObject2D.disable(viz.RENDERING)
				self.SetPickable(False)
	
	def SetRatio(self,per):
		WINDOW_SIZE = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		OFFSET = [WINDOW_SIZE[0]-self._mWindowSize[0],WINDOW_SIZE[1]-self._mWindowSize[1]]
		self._ResetSize()
		if self._mObject2D:
			if self._mKeepSize == 1:	#メニューバーは横幅を一定のサイズに固定
				dispSize = viz.window.getSize()
				self._mTextureSize[0] = (dispSize[0]-(0.0565*(1280.0/1024.0)*1024)-12.0)*(1280.0/1024.0)
			self._mObject2D.setScale(self._mTextureSize[0]*per[0],
									 self._mTextureSize[1]*per[1])
			self._mObject2D.setPosition(self._mPosition[0]/WINDOW_SIZE[0],
										1.0-(self._mPosition[1]+OFFSET[1])/WINDOW_SIZE[1])

	def SetkeepSize(self,keep):
		self._mKeepSize = keep
		
#　テスト用プログラム
#-------------------------------
if __name__ == '__main__':
	
	viz.go()

	disp1 = Display2D()
	disp1.SetPosion(100,100)
	disp1.SetSize(100,100)
	disp1.SetTexture('Jellyfish.jpg')
	disp1.SetRenderOrder(15)

	disp2 = Display2D()
	disp2.SetPosion(200,200)
	disp2.SetSize(150,150)
	disp2.SetTexture('Lighthouse.jpg')
	disp2.SetRenderOrder(5)