﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#スタート位置回転制御のクラス定義
class RotateStartPosition(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateStartPosition] : Init RotateStartPosition')
		
		self._InputClass = None
		self._StartPositionClass = None
		self._SelectStartPositionClass = None
		self._ManipulatorClass = None
		
		self._SelectedState = False
		
		self._RotateVal = 0.0
		self._AngleVal = 0.0
		self._PreAngleVal = 0.0
		
		self._State = False
		self._PreState = False
		
	#使用するクラスの追加
	def AddClass(self,inputClass,startPositionClass,selectStartPositionClass,manipulatorClass):
		self._InputClass = inputClass
		self._StartPositionClass = startPositionClass
		self._SelectStartPositionClass = selectStartPositionClass
		self._ManipulatorClass = manipulatorClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateStartPosition] : Start RotateStartPosition Update')
		
		self._State = False
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState == 2:
			self._SelectedState = self._SelectStartPositionClass.GetSelectedState()
			if self._SelectedState:
				self.Rotate()
				self._State = True
		else:
			self._RotateVal = 0.0

		if self._State == False and self._PreState == True:
			self.ResetOrientationSnap()
			
		self._PreState = self._State
		
	#回転
	def Rotate(self):
		modelPos = self._StartPositionClass.GetPosition()
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		posX = modelPos[0] - pickingPosition[0]
		posZ = modelPos[2] - pickingPosition[2]
		
		angleTemp = math.atan2(posX,posZ)
		angle = math.degrees(angleTemp)
		
		self._AngleVal = angle
		
		rotateValTemp = self._AngleVal - self._PreAngleVal
		if rotateValTemp > -10.0 and rotateValTemp < 10:
			self._RotateVal = self._AngleVal - self._PreAngleVal
		
		modelAngle = self._StartPositionClass.GetOrientation()
		newModelAngle = modelAngle + self._RotateVal
		
		if newModelAngle < 0:
			newModelAngle = newModelAngle + 360
		elif newModelAngle > 360:
			newModelAngle = newModelAngle - 360
			
		self._StartPositionClass.SetOrientation(newModelAngle)
		
		self._PreAngleVal = angle
		
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		self._StartPositionClass.ResetOrientationSnap()
		