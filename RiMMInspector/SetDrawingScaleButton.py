﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizinput
import vizmat

import Object2d

import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面設定ボタン制御のクラス定義
class SetDrawingScaleButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDrawingScaleButton] : Init SetDrawingScaleButton')
		
		self._InputClass = None
		self._DrawingClass = None
		self._CheckStateClass = None
		self._StencilClass = None
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		self._PreVisibleState = True
		
		self._PreKeyState = False
		self._PreForcus = False
		
		self._SettingState = False
		
		#モデル関係
		self._BallState = 0
		self._BallList = []
		
		for x in range(2):
			ball = vizshape.addSphere(0.1)
			ball.disable(viz.LIGHTING)
			ball.color(viz.RED)
			ball.disable(viz.PICKING)
			
			self._BallList.append(ball)
		
		self.SetBallVisible(False)
		
		#メニュー関係
		self._IconCount = 2
		
		self._Frame = None
		self._TextList = []
		self._IconList = []
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [819,windowSize[1]-127]
		self._FrameScale = [210,127]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#テキスト追加
		self._TextPositionX = 819
		self._TextPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._TextScale = [18.6,18.6]
		self._TextMessage = ["決定","キャンセル"]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			message = self._TextMessage[x]
			
			text = self.CreateText(message,position,self._TextScale)
			self._TextList.append(text)
			
		#アイコン追加
		self._IconPositionX = 819
		self._IconPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._IconScale = [200,36]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._IconPositionX
			position[1] = self._IconPositionY[x]
			
			icon = self.CreateIcon(position,self._IconScale)
			self._IconList.append(icon)
			
		self._Title = None
		self._TextBox = None
		self._DummyText = None
		
		self._Value = 1.0
		self._DeraultValue = 1.0
		
		#タイトル追加
		self._TitlePosition = [752,windowSize[1]-167]
		self._TitleScale = [18.6,18.6]
		
		self._Title = self.CreateText("寸法",self._TitlePosition,self._TitleScale)
		
		#テキストボックス追加
		self._TextBoxPosition = [853,windowSize[1]-167]
		self._TextBoxScale = [132,36]
		
		self._TextBox = self.CreateTextBox(self._TextBoxPosition,self._TextBoxScale)
		self.SetText()
		
		#ダミーテキスト追加
		self._DummyTextPosition = [795,windowSize[1]-167]
		self._DummyTextScale = [13,27]
		
		self._DummyText = self.CreateDummyText('1000.0',self._DummyTextPosition,self._DummyTextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,drawingClass,checkStateClass,stencilClass):
		self._InputClass = inputClass
		self._DrawingClass = drawingClass
		self._CheckStateClass = checkStateClass
		self._StencilClass = stencilClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDrawingScaleButton] : Start SetDrawingScaleButton Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#マウスの重なり状態を確認
		if self._VisibleState == True and self._PreVisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				num = None
				for x in range(self._IconCount):
					if self._IconList[x].getVisible():
						if self.CheckRollOver(mousePosition,[self._IconPositionX,self._IconPositionY[x]],self._IconScale):
							num = x
							
				if num != None:
					self._OverIconState = num
					
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.Set()		#決定
						elif num == 1:
							self.Cancel()	#キャンセル
			
			else:
				#シングルクリックによる選択
				if self._InputClass.GetMouseClickState():
					if self._CheckStateClass.GetOverState() != True:
						state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
						
						pos = [0,0.1,0]
						pos[0] = pickingPosition[0]
						pos[2] = pickingPosition[2]
						
						ball = self._BallList[self._BallState]
						ball.setPosition(pos)
						
						if self._BallState == 0:
							self._BallState = 1
						else:
							self._BallState = 0
				
					else:	#他のボタンが押されたらキャンセル
						self.Cancel()
			
			if self._StencilClass.GetClickState():	#ステンシルを操作したらキャンセル
				self.Cancel()
				
			#ロールオーバー
			self.Rollover(self._OverIconState)
			
		self.InputVal()
		
		self._PreVisibleState = self._VisibleState
		
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
		self.SetValue(self._DeraultValue)
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			val = viz.ON
			
			#表示したらフォーカス
			#self.SetTextBoxForcus()
			
		else:
			val = viz.OFF
			
		self._Frame.visible(val)
		
		for text in self._TextList:
			text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
		
		self._Title.visible(val)
		
		self.SetValue(self._DeraultValue)
		self._TextBox.visible(val)
		self._DummyText.visible(val)
		
		self.SetBallVisible(state)
		
		#パラメータ設定
		self._VisibleState = state
		self._SettingState = state
			
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#ボールの表示状態を設定
	def SetBallVisible(self,state):
		val = viz.OFF
		if state == True:
			val = viz.ON
		else:
			val = viz.OFF
			
		for ball in self._BallList:
			ball.visible(val)
			ball.setPosition([0,0.1,0])
		
	#数値を設定
	def SetValue(self,val):
		self._Value = val
		self.SetText()
		
	#数値を取得
	def GetValue(self):
		val = self._Value
		
		return val
		
	#数値の表示を更新
	def SetText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(val)
		self._TextBox.message(message)
	
	#ダミーテキストを表示
	def SetDummyText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(round(val,3))
		self._DummyText.message(message)
	
		self._TextBox.message('')
		
	#文字入力
	def InputVal(self):
		keyState = False
		forcus = False
		
		if self._VisibleState == True:
			keyState = False
			if viz.key.isDown(viz.KEY_RETURN):
				keyState = True
			if viz.key.isDown(viz.KEY_KP_ENTER):
				keyState = True
				
			if keyState:
				inputVal = self._TextBox.getMessage()
				preVal = self.GetValue()
				val = 1.0
				
				try:
					val = float(inputVal) * 0.001	#入力時はミリメートル → メートル
				except:
					val = preVal
				
				if val != preVal:
					self.SetValue(val)
					
			forcus = self.GetTextBoxForcus()
			if forcus != self._PreForcus and keyState == 0:
				preVal = self.GetValue()
				self.SetValue(preVal)
				
				if forcus:
					self.SetDummyText()
					
			inputValTemp = self._TextBox.getMessage()
			if len(inputValTemp) != 0:
				self._DummyText.message('')
				
		self._PreKeyState = keyState
		self._PreForcus = self.GetTextBoxForcus()
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#設定状態の取得
	def GetSettingState(self):
		state = self._SettingState
		return state
	
	#決定
	def Set(self):
		ballA = self._BallList[0]
		ballB = self._BallList[1]
		ballDistance = vizmat.Distance(ballA.getPosition(),ballB.getPosition())		#図面上の寸法
		
		val = self._Value	#実際の寸法
		drawingScale = self._DrawingClass.GetScale()	#図面のサイズ
		
		if ballDistance != 0.0 and val != 0.0:
			scaleTemp = 1.0 / ballDistance * val * drawingScale
			scale = round(scaleTemp,4)	#小数点以下4桁まで
			self._DrawingClass.SetScale(scale)
		
		self.SetVisible(False)
			
	#キャンセル
	def Cancel(self):
		self.SetVisible(False)
			
	#ロールオーバー処理
	def Rollover(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
			
	#テキストボックスのフォーカスを設定
	def SetTextBoxForcus(self):
		self.SetDummyText()
		self._TextBox.setFocus(viz.ON)
			
	def GetTextBoxForcus(self):
		state = self._TextBox.getFocus()
		return state
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [819,windowSize[1]-127]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TextPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._IconPositionY = [windowSize[1]-127, windowSize[1]-87]
		
		for x in range(self._IconCount):
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			self.SetIconPosition(self._TextList[x],position,self._TextScale)
			self.SetIconPosition(self._IconList[x],position,self._IconScale)
			
		self._TitlePosition = [752,windowSize[1]-167]
		self.SetIconPosition(self._Title,self._TitlePosition,self._TitleScale)
		
		self._TextBoxPosition = [853,windowSize[1]-167]
		self.SetIconPosition(self._TextBox,self._TextBoxPosition,self._TextBoxScale,True)
		
		self._DummyTextPosition = [795,windowSize[1]-167]
		self.SetIconPosition(self._DummyText,self._DummyTextPosition,self._DummyTextScale)
		