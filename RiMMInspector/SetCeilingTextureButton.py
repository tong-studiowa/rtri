﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Object2d

import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#天井のテクスチャ設定ボタン制御のクラス定義
class SetCeilingTextureButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetCeilingTextureButton] : Init SetCeilingTextureButton')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._VisibleState = True
		
		self._Frame = None
		self._Text = None
		self._Icon = None
		
		self._SelectedRoomList = []
		self._Value = None
		
		self._Filter = [('All Files (JPG,BMP,TGA,PNG,TIFF)','*.jpg;*.jpeg;*.bmp;*.tga;*.png;*.tif')
					   ,('JPG Files','*.jpg;*.jpeg'),('BMP Files','*.bmp')
					   ,('TGA Files','*.tga')
					   ,('PNG Files','*.png')
					   ,('TIFF Files','*.tif')]
		self._Directory = self.GetRootPath() + 'Resource\\Textures\\Ceiling\\.'

		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [794,windowSize[1]-115]
		self._FrameScale = [170,103]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#タイトル追加
		self._TitlePosition = [742,windowSize[1]-115]
		self._TitleScale = [18.6,18.6]
		
		self._Title = self.CreateText("天井",self._TitlePosition,self._TitleScale)
		
		#アイコン追加
		self._IconPosition = [825,windowSize[1]-115]
		self._IconScale = [100,94]
		
		self._Icon = self.CreateIcon(self._IconPosition,self._IconScale)
		self.SetTexture()
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetCeilingTextureButton] : Start SetCeilingTextureButton Update')
		
		self._ClickState = False
		self._OverState = False
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				if self._InputClass.GetMouseClickState():
					self._ClickState = True
					self.LoadTexture()
						
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		self._Title.visible(val)
		self._Icon.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#数値を設定
	def SetValue(self,roomList,val):
		self._SelectedRoomList = roomList
		self._Value = val
		self.SetTexture()
		
	#数値を取得
	def GetValue(self):
		roomList = self._SelectedRoomList
		val = self._Value
		
		return roomList,val
		
	#テクスチャを更新
	def SetTexture(self):
		texture = self._Value
		self._Icon.texture(texture)
	
	#テクスチャを読み込み
	def LoadTexture(self):
		filePath = vizinput.fileOpen(filter=self._Filter,file=self._Directory)
		try:
			filePath.encode("shift_jis")
		except:
			vizinput.message("ファイルパスに機種依存文字が含まれています。")
			filePath = ""
		
		preRoomList,preVal = self.GetValue()
		val = None
		
		if filePath != "":
			try:
				val = viz.addTexture(filePath)
			except:
				val = preVal
			
			if val != preVal:
				self.SetValue(preRoomList,val)
				
				self._UndoData = []
				self._UndoData.append(7)
				
				#相対パスに変換
				rootPath = self.GetRootPath()
				rootCount = len(rootPath)
				fileCount = len(filePath)
				if rootPath == filePath[:rootCount]:
					filePath = '..\\' + filePath[-(fileCount-rootCount):]
				
				#選択中の部屋に情報を渡す
				for room in self._SelectedRoomList:	
					preTexture = self._RoomControlClass.GetCeilingTexture(room)
					preTexFile,preTexSize = self._RoomControlClass.GetCeilingTextureSetting(room)

					self._RoomControlClass.SetCeilingTexture(room,self._Value,filePath)
					
					self._UndoData.append([room,preTexture,preTexFile])
					
				self._UndoClass.AddToList(self._UndoData)
				self._RedoClass.ClearList()
				#print 'undoData=',self._UndoData
				
	#ルートフォルダの取得
	def GetRootPath(self):
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
			exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
			
		return rootDir
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [794,windowSize[1]-115]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TitlePosition = [742,windowSize[1]-115]
		self.SetIconPosition(self._Title,self._TitlePosition,self._TitleScale)
		
		self._IconPosition = [825,windowSize[1]-115]
		self.SetIconPosition(self._Icon,self._IconPosition,self._IconScale)
		