﻿# coding: utf-8
#PathSimulator

import viz
import LoadIniFile
from win32api import OutputDebugString

preWindowSetting = [[0,0],[0.0],1,1]

def GetDisplay(path):
	# 設定ファイルの読み込み
	iniFile = LoadIniFile.LoadIniFileClass()
	iniFile.LoadIniFile(viz.res.getPublishedPath(path))
	
	"""
	# 画面サイズの取得＆設定
	DispSizeTemp1	= iniFile.GetItemData('PathSimulator_Setting','DispSize')
	DispSizeTemp2	= DispSizeTemp1.split(',')
	DispSize		= [float(DispSizeTemp2[0]),float(DispSizeTemp2[1])]
	
	# 画面オフセットの取得＆設定
	DispOffsetTemp1	= iniFile.GetItemData('PathSimulator_Setting','DispOffset')
	DispOffsetTemp2	= DispOffsetTemp1.split(',')
	DispOffset		= [float(DispOffsetTemp2[0]),float(DispOffsetTemp2[1])]
	
	# ウィンドウのバーの取得＆設定
	borderTemp	= iniFile.GetItemData('PathSimulator_Setting','Border')
	border = int(borderTemp)
	"""
	DispSize	= [1280,720]
	DispOffset	= [0,0]
	border		= 1
	
	#フルスクリーンの取得
	fullscreen = 0
	
	return [DispSize,DispOffset,border,fullscreen]
	
def SetDisplay(displaySettings,state):
	global preWindowSetting
	fullscreen = viz.window.getFullscreen()
	
	#レイアウター
	if state == 0:
		if fullscreen:
			if preWindowSetting[0] != [0,0]:
				displaySettings[1][0] = preWindowSetting[0]
				displaySettings[1][1] = preWindowSetting[1]
				displaySettings[1][2] = preWindowSetting[2]
		else:
			displaySettings[1][0] = viz.window.getSize()
			displaySettings[1][1] = viz.window.getPosition()
			displaySettings[1][2] = viz.window.getBorder()
		displaySettings[1][3] = viz.window.getFullscreen()
		
		viz.window.setSize(displaySettings[0][0])
		viz.window.setPosition(displaySettings[0][1])
		if displaySettings[0][2] == 0:
			viz.window.setBorder(viz.BORDER_NONE)
		else:
			viz.window.setBorder(viz.BORDER_RESIZE)
		if displaySettings[0][3] == 0:
			viz.window.setFullscreen(viz.OFF)
		else:
			viz.window.setFullscreen(viz.ON)
			
	#パスシミュレータ
	else:
		if fullscreen:
			if preWindowSetting[0] != [0,0]:
				displaySettings[0][0] = preWindowSetting[0]
				displaySettings[0][1] = preWindowSetting[1]
				displaySettings[0][2] = preWindowSetting[2]
		else:
			displaySettings[0][0] = viz.window.getSize()
			displaySettings[0][1] = viz.window.getPosition()
			displaySettings[0][2] = viz.window.getBorder()
		displaySettings[0][3] = viz.window.getFullscreen()
		
		viz.window.setSize(displaySettings[1][0])
		viz.window.setPosition(displaySettings[1][1])
		if displaySettings[1][2] == 0:
			viz.window.setBorder(viz.BORDER_NONE)
		else:
			viz.window.setBorder(viz.BORDER_RESIZE)
		if displaySettings[1][3] == 0:
			viz.window.setFullscreen(viz.OFF)
		else:
			viz.window.setFullscreen(viz.ON)
			
	preWindowSetting = [[0,0],[0.0],1,1]
	return displaySettings
	
def CheckFullScreen():
	global preWindowSetting
	
	if viz.window.getFullscreen() == False:
		preWindowSetting[0] = viz.window.getSize()
		preWindowSetting[1] = viz.window.getPosition()
		preWindowSetting[2] = viz.window.getBorder()
		