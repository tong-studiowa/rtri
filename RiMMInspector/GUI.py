﻿# coding: utf-8
#PathSimulator

import viz
import viztask

import Display2D
import DisplayText
import DisplayLabel
import DisplayTextBox

from win32api import OutputDebugString

LOADING_DIALOG=1
VISIBLE_DIALOG=2
COLLIDE_DIALOG=3
AVATARLIST_DIALOG=4

CHECK_TEXTURE_ON  = viz.res.getPublishedPath('Resource\\Interface\\CheckOn.tga')
CHECK_TEXTURE_OFF = viz.res.getPublishedPath('Resource\\Interface\\CheckOff.tga')

def CreateGUIs():
	
	guiList		= []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	#print dispSize
	size1		= [dispSize[0]*1.168, dispSize[1]*0.06] #メニューバー
	size2		= [dispSize[0]*0.40 , dispSize[1]*0.28] #読込ダイアログBG	
	size3		= [dispSize[0]*0.25 , dispSize[1]*0.38] #表示/非表示ダイアログBG
	size4		= [dispSize[0]*0.25 , dispSize[1]*0.14] #衝突判定ダイアログBG
	size5		= [dispSize[0]*0.495, dispSize[1]*0.23] #アバターダイアログBG
	size6		= [dispSize[0]*0.32 , dispSize[1]*0.14] #視点ダイアログBG
	texture1	= None
	color1		= viz.GRAY	#GUIの色
	pos1		= [dispSize[0]*0.0565, dispSize[1]*0.918] #メニューバー
	pos2		= [dispSize[0]*0.078  , dispSize[1]*0.612] #読込ダイアログBG	
	pos3		= [dispSize[0]*0.19  , dispSize[1]*0.506 ] #表示/非表示ダイアログBG
	pos4		= [dispSize[0]*0.295  , dispSize[1]*0.762 ] #衝突判定ダイアログBG
	pos5		= [dispSize[0]*0.403  , dispSize[1]*0.666 ] #アバターダイアログBG
	pos6		= [dispSize[0]*0.508  , dispSize[1]*0.762 ] #視点ダイアログBG
	alignment	= viz.ALIGN_LEFT_TOP
	order		= 5
	alpha		= 0.75
	
	guiList.append(CreateGUI(size1,texture1,color1,pos1,alignment,order,alpha,1))	#0-メニューバー
	guiList.append(CreateGUI(size2,texture1,color1,pos2,alignment,order,alpha)) 	#1-読込ダイアログBG	
	guiList.append(CreateGUI(size3,texture1,color1,pos3,alignment,order,alpha)) 	#2-表示/非表示ダイアログBG
	guiList.append(CreateGUI(size4,texture1,color1,pos4,alignment,order,alpha)) 	#3-衝突判定ダイアログBG
	guiList.append(CreateGUI(size5,texture1,color1,pos5,alignment,order,alpha)) 	#4-アバターダイアログBG
	guiList.append(CreateGUI(size6,texture1,color1,pos6,alignment,order,alpha)) 	#5-視点ダイアログBG
	
	return guiList

def CreateButtons():
	
	buttonList	= []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	size1		= [dispSize[0]*0.0375, dispSize[1]*0.05]	#正方形型
	size2		= [dispSize[0]*0.13, dispSize[1]*0.047]	#長方形型（ボタン）
	size3		= [dispSize[0]*0.07, dispSize[1]*0.04]	#長方形型（ボタン小）
	size4		= [dispSize[0]*0.045, dispSize[1]*0.06] #モード切り替えボタン
	texture1	= viz.res.getPublishedPath('Resource\\Interface\\ControlFastRewind.tga')	#巻き戻し
	texture2	= viz.res.getPublishedPath('Resource\\Interface\\ControlPlay.tga')			#再生
	texture3	= viz.res.getPublishedPath('Resource\\Interface\\ControlPose.tga')			#停止
	texture4	= viz.res.getPublishedPath('Resource\\Interface\\ControlFastForward.tga')	#終わり出し
	texture5	= None#viz.res.getPublishedPath('Resource\\Interface\\Button.tga')			#ボタン
	texture6	= viz.res.getPublishedPath('Resource\\Interface\\Mode_Layouter.jpg')		#モード切り替え
	color2		= viz.GRAY #ボタンの色（無地）
	color1		= viz.WHITE #ボタンの色（テクスチャ有）
	brank		= [dispSize[0]*0.025, dispSize[1]*0.015]
	#再生ボタン
	pos1		= [dispSize[0]*0.675			, dispSize[1]*0.9235]	#巻き戻し
	pos2		= [pos1[0]+size1[0]+brank[0]/2.0, dispSize[1]*0.9235]	#再生
	pos3		= [pos1[0]+size1[0]+brank[0]/2.0, dispSize[1]*0.9235]	#停止
	pos4		= [pos3[0]+size1[0]+brank[0]/2.0, dispSize[1]*0.9235]	#終わり出し
	#メニューボタンの表示
	pos5		= [dispSize[0]*0.0595   , dispSize[1]*0.925]	#読込
	pos6		= [pos5[0]+size2[0]*0.73   +brank[0]/2.0, dispSize[1]*0.925]	#表示
	pos7		= [pos6[0]+size2[0]*0.73   +brank[0]/2.0, dispSize[1]*0.925]	#衝突
	pos8		= [pos7[0]+size2[0]*0.73   +brank[0]/2.0, dispSize[1]*0.925]	#アバター
	pos15		= [pos8[0]+size2[0]*0.73   +brank[0]/2.0, dispSize[1]*0.925]	#視点
	#読込みダイアログ
	pos9		= [dispSize[0]*0.323	, dispSize[1]*0.658]				#参照（間取）
	pos10		= [pos9[0]			, pos9[1] +size3[1]+brank[1]]	#参照（アイテム）
	pos11		= [pos9[0]			, pos10[1]+size3[1]+brank[1]]	#参照（ログ）
	pos12		= [pos9[0]			, pos11[1]+size3[1]+brank[1]]	#更新
	#アバターダイアログ
	pos13		= [dispSize[0]*0.803	, dispSize[1]*0.868	]
	#モード切り替えボタン
	pos14		= [dispSize[0]*0.01, dispSize[1]*0.918]
	alignment	= viz.ALIGN_LEFT_TOP
	order		= 10
	
	#再生ボタン
	buttonList.append(CreateGUI(size1,texture1,color1,pos1 ,alignment,order)) #0-巻き戻し
	buttonList.append(CreateGUI(size1,texture2,color1,pos2 ,alignment,order)) #1-再生
	buttonList.append(CreateGUI(size1,texture3,color1,pos3 ,alignment,order)) #2-停止
	buttonList.append(CreateGUI(size1,texture4,color1,pos4 ,alignment,order)) #3-終わり出し
	#メニューボタンの表示
	buttonList.append(CreateGUI(size2,texture5,color2,pos5 ,alignment,order)) #4-読込
	buttonList.append(CreateGUI(size2,texture5,color2,pos6 ,alignment,order)) #5-表示
	buttonList.append(CreateGUI(size2,texture5,color2,pos7 ,alignment,order)) #6-衝突
	#読込みダイアログ
	buttonList.append(CreateGUI(size3,texture5,color2,pos9 ,alignment,order)) #7-参照（間取）
	buttonList.append(CreateGUI(size3,texture5,color2,pos10,alignment,order)) #8-参照（アイテム）
	buttonList.append(CreateGUI(size3,texture5,color2,pos11,alignment,order)) #9-参照（ログ）
	buttonList.append(CreateGUI(size3,texture5,color2,pos12,alignment,order)) #10-更新
	#アバターダイアログ
	buttonList.append(CreateGUI(size2,texture5,color2,pos8 ,alignment,order)) #11-アバターダイアログ
	buttonList.append(CreateGUI(size3,texture5,color2,pos13,alignment,order)) #12-視点の高さ
	#モード切り替えボタン
	buttonList.append(CreateGUI(size4,texture6,color2,pos14,alignment,order)) #13-モード切り替えボタン
	#視点ボタン
	buttonList.append(CreateGUI(size2,texture5,color2,pos15,alignment,order)) #14-視点設定ボタン
	
	return buttonList

def CreateCheckBoxes():
	
	checkboxList = []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	size1		 = [dispSize[0]*0.070, dispSize[1]*0.040]
	size2		 = [dispSize[0]*0.015, dispSize[1]*0.020]
	brank		 = [dispSize[0]*0.025, dispSize[1]*0.008]
	texture1	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOff.tga')
	texture2	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOn.tga')
	color1		 = viz.WHITE
	#表示/非表示	
	pos1		 = [dispSize[0]*0.215 , dispSize[1]*0.555		]
	pos2		 = [pos1[0]			 , pos1[1]+size1[1]+brank[1]]
	pos3		 = [pos1[0]			 , pos2[1]+size1[1]+brank[1]]
	pos4		 = [pos1[0]			 , pos3[1]+size1[1]+brank[1]]
	pos5		 = [pos1[0]			 , pos4[1]+size1[1]+brank[1]]
	pos6		 = [pos1[0]			 , pos5[1]+size1[1]+brank[1]]
	pos7		 = [pos1[0]			 , pos6[1]+size1[1]+brank[1]]
	#衝突判定
	pos8		 = [dispSize[0]*0.322 , pos6[1]					]
	pos9		 = [pos8[0]			 , pos7[1]					]
	alignment	 = viz.ALIGN_LEFT_TOP
	order		 = 10
	
	#表示/非表示ダイアログ
	checkboxList.append(CreateGUI(size2,texture2,color1,pos1,alignment,order)) #0-床グリッド
	checkboxList.append(CreateGUI(size2,texture2,color1,pos2,alignment,order)) #1-照明の位置
	checkboxList.append(CreateGUI(size2,texture2,color1,pos3,alignment,order)) #2-パス
	checkboxList.append(CreateGUI(size2,texture2,color1,pos4,alignment,order)) #3-機械
	checkboxList.append(CreateGUI(size2,texture2,color1,pos5,alignment,order)) #4-建物
	checkboxList.append(CreateGUI(size2,texture2,color1,pos6,alignment,order)) #3-機械
	checkboxList.append(CreateGUI(size2,texture2,color1,pos7,alignment,order)) #4-建物
	#衝突判定
	checkboxList.append(CreateGUI(size2,texture1,color1,pos8,alignment,order)) #5-
	checkboxList.append(CreateGUI(size2,texture1,color1,pos9,alignment,order)) #5-
	
	return checkboxList

#アバターのON/OFF
def CreateCheckBoxes2():
	
	checkboxList = []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	size1		 = [dispSize[0]*0.070, dispSize[1]*0.040]
	size2		 = [dispSize[0]*0.015, dispSize[1]*0.020]
	brank		 = [size2[0]+dispSize[0]*0.075, size2[1]+dispSize[1]*0.015]
	texture1	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOff.tga')
	texture2	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOn.tga')
	color1		 = viz.WHITE
	#表示/非表示	
	pos		 	 = [dispSize[0]*0.433 , dispSize[1]*0.70			]
	pos1		 = [dispSize[0]*0.18 , dispSize[1]*0.65			]
	pos2		 = [pos1[0]			 , pos1[1]+size1[1]+brank[1]]
	pos3		 = [pos1[0]			 , pos2[1]+size1[1]+brank[1]]
	pos4		 = [pos1[0]			 , pos3[1]+size1[1]+brank[1]]
	pos5		 = [pos1[0]			 , pos4[1]+size1[1]+brank[1]]
	#衝突判定
	pos6		 = [dispSize[0]*0.31 , pos4[1]					]
	pos7		 = [pos6[0]			 , pos5[1]					]
	alignment	 = viz.ALIGN_LEFT_TOP
	order		 = 10
	
	#表示/非表示ダイアログ
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*4],alignment,order))
	
	return checkboxList

#視点カメラ選択用
def CreateCheckBoxes3():
	
	checkboxList = []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	 = viz.window.getSize()
	size1		 = [dispSize[0]*0.070, dispSize[1]*0.040]
	size2		 = [dispSize[0]*0.015, dispSize[1]*0.020]
	brank		 = [size2[0]+dispSize[0]*0.075, size2[1]+dispSize[1]*0.015]
	texture1	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOff.tga')
	texture2	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOn.tga')
	color1		 = viz.WHITE
	#表示/非表示	
	pos		 	 = [dispSize[0]*0.447, dispSize[1]*0.70			]
	pos1		 = [dispSize[0]*0.18 , dispSize[1]*0.65			]
	pos2		 = [pos1[0]			 , pos1[1]+size1[1]+brank[1]]
	pos3		 = [pos1[0]			 , pos2[1]+size1[1]+brank[1]]
	pos4		 = [pos1[0]			 , pos3[1]+size1[1]+brank[1]]
	pos5		 = [pos1[0]			 , pos4[1]+size1[1]+brank[1]]
	#衝突判定
	pos6		 = [dispSize[0]*0.35 , pos4[1]					]
	pos7		 = [pos6[0]			 , pos5[1]					]
	alignment	 = viz.ALIGN_LEFT_TOP
	order		 = 10
	
	#表示/非表示ダイアログ
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*4],alignment,order))
	
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*0],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*1],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*2],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*3],alignment,order))
	checkboxList.append(CreateGUI(size2,texture2,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*4],alignment,order))
	
	return checkboxList

def CreateTexts():
	textList	= []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	size1		= [dispSize[1]*0.05, dispSize[1]*0.05]	#正方形型
	size2		= [dispSize[0]*0.13, dispSize[1]*0.047]	#長方形型（ボタン）
	size3		= [dispSize[0]*0.07, dispSize[1]*0.04]	#長方形型（ボタン小）
	fontsize1	= 25
	fontsize2	= 20
	fontsize3	= 15
	font1		= 'meiryo.ttc'
	font2		= 'meiryo.ttc'
	color1		= [1,1,1]
	color2		= [1,1,1]
	brank1		= [dispSize[0]*0.025, dispSize[1]*0.015]
	brank2		= [dispSize[0]*0.025, dispSize[1]*0.008]
	#メニューボタンの表示
	pos1		= [dispSize[0]*0.0218+size2[0]/2.0 +brank1[0]    , size2[1]/2+dispSize[1]*0.926]	#読込
	pos2		= [pos1[0]           +size2[0]*0.73     +brank1[0]/2.0, pos1[1]]					#表示
	pos3		= [pos2[0]           +size2[0]*0.73     +brank1[0]/2.0, pos1[1]]					#衝突
	pos4		= [pos3[0]           +size2[0]*0.73     +brank1[0]/2.0, pos1[1]]					#アバター
	pos25		= [pos4[0]           +size2[0]*0.73     +brank1[0]/2.0, pos1[1]]					#視点
	#読込みダイアログ
	#カテゴリ
	pos5		= [dispSize[0]*0.10	, dispSize[1]*0.67]				#間取り
	pos6		= [pos5[0]			, pos5[1] +size3[1]+brank1[1]]	#アイテム
	pos7		= [pos5[0]			, pos6[1] +size3[1]+brank1[1]]	#ログ
	#ボタン
	pos8		= [size3[0]/2+dispSize[0]*0.316	, size3[1]/2+dispSize[1]*0.66]	#参照
	pos9		= [pos8[0]						, pos8[1] +size3[1]+brank1[1]]	#参照
	pos10		= [pos8[0]						, pos9[1] +size3[1]+brank1[1]]	#参照
	pos11		= [pos8[0]						, pos10[1]+size3[1]+brank1[1]]	#更新
	#ファイルパス
	pos12		= [size3[0]/2+dispSize[0]*0.13	, size3[1]/2+dispSize[1]*0.655]	#間取り
	pos13		= [pos12[0]						, pos12[1]+size3[1]+brank1[1]]	#アイテム
	pos14		= [pos12[0]						, pos13[1]+size3[1]+brank1[1]]	#ログ
	#表示/非表示
	pos15		= [dispSize[0]*0.245	, dispSize[1]*0.555			 ]	#床グリッド
	pos16		= [pos15[0]			, pos15[1]+size3[1]+brank2[1]]	#照明の位置
	pos17		= [pos15[0]			, pos16[1]+size3[1]+brank2[1]]	#パス
	pos18		= [pos15[0]			, pos17[1]+size3[1]+brank2[1]]	#機器
	pos19		= [pos15[0]			, pos18[1]+size3[1]+brank2[1]]	#建物
	pos20		= [pos15[0]			, pos19[1]+size3[1]+brank2[1]]	#
	pos21		= [pos15[0]			, pos20[1]+size3[1]+brank2[1]]	#
	#衝突判定
	pos22		= [dispSize[0]*0.352, pos20[1]			]	#衝突判定
	pos23		= [pos22[0]			, pos21[1]			]	#一時停止
	#アバターダイアログ
	pos24		= [dispSize[0]*0.807, dispSize[1]*0.882	]	#視点の高さ
	
	#視点
	pos26		= [dispSize[0]*0.532, pos20[1]			]	#目線の高さ
	pos27		= [pos26[0]			, pos21[1]			]	#上空からの高さ
	
	alignment	= viz.ALIGN_LEFT_TOP
	alignment2	= viz.ALIGN_CENTER
	order		= 15
	
	#メニュー
	textList.append(CreateText('読込'		,fontsize1,font1,color1,pos1 ,alignment2,order)) #0 -読込
	textList.append(CreateText('表示'		,fontsize1,font1,color1,pos2 ,alignment2,order)) #1 -表示
	textList.append(CreateText('衝突'		,fontsize1,font1,color1,pos3 ,alignment2,order)) #2 -衝突
	textList.append(CreateText('アバター'		,fontsize1,font1,color1,pos4 ,alignment2,order)) #3 -アバター
	#読み込みダイアログ
	textList.append(CreateText('間取り '		,fontsize2,font1,color2,pos5 ,alignment,order)) #4 -
	textList.append(CreateText('アイテム'		,fontsize2,font1,color2,pos6 ,alignment,order)) #5 -
	textList.append(CreateText('パス  '		,fontsize2,font1,color2,pos7 ,alignment,order)) #6 -
	textList.append(CreateText('参照'		,fontsize2,font1,color2,pos8 ,alignment2,order)) #7 -
	textList.append(CreateText('参照'		,fontsize2,font1,color2,pos9 ,alignment2,order)) #8 -
	textList.append(CreateText('参照'		,fontsize2,font1,color2,pos10,alignment2,order)) #9 -
	textList.append(CreateText('更新'		,fontsize2,font1,color2,pos11,alignment2,order)) #10-
	textList.append(CreateText('No File'	,fontsize3,font1,color2,pos12,alignment,order))  #11-
	textList.append(CreateText('No File'	,fontsize3,font1,color2,pos13,alignment,order))  #12-
	textList.append(CreateText('No File'	,fontsize3,font1,color2,pos14,alignment,order))  #13-
	#チェックボックス
	textList.append(CreateText('グリッド'		,fontsize2,font1,color2,pos15,alignment,order))  #14-
	textList.append(CreateText('照明'		,fontsize2,font1,color2,pos16,alignment,order))  #15-
	textList.append(CreateText('パス'		,fontsize2,font1,color2,pos17,alignment,order))  #16-
	textList.append(CreateText('アイテム'		,fontsize2,font1,color2,pos18,alignment,order))  #17-
	textList.append(CreateText('間取り'		,fontsize2,font1,color2,pos19,alignment,order))  #18-
	textList.append(CreateText('マップ'		,fontsize2,font1,color2,pos20,alignment,order))  #19-
	textList.append(CreateText('図面'		,fontsize2,font1,color2,pos21,alignment,order))  #20-
	#衝突判定
	textList.append(CreateText('衝突判定'		,fontsize2,font1,color2,pos22,alignment,order))  #21-
	textList.append(CreateText('衝突時一時停止'	,fontsize2,font1,color2,pos23,alignment,order))  #22-
	#
	textList.append(CreateText('視点の高さ'	,fontsize3,font1,color2,pos24,alignment,order))  #23-
	textList.append(CreateText('視点'		,fontsize1,font1,color1,pos25,alignment2,order)) #24- 視点
	
	#視点ダイアログ
	textList.append(CreateText('ウォークスルー'	,fontsize2,font1,color2,pos26,alignment,order))  #25-
	textList.append(CreateText('俯瞰'		,fontsize2,font1,color2,pos27,alignment,order))  #26-
	
	return textList

def CreateTexts2():
	
	textList = []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	fontsize1	 = 15
	font1		 = 'meiryo.ttc'
	size1		 = [dispSize[0]*0.070, dispSize[1]*0.040]
	size2		 = [dispSize[0]*0.015, dispSize[1]*0.020]
	brank		 = [size2[0]+dispSize[0]*0.075, size2[1]+dispSize[1]*0.015]
	texture1	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOff.tga')
	texture2	 = viz.res.getPublishedPath('Resource\\Interface\\CheckOn.tga')
	color1		 = viz.WHITE
	#表示/非表示	
	pos			 = [dispSize[0]*0.462, dispSize[1]*0.703]
	#pos			 = [dispSize[0]*0.465, dispSize[1]*0.703]
	alignment	 = viz.ALIGN_LEFT_TOP
	alignment2	 = viz.ALIGN_LEFT_TOP
	order		 = 10
	
	#表示/非表示ダイアログ
	textList.append(CreateText('Avatar1',fontsize1,font1,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*0],alignment2,order))
	textList.append(CreateText('Avatar2',fontsize1,font1,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*1],alignment2,order))
	textList.append(CreateText('Avatar3',fontsize1,font1,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*2],alignment2,order))
	textList.append(CreateText('Avatar4',fontsize1,font1,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*3],alignment2,order))
	textList.append(CreateText('Avatar5',fontsize1,font1,color1,[pos[0]+brank[0]*0,pos[1]+brank[1]*4],alignment2,order))
	
	textList.append(CreateText('Avatar6' ,fontsize1,font1,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*0],alignment2,order))
	textList.append(CreateText('Avatar7' ,fontsize1,font1,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*1],alignment2,order))
	textList.append(CreateText('Avatar8' ,fontsize1,font1,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*2],alignment2,order))
	textList.append(CreateText('Avatar9' ,fontsize1,font1,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*3],alignment2,order))
	textList.append(CreateText('Avatar10',fontsize1,font1,color1,[pos[0]+brank[0]*1,pos[1]+brank[1]*4],alignment2,order))
	
	textList.append(CreateText('Avatar11',fontsize1,font1,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*0],alignment2,order))
	textList.append(CreateText('Avatar12',fontsize1,font1,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*1],alignment2,order))
	textList.append(CreateText('Avatar13',fontsize1,font1,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*2],alignment2,order))
	textList.append(CreateText('Avatar14',fontsize1,font1,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*3],alignment2,order))
	textList.append(CreateText('Avatar15',fontsize1,font1,color1,[pos[0]+brank[0]*2,pos[1]+brank[1]*4],alignment2,order))
	
	textList.append(CreateText('Avatar16',fontsize1,font1,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*0],alignment2,order))
	textList.append(CreateText('Avatar17',fontsize1,font1,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*1],alignment2,order))
	textList.append(CreateText('Avatar18',fontsize1,font1,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*2],alignment2,order))
	textList.append(CreateText('Avatar19',fontsize1,font1,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*3],alignment2,order))
	textList.append(CreateText('Avatar20',fontsize1,font1,color1,[pos[0]+brank[0]*3,pos[1]+brank[1]*4],alignment2,order))
	
	
	return textList

def CreateTextBoxes():
	textBoxList	= []
	#dispSize	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
	dispSize	= viz.window.getSize()
	size1		= [dispSize[0]*0.0004, dispSize[1]*0.001]	#長方形型（ボタン）
	fontsize1	= 25
	color1		= [1,1,1]
	color2		= [1,1,1]
	brank1		= [dispSize[0]*0.025, dispSize[1]*0.048]
	pos1		= [dispSize[0]*0.685, size1[1]/2+dispSize[1]*0.805]		#ウォークスルー
	pos2		= [pos1[0]          , pos1[1]+size1[1]+brank1[1]]		#俯瞰
	
	alignment	= viz.ALIGN_LEFT_TOP
	alignment2	= viz.ALIGN_CENTER
	order		= 15
	
	#視点
	textBoxList.append(CreateTextBox(size1,fontsize1,color1,pos1,alignment2,order)) #0 -ウォークスルー
	textBoxList.append(CreateTextBox(size1,fontsize1,color1,pos2,alignment2,order)) #1 -俯瞰
	
	return textBoxList

def CreateText(text,size,font,color,pos,alignment=viz.ALIGN_LEFT_TOP,order=1,alpha=1):
	guiText = DisplayText.DisplayText()
	guiText.SetAlignment(alignment)
	guiText.SetFont(font)
	guiText.SetText(text)
	guiText.SetSize(size)
	guiText.SetPosition(pos[0],pos[1])
	guiText.SetFontColor(color)
	guiText.SetRenderOrder(order)
	return guiText
def CreateLabel(text,size,font,color,pos,alignment=viz.ALIGN_LEFT_TOP,order=1,alpha=1):
	guiText = DisplayLabel.DisplayLabel()
	guiText.SetAlignment(alignment)
	guiText.SetFont(font)
	guiText.SetText(text)
	guiText.SetSize(size)
	guiText.SetPosion(pos[0],pos[1])
	guiText.SetFontColor(color)
	guiText.SetRenderOrder(order)
	return guiText
def CreateGUI(size,texture,color,pos,alignment=viz.ALIGN_LEFT_TOP,order=0,alpha=1,keepSizeX=0):
	guiObj = Display2D.Display2D()
	guiObj.SetAlignment(alignment)
	guiObj.SetPosition(pos[0],pos[1])
	if texture!=None:
		guiObj.SetTexture(texture)
	if color!=None:
		guiObj.SetColor(color)
	guiObj.SetkeepSize(keepSizeX)
	guiObj.SetSize(size[0],size[1])
	guiObj.SetRenderOrder(order)
	guiObj.SetAlpha(alpha)
	return guiObj
def CreateTextBox(size,fontSize,color,pos,alignment=viz.ALIGN_LEFT_TOP,order=1,alpha=1):
	guiTextBox = DisplayTextBox.DisplayTextBox()
	guiTextBox.SetAlignment(alignment)
	guiTextBox.SetSize(size[0],size[1])
	guiTextBox.SetFontSize(fontSize)
	guiTextBox.SetPosition(pos[0],pos[1])
	guiTextBox.SetFontColor(color)
	guiTextBox.SetRenderOrder(order)
	return guiTextBox

def HideGUIList(list):
	for data in list:
		if data:
			data.SetVisible(False)
def ShowGUIList(list):
	for data in list:
		if data:
			data.SetVisible(True)

def SetCheckBoxTexture(target,check):
	global CHECK_TEXTURE_ON
	global CHECK_TEXTURE_OFF
	if check:
		target.SetTexture(CHECK_TEXTURE_ON)
	else:
		target.SetTexture(CHECK_TEXTURE_OFF)

def Debug(message):
	OutputDebugString('[STUDIOWA][PATH-SIM][GUI]'+str(message))
	