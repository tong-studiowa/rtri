﻿import sys
import time
import viz
import viztask
from ctypes import *
import vizinput
gAppID=3
gOption=1
gRevision=16070100
gLisenceVersion=1
from win32api import OutputDebugString

#gSecurity = cdll.LoadLibrary("SecurityKey.dll")	#32bit
#gSecurity = cdll.LoadLibrary("SecurityKeyx64.dll")	#64bit
gSecurity = cdll.LoadLibrary("SecurityKey_STS0852-C0430120160701-00_x64.dll")	#64bit

gTextSecurity=cdll.LoadLibrary("rimmsyslibx64.dll")	#64bit
stopVizardFlag=False

def SecurityCheck():
	viztask.schedule( _SecurityCheck )
	
def _SecurityCheck():
	global stopVizardFlag
	while True:
		if stopVizardFlag:
			viz.quit()
			return
		yield viztask.waitFrame(1)

def SecurityCheckFunction():
	global gSecurity
	global gTextSecurity
	global stopVizardFlag
	
	mSecurityCount=0
	#ドングルチェック
	OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]SecurityCheckFunction 001")
	while True:
		data=1
		if(0==mSecurityCount%int(6)):			#ここでチェック頻度を設定
			data=gSecurity.SecurityCheckDLL()	#ドングルが刺さっているかどうかチェック
			if 0==data:
				break
#				stopVizardFlag=True
		mSecurityCount+=1
		#print 'mSecurityCount',mSecurityCount
		
		if time == None:
			break
		time.sleep(0.1)
	#ドングルチェックに失敗したとき
	missCount=0
	OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]SecurityCheckFunction dongleerro 001")
	while(True):
		if None==gTextSecurity:
			stopVizardFlag=True
			OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]text error 001")
			stopVizardFlag=True
			return
		errottype=	CheckTextLicense()
#		if 0!=errottype:
		if False==errottype:
			missCount+=1;
			if 5<missCount:#ミスを5回まで許す
				#認証
				if False==RegistUserLicence():
					OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]text error 002 type "+str(errottype))
					#認証に失敗したら止める
					stopVizardFlag=True
				else:
					OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]SecurityCheckFunction text success 001")

				return
		else:
			OutputDebugString("[StudioWa][INFO][ContentsPlayerLicense]SecurityCheckFunction text success 002")
			#テキストの認識ができたら終了
			return 
#			missCount=0
		time.sleep(0.5)
def CheckTextLicense():
	global gTextSecurity
	global gAppID
	global gOption
	global gRevision
	global gLisenceVersion
	licerr = gTextSecurity.CheckUserLicence(gAppID,gRevision,gOption,gLisenceVersion)
	if licerr == 0:#チェック成功
		return True
	else:
		return False
def RegistUserLicence():
	global gTextSecurity
	licerr = gTextSecurity.RegistUserLicence(viz.window.getHandle(),gAppID,gRevision,gOption,gLisenceVersion)
	if licerr == 0:#チェック成功
		return True
	else:
		return False
from threading import Thread

SecurityCheckThread=Thread(target=SecurityCheckFunction)
SecurityCheckThread.daemon = True
SecurityCheckThread.start()
