﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Object2d

import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテムファイル制御のクラス定義
class SetItemButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemButton] : Init SetItemButton')
		
		self._InputClass = None
		self._ItemControlClass = None
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		
		self._IconCount = 2
		
		self._Frame = None
		self._TextList = []
		self._IconList = []
		
		self._Filter = [('INI Files','*.ini')]
		self._Directory = self.GetRootPath() + 'Settings\\Item\\.'
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [409,windowSize[1]-107]
		self._FrameScale = [210,85]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#テキスト追加
		self._TextPositionX = 409
		self._TextPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._TextScale = [18.6,18.6]
		self._TextMessage = ["読み込み","保存"]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			message = self._TextMessage[x]
			
			text = self.CreateText(message,position,self._TextScale)
			self._TextList.append(text)
			
		#アイコン追加
		self._IconPositionX = 409
		self._IconPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._IconScale = [200,36]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._IconPositionX
			position[1] = self._IconPositionY[x]
			
			icon = self.CreateIcon(position,self._IconScale)
			self._IconList.append(icon)
			
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemButton] : Start SetItemButton Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				num = None
				for x in range(self._IconCount):
					if self._IconList[x].getVisible():
						if self.CheckRollOver(mousePosition,[self._IconPositionX,self._IconPositionY[x]],self._IconScale):
							num = x
							
				self._OverIconState = num
				
				if num != None:
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.LoadItem()		#アイテムを読み込む
						elif num == 1:
							self.SaveItem()		#アイテムを保存する
						elif num == 2:
							self.DeleteItem()	#アイテムを削除する
						
			#ロールオーバー
			self.Rollover(self._OverIconState)
				
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		
		for text in self._TextList:
			text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#アイテムを読み込む
	def LoadItem(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemButton] : LoadItem , directory='+self._Directory)
		filePath = vizinput.fileOpen(filter=self._Filter,file=self._Directory)
		
		if filePath != "":
			self._ItemControlClass.Load(filePath)
		
		self.SetVisible(False)
		
	#アイテムを保存
	def SaveItem(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemButton] : SaveItem , directory='+self._Directory)
		filePath = vizinput.fileSave(file=self._Directory)
		
		if filePath != "":
			if filePath.endswith('.ini') == False:
				filePath = filePath + '.ini'

			self._ItemControlClass.Save(filePath)
		
		self.SetVisible(False)
		
	#アイテムを削除
	def DeleteItem(self):
		self._ItemControlClass.DeleteAll()
		
		self.SetVisible(False)
	
	#ロールオーバー処理
	def Rollover(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
			
	#ルートフォルダの取得
	def GetRootPath(self):
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False:
			exeDir = 'C:\\RiMMroot\\RiMMInspector\\'
			if os.path.exists(exeDir) == False:
				exeDir = 'E:\\Git\\rtri\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
			
		return rootDir
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [409,windowSize[1]-107]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TextPositionY = [windowSize[1]-127, windowSize[1]-87]
		self._IconPositionY = [windowSize[1]-127, windowSize[1]-87]
		
		for x in range(self._IconCount):
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			self.SetIconPosition(self._TextList[x],position,self._TextScale)
			self.SetIconPosition(self._IconList[x],position,self._IconScale)
			