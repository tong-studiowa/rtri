﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#状態の確認用クラス定義
class CheckState(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CheckState] : Init CheckState')
		
		self._StencilClass = None
		self._FunctionButtonAreaClass = None
		self._ChangeViewClass = None
		self._BackgroundClass = None
		self._SetDirectionClass = None
		self._AddLogClass = None
		self._AddPathClass = None
		self._PlayPathClass = None
		self._ShowDrawingClass = None
		self._ShowPathClass = None
		self._SetDrawingButtonClass = None
		self._SetFileterClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._ChangeModeClass = None
			
		self._OverState = False
		self._ModeState = 0
		
	#使用するクラスの追加
	def AddClass(self,stencilClass,functionButtonAreaClass,changeViewClass,backgroundClass
				,setDirectionClass,addLogClass,addPathClass,playPathClass,showDrawingClass
				,showPathClass,setDrawingButtonClass,setFileterClass,undoClass,redoClass
				,changeModeClass):
		self._StencilClass = stencilClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._ChangeViewClass = changeViewClass
		self._BackgroundClass = backgroundClass
		self._SetDirectionClass = setDirectionClass
		self._AddLogClass = addLogClass
		self._AddPathClass = addPathClass
		self._PlayPathClass = playPathClass
		self._ShowDrawingClass = showDrawingClass
		self._ShowPathClass = showPathClass
		self._SetDrawingButtonClass = setDrawingButtonClass
		self._SetFileterClass = setFileterClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._ChangeModeClass = changeModeClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString("[STUDIOWA][INFO][RiMMInspector][CheckState] : Start CheckState Update")
		
		#インターフェイス上にマウスがあるか確認
		self._OverState = False
		self._ModeState = 0
		if self._StencilClass.GetOverState():
			self._OverState = True
		elif self._FunctionButtonAreaClass.GetOverState():
			self._OverState = True
		elif self._ChangeViewClass.GetOverState():
			self._OverState = True
		elif self._BackgroundClass.GetOverState():
			self._OverState = True
		elif self._SetDirectionClass.GetOverState() != -1:
			self._OverState = True
		elif self._AddLogClass.GetOverState():
			self._OverState = True
		elif self._AddPathClass.GetOverState():
			self._OverState = True
		elif self._PlayPathClass.GetOverState():
			self._OverState = True
		elif self._ShowDrawingClass.GetOverState():
			self._OverState = True
		elif self._ShowPathClass.GetOverState():
			self._OverState = True
		elif self._SetDrawingButtonClass.GetOverState():
			self._OverState = True
		elif self._SetFileterClass.GetOverState():
			self._OverState = True
		elif self._UndoClass.GetOverState():
			self._OverState = True
		elif self._RedoClass.GetOverState():
			self._OverState = True
		elif self._ChangeModeClass.GetOverState():
			self._OverState = True
		
		#モードを確認
		self._ModeState = 0
		if self._AddLogClass.GetAddState():
			self._ModeState = 1
		elif self._AddPathClass.GetAddState():
			self._ModeState = 2
		elif self._PlayPathClass.GetPlayState():
			self._ModeState = 3
		elif self._SetDrawingButtonClass.GetSettingState():
			self._ModeState = 4
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
		
	#モードの取得（0:通常、1:ログ追加中、2:パス追加中、3:パス再生中）
	def GetModeState(self):
		state = self._ModeState
		return state
		
	#重なり状態とモードの取得
	def GetOverAndModeState(self):
		state = False
		if self._OverState or self._ModeState != 0:
			state = True
			
		return state
		