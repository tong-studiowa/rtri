﻿# coding: utf-8

import viz

import Object3d
import LoadIniFile

from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス制御のクラス定義
class Path(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,modelControlClass,isItem=False):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Path] : Init Path')
		
		self._ModelControlClass = modelControlClass
		self._IsItem = isItem
		
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionDefault = [0.0,0.0,0.0]
		
		self._Id = None
		
		self.SetModel()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Start Item Update')
		pass
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
		
	#モデル設定
	def SetModel(self):
		filePath = viz.res.getPublishedPath('Resource\\Interface\\Path.osgb')
		
		model = self._ModelControlClass.Add(filePath)	#モデルを追加
		
		self._Model = model
		
		self._Model.disable(viz.DEPTH_WRITE)
		#self._Model.alpha(0.9)
		self._Model.drawOrder(-5)
		
		color = viz.BLUE
		if self._IsItem:
			#color = viz.GREEN
			color = viz.BLUE
		
		self._Model.color(color)
		self._Model.emissive(color)
		self._Model.ambient(color)
		self._Model.specular(color)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		
		self.SetPickable(False)
		
		self.SetPosition(self._PositionDefault)
		
	#位置設定
	def SetPosition(self,iPosition):
		if iPosition != None:
			position = [0,0,0]
			position[0] = iPosition[0]
			position[1] = iPosition[1]
			position[2] = iPosition[2]
			self._Position = position
			
			position2 = [0,0,0]
			position2[0] = iPosition[0]
			position2[1] = 0.01
			position2[2] = iPosition[2]
			self._Model.setPosition(position2)
			
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
		
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.specular(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.specular(self._Specular)
			
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
			
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#ID設定
	def SetId(self,id):
		self._Id = id
		
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#表示状態設定
	def SetVisible(self,state):
		if state == True:
			self._Model.visible(viz.ON)
		else:
			self._Model.visible(viz.OFF)
			