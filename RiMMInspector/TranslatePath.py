﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス移動制御のクラス定義
class TranslatePath(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslatePath] : Init TranslatePath')
		
		self._InputClass = None
		self._PathControlClass = None
		self._SelectPathClass = None
		self._ManipulatorClass = None
		self._CharacterControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._ItemControlClass = None

		self._SelectedCharacter = None
		self._SelectedPath = None
		self._isItem = False
		
		self._ManipulatorState = 0
		
		self._TranslateState = False
		self._PreTranslateState = False
		
		self._TranslateSnap = translateSnap
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,pathControlClass,selectPathClass,manipulatorClass,characterControlClass
				,undoClass,redoClass,itemControlClass):
		self._InputClass = inputClass
		self._PathControlClass = pathControlClass
		self._SelectPathClass = selectPathClass
		self._ManipulatorClass = manipulatorClass
		self._CharacterControlClass = characterControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._ItemControlClass = itemControlClass

		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslatePath] : Start TranslatePath Update')
		
		self._TranslateState = False
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState != 0 and manipulatorState != 2 :
			self._SelectedCharacter,self._SelectedPath,self._isItem = self._SelectPathClass.GetSelectedModel()
			if self._SelectedCharacter != None and self._SelectedPath != None:
				self._TranslateState = True
				self.Translate(manipulatorState)
				
		if self._TranslateState == False and self._PreTranslateState == True:
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
		self._PreTranslateState = self._TranslateState
		
	#移動
	def Translate(self,manipulatorState):
		characterNum,pathNum,isItem = self._SelectPathClass.GetSelectedModel()
		pathPosition = self._PathControlClass.GetPosition(characterNum,pathNum,isItem)
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = pathPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = pathPosition[1]
			position[2] = pathPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = pathPosition[0]
			position[1] = pickingPosition[1]
			position[2] = pathPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = pathPosition[0]
			position[1] = pathPosition[1]
			position[2] = pickingPosition[2]
		
		#アイテムを移動
		self._PathControlClass.SetPosition(characterNum,pathNum,position,isItem)
		
		if self._TranslateState == True and self._PreTranslateState == False:
			self._UndoData = []
			self._UndoData.append(32)
			self._UndoData.append([characterNum,pathNum,pathPosition,isItem])
			
		if pathNum == 0:
			if isItem:
				prePos = self._ItemControlClass.GetPosition(characterNum)
				newPos = [0,0,0]
				newPos[0] = position[0]
				newPos[1] = prePos[1]
				newPos[2] = position[2]
				
				self._ItemControlClass.SetPosition(characterNum,newPos)
			else:
				prePos = self._CharacterControlClass.GetPosition(characterNum)
				newPos = [0,0,0]
				newPos[0] = position[0]
				newPos[1] = prePos[1]
				newPos[2] = position[2]
				
				self._CharacterControlClass.SetPosition(characterNum,newPos)
				
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)
		
		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
