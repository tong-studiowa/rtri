﻿# FCameraClass
# Develop       : Python v2.7.2 / Vizard4.05.0095
# LastUpdate    : 2012/11/22
# Programer     : nishida

import viz
import F3DObjectClass

class FCameraClass(F3DObjectClass.F3DObjectClass):

# member
	mFieldOfView       = None
	mPositionOffset    = None
	mQuaternionOffset  = None
	mCollision         = None

# method
	# called method at first
	def __init__(self):
		F3DObjectClass.F3DObjectClass.__init__(self)

	# called method at delete
	def __del__(self):
		pass

	# set field of view
	def SetFieldOfView(self,fieldOfView):
		self.mModel.fov(fieldOfView)
		self.mFieldOfView = fieldOfView

	# return field of view
	def GetFieldOfView(self):
		return self.mFieldOfView

	# set position offset
	def SetPositionOffset(self,positionOffset):				#temp(未完)
		self.mPositionOffset = positionOffset

	# return position offset
	def GetPositionOffset(self):
		return self.mPositionOffset

	# set quaternion offset
	def SetQuaternionOffset(self,quaternionOffset):			#temp(未完)
		self.mQuaternionOffset = quaternionOffset

	# return quaternion offset
	def SetQuaternionOffset(self):
		return self.mQuaternionOffset

	# set collision
	def SetCollision(self,collision):						#temp(未完)
		self.mCollision = collision

	# return collision
	def GetCollision(self):
		return self.mCollision
