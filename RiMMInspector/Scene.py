﻿# coding: utf-8

import viz
import vizinput

import Interface
import Input
import Background
import View2d
import View3d
import ViewVR
import ChangeView
import Stencil
import ItemControl
import FunctionButtonArea
import RoomControl
import SelectWall
import Manipulator
import TranslateWall
import SetWallHeightButton
import SetFloorTextureButton
import SetWallTextureButton
import SetCeilingTextureButton
import SetWindowHeightButton
import EnvironmentControl
import SetItemButton
import SetRoomButton
import SetEnvironmentButton
import SetItemSizeButton
import SetItemTextureButton
import SetDirection
import OffsetItem
import StartPosition
import SelectStartPosition
import TranslateStartPosition
import RotateStartPosition
import LogControl
import SelectLog
import AddLog
import TranslateLog
import RotateLog
import SetLogButton
import Drawing
import SetDrawingButton
import CharacterControl
import SetPathButton
import AddPath
import PathControl
import SelectPath
import TranslatePath
import PlayPath
import CheckState
import ShowDrawing
import ShowPath
import SelectModel
import SetDrawingScaleButton
import SetFilter
import ModelControl
import ScaleItem
import Undo
import Redo
import TranslateModel
import RotateModel
import ChangeMode
import RoomData
import ItemData
import PathData
import LogVR
import SetLightButton
from win32api import OutputDebugString

DebugMode = False

# - - - - - - - - - - - - - - - - - - - - - - - - -
#状態管理シーンのクラス定義
class Scene(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Scene] : Init Scene')
		
		self._WindowSize = viz.window.getSize()
		
		self._InputClass = Input.Input()
		self._BackgroundClass = Background.Background()
		self._View2dClass = View2d.View2d()
		self._View3dClass = View3d.View3d()

		try:
			self.__mViewVR = ViewVR.ViewVR()
		except:
			print "Can't find VR!"
			self.__mViewVR = None
		
		self._ChangeViewClass = ChangeView.ChangeView()
		self._StencilClass = Stencil.Stencil()
		self._ItemControlClass = ItemControl.ItemControl()
		self._FunctionButtonAreaClass = FunctionButtonArea.FunctionButtonArea()
		self._RoomControlClass = RoomControl.RoomControl()
		self._SelectWallClass = SelectWall.SelectWall()
		self._ManipulatorClass = Manipulator.Manipulator()
		self._TranslateWallClass = TranslateWall.TranslateWall()
		self._SetWallHeightButtonClass = SetWallHeightButton.SetWallHeightButton()
		self._SetFloorTextureButtonClass = SetFloorTextureButton.SetFloorTextureButton()
		self._SetWallTextureButtonClass = SetWallTextureButton.SetWallTextureButton()
		self._SetCeilingTextureButtonClass = SetCeilingTextureButton.SetCeilingTextureButton()
		self._SetWindowHeightButtonClass = SetWindowHeightButton.SetWindowHeightButton()
		self._EnvironmentControlClass = EnvironmentControl.EnvironmentControl()
		self._SetItemButtonClass = SetItemButton.SetItemButton()
		self._SetRoomButtonClass = SetRoomButton.SetRoomButton()
		self._SetEnvironmentButtonClass = SetEnvironmentButton.SetEnvironmentButton()
		self._SetItemSizeButtonClass = SetItemSizeButton.SetItemSizeButton()
		self._SetItemTextureButtonClass = SetItemTextureButton.SetItemTextureButton()
		self._SetDirectionClass = SetDirection.SetDirection()
		self._OffsetItemClass = OffsetItem.OffsetItem()
		self._StartPositionClass = StartPosition.StartPosition()
		self._SelectStartPositionClass = SelectStartPosition.SelectStartPosition()
		self._TranslateStartPositionClass = TranslateStartPosition.TranslateStartPosition()
		self._RotateStartPositionClass = RotateStartPosition.RotateStartPosition()
		self._LogControlClass = LogControl.LogControl()
		self._SelectLogClass = SelectLog.SelectLog()
		self._AddLogClass = AddLog.AddLog()
		self._TranslateLogClass = TranslateLog.TranslateLog()
		self._RotateLogClass = RotateLog.RotateLog()
		self._SetLogButtonClass = SetLogButton.SetLogButton()
		self._DrawingClass = Drawing.Drawing()
		self._SetDrawingButtonClass = SetDrawingButton.SetDrawingButton()
		self._CharacterControlClass = CharacterControl.CharacterControl()
		self._SetPathButtonClass = SetPathButton.SetPathButton()
		self._AddPathClass = AddPath.AddPath()
		self._PathControlClass = PathControl.PathControl()
		self._SelectPathClass = SelectPath.SelectPath()
		self._TranslatePathClass = TranslatePath.TranslatePath()
		self._PlayPathClass = PlayPath.PlayPath()
		self._CheckStateClass = CheckState.CheckState()
		self._ShowDrawingClass = ShowDrawing.ShowDrawing()
		self._ShowPathClass = ShowPath.ShowPath()
		self._SelectModelClass = SelectModel.SelectModel()
		self._SetDrawingScaleButtonClass = SetDrawingScaleButton.SetDrawingScaleButton()
		self._SetFileterClass = SetFilter.SetFilter()
		self._ModelControlClass = ModelControl.ModelControl()
		self._ScaleItemClass = ScaleItem.ScaleItem()
		self._UndoClass = Undo.Undo()
		self._RedoClass = Redo.Redo()
		self._TranslateModelClass = TranslateModel.TranslateModel()
		self._RotateModelClass = RotateModel.RotateModel()
		self._ChangeModeClass = ChangeMode.ChangeMode(self.ChangeMode)
		self._SetLightButtonClass = SetLightButton.SetLightButton()
		
		self._RoomDataClass = RoomData.RoomData(self._RoomControlClass)
		self._ItemDataClass = ItemData.ItemData(self._ItemControlClass)
		self._PathDataClass = PathData.PathData(self._CharacterControlClass)
		
		self._BackgroundClass.AddClass(self._InputClass,self._StencilClass,self._ManipulatorClass)
		self._View2dClass.AddClass(self._InputClass,self._SelectModelClass,self._StencilClass,self._TranslateModelClass)
		self._View3dClass.AddClass(self._InputClass)
		self._ChangeViewClass.AddClass(self._InputClass,self._View2dClass,self._View3dClass,self._StencilClass,self._RoomControlClass
									  ,self._ManipulatorClass,self._FunctionButtonAreaClass,self._SelectWallClass,self._SetDirectionClass
									  ,self._AddLogClass,self._ItemControlClass,self._AddPathClass,self._BackgroundClass,self._ShowDrawingClass
									  ,self._ShowPathClass,self._PlayPathClass,self._SelectModelClass,self._SetFileterClass,self._TranslateModelClass
									  ,self._TranslateWallClass,self._UndoClass,self._RedoClass)
		self._StencilClass.AddClass(self._InputClass,self._ItemControlClass,self._RoomControlClass,self._SelectWallClass,self._CharacterControlClass
								   ,self._SelectModelClass,self._ModelControlClass,self._UndoClass,self._RedoClass)
		self._ItemControlClass.AddClass(self._SelectModelClass,self._ModelControlClass,self._UndoClass,self._RedoClass,self._PathControlClass)
		self._FunctionButtonAreaClass.AddClass(self._InputClass,self._ItemControlClass,self._SelectModelClass,self._RoomControlClass,self._SelectWallClass
											  ,self._SetWallHeightButtonClass,self._SetFloorTextureButtonClass
											  ,self._SetWallTextureButtonClass,self._SetCeilingTextureButtonClass,self._SetWindowHeightButtonClass
											  ,self._SetItemButtonClass,self._SetRoomButtonClass,self._SetEnvironmentButtonClass,self._SetItemSizeButtonClass
											  ,self._SetItemTextureButtonClass,self._StartPositionClass,self._EnvironmentControlClass,self._LogControlClass
											  ,self._SelectLogClass,self._SetLogButtonClass,self._DrawingClass,self._SetDrawingButtonClass,self._CharacterControlClass
											  ,self._SetPathButtonClass,self._PathControlClass,self._SelectPathClass,self._AddPathClass,self._PlayPathClass
											  ,self._SetDrawingScaleButtonClass,self._StencilClass,self._ManipulatorClass,self._TranslateModelClass
											  ,self._ChangeViewClass,self._UndoClass,self._RedoClass,self._RotateModelClass,self._ScaleItemClass,self._SetLightButtonClass)
		self._RoomControlClass.AddClass(self._InputClass,self._ChangeViewClass,self._StartPositionClass,self._DrawingClass,self._ModelControlClass
									   ,self._UndoClass,self._RedoClass, self.__mViewVR)
		self._SelectWallClass.AddClass(self._InputClass,self._RoomControlClass,self._StencilClass,self._ManipulatorClass,self._SelectModelClass,self._CheckStateClass)
		self._ManipulatorClass.AddClass(self._InputClass,self._FunctionButtonAreaClass,self._View2dClass,self._BackgroundClass,self._StencilClass,self._CheckStateClass)
		self._TranslateWallClass.AddClass(self._InputClass,self._RoomControlClass,self._SelectWallClass,self._ManipulatorClass,self._UndoClass,self._RedoClass)
		self._SetWallHeightButtonClass.AddClass(self._InputClass,self._RoomControlClass,self._UndoClass,self._RedoClass)
		self._SetFloorTextureButtonClass.AddClass(self._InputClass,self._RoomControlClass,self._UndoClass,self._RedoClass)
		self._SetWallTextureButtonClass.AddClass(self._InputClass,self._RoomControlClass,self._UndoClass,self._RedoClass)
		self._SetCeilingTextureButtonClass.AddClass(self._InputClass,self._RoomControlClass,self._UndoClass,self._RedoClass)
		self._SetWindowHeightButtonClass.AddClass(self._InputClass,self._RoomControlClass,self._SelectModelClass,self._UndoClass,self._RedoClass)
		self._SetItemButtonClass.AddClass(self._InputClass,self._ItemControlClass)
		self._SetRoomButtonClass.AddClass(self._InputClass,self._RoomControlClass)
		self._SetEnvironmentButtonClass.AddClass(self._InputClass,self._EnvironmentControlClass,self._BackgroundClass)
		self._SetItemSizeButtonClass.AddClass(self._InputClass,self._ItemControlClass,self._UndoClass,self._RedoClass)
		self._SetItemTextureButtonClass.AddClass(self._InputClass,self._ItemControlClass,self._UndoClass,self._RedoClass)
		self._SetDirectionClass.AddClass(self._InputClass,self._StencilClass,self._AddLogClass)
		self._OffsetItemClass.AddClass(self._InputClass,self._ItemControlClass,self._SelectModelClass)
		self._SelectStartPositionClass.AddClass(self._InputClass,self._StartPositionClass,self._StencilClass,self._ManipulatorClass,self._SelectWallClass
											   ,self._SelectModelClass,self._CheckStateClass)
		self._TranslateStartPositionClass.AddClass(self._InputClass,self._StartPositionClass,self._SelectStartPositionClass,self._ManipulatorClass)
		self._RotateStartPositionClass.AddClass(self._InputClass,self._StartPositionClass,self._SelectStartPositionClass,self._ManipulatorClass)
		self._LogControlClass.AddClass(self._SelectLogClass,self._ItemControlClass,self._RoomControlClass,self._UndoClass)
		self._SelectLogClass.AddClass(self._InputClass,self._LogControlClass,self._StencilClass,self._ManipulatorClass,self._SelectWallClass
									 ,self._SelectModelClass,self._SelectStartPositionClass,self._CheckStateClass)
		self._AddLogClass.AddClass(self._InputClass,self._LogControlClass,self._StencilClass,self._FunctionButtonAreaClass,self._ChangeViewClass,self._BackgroundClass
								  ,self._SetDirectionClass)
		self._TranslateLogClass.AddClass(self._InputClass,self._LogControlClass,self._SelectLogClass,self._ManipulatorClass)
		self._RotateLogClass.AddClass(self._InputClass,self._LogControlClass,self._SelectLogClass,self._ManipulatorClass)
		self._SetLogButtonClass.AddClass(self._InputClass,self._LogControlClass,self._RoomControlClass,self._ItemControlClass)
		self._SetDrawingButtonClass.AddClass(self._InputClass,self._DrawingClass,self._SetDrawingScaleButtonClass)
		self._CharacterControlClass.AddClass(self._SelectModelClass,self._PathControlClass,self._ModelControlClass,self._UndoClass,self._RedoClass)
		self._SetPathButtonClass.AddClass(self._InputClass,self._CharacterControlClass,self._PathControlClass)
		self._AddPathClass.AddClass(self._InputClass,self._CharacterControlClass,self._SelectModelClass,self._PathControlClass,self._CheckStateClass
								   ,self._StencilClass,self._ManipulatorClass,self._FunctionButtonAreaClass,self._UndoClass,self._RedoClass,self._ItemControlClass)
		self._PathControlClass.AddClass(self._SelectPathClass,self._CharacterControlClass,self._ModelControlClass,self._ItemControlClass)
		self._SelectPathClass.AddClass(self._InputClass,self._PathControlClass,self._StencilClass,self._ManipulatorClass,self._SelectWallClass
									  ,self._SelectModelClass,self._SelectStartPositionClass,self._SelectLogClass,self._CheckStateClass)
		self._TranslatePathClass.AddClass(self._InputClass,self._PathControlClass,self._SelectPathClass,self._ManipulatorClass,self._CharacterControlClass
										 ,self._UndoClass,self._RedoClass,self._ItemControlClass)
		self._PlayPathClass.AddClass(self._InputClass,self._CharacterControlClass,self._SelectModelClass,self._PathControlClass,self._CheckStateClass
									,self._StencilClass,self._ManipulatorClass,self._AddPathClass,self._FunctionButtonAreaClass,self._ItemControlClass)
		self._CheckStateClass.AddClass(self._StencilClass,self._FunctionButtonAreaClass,self._ChangeViewClass,self._BackgroundClass
									  ,self._SetDirectionClass,self._AddLogClass,self._AddPathClass,self._PlayPathClass,self._ShowDrawingClass
									  ,self._ShowPathClass,self._SetDrawingScaleButtonClass,self._SetFileterClass,self._UndoClass,self._RedoClass
									  ,self._ChangeModeClass)
		self._ShowDrawingClass.AddClass(self._InputClass,self._DrawingClass,self._StencilClass,self._ManipulatorClass)
		self._ShowPathClass.AddClass(self._InputClass,self._PathControlClass,self._StencilClass,self._ManipulatorClass)
		self._SelectModelClass.AddClass(self._InputClass,self._ItemControlClass,self._StencilClass,self._ManipulatorClass,self._CheckStateClass,self._RoomControlClass
									   ,self._CharacterControlClass,self._StartPositionClass,self._SetFileterClass,self._BackgroundClass,self._PathControlClass
									   ,self._SelectLogClass,self._SelectPathClass,self._SelectStartPositionClass,self._SelectWallClass
									   ,self._ScaleItemClass)
		self._SetDrawingScaleButtonClass.AddClass(self._InputClass,self._DrawingClass,self._CheckStateClass,self._StencilClass)
		self._SetFileterClass.AddClass(self._InputClass,self._StencilClass,self._ManipulatorClass)
		self._ScaleItemClass.AddClass(self._InputClass,self._ItemControlClass,self._SelectModelClass,self._ManipulatorClass,self._CheckStateClass
									 ,self._StencilClass,self._SetItemSizeButtonClass,self._UndoClass,self._RedoClass)
		self._UndoClass.AddClass(self._InputClass,self._LogControlClass,self._StencilClass,self._FunctionButtonAreaClass,self._ChangeViewClass,self._BackgroundClass
								,self._SetDirectionClass,self._ItemControlClass,self._RoomControlClass,self._SelectModelClass,self._SelectWallClass
								,self._SelectLogClass,self._SelectStartPositionClass,self._RedoClass
								,self._CharacterControlClass,self._PathControlClass,self._SelectPathClass)
		self._RedoClass.AddClass(self._InputClass,self._LogControlClass,self._StencilClass,self._FunctionButtonAreaClass,self._ChangeViewClass,self._BackgroundClass
								,self._SetDirectionClass,self._ItemControlClass,self._RoomControlClass,self._SelectModelClass,self._SelectWallClass
								,self._SelectLogClass,self._SelectStartPositionClass,self._UndoClass
								,self._CharacterControlClass,self._PathControlClass,self._SelectPathClass)
		self._TranslateModelClass.AddClass(self._InputClass,self._ItemControlClass,self._RoomControlClass,self._CharacterControlClass,self._SelectModelClass
										  ,self._ManipulatorClass,self._AddPathClass,self._UndoClass,self._RedoClass,self._SelectWallClass)
		self._RotateModelClass.AddClass(self._InputClass,self._ItemControlClass,self._CharacterControlClass,self._SelectModelClass,self._ManipulatorClass,self._UndoClass,self._RedoClass)
		self._ChangeModeClass.AddClass(self._InputClass,self._ManipulatorClass)
		self._SetLightButtonClass.AddClass(self._InputClass,self._ItemControlClass,self._UndoClass,self._RedoClass)
		
		self.__LogVR = LogVR.LogVR()
			
		self._AltKeyState = False
		self._QKeyState = False
		self._PreQKeyState = False
		
		self._Is3d = False
		
		self._ModeState = 0
		self.__mVRState = False
		
		viz.callback(viz.EXIT_EVENT,self.OnExit)	#ソフト終了時のメッセージ設定

	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Scene] : Start Scene Update')
		
		#パスシミュレータ時は処理を行わない
		if self._ModeState == 1:
			return
			
		#画面サイズの確認
		self.CheckWindowSize()
		
		#入力関係処理
		self._InputClass.Update()
		
		if self.__mVRState:
			if self.__mViewVR:
				self.__mViewVR.Update()
				self._RoomControlClass.CeilingVisibleUpdate()
				self.__LogVR.Update(self._InputClass.GetLogKeyState(),self.__mViewVR.GetViewState())
			if self._InputClass.GetOKeyDownState():
				self.ChangeMode()
			return
		
		#メニューの選択
		self._ChangeModeClass.Update()
		
		self._StencilClass.Update()
		self._FunctionButtonAreaClass.Update()
		self._ChangeViewClass.Update()
		#self._BackgroundClass.Update()
		#self._SetDirectionClass.Update()
		#self._AddLogClass.Update()
		#self._AddPathClass.Update()
		#self._PlayPathClass.Update()
		#self._ShowDrawingClass.Update()
		#self._ShowPathClass.Update()
		#self._SetFileterClass.Update()
		self._UndoClass.Update()
		self._RedoClass.Update()
		
		#ボタン
		self._SetWallHeightButtonClass.Update()
		self._SetFloorTextureButtonClass.Update()
		self._SetWallTextureButtonClass.Update()
		self._SetCeilingTextureButtonClass.Update()
		self._SetWindowHeightButtonClass.Update()
		self._SetItemButtonClass.Update()
		self._SetRoomButtonClass.Update()
		self._SetEnvironmentButtonClass.Update()
		self._SetItemSizeButtonClass.Update()
		self._SetItemTextureButtonClass.Update()
		self._SetLogButtonClass.Update()
		self._SetDrawingButtonClass.Update()
		self._SetDrawingScaleButtonClass.Update()
		self._SetPathButtonClass.Update()
		self._SetLightButtonClass.Update()
		
		#インターフェイスの選択状態の確認
		self._CheckStateClass.Update()
		
		#配置操作
		self._ManipulatorClass.Update()
		
		self._SelectModelClass.Update()
		
		self._SelectWallClass.Update()
		self._SelectStartPositionClass.Update()
		self._SelectLogClass.Update()
		self._SelectPathClass.Update()
		
		self._TranslateModelClass.Update()
		self._RotateModelClass.Update()
		self._ScaleItemClass.Update()
		self._TranslateStartPositionClass.Update()
		self._RotateStartPositionClass.Update()
		self._TranslateLogClass.Update()
		self._RotateLogClass.Update()
		self._TranslatePathClass.Update()
		self._TranslateWallClass.Update()
		
		#通路幅変更機能操作
		#self._OffsetItemClass.Update()
		
		#視点操作
		clickState = self.CheckClickState()
		if self._ChangeViewClass.GetViewState() == False:
			self._View2dClass.Update(clickState)
		else:
			self._View3dClass.Update(clickState)
		
		#各部屋を更新
		roomCount = self._RoomControlClass.GetRoomCount()
		for x in range(roomCount):
			room = self._RoomControlClass.GetRoom(x)
			room.Update()
			
		#Alt+Qでソフト終了
		self._AltKeyState = self._InputClass.GetAltKeyState()
		self._QKeyState = self._InputClass.GetQKeyState()
		if self._AltKeyState and self._PreQKeyState and self._QKeyState==False:
			answer = vizinput.ask('ソフトを終了しますか？',title='RiMMInspector')
			if answer == 1:
				viz.quit()
			else:
				self._InputClass.ResetAltKey()
		
		#モード（レイアウター/パスシミュレータ）切り替え
		if self._ChangeModeClass.GetModeState():
			self._ModeState = 1
			self.SetMode(self._ModeState)
			
		self._PreQKeyState = self._QKeyState
		
	#選択状態の確認
	def CheckClickState(self):
		clickState = False
		
		drugState			= self._StencilClass.GetClickState()
		functionState		= self._FunctionButtonAreaClass.GetClickState()
		manipulatorState	= self._ManipulatorClass.GetState()
		
		if drugState or functionState or manipulatorState:
			clickState = True
			
		return clickState
		
	#画面サイズの確認
	def CheckWindowSize(self):
		windowSize = viz.window.getSize()
		
		if windowSize[0] != 0 and windowSize[1] != 0:
			if windowSize[0] != self._WindowSize[0] or windowSize[1] != self._WindowSize[1]:
				if self._ChangeViewClass.GetViewState():
					self._View3dClass.SetAspectRatio()
				else:
					self._View2dClass.SetAspectRatio()
				
				self._StencilClass.SetAspectRatio()
				self._FunctionButtonAreaClass.SetAspectRatio()
				self._ChangeViewClass.SetAspectRatio()
				#self._BackgroundClass.SetAspectRatio()
				self._ShowDrawingClass.SetAspectRatio()
				self._ShowPathClass.SetAspectRatio()
				self._AddPathClass.SetAspectRatio()
				self._PlayPathClass.SetAspectRatio()
				self._SetRoomButtonClass.SetAspectRatio()
				self._SetItemButtonClass.SetAspectRatio()
				self._SetDrawingButtonClass.SetAspectRatio()
				self._SetDrawingScaleButtonClass.SetAspectRatio()
				self._SetPathButtonClass.SetAspectRatio()
				self._SetItemSizeButtonClass.SetAspectRatio()
				self._SetItemTextureButtonClass.SetAspectRatio()
				self._SetWallHeightButtonClass.SetAspectRatio()
				self._SetFloorTextureButtonClass.SetAspectRatio()
				self._SetWallTextureButtonClass.SetAspectRatio()
				self._SetCeilingTextureButtonClass.SetAspectRatio()
				self._SetWindowHeightButtonClass.SetAspectRatio()
				self._SetFileterClass.SetAspectRatio()
				self._UndoClass.SetAspectRatio()
				self._RedoClass.SetAspectRatio()
				self._ChangeModeClass.SetAspectRatio()
				self._SetLightButtonClass.SetAspectRatio()
				
			self._WindowSize = windowSize
		
	#終了時の処理
	def OnExit(self):
		roomCount = self._RoomControlClass.GetRoomCount()
		itemCount = self._ItemControlClass.GetItemCount()
		characterCount = self._CharacterControlClass.GetCharacterCount()
		
		#テキストの作成
		questionText = ''
		if roomCount != 0 and itemCount == 0 and characterCount == 0:
			questionText = '間取りの状態を保存しますか？'
		elif roomCount == 0 and itemCount != 0 and characterCount == 0:
			questionText = 'アイテムの状態を保存しますか？'
		elif roomCount != 0 and itemCount != 0 and characterCount == 0:
			questionText = '間取り、アイテムの状態を保存しますか？'
		elif roomCount == 0 and itemCount == 0 and characterCount != 0:
			questionText = 'パスの状態を保存しますか？'
		elif roomCount != 0 and itemCount == 0 and characterCount != 0:
			questionText = '間取り、パスの状態を保存しますか？'
		elif roomCount == 0 and itemCount != 0 and characterCount != 0:
			questionText = 'アイテム、パスの状態を保存しますか？'
		elif roomCount != 0 and itemCount != 0 and characterCount != 0:
			questionText = '間取り、アイテム、パスの状態を保存しますか？'
		
		"""
		if questionText != '':
			answer = vizinput.ask(questionText,title='RiMMInspector')
			
			if answer == 1:		#「はい」を選択したらファイル名を付けて保存
				if roomCount != 0:
					self._SetRoomButtonClass.SaveRoom()
				if itemCount != 0:
					self._SetItemButtonClass.SaveItem()
				if characterCount != 0:
					self._SetPathButtonClass.SavePath()
		"""
		
		#FOVEの視線トラッキング処理を終了
		if self.__mViewVR != None:
			self.__mViewVR.Finalize()
			del self.__mViewVR
		
	#モードを設定
	def SetMode(self,state,viewPos=None,buttonState=None,eyeHeightList=None):
		self._ModeState = state
		self.ChangeMode(self._ModeState,viewPos,buttonState,eyeHeightList)
		
	#モードを取得
	def GetMode(self):
		return self._ModeState
		
	#---------------------------------------------------------
	def ChangeMode(self):
		"""
		モードを切り替え
		"""
		if not self.__mViewVR:
			return
			
		self.__mVRState = not self.__mVRState
		
		# ボタンを表示・非表示
		if self.__mVRState:
			self._ChangeModeClass.Rollover(False)
			
			#選択を解除
			if self._PlayPathClass.GetPlayState():
				self._PlayPathClass.SetPlayState(False)
				self._PlayPathClass.Stop()
				
			self._Is3d = self._ChangeViewClass.GetViewState()
			if self._Is3d == False:
				self._ChangeViewClass.Change()
				
			self._SelectModelClass.UnSelect()
			self._SelectWallClass.UnSelect()
			self._SelectPathClass.UnSelect()
			self._SelectStartPositionClass.UnSelect()
			
			self._ManipulatorClass.Update()
			self._AddPathClass.Update()
			self._PlayPathClass.Update()
			self._FunctionButtonAreaClass.Update()
			
			#キャラクター関係を非表示
			self._CharacterControlClass.SetVisibleCharacterAndPath(False)
			#self._PathControlClass.SetVisible(False)
			self._ItemControlClass.SetAllItemLineVisible(True)
			self._ItemControlClass.SetAllItemLineColor(False)
			self._ItemControlClass.CreateItemAnimationPath()
			
			#ダミーボックスのラインを非表示
			self._ItemControlClass.SetDummyBoxLineVisible(False)
			
			#音を再生
			self._ItemControlClass.PlaySound(True)
			
			#照明を反映
			self._ItemControlClass.PlayLight(True)
			
			#ボタンなどを非表示
			self._ChangeViewClass.SetVisible(False)
			#self._SetFileterClass.SetVisible(False)
			#self._BackgroundClass.SetVisible(False)
			#self._ShowDrawingClass.SetVisible(False)
			#self._ShowPathClass.SetVisible(False)
			self._UndoClass.SetVisible(False)
			self._RedoClass.SetVisible(False)
			self._FunctionButtonAreaClass.SetVisible(False)
			self._StartPositionClass.SetVisible(False)
			self._StencilClass.SetVisible(False)
			
			self._ChangeModeClass.SetVisible(False)
			
		else:
			#キャラクター関係を表示
			self._CharacterControlClass.SetVisibleCharacterAndPath(True)
			self._ItemControlClass.SetAllItemLineColor(True)
			if self._ShowPathClass.GetPathVisible() == False:
				self._PathControlClass.SetVisible(False)
			self._ItemControlClass.DeleteItemAnimationPath()
				
			#ダミーボックスのラインを表示
			self._ItemControlClass.SetDummyBoxLineVisible(True)
			
			#音を再生
			self._ItemControlClass.PlaySound(False)
			
			#照明を反映
			self._ItemControlClass.PlayLight(False)
			
			#ボタンを表示
			self._ChangeViewClass.SetVisible(True)
			self._SetFileterClass.SetVisible(True)
			self._BackgroundClass.SetVisible(True)
			self._ShowDrawingClass.SetVisible(True)
			self._ShowPathClass.SetVisible(True)
			self._UndoClass.SetVisible(True)
			self._RedoClass.SetVisible(True)
			self._FunctionButtonAreaClass.SetVisible(True)
			self._StartPositionClass.SetVisible(True)
			
			self._ChangeModeClass.SetVisible(True)
			
			if self._Is3d == False:
				self._ChangeViewClass.Change()
			self._StencilClass.SetVisible(not self._Is3d)
		
		self.__mViewVR.SetWindowVisible(self.__mVRState)
			
	#グリッドを取得
	def GetGrid(self):
		return self._BackgroundClass
		
	#グリッドを取得
	def GetBg(self):
		return self._BackgroundClass.GetBgModel()
		
	#インプットクラスを取得
	def GetInput(self):
		return self._InputClass
		
	#レイアウトの情報を取得
	def GetLayoutData(self):
		data = []
		data.append(self._RoomDataClass)
		data.append(self._ItemDataClass)
		data.append(self._PathDataClass)
		data.append(self._CharacterControlClass.GetSaveData())
		data.append(self._View3dClass.GetViewPos())
		return data
		
	#表示状態を取得
	def GetButtonState(self):
		gridState		= self._BackgroundClass.GetGridVisible()
		drawingState	= self._DrawingClass.GetVisible()
		pathState		= self._ShowPathClass.GetPathVisible()
		
		return [gridState,drawingState,pathState]
		