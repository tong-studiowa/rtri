﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

#areaSelectHeightTemp = iniFile.GetItemData('Setting','AreaSelectHeight')
#areaSelectHeight = float(areaSelectHeightTemp)
areaSelectHeight = 10000.0

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログ選択制御のクラス定義
class SelectLog(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectLog] : Init SelectLog')
		
		self._InputClass = None
		self._LogControlClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._SelectWallClass = None
		self._SelectModelClass = None
		self._SelectStartPositionClass = None
		self._CheckStateClass = None
		
		self._SelectedModel = None
		self._ClickState = False
		
		self._RotateSnap = rotateSnap
		
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,stencilClass,manipulatorClass,selectWallClass
				,selectModelClass,selectStartPositionClass,checkStateClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._SelectWallClass = selectWallClass
		self._SelectModelClass = selectModelClass
		self._SelectStartPositionClass = selectStartPositionClass
		self._CheckStateClass = checkStateClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectLog] : Start SelectLog Update')
		
		self._ClickState = False
		modelNum = self._SelectedModel
		
		#シングルクリックによる選択
		if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True:
			modelNum = self.Select()
			
			#マニピュレータを操作中
			if self._ManipulatorClass.GetState():
				pass
				
			#選択中のモデルをクリック
			elif modelNum != None and modelNum == self._SelectedModel:
				pass
				
			#モデルをクリックし、選択
			elif modelNum != None:
				#選択していたモデルのハイライト表示をOFF
				if self._SelectedModel != None:
					self._LogControlClass.SetHighlight(self._SelectedModel,False)
				
				self._SelectedModel = modelNum
				self._LogControlClass.SetHighlight(self._SelectedModel,True)
				
				self._ManipulatorClass.SetArrowYVisible(1)
				self._ManipulatorClass.SetCircle2Visible(1)
				
				log = self._LogControlClass.GetLog(self._SelectedModel)
				model = log.GetModel()
				self._ManipulatorClass.SetModel(model,False)	#マニピュレータ設定
				
			#選択中にモデル以外をクリック
			else:
				if self._SelectedModel != None:
					self._ClickState = True
					
					#選択を解除し、モデルのハイライト表示をOFF
					self._LogControlClass.SetHighlight(self._SelectedModel,False)
					
					selectedLog = self._LogControlClass.GetLog(self._SelectedModel)
					selectedModel = selectedLog.GetModel()
					
					if selectedModel == self._ManipulatorClass.GetModel():
						self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
						
					self._SelectedModel = None
					
		if self._StencilClass.GetClickState():
			if self._SelectedModel != None:
				
				#選択を解除し、モデルのハイライト表示をOFF
				self._LogControlClass.SetHighlight(self._SelectedModel,False)
				self._SelectedModel = None
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
		if self._SelectModelClass.GetDragState():
			if self._SelectedModel != None:
				
				#選択を解除し、モデルのハイライト表示をOFF
				self._LogControlClass.SetHighlight(self._SelectedModel,False)
				self._SelectedModel = None
				
	#選択
	def Select(self):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		modelNum = None
		
		selectedModel = None
		selectedState = False
		selectedRoom,selectedModel,selectedModelState = self._SelectWallClass.GetSelectedModel()
		if selectedModel == None:
			selectedState = self._SelectStartPositionClass.GetSelectedState()
						
		if selectedModel == None and selectedState == False and self._SelectModelClass.GetSelectedState() == False:
			self._LogControlClass.SetPickableForAllLog(True)
			state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
			self._LogControlClass.SetPickableForAllLog(False)
			
			modelNum = self._LogControlClass.GetLogNum(pickedModel)
			
		return modelNum
	
	#選択解除
	def UnSelect(self):
		if self._SelectedModel != None:
			self._LogControlClass.SetHighlight(self._SelectedModel,False)
			self._SelectedModel = None
			self._SelectedNode = None
			
		self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
	#選択しているモデルの取得
	def GetSelectedModel(self):
		selectedModel = self._SelectedModel
		return selectedModel
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
	