﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import LoadIniFile

from os import path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#スタート位置制御のクラス定義
class StartPosition(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][StartPosition] : Init StartPosition')
		
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionDefault = [0.0,1.6,0.0]
		self._Orientation = 0.0
		
		self._RotateSnap = rotateSnap
		
		self.SetModel()
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Start Item Update')
		pass
		
	#モデル設定
	def SetModel(self):
		filePath = viz.res.getPublishedPath('Resource\\Interface\\StartPosition.osgb')
			
		model = viz.addChild(filePath)	#モデルを追加
		
		self._Model = model
		
		self._Model.color(viz.BLUE)
		self._Model.emissive(viz.BLUE)
		self._Model.ambient(viz.BLUE)
		self._Model.specular(viz.BLUE)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		
		self.SetPickable(False)
		
		self.SetVisible(False)
		
	#リセット
	def Reset(self):
		self.SetPosition(self._PositionDefault)
		self.SetOrientation(0.0)
		self.ResetOrientationSnap()
		
	#位置設定
	def SetPosition(self,iPosition):
		if iPosition != None:
			position = [0,0,0]
			position[0] = iPosition[0]
			position[1] = iPosition[1]
			position[2] = iPosition[2]
			
			self._Model.setPosition(position)
			self._Position = position
		
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
			
			#スナップ
			snapOrientation = 0
			count = 360 / self._RotateSnap
			for x in range(count):
				angle = self._RotateSnap * x + self._RotateSnap / 2
				if self._Orientation < angle:
					snapOrientation = self._RotateSnap * x
					break
			
			self._Model.setEuler(snapOrientation)
			
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		snapOrientation = 0
		count = 360 / self._RotateSnap
		for x in range(count):
			angle = self._RotateSnap * x + self._RotateSnap / 2
			if self._Orientation < angle:
				snapOrientation = self._RotateSnap * x
				break
		
		self._Orientation = snapOrientation
		
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#角度設定(INIファイル用)
	def SetAxisAngle(self,axisAngle):
		if self._Model != None and axisAngle != None:
			self._Model.setAxisAngle(axisAngle)
			
			val = 0
			yVal = axisAngle[1]
			angleVal = axisAngle[3]
			if yVal < -0.5:
				val = angleVal * -1
			elif yVal > 0.5:
				val = angleVal
			if val < 0:
				val += 360
			self._Orientation = val
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self):
		axisAngle = self._Model.getAxisAngle(viz.ABS_GLOBAL)
		return axisAngle
	
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.specular(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.specular(self._Specular)
			
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
	
	#表示状態設定
	def SetVisible(self,state):
		if state:
			#self._Model.visible(viz.ON)
			self._Model.visible(viz.OFF)
		else:
			self._Model.visible(viz.OFF)
			