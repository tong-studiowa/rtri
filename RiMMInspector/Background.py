﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat
import vizfx

import math
import Object2d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
BG_MODEL_PATH = 'Resource\\Bg\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

gridStepTemp = iniFile.GetItemData('Setting','GridStep')
gridStep = float(gridStepTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#背景画像制御のクラス定義
class Background(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Background] : Init Background')
		
		self._InputClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		
		self._GridSize = [100,100]
		self._GridStep = gridStep
		
		self._SkyModel = None
		self._GroundModel = None
		
		#環境設定
		viz.clearcolor(viz.WHITE)
		#viz.fog(30,60)
		#viz.fogcolor(viz.WHITE)
		
		self.SetLight()
		
		#グリッド設定
		self._Grid = vizshape.addGrid(self._GridSize,self._GridStep*10)
		
		self._Grid.disable(viz.PICKING)
		self._Grid.disable(viz.LIGHTING)
		self._Grid.color(viz.BLACK)
		self._Grid.alpha(0.9)
		
				
		self._Grid.disable(viz.DEPTH_WRITE)
		self._Grid.drawOrder(-30)
		self._Grid.setPosition([0,0.005,0])
		
		self._Grid2 = vizshape.addGrid(self._GridSize,self._GridStep)
		
		self._Grid2.disable(viz.PICKING)
		self._Grid2.disable(viz.LIGHTING)
		self._Grid2.color(viz.BLACK)
		self._Grid2.alpha(0.4)
		
		self._Grid2.disable(viz.DEPTH_WRITE)
		self._Grid2.drawOrder(-29)
		
		self._Grid2.setParent(self._Grid)
		
		#配置用の透明な板設定
		self._BgPlane = vizshape.addPlane([2000,2000])
		self._BgPlane.alpha(0)
		self._BgPlane.disable(viz.DEPTH_WRITE)
		
		#フレーム追加
		self._Position = [226,264]
		self._PositionX = [226,63]
		self._Scale = [100,36]
		#self._Frame = self.CreateFrame(self._Position,self._Scale)
		self._Frame = None
		
		#テキスト追加
		self._TextScale = [18.6,18.6]
		#self._Text = self.CreateText("",self._Position,self._TextScale)
		self._Text = None
		
		#背景モデルの設定（空、地面）
		self.SetBgModel()
		
	#使用するクラスの追加
	def AddClass(self,inputClass,stencilClass,manipulatorClass):
		self._InputClass = inputClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Background] : Start Background Update')

		self._ClickState = False
		self._OverState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		#self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
				
		if self._OverState:
			if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
				self._ClickState = True
				
				if self._IsVisible == True:
					self._IsVisible = False
				else:
					self._IsVisible = True
					
				self.Set()
		
		#ロールオーバー
		self.Rollover(self._OverState)

	#リセット処理
	def Reset(self):
		self._ClickState = False
		self._IsVisible = True
		self.SetText(True)
		self.SetGridVisible(True)
		
	#設定を反映
	def Set(self):
		if self._IsVisible == False:
			self.SetText(False)
			self.SetGridVisible(False)
		else:
			self.SetText(True)
			self.SetGridVisible(True)
			
	#ライト設定
	def SetLight(self):
		"""
		Light = viz.addLight()
		Light.position(0,1,0)
		Light.direction(0,-1,0)
		Light.intensity(1)
		Light.setPosition([0,1000,0])
		
		viz.MainView.getHeadLight().disable()
		
		skyLight=vizfx.lighting.addDirectionalLight(euler=(40,15,0))
		skyLight.color([0.6,0.6,0.6])
		skyLight.ambient([0.1]*3)
		viz.setOption('viz.lightModel.ambient',[0]*3)
		
		skyLight2=vizfx.lighting.addDirectionalLight(euler=(240,30,0))
		skyLight2.color([0.4,0.4,0.4])
		"""
		
		"""
		Light = viz.addLight()
		Light.position(0,1,0)
		Light.direction(0,-1,0)
		Light.intensity(1)
		Light.setPosition([0,1000,0])
		
		skyLight=vizfx.lighting.addDirectionalLight(euler=(40,15,0))
		skyLight.color([0.6,0.6,0.6])
		skyLight.ambient([0.1]*3)
		viz.setOption('viz.lightModel.ambient',[0]*3)
		
		skyLight2=vizfx.lighting.addDirectionalLight(euler=(240,30,0))
		skyLight2.color([0.4,0.4,0.4])
		"""
		
		viz.MainWindow.getView().getHeadLight().disable()
		viz.MainView.getHeadLight().disable()
		
	#表示状態設定
	def SetVisible(self,state):
		pass
		"""
		if state:
			self._Frame.visible(viz.ON)
			self._Text.visible(viz.ON)
		else:
			self._Frame.visible(viz.OFF)
			self._Text.visible(viz.OFF)
		"""
		
	#グリッドの表示状態設定
	def SetGridVisible(self,state):
		val = viz.OFF
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		self._Grid.visible(val)
		
	#グリッドの表示状態取得
	def GetGridVisible(self):
		return self._IsVisible
		
	#選択状態設定
	def SetPickable(self,state):
		if state == True:
			self._BgPlane.enable(viz.PICKING)
		else:
			self._BgPlane.disable(viz.PICKING)
		
	#テキスト設定
	def SetText(self,state):
		pass
		"""
		if state == False:
			self._Text.message("グリッド有")
		else:
			self._Text.message("グリッド無")
		"""
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		#self._Frame.color(color)
		
	#背景モデルの設定
	def SetBgModel(self):
		self._SkyModel = viz.addChild(viz.res.getPublishedPath(BG_MODEL_PATH+'sky.osgb'))
		self._SkyModel.disable(viz.PICKING)
		
		self._GroundModel = vizshape.addPlane([35,35])
		self._GroundModel.disable(viz.PICKING)
		
		self._GroundModel.color(0.3,0.3,0.3)
		self._GroundModel.setParent(self._SkyModel)
		self._GroundModel.disable(viz.LIGHTING)
		self._GroundModel.drawOrder(-200)
		
		self._SkyModel.setPosition([0,-10,0])
		self._SkyModel.setScale(100,100,100)
		self._SkyModel.color(viz.WHITE)
		self._SkyModel.disable(viz.LIGHTING)
		self._SkyModel.drawOrder(-300)
		
	#背景モデルの取得
	def GetBgModel(self):
		return [self._SkyModel,self._GroundModel]
		
	#グリッドモデルの取得
	def GetModel(self):
		return self._Grid
		
	#表示状態の設定
	def SetViewState(self,state):
		if state:
			self._Position[0] = self._PositionX[1]
		else:
			self._Position[0] = self._PositionX[0]
		
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Text,self._Position,self._TextScale)
		