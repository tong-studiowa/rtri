﻿# Object3d
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2013/10/07
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Object3d(Interface.Interface):
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass
