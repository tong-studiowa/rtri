﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋関係のファンクション制御のクラス定義
class RoomFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomFunction] : Init RoomFunction')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectModelClass = None
		self._SetWallHeightButtonClass = None
		self._SetFloorTextureButtonClass = None
		self._SetWallTextureButtonClass = None
		self._SetCeilingTextureButtonClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectModelClass,setWallHeightButtonClass
				,setFloorTextureButtonClass,setWallTextureButtonClass,setCeilingTextureButtonClass
				,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectModelClass = selectModelClass
		self._SetWallHeightButtonClass = setWallHeightButtonClass
		self._SetFloorTextureButtonClass = setFloorTextureButtonClass
		self._SetWallTextureButtonClass = setWallTextureButtonClass
		self._SetCeilingTextureButtonClass = setCeilingTextureButtonClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomFunction] : Start RoomFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.SetHeight()
			self._SetFloorTextureButtonClass.SetVisible(False)
			self._SetWallTextureButtonClass.SetVisible(False)
			self._SetCeilingTextureButtonClass.SetVisible(False)
		elif num == 1:
			self.SetFloorTexture()
			self._SetWallHeightButtonClass.SetVisible(False)
			self._SetWallTextureButtonClass.SetVisible(False)
			self._SetCeilingTextureButtonClass.SetVisible(False)
		elif num == 2:
			self.SetWallTexture()
			self._SetWallHeightButtonClass.SetVisible(False)
			self._SetFloorTextureButtonClass.SetVisible(False)
			self._SetCeilingTextureButtonClass.SetVisible(False)
		elif num == 3:
			self.SetCeilingTexture()
			self._SetWallHeightButtonClass.SetVisible(False)
			self._SetFloorTextureButtonClass.SetVisible(False)
			self._SetWallTextureButtonClass.SetVisible(False)
		elif num == 4:
			self.Delete()
			self._SetWallHeightButtonClass.SetVisible(False)
			self._SetFloorTextureButtonClass.SetVisible(False)
			self._SetWallTextureButtonClass.SetVisible(False)
			self._SetCeilingTextureButtonClass.SetVisible(False)
	
	#選択が解除された
	def Hide(self):
		self._SetWallHeightButtonClass.SetVisible(False)
		self._SetFloorTextureButtonClass.SetVisible(False)
		self._SetWallTextureButtonClass.SetVisible(False)
		self._SetCeilingTextureButtonClass.SetVisible(False)
		
	#高さ変更
	def SetHeight(self):
		if self._SetWallHeightButtonClass.GetVisible():
			self._SetWallHeightButtonClass.SetVisible(False)
		else:
			self._SetWallHeightButtonClass.SetVisible(True)
			
			selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
			height = self._RoomControlClass.GetWallHeight(selectedRoomList[0])
			self._SetWallHeightButtonClass.SetValue(selectedRoomList,height)
			
	#床変更
	def SetFloorTexture(self):
		if self._SetFloorTextureButtonClass.GetVisible():
			self._SetFloorTextureButtonClass.SetVisible(False)
		else:
			self._SetFloorTextureButtonClass.SetVisible(True)
			
			selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
			texture = self._RoomControlClass.GetFloorTexture(selectedRoomList[0])
			self._SetFloorTextureButtonClass.SetValue(selectedRoomList,texture)
		
	#壁紙変更
	def SetWallTexture(self):
		if self._SetWallTextureButtonClass.GetVisible():
			self._SetWallTextureButtonClass.SetVisible(False)
		else:
			self._SetWallTextureButtonClass.SetVisible(True)
			
			selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
			texture = self._RoomControlClass.GetWallTexture(selectedRoomList[0])
			self._SetWallTextureButtonClass.SetValue(selectedRoomList,texture)
		
	#天井変更
	def SetCeilingTexture(self):
		if self._SetCeilingTextureButtonClass.GetVisible():
			self._SetCeilingTextureButtonClass.SetVisible(False)
		else:
			self._SetCeilingTextureButtonClass.SetVisible(True)
			
			selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
			texture = self._RoomControlClass.GetCeilingTexture(selectedRoomList[0])
			self._SetCeilingTextureButtonClass.SetValue(selectedRoomList,texture)
		
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(16)
		
		selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
		
		for x in range(len(selectedRoomList)):
			roomNum = selectedRoomList[x]
			
			file,roomName	= self._RoomControlClass.GetFileName(roomNum)
			pos				= self._RoomControlClass.GetFloorPosition(roomNum)
			pointData		= self._RoomControlClass.GetWallPointList(roomNum)
			wallheight		= self._RoomControlClass.GetWallHeight(roomNum)
			texData			= self._RoomControlClass.GetTextureData(roomNum)
			doorData		= self._RoomControlClass.GetDoorData(roomNum)
			windowData		= self._RoomControlClass.GetWindowData(roomNum)
			wallVisible		= self._RoomControlClass.GetWallVisibleList(roomNum)
			isOpRoom		= self._RoomControlClass.GetIsOpRoom(roomNum)
			
			self._UndoData.append([roomNum,file,pos,pointData,wallheight,texData,doorData,windowData,wallVisible,isOpRoom])
			
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
				
		self._SelectModelClass.UnSelectRoom()
		self._SelectModelClass.UnSelectDoor()
		self._SelectModelClass.UnSelectWindow()
		
		for x in range(len(selectedRoomList)):
			roomNum = selectedRoomList[x] - x
			
			self._RoomControlClass.Delete(roomNum)
		