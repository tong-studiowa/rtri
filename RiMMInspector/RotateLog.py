﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログ回転制御のクラス定義
class RotateLog(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateLog] : Init RotateLog')
		
		self._InputClass = None
		self._LogControlClass = None
		self._SelectLogClass = None
		self._ManipulatorClass = None
		
		self._SelectedModel = None
		
		self._RotateVal = 0.0
		self._AngleVal = 0.0
		self._PreAngleVal = 0.0
		
		self._State = False
		self._PreState = False
		
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,selectLogClass,manipulatorClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._SelectLogClass = selectLogClass
		self._ManipulatorClass = manipulatorClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateLog] : Start RotateLog Update')
		
		self._State = False
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState == 2:
			self._SelectedModel = self._SelectLogClass.GetSelectedModel()
			if self._SelectedModel != None:
				self.Rotate()
				self._State = True
		else:
			self._RotateVal = 0.0

		if self._State == False and self._PreState == True:
			self.ResetOrientationSnap()
			
		self._PreState = self._State
		
	#回転
	def Rotate(self):
		logPosition = self._LogControlClass.GetPosition(self._SelectedModel)
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		posX = logPosition[0] - pickingPosition[0]
		posZ = logPosition[2] - pickingPosition[2]
		
		angleTemp = math.atan2(posX,posZ)
		angle = math.degrees(angleTemp)
		
		self._AngleVal = angle
		
		rotateValTemp = self._AngleVal - self._PreAngleVal
		if rotateValTemp > -10.0 and rotateValTemp < 10:
			self._RotateVal = self._AngleVal - self._PreAngleVal
		
		logAngle = self._LogControlClass.GetOrientation(self._SelectedModel)
		newLogAngle = logAngle + self._RotateVal
		
		if newLogAngle < 0:
			newLogAngle = newLogAngle + 360
		elif newLogAngle > 360:
			newLogAngle = newLogAngle - 360
			
		self._LogControlClass.SetOrientation(self._SelectedModel,newLogAngle)
		
		self._PreAngleVal = angle
		
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		self._LogControlClass.ResetOrientationSnap(self._SelectedModel)
		