﻿# coding: utf-8

import viz

import Object2d
import GeneralFunction
import ItemFunction
import WallFunction
import PillarFunction
import DoorFunction
import WindowFunction
import RoomFunction
import LogFunction
import CharacterFunction
import PathFunction
import ShortcutKeyControl

import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

functionButtonAreaFilePath = iniFile.GetItemData('Setting','FunctionButtonArea')
functionButtonAreaFile = LoadIniFile.LoadIniFileClass()
functionButtonAreaFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+functionButtonAreaFilePath))

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ファンクションボタンエリア制御のクラス定義
class FunctionButtonArea(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][FunctionButtonArea] : Init FunctionButtonArea')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._SelectModelClass = None
		self._RoomControlClass = None
		self._SelectWallClass = None
		self._SetWallHeightButtonClass = None
		self._SetFloorTextureButtonClass = None
		self._SetWallTextureButtonClass = None
		self._SetCeilingTextureButtonClass = None
		self._SetWindowHeightButtonClass = None
		self._SetItemButtonClass = None
		self._SetRoomButtonClass = None
		self._SetEnvironmentButtonClass = None
		self._SetItemSizeButtonClass = None
		self._SetItemTextureButtonClass = None
		self._StartPositionClass = None
		self._EnvironmentControlClass = None
		self._LogControlClass = None
		self._SelectLogClass = None
		self._SetLogButtonClass = None
		self._DrawingClass = None
		self._SetDrawingButtonClass = None
		self._CharacterControlClass = None
		self._SetPathButtonClass = None
		self._PathControlClass = None
		self._SelectPathClass = None
		self._AddPathClass = None
		self._PlayPathClass = None
		self._SetDrawingScaleButtonClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._TranslateModelClass = None
		self._ChangeViewClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._RotateModelClass = None
		self._ScaleItemClass = None
		self._SetLightButtonClass = None
		
		self._ShortcutKeyControlClass = None
		
		self._FunctionState = 0
		self._FunctionClassList = []
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		
		self._PreItem = None
		self._PreWindow = None
		self._PreRoom = None
		self._PreLog = None
		
		self._CopyItemList = []
		self._CopyRoomList = []
		self._CopyCharacterList = []
		
		#インターフェイスの作成
		self._Frame = None
		self._TextList = []
		
		self._IconCount = 5
		self._IconList = []
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [(windowSize[0]-26)/2+13+30,windowSize[1]-36]
		self._FrameScale = [windowSize[0]-26-58,46]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#アイコン、テキスト追加
		self._IconPosition = [118+58,windowSize[1]-36]
		self._IconScale = [200,36]
		self._TextScale = [18.6,18.6]
		self._IconPositionList = []
		
		for x in range(self._IconCount):
			iconPos = [0.0,0.0]
			iconPos[0] = self._IconPosition[0] + (205 * x)
			iconPos[1] = self._IconPosition[1]
			self._IconPositionList.append(iconPos)
			
			icon = self.CreateIcon(iconPos,self._IconScale)
			self._IconList.append(icon)
			
			text = self.CreateText("",iconPos,self._TextScale)
			self._TextList.append(text)
			
		#ファンクションの設定
		self.CreateFunction()
		
	#ファンクションの作成
	def CreateFunction(self):
		self._FunctionClassList = []
		
		generalFunctionClass = GeneralFunction.GeneralFunction()		#0:基本
		self._FunctionClassList.append(generalFunctionClass)
		
		itemFunctionClass = ItemFunction.ItemFunction()					#1:アイテム
		self._FunctionClassList.append(itemFunctionClass)
		
		wallFunctionClass = WallFunction.WallFunction()					#2:壁
		self._FunctionClassList.append(wallFunctionClass)
		
		pillarFunctionClass = PillarFunction.PillarFunction()			#3:柱
		self._FunctionClassList.append(pillarFunctionClass)
		
		doorFunctionClass = DoorFunction.DoorFunction()					#4:ドア
		self._FunctionClassList.append(doorFunctionClass)
		
		windowFunctionClass = WindowFunction.WindowFunction()			#5:窓
		self._FunctionClassList.append(windowFunctionClass)
		
		RoomFunctionClass = RoomFunction.RoomFunction()					#6:部屋
		self._FunctionClassList.append(RoomFunctionClass)
		
		logFunctionClass = LogFunction.LogFunction()					#7:ログ
		self._FunctionClassList.append(logFunctionClass)
		
		characterFunctionClass = CharacterFunction.CharacterFunction()	#8:キャラクター
		self._FunctionClassList.append(characterFunctionClass)
		
		pathFunctionClass = PathFunction.PathFunction()					#9:パス
		self._FunctionClassList.append(pathFunctionClass)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,selectModelClass,roomControlClass,selectWallClass
				,setWallHeightButtonClass,setFloorTextureButtonClass
				,setWallTextureButtonClass,setCeilingTextureButtonClass,setWindowHeightButtonClass
				,setItemButtonClass,setRoomButtonClass,setEnvironmentButtonClass,setItemSizeButtonClass
				,setItemTextureButtonClass,startPositionClass,environmentControlClass,logControlClass
				,selectLogClass,setLogButtonClass,drawingClass,setDrawingButtonClass,characterControlClass
				,setPathButtonClass,pathControlClass,selectPathClass,addPathClass,playPathClass
				,setDrawingScaleButtonClass,stencilClass,manipulatorClass,translateModelClass,changeViewClass
				,undoClass,redoClass,rotateModelClass,scaleItemClass,setLightButtonClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._SelectModelClass = selectModelClass
		self._RoomControlClass = roomControlClass
		self._SelectWallClass = selectWallClass
		self._SetWallHeightButtonClass = setWallHeightButtonClass
		self._SetFloorTextureButtonClass = setFloorTextureButtonClass
		self._SetWallTextureButtonClass = setWallTextureButtonClass
		self._SetCeilingTextureButtonClass = setCeilingTextureButtonClass
		self._SetWindowHeightButtonClass = setWindowHeightButtonClass
		self._SetItemButtonClass = setItemButtonClass
		self._SetRoomButtonClass = setRoomButtonClass
		self._SetEnvironmentButtonClass = setEnvironmentButtonClass
		self._SetItemSizeButtonClass = setItemSizeButtonClass
		self._SetItemTextureButtonClass = setItemTextureButtonClass
		self._StartPositionClass = startPositionClass
		self._EnvironmentControlClass = environmentControlClass
		self._LogControlClass = logControlClass
		self._SelectLogClass = selectLogClass
		self._SetLogButtonClass = setLogButtonClass
		self._DrawingClass = drawingClass
		self._SetDrawingButtonClass = setDrawingButtonClass
		self._CharacterControlClass = characterControlClass
		self._SetPathButtonClass = setPathButtonClass
		self._PathControlClass = pathControlClass
		self._SelectPathClass = selectPathClass
		self._AddPathClass = addPathClass
		self._PlayPathClass = playPathClass
		self._SetDrawingScaleButtonClass = setDrawingScaleButtonClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._TranslateModelClass = translateModelClass
		self._ChangeViewClass = changeViewClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._RotateModelClass = rotateModelClass
		self._ScaleItemClass = scaleItemClass
		self._SetLightButtonClass = setLightButtonClass
		
		self.AddClassToFunction()
		self.CreateShortcutKeyControlClass()
		
		self.Reset()
		
	#各ファンクションへの使用するクラスの追加
	def AddClassToFunction(self):
		for x in range(len(self._FunctionClassList)):
			function = self._FunctionClassList[x]
			if x == 0:		#基本
				function.AddClass(self._InputClass,self._ItemControlClass,self._RoomControlClass
								 ,self._SetItemButtonClass,self._SetRoomButtonClass
								 ,self._SetEnvironmentButtonClass,self._StartPositionClass
								 ,self._EnvironmentControlClass,self._LogControlClass
								 ,self._SetLogButtonClass,self._DrawingClass,self._SetDrawingButtonClass
								 ,self._SetPathButtonClass,self._CharacterControlClass,self._PathControlClass)
			elif x == 1:	#アイテム
				function.AddClass(self._InputClass,self._ItemControlClass,self._SelectModelClass
								 ,self._SetItemSizeButtonClass,self._SetItemTextureButtonClass,self._UndoClass
								 ,self._RedoClass,self._AddPathClass,self._PlayPathClass,self._SetLightButtonClass)
			elif x == 2:	#壁
				function.AddClass(self._InputClass,self._RoomControlClass,self._SelectWallClass,self._UndoClass
								 ,self._RedoClass)
			elif x == 3:	#柱
				function.AddClass(self._InputClass,self._RoomControlClass,self._SelectWallClass,self._UndoClass
								 ,self._RedoClass)
			elif x == 4:	#ドア
				function.AddClass(self._InputClass,self._RoomControlClass,self._SelectModelClass,self._UndoClass
								 ,self._RedoClass)
			elif x == 5:	#窓
				function.AddClass(self._InputClass,self._RoomControlClass,self._SelectModelClass
								 ,self._SetWindowHeightButtonClass,self._UndoClass,self._RedoClass)
			elif x == 6:	#部屋
				function.AddClass(self._InputClass,self._RoomControlClass,self._SelectModelClass
								 ,self._SetWallHeightButtonClass,self._SetFloorTextureButtonClass
								 ,self._SetWallTextureButtonClass,self._SetCeilingTextureButtonClass
								 ,self._UndoClass,self._RedoClass)
			elif x == 7:	#ログ
				function.AddClass(self._InputClass,self._LogControlClass,self._SelectLogClass)
			elif x == 8:	#キャラクター
				function.AddClass(self._InputClass,self._CharacterControlClass,self._SelectModelClass
								 ,self._AddPathClass,self._PlayPathClass,self._PathControlClass,self._UndoClass
								 ,self._RedoClass)
			elif x == 9:	#パス
				function.AddClass(self._InputClass,self._PathControlClass,self._SelectPathClass,self._UndoClass
								 ,self._RedoClass)
				
	#ショートカットキークラスの作成
	def CreateShortcutKeyControlClass(self):
		self._ShortcutKeyControlClass = ShortcutKeyControl.ShortcutKeyControl(self._FunctionClassList)
		self._ShortcutKeyControlClass.AddClass(self._InputClass,self._ManipulatorClass,self._ItemControlClass
											  ,self._RoomControlClass,self._CharacterControlClass,self._PathControlClass
											  ,self._SelectModelClass,self._SelectWallClass,self._SelectPathClass,self._AddPathClass
											  ,self._PlayPathClass,self._ChangeViewClass,self._UndoClass,self._RedoClass
											  ,self._StencilClass,self._TranslateModelClass,self._RotateModelClass,self._ScaleItemClass)
		
		self._TranslateModelClass.SetShortcutKeyControlClass(self._ShortcutKeyControlClass)
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][FunctionButtonArea] : Start FunctionButtonArea Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#アイコンの選択
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				num = None
				for x in range(self._IconCount):
					if self._IconList[x].getVisible():
						if self.CheckRollOver(mousePosition,self._IconPositionList[x],self._IconScale):
							num = x
							
				self._OverIconState = num
				
				#アイコンをクリック
				if self._OverIconState != None:
					if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
						self._ClickState = True
						
						#オペ室の壁の場合はボタンが1つずれる
						if self._FunctionState == 2:
							room,wall,state = self._SelectWallClass.GetSelectedModel()
							isOpRoom = self._RoomControlClass.GetOpRoomState(room)
							
							if isOpRoom:
								self._OverIconState = self._OverIconState + 1
						
						function = self._FunctionClassList[self._FunctionState]
						function.Click(self._OverIconState)
		
		#表示内容設定
		item = self._SelectModelClass.GetSelectedModel()
		itemNode = self._SelectModelClass.GetSelectedNode()
		wallRoom,wall,wallState = self._SelectWallClass.GetSelectedModel()
		doorRoom,door = self._SelectModelClass.GetSelectedDoor()
		doorList = self._SelectModelClass.GetSelectedDoorList()
		windowRoom,window = self._SelectModelClass.GetSelectedWindow()
		windowList = self._SelectModelClass.GetSelectedWindowList()
		room = self._SelectModelClass.GetSelectedRoom()
		roomList = self._SelectModelClass.GetSelectedRoomList()
		log = self._SelectLogClass.GetSelectedModel()
		character = self._SelectModelClass.GetSelectedCharacter()
		characterList = self._SelectModelClass.GetSelectedCharacterList()
		itemList = self._SelectModelClass.GetSelectedModelList()
		pathCharacter,path,isItem = self._SelectPathClass.GetSelectedModel()
		setDrawingState = self._SetDrawingScaleButtonClass.GetSettingState()
		
		if setDrawingState:								#図面設定中
			pass
			
		elif itemNode != None:							#アイテム
			if self._FunctionState != 1 or self._PreItem != item:
				self.SetFunction(1)
				self.HideOtherFunctionButton(1)
			if self._FunctionState == 1 and self._PreItem != item:
				self._SetItemSizeButtonClass.Reset()
				self._SetItemTextureButtonClass.Reset()
				self._SetLightButtonClass.Reset()
				
		elif wall != None and wallState == 1:			#壁
			if self._FunctionState != 2:
				self.SetFunction(2)
				self.HideOtherFunctionButton(2)
				
		elif wall != None and wallState == 2:			#柱
			if self._FunctionState != 3:
				self.SetFunction(3)
				self.HideOtherFunctionButton(3)
				
		elif door != None and itemList == [] and characterList == [] and roomList == [] and windowList == []:			#ドア
			if self._FunctionState != 4:
				self.SetFunction(4)
				self.HideOtherFunctionButton(4)
				
		elif window != None and itemList == [] and characterList == [] and roomList == [] and doorList == []:			#窓
			if self._FunctionState != 5:
				self.SetFunction(5)
				self.HideOtherFunctionButton(5)
			if self._FunctionState == 5 and self._PreWindow != window:
				self._SetWindowHeightButtonClass.Reset()
				
		elif room != None and roomList != []:			#部屋
			if self._FunctionState != 6:
				self.SetFunction(6)
				self.HideOtherFunctionButton(6)
			if self._FunctionState == 6 and self._PreRoom != room:
				self._SetWallHeightButtonClass.Reset()
				self._SetFloorTextureButtonClass.Reset()
				self._SetWallTextureButtonClass.Reset()
				self._SetCeilingTextureButtonClass.Reset()
				
		elif log != None:								#ログ
			if self._FunctionState != 7:
				self.SetFunction(7)
				self.HideOtherFunctionButton(7)
				
		elif character != None and len(characterList) == 1 and itemList == [] and roomList == [] and doorList == [] and windowList == []:
			if self._FunctionState != 8:				#キャラクター
				self.SetFunction(8)
				self.HideOtherFunctionButton(8)
				
		elif pathCharacter != None and path != None:	#パス
			if self._FunctionState != 9:
				self.SetFunction(9)
				self.HideOtherFunctionButton(9)
				
		else:
			if self._FunctionState != 0:				#基本
				self.SetFunction(0)
				self.HideOtherFunctionButton(0)
				
		#ロールオーバー
		self.Rollover(self._OverIconState)
		
		if self._InputClass.GetMouseClickState():
			if self._FunctionState == 0 and self.GetOverState() == False:
				function = self._FunctionClassList[self._FunctionState]
				function.Click(self._OverIconState)
				
		self._PreItem = item
		self._PreWindow = window
		self._PreRoom = room
		self._PreLog = log
		
		#ショートカットキーによる操作（削除、コピー）
		self._ShortcutKeyControlClass.Update(self._FunctionState)
		
	#リセット処理
	def Reset(self):
		self.SetVisible(True)
		self.SetFunction(0)
		self._ClickState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		for icon in self._IconList:
			icon.visible(val)
		for text in self._TextList:
			text.visible(val)
		
	#アイコンの表示状態設定
	def SetIconVisible(self):
		self.SetFunction(self._FunctionState)
			
	#ファンクション設定
	def SetFunction(self,num):
		self._FunctionState = num
		
		#ウィンドウサイズを変更した際に表示可能なアイコンの数を確認
		windowSize  = viz.window.getSize()
		
		iconCount = 5
		for x in range(self._IconCount):
			scaleLimt = self._IconPositionList[x][0] + 120
			if windowSize[0] < scaleLimt:
				iconCount = x
				break
				
		functionName = functionButtonAreaFile.GetItemData('Function',str(self._FunctionState))
		self._IconTextList = []
		
		for x in range(self._IconCount):
			message = functionButtonAreaFile.GetItemData(functionName,str(x))
			
			#照明と音源は非表示
			if functionName == 'アイテム':
				item = self._SelectModelClass.GetSelectedModel()
				
				if self._ItemControlClass.GetSoundState(item):	#音響
					if message == 'コピー' or message == 'サイズ' or message == '画像':
						message = "None"
						
				if self._ItemControlClass.GetLightState(item):	#照明
					if message == 'コピー':
						message = "照明"
					elif message == 'サイズ' or message == '画像':
						message = "None"
						
			"""
			#ダミーボックスのみサイズと画像を表示
			if functionName == 'アイテム':
				item = self._SelectModelClass.GetSelectedModel()
				itemName = self._ItemControlClass.GetFileName(item)
				
				if itemName[-13:] != 'DummyBox.osgb':
					if message == 'サイズ' or message == '画像':
						message = "None"
					
			#オペ室のみ壁の接合点の追加は非表示
			if functionName == '壁':
				room,wall,state = self._SelectWallClass.GetSelectedModel()
				isOpRoom = self._RoomControlClass.GetOpRoomState(room)
				
				if isOpRoom:
					num = x+1
					if num == self._IconCount:
						message = "None"
					else:
						message = functionButtonAreaFile.GetItemData(functionName,str(num))
				
			#オペ室のみ柱の削除は非表示
			if functionName == '柱' and message == '削除':
				room,wall,state = self._SelectWallClass.GetSelectedModel()
				isOpRoom = self._RoomControlClass.GetOpRoomState(room)
				
				if isOpRoom:
					message = "None"
				
			#キャラクターのパス追加、再生中は非表示
			if functionName == 'キャラクター':
				if self._AddPathClass.GetAddState() or self._PlayPathClass.GetPlayState():
					message = "None"
			"""
				
			if message == "None":
				message = ""
				
			icom = self._IconList[x]
			text = self._TextList[x]
			
			text.message(message)
			
			if message != "" and x < iconCount and self._VisibleState:
				icom.visible(viz.ON)
				text.visible(viz.ON)
			else:
				icom.visible(viz.OFF)
				text.visible(viz.OFF)
				
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		if state == False:
			itemButton = self._SetItemButtonClass.GetOverState()
			roomButton = self._SetRoomButtonClass.GetOverState()
			enviButton = self._SetEnvironmentButtonClass.GetOverState()
			heightButton = self._SetWallHeightButtonClass.GetOverState()
			floorTexButton = self._SetFloorTextureButtonClass.GetOverState()
			wallTexButton = self._SetWallTextureButtonClass.GetOverState()
			ceilingTexButton = self._SetCeilingTextureButtonClass.GetOverState()
			windowHeightButton = self._SetWindowHeightButtonClass.GetOverState()
			itemSizeButton = self._SetItemSizeButtonClass.GetOverState()
			itemTextureButton = self._SetItemTextureButtonClass.GetOverState()
			logButton = self._SetLogButtonClass.GetOverState()
			drawingButton = self._SetDrawingButtonClass.GetOverState()
			pathButton = self._SetPathButtonClass.GetOverState()
			lightButton = self._SetLightButtonClass.GetOverState()
			
			if itemButton or roomButton or enviButton or heightButton or floorTexButton or wallTexButton or ceilingTexButton or windowHeightButton or itemSizeButton or itemTextureButton or logButton or drawingButton or pathButton or lightButton:
				state = True
		
		return state
		
	#他ファンクションのボタンを非表示
	def HideOtherFunctionButton(self,num):
		for x in range(len(self._FunctionClassList)):
			if x != num:
				function = self._FunctionClassList[x]
				function.Hide()
				
	#ロールオーバー処理
	def Rollover(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [(windowSize[0]-26)/2+13+30,windowSize[1]-36]
		self._FrameScale = [windowSize[0]-26-58,46]
		
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._IconPosition = [118,windowSize[1]-36]
		
		for x in range(self._IconCount):
			self._IconPositionList[x][1] = self._IconPosition[1]
			
			self.SetIconPosition(self._IconList[x],self._IconPositionList[x],self._IconScale)
			self.SetIconPosition(self._TextList[x],self._IconPositionList[x],self._TextScale)
			
		self.SetFunction(self._FunctionState)
		