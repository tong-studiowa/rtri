﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

#モジュールのインポート
import viz

import Object2d

from win32api import OutputDebugString

#---------------------------------------------------------
class ChangeMode(Object2d.Object2d):
	"""
	モード（レイアウター/パスシミュレータ）切り替え制御のクラス定義
	"""
	#---------------------------------------------------------
	def __init__(self, callBackFunc):
		"""
		コンストラクタ
		"""
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ChangeView] : Init ChangeView')
		
		self._InputClass = None
		self._ManipulatorClass = None
		
		self._ModeState = 0
		
		self._ClickState = False
		self._OverState = False
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._Position = [36,windowSize[1]-36]
		self._Scale = [46,46]
		self._Frame = self.CreateFrame(self._Position,self._Scale,order=2)
		
		#アイコン追加
		texturePath = viz.res.getPublishedPath('Resource\\Interface\\Mode_PathSim.jpg')
		self._Icon = self.CreateTexIcon(texturePath,self._Position,self._Scale,order=3)
		
		# コールバック関数を追加
		self.__mCallBackFunc = callBackFunc

	#---------------------------------------------------------
	def AddClass(self,inputClass,manipulatorClass):
		"""
		使用するクラスの追加
		"""
		self._InputClass = inputClass
		self._ManipulatorClass = manipulatorClass
		
		self.Reset()
		
	#---------------------------------------------------------
	def Update(self):
		"""
		毎フレーム呼び出されるための関数
		"""
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ChangeView] : Start ChangeView Update')
		
		self._ClickState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
		
		if self._OverState:
			if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False\
			and self._InputClass.GetShiftKeyState() ==False and self._ManipulatorClass.GetState() == False:
				self._ClickState = True
				self.__mCallBackFunc()
				
		#ロールオーバー
		self.Rollover(self._OverState)
		
	#---------------------------------------------------------
	def ChangeMode(self):
		"""
		モードを切り替え
		"""
		if self._ModeState == 0:
			self._ModeState = 1
		else:
			self._ModeState = 0
			
	#---------------------------------------------------------
	def Reset(self):
		"""
		リセット処理
		"""
		self._ClickState = False
		
	#---------------------------------------------------------
	def SetModeState(self,state):
		"""
		モードの状態を設定
		"""
		self._ModeState = state
		
	#---------------------------------------------------------
	def GetModeState(self):
		"""
		モードの状態を取得
		"""
		state = self._ModeState
		return state
		
	#---------------------------------------------------------
	def SetVisible(self, state):
		"""
		表示状態設定
		"""
		if state:
			self._Frame.visible(viz.ON)
			self._Icon.visible(viz.ON)
		else:
			self._Frame.visible(viz.OFF)
			self._Icon.visible(viz.OFF)
			
	#---------------------------------------------------------
	def GetOverState(self):
		"""
		重なり状態の取得
		"""
		state = self._OverState
		return state
		
	#---------------------------------------------------------
	def Rollover(self,state):
		"""
		ロールオーバー処理
		"""
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#---------------------------------------------------------
	def SetAspectRatio(self):
		"""
		画角を設定
		"""
		windowSize  = viz.window.getSize()
		
		self._Position = [36,windowSize[1]-36]
		
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Icon,self._Position,self._Scale)
		