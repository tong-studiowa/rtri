﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁の線制御のクラス定義
class WallLine(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,height,visibleList,isOpeRoom):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallLine] : Init WallLine')
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._Height = height
		self._VisibleList = visibleList
		
		self._Width = 0.15
		self._ModelList = []
		
		self._Color = viz.BLACK
		self._HideColor = viz.GRAY
		self._Alpha = 0.75
		
		self._PositionOffset = [0.0,0.0,0.0]
		
		self._IsOpRoom = isOpeRoom
		
		self.Create()
		
		#壁のラインを表示
		self.SetVisible(True)
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallLine] : Start WallLine Update')
		pass
		
	#壁モデルの作成
	def Create(self):
		self._ModelList = []
		
		for x in range(len(self._PointList)):
			point0,point1 = self.GetPoint(x)
			pointDistance = vizmat.Distance(point0,point1)
			
			#板ポリゴン作成
			model = vizshape.addPlane([1.0,1.0])
			model.setScale([pointDistance,1.0,self._Width])
			model.disable(viz.LIGHTING)
			
			color = self._Color
			if self._VisibleList[x] == False:
				color = self._HideColor
			model.color(color)
			model.alpha(self._Alpha)
			
			self.SetModelPosition(model,point0,point1)
			self.SetPickable(False)
			
			self._ModelList.append(model)
		
	#壁のラインモデルの削除
	def Delete(self):
		for model in self._ModelList:
			model.remove()	#モデルを削除
			del model
				
		self._ModelList = []
		
	#壁のラインの表示状態を設定
	def SetVisible(self,state):
		visibleState = viz.OFF
		if state == True:
			visibleState = viz.ON
		else:
			visibleState = viz.OFF
			
		for line in self._ModelList:
			line.visible(visibleState)
			
		self.ReCreate()
		
	#壁のハイライト表示
	def SetHighlight(self,num,state):
		color = self._Color
		if state == True:
			color = viz.RED
		else:
			if self._VisibleList[num]:
				color = self._Color
			else:
				color = self._HideColor

		line =  self._ModelList[num]
		line.color(color)
		
		self.ReCreate()
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x] == model:
				num =  x
		
		return num
		
	#モデルを取得
	def GetModel(self,num):
		model = self._ModelList[num] 
		
		return model
		
	#選択可能状態の設定
	def SetPickable(self,state):
		for model in self._ModelList:
			if state == True:
				model.enable(viz.PICKING)
			elif state == False:
				model.disable(viz.PICKING)
				
	#位置設定
	def SetPosition(self,num,iPosition):
		model = self._ModelList[num]
		model.setPosition([0.0,0.0,0.0])
			
		if model != None and iPosition != None:
			position = iPosition
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = self._Height+0.1
			position[2] = position[2] + self._PositionOffset[2]
			
			model.setPosition(position)
			
			num,num2 = self.GetPointNum(num)
			
			point0Temp1 = model.getVertex(1,viz.ABS_GLOBAL)
			point0Temp2 = model.getVertex(2,viz.ABS_GLOBAL)
			point0 = [0.0,0.0]
			point0[0] = (point0Temp1[0]+point0Temp2[0])/2
			point0[1] = (point0Temp1[2]+point0Temp2[2])/2
			
			point1Temp1 = model.getVertex(0,viz.ABS_GLOBAL)
			point1Temp2 = model.getVertex(3,viz.ABS_GLOBAL)
			point1 = [0.0,0.0]
			point1[0] = (point1Temp1[0]+point1Temp2[0])/2
			point1[1] = (point1Temp1[2]+point1Temp2[2])/2
			
			self.SetPoint(num,point0,point1)
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,clickPosition):
		point0,point1 = self.GetPoint(num)
		
		if clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = point1[0] - clickPosition[0]
			self._PositionOffset[1] = self._Height+0.1
			self._PositionOffset[2] = point1[1] - clickPosition[2]
	
	#位置取得
	def GetPosition(self,num):
		position = [0.0,0.0,0.0]
		point0,point1 = self.GetPoint(num)
		
		position[0] = point1[0]
		position[1] = self._Height+0.1
		position[2] = point1[1]
		
		return position
		
	#ポイントを取得
	def GetPoint(self,num):
		num,num2 = self.GetPointNum(num)
		point0 = self._PointList[num]
		point1 = self._PointList[num2]
		
		return point0,point1
		
	#ポイント番号を取得
	def GetPointNum(self,num):
		num2 = num + 1
		if num2 == len(self._PointList):
			num2 = 0
		
		return num,num2
		
	#ポイントを設定
	def SetPoint(self,num,point0,point1):
		num,num2 = self.GetPointNum(num)
		
		self._PointList[num] = point0
		self._PointList[num2] = point1
		
	#フロアのポイントリストを取得
	def GetFloorPointList(self):
		list = self._PointList
		
		return list
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._PointList)):
			point0,point1 = self.GetPoint(x)
			pointDistance = vizmat.Distance(point0,point1)
			
			model = self._ModelList[x]
			
			model.setPosition(0,0,0)
			model.setEuler(0,0,0)
			
			model.setScale([pointDistance,1.0,self._Width])
			self.SetModelPosition(model,point0,point1)

	#位置再設定
	def SetModelPosition(self,model,point0,point1):
		#位置
		position = [0.0,0.0,0.0]
		position[0] = (point0[0] + point1[0]) / 2 + self._FloorPosition[0]
		position[1] = 80.0
		position[2] = (point0[1] + point1[1]) / 2 + self._FloorPosition[2]
		
		#選択中は上にずらす
		color = model.getColor()
		if color[0] == 1.0 and color[1] == 0.0 and color[2] == 0.0:
			position[1] = 82.0
			
		if self._IsOpRoom == 1:
			position[1] = position[1] - 1.0
		elif self._IsOpRoom == 2:
			position[1] = position[1] - 0.5
			
		model.setPosition(position)
		
		#角度
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		model.setEuler(wallAngle-90.0,0.0,0.0)
		
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
	
	#壁のポイントを再設定
	def SetPointList(self,pointList):
		self._PointList = pointList
		
		self.Delete()
		self.Create()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		
	#モデルの表示状態のリストを設定
	def SetVisibleList(self,visibleList):
		self._VisibleList = visibleList
		