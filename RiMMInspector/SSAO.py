"""
http://www.opengl.org/discussion_boards/ubbthreads.php?ubb=showflat&Number=236805&fpart=1
"""

import viz
import vizpp

class ScreenSpaceAmbientOcclusion(vizpp.PostProcessEffect):

	def _setupEffect(self):
		"""Create resources for effect"""
		frag="""
				uniform sampler2D vizpp_InputDepthTex;
				uniform sampler2D vizpp_InputTex;
				uniform ivec2 viz_ScreenSize;

				uniform vec2 camerarange;

				float readDepth( in vec2 coord ) {
					return (2.0 * camerarange.x) / (camerarange.y + camerarange.x - texture2D( vizpp_InputDepthTex, coord ).x * (camerarange.y - camerarange.x));
				}

				float compareDepths( in float depth1, in float depth2 ) {
					float aoCap = 1.0;
					float aoMultiplier=500.0;	// @
					float depthTolerance=0.00;
					float aorange = 10.0;		// @ units in space the AO effect extends to (this gets divided by the camera far range
					float diff = sqrt( clamp(1.0-(depth1-depth2) / (aorange/(camerarange.y-camerarange.x)),0.0,1.0) );
					float ao = min(aoCap,max(0.0,depth1-depth2-depthTolerance) * aoMultiplier) * diff;
					return ao;
				}

				void main(void)
				{
					vec2 texCoord = gl_TexCoord[0].st;

					float depth = readDepth( texCoord );
					float d;

					float pw = 1.0 / (float)viz_ScreenSize.x;
					float ph = 1.0 / (float)viz_ScreenSize.y;

					float aoCap = 1.0;

					float ao = 0.0;

					float aoMultiplier=10000.0;

					float depthTolerance = 0.001;

					float aoscale=1.0;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					pw*=2.0;
					ph*=2.0;
					aoMultiplier/=2.0;
					aoscale*=1.2;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					pw*=2.0;
					ph*=2.0;
					aoMultiplier/=2.0;
					aoscale*=1.2;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					pw*=2.0;
					ph*=2.0;
					aoMultiplier/=2.0;
					aoscale*=1.2;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y+ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x+pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					d=readDepth( vec2(texCoord.x-pw,texCoord.y-ph));
					ao+=compareDepths(depth,d)/aoscale;

					ao/=16.0;

					gl_FragColor = vec4(1.0-ao) * texture2D(vizpp_InputTex,texCoord);
				}
				"""
		self._shader = viz.addShader(frag = frag)

		#self._shader.attach(viz.addUniformFloat('camerarange',[0.1,2000]))
		self._shader.attach(viz.addUniformFloat('camerarange',[0.1,100]))

	def _applyEffect(self,data):
		"""Apply effect"""
		data.overlay.apply(self._shader)

#viz.clip(1,100)

ssao = ScreenSpaceAmbientOcclusion()
vizpp.addEffect(ssao)
#vizact.onkeydown(' ',ssao.setEnabled,viz.TOGGLE)
