﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput
import vizmat

import Interface
import Log

import os
import sys
import codecs
import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログ制御のクラス定義
class LogControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogControl] : Init LogControl')
		
		self._SelectLogClass = None
		self._UndoClass = None
		
		self._LogIniFileName = ""
		self._LogIniFile = None
		
		self._SaveIniFile = None
		
		self._LoglList = []
		
		self._Line = None
		self._Time = 60		#初期値は60秒
		self._Height = 1.4
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogControl] : Start LogControl Update')
		pass
		
	#使用するクラスの追加
	def AddClass(self,selectLogClass,itemControlClass,roomControlClass,undoClass):
		self._SelectLogClass = selectLogClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._UndoClass = undoClass
		
	#INIファイルを基にアイテムを読み込む
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogControl] : Load')
		
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		dispnum = iniFile.GetItemData('File','dispnum')
		
		if dispnum == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはログ定義ファイルではありません。', title='RiMMInspector')
			del iniFile
		
		else:
			#全ログを削除
			self.DeleteAll()
			
			self._LogIniFileName = fileName
			
			#読み込み用iniファイルの作成
			self._LogIniFile = iniFile
			
			dispnum = self._LogIniFile.GetItemData('File','dispnum')
			self._Dispnum = int(dispnum)
			
			self._Room1 = self._LogIniFile.GetItemData('File','room1')
			self._Item1 = self._LogIniFile.GetItemData('File','item1')
			
			self._Room2 = None
			self._Item2 = None
			
			if self._Dispnum == 2:
				self._Room2 = self._LogIniFile.GetItemData('File','room2')
				self._Item2 = self._LogIniFile.GetItemData('File','item2')
			
			self._LogFile = self._LogIniFile.GetItemData('Log','filepath')
			
			#ファイルが存在するか確認
			errorState = False
			if os.path.exists(self._LogFile) == False:
				vizinput.message('ファイル読み込みエラー!\n指定のLOGファイルが見つかりません。', title='RiMMInspector')
				errorState = True
				
			#読み込み用iniファイルの作成
			logmIniFile = LoadIniFile.LoadIniFileClass()
			logmIniFile.LoadIniFile(self._LogFile+'m')
			
			#編集用ログファイルがあるか確認
			test = logmIniFile.GetItemData('Log','Time')
				
			#編集用ログファイルの読み込み
			if test != None:
				#時間を設定
				time = logmIniFile.GetItemData('Log','Time')
				self._Time = int(time)
				
				#視点の高さを設定
				height = logmIniFile.GetItemData('Log','Height')
				self._Height = float(height)
		
				positions = []
				orientations = []
				for x in range(1000):
					logData = logmIniFile.GetItemData('Log',str(x))
					
					if logData == None:
						break
					
					else:
						pos = [0.0,0.0,0.0]
						pos[0] = float(logData.split(',')[0])
						pos[1] = float(logData.split(',')[1])
						pos[2] = float(logData.split(',')[2])
						
						positions.append(pos)
						
						ori = float(logData.split(',')[3])
						orientations.append(ori)
					
				#ログの作成
				for x in range(len(positions)):
					pos = positions[x]
					ori = orientations[x]
					
					logNum = self.Add()
					self.SetPosition(logNum,pos)
					self.SetOrientation(logNum,ori)

			#ログファイルの読み込み（編集用ログファイルが無い場合）
			else:
				if errorState == False:
					logFile = codecs.open(self._LogFile, 'r', 'shift-jis')
					
					positions = []
					orientations = []
					for logData in logFile:
						pos = [0.0,0.0,0.0]
						pos[0] = float(logData.split(',')[1])
						pos[1] = float(logData.split(',')[2])
						pos[2] = float(logData.split(',')[3])
						positions.append(pos)
						
						ori = float(logData.split(',')[4])
						orientations.append(ori)
						
					logFile.close()
					
					#時間を設定
					self._Time = len(positions) - 1
					
					#視点の高さを設定
					self._Height = float(logData.split(',')[10])
					
					#ログの作成
					for x in range(len(positions)):
						pos = positions[x]
						ori = orientations[x]
						if ori < 0:
							ori = ori + 360
						
						logNum = self.Add()
						self.SetPosition(logNum,pos)
						self.SetOrientation(logNum,ori)
					
			#アイテムを読み込み
			if self._Room1 != "":
				self._RoomControlClass.Load(self._Room1)
			else:
				vizinput.message('ファイル読み込みエラー!\n間取り定義ファイルが指定されていません。', title='RiMMInspector')
			
			#アイテムを読み込み
			if self._Item1 != "":
				self._ItemControlClass.Load(self._Item1)
			else:
				vizinput.message('ファイル読み込みエラー!\nアイテム定義ファイルが指定されていません。', title='RiMMInspector')
			
			#Undoの情報をクリア
			self._UndoClass.ClearList()
		
	#INIファイルにアイテムを保存する
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogControl] : Save')
		
		time = self.GetTime()
		roomPath = self._RoomControlClass.GetIniFileName()
		itemPath = self._ItemControlClass.GetIniFileName()
		logPath = fileName[:-3]+'log'
		
		if roomPath == '' and itemPath == '':
			vizinput.message('ファイル保存エラー!\n間取りとアイテム定義ファイルが設定されていません。', title='RiMMInspector')
			
		elif roomPath == '':
			vizinput.message('ファイル保存エラー!\n間取り定義ファイルが設定されていません。', title='RiMMInspector')
		
		elif itemPath == '':
			vizinput.message('ファイル保存エラー!\nアイテム定義ファイルが指定されていません。', title='RiMMInspector')
		
		else:
			#保存用iniファイルの作成
			self._SaveIniFile = LoadIniFile.LoadIniFileClass()
			self._SaveIniFile.ResetIniData()
			
			self._SaveIniFile.AddSection('File')
			self._SaveIniFile.AddItem('File','dispnum','1')
			self._SaveIniFile.AddItem('File','room1',roomPath)
			self._SaveIniFile.AddItem('File','item1',itemPath)
			self._SaveIniFile.AddItem('File','room2','')
			self._SaveIniFile.AddItem('File','item2','')
			
			self._SaveIniFile.AddSection('Log')
			self._SaveIniFile.AddItem('Log','filepath',logPath)
			
			self._SaveIniFile.WriteIniData(fileName)
			
			#保存用logファイルの作成
			logData = self.CreateLogData()
			logFile = codecs.open(logPath, 'w', 'shift-jis')
			logFile.write(logData)
			logFile.close()
			
	#INIファイルにアイテムを保存する（2画面）
	def Save2(self,fileName,room2Name,item2Name):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogControl] : Save2')
		
		time = self.GetTime()
		roomPath = self._RoomControlClass.GetIniFileName()
		itemPath = self._ItemControlClass.GetIniFileName()
		roomPath2 = room2Name
		itemPath2 = item2Name
		logPath = fileName[:-3]+'log'
		
		if roomPath == '' and itemPath == '':
			vizinput.message('ファイル保存エラー!\n間取りとアイテム定義ファイルが設定されていません。', title='RiMMInspector')
			
		elif roomPath == '':
			vizinput.message('ファイル保存エラー!\n間取り定義ファイルが設定されていません。', title='RiMMInspector')
		
		elif itemPath == '':
			vizinput.message('ファイル保存エラー!\nアイテム定義ファイルが指定されていません。', title='RiMMInspector')
		
		else:
			#保存用iniファイルの作成
			self._SaveIniFile = LoadIniFile.LoadIniFileClass()
			self._SaveIniFile.ResetIniData()
			
			self._SaveIniFile.AddSection('File')
			self._SaveIniFile.AddItem('File','dispnum','2')
			self._SaveIniFile.AddItem('File','room1',roomPath)
			self._SaveIniFile.AddItem('File','item1',itemPath)
			self._SaveIniFile.AddItem('File','room2',roomPath2)
			self._SaveIniFile.AddItem('File','item2',itemPath2)
			
			self._SaveIniFile.AddSection('Log')
			self._SaveIniFile.AddItem('Log','filepath',logPath)
			
			self._SaveIniFile.WriteIniData(fileName)
			
			#保存用logファイルの作成
			logData = self.CreateLogData()
			logFile = codecs.open(logPath, 'w', 'shift-jis')
			logFile.write(logData)
			logFile.close()
			
		
	#出力用ログデータを作成
	def CreateLogData(self):
		logDataTemp = []
		count = self._Time
		logCount = self.GetLogCount()
		
		if count == logCount-1 or count < logCount-1:
			for x in range(logCount):
				log = self._LoglList[x]
				logDataTemp.append([log.GetPosition(),log.GetOrientation()])
			
		else:
			distanceList = []
			allDistance = 0.0
			allDistanceList = []
			
			distanceList.append(0.0)
			allDistanceList.append(0.0)
			for x in range(logCount-1):
				log0 = self._LoglList[x]
				logPos0 = log0.GetPosition()
				logPos0[1] = 0
				
				log1 = self._LoglList[x+1]
				logPos1 = log1.GetPosition()
				logPos1[1] = 0
				
				distance = vizmat.Distance(logPos0,logPos1)
				
				distanceList.append(distance)
				allDistance = allDistance + distance
				allDistanceList.append(allDistance)
				
			deltaDistance = allDistance / count
			
			dist = 0.0
			preDist = 0.0
			for x in range(count):
				
				dist = dist + deltaDistance
						
				#最初の点を設定
				if x == 0:
					log = self._LoglList[x]
					logDataTemp.append([log.GetPosition(),log.GetOrientation()])
			
				#最後の点を設定
				elif x == count-1:
					lastNum = len(self._LoglList)-1
					log = self._LoglList[lastNum]
					logDataTemp.append([log.GetPosition(),log.GetOrientation()])
					
				elif x < count-1:
					for y in range(len(allDistanceList)):
						p = allDistanceList[y]
						if dist < p or dist == p:
							
							#割合を確認
							distMin = allDistanceList[y-1]
							distMax = allDistanceList[y]
							
							logMin = self._LoglList[y-1]
							logMax = self._LoglList[y]
							
							perc = (dist - distMin) / (distMax - distMin)
							
							#位置設定
							logPosMin = logMin.GetPosition()
							logPosMax = logMax.GetPosition()
							
							pos = [0,0,0]
							pos[0] = logPosMin[0]+(logPosMax[0]-logPosMin[0])*perc
							pos[1] = logPosMax[1]
							pos[2] = logPosMin[2]+(logPosMax[2]-logPosMin[2])*perc
							
							#角度設定
							logOriMin = logMin.GetOrientation()
							logOriMax = logMax.GetOrientation()
							
							if logOriMin > 269 and logOriMax == 0:
								logOriMin = logOriMin - 360.0
								
							elif logOriMin == 0 and logOriMax > 269:
								logOriMax = logOriMax - 360.0
							
							elif logOriMin > 269 and logOriMax < 91:
								logOriMin = logOriMin - 360.0
								
							elif logOriMin < 91 and logOriMax > 269:
								logOriMax = logOriMax - 360.0
							
							Ori = 0.0
							Ori = logOriMin+(logOriMax-logOriMin)*perc
							
							if Ori > 180:
								Ori = Ori - 360.0
							
							logDataTemp.append([pos,Ori])
							
							#点の位置に移動
							p2 = allDistanceList[y-1]
							if dist > p2 and preDist < p2:
								num = len(logDataTemp) - 1
								if dist - p2 > p2 - preDist:
									num = len(logDataTemp) - 2
								
								log = self._LoglList[y-1]
								logDataTemp[num] = [log.GetPosition(),log.GetOrientation()]
							
							break
				
				preDist = dist
		
		#リストを作成
		logData = ""
		for x in range(len(logDataTemp)):
			pos = logDataTemp[x][0]
			Ori = logDataTemp[x][1]
			data = str(x)+','+str(pos[0])+','+str(0.0)+','+str(pos[2])+','+str(Ori)+','+str(0.0)+','+str(0.0)+','+str(Ori)+','+str(0.0)+','+str(0.0)+','+str(pos[1])+'\n'
			logData = logData + data
			
		return logData
		
	#追加
	def Add(self):
		log = Log.Log()
		
		self._LoglList.append(log)
		
		num = len(self._LoglList) - 1
		
		log.SetId(num)
		self.SetLine()
		
		return num
	
	#削除
	def Delete(self,num):
		deleteModel = self._LoglList[num]
		
		#モデルリストを更新
		newModelLiset = []
		
		for x in range(len(self._LoglList)):
			log = self._LoglList[x]
			if log != deleteModel:
				
				newModelLiset.append(log)
						
		self._LoglList = newModelLiset
		
		#モデルを削除
		if deleteModel != None:
			deleteModel.Delete()
			del deleteModel
		
		#IDを再設定
		for x in range(len(self._LoglList)):
			log = self._LoglList[x]
			log.SetId(num)
		
		self.SetLine()
		
	#全ログ削除
	def DeleteAll(self):
		self._SelectLogClass.UnSelect()
		
		logCount = self.GetLogCount()
		for x in range(logCount):
			self.Delete(0)
			
		self.SetLine()
		
		self._Time = 60		#初期値は60秒
		
	#位置設定
	def SetPosition(self,num,position):
		log = self._LoglList[num]
		
		if log != None and position != None:	
			log.SetPosition(position)
	
		self.SetLine()
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		log = self._LoglList[num]
		
		if log != None and position != None:	
			log.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		log = self._LoglList[num]
		position = [0,0,0]
		
		if log != None:
			position = log.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		log = self._LoglList[num]
		
		if log != None and orientation != None:	
			log.SetOrientation(orientation)
	
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self,num):
		log = self._LoglList[num]
		
		if log != None:	
			log.ResetOrientationSnap()
		
	#角度取得
	def GetOrientation(self,num):
		log = self._LoglList[num]
		orientation = 0.0
		
		if log != None:
			orientation = log.GetOrientation()
		
		return orientation
		
	#角度設定(INIファイル用)
	def SetAxisAngle(self,num,axisAngle):
		log = self._LoglList[num]
		
		if log != None and axisAngle != None:	
			log.SetAxisAngle(axisAngle)
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self,num):
		log = self._LoglList[num]
		axisAngle = [0,0,0,0]
		
		if log != None:
			axisAngle = log.GetAxisAngle()
		
		return axisAngle
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._LoglList[num]
		model.SetHighlight(state)
		
	#モデルから番号を取得
	def GetLogNum(self,model):
		num = None
		
		for x in range(len(self._LoglList)):
			if self._LoglList[x]._Model == model:
				num =  x
		
		return num
		
	#全ログの選択可能状態を設定
	def SetPickableForAllLog(self,state):
		for log in self._LoglList:
			log.SetPickable(state)
			
	#ログ取得
	def GetLog(self,num):
		log = self._LoglList[num]
		
		return log
		
	#ログ数を取得
	def GetLogCount(self):
		count = len(self._LoglList)
		return count
		
	#ライン設定
	def SetLine(self):
		if self._Line != None:
			self._Line.remove()
			self._Line = None
		
		if self.GetLogCount() > 1:
			viz.startLayer(viz.LINE_STRIP) 
			for log in self._LoglList:
				logPos = log.GetPosition()
				pos = [0,0,0]
				pos[0] = logPos[0]
				pos[1] = logPos[1]
				pos[2] = logPos[2]
			
				viz.vertex(pos)
			
			line = viz.endLayer()
			line.disable(viz.LIGHTING)
			line.disable(viz.PICKING)
			line.color(viz.GREEN)
			line.lineWidth(5)			#線の太さ
			
			self._Line = line
		
	#時間設定
	def SetTime(self,time):
		self._Time = time
		
	#時間取得
	def GetTime(self):
		time = self._Time
		
		return time
		