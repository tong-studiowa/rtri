﻿# coding: utf-8

import viz
import vizact
import viztask
import vizmat
import Interface
from win32api import OutputDebugString

DebugMode = False

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ジョイスティック関係
#dinput = viz.add('DirectInput.dle')
joystick = None
"""
if dinput.getJoystickDevices() != []:
	joystick = dinput.addJoystick()
	joystick.setDeadZone(0.2)
"""

# - - - - - - - - - - - - - - - - - - - - - - - - -
#入出力制御のクラス定義
class Input(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Input] : Init Input')
		
		#パラメーターの初期化
		self._OrthoViewWindow = None
		self._Is3d = False
		
		self._IsButtonDown_Left		= False
		self._PreIsButtonDown_Left	= False
		self._IsButtonDown_Right	= False
		self._IsButtonDown_Middle	= False
		self._IsWheelMove			= 0
		self._IsWheelMoveTemp		= 0
		self._MousePosition			= [0.0,0.0]
		self._PreMousePosition		= [0.0,0.0]
		self._MouseDisplacement		= [0.0,0.0]
		self._MouseClickPosition	= [0.0,0.0]
		
		self._ClickState		= False
		self._DragState			= False
		self._PreDragState		= False
		self._StartDragState	= False
		self._FinDragState		= False
		self._PressState		= False
		self._PressCount		= 0
		
		self._ShiftKeyState		= False
		self._CtrlKeyState		= False
		self._AltKeyState		= False
		self._SpaceKeyState		= False
		
		#キーボードでの視点移動
		self._MoveForwardKeyState	= False
		self._MoveBackwardKeyState	= False
		self._MoveLeftKeyState		= False
		self._MoveRightKeyState		= False
		self._MoveUpKeyState		= False
		self._MoveDownKeyState		= False
		
		self._RotateUpKeyState		= False
		self._RotateDownKeyState	= False
		self._RotateLeftKeyState	= False
		self._RotateRightKeyState	= False
		
		self._ResetViewKeyState		= False
		
		#ジョイスティックでの視点移動
		self._MoveForwardJoyState	= 0.0
		self._MoveBackwardJoyState	= 0.0
		self._MoveLeftJoyState		= 0.0
		self._MoveRightJoyState		= 0.0
		self._MoveUpJoyState		= False
		self._MoveDownJoyState		= False
		
		self._RotateUpJoyState		= 0.0
		self._RotateDownJoyState	= 0.0
		self._RotateLeftJoyState	= 0.0
		self._RotateRightJoyState	= 0.0
		
		self._ResetViewJoyState		= False
		
		#通路幅変更機能
		self._OffsetUpKeyState		= False
		self._OffsetDownKeyState	= False
		self._OffsetLeftKeyState	= False
		self._OffsetRightKeyState	= False
		
		#ショートカットの処理開始
		self._PKeyState		= False
		self._QKeyState		= False
		self._TabKeyState	= False
		self._DelKeyState	= False
		self._CKeyState		= False
		self._VKeyState		= False
		self._XKeyState		= False
		self._ZKeyState		= False
		self._YKeyState		= False
		self._HKeyState		= False
		
		self.__mOKeyState		= False
		self.__mPreOkeyState 	= False
		self.__mOKeyDownState 	= False
		self.__mOKeyUpState		= False
		
		self._EKeyState		= False
		
		self._1KeyState		= False
		self._2KeyState		= False
		self._3KeyState		= False
		self._4KeyState		= False
		self._5KeyState		= False
		self._6KeyState		= False
		self._7KeyState		= False
		self._8KeyState		= False
		self._9KeyState		= False
		self._0KeyState		= False
		
		self._MouseLeftState		= False
		self._PreMouseLeftState		= False
		self._MouseRightState		= False
		self._PreMouseRightState	= False
		self._MouseMiddleState		= False
		self._PreMouseMiddleState	= False
		
		#マウス入力の処理開始
		viztask.schedule(self.CheckMousePosition)				#マウスの位置取得
		viztask.schedule(self.CheckMouseWheelState)				#マウスホイールの状態取得
		
		#viz.callback(viz.MOUSEDOWN_EVENT,self.MouseDownEvent)	#マウスのボタンが押された
		#viz.callback(viz.MOUSEUP_EVENT,self.MouseUpEvent)		#マウスのボタンが離された
		#viz.callback(viz.MOUSEWHEEL_EVENT,self.MouseWheelEvent)	#マウスのホイールが操作された
		
		#キーボード入力の処理開始
		#viz.callback(viz.KEYDOWN_EVENT,self.OnKeyDown)
		#viz.callback(viz.KEYUP_EVENT,self.OnKeyUp)
		
		#ジョイスティック操作の処理開始
		self._mDeviceType = 1
		if joystick != None:
			viztask.schedule(self.CheckJoystick)				#ジョイスティック操作の状態取得
		
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString("[STUDIOWA][INFO][RiMMInspector][Input] : Start Input Update")
		
		#self.CheckMouseState()
		
		#クリックかドラッグかの確認
		self._ClickState = False
		self._StartDragState = False
		self._FinDragState = False
		
		#LeftButtonDown
		if self._IsButtonDown_Left == True and self._PreIsButtonDown_Left == False:
			self._PressState = True
			
			self._ClickState = False
			self._DragState = True
			
			self._MouseClickPosition[0] = self._MousePosition[0]
			self._MouseClickPosition[1] = self._MousePosition[1]
			
		#LeftButtonUp
		if self._IsButtonDown_Left == False and self._PreIsButtonDown_Left == True:
			if self._DragState:
				self._FinDragState = True
				
			self._ClickState = True
				
			self._PressCount = 0
			self._PressState = False
			self._DragState = False
			
		#LeftButtonHold
		if self._PressState == True:
			windowSize  = viz.window.getSize()
			distanceX = self._MousePosition[0]*windowSize[0]-self._PreMousePosition[0]*windowSize[0]
			distanceY = self._MousePosition[1]*windowSize[1]-self._PreMousePosition[1]*windowSize[1]
		
			if self._DragState == True and self._PreDragState == False:
				self._StartDragState = True
				
		self._PreIsButtonDown_Left = self._IsButtonDown_Left
		self._PreDragState = self._DragState
		
		#マウスの移動量の確認
		self._MouseDisplacement[0] = self._MousePosition[0] - self._PreMousePosition[0]
		self._MouseDisplacement[1] = self._MousePosition[1] - self._PreMousePosition[1]
		
		self._PreMousePosition[0] = self._MousePosition[0]
		self._PreMousePosition[1] = self._MousePosition[1]
		
		#print "mouseDisplacement = ",self._MouseDisplacement
		
		self.__mOKeyDownState = False
		self.__mOKeyUpState = False
		
		if (not self.__mPreOkeyState) and self.__mOKeyState:
			self.__mOKeyDownState = True
		elif self.__mPreOkeyState and (not self.__mOKeyState):
			self.__mOKeyUpState = True
		self.__mPreOkeyState = self.__mOKeyState
		
	#マウスのボタンが離された状態から押された状態に切り替わったタイミングで呼び出される関数	
	def MouseDownEvent(self,button):
		if button == viz.MOUSEBUTTON_LEFT:
			self._IsButtonDown_Left = True
		if button == viz.MOUSEBUTTON_RIGHT:
			self._IsButtonDown_Right = True
		if button == viz.MOUSEBUTTON_MIDDLE:
			self._IsButtonDown_Middle = True
	
	#マウスのボタンが押された状態から離された状態に切り替わったタイミングで呼び出される関数	
	def MouseUpEvent(self,button):
		if button == viz.MOUSEBUTTON_LEFT:
			self._IsButtonDown_Left = False
		if button == viz.MOUSEBUTTON_RIGHT:
			self._IsButtonDown_Right = False
		if button == viz.MOUSEBUTTON_MIDDLE:
			self._IsButtonDown_Middle = False
			
	#マウスのホイールが操作されたときに呼び出される関数
	def MouseWheelEvent(self,val):
		self._IsWheelMoveTemp = val
		
	#マウス位置を確認するための関数
	def CheckMousePosition(self):
		while True:
			x,y = viz.mouse.getPosition()	#マウス位置を取得
			self._MousePosition[0] = x
			self._MousePosition[1] = y
			
			yield viztask.waitFrame(1)
	
	#マウスホイールの状態を確認するための関数
	def CheckMouseWheelState(self):
		while True:
			self._IsWheelMove = self._IsWheelMoveTemp
			if self._IsWheelMoveTemp != 0:
				self._IsWheelMoveTemp = 0
				
			yield viztask.waitFrame(1)
			
	#マウス左ボタンの状態を取得するための関数
	def GetMouseLeftButtonState(self):
		mouseLeftButtonState = self._IsButtonDown_Left
		return mouseLeftButtonState
		
	#マウス左ボタンの押下状態を取得するための関数
	def GetMouseLeftButtonDownState(self):
		mouseLeftButtonDownState = False
		if self._PressCount == 1:
			 mouseLeftButtonDownState = True
		
		return mouseLeftButtonDownState
		
	#マウス右ボタンの状態を取得するための関数
	def GetMouseRightButtonState(self):
		mouseRightButtonState = self._IsButtonDown_Right
		return mouseRightButtonState
		
	#マウス中ボタンの状態を取得するための関数
	def GetMouseMiddleButtonState(self):
		mouseMiddleButtonState = self._IsButtonDown_Middle
		return mouseMiddleButtonState
		
	#マウスの左クリックの状態を取得するための関数
	def GetMouseClickState(self):
		mouseClickState = self._ClickState
		return mouseClickState
		
	#マウスの左ドラッグの状態を取得するための関数
	def GetMouseDragState(self):
		mouseDragState = self._DragState
		return mouseDragState
		
	#マウスの左ドラッグが開始したか確認するための関数
	def GetMouseStartDragState(self):
		mouseStartDragState = self._StartDragState
		return mouseStartDragState
		
	#マウスの左ドラッグが終了したかを確認するための関数
	def GetMouseFinDragState(self):
		mouseFinDragState = self._FinDragState
		return mouseFinDragState
		
	#マウス位置を取得するための関数
	def GetMousePosition(self):
		mousePosition = self._MousePosition
		return mousePosition
	
	#マウスの移動量を取得するための関数
	def GetMouseDisplacement(self):
		mouseDisplacement = self._MouseDisplacement
		return mouseDisplacement
		
	#マウスホイールの状態を取得するための関数
	def GetMouseWheelState(self):
		mouseWheel = self._IsWheelMove
		return mouseWheel
	
	#モデルを選択するための関数
	def PickModel(self,pos=[-1,-1]):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		object = None
		if self._Is3d:
			object = viz.pick(info=True,ignoreBackFace=True)
		else:
			object = self._OrthoViewWindow.pick(info=True,pos=pos,ignoreBackFace=True)
		
		if object.valid == True:
			state = True
			pickedModel = object.object
			pickingPosition = object.point
			pickedModelName = object.name
			
		return state,pickedModel,pickingPosition,pickedModelName

	def OnKeyDown(self,key): 
		if key == viz.KEY_SHIFT_L or key == viz.KEY_SHIFT_R:		#Shiftキー
			self._ShiftKeyState = True
		if key == viz.KEY_CONTROL_L or key == viz.KEY_CONTROL_R:	#Controlキー
			self._CtrlKeyState = True
		if key == viz.KEY_ALT_L or key == viz.KEY_ALT_R:			#Altキー
			self._AltKeyState = True
			
		if key == ' ':								#スペースキー
			self._SpaceKeyState = True
			
		if key == viz.KEY_UP:						#矢印上キー
			self._RotateUpKeyState		= True
		if key == viz.KEY_DOWN:						#矢印下キー
			self._RotateDownKeyState	= True
		if key == viz.KEY_LEFT:						#矢印左キー
			self._RotateLeftKeyState	= True
		if key == viz.KEY_RIGHT:					#矢印右キー
			self._RotateRightKeyState	= True
		if key == viz.KEY_PAGE_UP:					#PageUpキー
			self._MoveUpKeyState		= True
		if key == viz.KEY_PAGE_DOWN:				#PageDownキー
			self._MoveDownKeyState		= True
			
		if key == 'w':								#Wキー
			self._MoveForwardKeyState	= True
		if key == 's':								#Sキー
			self._MoveBackwardKeyState	= True
		if key == 'a':								#Aキー
			self._MoveLeftKeyState		= True
		if key == 'd':								#Dキー
			self._MoveRightKeyState		= True
			
		if key == viz.KEY_HOME:						#Homeキー
			self._ResetViewKeyState		= True
			
		if key == 't':								#Tキー
			self._OffsetUpKeyState		= True
		if key == 'g':								#Gキー
			self._OffsetDownKeyState	= True
		if key == 'f':								#Fキー
			self._OffsetLeftKeyState	= True
		if key == 'h':								#Hキー
			self._OffsetRightKeyState	= True
		
		if key == 'p':								#Pキー
			self._PKeyState				= True
		if key == 'q':								#Qキー
			self._QKeyState				= True
		if key == viz.KEY_TAB:						#Tabキー
			self._TabKeyState			= True
		if key == viz.KEY_DELETE:					#Delキー
			self._DelKeyState			= True
		if key == 'c':								#Cキー
			self._CKeyState				= True
		if key == 'v':								#Vキー
			self._VKeyState				= True
		if key == 'x':								#Xキー
			self._XKeyState				= True
		if key == 'z':								#Zキー
			self._ZKeyState				= True
		if key == 'y':								#Yキー
			self._YKeyState				= True
		if key == 'h':								#Hキー
			self._HKeyState				= True
		if key == 'o':								# Oキー
			self.__mOKeyState			= True		
		if key == 'e':								#Eキー
			self._EKeyState				= True
		
		if key == '1':								#1キー
			self._1KeyState				= True
		if key == '2':								#2キー
			self._2KeyState				= True
		if key == '3':								#3キー
			self._3KeyState				= True
		if key == '4':								#4キー
			self._4KeyState				= True
		if key == '5':								#5キー
			self._5KeyState				= True
		if key == '6':								#6キー
			self._6KeyState				= True
		if key == '7':								#7キー
			self._7KeyState				= True
		if key == '8':								#8キー
			self._8KeyState				= True
		if key == '9':								#9キー
			self._9KeyState				= True
		if key == '0':								#0キー
			self._0KeyState				= True
			
	def OnKeyUp(self,key): 
		if key == viz.KEY_SHIFT_L or key == viz.KEY_SHIFT_R:		#Shiftキー
			self._ShiftKeyState = False
		if key == viz.KEY_CONTROL_L or key == viz.KEY_CONTROL_R:	#Controlキー
			self._CtrlKeyState = False
		if key == viz.KEY_ALT_L or key == viz.KEY_ALT_R:			#Altキー
			self._AltKeyState = False
			
		if key == ' ':								#スペースキー
			self._SpaceKeyState = False
			
		if key == viz.KEY_UP:						#矢印上キー
			self._RotateUpKeyState		= False
		if key == viz.KEY_DOWN:						#矢印下キー
			self._RotateDownKeyState	= False
		if key == viz.KEY_LEFT:						#矢印左キー
			self._RotateLeftKeyState	= False
		if key == viz.KEY_RIGHT:					#矢印右キー
			self._RotateRightKeyState	= False
			
		if key == viz.KEY_PAGE_UP:					#PageUpキー
			self._MoveUpKeyState		= False
		if key == viz.KEY_PAGE_DOWN:				#PageDownキー
			self._MoveDownKeyState		= False
			
		if key == 'w':								#Wキー
			self._MoveForwardKeyState	= False
		if key == 's':								#Sキー
			self._MoveBackwardKeyState	= False
		if key == 'a':								#Aキー
			self._MoveLeftKeyState		= False
		if key == 'd':								#Dキー
			self._MoveRightKeyState		= False
			
		if key == viz.KEY_HOME:						#Homeキー
			self._ResetViewKeyState		= False
			
		if key == 't':								#Tキー
			self._OffsetUpKeyState		= False
		if key == 'g':								#Gキー
			self._OffsetDownKeyState	= False
		if key == 'f':								#Fキー
			self._OffsetLeftKeyState	= False
		if key == 'h':								#Hキー
			self._OffsetRightKeyState	= False
		
		if key == 'p':								#Pキー
			self._PKeyState				= False
		if key == 'q':								#Qキー
			self._QKeyState				= False
		if key == viz.KEY_TAB:						#Tabキー
			self._TabKeyState			= False
		if key == viz.KEY_DELETE:					#Delキー
			self._DelKeyState			= False
		if key == 'c':								#Cキー
			self._CKeyState				= False
		if key == 'v':								#Vキー
			self._VKeyState				= False
		if key == 'x':								#Xキー
			self._XKeyState				= False
		if key == 'z':								#Zキー
			self._ZKeyState				= False
		if key == 'y':								#Yキー
			self._YKeyState				= False
		if key == 'h':								#Hキー
			self._HKeyState				= False
		if key == 'o':								# Oキー
			self.__mOKeyState			= False
		if key == 'e':								#Eキー
			self._EKeyState				= False
			
		if key == '1':								#1キー
			self._1KeyState				= False
		if key == '2':								#2キー
			self._2KeyState				= False
		if key == '3':								#3キー
			self._3KeyState				= False
		if key == '4':								#4キー
			self._4KeyState				= False
		if key == '5':								#5キー
			self._5KeyState				= False
		if key == '6':								#6キー
			self._6KeyState				= False
		if key == '7':								#7キー
			self._7KeyState				= False
		if key == '8':								#8キー
			self._8KeyState				= False
		if key == '9':								#9キー
			self._9KeyState				= False
		if key == '0':								#0キー
			self._0KeyState				= False
			
	#Shitキーの押下状態を取得するための関数
	def GetShiftKeyState(self):
		state = self._ShiftKeyState
		return state
		
	#Ctrlキーの押下状態を取得するための関数
	def GetCtrlKeyState(self):
		state = self._CtrlKeyState
		return state
		
	#Altキーの押下状態を取得するための関数
	def GetAltKeyState(self):
		state = self._AltKeyState
		return state
		
	#スペースキーの押下状態を取得するための関数
	def GetSpaceKeyState(self):
		state = self._SpaceKeyState
		return state
		
	#視点移動用キーの押下状態を取得するための関数
	def GetMoveViewKeyState(self):
		state = []
		state.append(self._MoveForwardKeyState)
		state.append(self._MoveBackwardKeyState)
		state.append(self._MoveLeftKeyState)
		state.append(self._MoveRightKeyState)
		state.append(self._MoveUpKeyState)
		state.append(self._MoveDownKeyState)
		
		return state
		
	#視点回転用キーの押下状態を取得するための関数
	def GetRotateViewKeyState(self):
		state = []
		state.append(self._RotateUpKeyState)
		state.append(self._RotateDownKeyState)
		state.append(self._RotateLeftKeyState)
		state.append(self._RotateRightKeyState)
		
		return state
		
	#視点リセット用キーの押下状態を取得するための関数
	def GetResetViewKeyState(self):
		state = self._ResetViewKeyState
		return state
		
	#通路幅変更機能用キーの押下状態を取得するための関数
	def GetOffsetKeyState(self):
		state = []
		state.append(self._OffsetUpKeyState)
		state.append(self._OffsetDownKeyState)
		state.append(self._OffsetLeftKeyState)
		state.append(self._OffsetRightKeyState)
		
		return state
		
	#ジョイスティック操作を確認するための関数
	def CheckJoystick(self):
		while True:
			x,y,z = joystick.getPosition()
			rx,ry,rz = joystick.getRotation()
			button2 = joystick.isButtonDown(2)	#リセット
			button4 = joystick.isButtonDown(4)	#上移動
			button5 = joystick.isButtonDown(5)	#下移動
			
			if self._mDeviceType==1:
				rx=z
				ry=rz
			
			self._MoveForwardJoyState = 0.0
			if y > 0:
				self._MoveForwardJoyState = y
			
			self._MoveBackwardJoyState = 0.0
			if y < 0:
				self._MoveBackwardJoyState = y * -1
				
			self._MoveLeftJoyState = 0.0
			if x < 0:
				self._MoveLeftJoyState = x * -1
				
			self._MoveRightJoyState = 0.0
			if x > 0:
				self._MoveRightJoyState = x
				
			self._MoveUpJoyState = False
			if button4 != 0:
				self._MoveUpJoyState = True
				
			self._MoveDownJoyState = False
			if button5 != 0:
				self._MoveDownJoyState = True
			
			self._RotateUpJoyState		= 0.0
			if ry > 0:
				self._RotateUpJoyState = ry
				
			self._RotateDownJoyState	= 0.0
			if ry < 0:
				self._RotateDownJoyState = ry * -1
				
			self._RotateLeftJoyState	= 0.0
			if rx < 0:
				self._RotateLeftJoyState = rx * -1
				
			self._RotateRightJoyState	= 0.0
			if rx > 0:
				self._RotateRightJoyState = rx
			
			self._ResetViewJoyState = False
			if button2 != 0:
				self._ResetViewJoyState = True
			
			yield viztask.waitFrame(1)
			
	#視点移動用ジョイスティック操作の状態を取得するための関数
	def GetMoveViewJoyState(self):
		state = []
		state.append(self._MoveForwardJoyState)
		state.append(self._MoveBackwardJoyState)
		state.append(self._MoveLeftJoyState)
		state.append(self._MoveRightJoyState)
		state.append(self._MoveUpJoyState)
		state.append(self._MoveDownJoyState)
		
		return state
		
	#視点回転用ジョイスティック操作の状態を取得するための関数
	def GetRotateViewJoyState(self):
		state = []
		state.append(self._RotateUpJoyState)
		state.append(self._RotateDownJoyState)
		state.append(self._RotateLeftJoyState)
		state.append(self._RotateRightJoyState)
		
		return state
		
	#視点リセット用ジョイスティック操作の状態を取得するための関数
	def GetResetViewJoyState(self):
		state = self._ResetViewJoyState
		return state
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
	#2D視点用のWindowを設定するための関数
	def SetOrthoViewWindow(self,window):
		self._OrthoViewWindow = window
		
	#Pの押下状態を取得するための関数
	def GetPKeyState(self):
		state = self._PKeyState
		return state
		
	#Qの押下状態を取得するための関数
	def GetQKeyState(self):
		state = self._QKeyState
		return state
		
	#TABの押下状態を取得するための関数
	def GetTabKeyState(self):
		state = self._TabKeyState
		return state
		
	#DELETEの押下状態を取得するための関数
	def GetDeleteKeyState(self):
		state = self._DelKeyState
		return state
		
	#Altキーの押下状態をリセットするための関数
	def ResetAltKey(self):
		self._AltKeyState = False
		
	#Cの押下状態を取得するための関数
	def GetCKeyState(self):
		state = self._CKeyState
		return state
		
	#Vの押下状態を取得するための関数
	def GetVKeyState(self):
		state = self._VKeyState
		return state
		
	#Xの押下状態を取得するための関数
	def GetXKeyState(self):
		state = self._XKeyState
		return state
		
	#Zの押下状態を取得するための関数
	def GetZKeyState(self):
		state = self._ZKeyState
		return state
		
	#Yの押下状態を取得するための関数
	def GetYKeyState(self):
		state = self._YKeyState
		return state
		
	#H押下状態を取得するための関数
	def GetHKeyState(self):
		state = self._HKeyState
		return state
		
	def GetOKeyState(self):
		return self.__mOKeyState
		
	def GetOKeyDownState(self):
		return self.__mOKeyDownState
		
	def GetOKeyUpState(self):
		return self.__mOKeyUpState
		
	def GetEKeyState(self):
		state = self._EKeyState
		return state
		
	#ログ関係キー押下状態を取得するための関数
	def GetLogKeyState(self):
		#S,E,Fキーの状態を取得
		state = [self._MoveBackwardKeyState, self._EKeyState, self._OffsetLeftKeyState]
		return state
		
	"""
	#毎フレーム呼び出されるための関数
	def CheckMouseState(self):
		
		self._MouseLeftState	= False
		self._MouseRightState	= False
		self._MouseMiddleState	= False
		
		mosueState = viz.mouse.getState()
		
		if mosueState & viz.MOUSEBUTTON_LEFT:
			self._MouseLeftState = True
		elif mosueState & viz.MOUSEBUTTON_RIGHT:
			self._MouseRightState = True
		elif mosueState & viz.MOUSEBUTTON_MIDDLE:
			self._MouseMiddleState = True
			
		#Down
		downState = None
		if self._MouseLeftState == True and self._PreMouseLeftState == False:
			downState = viz.MOUSEBUTTON_LEFT
		elif self._MouseRightState == True and self._PreMouseRightState == False:
			downState = viz.MOUSEBUTTON_RIGHT
		elif self._MouseMiddleState == True and self._PreMouseMiddleState == False:
			downState = viz.MOUSEBUTTON_MIDDLE
			
		if downState != None:
			self.MouseDownEvent(downState)
			
		#Up
		upState = None
		if self._MouseLeftState == False and self._PreMouseLeftState == True:
			upState = viz.MOUSEBUTTON_LEFT
		elif self._MouseRightState == False and self._PreMouseRightState == True:
			upState = viz.MOUSEBUTTON_RIGHT
		elif self._MouseMiddleState == False and self._PreMouseMiddleState == True:
			upState = viz.MOUSEBUTTON_MIDDLE
			
		if upState != None:
			self.MouseUpEvent(upState)
			
		self._PreMouseLeftState		= self._MouseLeftState
		self._PreMouseRightState	= self._MouseRightState
		self._PreMouseMiddleState	= self._MouseMiddleState
	"""
	
	#1～0キー押下状態を取得するための関数
	def GetNumKeyState(self):
		list = [self._1KeyState
			   ,self._2KeyState
			   ,self._3KeyState
			   ,self._4KeyState
			   ,self._5KeyState
			   ,self._6KeyState
			   ,self._7KeyState
			   ,self._8KeyState
			   ,self._9KeyState
			   ,self._0KeyState]
			   
		return list
		