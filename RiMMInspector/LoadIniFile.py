﻿# -*- coding: utf-8 -*-

import ConfigParser
import os
import sys
import codecs
import viz
from win32api import OutputDebugString

class LoadIniFileClass():
	
	mReadConfig		= None	#読み込みデータ
	mWriteConfig	= None	#書き込みデータ
	
	#コンストラクタ
	def __init__(self):
		pass
		
	#デストラクタ
	def __del__(self):
		self.mReadConfig	= None
		self.mWriteConfig	= None
	
	#iniファイルの読み込み
	def LoadIniFile(self,inPath):
		if inPath!='':	#iniファイルのパス確認
			self.mReadConfig = ConfigParser.SafeConfigParser()
			absPath = inPath
			try:
				#--- [メモ]
				#--- utf-8形式で読み込みを行う場合はBOM（Byte Oder Mark）があれば
				#--- そのバイト分を読み飛ばす処理を追加する必要がある
				f = codecs.open(inPath, 'r', 'shift-jis')
				self.mReadConfig.readfp(f)
				f.close()
				OutputDebugString("[StudioWa][INFO]: Load File "+absPath)
				return True		#成功
			except:
				try:
					absPath = viz.res.getPublishedPath(inPath)	#絶対パス変換
					f = codecs.open(absPath, 'r', 'shift-jis')	#再度読み込み
					self.mReadConfig.readfp(f)
					f.close()
					OutputDebugString("[StudioWa][INFO]: Load File "+str(absPath))
					return True		#成功
				except:
					OutputDebugString("[StudioWa][ERROR]: File load error ("+str(absPath)+")")
					return False	#失敗
		return False
	
	#データの取得（指定アイテムの値のみ）
	def GetItemData(self,inSection,inItem):
		try:
			data = unicode(self.mReadConfig.get(inSection,inItem))
			return data
		except:
			return None
		
	#データの取得（セクションデータをすべて取得）
	def GetSectionData(self,inSection):
		try:
			data = unicode(self.mReadConfig.items(inSection))
			return data
		except:
			return None
	
	#新規iniファイル設定の準備（リセット）
	def ResetIniData(self):
		self.mWriteConfig = ConfigParser.SafeConfigParser()
	
	#セクションの追加
	def AddSection(self,inSection):
		if self.mWriteConfig==None:
			self.mWriteConfig = ConfigParser.SafeConfigParser()
		try:
			self.mWriteConfig.add_section(inSection)
			return True
		except:
			return False
		
	#アイテムの追加
	def AddItem(self,inSection,inItem,inValue):
		if self.mWriteConfig==None:
			self.mWriteConfig = ConfigParser.SafeConfigParser()
		try:
			self.mWriteConfig.set(inSection,inItem,inValue)
			return True
		except:
			return False
		
	#iniファイルを書き出す
	def WriteIniData(self,filePath):
		try:
			f = codecs.open(filePath, 'w', 'shift-jis')		#ファイルを開く
			self.mWriteConfig.write(f)						#データの書き出し
			f.close()										#ファイルのクローズ
			return True
		except:
			return False
	