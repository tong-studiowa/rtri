﻿# F3DObjectClass
# Develop       : Python v2.7.2 / Vizard4.06.0138
# LastUpdate    : 2013/02/15
# Programer     : nishida

import viz
import vizact
import vizshape
import viztask
import vizproximity
import HandleIdClass
import FModalClass
from win32api import OutputDebugString

import ScenarioReciverClass

class F3DObjectClass(FModalClass.FModalClass):

# method
	# called method at first
	def __init__(self):
		self.mHandleId          = None
		self.mType              = None
		self.mModel             = None
		self.mIsVisible         = None
		self.mWorldPosition     = [0,0,0]
		self.mLocalPosition     = [0,0,0]
		self.mWorldQuaternion   = None
		self.mLocalQuaternion   = None
		self.mWorldScale        = None
		self.mLocalScale        = None
		self.mParent            = None
		self.mChildrenList      = None
		self.mIsKeepOnFloor     = None
		self._mCollision		= [0,0,0,0,0,0]
		
		self._mInKeycode		= None		#イベント開始用キーコード　（2013/8/9
		self._mOutKeycode		= None		#衝突判定用キーコード （2013/8/9
		self._mCollideManager	= None		#衝突判定用マネージャー　（2013/8/9
		self._mCollideTargetFlag= False		#衝突判定用ターゲットフラグ　（2013/8/9
		self._mCollideTask		= None		#衝突判定用処理スレッド　（2013/8/9
		self._mCollideTarget	= None
		self._mCollideSensor	= None
		
	# called method at delete
	def __del__(self):
		self.CollisionDeactive()
		if self.mModel!=None:
			self.mModel.remove()

	# initialize
	def Initialize(self):
		self.SetHandleId()				# set handle id

	# set handle id (private)
	def SetHandleId(self):
		if self.mHandleId==None:
			handleIdObject  = HandleIdClass.HandleIdClass() # create instance
			inHandleId      = handleIdObject.GetHandleId()  # get handle id
			self.mHandleId  = inHandleId                    # set parameter of handle id

	# return handle id
	def GetHandleId(self):
		return self.mHandleId

	# set 3dmodel
	def SetModel(self,model):
		if self.mModel!=None:									#値が入っていない場合のみ
			self.SetIsVisible(False)
			pos = self.mModel.getPosition(mode=viz.ABS_GLOBAL)
			quad = self.mModel.getQuat(mode=viz.ABS_GLOBAL)
			if self.mParent:
				model.setParent(self.mParent)
			model.setPosition(pos,viz.ABS_GLOBAL)
			model.setQuat(quad,mode=viz.ABS_GLOBAL)
			self.mModel.remove()
		self.mModel = model

	# return 3dmodel
	def GetModel(self):
		return self.mModel
	
	# set visble of 3dobject
	def SetIsVisible(self,isVisible=True,children=True):					#temp(未完)　子の階層の設定が未設定
		self.GetModel().visible(isVisible)
		self.mIsVisible = isVisible
		
	# return visble of 3dobject
	def GetIsVisible(self):
		return self.mIsVisible

	# set position of 3dobject
	def SetPosition(self,position,coordinates=True,children=True):		#temp(未完)　子の階層の設定が未設定
		if coordinates==False:	#local coordinates
			self.GetModel().setPosition(position,viz.ABS_PARENT)
			self.mLocalPosition = position
		else:					#world coordinates
			self.GetModel().setPosition(position,viz.ABS_GLOBAL)
			self.mWorldPosition = position

	# return position of 3dobject
	def GetPosition(self,coordinates=True):
		if coordinates==False:	#local coordinates
			return self.mLocalPosition
		else:					#world coordinates
			return self.mWorldPosition

	# set quaternion of 3dobject
	def SetQuaternion(self,quaternion,coordinates=True,children=True):	#temp(未完)　子の階層の設定が未設定
		if quaternion == [0.0,0.0,0.0,0.0]:
			return
		if coordinates==False:	#local coordinates
			self.GetModel().setQuat(quaternion,mode=viz.ABS_PARENT)
			self.mLocalQuaternion = quaternion
		else:					#world coordinates
			self.GetModel().setQuat(quaternion,mode=viz.ABS_GLOBAL)
			self.mWorldQuaternion = quaternion

	# return quaternion of 3dobject
	def GetQuaternion(self,coordinates=True):
		if coordinates==False:	#local coordinates
			return self.mLocalQuaternion
		else:					#world coordinates
			return self.mWorldQuaternion

	# set scale of 3dobject
	def SetScale(self,scale,coordinates=True,children=True):				#temp(未完)　子の階層の設定が未設定
		if coordinates==False:	#local coordinates
			self.GetModel().setScale(scale,viz.ABS_PARENT)
			self.mLocalScale = scale
		else:					#world coordinates
			self.GetModel().setScale(scale,viz.ABS_GLOBAL)
			self.mWorldScale = scale

	# return scale of 3dobject
	def GetScale(self,coordinates=True):
		if coordinates==False:	#local coordinates
			return self.mLocalScale
		else:					#world coordinates
			return self.mLocalScale

	# translate
	def Translate(self,translateValue,coordinates=True,children=True):	#temp(未完)　子の階層の設定が未設定
		coordinatesParameter = None
		if coordinates==False:	#local coordinates
			coordinatesParameter = viz.ABS_PARENT
		else:					#world coordinates
			coordinatesParameter = viz.ABS_GLOBAL

		actionMoveTo = vizact.moveTo(position,mode=coordinatesParameter)
		self.GetModel().runAction(actionMoveTo)

	# move to
	def MoveTo(self,position,coordinates=True,children=True,time=0,lookAt=[0,0,0]):	#temp(未完)　子の階層の設定が未設定
		coordinatesParameter = None
		p = position
		
		if coordinates==False:	#local coordinates
#			coordinatesParameter = viz.ABS_PARENT
			coordinatesParameter = viz.REL_LOCAL
			
			startPos =  self.GetModel().getPosition()
		
			actionMoveTo = vizact.moveTo(position,time=0,mode=coordinatesParameter)
			self.GetModel().runAction(actionMoveTo)
			
			p = self.GetModel().getPosition()
		
			self.SetPosition(startPos)
			
			coordinatesParameter = viz.ABS_GLOBAL

		else:					#world coordinates
			coordinatesParameter = viz.ABS_GLOBAL

		actionMoveTo = vizact.moveTo(p,time=time,mode=coordinatesParameter)
		self.GetModel().runAction(actionMoveTo)
		self.GetModel().lookAt(lookAt,roll=0.0)#,mask=viz.HEAD_ORI)
		
		self.mWorldPosition = self.GetModel().getPosition()
		self.mWorldQuaternion = self.GetModel().getQuat()

	# rotate
	def Rotate(self,quaternion,coordinates=True,children=True):			#temp(未完)　子の階層の設定が未設定
		coordinatesParameter = None
		if coordinates==False:	#local coordinates
			coordinatesParameter = viz.ABS_PARENT
		else:					#world coordinates
			coordinatesParameter = viz.ABS_GLOBAL

		actionSpinTo = vizact.spinTo(quat=quaternion,mode=coordinatesParameter)
		self.GetModel().runAction(actionSpinTo)
		
		self.mWorldQuaternion = self.GetModel().getQuat()

	# turn to
	def TurnTo(self,quaternion,coordinates=True,children=True,time=0):		#temp(未完)　子の階層の設定が未設定
		coordinatesParameter = None
		if coordinates==False:	#local coordinates
			coordinatesParameter = viz.ABS_PARENT
		else:					#world coordinates
			coordinatesParameter = viz.ABS_GLOBAL

		actionSpinTo = vizact.spinTo(quat=quaternion,time=time,mode=coordinatesParameter)
		self.GetModel().runAction(actionSpinTo)
		
		self.mWorldQuaternion = self.GetModel().getQuat()
		
	# return position of screen coordinates
	def Get2DPosition(self):
		return viz.worldtoscreen(self.GetPosition(True))

	# return mouse position of world coordinates
	def GetMousePosition(self):
		return viz.screentoworld(viz.mouse.getPosition())

	# set parent 3dobject
	def SetParent(self,parent):
		self.GetModel().setParent(parent)
		self.mParent = parent

	# return parent 3dobject
	def GetParent(self):
		self.mParent

	# add child 3dobject
	def AddChild(self,child):									#temp(未完)
		pass

	# remove child 3dobject
	def RemoveChild(self,child):								#temp(未完)
		pass

	# return children 3dobjects List
	def GetChildrenList(self):
		self.mChildrenList

	# set floor
	def SetIsKeepOnFloor(self,isKeepOnFloor=False):					#temp(未完)
		self.mIsKeepOnFloor = isKeepOnFloor

	# get floor
	def GetIsKeepOnFloor(self):
		return self.mIsKeepOnFloor
		
	# move to 2
	def MoveTo2(self,position,quaternion,coordinates=True,children=True,time=0):	#temp(未完)　子の階層の設定が未設定
		coordinatesParameter = None
		targetPosition   = position
		targetQuaternion = quaternion

		if coordinates==False:	#local coordinates
			coordinatesParameter = viz.REL_LOCAL
			
			dummy = vizshape.addPlane([0,0])
			dummy.disable(viz.PICKING)
			dummy.setPosition(self.GetPosition(True))
			dummy.setQuat(self.GetQuaternion(True))
			
			actionMoveTo = vizact.moveTo(position,time=0,mode=coordinatesParameter)
			actionSpinTo = vizact.spinTo(quat=quaternion,time=0,mode=coordinatesParameter)
			action		 = vizact.parallel(actionMoveTo,actionSpinTo)
			dummy.runAction(action)
			
			targetPosition   = dummy.getPosition(mode=viz.ABS_GLOBAL)
			targetQuaternion = dummy.getQuat(mode=viz.ABS_GLOBAL)
			
			self.mWorldPosition		= targetPosition
			self.mWorldQuaternion	= targetQuaternion
			
			dummy.remove()
			
			coordinatesParameter = viz.ABS_GLOBAL

		else:					#world coordinates
			coordinatesParameter = viz.ABS_GLOBAL
		
		q0=[0,0,0,0]
		if targetQuaternion==q0:
			targetQuaternion=[0,0,0,1]
		
		if position==[0.0,0.0,0.0]:
			if quaternion==[0.0,0.0,0.0,1.0]:
				return
		
		actionMoveTo = vizact.moveTo(targetPosition,time=time,mode=coordinatesParameter)
		actionSpinTo = vizact.spinTo(quat=targetQuaternion,time=time,mode=coordinatesParameter)
		action		 = vizact.parallel(actionMoveTo,actionSpinTo)
		
		self.GetModel().runAction(action)
		
		
	# stop actions
	def StopAction(self):
		self.GetModel().clearActions(pool=viz.ALL_POOLS)
		
	# pause actions
	def pauseAction(self):	
		self.GetModel().pauseActions(pool=viz.ALL_POOLS)
	
	# restart actions
	def restartAction(self):
		self.GetModel().resumeActions(pool=viz.ALL_POOLS)
	
	# add method by Kagawadai
	#def SetKeycode(self,iKey):
	#	self._mKeyCode = iKey
	
	def SetCollision(self,iCollision):
		if iCollision!=None and iCollision!="":
			try:
				data = iCollision.split(",")
				if data!=None:
					self._mCollision = [float(data[0]),float(data[1]),float(data[2]),
										float(data[3]),float(data[4]),float(data[5])]
			except:
				self._mCollision = [0,0,0,0,0,0]
	
	
	#衝突判定用メソッド　（2013/8/9）
	#---------------------------------------------
	def SetKeycodeIn(self,inkeycode):
		if inkeycode!=None and inkeycode!='':
			self._mInKeycode = int(inkeycode)
	
	def SetKeycodeOut(self,outkeycode):
		if outkeycode!=None and outkeycode!='':
			self._mOutKeycode = int(outkeycode)
	
	def _CheckCollide(self,sensor,key):
		yield vizproximity.waitEnter(sensor)				#衝突検知待機
		ScenarioReciverClass.SendKeycodeToInputmanager(key)	#キーコードの送信
	
	def SetCollisionTarget(self,isTarget):
		if isTarget=='1':
			self._mCollideTargetFlag = True
		else:
			self._mCollideTargetFlag = False
	
	def CollisionActivate(self,manager,model):
		#マネージャーの登録
		self._mCollideManager = manager
		#ターゲットの生成
		if self._mCollideTargetFlag==True:
			if model:
				try:
					#boundingBox = model.getBoundingBox()
					#boundingBox.setCenter(collideOffset)
					#boundingBox.setSize(collideSize)
					self._mCollideTarget = vizproximity.Target(model)
					if self._mCollideTarget:
						manager.addTarget(self._mCollideTarget)
					OutputDebugString('[StudioWa][INFO][CollisionSetting] create collide target model')
					return
				except:
					OutputDebugString('[StudioWa][ERROR][CollisionSetting] not create collide target model')
					return
			else:
				OutputDebugString('[StudioWa][ERROR][CollisionSetting] not find collide model')
				return
		
		collideSize = [0,0,0]
		collideOffset = [0,0,0]
		
		#コリジョン値の入力があることをチェック　（なければ衝突判定できない為）
		if self._mCollision!=[0,0,0,0,0,0]:
			try:	#衝突判定ボックスのサイズと位置取得
				collideStartPos = [self._mCollision[0],self._mCollision[1],self._mCollision[2]]
				collideSize		= [self._mCollision[3],self._mCollision[4],self._mCollision[5]]
				collideOffset	= [	collideSize[0]/2+collideStartPos[0],
									collideSize[1]/2+collideStartPos[1],
									collideSize[2]/2+collideStartPos[2]]
			except:
				OutputDebugString('[StudioWa][ERROR][CollisionSetting] invalid value (collide paramere) '+str(self._mCollision))
				return
		else:
			return
		
		#キーコードの入力があることをチェック　（なければ衝突判定してもキーコードが送られない為）
		if self._mOutKeycode!=None and self._mOutKeycode!='':	
			if self._mCollideTask==None:	#既に衝突判定タスクが動作していないことをチェック
				if model:
					try:
						self._mCollideSensor = vizproximity.Sensor(vizproximity.Box(collideSize,center=collideOffset),model)
						if self._mCollideSensor:
							manager.addSensor(self._mCollideSensor)
						#衝突判定タスクの開始
						self._mCollideTask = viztask.schedule(self._CheckCollide(self._mCollideSensor,self._mOutKeycode))
					except:
						OutputDebugString('[StudioWa][ERROR][CollisionSetting] not create collide secsor model')
						return
				else:
					OutputDebugString('[StudioWa][ERROR][CollisionSetting] not find collide model')
					return
	
	def CollisionDeactive(self):
		if self._mCollideTask:#衝突判定タスクの終了
			self._mCollideTask.kill()
		if self._mCollideTarget and self._mCollideManager:				#衝突判定用ターゲットをマネージャーから外す
			self._mCollideManager.removeTarget(self._mCollideTarget)
		if self._mCollideSensor and self._mCollideManager:				#衝突判定用センサーをマネージャーから外す
			self._mCollideManager.removeSensor(self._mCollideSensor)
		self._mCollideManager = None
