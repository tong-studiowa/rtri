﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓関係のファンクション制御のクラス定義
class WindowFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowFunction] : Init WindowFunction')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectModelClass = None
		self._SetWindowHeightButtonClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		windowList = []
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectModelClass,setWindowHeightButtonClass,undoClass
				,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectModelClass = selectModelClass
		self._SetWindowHeightButtonClass = setWindowHeightButtonClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowFunction] : Start WindowFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.SetHeight()
		elif num == 1:
			self.Delete()
		elif num == 2:
			self.Copy()
	
	#選択が解除された
	def Hide(self):
		self._SetWindowHeightButtonClass.SetVisible(False)
		
	#高さ変更
	def SetHeight(self):
		if self._SetWindowHeightButtonClass.GetVisible():
			self._SetWindowHeightButtonClass.SetVisible(False)
		else:
			self._SetWindowHeightButtonClass.SetVisible(True)
			
			windowList = self._SelectModelClass.GetSelectedWindowList()
			height = self._RoomControlClass.GetWindowHeight(windowList[0][0],windowList[0][1])
			self._SetWindowHeightButtonClass.SetValue(windowList,height)
			
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(22)
		
		windowList = self._SelectModelClass.GetSelectedWindowList()
		
		self._SelectModelClass.UnSelectWindow()
		
		if windowList != []:
			for x in range(len(windowList)):
				windowData = windowList[x]
		
				windowStateTemp = self._RoomControlClass.GetWindowData(windowData[0])
				windowState = ['',0.0,0,0.0]
				windowState[0] = windowStateTemp[windowData[1]][0]
				windowState[1] = windowStateTemp[windowData[1]][1]
				windowState[2] = windowStateTemp[windowData[1]][2]
				windowState[3] = windowStateTemp[windowData[1]][3]
				
				self._UndoData.append([windowData[0],windowData[1],windowState])
				
			roomCount = self._RoomControlClass.GetRoomCount()
			delWindowList = []
			for x in range(roomCount):
				delWindowList.append([])
				
			for x in range(len(windowList)):
				windowData = windowList[x]
				delWindowList[windowData[0]].append(windowData[1])
				
			for x in range(len(delWindowList)):
				delWindowList2 = delWindowList[x]
				for y in range(len(delWindowList2)):
					windowNum = delWindowList2[y] - y
					self._RoomControlClass.DeleteWindow(x,windowNum)
					
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#コピー
	def Copy(self):
		self._UndoData = []
		self._UndoData.append(21)
		
		windowList = self._SelectModelClass.GetSelectedWindowList()
		
		count = len(windowList)
		if windowList != []:
			for x in range(len(windowList)):
				windowData = windowList[x]
		
				self._RoomControlClass.CopyWindow(windowData[0],windowData[1])
				
				windowCount = self._RoomControlClass.GetWindowCount(windowData[0])
				newWindowNum = windowCount - 1
				self._UndoData.append([windowData[0],newWindowNum])
				
				self._RoomControlClass.SetWindowVisible(windowData[0],False)
				
		#コピー先を選択
		newWindowList = []
		for x in range(len(self._UndoData)):
			if x != 0:
				newWindowList.append(self._UndoData[x])
		self._SelectModelClass.ReSelectWindow(newWindowList)
		
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		