﻿# coding: utf-8

import viz
import vizshape
import vizfx

import Object3d
import LoadIniFile
import Light

from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'
RESOURCE_FOLDER_PATH = '..\\Resource\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#色付け用シェーダーの設定
vertexCode = """
void main(void)
{
	vec4 position = gl_ModelViewMatrix * gl_Vertex; // 頂点位置＠視点座標
	vec3 normal = normalize(gl_NormalMatrix * gl_Normal);
	vec3 light = normalize((gl_LightSource[0].position - position).xyz);
	float diffuse = max(dot(light, normal), 0.0);
	
	gl_FrontColor = gl_FrontLightProduct[0].diffuse * 0.25 * diffuse + gl_FrontMaterial.emission;
	
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
"""

fragmentCode = """
void main(void)
{
	gl_FragColor = gl_Color;
}
"""

shader = viz.addShader(frag = fragmentCode, vert = vertexCode)
colorUniform = viz.addUniformFloat('color',viz.RED)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム制御のクラス定義
class Item(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,modelControlClass):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Init Item')
		
		self._ModelControlClass = modelControlClass
		
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		self._Texture = None
		self._TextureList = []
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		
		self._Size = [1.0,1.0,1.0]			#今設定しているサイズ
		self._SizeBox = [1.0,1.0,1.0]		#モデル読み込み時の実際のサイズ
		self._SizeOriginal = [1.0,1.0,1.0]	#モデル読み込み時の影を外したサイズ
		
		self._IsSlimModel = False	#ポップなどモデルが薄い場合は板を表示
		
		self._IsDummyBox = False
		self._DummyBoxRoot = None
		self._DummyBoxDummy1 = None
		self._DummyBoxDummy2 = None
		
		self._DummyBoxLineSize = 0.1		#各辺のラインの幅
		self._DummyBoxLineList = []
		
		self._DummyBoxNodeNameList = ['Top'
									 ,'Front'
									 ,'Right'
									 ,'Back'
									 ,'Left'
									 ,'Bottom']
		
		self._Id = None
		self._FileName = None
		self._MoveYState = False
		
		self._RotateSnap = rotateSnap
		
		self._PlaneModel = None
		self._PlaneColor = viz.BLUE
		
		self._Is3d = False
		
		self._HighlightState = False
		
		self._SoundFilePath = None
		
		self._LightCategory = -1
		self._LightClass = None
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Item] : Start Item Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = None
		if path.exists(fileName) == True:		#絶対パスかどうか
			filePath = fileName
		else:
			filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		#print 'itemFilePath=',filePath
		
		#ローカルフォルダを確認
		localFileName = filePath.split('/')
		if len(localFileName) == 1:
			localFileName = filePath.split('\\')
		localFileName = localFileName[-1]
		#print 'localFileName=',localFileName
		localFilePath = viz.res.getPublishedPath(RESOURCE_FOLDER_PATH + 'Items\\' + localFileName)
		
		#モデルの読み込み
		model = None
		if path.exists(localFilePath) == True:	#ローカルフォルダにファイルがあるか
#			model = self._ModelControlClass.Add(localFilePath)
			model = vizfx.addChild(localFilePath)
		else:
#			model = self._ModelControlClass.Add(filePath)
			model = vizfx.addChild(filePath)
			
		self._Model = model
		self._Model.color([1,1,1])
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		self._Texture = self._Model.getTexture()
		self._TextureList = []
	
		#半透明モデルのオーダーを設定
		nodeNameList = self._Model.getNodeNames()
		for name in nodeNameList:
			if name[-2:] == '_A':
				#self._Model.drawOrder(40,name)
				self._Model.enable(viz.SAMPLE_ALPHA_TO_COVERAGE)
				self._Model.disable(viz.BLEND)
				
		#ライトの設定
		#print 'filePath',filePath
		if filePath[-10:] == 'Light.osgb':
			#print 'light'
			self._LightCategory = 0
			if filePath[-15:] == 'PointLight.osgb':
				self._LightCategory = 1
			elif filePath[-14:] == 'SpotLight.osgb':
				self._LightCategory = 2
				
			self._LightClass = Light.Light(self._LightCategory,self._Model)
			
		#ダミーボックスの設定
		#print 'filePath',filePath
		if filePath[-13:] == 'DummyBox.osgb':
			#print 'dummyBox'
			self._IsDummyBox = True
			self.InitDummyBox(model)
			
			#self._Model.drawOrder(40)
			self._Model.enable(viz.SAMPLE_ALPHA_TO_COVERAGE)
			self._Model.disable(viz.BLEND)
		
		self.CheckSize()
		
		if self._IsSlimModel:
			self.CreatePlaneModel()
			self.SetPlaneVisible()
		
		self.SetPickable(False)
		
		self._FileName = filePath
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			if self._LightCategory != -1:			#ライトを削除
				self._LightClass.Delete()
			
			self._Model.remove()	#モデルを削除
			
			if self._IsDummyBox:
				self._DummyBoxRoot.remove()
				self._DummyBoxDummy1.remove()
				self._DummyBoxDummy2.remove()
				for line in self._DummyBoxLineList:
					line.remove()
					
		self._Model = None
		self._FileName = None
		
		self._IsDummyBox = False
		self._DummyBoxRoot = None
		self._DummyBoxDummy1 = None
		self._DummyBoxDummy2 = None
		self._DummyBoxLineList = []
		
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			position = iPosition
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = position[1] + self._PositionOffset[1]
			position[2] = position[2] + self._PositionOffset[2]
			
			if self._IsDummyBox:
				self._DummyBoxRoot.setPosition(position)
			else:
				self._Model.setPosition(position)
				
			self._Position = position
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation,snap=True):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			if self._IsDummyBox:
				self._DummyBoxRoot.setEuler(eulerAngle)
			else:
				self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
			
			#スナップ
			ori = 0.0
			if snap:
				snapOrientation = 0.0
				count = 360 / self._RotateSnap
				for x in range(count):
					angle = self._RotateSnap * x + self._RotateSnap / 2
					if self._Orientation < angle:
						snapOrientation = self._RotateSnap * x
						break
				ori = snapOrientation
			else:
				ori = self._Orientation
						
			if self._IsDummyBox:
				self._DummyBoxRoot.setEuler(ori)
			else:
				self._Model.setEuler(ori)
				
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		snapOrientation = 0
		count = 360 / self._RotateSnap
		for x in range(count):
			angle = self._RotateSnap * x + self._RotateSnap / 2
			if self._Orientation < angle:
				snapOrientation = self._RotateSnap * x
				break
				
		self._Orientation = snapOrientation
		
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
		
	#角度設定(INIファイル用)
	def SetAxisAngle(self,axisAngle):
		if self._Model != None and axisAngle != None:
			if self._IsDummyBox:
				self._DummyBoxRoot.setAxisAngle(axisAngle)
			else:
				self._Model.setAxisAngle(axisAngle)
				
			val = 0
			yVal = axisAngle[1]
			angleVal = axisAngle[3]
			if yVal < -0.5:
				val = angleVal * -1
			elif yVal > 0.5:
				val = angleVal
			if val < 0:
				val += 360
			self._Orientation = val
			
	#角度取得(INIファイル用)
	def GetAxisAngle(self):
		if self._IsDummyBox:
			axisAngle = self._DummyBoxRoot.getAxisAngle(viz.ABS_GLOBAL)
		else:
			axisAngle = self._Model.getAxisAngle(viz.ABS_GLOBAL)
		return axisAngle
		
	#ハイライト表示
	def SetHighlight(self,state):
		self._HighlightState = state
		
		if state:
			#色付け用のシェーダーをモデルに設定
			self._Model.apply(shader)
			self._Model.apply(colorUniform)
			self._Model.emissive([1.0,0,0])
		else:
			#色付け用のシェーダーをモデルから外す
			shader.unapply()
			self._Model.emissive(self._Emissive)
		
		if self._PlaneModel:
			if state == True:
				self._PlaneModel.color(viz.RED)
				self._PlaneModel.emissive(viz.RED)
				self._PlaneModel.ambient(viz.RED)
				self._PlaneModel.specular(viz.RED)
			else:
				self._PlaneModel.color(self._PlaneColor)
				self._PlaneModel.emissive(self._PlaneColor)
				self._PlaneModel.ambient(self._PlaneColor)
				self._PlaneModel.specular(self._PlaneColor)
				
	#ハイライト状態の取得
	def GetHighlightState(self):
		state = self._HighlightState
		return state
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
		
		if self._PlaneModel:
			if state == True:
				self._PlaneModel.enable(viz.PICKING)
			elif state == False:
				self._PlaneModel.disable(viz.PICKING)
		
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#サイズ設定
	def SetSize(self,size):
		if size[0] == 0.0 and size[1] == 0.0 and  size[2] == 0.0:
			pass
		else:
			self._Size = size
			
			newSize = [1.0,1.0,1.0]
			if self._SizeOriginal[0] != 0:
				newSize[0] = self._Size[0] / self._SizeOriginal[0]
			if self._SizeOriginal[1] != 0:
				newSize[1] = self._Size[1] / self._SizeOriginal[1]
			if self._SizeOriginal[2] != 0:
				newSize[2] = self._Size[2] / self._SizeOriginal[2]
			
			self._Model.setScale(newSize)
			self.SetDummyBoxLinePosition()
		
	#サイズ取得
	def GetSize(self):
		size = self._Size
		
		return size
		
	#テクスチャ設定
	def SetTexture(self,node,filePath):
		fullPath = None
		if path.exists(filePath) == True:		#絶対パスかどうか
			fullPath = filePath
		else:
			fullPath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + filePath)
			
		newTexture = viz.addTexture(fullPath)
		if newTexture != None:
			newTexture.wrap(viz.WRAP_S,viz.REPEAT)
			newTexture.wrap(viz.WRAP_T,viz.REPEAT)
			
			self._Model.texture(newTexture,node=node)
			#self._Model.texture(newTexture)
			
			#テクスチャリスト更新
			self.RemoveFromTextureList(node)
			
			texture = [node,filePath]
			self._TextureList.append(texture)
			
	#テクスチャ取得
	def GetTexture(self,node):
		#texture = self._Model.getTexture(node=node)
		texture = self._Model.getTexture()
		
		return texture
		
	#テクスチャ再設定
	def ResetTexture(self,node,texture,filePath):
		#テクスチャ変更
		if filePath == None:
			#self._Model.texture(texture,node=node)
			self._Model.texture(texture)
			
		else:
			fullPath = None
			if path.exists(filePath) == True:		#絶対パスかどうか
				fullPath = filePath
			else:
				fullPath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + filePath)
				
			preTexture = viz.addTexture(fullPath)
			#self._Model.texture(preTexture,node=node)
			self._Model.texture(preTexture)
			
		#テクスチャリスト更新
		self.RemoveFromTextureList(node)
		if filePath != None:
			newData = [node,filePath]
			self._TextureList.append(newData)
			
	#指定のNodeの情報をTextureListから外す
	def RemoveFromTextureList(self,node):
		newList = []
		for data in self._TextureList:
			if data[0] != node:
				newList.append(data)
		self._TextureList = newList
		
	#テクスチャのファイルパス取得
	def GetTextureFilePath(self,node):
		filePath = None
		for data in self._TextureList:
			if data[0] == node:
				filePath = data[1]
		return filePath
		
	#テクスチャリスト取得
	def GetTextureList(self):
		textureList = self._TextureList
		
		return textureList
	
	#影抜きのモデルのサイズを設定
	def CheckSize(self):
		#モデル読み込み時の実際のサイズ
		box = self._Model.getBoundingBox()
		self._SizeBox = box.size
		
		#モデル読み込み時の影を外したサイズ
		sizeXmin = 100
		sizeXmax = -100
		sizeYmin = 100
		sizeYmax = -100
		sizeZmin = 100
		sizeZmax = -100
		
		nodeNameList = self._Model.getNodeNames()
		for nodeName in nodeNameList:
			if nodeName[:6] != 'shadow':
				nodeBox = self._Model.getBoundingBox(mode=viz.ABS_GLOBAL,node=nodeName)
				if nodeBox.size != box.size:
					nodeXmin = nodeBox.getXmin()
					if nodeXmin < sizeXmin:
						sizeXmin = nodeXmin
						
					nodeXmax = nodeBox.getXmax()
					if nodeXmax > sizeXmax:
						sizeXmax = nodeXmax
				
					nodeYmin = nodeBox.getYmin()
					if nodeYmin < sizeYmin:
						sizeYmin = nodeYmin
						
					nodeYmax = nodeBox.getYmax()
					if nodeYmax > sizeYmax:
						sizeYmax = nodeYmax
				
					nodeZmin = nodeBox.getZmin()
					if nodeZmin < sizeZmin:
						sizeZmin = nodeZmin
						
					nodeZmax = nodeBox.getZmax()
					if nodeZmax > sizeZmax:
						sizeZmax = nodeZmax
		
		originalSize = [sizeXmax-sizeXmin,sizeYmax-sizeYmin,sizeZmax-sizeZmin]
		if originalSize == [0.0,0.0,0.0] or originalSize == [-200.0,-200.0,-200.0]:
			originalSize = box.size
			
		self._SizeOriginal = originalSize
		
		#今設定しているサイズ
		self._Size = self._SizeOriginal
		
		#モデルが薄いか確認
		self._IsSlimModel = False
		if self._SizeBox[2] < 0.0025:
			self._IsSlimModel = True
		
	#縦方向の移動制限を設定
	def SetMoveYState(self,state):
		self._MoveYState = False
		if state == 1:
			self._MoveYState = True
			
	#縦方向の移動制限を取得
	def GetMoveYState(self):
		state = 0
		if self._MoveYState:
			state = 1
			
		return state
	
	#音源を設定
	def SetSound(self,sound):
		self._SoundFilePath = sound
			
	#音源を取得
	def GetSound(self):
		sound = self._SoundFilePath
		return sound
	
	#薄い場合の板モデルを作成
	def CreatePlaneModel(self):
		planeModel = vizshape.addPlane([self._SizeOriginal[0],0.1])
		planeModel.setParent(self._Model)
		planeModel.setPosition(0,0.1,0)
		
		planeModel.color(self._PlaneColor)
		planeModel.emissive(self._PlaneColor)
		planeModel.ambient(self._PlaneColor)
		planeModel.specular(self._PlaneColor)
	
		self._PlaneModel = planeModel
		
		print 'create',self._PlaneModel
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		self.SetPlaneVisible()
		
	#薄い場合の板モデルの表示設定
	def SetPlaneVisible(self):
		if self._PlaneModel:
			if self._Is3d == True:
				self._PlaneModel.visible(viz.OFF)
			elif self._Is3d == False:
				self._PlaneModel.visible(viz.ON)
	
	#モデル名を取得
	def GetFileName(self):
		fileName = self._FileName
		return fileName
		
	#ダミーボックスの設定
	def InitDummyBox(self,model):
		root = vizshape.addBox()
		root.disable(viz.RENDERING)
		root.disable(viz.INTERSECTION)
		root.disable(viz.PICKING)
		self._DummyBoxRoot = root
		
		model.setParent(root)

		#マウスのローカル座標取得用
		dummy = vizshape.addBox()
		dummy.disable(viz.RENDERING)
		dummy.disable(viz.DEPTH_WRITE)
		dummy.disable(viz.INTERSECTION)
		dummy.disable(viz.PICKING)
		dummy.setParent(root)
		self._DummyBoxDummy1 = dummy

		#中心点のローカル座標取得用
		dummy2 = vizshape.addBox()
		dummy2.disable(viz.RENDERING)
		dummy2.disable(viz.DEPTH_WRITE)
		dummy2.disable(viz.INTERSECTION)
		dummy2.disable(viz.PICKING)
		dummy2.setParent(root)
		self._DummyBoxDummy2 = dummy2

		for x in range(12):
			line = vizshape.addBox()
			line.color(viz.GRAY)
			line.disable(viz.INTERSECTION)
			line.disable(viz.PICKING)
			line.setParent(root)
			self._DummyBoxLineList.append(line)
			
		self.SetDummyBoxLinePosition()

	#ダミーボックスの枠線を配置
	def SetDummyBoxLinePosition(self):
		box = self._Model.getBoundingBox()
		modelSize = box.size
		#print 'modelSize=',modelSize
		
		lineSize = self._DummyBoxLineSize
		
		for x in range(len(self._DummyBoxLineList)):
			line = self._DummyBoxLineList[x]
			
			scale = [lineSize,lineSize,lineSize]
			if x < 4:
				scale[0] = (modelSize[0]+0.02)
			elif x < 8:
				scale[1] = (modelSize[1]+0.02)
			else:
				scale[2] = (modelSize[2]+0.02)
			line.setScale(scale)
			
			pos = [0,modelSize[1]*0.5,0]
			
			if   x == 0:
				pos[1] = (lineSize*0.5)-0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 1:
				pos[1] = (lineSize*0.5)-0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 2:
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 3:
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
				
			elif x == 4:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 5:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[2] = -(modelSize[2]*0.5)+(lineSize*0.5)-0.01
			elif x == 6:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
			elif x == 7:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[2] = (modelSize[2]*0.5)-(lineSize*0.5)+0.01
				
			elif x == 8:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[1] = (lineSize*0.5)-0.01
			elif x == 9:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[1] = (lineSize*0.5)-0.01
			elif x == 10:
				pos[0] = (modelSize[0]*0.5)-(lineSize*0.5)+0.01
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
			elif x == 11:
				pos[0] = -(modelSize[0]*0.5)+(lineSize*0.5)-0.01
				pos[1] = modelSize[1]-(lineSize*0.5)+0.01
				
			line.setPosition(pos)
			
	#ダミーボックス関係のモデル取得
	def GetDummyBoxModel(self):
		return self._Model,self._DummyBoxRoot,self._DummyBoxDummy1,self._DummyBoxDummy2,self._DummyBoxLineList
		
	#ダミーボックスのラインの表示を設定
	def SetDummyBoxLineVisible(self,state):
		visibleState = viz.OFF
		if state:
			visibleState = viz.ON
			
		for lineModel in self._DummyBoxLineList:
			lineModel.visible(visibleState)
			
	#ダミーボックスのルートを選択
	def GetDummyBoxRoot(self):
		root = None
		if self._IsDummyBox:
			root = self._DummyBoxRoot
			
		return root
		
	#音源かどうかの状態取得
	def GetSoundState(self):
		state = False
		if self._SoundFilePath != None:
			state = True
		return state
		
	#音の再生制御
	def PlaySound(self,state):
		if self._Model != None and self._SoundFilePath != None:
			sound = viz.res.getPublishedPath(RESOURCE_FOLDER_PATH + self._SoundFilePath)
			if state:
				self._Model.playsound( sound )
				
				self._Model.visible(viz.OFF)
				self._Model.disable(viz.RENDERING)
				self._Model.disable(viz.DEPTH_WRITE)
				self._Model.disable(viz.INTERSECTION)
				
			else:
				self._Model.playsound( sound, flag = viz.STOP )
				
				self._Model.visible(viz.ON)
				self._Model.enable(viz.RENDERING)
				self._Model.enable(viz.DEPTH_WRITE)
				self._Model.enable(viz.INTERSECTION)
				
				
	#ライトの設定情報を取得
	def GetLightData(self):
		category = 0
		intensity = 1.0
		attenuation = 1.0
		spread = 1.0
		
		if self._LightCategory != -1:
			category,intensity,attenuation,spread = self._LightClass.GetData()
		
		return category,intensity,attenuation,spread
		
	#照度を設定
	def SetIntensity(self,intensity):
		if self._LightCategory != -1:
			self._LightClass.SetIntensity(intensity)
		
	#減衰を設定
	def SetAttenuation(self,attenuation):
		if self._LightCategory != -1:
			self._LightClass.SetAttenuation(attenuation)
		
	#広がりを設定
	def SetSpread(self,spread):
		if self._LightCategory != -1:
			self._LightClass.SetSpread(spread)
			
	#ライトかどうかの状態取得
	def GetLightState(self):
		state = False
		if self._LightCategory != -1:
			state = True
		return state
		
	#ライトのアクティブ状態を設定
	def SetLightState(self,state):
		self._LightClass.SetState(state)
		
	#照明の制御
	def PlayLight(self,state):
		if self._Model != None:
			if self._LightCategory != -1:
				if state:
					self._LightClass.SetState(True)
					
					self._Model.visible(viz.OFF)
					self._Model.disable(viz.RENDERING)
					self._Model.disable(viz.DEPTH_WRITE)
					self._Model.disable(viz.INTERSECTION)
					
				else:
					self._LightClass.SetState(False)
					
					self._Model.visible(viz.ON)
					self._Model.enable(viz.RENDERING)
					self._Model.enable(viz.DEPTH_WRITE)
					self._Model.enable(viz.INTERSECTION)
					
			else:
				if state:
					self._Model.enable(viz.LIGHTING)
				else:
					self._Model.disable(viz.LIGHTING)
					
	#照明のパラメータ取得
	def GetLightParam(self):
		lightParamTemp = self._LightClass.GetData()
		lightParam = [0,0,0]
		lightParam[0] = lightParamTemp[1]
		lightParam[1] = lightParamTemp[2]
		lightParam[2] = lightParamTemp[3]
		
		return lightParam
			
	#照明のパラメータ設定
	def SetLightParam(self,lightParam):
		self._LightClass.SetIntensity(lightParam[0])
		self._LightClass.SetAttenuation(lightParam[1])
		self._LightClass.SetSpread(lightParam[2])
		