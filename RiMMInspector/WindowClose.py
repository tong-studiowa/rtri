﻿import viz
import vizinput
import win32con

class WindowClose(viz.EventClass):

	CLOSE_BUTTON_EVENT = viz.getEventID('CLOSE_BUTTON_EVENT')

	def __init__(self):
		viz.EventClass.__init__(self)
		self.addWindowMessage(win32con.WM_SYSCOMMAND)
		self.addWindowMessage(win32con.WM_KEYDOWN)
		self.callback(viz.WINDOW_EVENT,self._onWindowClose)
		self.callback(self.CLOSE_BUTTON_EVENT, self.onClose)

	def _onWindowClose(self,e):
		if e.wParam == win32con.SC_CLOSE or e.wParam == win32con.VK_ESCAPE:
			viz.postEvent(self.CLOSE_BUTTON_EVENT)
			return 1

	def onClose(self):
		if vizinput.ask('ソフトを終了しますか？',title='RiMMInspector'):
			viz.quit()
