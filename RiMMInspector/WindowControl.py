﻿# WindowControl
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/1
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Interface
import Window
import WindowLine

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓制御のクラス定義
class WindowControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,windowList,wallHeight):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowControl] : Init WindowControl')
		
		self._WallClass = None
		self._ModelControlClass = None
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._WindowList = windowList
		self._WallHeight = wallHeight
		
		self._ModelList = []
		self._LineModelList = []
		
		self._DefaultHeight  = 1.2
		
		self._WindowRef = vizshape.addBox([1,1,1])
		self._WindowRef.visible(viz.OFF)
		self._WindowRef.disable(viz.PICKING)
		
		self._LoadErrorState = False
		
	#使用するクラスの追加
	def AddClass(self,wallClass,modelControlClass):
		self._WallClass = wallClass
		self._ModelControlClass = modelControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowControl] : Start WindowControl Update')
		pass
		
	#iniファイルを基に窓を配置
	def SetWindow(self):
		for data in self._WindowList:
			num = self.Create(data[0],data[1],data[2],data[3])
			
		for window in self._ModelList:
			if window.GetLoadErrorState():
				self._LoadErrorState = True
				
	#追加
	def Add(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,self._DefaultHeight,wallNum,percentage]
		self._WindowList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,self._DefaultHeight,wallNum,percentage)
		
		return num,self._WindowList
		
	#ダミーを追加
	def AddDummy(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,self._DefaultHeight,wallNum,percentage]
		self._WindowList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,self._DefaultHeight,wallNum,percentage)
		
		window = self._ModelList[num]
		window.SetDummyFlag(True)
		windowLine = self._LineModelList[num]
		windowLine.SetDummyFlag(True)
		
		return num,self._WindowList
		
	#作成
	def Create(self,fileName,height,wallNum,percentage):
		#モデル
		window = Window.Window(self._ModelControlClass)
		window.Add(fileName)	#モデルを追加
		
		self._ModelList.append(window)
		
		#ラインモデル
		windowLine = WindowLine.WindowLine()
		windowLine.SetWallHeight(self._WallHeight)
		windowLine.Add(window)	#モデルを追加
		
		self._LineModelList.append(windowLine)
		
		#ID設定
		num = len(self._ModelList) - 1
		
		window.SetId(num)
		windowLine.SetId(num)
		
		#配置
		self.SetPosition(num,wallNum,percentage)
		
		return num
		
	#削除
	def Delete(self,num):
		#モデル
		model = self._ModelList[num]
		
		self._ModelList.remove(model)
		
		if model != None:
			model.Delete()	#モデルを削除
			del model
		
		#ラインモデル
		lineModel = self._LineModelList[num]
		
		self._LineModelList.remove(lineModel)
		
		if lineModel != None:
			lineModel.Delete()	#モデルを削除
			del lineModel
			
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
			lineModel = self._LineModelList[x]
			lineModel.SetId(num)
			
		#窓のリストを更新
		deletedWindow = self._WindowList[num]
		self._WindowList.remove(deletedWindow)
		
		return self._WindowList
		
	#全モデル削除
	def DeleteAll(self):
		#モデル
		for model in self._ModelList:
			model.Delete()	#モデルを削除
			del model
		
		self._ModelList = []
		
		#ラインモデル
		for lineModel in self._LineModelList:
			lineModel.Delete()	#モデルを削除
			del lineModel
		
		self._LineModelList = []
		
	#コピー
	def Copy(self,num,wallSize):
		window = self._WindowList[num]
		
		fileName = window[0]
		wallNum = window[2]
		percentage = window[3]
		
		#オフセット量を算出
		offsetVal=0.1
		if wallSize != 0.0:
			model = self._ModelList[num]
			size = model.GetSize()
			offsetVal = (size[0]+0.1)/wallSize
		
		newPercentage = percentage + offsetVal
		if newPercentage > 0.99:
			newPercentage = 0.99
		
		newNum,list = self.Add(fileName,wallNum,newPercentage)
		
		#高さ設定
		windowHeight = self.GetWindowHeight(num)
		self.SetWindowHeight(newNum,windowHeight)
		
		return self._WindowList
			
	#位置設定
	def SetPosition(self,num,wallNum,percentage):
		model = self._ModelList[num]
		lineModel = self._LineModelList[num]
		
		position = [0,0]
		orientation = 0
		
		if model != None and wallNum != None:	
			point0,point1 = self._WallClass.GetPoint(wallNum)
			position,orientation = self.GetModelPosition(model,point0,point1,percentage)
			
			window = self._WindowList[num]
			position[1] = window[1]
			
			#床の位置だけオフセット
			position [0] = position[0] + self._FloorPosition[0]
			position [1] = position[1] + self._FloorPosition[1]
			position [2] = position[2] + self._FloorPosition[2]
			
			#モデル
			model.SetOrientation(orientation)
			model.SetPosition(position,self._WindowRef)
	
			#ラインモデル
			lineModel.SetOrientation(orientation)
			lineModel.SetPosition(position)
			
	#高さ設定
	def SetHeight(self,num,height):
		self._WindowList[num][1] = height
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
	
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
		lineModel = self._LineModelList[num]
		lineModel.SetHighlight(state)
		
	#ハイライト状態の取得
	def GetHighlight(self,num):
		model = self._ModelList[num]
		state = model.GetHighlight()
		
		return state
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model or self._LineModelList[x]._Model == model:
				num =  x
		
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for x in range(len(self._ModelList)):
			#model = self._ModelList[x]
			#model.SetPickable(state)
			
			lineModel = self._LineModelList[x]
			lineModel.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#ラインモデル取得
	def GetLineModel(self,num):
		lineModel = self._LineModelList[num]
		
		return lineModel
		
	#モデルの位置取得
	def GetModelPosition(self,window,point0,point1,percentage):
		position = [0,0,0]
		orientation = 0
				
		position[0] = point0[0]*(1-percentage)+point1[0]*percentage
		position[1] = 0.0
		position[2] = point0[1]*(1-percentage)+point1[1]*percentage
		
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		
		orientation = wallAngle - 90.0
		
		return position,orientation
		
	#窓の表示状態を設定
	def SetVisible(self,state):
		for model in self._ModelList:
			model.SetVisible(state)
	
	#ラインの表示状態を設定
	def SetLineVisible(self,state):
		for lineModel in self._LineModelList:
			lineModel.SetVisible(state)
	
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
		for lineModel in self._LineModelList:
			lineModel.SetWallHeight(self._WallHeight)
			
	#壁のポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._WindowList)):
			data = self._WindowList[x]
			self.SetPosition(x,data[2],data[3])
			
	#リストを更新
	def UpdateList(self,num,state,height,wallNum,percentage):
		data = self._WindowList[num]
		
		if state == 0:		#全部更新
			data[1] = height
			data[2] = wallNum
			data[3] = percentage
		elif state == 1:	#高さのみ更新
			data[1] = height
		elif state == 2:	#壁上の位置のみ更新
			data[2] = wallNum
			data[3] = percentage
			
		self._WindowList[num] = data
		
		return self._WindowList
		
	#窓の高さを設定
	def SetWindowHeight(self,num,height):
		window = self._WindowList[num]
		window[1] = height
		self._WindowList[num] = window
		
		self.ReCreate()
		
		return self._WindowList
		
	#窓の高さを取得
	def GetWindowHeight(self,num):
		window = self._WindowList[num]
		height = window[1]
		
		return height
		
	#壁のポイントを再設定
	def SetPointList(self,pointList,windowList):
		self._PointList = pointList
		self._WindowList = windowList
		
		self.ReCreate()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		
	#窓の数を取得
	def GetCount(self):
		count = len(self._ModelList)
			
		return count
		
	#窓の情報を取得
	def GetState(self,num):
		data = self._WindowList[num]
			
		return data
		
	#窓モデルのリストを取得
	def GetWindowModelList(self):
		modelList = []
		
		for window in self._ModelList:
			model = window.GetModel()
			modelList.append(model)
			
		return modelList
		
	#IDを入れ替え
	def ChangeId(self,num):
		#print 'changeID',num
		newWindowNum = len(self._WindowList)-1
		
		#リストを更新
		newWindowList		= []
		newModelList		= []
		newLineModelList	= []
		
		for x in range(len(self._WindowList)):
			window		= self._WindowList[x]
			model		= self._ModelList[x]
			lineModel	= self._LineModelList[x]
			
			if x == num:
				newWindow = self._WindowList[newWindowNum]
				newWindowList.append(newWindow)
				newModel = self._ModelList[newWindowNum]
				newModelList.append(newModel)
				newLineModel = self._LineModelList[newWindowNum]
				newLineModelList.append(newLineModel)
				
			if x != newWindowNum:
				newWindowList.append(window)
				newModelList.append(model)
				newLineModelList.append(lineModel)
				
		self._WindowList	= newWindowList
		self._ModelList		= newModelList
		self._LineModelList	= newLineModelList
		
		return self._WindowList
		
	#読み込みに失敗したかどうかを取得
	def GetLoadErrorState(self):
		return self._LoadErrorState
		