﻿# Window
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/1
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d

from os import path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓制御のクラス定義
class Window(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,modelControlClass):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Window] : Init Window')
		
		self._ModelControlClass = modelControlClass
		
		self._FileName = ""
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		self._Texture = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		self._Size = [1,1,1]
		
		self._Id = None
		self._DummyFlag = False
		
		self._LoadErrorState = False
		
		self._HighlightState = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Window] : Start Window Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = self._ModelControlClass.Add(filePath)	#モデルを追加
		
		if path.exists(filePath) == False:
			#vizinput.message('ファイル読み込みエラー!\n間取りに設定されている窓の形状ファイルが見つかりません。'+fileName, title='RiMMInspector')
			self._LoadErrorState = True
			
		model.setPosition([0,0,0])
		model.setEuler([0,0,0])
		
		self._Model = model
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		self._Texture = self._Model.getTexture()
		
		#半透明モデルのオーダーを設定
		nodeNameList = self._Model.getNodeNames()
		for name in nodeNameList:
			if name[-2:] == '_A':
				self._Model.drawOrder(50,name)
		
		self.SetPositionOffset()
		self.SetSize()
		self.SetPickable(False)
		
		self._FileName = fileName
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,position,ref):
		if self._Model != None and position != None:
			refModel = ref
			
			refModel.setPosition(position,mode=viz.ABS_GLOBAL)
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = self._Orientation
			refModel.setEuler(eulerAngle)
			
			self._Model.setParent(refModel)
			self._Model.setPosition(self._PositionOffset,mode=viz.ABS_PARENT)
			globalPosition = self._Model.getPosition(mode=viz.ABS_GLOBAL)
			self._Model.setParent(viz.WORLD)
			self._Model.setPosition(globalPosition,mode=viz.ABS_GLOBAL)

			self._Position = position
			
	#オフセット設定
	def SetPositionOffset(self):
		if self._Model != None:
			self._PositionOffset = [0.0,0.0,0.0]
			offset = [0.0,0.0,0.0]
			
			box = self._Model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()

			#offset = boxCenter

			offset[0] = boxCenter[0] * -1
			offset[1] = (boxCenter[1] - boxSize[1] / 2) * -1
			offset[2] = boxCenter[2] * -1
			
			self._PositionOffset[0] = offset[0]
			self._PositionOffset[1] = offset[1]
			self._PositionOffset[2] = offset[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#位置のオフセット値取得
	def GetPositionOffset(self):
		positionOffset = self._PositionOffset
		return positionOffset
	
	#角度設定
	def SetOrientation(self,orientation):
		if self._Model != None and orientation != None:
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		if self._DummyFlag:
			self._Model.visible(False)
		else:
			self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		self._HighlightState = state
		
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.texture(None)
			self._Model.specular(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.texture(self._Texture)
			self._Model.specular(self._Specular)
			
	#ハイライト状態の取得
	def GetHighlight(self):
		return self._HighlightState
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#モデルのサイズを設定
	def SetSize(self):
		box = self._Model.getBoundingBox()
		self._Size =  box.size
		
	#モデルのサイズを取得
	def GetSize(self):
		size = self._Size
		
		return size
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
		
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		
	#モデルのファイル名を取得
	def GetFileName(self):
		name = self._FileName
		return name
		
	#読み込みに失敗したかどうかを取得
	def GetLoadErrorState(self):
		return self._LoadErrorState
		