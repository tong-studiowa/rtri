﻿# coding: utf-8

import viz
import vizfx
import Object3d
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ライト制御のクラス定義
class Light(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,category,model):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Light] : Init Light')
		
		self._Light = None
		self._LightTargetModel = model
		
		self._LightCategory = category		#0:平行、1:点、2:スポット
		self._LightIntensity = 0.5			#照度
		self._LightAttenuation = 0.05		#減衰
		self._LightSpread = 30.0			#広がり
		
		self._State = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Light] : Start Light Update')
		pass
		
	#追加
	def Add(self):
		if self._Light != None:
			self.Delete()
			
		if self._LightCategory == 1:						#点光源
			#print 'addPointLight'
			#self._Light = vizfx.addPointLight()
			self._Light = vizfx.addPointLight(shadow=viz.SHADOW_DEPTH_MAP)
			
		elif self._LightCategory == 2:						#スポットライト
			#print 'addSpotLight'
			#self._Light = vizfx.addSpotLight()
			self._Light = vizfx.addSpotLight(shadow=viz.SHADOW_DEPTH_MAP)
			self._Light.spotexponent(2)
			
		else:												#平行光源
			#print 'addDirectionalLight'
			self._Light = vizfx.addDirectionalLight()
			
		self._Light.setPosition(self._LightTargetModel.getPosition(viz.ABS_GLOBAL))
		self.SetData()
		
	#削除
	def Delete(self):
		if self._Light != None:
			self._Light.remove()	#ライトを削除
			del self._Light
			
		self._Light = None
		
	#ライトを取得
	def GetLight(self):
		return self._Light
		
	#各パラメータを設定
	def SetData(self):
		self._Light.intensity(self._LightIntensity)
		
		if self._LightCategory != 0:
			self._Light.constantattenuation(self._LightAttenuation)
			self._Light.linearattenuation(self._LightAttenuation)
			self._Light.quadraticattenuation(self._LightAttenuation)
			
		if self._LightCategory == 2:
			self._Light.spread(self._LightSpread * 0.5)
			
	#各パラメータを設定
	def GetData(self):
		return self._LightCategory,self._LightIntensity,self._LightAttenuation,self._LightSpread
		
	#照度を設定
	def SetIntensity(self,intensity):
		self._LightIntensity = intensity
		
		if self._Light != None:
			self.SetData()
			
	#減衰を設定
	def SetAttenuation(self,attenuation):
		self._LightAttenuation = attenuation
		
		if self._Light != None:
			self.SetData()
			
	#広がりを設定
	def SetSpread(self,spread):
		self._LightSpread = spread
		
		if self._Light != None:
			self.SetData()
			
	#アクティブ状態を設定
	def SetState(self,state):
		if state == True and self._State == False:
			self._State = True
			if self._Light == None:
				self.Add()
				
		if state == False and self._State == True:
			self._State = False
			if self._Light != None:
				self.Delete()
			