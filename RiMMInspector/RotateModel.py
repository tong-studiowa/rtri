﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム、キャラクター回転制御のクラス定義
class RotateModel(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateItem] : Init RotateItem')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._ManipulatorClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._SelectedModel = None
		self._SelectedCharacter = None
		
		self._RotateVal = 0.0
		self._AngleVal = 0.0
		self._PreAngleVal = 0.0
		
		self._State = False
		self._PreState = False
		
		self._UndoData = []
		
		self._Dummy = vizshape.addBox([0.01,0.01,0.01])
		self._Dummy.visible(viz.OFF)
		self._Dummy.disable(viz.PICKING)

	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,characterControlClass,selectModelClass,manipulatorClass
				,undoClass,redoClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._ManipulatorClass = manipulatorClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RotateItem] : Start RotateItem Update')
		
		self._State = False
		
		#マニピュレータの状態を取得
		manipulatorState	= self._ManipulatorClass.GetState()
		selectedRoomList	= self._SelectModelClass.GetSelectedRoomList()
		selectedDoorList	= self._SelectModelClass.GetSelectedDoorList()
		selectedWindowList	= self._SelectModelClass.GetSelectedWindowList()
		
		if manipulatorState == 2 and selectedRoomList == [] and selectedDoorList == [] and selectedWindowList == []:
			self._SelectedModel = self._SelectModelClass.GetSelectedModel()
			self._SelectedCharacter = self._SelectModelClass.GetSelectedCharacter()
			
			if self._SelectedModel != None or self._SelectedCharacter != None:
				self._State = True
				
				if self._State == True and self._PreState == False:
					self._UndoData = []
					self._UndoData.append(37)
					
				self.Rotate()
					
		else:
			self._RotateVal = 0.0
			
		if self._State == False and self._PreState == True:
			self.ResetOrientationSnap()
			
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
		self._PreState = self._State
		
	#回転
	def Rotate(self):
		selectedDummy = self._SelectModelClass.GetDummyModel()
		modelPos = selectedDummy.getPosition()
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		posX = modelPos[0] - pickingPosition[0]
		posZ = modelPos[2] - pickingPosition[2]
		
		angleTemp = math.atan2(posX,posZ)
		angle = math.degrees(angleTemp)
		
		self._AngleVal = angle
		
		rotateValTemp = self._AngleVal - self._PreAngleVal
		if rotateValTemp > -10.0 and rotateValTemp < 10:
			self._RotateVal = self._AngleVal - self._PreAngleVal
		
		modelAngle = 0.0
		newModelAngle = 0.0
		
		if self._SelectedModel != None:
			modelAngle = self._ItemControlClass.GetOrientation(self._SelectedModel)
		else:
			modelAngle = self._CharacterControlClass.GetOrientation(self._SelectedCharacter)
		
		newModelAngle = modelAngle + self._RotateVal

		if newModelAngle < 0:
			newModelAngle = newModelAngle + 360
		elif newModelAngle > 360:
			newModelAngle = newModelAngle - 360
		
		self._PreAngleVal = angle
		
		#複数選択時
		mainItem = None
		mainCharacter = None
		mainModel = None
		
		if self._SelectedModel != None:
			mainItem = self._ItemControlClass.GetModel(self._SelectedModel)
			mainModel = mainItem.GetModel()
		else:
			mainCharacter = self._CharacterControlClass.GetModel(self._SelectedCharacter)
			mainModel = mainCharacter.GetModel()

		self._Dummy.setParent(selectedDummy)
		
		#アイテム
		category = 0
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		for x in range(len(selectedItemList)):
			itemNum = selectedItemList[x]
			
			#位置取得
			self._SelectModelClass.SetDummyOrientation(modelAngle)
			
			item = self._ItemControlClass.GetModel(itemNum)
			model = item.GetModel()
			pos = model.getPosition(viz.ABS_GLOBAL)
			self._Dummy.setPosition(pos,viz.ABS_GLOBAL)
	
			self._SelectModelClass.SetDummyOrientation(newModelAngle)
		
			#角度設定
			modelAngle2 = self._ItemControlClass.GetOrientation(itemNum)
			newModelAngle2 = modelAngle2 + self._RotateVal
			
			if newModelAngle2 < 0:
				newModelAngle2 = newModelAngle2 + 360
			elif newModelAngle2 > 360:
				newModelAngle2 = newModelAngle2 - 360
				
			angle = self._ItemControlClass.GetAxisAngle(itemNum)
			
			self._ItemControlClass.SetOrientation(itemNum,newModelAngle2)
			
			#位置設定
			pos_dummy = self._Dummy.getPosition(viz.ABS_GLOBAL)
			self._ItemControlClass.SetPosition(itemNum,pos_dummy)
			
			if self._State == True and self._PreState == False:
				self._UndoData.append([category,itemNum,pos,angle])
				
		#キャラクター
		category = 1
		selectedCharacterList = self._SelectModelClass.GetSelectedCharacterList()
		for x in range(len(selectedCharacterList)):
			characterNum = selectedCharacterList[x]
			
			#位置取得
			self._SelectModelClass.SetDummyOrientation(modelAngle)
			
			character = self._CharacterControlClass.GetModel(characterNum)
			model = character.GetModel()
			pos = model.getPosition(viz.ABS_GLOBAL)
			self._Dummy.setPosition(pos,viz.ABS_GLOBAL)
	
			self._SelectModelClass.SetDummyOrientation(newModelAngle)
		
			#角度設定
			modelAngle2 = self._CharacterControlClass.GetOrientation(characterNum)
			newModelAngle2 = modelAngle2 + self._RotateVal
			
			if newModelAngle2 < 0:
				newModelAngle2 = newModelAngle2 + 360
			elif newModelAngle2 > 360:
				newModelAngle2 = newModelAngle2 - 360
				
			angle = self._CharacterControlClass.GetAxisAngle(characterNum)
			
			self._CharacterControlClass.SetOrientation(characterNum,newModelAngle2)
			
			#位置設定
			pos_dummy = self._Dummy.getPosition(viz.ABS_GLOBAL)
			self._CharacterControlClass.SetPosition(characterNum,pos_dummy)
			
			if self._State == True and self._PreState == False:
				self._UndoData.append([category,characterNum,pos,angle])
				
		self._Dummy.setParent(viz.WORLD)
		
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		
		for x in range(len(selectedItemList)):
			itemNum = selectedItemList[x]
			self._ItemControlClass.ResetOrientationSnap(itemNum)
			
		selectedCharacterList = self._SelectModelClass.GetSelectedCharacterList()
		
		for x in range(len(selectedCharacterList)):
			charaNum = selectedCharacterList[x]
			self._CharacterControlClass.ResetOrientationSnap(charaNum)
			
		self._SelectModelClass.SetDummyOrientation(0.0)
		
	#回転状態の確認
	def GetState(self):
		state = self._State
		return state
		