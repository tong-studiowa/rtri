﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Object2d

import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面設定ボタン制御のクラス定義
class SetDrawingButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDrawingButton] : Init SetDrawingButton')
		
		self._InputClass = None
		self._DrawingClass = None
		self._SetDrawingScaleButtonClass = None
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		
		self._PreKeyState = False
		self._PreForcus = False
		
		self._IconCount = 3
		
		self._Frame = None
		self._TextList = []
		self._IconList = []
		
		self._Filter = [('All Files (JPG,BMP,TGA,PNG,TIFF)','*.jpg;*.jpeg;*.bmp;*.tga;*.png;*.tif')
					   ,('JPG Files','*.jpg;*.jpeg'),('BMP Files','*.bmp')
					   ,('TGA Files','*.tga')
					   ,('PNG Files','*.png')
					   ,('TIFF Files','*.tif')]
					   
		self._Directory = self.GetRootPath() + 'Resource\\Drawing\\.'

		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [819,windowSize[1]-148]
		self._FrameScale = [210,168]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#テキスト追加
		self._TextPositionX = 819
		self._TextPositionY = [windowSize[1]-167,windowSize[1]-127, windowSize[1]-87]
		self._TextScale = [18.6,18.6]
		self._TextMessage = ["寸法設定","読み込み","削除"]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			message = self._TextMessage[x]
			
			text = self.CreateText(message,position,self._TextScale)
			self._TextList.append(text)
			
		#アイコン追加
		self._IconPositionX = 819
		self._IconPositionY = [windowSize[1]-167,windowSize[1]-127, windowSize[1]-87]
		self._IconScale = [200,36]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._IconPositionX
			position[1] = self._IconPositionY[x]
			
			icon = self.CreateIcon(position,self._IconScale)
			self._IconList.append(icon)
			
		#タイトル追加
		self._Title = None
		
		self._TitlePosition = [752,windowSize[1]-208]
		self._TitleScale = [18.6,18.6]
		
		self._Title = self.CreateText("横幅",self._TitlePosition,self._TitleScale)
		
		#テキストボックス追加
		self._TextBox = None
		self._Value = 1.0
		
		self._TextBoxPosition = [853,windowSize[1]-208]
		self._TextBoxScale = [132,36]
		
		self._TextBox = self.CreateTextBox(self._TextBoxPosition,self._TextBoxScale)
		self.SetText()
		
		#ダミーテキスト追加
		self._DummyText = None
		
		self._DummyTextPosition = [795,windowSize[1]-208]
		self._DummyTextScale = [13,27]
		
		self._DummyText = self.CreateDummyText('',self._DummyTextPosition,self._DummyTextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,drawingClass,setDrawingScaleButtonClass):
		self._InputClass = inputClass
		self._DrawingClass = drawingClass
		self._SetDrawingScaleButtonClass = setDrawingScaleButtonClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDrawingButton] : Start SetDrawingButton Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				num = None
				for x in range(self._IconCount):
					if self._IconList[x].getVisible():
						if self.CheckRollOver(mousePosition,[self._IconPositionX,self._IconPositionY[x]],self._IconScale):
							num = x
							
				if num != None:
					self._OverIconState = num
					
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.SetDrawingScale()	#図面のサイズを設定
						elif num == 1:
							self.LoadDrawing()		#図面を追加
						elif num == 2:
							self.DeleteDrawing()	#図面を削除
						
			#ロールオーバー
			self.Rollover(self._OverIconState)
			
		self.InputVal()
		
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
			self.SetValue(self._DrawingClass.GetScale())
			
			#表示したらフォーカス
			#self.SetTextBoxForcus()
			
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		
		for text in self._TextList:
			text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
		
		self._Title.visible(val)
		self._TextBox.visible(val)
		self._DummyText.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		return state
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#数値を設定
	def SetValue(self,val):
		self._Value = val
		self.SetText()
		
	#数値を取得
	def GetValue(self):
		val = self._Value
		
		return val
		
	#数値の表示を更新
	def SetText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(val)
		self._TextBox.message(message)
	
	#ダミーテキストを表示
	def SetDummyText(self):
		val = self._Value * 1000.0	#表示する時はメートル → ミリメートル
		message = str(round(val,3))
		self._DummyText.message(message)
	
		self._TextBox.message('')
		
	#文字入力
	def InputVal(self):
		keyState = False
		forcus = False
		
		if self._VisibleState == True:
			keyState = False
			if viz.key.isDown(viz.KEY_RETURN):
				keyState = True
			if viz.key.isDown(viz.KEY_KP_ENTER):
				keyState = True
				
			if keyState:
				inputVal = self._TextBox.getMessage()
				preVal = self.GetValue()
				val = 1.0
				
				try:
					val = float(inputVal) * 0.001	#入力時はミリメートル → メートル
				except:
					val = preVal
				
				if val != preVal:
					self.SetValue(val)
					self._DrawingClass.SetScale(val)
					
			forcus = self.GetTextBoxForcus()
			if forcus != self._PreForcus and keyState == 0:
				preVal = self.GetValue()
				self.SetValue(preVal)
				
				if forcus:
					self.SetDummyText()
					
			inputValTemp = self._TextBox.getMessage()
			if len(inputValTemp) != 0:
				self._DummyText.message('')
				
		self._PreKeyState = keyState
		self._PreForcus = self.GetTextBoxForcus()
		
	#図面のサイズを設定
	def SetDrawingScale(self):
		self._SetDrawingScaleButtonClass.SetVisible(True)
		
		self.SetVisible(False)
		
	#図面を読み込む
	def LoadDrawing(self):
		filePath = vizinput.fileOpen(filter=self._Filter,file=self._Directory)
		try:
			filePath.encode("shift_jis")
		except:
			vizinput.message("ファイルパスに機種依存文字が含まれています。")
			filePath = ""
		
		if filePath != "":
			#相対パスに変換
			rootPath = self.GetRootPath()
			rootCount = len(rootPath)
			fileCount = len(filePath)
			if rootPath == filePath[:rootCount]:
				filePath = '..\\' + filePath[-(fileCount-rootCount):]
			
			#図面の情報を渡す
			loadStaet = self._DrawingClass.Add(filePath)
			#if loadStaet == True:
			#	self._SetDrawingScaleButtonClass.SetVisible(True)	#読み込めたらサイズ設定へ移行
			
		#self.SetVisible(False)
		
	#図面を削除
	def DeleteDrawing(self):
		self._DrawingClass.Delete()
		
		self.SetVisible(False)
			
	#ロールオーバー処理
	def Rollover(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
			
	#テキストボックスのフォーカスを設定
	def SetTextBoxForcus(self):
		self.SetDummyText()
		self._TextBox.setFocus(viz.ON)
			
	def GetTextBoxForcus(self):
		state = self._TextBox.getFocus()
		return state
		
	#ルートフォルダの取得
	def GetRootPath(self):
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
			exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
			
		return rootDir
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [819,windowSize[1]-148]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TextPositionY = [windowSize[1]-167,windowSize[1]-127, windowSize[1]-87]
		self._IconPositionY = [windowSize[1]-167,windowSize[1]-127, windowSize[1]-87]
		
		for x in range(self._IconCount):
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			self.SetIconPosition(self._TextList[x],position,self._TextScale)
			self.SetIconPosition(self._IconList[x],position,self._IconScale)
			
		self._TitlePosition = [752,windowSize[1]-208]
		self.SetIconPosition(self._Title,self._TitlePosition,self._TitleScale)
		
		self._TextBoxPosition = [853,windowSize[1]-208]
		self.SetIconPosition(self._TextBox,self._TextBoxPosition,self._TextBoxScale,True)
		
		self._DummyTextPosition = [795,windowSize[1]-208]
		self.SetIconPosition(self._DummyText,self._DummyTextPosition,self._DummyTextScale)
		