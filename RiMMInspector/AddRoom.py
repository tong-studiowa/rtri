﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import Floor
import WallLine
import WallPoint
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋追加時のドラッグ用モデル制御のクラス定義
class AddRoom(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddModel] : Init AddRoom')
		
		self._RoomControlClass = None
		
		self._Picking = False
		self._RoomIniFile = None
		
		#部屋の設定
		self._Name = ""
		self._Position = [0.0,0.0,0.0]
		self._Angle = 0.0
		self._WallHeight = 3.0
		
		#形状関係
		self._FloorPointList = []
		
		#床関係
		self._FloorTextureFile = ""
		self._FloorTextureSize = 1.0
		
		self._FloorClass = None
		
		#壁関係
		self._WallVisibleList = []
		
		self._WallLineClass = None
		self._WallPointClass = None
		
		self._TranslateSnap = translateSnap
		
		self._IsOpRoom = 0
		
	#使用するクラスの追加
	def AddClass(self,roomControlClass):
		self._RoomControlClass = roomControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddRoom] : Start AddRoom Update')
		pass
	
	#追加
	def Add(self,fileName):
		if self._RoomIniFile != None:
			self.Delete()
		
		self.LoadIniFile(fileName)
		roomName = self._RoomIniFile.GetItemData('Rooms','0')
		
		self._Name = roomName
		
		self.LoadIniFileValue()
		
		floorTex,WallTex,CeilingTex,WallHeight = self._RoomControlClass.GetDefaultSettings()
		if floorTex != None and floorTex != '':
			self._FloorTextureFile = floorTex
				
		self._FloorClass = Floor.Floor(self._Position,self._FloorPointList,self._FloorTextureFile,self._FloorTextureSize)
		self._WallLineClass = WallLine.WallLine(self._Position,self._FloorPointList,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		self._WallPointClass = WallPoint.WallPoint(self._Position,self._FloorPointList,self._WallHeight,self._IsOpRoom)

		self.SetPickable(False)
		self.SetHighlight(True)
		
	#削除
	def Delete(self):
		if self._RoomIniFile != None:
			self._FloorClass.Delete()		#床モデルを削除
			self._WallLineClass.Delete()	#壁ラインモデルを削除
			self._WallPointClass.Delete()	#壁頂点モデルを削除
			del self._RoomIniFile
			
		self._RoomIniFile = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._RoomIniFile != None and iPosition != None:
			self._Position = iPosition
			
			#スナップ
			self._Position[0] = self.Snap(self._Position[0])
			self._Position[2] = self.Snap(self._Position[2])
			
			self._Position[1] = 0.0
		
			self._FloorClass.SetFloorPosition(self._Position)
			self._WallLineClass.SetFloorPosition(self._Position)
			self._WallPointClass.SetFloorPosition(self._Position)
	
	#位置取得
	def GetPosition(self):
		position = [0,0,0]
		
		if self._RoomIniFile != None:
			position = self._Position
		
		return position
	
	#ハイライト表示
	def SetHighlight(self,state):
		if self._WallLineClass != None and self._WallPointClass != None:
			for x in range(len(self._FloorPointList)):
				self._WallLineClass.SetHighlight(x,state)
				self._WallPointClass.SetHighlight(x,state)
		
	#選択可能かの設定
	def SetPickable(self,state):
		self._FloorClass.SetPickable(state)
			
	#設定ファイルの読み込み
	def LoadIniFileValue(self):
		#全体のパラメータ設定
		positionTemp = self._RoomIniFile.GetItemData(self._Name,'Position')
		self._Position[0] = float(positionTemp.split(',')[0])
		self._Position[1] = float(positionTemp.split(',')[1])
		self._Position[2] = float(positionTemp.split(',')[2])
		
		self._Angle = float(self._RoomIniFile.GetItemData(self._Name,'Angle'))
		
		self._FloorPointList = []
		self._WallVisibleList = []
		for x in range(100):
			pointName = "Point"+str(x)
			pointTemp = self._RoomIniFile.GetItemData(self._Name,pointName)
			if pointTemp != None:
				pointX = float(pointTemp.split(',')[0])
				pointY = float(pointTemp.split(',')[1])
				point = [pointX,pointY]
				self._FloorPointList.append(point)
				
				visibleState = True
				if len(pointTemp.split(',')) == 3:
					visibleStateTemp = int(pointTemp.split(',')[2])
					if visibleStateTemp == 0:
						visibleState = False
					elif visibleStateTemp == 1:
						visibleState = True
						
				self._WallVisibleList.append(visibleState)
				
			else:
				break
				
		self._WallHeight = float(self._RoomIniFile.GetItemData(self._Name,'WallHeight'))
		
		#床関係のパラメータ設定
		floorTextureTemp = self._RoomIniFile.GetItemData(self._Name,'FloorTexture')
		self._FloorTextureFile = floorTextureTemp.split(',')[0]
		self._FloorTextureSize = float(floorTextureTemp.split(',')[1])
	
		#オペ室かどうのパラメータ設定
		opRoomStateTemp = self._RoomIniFile.GetItemData(self._Name,'OpRoom')
		self._IsOpRoom = 0
		if opRoomStateTemp != None and opRoomStateTemp != '':
			self._IsOpRoom = int(opRoomStateTemp)
		
	#設定ファイルの読み込み
	def LoadIniFile(self,fileName):
		self._RoomIniFileName = fileName
		
		#iniファイルの読み込み
		self._RoomIniFile = LoadIniFile.LoadIniFileClass()
		self._RoomIniFile.LoadIniFile(viz.res.getPublishedPath(self._RoomIniFileName))
		
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
