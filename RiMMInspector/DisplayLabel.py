﻿# coding: utf-8
#PathSimulator

import viz

class DisplayLabel():
	def __init__(self):
		#画像の表示用オブジェクトを作成する
		self._mObjectText = viz.addTextbox()
		self._mObjectText.disable(viz.PICKING)
		self._mTextSize = [1.0,1.0]	#画像の表示用オブジェクトのサイズ(pixel)
		self._mTextLine = 1
	
	def __del__(self):
		if self._mObjectText:
			self._mObjectText.remove()	#画像の表示用オブジェクトを削除
	
	def SetFont(self,font):
		self._mObjectText.font(font)
	
	def SetAlignment(self,alignment):
		self._mObjectText.alignment(alignment)
	
	def SetFontColor(self,color):
		try:
			self._mObjectText.color(color)
		except:
			try:
				list = color.split(',')
				r=int(list[0])
				g=int(list[1])
				b=int(list[2])
				self._mObjectText.color(r,g,b)
			except:
				pass
	
	def SetRenderToAllWindowsExcept(self,list):
		if self._mObjectText:
			self._mObjectText.renderToAllWindowsExcept(list)
	
	def SetText(self,text):
		self._mTextLine = 1
		if text=='' or text==None:
			self._mObjectText.message(str(''))
		if self._mObjectText:
			try:
				list = text.split('\\n')
				newtext=''
				for listtext in list:
					newtext = newtext + listtext + '\n'
					self._mTextLine = self._mTextLine + 1
				self._mObjectText.message(str(newtext))
			except:
				self._mObjectText.message(str(text))
	
	def GetText(self):
		if self._mObjectText:
			return self._mObjectText.getMessage()
		return ''
	
	def SetPosition(self,x,y):
	#Windowのサイズを取得
		WINDOW_SIZE = viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		#オブジェクトの表示位置を設定する
		#　---Vizardは左下が(0,0)なので注意
		if self._mObjectText:
			self._mObjectText.setPosition(x/WINDOW_SIZE[0],1-y/WINDOW_SIZE[1])
	
	def SetSize(self,size):
		self._mObjectText.fontSize(size)
	
	def GetTextLineNum(self):
		return self._mTextLine
	
	def _ResetSize(self):
		SizeX = 1.0/self._mTextSize[0]	#現在のサイズから、1pixel*1pixelに戻す
		SizeY = 1.0/self._mTextSize[1]
		if self._mObjectText:
			self._mObjectText.setScale(SizeX,SizeY)
	
	def SetRenderOrder(self,orderVal):
	#レンダーオーダーを設定する
	#　---高い値になるほど、後に描画される（2Dモデルを最前面に表示したければこの値を高くする）
		if self._mObjectText:
			self._mObjectText.drawOrder(orderVal)
	
	def SetVisible(self,isVisible):
		if self._mObjectText:
			if isVisible:
				self._mObjectText.enable(viz.RENDERING)
			else:
				self._mObjectText.disable(viz.RENDERING)
	
	def GetModel(self):
		return self._mObjectText
