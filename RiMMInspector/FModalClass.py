﻿# FModalClass
# Develop       : Python v2.7.2 / Vizard4.05.0095
# LastUpdate    : 2012/11/22
# Programer     : nishida

import viz
import FBaseClass

class FModalClass(FBaseClass.FBaseClass):

# member
	mMode = None

# method
	# called method at first
	def __init__(self):
		self.SetMode(0)

	# called method at delete
	def __del__(self):
		pass

	# set mode
	def SetMode(self,mode):
		self.mMode = mode

	# return mode
	def GetMode(self):
		return self.mMode
