﻿# GetCameraFovFunction
# Develop       : Python v2.7.2 / Vizard4.06.0138
# LastUpdate    : 2013/02/07
# Programer     : nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import math

# - - - - - - - - - - - - - - - - - - - - - - - - -
#関数の定義

#香川大向けカメラの画角設定用関数 （視点からスクリーンまでの距離（m）を入力し、カメラの画角を出力）
def GetCameraFovFunction(screensize,distance):		
	#SCREEN_SIZE = 1.628		#香川大で設置されているスクリーンの横幅（m） ← 定数
	
	fov = math.atan2(screensize/2,distance)*180/3.14*2		#カメラの画角を算出
	#print fov
	
	return fov

import LoadIniFile
def GetCameraFov(path):
	
	#設定ファイルの読み込み
	iniFile = LoadIniFile.LoadIniFileClass()
	iniFile.LoadIniFile(viz.res.getPublishedPath(path))
	
	#画面サイズの取得＆設定
	DispSizeTemp1	= iniFile.GetItemData('Setting','DispSize')
	DispSizeTemp2	= DispSizeTemp1.split(',')
	DispSize		= [float(DispSizeTemp2[0]),float(DispSizeTemp2[1])]
	
	"""
	ScreenSizeTemp	= iniFile.GetItemData('Screen','screensize_wide')
	ScreenSize		= float(ScreenSizeTemp)
	DistanceTemp	= iniFile.GetItemData('Screen','distance')
	Distance		= float(DistanceTemp)
	"""
	ScreenSize	= 1.4
	Distance	= 1.0
	
	fov   = GetCameraFovFunction(ScreenSize,Distance)
	ratio = float(DispSize[0])/DispSize[1]
	
	#return fov,ratio
	return fov,-1