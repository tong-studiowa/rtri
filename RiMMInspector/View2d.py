﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizact
import vizshape
import vizmat

import Object3d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#2D視点制御のクラス定義
class View2d(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][View2d] : Init View2d')
		
		self._DispSize = viz.window.getSize()
		self._AspectRatio = float(self._DispSize[1]) / float(self._DispSize[0])
		
		self._InputClass = None
		self._SelectModelClass = None
		self._StencilClass = None
		self._TranslateModelClass = None
		
		self._TranslateSpeed = 10.0
		self._ZoomSpeed = 10.0
		
		self._ZoomMin = 0.1
		self._ZoomMax = 500
		
		#平行投影用ウィンドウの作成
		self._OrthoViewWindow = viz.addWindow()
		self._OrthoViewWindow.setPosition(0,1)
		self._OrthoViewWindow.setSize(1,1)
		self._OrthoViewWindow.clearcolor(viz.WHITE)
		
		self._OrthoViewPosXY	= [0,0]
		self._OrthoViewPosZ		= [-100,100]
		self._OrthoViewScale	= 10
		
		#self._OrthoViewWindow.ortho(-16,16,-9,9,-10,100)
		self.SetOrthoView()
		
	#使用するクラスの追加
	def AddClass(self,inputClass,selectModelClass,stencilClass,translateModelClass):
		self._InputClass = inputClass
		self._SelectModelClass = selectModelClass
		self._StencilClass = stencilClass
		self._TranslateModelClass = translateModelClass
		
		self.SetOrthoViewWindow()
		self.Reset()
		
	#毎フレーム呼び出されるための関数	
	def Update(self,clickState):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][View2d] : Start View2d Update')
		
		if self._InputClass.GetAltKeyState():
			
			#マウス左ボタン操作（回転）
			#if self._InputClass.GetShiftKeyState() == False and self._InputClass.GetMouseDragState() and clickState == False:
			#	self.Spin(self._InputClass.GetMouseDisplacement())
			
			#マウス中ボタン操作（移動）
			if self._InputClass.GetMouseMiddleButtonState():
				self.Translate(self._InputClass.GetMouseDisplacement())
				
			#マウス右ボタン操作（ズーム）
			if self._InputClass.GetMouseRightButtonState():
				zoomVal = self._InputClass.GetMouseDisplacement()[1] * -10
				self.Zoom(zoomVal)
			
		#マウスホイール操作
		if self._StencilClass.GetOverState() == False:
			mouseWheelState = self._InputClass.GetMouseWheelState()
			if mouseWheelState != 0:
				self.Zoom(mouseWheelState)
		
	#リセットするための関数
	def Reset(self):
		self.SetAspectRatio()
		
	#移動
	def Translate(self,displacementVal):
		moveValX = displacementVal[0] * self._TranslateSpeed * self._OrthoViewScale * -0.1
		moveValZ = displacementVal[1] * self._TranslateSpeed * self._OrthoViewScale * -0.1
		
		newPos = [0,0]
		newPos[0] = self._OrthoViewPosXY[0] + moveValX
		newPos[1] = self._OrthoViewPosXY[1] + moveValZ
		
		self._OrthoViewPosXY = newPos
		self.SetOrthoView()
		
	#ズーム
	def Zoom(self,wheelVal):
		scale = self._OrthoViewScale + (self._ZoomSpeed * wheelVal * self._OrthoViewScale * -0.01)
		
		#サイズ制限
		if scale < self._ZoomMin or scale == self._ZoomMin:
			scale = self._ZoomMin
		elif scale > self._ZoomMax or scale == self._ZoomMax:
			scale = self._ZoomMax
		
		self._OrthoViewScale = scale
		self.SetOrthoView()
		
	#視点の位置を設定
	def SetViewPos(self,pos):
		newPosXY = [0.0,0.0]
		newPosXY[0] = pos[0]
		newPosXY[1] = pos[2]
		
		self._OrthoViewPosXY = newPosXY
		
	#視点の位置を取得
	def GetViewPos(self):
		pos = [0.0,0.0,0.0]
		pos[0] = self._OrthoViewPosXY[0]
		pos[2] = self._OrthoViewPosXY[1]
		
		return pos
		
	#視点のスケールを取得
	def GetViewScale(self):
		scale = self._OrthoViewScale
		
		return scale
		
	#ウィンドウの表示状態を設定
	def SetWindowVisible(self,state):
		if state ==True:
			self._OrthoViewWindow.visible(viz.ON)
		else:
			self._OrthoViewWindow.visible(viz.OFF)
			
	#Inputクラスのウィンドウを設定
	def SetOrthoViewWindow(self):
		self._InputClass.SetOrthoViewWindow(self._OrthoViewWindow)
		self._SelectModelClass.SetOrthoViewWindow(self._OrthoViewWindow)
		self._TranslateModelClass.SetOrthoViewWindow(self._OrthoViewWindow)
		
	#画面を設定
	def SetOrthoView(self):
		pos = [0,0,0]
		pos[0] = self._OrthoViewPosXY[0]
		pos[2] = self._OrthoViewPosXY[1]
		viz.MainView.setPosition(pos)
		viz.MainView.setEuler(0,90,0)
		
		posX = self._OrthoViewScale
		posY = self._OrthoViewScale * self._AspectRatio
		posZmin = self._OrthoViewPosZ[0]
		posZmax = self._OrthoViewPosZ[1]
		
		self._OrthoViewWindow.ortho(-posX,posX,-posY,posY,posZmin,posZmax)
		
	#画角を設定
	def SetAspectRatio(self):
		self._DispSize = viz.window.getSize()
		self._AspectRatio = float(self._DispSize[1]) / float(self._DispSize[0])
		
		self.SetOrthoView()
		