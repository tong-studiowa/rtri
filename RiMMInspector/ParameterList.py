﻿import viz
from os import path

#ローダーの明示
viz.res.addPublishFileLoader('osg')
viz.res.addPublishFileLoader('osgb')
viz.res.addPublishFileLoader('wav')
viz.res.addPublishFileLoader('wmv')
viz.res.addPublishFileLoader('tga')
viz.res.addPublishFileLoader('ttc')

NONE	= ""
VR		= "VR"
MOVIE	= "Movie"
PHOTO	= "Photo"

#定数(State)
STOP	= 0
PLAY	= 1
PAUSE	= 2

ROOT_PATH = ""
MONITOR_NUM = 1

#環境モデル
SKY_MODEL_PATH		= viz.res.getPublishedPath("..\..\Resource\Models\sky.osgb")
SKY_TEXTURE_LIST	= [	viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_morning.tga"),
						viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_day.tga"),
						viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_yuyake.tga"),
						viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_night.tga")]

CLOUDY_SKY_TEXTURE_LIST	= [	viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_cloudy_morning.tga"),
							viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_cloudy_day.tga"),
							viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_cloudy_yuyake.tga"),
							viz.res.getPublishedPath("..\..\Resource\Textures\sky_tex_cloudy_night.tga")]

CLOUD_MODEL_PATH1	= viz.res.getPublishedPath("..\..\Resource\Models\\662_0001_cloud_0001.osgb")
CLOUD_MODEL_PATH2	= viz.res.getPublishedPath("..\..\Resource\Models\\662_0001_cloud_0001.osgb")
BASE_MODEL_PATH		= viz.res.getPublishedPath("..\..\Resource\Models\BasePlane.osg")

#コンテンツ再生用テクスチャ
BLACKTEXTURE_PATH	= viz.res.getPublishedPath('..\..\Resource\Textures\BlackTexture.jpg')

#定義ファイルパス
NETCONFIG_PATH	= viz.res.getPublishedPath("..\..\Settings\NetConfig.ini")
SETTINGS_PATH	= viz.res.getPublishedPath("ContentsPlayerSettings.ini")
STATE_PATH		= viz.res.getPublishedPath("..\..\Settings\StateList.ini")
BASE_PATH		= viz.res.getPublishedPath("..\..\Settings\Base.ini")
CAST_PATH		= viz.res.getPublishedPath("..\..\Settings\Cast.ini")
ITEMS_PATH		= viz.res.getPublishedPath("..\..\Settings\Items.ini")
QUEST_PATH		= viz.res.getPublishedPath("..\..\Settings\Quest.ini")

#FWindowsClass用のパラメーター
import win32gui
FONT_STYLE			= ""
rect = win32gui.GetWindowRect(win32gui.GetDesktopWindow())
#DISPLAY_SIZE_WIDTH = float(rect[2])-float(rect[0])
#DISPLAY_SIZE_HEIGHT = float(rect[3])-float(rect[1])
DISPLAY_SIZE_WIDTH	= 1024										#ディスプレイサイズｘ
DISPLAY_SIZE_HEIGHT = 768										#ディスプレイサイズｙ
DISPLAY_FULL_SCALE_WIDTH	= 12.8								#フルスクリーンにする際のスケール値ｘ
DISPLAY_FULL_SCALE_HEIGHT	= 10.24								#フルスクリーンにする際のスケール値ｙ
SCALE_VALUE_X = DISPLAY_FULL_SCALE_WIDTH/DISPLAY_SIZE_WIDTH		#スケール補正値ｘ
SCALE_VALUE_Y = DISPLAY_FULL_SCALE_HEIGHT/DISPLAY_SIZE_HEIGHT	#スケール補正値ｙ

#質問文GUI
QUEST_WIDTH		= int(DISPLAY_SIZE_WIDTH	*0.70)
QUEST_HEIGHT	= int(DISPLAY_SIZE_HEIGHT	*0.20)
QUEST_POS_X		= int(DISPLAY_SIZE_WIDTH	*0.15)
QUEST_POS_Y		= int(DISPLAY_SIZE_HEIGHT	*0.10)
QUEST_TEXT_SIZE_X		= int(DISPLAY_SIZE_WIDTH	*0.20)
QUEST_TEXT_SIZE_Y		= QUEST_TEXT_SIZE_X
QUEST_TEXT_POS_OFFSET_X	= int(DISPLAY_SIZE_WIDTH	*0.05)
QUEST_TEXT_POS_OFFSET_Y	= int(DISPLAY_SIZE_WIDTH	*0.05)

#選択肢GUI
SEL_STARTPOS_X	= int(DISPLAY_SIZE_WIDTH	*0.15)	#ボタンの配置開始位置（ｘ）
SEL_STARTPOS_Y	= int(DISPLAY_SIZE_HEIGHT	*0.40)	#ボタンの配置開始位置（ｙ）
SEL_WIDTH		= int(DISPLAY_SIZE_WIDTH	*0.20)	#ボタンの幅
SEL_HEIGHT		= int(DISPLAY_SIZE_HEIGHT	*0.15)	#ボタンの高さ
SEL_BLANK_X		= int(DISPLAY_SIZE_WIDTH	*0.05)	#ボタン間の距離（横）
SEL_BLANK_Y		= int(DISPLAY_SIZE_HEIGHT	*0.10)	#ボタン間の距離（上）
SEL_POS_X		= []
SEL_POS_Y		= []
SEL_POS_X.append(int(SEL_STARTPOS_X+(SEL_WIDTH +SEL_BLANK_X)*0))	#1列目のx座標
SEL_POS_Y.append(int(SEL_STARTPOS_Y+(SEL_HEIGHT+SEL_BLANK_Y)*0))	#1行目のｙ座標
SEL_POS_X.append(int(SEL_STARTPOS_X+(SEL_WIDTH +SEL_BLANK_X)*1))	#2列目のx座標
SEL_POS_Y.append(int(SEL_STARTPOS_Y+(SEL_HEIGHT+SEL_BLANK_Y)*1))	#2行目のｙ座標
SEL_POS_X.append(int(SEL_STARTPOS_X+(SEL_WIDTH +SEL_BLANK_X)*2))	#3列目のx座標
SEL_POS_Y.append(int(SEL_STARTPOS_Y+(SEL_HEIGHT+SEL_BLANK_Y)*2))	#3行目のｙ座標
SEL_TEXT_SIZE_X			= int(DISPLAY_SIZE_WIDTH	*0.15)
SEL_TEXT_SIZE_Y			= SEL_TEXT_SIZE_X
SEL_TEXT_POS_OFFSET_X	= int(DISPLAY_SIZE_WIDTH	*0.025)
SEL_TEXT_POS_OFFSET_Y	= int(DISPLAY_SIZE_HEIGHT	*0.06)

#制限時間GUI
LIMIT_WIDTH		= int(DISPLAY_SIZE_WIDTH	*0.20)
LIMIT_HEIGHT	= int(DISPLAY_SIZE_HEIGHT	*0.20)
LIMIT_POS_X		= int(DISPLAY_SIZE_WIDTH	*0.20)
LIMIT_POS_Y		= int(DISPLAY_SIZE_HEIGHT	*0.65)

#クエスト名
QUEST			= "QuestA"
QUEST_TEXTURE	= viz.res.getPublishedPath("..\..\Resource\Textures\Button_Base.tga")
SEQ1_TEXTURE	= viz.res.getPublishedPath("..\..\Resource\Textures\Button_Base.tga")
SEQ2_TEXTURE	= viz.res.getPublishedPath("..\..\Resource\Textures\Button_Base.tga")
SEQ3_TEXTURE	= viz.res.getPublishedPath("..\..\Resource\Textures\Button_Base.tga")

#教室モデル
SCHOOL_MODEL_PATH		= [	viz.res.getPublishedPath("..\..\Resource\Models\\681_0001_100m_school_0001.osgb"),
							viz.res.getPublishedPath("..\..\Resource\Models\\682_0001_school_normal_0001.osgb")]
CLASSROOM_MODEL_PATH	= [	viz.res.getPublishedPath("..\..\Resource\Models\\681_0001_100m_school_0001.osgb"),
							viz.res.getPublishedPath("..\..\Resource\Models\\682_0001_school_normal_0001.osgb")]
SCHOOL_ROOM_W			=  0.0	#7.692
SCHOOL_ROOM_H			=  0.0
SCHOOL_ROOM_D			=  0.0	#13.048
SCHOOL_ROOM_POS_X		=  0.0	# -0.0198427
SCHOOL_ROOM_POS_Y		=  0.0	# -0.00353637
SCHOOL_ROOM_POS_Z		=  0.0

#GUIのZオーダー
QUESTION_ZORDER		= 15
CLEAR_ZORDER		= 20
CONTENTS2D_ZORDER	= 10
EYE_POINT_ZORDER	= 30

#災害再現用データ
EARTHQUAKE_POWER			= 1
EARTHQUAKE_POWER_MAX		= 2
EARTHQUAKE_POWER_MIN		= 1
EARTHQUAKE_T_X				= 0.1
EARTHQUAKE_T_Y				= 0.2
MOVE_LIMIT					= 0.01
EARTHQUAKE_SOUND_PATH		= viz.res.getPublishedPath("..\..\Resource\Sounds\地震.wav")			#地震音
EARTHQUAKEALARM_SOUND_PATH	= viz.res.getPublishedPath("..\..\Resource\Sounds\緊急地震速報.wav")	#緊急地震速報
EARTHQUAKEALARM_SOUND_PATH2	= viz.res.getPublishedPath("..\..\Resource\Sounds\避難開始.wav")		#避難開始アナウンス
SMOKE_MODEL_PATH			= viz.res.getPublishedPath("..\..\Resource\Models\smoke_trail.osg")	#火災用煙モデル

TSUNAMI_MODEL	 = viz.res.getPublishedPath('..\..\Resource\Models\\662_0001_tsunami_0001.osgb')
POND_MODEL_LIST	 = ['611_0012_500m_mountain_pond_0001.osgb']
POND_WATER_MODEL1= '642_0004_pond_water_uv_anim_0001'
POND_WATER_MODEL2= '642_0003_pond_flood_uv_anim_0001'
PONDBLAKE_MODEL	 = viz.res.getPublishedPath('..\..\Resource\Block\\611_0013_500m_mountain_pond_broke_0001.osgb')	#20130201 19:00
ROAD_MODEL_LIST	 = ['697_0010_100m_town_0001.osgb']
ROADBLAKE_MODEL	 = viz.res.getPublishedPath('..\..\Resource\Pack\\697_0010_100m_town_crack_0001.osgb')
ROOF_MODEL_LIST	 = ["school_roof_01"]#["school_roof_01","school_roof_02"]

PARTICLE_FIRE		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_fire.osg')
PARTICLE_SMOKE		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_fire_smoke.osg')
PARTICLE_FIRE2		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_fire2.osg')
PARTICLE_SMOKE2		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_fire2_smoke.osg')
PARTICLE_DUST		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_dust.osg')
PARTICLE_EXPLODE	= viz.res.getPublishedPath('..\..\Resource\Particles\explosion.osgb')
PARTICLE_EXPLODE2	= viz.res.getPublishedPath('..\..\Resource\Particles\particle_smoke2.osg')
PARTICLE_SPLASH		= viz.res.getPublishedPath('..\..\Resource\Particles\\particle_splash_water.osg')