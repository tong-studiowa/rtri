﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat
import vizfx

import math
import Object2d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
CHECKBOX_TEXTURE_PATH = 'Resource\\Interface\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面の表示状態設定クラス定義
class SetFilter(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetFilter] : Init SetFilter')
		
		self._InputClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._StateList = []
		
		#フレーム追加
		self._Position = [226,99]
		self._PositionX = [226,63]
		self._Scale = [100,168]
		self._Frame = self.CreateBgFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._CheckBoxCount = 8
		
		self._TextPosition = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
		self._TextPositionY = [-70,-50,-30,-10,10,30,50,70]
		self._TextScale = [14,14]
		self._TextMessageList = ['間取り','柱','照明','エアコン','アイテム','キャラ','ドア','窓']
		self._TextList = []
		
		self._CheckBoxPosition = [[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]
		self._CheckBoxScale = [15,15]
		self._CheckBoxList = []
		
		self.SetViewState(False)
		
		#チェックボックス用テクスチャを設定
		self._CheckBoxTextureList = []
		self._CheckBoxTextureNameList = ['CheckOff','CheckOn']
		for x in range(2):
			tex = viz.addTexture(viz.res.getPublishedPath(CHECKBOX_TEXTURE_PATH+self._CheckBoxTextureNameList[x]+'.tga'))
			self._CheckBoxTextureList.append(tex)
		
		for x in range(self._CheckBoxCount):
			text = self.CreateText(self._TextMessageList[x],self._TextPosition[x],self._TextScale)
			self._TextList.append(text)
			
			checkBox = self.CreateCheckBox(self._CheckBoxPosition[x],self._CheckBoxScale,self._CheckBoxTextureList[1])
			self._CheckBoxList.append(checkBox)
			
			self._StateList.append(True)
			
	#使用するクラスの追加
	def AddClass(self,inputClass,stencilClass,manipulatorClass):
		self._InputClass = inputClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetFilter] : Start SetFilter Update')

		self._ClickState = False
		self._OverState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
				
		if self._OverState:
			if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
				self._ClickState = True
				
				for x in range(self._CheckBoxCount):
					if self.CheckRollOver(mousePosition,self._CheckBoxPosition[x],self._CheckBoxScale):
						
						if self._StateList[x]:	#ON->OFF
							self._StateList[x] = False
							self.SetCheckBoxTexture(self._CheckBoxList[x],self._CheckBoxTextureList[0])
							
						else:					#OFF->ON
							self._StateList[x] = True
							self.SetCheckBoxTexture(self._CheckBoxList[x],self._CheckBoxTextureList[1])
							
	#リセット処理
	def Reset(self):
		self._ClickState = False
		
		self.SetVisible(False)
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
		
	#フィルターの設定を取得
	def GetFilterStateList(self):
		stateList = self._StateList
			
		return stateList
		
	#表示状態の設定
	def SetViewState(self,state):
		if state:
			self._Position[0] = self._PositionX[1]
			for x in range(self._CheckBoxCount):
				pos = [self._PositionX[1]+10,self._Position[1]+self._TextPositionY[x]]
				self._TextPosition[x] = pos
				
				pos2 = [self._PositionX[1]-35,self._Position[1]+self._TextPositionY[x]]
				self._CheckBoxPosition[x] = pos2
		
		else:
			self._Position[0] = self._PositionX[0]
			for x in range(self._CheckBoxCount):
				pos = [self._PositionX[0]+10,self._Position[1]+self._TextPositionY[x]]
				self._TextPosition[x] = pos
		
				pos2 = [self._PositionX[0]-35,self._Position[1]+self._TextPositionY[x]]
				self._CheckBoxPosition[x] = pos2
				
	#表示状態設定
	def SetVisible(self,state):
		self._Frame.visible(viz.OFF)
		
		for x in range(self._CheckBoxCount):
			self._TextList[x].visible(viz.OFF)
			self._CheckBoxList[x].visible(viz.OFF)
		"""
		if state:
			self._Frame.visible(viz.ON)
			
			for x in range(self._CheckBoxCount):
				self._TextList[x].visible(viz.ON)
				self._CheckBoxList[x].visible(viz.ON)
				
		else:
			self._Frame.visible(viz.OFF)
			
			for x in range(self._CheckBoxCount):
				self._TextList[x].visible(viz.OFF)
				self._CheckBoxList[x].visible(viz.OFF)
		"""
				
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		
		for x in range(self._CheckBoxCount):
			self.SetIconPosition(self._TextList[x],self._TextPosition[x],self._TextScale)
			self.SetIconPosition(self._CheckBoxList[x],self._CheckBoxPosition[x],self._CheckBoxScale)
			