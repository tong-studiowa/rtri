﻿# coding: utf-8
#PathSimulator

import viz
import vizjoy
import viztask
import LoadIniFile

#viz.setOption('viz.AVIRecorder.fps','15') 

#キーボード&ゲームパッド対応カメラハンドル
class MyCameraHandler(viz.CameraHandler):

	def __init__(self,path=''):
		
		self._mKeyList	 = [1,2,3,4,5,6,7,8,9,10]
		self._mKeyList2	 = []
		
		self._mMoveVal	 = 2.0
		self._mRotateVal = 20.0
		self.ActiveFlag	 = True
		self._mKey		 = [0,0,0,0,	#移動(0~3)
							0,0,0,0,	#視点操作(4~7)
							0,0,0,0,0,	#その他(8~12)
							0,0,0,0]	#Y,N,Enter,C(13~16)
		self._mPadKey	 = [0,0,0,0,	#ゲームパッド ボタン(0~3)
							0,0,0,0,	#ゲームパッド 十字(4~8)
							0,0]
		
		self._StartPosition	= [0,0,0]
		self._StartAngle	= [0,0,0]
		
		self._Min=0.5
		self._Max=10.0
		
		#カメラビューの生成
		self._mCamera 	 = viz.MainWindow.getView()
		self._mCamera.collision(viz.OFF)
		self._mViewCam 	 = viz.addView()
		headlight1=self._mCamera.getHeadLight()
		headlight2=self._mViewCam.getHeadLight()
		headlight1.disable()
		headlight2.disable()
		
		# 設定ファイルの読み込み
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(viz.res.getPublishedPath(path))
		
		#ジョイスティックの設定
		dinput = viz.add('DirectInput.dle')
		devices = dinput.getJoystickDevices()
		self._mJoy=None
		try:
			self._mJoy=dinput.addJoystick(devices[0])
			self._mJoy.setDeadZone(0.2)
		except:
			self._mJoy=None
		self._mButton=-1
		
		self._mDeviceType=1
		
		#視点ｶﾒﾗの位置を設定
		camPos  = self._mCamera.getPosition()
		camQuat = self._mCamera.getQuat()
		self._mViewCam.setPosition(camPos[0],camPos[1],camPos[2])
		self._mViewCam.setQuat(camQuat[0],camQuat[1],camQuat[2],camQuat[3])
		
		#視点ｶﾒﾗの設定
		viz.MainWindow.setView(self._mViewCam)
		self._mLink = viz.grab(self._mCamera,self._mViewCam)
		self._mLink.setMask(viz.LINK_POS)
		
		# 画面サイズの取得＆設定
		eyeHeightTemp = iniFile.GetItemData('Walkthrough','DefaultHeight')
		self._mEyeHeight = 1.4
		self.SetEyeHight(eyeHeightTemp)
		
		stepSizeTemp  = iniFile.GetItemData('Walkthrough','StepSize')
		self._mStepSize=0.3
		self.SetStepSize(stepSizeTemp)
		
		#iniposTemp1 = iniFile.GetItemData('Walkthrought','StartPosition')
		#iniposTemp2 = iniposTemp1.split(',')
		#self._mCamera.setPosition([float(iniposTemp2[0]),float(iniposTemp2[1]),float(iniposTemp2[2])])
		
		#viz.MainWindow.clip(0.1, 10000)
		
		#viz.callback(viz.KEYDOWN_EVENT,self._KeyDown)
		#viz.callback(viz.KEYUP_EVENT,self._KeyUp)
		
		self._Task = None
		#viz.callback(viz.MOUSEDOWN_EVENT,self.MouseDownEvent)	#マウスのボタンが押された
		#viz.callback(viz.MOUSEUP_EVENT,self.MouseUpEvent)		#マウスのボタンが離された
		
		self._mMyThread	 = viztask.schedule(self._TrackingHandler)
	
	
	def _MouseRotate(self):
		startMousePos=viz.mouse.getPosition()
		while True:
			mousePos=viz.mouse.getPosition()
			rotateValue=[mousePos[0]-startMousePos[0], startMousePos[1]-mousePos[1]]
			if abs(rotateValue[0])>0.2:
				self._Rotate(0,self._mRotateVal*1.5*rotateValue[0],0)
				self._Pan(0,self._mRotateVal*1.5*rotateValue[0],0)
			if abs(rotateValue[1])>0.2:
				self._Pan(self._mRotateVal*1.5*rotateValue[1],0,0)
			
			yield viztask.waitFrame(1)
	
	#マウスのボタンが離された状態から押された状態に切り替わったタイミングで呼び出される関数	
	def MouseDownEvent(self):
	#def MouseDownEvent(self,button):
		self._Task = viztask.schedule(self._MouseRotate())
	
	#マウスのボタンが押された状態から離された状態に切り替わったタイミングで呼び出される関数	
	def MouseUpEvent(self,button):
		if button == viz.MOUSEBUTTON_LEFT:
			if self._Task:
				self._Task.kill()
	
	def SetInitPositionAndAngle(self,startPosition,startAngle):
		self._StartPosition=[startPosition[0],self._mEyeHeight,startPosition[2]]
		self._StartAngle=startAngle
		self.InitPositionAndAngle()
	
	def InitPositionAndAngle(self):
		self.SetCollision(False)
		self._mCamera.setPosition(self._StartPosition)
		self._mCamera.setEuler(self._StartAngle)
		self._mViewCam.setPosition(self._StartPosition)
		self._mViewCam.setEuler(self._StartAngle)
		self.SetEyeHight(self._StartPosition[1])
		self.SetCollision(True)
	
	def __del__(self):
		if self._mDll!=None:
			self._mDll.remove()
		self._mCamera = None
		if self._mMyThread:
			self._mMyThread.kill()
		
		#viz.callback(vizjoy.BUTTONDOWN_EVENT,None)
		#viz.callback(vizjoy.BUTTONUP_EVENT,None)
		#viz.callback(vizjoy.HAT_EVENT,None)
		#viz.callback(viz.KEYDOWN_EVENT,None)
		#viz.callback(viz.KEYUP_EVENT,None)
	
	def SetEyeHight(self,hight):
		try:
			self._mEyeHeight = float(hight)
			if self._mEyeHeight>self._Max:
				self._mEyeHeight=self._Max
			if self._mEyeHeight<self._Min:
				self._mEyeHeight=self._Min
		except:
			self._mEyeHeight = 1.4
		self._mCamera.eyeheight(self._mEyeHeight)
	
	def SetStepSize(self,hight):
		try:
			self._mStepSize = float(hight)
			if self._mStepSize>2.0:
				self._mStepSize=2.0
			if self._mStepSize<0.01:
				self._mStepSize=0.01
		except:
			self._mStepSize=0.30
		self._mCamera.eyeheight(self._mEyeHeight)
	
	def SetPosition(self,pos):
		self._mCamera.setPosition(pos)
	def GetPosition(self):
		return self._mCamera.getPosition()
	def SetCameraAngle(self,ang):
		self._mCamera.setEuler(ang)
	def GetCameraAngle(self):
		return self._mCamera.getEuler()
	def SetViewAngle(self,pan):
		self._mViewCam.setEuler(pan)
	def GetViewAngle(self):
		return self._mViewCam.getEuler()
	def SetHight(self,hight):
		self.SetEyeHight(hight)
	def GetHight(self):
		return self._mEyeHeight
	
	def GetMainView(self):
		return self._mViewCam
	
	def _KeyDown(self,e):
		
		#移動
		if   e==viz.KEY_UP:
			self._mKey[4]=1
		elif e==viz.KEY_RIGHT:
			self._mKey[5]=1
		elif e==viz.KEY_DOWN:
			self._mKey[6]=1
		elif e==viz.KEY_LEFT:
			self._mKey[7]=1
		
		#視点操作
		elif e=='w':
			self._mKey[0]=1
		elif e=='d':
			self._mKey[1]=1
		elif e=='s':
			self._mKey[2]=1
		elif e=='a':
			self._mKey[3]=1
		
		#その他
		elif e==viz.KEY_PAGE_UP:
			#if self._mKey[8]==0: self._mKey[8]=1
			if self.ActiveFlag==True:
				self._mKey[8]=1
		
		elif e==viz.KEY_PAGE_DOWN:
			#if self._mKey[9]==0: self._mKey[9]=1
			if self.ActiveFlag==True:
				self._mKey[9]=1
		
		#elif e==viz.KEY_BACKSPACE:
		#	self._mKey[10]=1
		
		elif e==' ':
			self._mKey[11]=1
		
		elif e==viz.KEY_HOME:
			if self.ActiveFlag==True:
				self._mKey[12]=1
		
		#elif e=='y':
		#	self._mKey[13]=1
		#
		#elif e=='n':
		#	self._mKey[14]=1
		
		#elif e==viz.KEY_RETURN:
		#	if self.ActiveFlag==True:
		#		if self._mKey[15]==0:
		#			self._mKey[15]=1
		
		#elif e=='c':
		#	if self._mKey[16]==0:
		#		self._mKey[16]=1
		
		else:
			pass
	def _KeyUp(self,e):
		if e==viz.KEY_UP:
			self._mKey[4]=0
		elif e==viz.KEY_RIGHT:
			self._mKey[5]=0
		elif e==viz.KEY_DOWN:
			self._mKey[6]=0
		elif e==viz.KEY_LEFT:
			self._mKey[7]=0
		elif e=='w':
			self._mKey[0]=0
		elif e=='d':
			self._mKey[1]=0
		elif e=='s':
			self._mKey[2]=0
		elif e=='a':
			self._mKey[3]=0
		elif e==viz.KEY_PAGE_UP:
			self._mKey[8]=0
		elif e==viz.KEY_PAGE_DOWN:
			self._mKey[9]=0
		elif e==viz.KEY_BACKSPACE:
			self._mKey[10]=0
		elif e==' ':
			self._mKey[11]=0
		elif e==viz.KEY_HOME:
			self._mKey[12]=0
		elif e=='y':
			self._mKey[13]=0
		elif e=='n':
			self._mKey[14]=0
		elif e==viz.KEY_RETURN:
			self._mKey[15]=0
		elif e=='c':
			self._mKey[16]=0
		else:
			pass
	
	def IsDownBackSpaceKey(self):
		if self._mKey[10]!=0:
			self._mKey[10]=0
			return True
		self._mKey[10]=0
		return False	
	def IsDownSpaceKey(self):
		if self._mKey[11]!=0:
			self._mKey[11]=0
			return True
		self._mKey[11]=0
		return False
	def IsDownHomeKey(self):
		if self._mKey[12]!=0:
			self._mKey[12]=0
			return True
		self._mKey[12]=0
		return False
	def IsDownPadButtonA(self):
		if self._mPadKey[0]!=0:
			self._mPadKey[0]=0
			return True
		self._mPadKey[0]=0
		return False
	def IsDownPadButtonB(self):
		if self._mPadKey[1]!=0:
			self._mPadKey[1]=0
			return True
		self._mPadKey[1]=0
		return False
	def IsDownPadButtonX(self):
		if self._mPadKey[2]!=0:
			self._mPadKey[2]=0
			return True
		self._mPadKey[2]=0
		return False
	def IsDownPadButtonY(self):
		if self._mPadKey[3]!=0:
			self._mPadKey[3]=0
			return True
		self._mPadKey[3]=0
		return False
	def IsDownPadButtonBack(self):
		if self._mPadKey[9]!=0:
			self._mPadKey[9]=0
			return True
		self._mPadKey[9]=0
		return False
	def IsDownPadStartButton(self):
		if self._mPadKey[8]!=0:
			self._mPadKey[8]=0
			return True
		self._mPadKey[8]=0
		return False
	def IsDownKeyY(self):
		if self._mKey[13]!=0:
			self._mKey[13]=0
			return True
		self._mKey[13]=0
		return False
	def IsDownKeyN(self):
		if self._mKey[14]!=0:
			self._mKey[14]=0
			return True
		self._mKey[14]=0
		return False
	def IsDownKeyEnter(self):
		if self._mKey[15]==1:
			self._mKey[15]=2
			return True
		#self._mKey[15]=0
		return False
	def IsDownKeyC(self):
		if self._mKey[16]==1:
			self._mKey[16]=2
			return True
		#self._mKey[16]=0
		return False
	
	def SetMoveValue(self,val):
		try:
			self._mMoveVal = float(val)
		except:
			self._mMoveVal = 10.0
	def SetRotateValue(self,val):
		try:
			self._mRotateVal = float(val)
		except:
			self._mRotateVal = 50.0
	def SetMoveCamera(self,iCamera):
		self._mCamera = iCamera
	
	def _Move(self,iX,iY,iZ):
		elapsedTime = viz.getFrameElapsed()
		moveVal = [iX*elapsedTime,iY*elapsedTime,iZ*elapsedTime]
		self._mCamera.move(moveVal)
	def _Rotate(self,iX,iY,iZ):
		currentEuler = self._mViewCam.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,0,0]
		self._mCamera.setEuler(rotateVal)
	def _Pan(self,iX,iY,iZ):
		currentEuler = self._mViewCam.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,currentEuler[1]+iX*elapsedTime,0]
		self._mViewCam.setEuler(rotateVal)
	def _SetEyePos(self,val=1):
		nextHeight = self._mEyeHeight + 1.0*val*viz.getFrameElapsed()
		if nextHeight<=self._Max and nextHeight>=self._Min:
			self._mCamera.eyeheight(nextHeight)
			self._mEyeHeight=nextHeight
		elif nextHeight>self._Max:
			nextHeight=self._Max
			self._mCamera.eyeheight(nextHeight)
			self._mEyeHeight=nextHeight
		elif nextHeight<self._Min:
			nextHeight=self._Min
			self._mCamera.eyeheight(nextHeight)
			self._mEyeHeight=nextHeight

	def ActiveTracking(self,isActive=True):
		if isActive==True:
			self.ActiveFlag=True
		else:
			self.ActiveFlag=False
	def SetCollision(self,isCollide=True):
		if isCollide:
			self._mCamera.collision(viz.ON)
			self._mCamera.stepsize(self._mStepSize)
		else:
			self._mCamera.collision(viz.OFF)
	def ResetRotationOfEyeView(self):
		
		#self._mCamera.setEuler(0,0,0)
		#self._mViewCam.setEuler(0,0,0)
	
		ang1=self._mCamera.getEuler()
		ang2=self._mViewCam.getEuler()
		
		self._mCamera.setEuler(ang1[0],0,0)
		self._mViewCam.setEuler(ang2[0],0,0)

	def _TrackingHandler(self):
		while True:
			if self._mCamera!=None:
				
				#=================
				# キーボード
				#=================
				if self.ActiveFlag==True:
					#移動
					if self._mKey[0] == 1:
						self._Move(0,0,self._mMoveVal)
					elif self._mKey[2] == 1:
						self._Move(0,0,-self._mMoveVal)
					if self._mKey[1] == 1:
						self._Move(self._mMoveVal,0,0)
					elif self._mKey[3] == 1:
						self._Move(-self._mMoveVal,0,0)
					
					#視点操作
					if self._mKey[4] == 1:	#下
						self._Pan(-self._mRotateVal,0,0)
					elif self._mKey[6] == 1:	#上
						self._Pan(self._mRotateVal,0,0)
					if self._mKey[5] == 1:	#右
						self._Rotate(0,self._mRotateVal,0)
						self._Pan(0,self._mRotateVal,0)
					elif self._mKey[7] == 1:	#左
						self._Rotate(0,-self._mRotateVal,0)
						self._Pan(0,-self._mRotateVal,0)
					
					#視点位置移動
					if self._mKey[8] == 1:
						self._SetEyePos(1)
					elif self._mKey[9] == 1:
						self._SetEyePos(-1)
					elif self._mKey[12] == 1:
						self.ResetRotationOfEyeView()
					else:
						pass
				
				
				#=================
				# ジョイパッド
				#=================
				
				if self._mJoy:
					
					if self.ActiveFlag==True:
						
						pos = self._mJoy.getPosition()
						rot = self._mJoy.getRotation()
						hat = self._mJoy.getHat()
						
						if self._mDeviceType==1:
							rot=[pos[2],rot[2],0]
						
						#移動
						if abs(pos[1])>0.2:
							self._Move(0,0,self._mMoveVal*pos[1])
						if abs(pos[0])>0.2:
							self._Move(self._mMoveVal*pos[0],0,0)
						
						#視点操作
						if abs(rot[0])>0.2:
							self._Rotate(0,self._mRotateVal*rot[0],0)
							self._Pan(0,self._mRotateVal*rot[0],0)
						if abs(rot[1])>0.2:
							self._Pan(self._mRotateVal*rot[1],0,0)
						
						#視点リセット
						if self._mJoy.isButtonDown(2):	# Xbox360 -> X
							self._mPadKey[2]=1
							self.ResetRotationOfEyeView()
						
						#画面切り替え
						if self._mJoy.isButtonDown(6):	# Xbox360 -> X
							self._mPadKey[9]=1
						
						#スタート位置ジャンプ
						if self._mJoy.isButtonDown(7):	# Xbox360 -> X
							self._mPadKey[8]=1
							#self.InitPositionAndAngle()
						
						#視点位置移動
						if self._mJoy.isButtonDown(4):	# RB
							self._SetEyePos(-1)
						if self._mJoy.isButtonDown(5):	# LB
							self._SetEyePos(1)
					
					#はい
					if self._mJoy.isButtonDown(1):	# Xbox360 -> A
						self._mPadKey[0]=1
					#いいえ
					if self._mJoy.isButtonDown(0):	# Xbox360 -> B
						self._mPadKey[1]=1
					#メニュー表示
					if self._mJoy.isButtonDown(3):	# Xbox360 -> Y
						self._mPadKey[3]=1
			
			yield viztask.waitFrame(1)

class WindowClass():
	
	def __init__(self,view):
		self._mDispNum		= 1
		self._mToggle		= 0
		#self._mLeftWindow	= viz.MainWindow
		self._mLeftWindow	= viz.addWindow(size=[0,0],pos=[0,0])
		self._mRightWindow	= viz.addWindow(size=[0,0],pos=[0,0])
		
		self._mLeftWindow.setView(view)
		self._mRightWindow.setView(view)
	
	def ResetBackgroungColor(self):
		viz.MainWindow.clearcolor(viz.BLACK)
		self._mLeftWindow.clearcolor(viz.BLACK)
		self._mRightWindow.clearcolor(viz.BLACK)
	
	def GetDisplayNum(self):
		return self._mDispNum
	
	def SetDisplayNum(self,dNum):
		
		viz.MainWindow.clearcolor(viz.WHITE)
		self._mLeftWindow.clearcolor(viz.WHITE)
		self._mRightWindow.clearcolor(viz.WHITE)
		
		self._mDispNum = dNum
		
		if dNum==2:
			self._mLeftWindow.setSize([0.5,1.0])
			self._mLeftWindow.setPosition([0.0,1.0])
			
			self._mRightWindow.setSize([0.5,1.0])
			self._mRightWindow.setPosition([0.5,1.0])
			
			self._mToggle=0
		else:
			self._mLeftWindow.setSize([0,0])
			self._mLeftWindow.setPosition([0,0])
			
			self._mRightWindow.setSize([0,0])
			self._mRightWindow.setPosition([0,0])
	
	def ChangeDispMode(self):
		if self._mDispNum==2:
			if self._mToggle==0:
				self._mLeftWindow.setSize([1.0,1.0])
				self._mLeftWindow.setPosition([0.0,1.0])
				self._mRightWindow.setSize([0,0])
				self._mRightWindow.setPosition([0,0])
				self._mToggle=1
			elif self._mToggle==1:
				self._mLeftWindow.setSize([0,0])
				self._mLeftWindow.setPosition([0,0])
				self._mRightWindow.setSize([1.0,1.0])
				self._mRightWindow.setPosition([0.0,1.0])
				self._mToggle=2
			else:
				self._mLeftWindow.setSize([0.5,1.0])
				self._mLeftWindow.setPosition([0.0,1.0])
				self._mRightWindow.setSize([0.5,1.0])
				self._mRightWindow.setPosition([0.5,1.0])
				self._mToggle=0
	
	def GetDispMode(self):
		return self._mToggle
	
	def SetRenderingModels(self,rightModelList,leftModelList):
		
		for model in rightModelList:
			if model:
				model.renderOnlyToWindows([self._mRightWindow])
		
		for model in leftModelList:
			if model:
				model.renderOnlyToWindows([self._mLeftWindow])

	def NotRenderingModels(self,ModelList):
		for model in ModelList:
			if model:
				model.renderToAllWindowsExcept([viz.MainWindow,self._mLeftWindow,self._mRightWindow])

