﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#柱関係のファンクション制御のクラス定義
class PillarFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][Layouter][PillarFunction] : Init PillarFunction')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectWallClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectWallClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectWallClass = selectWallClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][Layouter][PillarFunction] : Start PillarFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num ==0:
			self.Delete()
	
	#選択が解除された
	def Hide(self):
		pass
		
	#削除
	def Delete(self):
		roomNum = None
		wallNum = None
		wallState = 0
		
		wallList = []
		
		roomNum,wallNum,wallState = self._SelectWallClass.GetSelectedModel()
		wallList = self._SelectWallClass.GetSelectedModel2()
		
		if self._RoomControlClass.GetWallPointCount(roomNum) > 3:	#3角形は削除できない
			pointDataTemp	= self._RoomControlClass.GetWallPointList(roomNum)
			pointData = []
			for point in pointDataTemp:
				pointData.append(point)
				
			wallVisibleListTemp	= self._RoomControlClass.GetWallVisibleList(roomNum)
			wallVisibleList = []
			for visible in wallVisibleListTemp:
				wallVisibleList.append(visible)
				
			floorPosTemp = self._RoomControlClass.GetFloorPosition(roomNum)
			floorPos = []
			for pos in floorPosTemp:
				floorPos.append(pos)
			
			self._UndoData = []
			self._UndoData.append(18)
			self._UndoData.append([roomNum,wallNum,pointData,wallVisibleList,floorPos])
			
			self._SelectWallClass.UnSelect()
			self._RoomControlClass.RemoveWallPoint(roomNum,wallNum)
			
			if wallList != []:
				for wall in wallList:
					if self._RoomControlClass.GetWallPointCount(wall[0]) > 3:	#3角形は削除できない
						pointDataTemp	= self._RoomControlClass.GetWallPointList(wall[0])
						pointData = []
						for data in pointDataTemp:
							pointData.append(data)
							
						wallVisibleListTemp	= self._RoomControlClass.GetWallVisibleList(wall[0])
						wallVisibleList = []
						for data in wallVisibleListTemp:
							wallVisibleList.append(data)
							
						floorPosTemp = self._RoomControlClass.GetFloorPosition(wall[0])
						floorPos = []
						for pos in floorPosTemp:
							floorPos.append(pos)
			
						self._UndoData.append([wall[0],wall[1],pointData,wallVisibleList,floorPos])
						
						self._RoomControlClass.RemoveWallPoint(wall[0],wall[1])
						
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()