﻿# coding: utf-8

import viz
import vizinput
import vizmat

import Interface
import Path

import os
import sys
import codecs
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス制御のクラス定義
class PathControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControl] : Init PathControl')
		
		self._SelectPathClass = None
		self._CharacterControlClass = None
		self._ModelControlClass = None
		self._ItemControlClass = None
		
		self._PathList = []
		self._LineList = []
		
		self._ItemPathList = []
		self._ItemLineList = []
		
		self._Height = 0.01
		self._IsVisible = True
		
	#使用するクラスの追加
	def AddClass(self,selectPathClass,characterControlClass,modelControlClass,itemControlClass):
		self._SelectPathClass = selectPathClass
		self._CharacterControlClass = characterControlClass
		self._ModelControlClass = modelControlClass
		self._ItemControlClass = itemControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControl] : Start PathControl Update')
		pass
		
	#パスを読み込む
	def Load(self,characterNum,fileName,isItem=False,usePathFile=False):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControl] : Load')
		
		positions = []
		angle = 180
		
		if usePathFile:
			#ファイルの読み込み
			filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
			logFile = codecs.open(filePath, 'r', 'shift-jis')
			
			setAngle = False
			for logData in logFile:
				pos = [0.0,0.0,0.0]
				pos[0] = float(logData.split(',')[1])
				pos[2] = float(logData.split(',')[3])
				positions.append(pos)
				
				if setAngle == False:
					angle = float(logData.split(',')[4])
					setAngle = True
					
			logFile.close()
			
		else:
			setAngle = False
			for logData in fileName:
				pos = [0.0,0.0,0.0]
				pos[0] = float(logData[0])
				pos[2] = float(logData[2])
				positions.append(pos)
				
				if setAngle == False:
					angle = float(logData[3])
					setAngle = True
					
		#パスの作成
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		
		for x in range(len(positions)):
			pos = positions[x]
			
			if x == 0:
				self.SetPosition(characterNum,x,pos,isItem)
				if isItem == False:
					self._CharacterControlClass.SetPosition(characterNum,pos)
					self._CharacterControlClass.SetOrientation(characterNum,angle)
			
			else:
				pathNum = self.Add(characterNum,isItem)
				self.SetPosition(characterNum,x,pos,isItem)
				
	#パスを保存する
	def Save(self,characterNum,fileName,characterAngle,isItem=False):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControl] : Save')
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		
		#保存用logファイルの作成
		logDataTemp = []
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		for x in range(len(pathData)):
			path = pathData[x]
			pos = path.GetPosition()
			ori = 0
			if x == 0:	#最初のポイントはキャラクターの角度を保存
				ori = characterAngle
			data =[pos[0],pos[1],pos[2],ori]
			logDataTemp.append(data)
			
		logData = ""
		for x in range(len(logDataTemp)):
			pos = logDataTemp[x]
			Ori = 0
			if x == 0:	#最初のポイントはキャラクターの角度を保存
				Ori = characterAngle
			data = str(x)+','+str(pos[0])+','+str(self._Height)+','+str(pos[2])+','+str(Ori)+','+str(0)+','+str(0)+','+str(Ori)+','+str(0)+','+str(0)+','+str(self._Height)+'\n'
			logData = logData + data
			
		#ファイルの保存
		#logFile = codecs.open(filePath, 'w', 'shift-jis')
		#logFile.write(logData)
		#logFile.close()
		
		#print 'logDataTemp',logDataTemp
		return logDataTemp
		
	#追加
	def Add(self,characterNum,isItem=False):
		path = Path.Path(self._ModelControlClass,isItem)
		
		num = 0
		if isItem:
			pathData = self._ItemPathList[characterNum]
			pathData.append(path)
			
			self._ItemPathList[characterNum] = (pathData)
			
			num = len(pathData) - 1
			path.SetId(num)
			
		else:
			pathData = self._PathList[characterNum]
			pathData.append(path)
			
			self._PathList[characterNum] = (pathData)
			
			num = len(pathData) - 1
			path.SetId(num)
			
		return num
		
	#削除
	def Delete(self,characterNum,num,isItem=False):
		if num == 0 or num == None:
			pass
		else:
			deletePathData = None
			if isItem:
				deletePathData = self._ItemPathList[characterNum]
			else:
				deletePathData = self._PathList[characterNum]
			deleteModel = deletePathData[num]
			
			#モデルリストを更新
			newModelLiset = []
			
			for path in deletePathData:
				if path != deleteModel:
					newModelLiset.append(path)
					
			if isItem:
				self._ItemPathList[characterNum] = newModelLiset
			else:
				self._PathList[characterNum] = newModelLiset
				
			#モデルを削除
			if deleteModel != None:
				deleteModel.Delete()
				del deleteModel
				
			#IDを再設定
			for path in deletePathData:
				path.SetId(num)
				
			self.SetLine(isItem)
			
	#全パス削除
	def DeleteAll(self,characterNum):
		self._SelectPathClass.UnSelect()
		
		deletePathData = self._PathList[characterNum]
		
		#モデルリストを更新
		newModelLiset = []
		
		for pathData in self._PathList:
			if pathData != deletePathData:
				
				newModelLiset.append(pathData)
				
		self._PathList = newModelLiset
		
		#モデルを削除
		for path in deletePathData:
			path.Delete()
			del path
			
		del deletePathData
		
		self.SetLine()
		
	#位置設定
	def SetPosition(self,characterNum,num,position,isItem=False):
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		path = pathData[num]
		
		if path != None and position != None:
			position[1] = self._Height
			path.SetPosition(position)
			
		self.SetLine(isItem)
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,characterNum,num,position):
		pathData = self._PathList[characterNum]
		path = pathData[num]
		
		if path != None and position != None:	
			path.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,characterNum,num,isItem=False):
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		path = pathData[num]
		position = [0,0,0]
		
		if path != None:
			position = path.GetPosition()
		
		return position
		
	#ハイライト表示
	def SetHighlight(self,characterNum,num,state,isItem=False):
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		model = pathData[num]
		model.SetHighlight(state)
		
	#モデルから番号を取得
	def GetPathNum(self,model):
		characterNum = None
		num = None
		isItem = False
		
		for x in range(len(self._PathList)):
			pathData = self._PathList[x]
			for y in range(len(pathData)):
				if pathData[y]._Model == model:
					characterNum = x
					num =  y
		
		if characterNum == None:
			for x in range(len(self._ItemPathList)):
				pathData = self._ItemPathList[x]
				for y in range(len(pathData)):
					if pathData[y]._Model == model:
						characterNum = x
						num =  y
						isItem = True
						
		return characterNum,num,isItem
		
	#全ログの選択可能状態を設定
	def SetPickableForAllPath(self,state):
		for pathData in self._PathList:
			for path in pathData:
				path.SetPickable(state)
				
		for pathData in self._ItemPathList:
			for path in pathData:
				path.SetPickable(state)
			
	#パス取得
	def GetPath(self,characterNum,num,isItem=False):
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		path = pathData[num]
		
		return path
		
	#パスのリスト取得
	def GetPathList(self,isItem=False):
		list = None
		if isItem:
			list = self._ItemPathList
		else:
			list = self._PathList
		return self._PathList
		
	#パスのデータを取得
	def GetPathData(self,characterNum,isItem=False):
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
		return pathData
		
	#パスの位置リストを取得
	def GetPathPositionList(self,characterNum,isItem=False):
		posList = []
		
		pathData = None
		if isItem:
			pathData = self._ItemPathList[characterNum]
		else:
			pathData = self._PathList[characterNum]
			
		for path in pathData:
			pos = path.GetPosition()
			posList.append(pos)
			
		return posList
		
	#パスの位置リストを設定
	def SetPathPositionList(self,characterNum,posList,isItem=False):
		for x in range(len(posList)):
			pos = posList[x]
			
			if x == 0:
				self.SetPosition(characterNum,x,pos,isItem)
				
			else:
				pathNum = self.Add(characterNum,isItem)
				self.SetPosition(characterNum,x,pos,isItem)
		
	#パスの数を取得
	def GetPathCount(self,isItem=False):
		count = 0
		if isItem:
			count = len(self._ItemPathList)
		else:
			count = len(self._PathList)
		return count
		
	#ライン設定
	def SetLine(self,isItem=False):
		#削除
		if isItem:
			lineCount = len(self._ItemLineList)
			if lineCount != 0:
				for x in range(lineCount):
					line = self._ItemLineList[x]
					if line:
						line.remove()
						del line
						
						self._ItemLineList[x] = None
				
				self._ItemLineList = []
				
		else:
			lineCount = len(self._LineList)
			if lineCount != 0:
				for x in range(lineCount):
					line = self._LineList[x]
					if line:
						line.remove()
						del line
						
						self._LineList[x] = None
				
				self._LineList = []
			
		#作成
		pathCount = 0
		if isItem:
			pathCount = len(self._ItemPathList)
		else:
			pathCount = len(self._PathList)
			
		color = viz.BLUE
		if isItem:
			#color = viz.GREEN
			color = viz.BLUE
			
		for x in range(pathCount):
			line = None
			pathData = None
			if isItem:
				pathData = self._ItemPathList[x]
			else:
				pathData = self._PathList[x]
			if len(pathData) > 1:
				viz.startLayer(viz.LINE_STRIP) 
				for path in pathData:
					pathPos = path.GetPosition()
					pos = [0,0,0]
					pos[0] = pathPos[0]
					pos[1] = self._Height
					pos[2] = pathPos[2]
					viz.vertex(pos)
					
				line = viz.endLayer()
				line.disable(viz.LIGHTING)
				line.disable(viz.PICKING)
				
				line.disable(viz.DEPTH_WRITE)
				#line.alpha(0.9)
				line.drawOrder(-5)
		
				line.color(color)
				line.lineWidth(3)			#線の太さ
				
			if isItem:
				self._ItemLineList.append(line)
			else:
				self._LineList.append(line)
			
		self.SetVisible(self._IsVisible)
		
	#追加
	def AddCharacter(self,isItem=False):
		path = Path.Path(self._ModelControlClass,isItem)
		pathData = [path]
		
		num = 0
		if isItem:
			self._ItemPathList.append(pathData)
			num = len(self._ItemPathList) - 1
		else:
			self._PathList.append(pathData)
			num = len(self._PathList) - 1
		
		path.SetId(0)
		
		return num
		
	#削除
	def DeleteCharacter(self,characterNum,isItem=False):
		deletePathData = None
		if isItem:
			deletePathData = self._ItemPathList[characterNum]
		else:
			deletePathData = self._PathList[characterNum]
			
		#モデルリストを更新
		newModelLiset = []
		
		if isItem:
			for x in range(len(self._ItemPathList)):
				if x != characterNum:
					pathData = self._ItemPathList[x]
					newModelLiset.append(pathData)
							
			self._ItemPathList = newModelLiset
			
		else:
			for x in range(len(self._PathList)):
				if x != characterNum:
					pathData = self._PathList[x]
					newModelLiset.append(pathData)
							
			self._PathList = newModelLiset
		
		#モデルを削除
		if deletePathData != None:
			for path in deletePathData:
				path.Delete()
				del path
			del deletePathData
		
		self.SetLine(isItem)
		
	#削除
	def DeleteCharacterPath(self,characterNum,isItem=False):
		deletePathData = None
		if isItem:
			deletePathData = self._ItemPathList[characterNum]
		else:
			deletePathData = self._PathList[characterNum]
		
		#モデルリストを更新
		newModelLiset = []
		newModelLiset.append(deletePathData[0])
		
		if isItem:
			self._ItemPathList[characterNum] = newModelLiset
		else:
			self._PathList[characterNum] = newModelLiset
		
		#モデルを削除
		for x in range(len(deletePathData)):
			if x != 0:
				deleteModel = deletePathData[x]
				deleteModel.Delete()
				del deleteModel
		
		self.SetLine(isItem)
	
	#全パスの削除
	def DeleteAllCharacter(self,isItem=False):
		self._SelectPathClass.UnSelect()
		
		if isItem:
			pathCount = self.GetPathCount(True)
			for x in range(pathCount):
				pathData = self._ItemPathList[x]
				PathDataCount = len(pathData)
				
				for y in range(PathDataCount):
					deleteModel = pathData[0]
					deleteModel.Delete()
					del deleteModel
					
				del pathData
				self._ItemPathList[x] = None
			
			self._ItemPathList = []
		
		else:
			pathCount = self.GetPathCount()
			for x in range(pathCount):
				pathData = self._PathList[x]
				PathDataCount = len(pathData)
				
				for y in range(PathDataCount):
					deleteModel = pathData[0]
					deleteModel.Delete()
					del deleteModel
					
				del pathData
				self._PathList[x] = None
			
			self._PathList = []
		
			
		self.SetLine(isItem)
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.ON
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		if self._ItemPathList != None:
			for pathData in self._ItemPathList:
				for path in pathData:
					path.SetVisible(val)
				
			for line in self._ItemLineList:
				if line != None:
					line.visible(val)
					
		if self._PathList != None:
			for pathData in self._PathList:
				for path in pathData:
					path.SetVisible(val)
				
			for line in self._LineList:
				if line != None:
					line.visible(val)
					
	#IDを入れ替え
	def ChangeId(self,num,isItem=False):
		#print 'changeID',num
		pathList = None
		lineList = None
		if isItem:
			pathList = self._ItemPathList
			lineList = self._ItemLineList
		else:
			pathList = self._PathList
			lineList = self._LineList
		newCharaNum = len(pathList)-1
		
		#リストを更新
		newPathList = []
		newLineList = []
		
		for x in range(len(pathList)):
			path = pathList[x]
			line = lineList[x]
			
			if x == num:
				newPath	= pathList[newCharaNum]
				newLine	= lineList[newCharaNum]
				
				newPathList.append(newPath)
				newLineList.append(newLine)
				
			if x != newCharaNum:
				newPathList.append(path)
				newLineList.append(line)
				
		if isItem:
			self._ItemPathList = newPathList
			self._ItemLineList = newLineList
		else:
			self._PathList = newPathList
			self._LineList = newLineList
		
		self.UpdateId(num,isItem)
		
		#デバッグ用
		#for x in range(self._ItemIniDataCount):
		#	data = self._ItemIniDataList[x]
		#	print 'data(changeID)=',x,data[0],data[10]
		
	#IDを更新
	def UpdateId(self,num,isItem=False):
		#パスのIDを更新
		pathData = None
		if isItem:
			pathData = self._ItemPathList[num]
		else:
			pathData = self._PathList[num]
			
		for x in range(len(pathData)):
			path = pathData[x]
			path.SetId(x)
			
	#IDを入れ替え
	def ChangeId2(self,charaNum,num,isItem=False):
		#print 'changeID2',num
		
		pathData = None
		if isItem:
			pathData = self._ItemPathList[charaNum]
		else:
			pathData = self._PathList[charaNum]
		newPathNum = len(pathData)-1
		
		#リストを更新
		newPathDataList = []
		
		for x in range(len(pathData)):
			path = pathData[x]
			
			if x == num:
				newPath	= pathData[newPathNum]
				newPathDataList.append(newPath)
				
			if x != newPathNum:
				newPathDataList.append(path)
				
		if isItem:
			self._ItemPathList[charaNum] = newPathDataList
		else:
			self._PathList[charaNum] = newPathDataList

		self.UpdateId2(charaNum,isItem)
		self.SetLine(isItem)
		
		#デバッグ用
		#for x in range(self._ItemIniDataCount):
		#	data = self._ItemIniDataList[x]
		#	print 'data(changeID)=',x,data[0],data[10]
		
	#IDを更新
	def UpdateId2(self,num,isItem=False):
		#パスのIDを更新2
		pathData = None
		if isItem:
			pathData = self._ItemPathList[num]
		else:
			pathData = self._PathList[num]
			
		for x in range(len(pathData)):
			path = pathData[x]
			path.SetId(x)
			
	#パスの表示を設定
	def SetPathVisible(self,id,state,isItem=False):
		val = viz.ON
		if state == True:
			val = viz.ON
		else:
			val = viz.OFF
			
		line = None
		pathData = None
		if isItem:
			line = self._ItemLineList[id]
			pathData = self._ItemPathList[id]
		else:
			line = self._LineList[id]
			pathData = self._PathList[id]
		
		if line != None:
			line.visible(val)
			
		if pathData != None:
			for path in pathData:
				path.SetVisible(val)
				
	#パスの表示を取得
	def GetPathVisible(self,id,isItem=False):
		line = None
		if isItem:
			line = self._ItemLineList[id]
		else:
			line = self._LineList[id]
		state = False
		
		if line != None:
			state = line.getVisible()
		
		return state
		
	#パスの保存するデータを取得
	def GetSaveData(self,characterNum,characterAngle):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathControl] : GetSaveData')
		
		#保存用logファイルの作成
		logDataTemp = []
		pathData = self._PathList[characterNum]
		for x in range(len(pathData)):
			path = pathData[x]
			logDataTemp.append(path.GetPosition())
			
		logData = []
		for x in range(len(logDataTemp)):
			pos = logDataTemp[x]
			Ori = 0
			if x == 0:	#最初のポイントはキャラクターの角度を保存
				Ori = characterAngle
			data = [pos[0],0.0,pos[2],Ori]
			logData.append(data)
			
		return logData
		
	#ラインを非表示
	def SetAllLineVisible(self):
		if self._PathList != None:
			for pathData in self._PathList:
				for path in pathData:
					path.SetVisible(viz.OFF)
					
			for line in self._LineList:
				if line != None:
					line.visible(viz.OFF)
					
		if self._ItemPathList != None:
			for pathData in self._ItemPathList:
				for path in pathData:
					path.SetVisible(viz.OFF)
					
	#ラインの表示を設定
	def SetLineVisible(self,id,state):
		val = viz.OFF
		if state:
			val = viz.ON
			
		if self._ItemPathList != None:
			if id < len(self._ItemPathList):
				line = self._ItemLineList[id]
				if line != None:
					line.visible(val)
					
	#ラインの色を設定
	def SetLineColor(self,state):
		color = viz.RED
		if state:
			#color = viz.GREEN
			color = viz.BLUE
			
		if self._ItemPathList != None:
			for line in self._ItemLineList:
				if line != None:
					line.color(color)
					
	#ラインモデルのリストを取得
	def GetLineModelList(self):
		list = []
		if self._ItemPathList != None:
			for line in self._ItemLineList:
				if line != None:
					list.append(line)
					
		return list
		