﻿# coding: utf-8

import viz
import vizmat
import Object3d
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム、間取り、キャラクター移動制御のクラス定義
class TranslateModel(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateItem] : Init TranslateItem')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._RoomControlClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._ManipulatorClass = None
		self._AddPathClass = None
		self._ShortcutKeyControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._SelectWallClass = None
		
		self._SelectedItem = None
		self._SelectedItemMoveYState = False
		self._SelectedRoom = None
		self._SelectedCharacter = None
		self._SelectedDoor = [None,None]
		self._SelectedWindow = [None,None]
		
		self._OrthoViewWindow = None
		
		self._DoorWallState = None
		self._DoorOffsetList = None
		self._DoorPrePosList = None
		
		self._WindowWallState = None
		self._WindowOffsetList = None
		self._WindowPrePosList = None
		
		self._ManipulatorState = 0
		
		self._TranslateState = False
		self._PreTranslateState = False
		
		self._TranslateSnap = translateSnap
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,roomControlClass,characterControlClass,selectModelClass
				,manipulatorClass,addPathClass,undoClass,redoClass,selectWallClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._ManipulatorClass = manipulatorClass
		self._AddPathClass = addPathClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._SelectWallClass = selectWallClass
		
	#使用するクラスの追加
	def SetShortcutKeyControlClass(self,shortcutKeyControlClass):
		self._ShortcutKeyControlClass = shortcutKeyControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateItem] : Start TranslateItem Update')
		
		self._TranslateState = False
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState != 0 and manipulatorState != 2 :
			self._SelectedItem = self._SelectModelClass.GetSelectedModel()
			self._SelectedItemMoveYState = self._SelectModelClass.GetMoveYState()
			self._SelectedRoom = self._SelectModelClass.GetSelectedRoom()
			self._SelectedCharacter = self._SelectModelClass.GetSelectedCharacter()
			self._SelectedDoor[0],self._SelectedDoor[1] = self._SelectModelClass.GetSelectedDoor()
			self._SelectedWindow[0],self._SelectedWindow[1] = self._SelectModelClass.GetSelectedWindow()
			
			if manipulatorState == 4 and self._SelectedItemMoveYState == False:
				pass
				
			else:
				if self._SelectedItem != None or self._SelectedRoom != None or self._SelectedCharacter != None or self._SelectedDoor != [None,None] or self._SelectedWindow != [None,None]:
					self._TranslateState = True
					
					#Ctrl＋ドラッグでコピー
					if self._TranslateState == True and self._PreTranslateState == False:
						if self._InputClass.GetCtrlKeyState():
							self._ShortcutKeyControlClass.Copy()
							self._ShortcutKeyControlClass.Paste(True)
							
					if self._TranslateState == True and self._PreTranslateState == False:
						self._UndoData = []
						self._UndoData.append(36)
						
					if self._SelectedItem != None:
						self.TranslateItem(manipulatorState)
					if self._SelectedRoom != None:
						self.TranslateRoom(manipulatorState)
					if self._SelectedCharacter != None:
						self.TranslateCharacter(manipulatorState)
					if self._SelectedDoor != [None,None]:
						self.TranslateDoor(manipulatorState)
					if self._SelectedWindow != [None,None]:
						self.TranslateWindow(manipulatorState)
						
		if self._TranslateState == False and self._PreTranslateState == True:
			self.FinTranslate()
			
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
		self._PreTranslateState = self._TranslateState
		
	#アイテムの移動
	def TranslateItem(self,manipulatorState):
		dummy = self._SelectModelClass.GetDummyModel()
		dummyPosition = dummy.getPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = dummyPosition[0]
			position[1] = pickingPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = dummyPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
		
		#アイテムを移動
		dispracement = [0,0,0]
		dispracement[0] = position[0] - dummyPosition[0]
		dispracement[1] = position[1] - dummyPosition[1]
		dispracement[2] = position[2] - dummyPosition[2]
		
		selectedItemList = self._SelectModelClass.GetSelectedModelList()
		
		for x in range(len(selectedItemList)):
			itemNum = selectedItemList[x]
			itemPosition = self._ItemControlClass.GetPosition(itemNum)
			
			newItemPositin = [0,0,0]
			newItemPositin[0] = itemPosition[0] + dispracement[0]
			newItemPositin[1] = itemPosition[1] + dispracement[1]
			newItemPositin[2] = itemPosition[2] + dispracement[2]
			
			self._ItemControlClass.SetPosition(itemNum,newItemPositin)
			
			if self._TranslateState == True and self._PreTranslateState == False:
				self._UndoData.append([0,itemNum,itemPosition])
				
	#間取りの移動
	def TranslateRoom(self,manipulatorState):
		dummy = self._SelectModelClass.GetDummyModel()
		dummyPosition = dummy.getPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = dummyPosition[0]
			position[1] = pickingPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = dummyPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		#部屋を移動
		dispracement = [0,0,0]
		dispracement[0] = position[0] - dummyPosition[0]
		dispracement[1] = position[1] - dummyPosition[1]
		dispracement[2] = position[2] - dummyPosition[2]
		
		selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
		
		for x in range(len(selectedRoomList)):
			roomNum = selectedRoomList[x]
			roomPosition = self._RoomControlClass.GetFloorPosition(roomNum)
			
			newRoomPositin = [0,0,0]
			newRoomPositin[0] = roomPosition[0] + dispracement[0]
			newRoomPositin[1] = roomPosition[1] + dispracement[1]
			newRoomPositin[2] = roomPosition[2] + dispracement[2]
			
			if self._TranslateState == True and self._PreTranslateState == False:
				self._UndoData.append([1,roomNum,roomPosition])
				
			self._RoomControlClass.SetFloorPosition(roomNum,newRoomPositin,withWall=False)
			
	#キャラクターの移動
	def TranslateCharacter(self,manipulatorState):
		dummy = self._SelectModelClass.GetDummyModel()
		dummyPosition = dummy.getPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = dummyPosition[0]
			position[1] = pickingPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = dummyPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		#アイテムを移動
		dispracement = [0,0,0]
		dispracement[0] = position[0] - dummyPosition[0]
		dispracement[1] = position[1] - dummyPosition[1]
		dispracement[2] = position[2] - dummyPosition[2]
		
		selectedCharacterList = self._SelectModelClass.GetSelectedCharacterList()
		
		for x in range(len(selectedCharacterList)):
			characterNum = selectedCharacterList[x]
			characterPositin = self._CharacterControlClass.GetPosition(characterNum)
			
			newCharacterPositin = [0,0,0]
			newCharacterPositin[0] = characterPositin[0] + dispracement[0]
			newCharacterPositin[1] = characterPositin[1] + dispracement[1]
			newCharacterPositin[2] = characterPositin[2] + dispracement[2]
			
			self._CharacterControlClass.SetPosition(characterNum,newCharacterPositin)
			
			if self._TranslateState == True and self._PreTranslateState == False:
				self._UndoData.append([2,characterNum,characterPositin])
				
	#ドアの移動
	def TranslateDoor(self,manipulatorState):
		dummy = self._SelectModelClass.GetDummyModel()
		dummyPosition = dummy.getPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = dummyPosition[0]
			position[1] = pickingPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = dummyPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		dispracement = [0,0,0]
		dispracement[0] = position[0] - dummyPosition[0]
		dispracement[1] = position[1] - dummyPosition[1]
		dispracement[2] = position[2] - dummyPosition[2]
		
		selectedDoorList = self._SelectModelClass.GetSelectedDoorList()
		
		#間取りをコピーする時はドア、窓の選択を解除
		roomList = self._SelectModelClass.GetSelectedRoomList()
		doorListTemp = []
		for doorData in selectedDoorList:
			state = True
			for room in roomList:
				if room == doorData[0]:
					state = False
			if state:
				doorListTemp.append(doorData)
		selectedDoorList = doorListTemp
		
		mousePosition = self._InputClass.GetMousePosition()
		#print 'mousePosition',mousePosition
		
		if self._TranslateState == True and self._PreTranslateState == False:
			for doorData in selectedDoorList:
				doorState = self._RoomControlClass.GetDoorState(doorData[0],doorData[1])
				self._UndoData.append([3,doorData[0],doorData[1],doorState[1],doorState[2]])
				
			self._DoorOffsetList = []
			self._DoorPrePosList = []
			for doorData in selectedDoorList:
				door = self._RoomControlClass.GetDoorModel(doorData[0],doorData[1])
				model = door.GetModel()
				
				box = model.getBoundingBox(mode=viz.ABS_GLOBAL)
				boxSize = box.getSize()
				boxCenter = box.getCenter()
				prePosition = boxCenter
				
				modelPos2d = self._OrthoViewWindow.worldToScreen( prePosition )
				dummyPos2d = self._OrthoViewWindow.worldToScreen( dummyPosition )
				
				offset3d = [0,0,0]
				offset3d[0] = prePosition[0] - dummyPosition[0]
				offset3d[1] = prePosition[1] - dummyPosition[1]
				offset3d[2] = prePosition[2] - dummyPosition[2]
				
				offset2d = [0,0]
				offset2d[0] = modelPos2d[0] - dummyPos2d[0]
				offset2d[1] = modelPos2d[1] - dummyPos2d[1]
				
				self._DoorOffsetList.append([offset3d,offset2d])
				
				position = model.getPosition(mode=viz.ABS_GLOBAL)
				euler = model.getEuler(mode=viz.ABS_GLOBAL)
				
				doorLine = self._RoomControlClass.GetDoorLineModel(doorData[0],doorData[1])
				lineModel = doorLine.GetModel()
				linePosition = lineModel.getPosition(mode=viz.ABS_GLOBAL)
				
				doorState = self._RoomControlClass.GetDoorState(doorData[0],doorData[1])
				self._DoorPrePosList.append([position,euler,linePosition,doorState[1],doorState[2]])
				
		self._DoorWallState = []
		for x in range(len(selectedDoorList)):
			doorData = selectedDoorList[x]
			door = self._RoomControlClass.GetDoorModel(doorData[0],doorData[1])
			model = door.GetModel()
			positionOffset = door.GetPositionOffset()
			
			box = model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()
			prePosition = boxCenter
			
			offset = self._DoorOffsetList[x]
			modelPos = [0,0]
			modelPos[0] = offset[1][0] + mousePosition[0]
			modelPos[1] = offset[1][1] + mousePosition[1]
			
			roomNum = None
			wallNum = None
			wallState = 0
			roomNum,wallNum,wallState = self._SelectWallClass.Select(False,onlyRoom=doorData[0],pos=modelPos)
			point0 = [0,0]
			point1 = [0,0]
			if wallState == 1:
				point0,point1 = self._RoomControlClass.GetWallPoint(roomNum,wallNum)
				
			self._DoorWallState.append(wallState)
			
			newDoorPositin = [0,0,0]
			newDoorPositin[0] = prePosition[0] + dispracement[0]
			newDoorPositin[1] = prePosition[1] + dispracement[1]
			newDoorPositin[2] = prePosition[2] + dispracement[2]
			
			if doorData[1] != None and pickingPosition != None:
				if roomNum == doorData[0] and wallState == 1:		#壁に配置
					floorPosition = self._RoomControlClass.GetFloorPosition(roomNum)
					
					point = [0,0]
					point[0] = newDoorPositin[0] - floorPosition[0]
					point[1] = newDoorPositin[2] - floorPosition[2]
					
					pointDistance = vizmat.Distance(point0,point1)
					point0Distance = vizmat.Distance(point,point0)
					percentage = point0Distance / pointDistance
					
					self._RoomControlClass.SetDoorPosition(doorData[0],doorData[1],wallNum,percentage)
					self._RoomControlClass.UpdateDoorList(doorData[0],doorData[1],wallNum,percentage)
					
				else:					#壁以外に配置
					offset = self._DoorOffsetList[x]
					doorPosition = [0,0,0]
					doorPosition[0] = pickingPosition[0] + offset[0][0]
					doorPosition[1] = pickingPosition[1] + offset[0][1]
					doorPosition[2] = pickingPosition[2] + offset[0][2]
					
					model.setPosition(doorPosition,mode=viz.ABS_GLOBAL)
					model.setEuler([0,0,0],mode=viz.ABS_GLOBAL)
					
					doorLine = self._RoomControlClass.GetDoorLineModel(doorData[0],doorData[1])
					lineModel = doorLine.GetModel()
					preLinePosition = lineModel.getPosition(mode=viz.ABS_GLOBAL)
					doorLinePosition = doorPosition
					doorLinePosition[1] = preLinePosition[1]
					lineModel.setPosition(doorLinePosition,mode=viz.ABS_GLOBAL)
					lineModel.setEuler([0,0,0],mode=viz.ABS_GLOBAL)
					
	#窓の移動
	def TranslateWindow(self,manipulatorState):
		dummy = self._SelectModelClass.GetDummyModel()
		dummyPosition = dummy.getPosition()
		position = [0.0,0.0,0.0]
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		if manipulatorState == 1:	#XZ平面で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position[0] = pickingPosition[0]
			position[1] = dummyPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 4:	#Y軸で移動
			position[0] = dummyPosition[0]
			position[1] = pickingPosition[1]
			position[2] = dummyPosition[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position[0] = dummyPosition[0]
			position[1] = dummyPosition[1]
			position[2] = pickingPosition[2]
			
		dispracement = [0,0,0]
		dispracement[0] = position[0] - dummyPosition[0]
		dispracement[1] = position[1] - dummyPosition[1]
		dispracement[2] = position[2] - dummyPosition[2]
		
		selectedWindowList = self._SelectModelClass.GetSelectedWindowList()
		
		#間取りをコピーする時はドア、窓の選択を解除
		roomList = self._SelectModelClass.GetSelectedRoomList()
		windowListTemp = []
		for windowData in selectedWindowList:
			state = True
			for room in roomList:
				if room == windowData[0]:
					state = False
			if state:
				windowListTemp.append(windowData)
		selectedWindowList = windowListTemp
		
		mousePosition = self._InputClass.GetMousePosition()
		#print 'mousePosition',mousePosition
		
		if self._TranslateState == True and self._PreTranslateState == False:
			for windowData in selectedWindowList:
				windowState = self._RoomControlClass.GetWindowState(windowData[0],windowData[1])
				self._UndoData.append([4,windowData[0],windowData[1],windowState[2],windowState[3]])
				
			self._WindowOffsetList = []
			self._WindowPrePosList = []
			for windowData in selectedWindowList:
				window = self._RoomControlClass.GetWindowModel(windowData[0],windowData[1])
				model = window.GetModel()
				
				box = model.getBoundingBox(mode=viz.ABS_GLOBAL)
				boxSize = box.getSize()
				boxCenter = box.getCenter()
				prePosition = boxCenter
				
				modelPos2d = self._OrthoViewWindow.worldToScreen( prePosition )
				dummyPos2d = self._OrthoViewWindow.worldToScreen( dummyPosition )
				
				offset3d = [0,0,0]
				offset3d[0] = prePosition[0] - dummyPosition[0]
				offset3d[1] = prePosition[1] - dummyPosition[1]
				offset3d[2] = prePosition[2] - dummyPosition[2]
				
				offset2d = [0,0]
				offset2d[0] = modelPos2d[0] - dummyPos2d[0]
				offset2d[1] = modelPos2d[1] - dummyPos2d[1]
				
				self._WindowOffsetList.append([offset3d,offset2d])
				
				position = model.getPosition(mode=viz.ABS_GLOBAL)
				euler = model.getEuler(mode=viz.ABS_GLOBAL)
				
				windowLine = self._RoomControlClass.GetWindowLineModel(windowData[0],windowData[1])
				lineModel = windowLine.GetModel()
				linePosition = lineModel.getPosition(mode=viz.ABS_GLOBAL)
				
				windowState = self._RoomControlClass.GetWindowState(windowData[0],windowData[1])
				self._WindowPrePosList.append([position,euler,linePosition,windowState[2],windowState[3]])
				
		self._WindowWallState = []
		for x in range(len(selectedWindowList)):
			windowData = selectedWindowList[x]
			window = self._RoomControlClass.GetWindowModel(windowData[0],windowData[1])
			model = window.GetModel()
			positionOffset = window.GetPositionOffset()
			
			box = model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()
			prePosition = boxCenter
			
			offset = self._WindowOffsetList[x]
			modelPos = [0,0]
			modelPos[0] = offset[1][0] + mousePosition[0]
			modelPos[1] = offset[1][1] + mousePosition[1]
			
			roomNum = None
			wallNum = None
			wallState = 0
			roomNum,wallNum,wallState = self._SelectWallClass.Select(False,onlyRoom=windowData[0],pos=modelPos)
			point0 = [0,0]
			point1 = [0,0]
			if wallState == 1:
				point0,point1 = self._RoomControlClass.GetWallPoint(roomNum,wallNum)
				
			self._WindowWallState.append(wallState)
			
			newWindowPositin = [0,0,0]
			newWindowPositin[0] = prePosition[0] + dispracement[0]
			newWindowPositin[1] = prePosition[1] + dispracement[1]
			newWindowPositin[2] = prePosition[2] + dispracement[2]
			
			if windowData[1] != None and pickingPosition != None:
				if roomNum == windowData[0] and wallState == 1:		#壁に配置
					floorPosition = self._RoomControlClass.GetFloorPosition(roomNum)
					
					point = [0,0]
					point[0] = newWindowPositin[0] - floorPosition[0]
					point[1] = newWindowPositin[2] - floorPosition[2]
					
					pointDistance = vizmat.Distance(point0,point1)
					point0Distance = vizmat.Distance(point,point0)
					percentage = point0Distance / pointDistance
					
					self._RoomControlClass.SetWindowPosition(windowData[0],windowData[1],wallNum,percentage)
					self._RoomControlClass.UpdateWindowList(windowData[0],windowData[1],2,0,wallNum,percentage)
					
				else:					#壁以外に配置
					
					offset = self._WindowOffsetList[x]
					windowPosition = [0,0,0]
					windowPosition[0] = pickingPosition[0] + offset[0][0]
					windowPosition[1] = pickingPosition[1] + offset[0][1]
					windowPosition[2] = pickingPosition[2] + offset[0][2]
					
					model.setPosition(windowPosition,mode=viz.ABS_GLOBAL)
					model.setEuler([0,0,0],mode=viz.ABS_GLOBAL)
					
					windowLine = self._RoomControlClass.GetWindowLineModel(windowData[0],windowData[1])
					lineModel = windowLine.GetModel()
					preLinePosition = lineModel.getPosition(mode=viz.ABS_GLOBAL)
					windowLinePosition = windowPosition
					windowLinePosition[1] = preLinePosition[1]
					lineModel.setPosition(windowLinePosition,mode=viz.ABS_GLOBAL)
					lineModel.setEuler([0,0,0],mode=viz.ABS_GLOBAL)
					
	#移動完了
	def FinTranslate(self):
		#間取り
		if self._SelectedRoom != None:
			selectedRoomList = self._SelectModelClass.GetSelectedRoomList()
			for x in range(len(selectedRoomList)):
				roomNum = selectedRoomList[x]
				roomPosition = self._RoomControlClass.GetFloorPosition(roomNum)
				
				self._RoomControlClass.SetFloorPosition(roomNum,roomPosition,withWall=True)
				
		#ドア
		if self._SelectedDoor != None:
			selectedDoorList = self._SelectModelClass.GetSelectedDoorList()
			
			#間取りをコピーする時はドア、窓の選択を解除
			roomList = self._SelectModelClass.GetSelectedRoomList()
			doorListTemp = []
			for doorData in selectedDoorList:
				state = True
				for room in roomList:
					if room == doorData[0]:
						state = False
				if state:
					doorListTemp.append(doorData)
			selectedDoorList = doorListTemp
			
			for x in range(len(selectedDoorList)):
				doorData = selectedDoorList[x]
				
				#壁に配置した時は壁を作り直す
				if self._DoorWallState[x] == 1:
					self._RoomControlClass.ReCreateWall(doorData[0])
					
				#壁以外の時は元の位置に戻す
				else:
					prePos = self._DoorPrePosList[x]
					
					door = self._RoomControlClass.GetDoorModel(doorData[0],doorData[1])
					model = door.GetModel()
					model.setPosition(prePos[0],mode=viz.ABS_GLOBAL)
					model.setEuler(prePos[1],mode=viz.ABS_GLOBAL)
					
					doorLine = self._RoomControlClass.GetDoorLineModel(doorData[0],doorData[1])
					lineModel = doorLine.GetModel()
					lineModel.setPosition(prePos[2],mode=viz.ABS_GLOBAL)
					lineModel.setEuler(prePos[1],mode=viz.ABS_GLOBAL)
					
					self._RoomControlClass.SetDoorPosition(doorData[0],doorData[1],prePos[3],prePos[4])
					self._RoomControlClass.UpdateDoorList(doorData[0],doorData[1],prePos[3],prePos[4])
					
		#窓
		if self._SelectedWindow != None:
			selectedWindowList = self._SelectModelClass.GetSelectedWindowList()
			
			#間取りをコピーする時はドア、窓の選択を解除
			roomList = self._SelectModelClass.GetSelectedRoomList()
			windowListTemp = []
			for windowData in selectedWindowList:
				state = True
				for room in roomList:
					if room == windowData[0]:
						state = False
				if state:
					windowListTemp.append(windowData)
			selectedWindowList = windowListTemp
			
			for x in range(len(selectedWindowList)):
				windowData = selectedWindowList[x]
				
				#壁に配置した時は壁を作り直す
				if self._WindowWallState[x] == 1:
					self._RoomControlClass.ReCreateWall(windowData[0])
					
				#壁以外の時は元の位置に戻す
				else:
					prePos = self._WindowPrePosList[x]
					
					window = self._RoomControlClass.GetWindowModel(windowData[0],windowData[1])
					model = window.GetModel()
					model.setPosition(prePos[0],mode=viz.ABS_GLOBAL)
					model.setEuler(prePos[1],mode=viz.ABS_GLOBAL)
					
					windowLine = self._RoomControlClass.GetWindowLineModel(windowData[0],windowData[1])
					lineModel = windowLine.GetModel()
					lineModel.setPosition(prePos[2],mode=viz.ABS_GLOBAL)
					lineModel.setEuler(prePos[1],mode=viz.ABS_GLOBAL)
					
					self._RoomControlClass.SetWindowPosition(windowData[0],windowData[1],prePos[3],prePos[4])
					self._RoomControlClass.UpdateWindowList(windowData[0],windowData[1],2,0,prePos[3],prePos[4])
					
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
		
	#移動状態の確認
	def GetTranslateState(self):
		state = self._TranslateState
		return state
		
	#2D表示のウィンドウの設定
	def SetOrthoViewWindow(self,window):
		self._OrthoViewWindow = window
		