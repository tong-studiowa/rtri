﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#キャラクター関係のファンクション制御のクラス定義
class CharacterFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterFunction] : Init CharacterFunction')
		
		self._InputClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._AddPathClass = None
		self._PlayPathClass = None
		self._PathControlControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._SelectedModel = None
		self._SelectedNode = None
		
	#使用するクラスの追加
	def AddClass(self,inputClass,characterControlClass,selectModelClass,addPathClass,playPathClass
				,pathControlControlClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._AddPathClass = addPathClass
		self._PlayPathClass = playPathClass
		self._PathControlControlClass = pathControlControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterFunction] : Start CharacterFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.Delete()
		elif num == 1:
			self.DeletePath()
			
	#選択が解除された
	def Hide(self):
		pass
		
	#削除
	def Delete(self):
		if self._AddPathClass.GetAddState() == False and self._PlayPathClass.GetPlayState() == False:
			self._UndoData = []
			self._UndoData.append(27)
			
			self._SelectedModel = self._SelectModelClass.GetSelectedCharacter()
			
			self._SelectModelClass.UnSelectCharacter()
			
			preFile		= self._CharacterControlClass.GetFileName(self._SelectedModel)
			prePos		= self._CharacterControlClass.GetPosition(self._SelectedModel)
			preOri		= self._CharacterControlClass.GetOrientation(self._SelectedModel)
			pathPosList	= self._CharacterControlClass.GetPathPositionList(self._SelectedModel)
			
			self._UndoData.append([self._SelectedModel,preFile,prePos,preOri,pathPosList])
			
			self._CharacterControlClass.Delete(self._SelectedModel)
			
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
	#パスを削除
	def DeletePath(self):
		if self._AddPathClass.GetAddState() == False and self._PlayPathClass.GetPlayState() == False:
			self._UndoData = []
			self._UndoData.append(34)
			
			self._SelectedModel = self._SelectModelClass.GetSelectedCharacter()
			
			pathPosList	= self._CharacterControlClass.GetPathPositionList(self._SelectedModel)
			self._UndoData.append([self._SelectedModel,pathPosList])
			
			self._CharacterControlClass.DeletePath(self._SelectedModel)
			
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			