﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム関係のファンクション制御のクラス定義
class ItemFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemFunction] : Init ItemFunction')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._SelectModelClass = None
		self._SetItemSizeButtonClass = None
		self._SetItemTextureButtonClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._AddPathClass = None
		self._PlayPathClass = None
		self._SetLightButtonClass = None
		
		self._SelectedModel = None
		self._SelectedNode = None
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,selectModelClass,setItemSizeButtonClass,setItemTextureButtonClass
				,undoClass,redoClass,addPathClass,playPathClass,setLightButtonClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._SelectModelClass = selectModelClass
		self._SetItemSizeButtonClass = setItemSizeButtonClass
		self._SetItemTextureButtonClass = setItemTextureButtonClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._AddPathClass = addPathClass
		self._PlayPathClass = playPathClass
		self._SetLightButtonClass = setLightButtonClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemFunction] : Start ItemFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.Delete()
			self._SetItemSizeButtonClass.SetVisible(False)
			self._SetItemTextureButtonClass.SetVisible(False)
			self._SetLightButtonClass.SetVisible(False)
		elif num == 1:
			itemList = self._SelectModelClass.GetSelectedModelList()
			if self._ItemControlClass.GetLightState(itemList[0]):	#照明のみ
				self.SetLight()
			else:
				self.Copy()
				self._SetItemSizeButtonClass.SetVisible(False)
				self._SetItemTextureButtonClass.SetVisible(False)
				self._SetLightButtonClass.SetVisible(False)
		elif num == 2:
			self.SetSize()
			self._SetItemTextureButtonClass.SetVisible(False)
			self._SetLightButtonClass.SetVisible(False)
		elif num == 3:
			self.SetTexture()
			self._SetItemSizeButtonClass.SetVisible(False)
			self._SetLightButtonClass.SetVisible(False)
	
	#選択が解除された
	def Hide(self):
		self._SetItemSizeButtonClass.SetVisible(False)
		self._SetItemTextureButtonClass.SetVisible(False)
		self._SetLightButtonClass.SetVisible(False)
		
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(14)
		
		self._SelectedModel = self._SelectModelClass.GetSelectedModel()
		itemList = self._SelectModelClass.GetSelectedModelList()
		#print 'deleteItemList=',itemList

		self._SelectModelClass.UnSelectItem()
		
		if itemList != []:
			for x in range(len(itemList)):
				itemNum = itemList[x]
				#print 'SetUndoData=',itemNum
				
				file		= self._ItemControlClass.GetFileName(itemNum)
				pos			= self._ItemControlClass.GetPosition(itemNum)
				ori			= self._ItemControlClass.GetOrientation(itemNum)
				size		= self._ItemControlClass.GetSize(itemNum)
				moveY		= self._ItemControlClass.GetMoveYState(itemNum)
				texList		= self._ItemControlClass.GetTextureList(itemNum)
				pathPosList	= self._ItemControlClass.GetPathPositionList(itemNum)

				self._UndoData.append([itemNum,file,pos,ori,size,moveY,texList,pathPosList])
				
			for x in range(len(itemList)):
				itemNum = itemList[x] - x
				#print 'DeleteItem=',itemNum
				self._ItemControlClass.Delete(itemNum)
				
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#コピー
	def Copy(self):
		self._UndoData = []
		self._UndoData.append(13)
		
		itemList = self._SelectModelClass.GetSelectedModelList()
		
		count = len(itemList)
		for itemNum in itemList:
			self._ItemControlClass.Copy(itemNum)
			
			newItemNum = self._ItemControlClass.GetItemCount() - 1
			self._UndoData.append([newItemNum])
			
		#print 'copyItem=',self._UndoData
		
		#コピー先を選択
		newItemList = []
		for x in range(count):
			itemCount = self._ItemControlClass.GetItemCount()
			newItemNum = (itemCount)-(count-x)
			newItemList.append(newItemNum)
		self._SelectModelClass.ReSelect(newItemList)
		
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		
	#パスを削除
	def DeletePath(self):
		if self._AddPathClass.GetAddState() == False and self._PlayPathClass.GetPlayState() == False:
			self._UndoData = []
			self._UndoData.append(34)
			
			self._SelectedModel = self._SelectModelClass.GetSelectedModel()
			
			pathPosList	= self._ItemControlClass.GetPathPositionList(self._SelectedModel)
			self._UndoData.append([self._SelectedModel,pathPosList,True])
			
			self._ItemControlClass.DeletePath(self._SelectedModel)
			
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
	#サイズ変更
	def SetSize(self):
		if self._SetItemSizeButtonClass.GetVisible():
			self._SetItemSizeButtonClass.SetVisible(False)
		else:
			self._SelectedModel = self._SelectModelClass.GetSelectedModel()
			size = self._ItemControlClass.GetSize(self._SelectedModel)
			self._SetItemSizeButtonClass.SetValue(self._SelectedModel,size)
			
			self._SetItemSizeButtonClass.SetVisible(True)
			
	#テクスチャ変更
	def SetTexture(self):
		if self._SetItemTextureButtonClass.GetVisible():
			self._SetItemTextureButtonClass.SetVisible(False)
		else:
			self._SetItemTextureButtonClass.SetVisible(True)
			
			self._SelectedModel = self._SelectModelClass.GetSelectedModel()
			self._SelectedNode = self._SelectModelClass.GetSelectedNode()
			
			if self._SelectedNode[:9] == 'DummyBox_':
				self._SelectedNode = 'DummyBox_Top'
				
			texture = self._ItemControlClass.GetTexture(self._SelectedModel,self._SelectedNode)
			
			self._SetItemTextureButtonClass.SetValue(self._SelectedModel,self._SelectedNode,texture)
			
	#照明のパラメータを設定
	def SetLight(self):
		if self._SetLightButtonClass.GetVisible():
			self._SetLightButtonClass.SetVisible(False)
		else:
			self._SelectedModel = self._SelectModelClass.GetSelectedModel()
			lightParam = self._ItemControlClass.GetLightParam(self._SelectedModel)
			self._SetLightButtonClass.SetValue(self._SelectedModel,lightParam)
			
			self._SetLightButtonClass.SetVisible(True)
			