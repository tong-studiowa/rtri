﻿# AddDoor
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/1
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d
import LoadIniFile

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドア追加時のドラッグ用モデル制御のクラス定義
class AddDoor(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddDoor] : Init AddDoor')
		
		self._RoomControlClass = None
		self._ModelControlClass = None
		
		self._Model = None
		self._LineModel = None
		self._Picking = False
		
		self._Width = 0.15
		self._WallHeight = 3	#壁の高さ
		
		self._Percentage = 0.0
		self._PositionOffset = [0.0,0.0,0.0]
		
		self._TranslateSnap = translateSnap
		
	#使用するクラスの追加
	def AddClass(self,roomControlClass,modelControlClass):
		self._RoomControlClass = roomControlClass
		self._ModelControlClass = modelControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddDoor] : Start AddDoor Update')
		pass
	
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		#モデル設定
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = self._ModelControlClass.Add(filePath)	#モデルを追加
		
		self._Model = model
		
		self._Model.visible(viz.OFF)
		self._Model.disable(viz.PICKING)
		
		
		#ラインモデル設定
		box = self._Model.getBoundingBox()
		modelSize = box.size
		modelCenter = box.getCenter()
		
		lineModel = vizshape.addPlane([1.0,1.0])
		lineModel.setScale([modelSize[0],1.0,self._Width])
		lineModel.setPosition(modelCenter[0],self._WallHeight,modelCenter[2])
		
		self._LineModel = lineModel
		
		#その他設定
		self.SetPositionOffset()
		self.SetPickable(False)
		self.SetHighlight(True)
			
	#削除
	def Delete(self):
		if self._Model != None:
			self._LineModel.remove()	#ラインモデルを削除
			self._Model.remove()		#ドアモデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition,modelNum,wallState,point0,point1,roomNum):
		if self._Model != None and iPosition != None:
			position = iPosition
			
			#スナップ
			position[0] = self.Snap(position[0])
			position[2] = self.Snap(position[2])
			
			if wallState == 1:		#壁に配置
				floorPosition = [0,0,0]
				floorPositionTemp = self._RoomControlClass.GetFloorPosition(roomNum)
				floorPosition[0] = floorPositionTemp[0]
				floorPosition[1] = floorPositionTemp[1]
				floorPosition[2] = floorPositionTemp[2]
				
				point = [0,0]
				point[0] = position[0] - floorPosition[0]
				point[1] = position[2] - floorPosition[2]
				
				pointDistance = vizmat.Distance(point0,point1)
				point0Distance = vizmat.Distance(point,point0)
				
				percentage = point0Distance / pointDistance
				
				self._Percentage = percentage
				self.SetModelPosition(point0,point1,self._Percentage,roomNum)
				
			else:					#壁以外に配置
				box = self._Model.getBoundingBox()
				modelSize = box.size
				
				position[1] = modelSize[1] * 0.5
				
				self._Model.setPosition(position)
				self._Model.setEuler(0,0,0)
				
				position[1] = 92.0
				self._LineModel.setPosition(position)
				self._LineModel.setEuler(0,0,0)
	
	#位置取得
	def GetPosition(self):
		position = [0,0,0]
		
		if self._Model != None:
			position = self._Model.getPosition()
		
		return position
	
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._LineModel.color(viz.RED)
			self._LineModel.emissive(viz.RED)
			self._LineModel.ambient(viz.RED)
			
		else:
			self._LineModel.color(viz.WHITE)
			self._LineModel.emissive(viz.BLACK)
			self._LineModel.ambient(viz.BLACK)
		
	#選択可能かの設定
	def SetPickable(self,state):
		if state == True:
			self._LineModel.enable(viz.PICKING)
			
		elif state == False:
			self._LineModel.disable(viz.PICKING)
	
	#ドアの位置設定
	def SetModelPosition(self,point0,point1,percentage,roomNum):
		position = [0,0,0]
		
		box = self._Model.getBoundingBox()
		modelSize = box.size
				
		positionY = modelSize[1] * 0.5
				
		position[0] = point0[0]*(1-percentage)+point1[0]*percentage
		position[1] = positionY
		position[2] = point0[1]*(1-percentage)+point1[1]*percentage
		
		#床の位置だけオフセット
		floorPosition = [0,0,0]
		floorPositionTemp = self._RoomControlClass.GetFloorPosition(roomNum)
		floorPosition[0] = floorPositionTemp[0]
		floorPosition[1] = floorPositionTemp[1]
		floorPosition[2] = floorPositionTemp[2]
				
		position [0] = position[0] + floorPosition[0]
		position [1] = position[1] + floorPosition[1]
		position [2] = position[2] + floorPosition[2]
		
		self._Model.setPosition(position)
		
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		self._Model.setEuler(wallAngle-90.0,0.0,0.0)
		
		position[1] = 92.0
		self._LineModel.setPosition(position)
		self._LineModel.setEuler(wallAngle-90.0,0.0,0.0)
		
	#壁上のドアの位置取得
	def GetPercentage(self):
		percentage = self._Percentage
		
		return percentage
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#オフセット設定
	def SetPositionOffset(self):
		if self._Model != None:
			self._PositionOffset = [0.0,0.0,0.0]
			offset = [0.0,0.0,0.0]
			
			box = self._Model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()

			#offset = boxCenter

			offset[0] = boxCenter[0] * -1
			offset[1] = (boxCenter[1] - boxSize[1] / 2) * -1
			offset[2] = boxCenter[2] * -1
			
			self._PositionOffset[0] = offset[0]
			self._PositionOffset[1] = offset[1]
			self._PositionOffset[2] = offset[2]
	
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
