﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数
DISPLAY_SIZE_WIDTH	= 1024										#ディスプレイサイズｘ
DISPLAY_SIZE_HEIGHT = 768										#ディスプレイサイズｙ
DISPLAY_FULL_SCALE_WIDTH	= 12.8								#フルスクリーンにする際のスケール値ｘ
DISPLAY_FULL_SCALE_HEIGHT	= 10.4								#フルスクリーンにする際のスケール値ｙ
SCALE_VALUE_X = DISPLAY_FULL_SCALE_WIDTH/DISPLAY_SIZE_WIDTH		#スケール補正値ｘ
SCALE_VALUE_Y = DISPLAY_FULL_SCALE_HEIGHT/DISPLAY_SIZE_HEIGHT	#スケール補正値ｙ

# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Object2d(Interface.Interface):
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass

	#BGフレームの作成
	def CreateBgFrame(self,position,scale):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.color(viz.GRAY)
		frame.drawOrder(6)
		frame.alpha(0.75)
		
		self.SetIconPosition(frame,position,scale)
		
		return frame
		
	#フレームの作成
	def CreateFrame(self,position,scale,order=None):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.color(viz.GRAY)
		
		if order != None:
			frame.drawOrder(order)
		else:
			frame.drawOrder(7)
		
		self.SetIconPosition(frame,position,scale)
		
		return frame
		
	#アイコンの作成
	def CreateIcon(self,position,scale):
		icon = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		icon.color(viz.GRAY)
		icon.drawOrder(8)
		
		self.SetIconPosition(icon,position,scale)
		
		return icon
		
	#アイコンの作成
	def CreateTexIcon(self,texPath,position,scale,order=None):
		icon = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		icon.color(viz.WHITE)
		
		if order != None:
			icon.drawOrder(order)
		else:
			icon.drawOrder(15)
		
		if texPath != None:
			tex = viz.addTexture(texPath)
			icon.texture(tex)
	
		self.SetIconPosition(icon,position,scale)
		
		return icon
		
	#テキストの作成
	def CreateText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		text.color(viz.WHITE)
		text.drawOrder(9)
		
		self.SetIconPosition(text,position,scale)
		
		return text
		
	#テキストボックスの作成
	def CreateTextBox(self,position,scale):
		textBox = viz.addTextbox()
		textBox.color(viz.GRAY)
		textBox.drawOrder(9)
		textBox.length(1)
		textBox.overflow(viz.OVERFLOW_GROW)
		
		self.SetIconPosition(textBox,position,scale,True)
		
		return textBox
		
	#ダミーテキストの作成
	def CreateDummyText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_LEFT_CENTER)
		text.color([0.6,0.6,0.6])
		text.drawOrder(9)
		
		self.SetIconPosition(text,position,scale)
		
		return text
		
	#スライダーのフレーム作成
	def CreateSliderFrame(self,position,scale):
		sliderFrame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		sliderFrame.color([0.8,0.8,0.8])
		sliderFrame.drawOrder(7)
		
		self.SetIconPosition(sliderFrame,position,scale)
		
		return sliderFrame
		
	#スライダーの作成
	def CreateSlider(self,position,scale):
		slider = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		slider.color(viz.GRAY)
		slider.drawOrder(8)
		
		self.SetIconPosition(slider,position,scale)
		
		return slider
		
	#チェックボックスの作成
	def CreateCheckBox(self,position,scale,tex):
		checkBox = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		checkBox.color(viz.WHITE)
		checkBox.texture(tex)
		checkBox.drawOrder(8)
		
		self.SetIconPosition(checkBox,position,scale)
		
		return checkBox
		
	#チェックボックスの作成
	def SetCheckBoxTexture(self,checkBox,tex):
		checkBox.texture(tex)
		
	#アイコンの位置設定
	def SetIconPosition(self,icon,position,scale,textBox=False):
		windowSize  = viz.window.getSize()
		
		posX = None
		if windowSize[0] != 0:
			posX = float(position[0])/windowSize[0]
		else:
			posX = float(position[0])/1
			
		posY = None
		if windowSize[1] != 0:
			posY = (float(windowSize[1])-float(position[1]))/windowSize[1]
		else:
			posY = (float(windowSize[1])-float(position[1]))/1
		
		scaleX = None
		if windowSize[0] != 0:
			scaleX = float(scale[0]*float(DISPLAY_SIZE_WIDTH)  / windowSize[0])*SCALE_VALUE_X
		else:
			scaleX = float(scale[0]*float(DISPLAY_SIZE_WIDTH)  / 1)*SCALE_VALUE_X
			
		scaleY = None
		if windowSize[1] != 0:
			scaleY = float(scale[1]*float(DISPLAY_SIZE_HEIGHT) / windowSize[1])*SCALE_VALUE_Y
		else:
			scaleY = float(scale[1]*float(DISPLAY_SIZE_HEIGHT) / 1)*SCALE_VALUE_Y
		
		if icon:
			if textBox:
				icon.setPosition(posX,posY)
				icon.setScale(scaleX*0.33,scaleY*2.6)
			else:
				icon.setPosition(posX,posY)
				icon.setScale(scaleX,scaleY)
			
	#マウスのロールオーバーの確認
	def CheckRollOver(self,mousePosition,position,scale):
		res = False
		windowSize  = viz.window.getSize()
		
		mousePosX = mousePosition[0]*windowSize[0]
		mousePosY = (1.0-mousePosition[1])*windowSize[1]
		
		if mousePosX > (position[0]-scale[0]/2) and mousePosX < (position[0]+scale[0]/2):
			if mousePosY > (position[1]-scale[1]/2) and mousePosY < (position[1]+scale[1]/2):
				res = True
		
		return res
		