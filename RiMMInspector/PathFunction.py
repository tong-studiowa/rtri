﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#パス関係のファンクション制御のクラス定義
class PathFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathFunction] : Init PathFunction')
		
		self._InputClass = None
		self._PathControlClass = None
		self._SelectPathClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._SelectedCharacter = None
		self._SelectedModel = None
		self._IsItem = False
		
	#使用するクラスの追加
	def AddClass(self,inputClass,PathControlClass,selectPathClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._PathControlClass = PathControlClass
		self._SelectPathClass = selectPathClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][PathFunction] : Start PathFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.Delete()
	
	#選択が解除された
	def Hide(self):
		pass
		
	#削除
	def Delete(self):
		self._UndoData = []
		self._UndoData.append(31)
		
		self._SelectedCharacter,self._SelectedModel,self._IsItem = self._SelectPathClass.GetSelectedModel()
		
		prePos  = self._PathControlClass.GetPosition(self._SelectedCharacter,self._SelectedModel,self._IsItem)
		
		self._SelectPathClass.UnSelect()
		self._PathControlClass.Delete(self._SelectedCharacter,self._SelectedModel,self._IsItem)
		
		self._UndoData.append([self._SelectedCharacter,self._SelectedModel,prePos,self._IsItem])
		self._UndoClass.AddToList(self._UndoData)
		self._RedoClass.ClearList()
		