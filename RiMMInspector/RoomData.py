﻿# coding: utf-8

import viz
import vizmat
import vizshape
import vizinput
import math
import LoadIniFile
from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#間取り読み込み用クラス定義
class RoomData():
	#コンストラクタ
	def __init__(self,roomControl):
		OutputDebugString('[STUDIOWA][INFO][LoadRoomDataClass] : Init LoadRoomDataClass')
		self._RoomControlClass = roomControl
		
		"""
		self._RoomControlClass = RoomControl()
		#self._RoomControlClass.Load(SETTING_FOLDER_PATH + fileName)
		self._RoomControlClass.Load(fileName)							#絶対パスのみ
		"""
		
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][LoadRoomDataClass] : del LoadRoomDataClass')
		"""
		modelCount = self._RoomControlClass.GetRoomCount()
		for x in range(modelCount):
			self._RoomControlClass.Delete(0)
		
		self._RoomControlClass.DeleteDrawing()
		"""
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadRoomDataClass] : Start LoadRoomDataClass Update')
		pass
	
	#読み込み
	def Load(self,filePath):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadRoomDataClass] : Start LoadRoomDataClass Load')
		self._RoomControlClass.Load(filePath)
	
	#保存
	def Save(self,filePath):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Save')
		self._RoomControlClass.Save(filePath)
		
	#間取り数取得
	def GetRoomCount(self):
		count = self._RoomControlClass.GetRoomCount()
		return count
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._RoomControlClass.GetDoorModelList()
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = self._RoomControlClass.GetModelList()
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = self._RoomControlClass.GetCollisionModelList()
		return modelList
		
	#スタート位置、角度を取得
	def GetStartPosition(self):
		startPos = [0,0,0]
		startAngle = [0,0,0]
		startPos,startAngle = self._RoomControlClass.GetStartPosition()
		return startPos,startAngle
	
	#読み込みに成功したかどうかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._RoomControlClass.IsSuccessLoading()
	
	#図面のモデルを取得
	def GetDrawingModel(self):
		model = self._RoomControlClass.GetDrawingModel()
		return model
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		scale = self._RoomControlClass.GetDrawingScale()
		return scale
		
	#図面の表示状態を設定
	def SetDrawingVisible(self,state):
		self._RoomControlClass.SetDrawingVisible(state)
		
	#パスシミュレータでの天井の表示切り替え
	def CeilingVisibleUpdate(self):
		self._RoomControlClass.CeilingVisibleUpdate()
		