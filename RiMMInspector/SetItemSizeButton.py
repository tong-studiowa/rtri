﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object2d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテムのサイズ設定ボタン制御のクラス定義
class SetItemSizeButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemSizeButton] : Init SetItemSizeButton')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._OverState = False
		
		self._VisibleState = True
		
		self._IconCount = 3
		
		self._Frame = None
		self._TitleList = []
		self._TextBoxList = []
		self._DummyTextList = []
		
		self._SelectedItem = None
		self._Value = [1.0,1.0,1.0]
		
		self._MinValue = 0.001
		
		self._SelectingTextBox = 0
		self._PreKeyState = 0
		self._PreForcus = -1
		
		#フレーム追加
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [709,windowSize[1]-127]
		self._FrameScale = [290,127]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#タイトル追加
		self._TitlePositionX = 608
		self._TitlePositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		self._TitleScale = [18.6,18.6]
		self._TitleMessage = ['幅','奥行き','高さ']
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TitlePositionX
			position[1] = self._TitlePositionY[x]
			
			message = self._TitleMessage[x]
			
			title = self.CreateText(message,position,self._TitleScale)
			self._TitleList.append(title)
			
		#テキストボックス追加
		self._TextBoxPositionX = 751
		self._TextBoxPositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		self._TextBoxScale = [200,36]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextBoxPositionX
			position[1] = self._TextBoxPositionY[x]
			
			textbox = self.CreateTextBox(position,self._TextBoxScale)
			self._TextBoxList.append(textbox)
			
		self.SetText()
		
		#ダミーテキスト追加
		self._DummyTextPositionX = 661
		self._DummyTextPositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		self._DummyTextScale = [20,27]
		self._DummyTextMessage = ['','','']
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._DummyTextPositionX
			position[1] = self._DummyTextPositionY[x]
			
			message = self._DummyTextMessage[x]
			
			title = self.CreateDummyText(message,position,self._DummyTextScale)
			self._DummyTextList.append(title)
			
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetItemSizeButton] : Start SetItemSizeButton Update')
		
		self._OverState = False
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
		self.InputVal()
		
		#print 'focus',self.GetTextBoxForcus()
		
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
			
			#表示したら一番上にフォーカス
			self._SelectingTextBox = 0
			self.SetTextBoxForcus(self._SelectingTextBox)
			self._SelectingTextBox += 1
			
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		for title in self._TitleList:
			title.visible(val)
		for textbox in self._TextBoxList:
			textbox.visible(val)
		for dummyText in self._DummyTextList:
			dummyText.visible(val)
			
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#数値を設定
	def SetValue(self,itemNum,val):
		self._SelectedItem = itemNum
		self._Value = val
		self.SetText()
		
	#数値を取得
	def GetValue(self):
		itemNum = self._SelectedItem
		val = self._Value
		
		return itemNum,val
		
	#数値の表示を更新
	def SetText(self):
		for x in range(self._IconCount):
			#横、奥行、高さの順に
			y = 0
			if x == 0:
				y = 0
			elif x == 1:
				y = 2
			elif x == 2:
				y = 1
				
			val = self._Value[y] * 1000.0	#表示する時はメートル → ミリメートル
			message = str(round(val,3))
			self._TextBoxList[x].message(message)
			
	#ダミーテキストを表示
	def SetDummyText(self,num):
		#横、奥行、高さの順に
		y = 0
		if num == 0:
			y = 0
		elif num == 1:
			y = 2
		elif num == 2:
			y = 1
			
		val = self._Value[y] * 1000.0	#表示する時はメートル → ミリメートル
		message = str(round(val,3))
		self._DummyTextList[num].message(message)
		
		self._TextBoxList[num].message('')
		
	#文字入力
	def InputVal(self):
		keyState = 0
		forcus = -1
		
		if self._VisibleState == True:
			if viz.key.isDown(viz.KEY_RETURN):
				keyState = 1
			if viz.key.isDown(viz.KEY_KP_ENTER):
				keyState = 1
			if viz.key.isDown(viz.KEY_TAB):
				keyState = 2
				
			if keyState == self._PreKeyState:
				pass
				
			elif keyState != 0:
				preItem,preVal = self.GetValue()
				
				inputVal1Temp = self._TextBoxList[0].getMessage()
				inputVal2Temp = self._TextBoxList[2].getMessage()
				inputVal3Temp = self._TextBoxList[1].getMessage()
				
				inputVal1 = 0.0
				try:
					inputVal1 = float(inputVal1Temp) * 0.001	#入力時はミリメートル → メートル
				except:
					inputVal1 = preVal[0]
				if inputVal1 == 0.0:
					inputVal1 = self._MinValue
				elif inputVal1 < 0:
					inputVal1 = preVal[0]
					
				inputVal2 = 0.0
				try:
					inputVal2 = float(inputVal2Temp) * 0.001	#入力時はミリメートル → メートル
				except:
					inputVal2 = preVal[1]
				if inputVal2 == 0.0:
					inputVal2 = self._MinValue
				elif inputVal2 < 0:
					inputVal2 = preVal[1]
					
				inputVal3 = 0.0
				try:
					inputVal3 = float(inputVal3Temp) * 0.001	#入力時はミリメートル → メートル
				except:
					inputVal3 = preVal[2]
				if inputVal3 == 0.0:
					inputVal3 = self._MinValue
				elif inputVal3 < 0:
					inputVal3 = preVal[2]
					
				inputVal = [inputVal1,inputVal2,inputVal3]
				val = [1.0,1.0,1.0]
				
				try:
					val = inputVal
				except:
					val = preVal
					
				self.SetValue(preItem,val)
				
				if val != preVal:
					self._ItemControlClass.SetSize(self._SelectedItem,self._Value)
					
					self._UndoData = []
					self._UndoData.append(2)
					self._UndoData.append([self._SelectedItem,preVal])
					self._UndoClass.AddToList(self._UndoData)
					self._RedoClass.ClearList()
					
				if keyState == 2 and self._PreKeyState != 2:
					self._SelectingTextBox = self.GetTextBoxForcus() + 1
					if self._SelectingTextBox > 2:
						self._SelectingTextBox = 0
						
					self.SetTextBoxForcus(self._SelectingTextBox)
					
			forcus = self.GetTextBoxForcus()
			if forcus != self._PreForcus and keyState == 0:
				preItem,preVal = self.GetValue()
				self.SetValue(preItem,preVal)
				
				if forcus != -1:
					self.SetDummyText(forcus)
					
			inputVal1Temp = self._TextBoxList[0].getMessage()
			if len(inputVal1Temp) != 0:
				self._DummyTextList[0].message('')
				
			inputVal2Temp = self._TextBoxList[2].getMessage()
			if len(inputVal2Temp) != 0:
				self._DummyTextList[2].message('')
				
			inputVal3Temp = self._TextBoxList[1].getMessage()
			if len(inputVal3Temp) != 0:
				self._DummyTextList[1].message('')
				
		self._PreKeyState = keyState
		self._PreForcus = self.GetTextBoxForcus()
		
	#テキストボックスのフォーカスを設定
	def SetTextBoxForcus(self,num):
		self.SetDummyText(num)
		self._TextBoxList[num].setFocus(viz.ON)
			
	def GetTextBoxForcus(self):
		res = -1
		for x in range(self._IconCount):
			state = self._TextBoxList[x].getFocus()
			if state:
				res = x
		
		return res
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._FramePosition = [709,windowSize[1]-127]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self._TitlePositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		self._TextBoxPositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		self._DummyTextPositionY = [windowSize[1]-168,windowSize[1]-127,windowSize[1]-86]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TitlePositionX
			position[1] = self._TitlePositionY[x]
			
			self.SetIconPosition(self._TitleList[x],position,self._TitleScale)
			
			position[0] = self._TextBoxPositionX
			position[1] = self._TextBoxPositionY[x]
			
			self.SetIconPosition(self._TextBoxList[x],position,self._TextBoxScale,True)
			
			position = [0.0,0.0]
			position[0] = self._DummyTextPositionX
			position[1] = self._DummyTextPositionY[x]
			
			self.SetIconPosition(self._DummyTextList[x],position,self._DummyTextScale)
			