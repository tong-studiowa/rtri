﻿# coding: utf-8

import viz
import vizinput
import vizmat
import Interface
import Room
import math
import os.path
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋制御のクラス定義
class RoomControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Init RoomControl')
		
		self._InputClass = None
		self._ChangeViewClass = None
		self._StartPositionClass = None
		self._DrawingClass = None
		self._ModelControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		self.__mViewVR = None
		
		self._DefaultRoomFile = SETTING_FOLDER_PATH + "Room_Default.ini"
		#self._DefaultRoomFile = SETTING_FOLDER_PATH + "Room_Default_Test.ini"
		
		self._RoomIniFileName = ""
		self._RoomIniFile = None
		self._LoadErrorFlag = False	#takahashi 2014/8/1
		
		self._RoomCount = 0
		self._RoomList = []
		
		self._StartPosition = [0,0,0]
		self._StartAngle = [0,0,0]
		
		#self._Drawing = None
		self._DrawingFile = ''
		self._DrawingScale = 10
		
		self._Is3d = False
		
		self._DummyDoorStateList = []
		self._DummyWindowStateList = []
		
		self._DefaultFloorTexPath = ''
		self._DefaultWallTexPath = ''
		self._DefaultCeilingTexPath = ''
		self._DefaultWallHight = 3.0
		self._DefaultWindowHeight  = 1.2
		
	#使用するクラスの追加
	def AddClass(self,inputClass,changeViewClass,startPositionClass,drawingClass
				,modelControlClass,undoClass,redoClass, viewVRClass):
		self._InputClass = inputClass
		self._ChangeViewClass = changeViewClass
		self._StartPositionClass = startPositionClass
		self._DrawingClass = drawingClass
		self._ModelControlClass = modelControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self.__mViewVR = viewVRClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Start RoomControl Update')
		pass
	
	#リセット
	def Reset(self):
		pass
	
	#読み込み
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Load')
		
		if self._Is3d:
			self.DeleteAllDoorHole()		#ダミードアを削除
			self.DeleteAllWindowHole()		#ダミー窓を削除
		
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		room0 = iniFile.GetItemData('Rooms','0')
		del iniFile
		
		if room0 == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルは間取り定義ファイルではありません。', title='RiMMInspector')
			self._LoadErrorFlag=True	#takahashi 2014/8/1
		
		else:
			#全部屋を削除
			self.DeleteAll()
			
			self._RoomIniFileName = fileName
			
			self.Create(fileName)
			
			#Undoの情報をクリア
			self._UndoClass.ClearList()
			self._RedoClass.ClearList()
		
		#表示関係を設定
		self.SetRoomVisibleState()
		
	#保存
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Save')
		
		if self._Is3d:
			self.DeleteAllDoorHole()		#ダミードアを削除
			self.DeleteAllWindowHole()		#ダミー窓を削除
		
		#保存用iniファイルの作成
		self._SaveIniFile = LoadIniFile.LoadIniFileClass()
		self._SaveIniFile.ResetIniData()
		
		self._SaveIniFile.AddSection('Define')
		
		#スタート位置関係
		self.SetStartPositionFromModel()
		
		satartPos = str(self._StartPosition[0]) + ',' + str(self._StartPosition[1]) + ',' + str(self._StartPosition[2])
		self._SaveIniFile.AddItem('Define','satartposition',satartPos)
		
		satartAngle= str(self._StartAngle[0]) + ',' + str(self._StartAngle[1]) + ',' + str(self._StartAngle[2])
		self._SaveIniFile.AddItem('Define','satartangle',satartAngle)
		
		#図面関係
		self.GetDrawing()
		
		if self._DrawingFile != '' and self._DrawingFile != None:
			self._SaveIniFile.AddItem('Define','drawingfile',self._DrawingFile)
			self._SaveIniFile.AddItem('Define','drawingscale',str(self._DrawingScale))
		
		#間取り関係
		self._SaveIniFile.AddSection('Rooms')
		
		self._SaveIniFile.AddItem('Rooms','Total',str(self._RoomCount))
		
		for x in range(self._RoomCount):
			room = self._RoomList[x]
			roomName = 'Room'+str(x)
			
			self._SaveIniFile.AddItem('Rooms',str(x),roomName)
			
			#各部屋の情報を追加
			self._SaveIniFile.AddSection(roomName)
			
			#位置
			positionTemp = room.GetFloorPosition()
			position = str(positionTemp[0])
			for x in range(2):
				position = position + ',' + str(positionTemp[x+1])
			self._SaveIniFile.AddItem(roomName,'Position',position)
			
			#角度
			#angleTemp = room.GetAxisAngle()
			angleTemp = [0,0,0]
			angle = str(angleTemp[0])
			self._SaveIniFile.AddItem(roomName,'Angle',angle)
			
			#ポイント
			floorPointList = room.GetWallPointList()
			wallVisibleList = room.GetWallVisibleList()
			for x in range(len(floorPointList)):
				pointName = 'Point' + str(x)
				pointTemp = floorPointList[x]
				
				visibleState = 0
				if wallVisibleList[x]:
					visibleState = 1
					
				point = str(pointTemp[0]) + ',' + str(pointTemp[1]) + ',' + str(visibleState)
				self._SaveIniFile.AddItem(roomName,pointName,point)
				
			#壁の高さ
			wallHeightTemp = room.GetWallHeight()
			wallHeight = str(wallHeightTemp)
			self._SaveIniFile.AddItem(roomName,'WallHeight',wallHeight)
			
			#床のテクスチャ
			floorTextureFile,floorTextureSize = room.GetFloorTextureSetting()
			floorTexture = floorTextureFile + ',' + str(floorTextureSize)
			self._SaveIniFile.AddItem(roomName,'FloorTexture',floorTexture)
			
			#壁のテクスチャ
			wallTextureFile,wallTextureSize = room.GetWallTextureSetting()
			wallTexture = wallTextureFile + ',' + str(wallTextureSize)
			self._SaveIniFile.AddItem(roomName,'WallTexture',wallTexture)
			
			#天井のテクスチャ
			ceilingTextureFile,ceilingTextureSize = room.GetCeilingTextureSetting()
			ceilingTexture = ceilingTextureFile + ',' + str(ceilingTextureSize)
			self._SaveIniFile.AddItem(roomName,'CeilingTexture',ceilingTexture)
			
			#ドア
			doorCount = room.GetDoorCount()
			for x in range(doorCount):
				doorName = 'Door'+str(x)
				dataTemp = room.GetDoorState(x)
				data = str(dataTemp[0]) + ',' + str(dataTemp[1]) + ',' + str(dataTemp[2])
				self._SaveIniFile.AddItem(roomName,doorName,data)
				
			#窓
			windowCount = room.GetWindowCount()
			for x in range(windowCount):
				windowName = 'Window'+str(x)
				dataTemp = room.GetWindowState(x)
				data = str(dataTemp[0]) + ',' + str(dataTemp[1]) + ',' + str(dataTemp[2]) + ',' + str(dataTemp[3])
				self._SaveIniFile.AddItem(roomName,windowName,data)
				
			#オペ室かどうか
			opRoomState = room.GetOpRoomState()
			self._SaveIniFile.AddItem(roomName,'OpRoom',str(opRoomState))
			
		self._SaveIniFile.WriteIniData(fileName)
		self._RoomIniFileName = fileName
		
		#表示関係を設定
		self.SetRoomVisibleState()
		
	#作成
	def Create(self,fileName):
		self.LoadIniFile(fileName)
		
		self._RoomCount = 1000
		
		total = self._RoomIniFile.GetItemData('Rooms','Total')
		if total != None:
			self._RoomCount = int(total)
			
		list = []
		for x in range(self._RoomCount):
			roomName = self._RoomIniFile.GetItemData('Rooms',str(x))
			if roomName !=None:
				room = Room.Room(self._ModelControlClass)
				room.Create(self._RoomIniFile,fileName,roomName)	#レイアウトを作成
				room.SetViewState(self._Is3d)
				
				room.SetDoorLineVisible(False)
				room.SetWindowLineVisible(False)
				
				list.append(room)
				
			else:
				self._RoomCount = x
				break
				
		self._RoomList = list
		
		#スタート位置の設定
		self.SetStartPosition()
		
		#図面の設定
		#self._Drawing = Drawing()
		self.SetDrawing()
		
	#Undoで再作成
	def ReCreate(self,data):
		self.LoadIniFile(data[1])
		
		roomName = self._RoomIniFile.GetItemData('Rooms',str('0'))
		room = Room.Room(self._ModelControlClass)
		room.ReCreate(self._RoomIniFile,roomName,data)	#レイアウトを作成
		room.SetViewState(self._Is3d)
		
		self._RoomList.append(room)
		num = len(self._RoomList) - 1
		
		self._RoomCount = len(self._RoomList)
		
		return num
		
	#追加
	def Add(self,fileName):
		self.LoadIniFile(fileName)
		
		roomName = self._RoomIniFile.GetItemData('Rooms',str('0'))
		room = Room.Room(self._ModelControlClass)
		room.SetDefaultSettings(self._DefaultFloorTexPath,self._DefaultWallTexPath,self._DefaultCeilingTexPath,self._DefaultWallHight)
		room.Create(self._RoomIniFile,fileName,roomName,add=True)	#レイアウトを作成
		room.SetViewState(self._Is3d)
		
		self._RoomList.append(room)
		num = len(self._RoomList) - 1
		
		self._RoomCount = len(self._RoomList)
		
		#表示関係を設定
		self.SetRoomVisibleState()
		
		return num
	
	#コピー
	def Copy(self,fileName,roomData):
		self.LoadIniFile(fileName)
		roomName = roomData[1]
		room = Room.Room(self._ModelControlClass)
		room.SetDefaultSettings(self._DefaultFloorTexPath,self._DefaultWallTexPath,self._DefaultCeilingTexPath,self._DefaultWallHight)
		room.Create(self._RoomIniFile,fileName,roomName,roomData,add=True,copy=True)	#レイアウトを作成
		room.SetViewState(self._Is3d)
		
		self._RoomList.append(room)
		num = len(self._RoomList) - 1
		
		self._RoomCount = len(self._RoomList)
		
		#表示関係を設定
		self.SetRoomVisibleState()
		
		return num
	
	#新規間取りをリストに追加
	def AddNewRoomToList(self,roomNum):
		room = self.GetRoom(roomNum)
		
	#設定ファイルの読み込み
	def LoadIniFile(self,fileName):
		self._RoomIniFileName = fileName
		
		#iniファイルの読み込み
		self._RoomIniFile = LoadIniFile.LoadIniFileClass()
		self._RoomIniFile.LoadIniFile(viz.res.getPublishedPath(self._RoomIniFileName))
		
	#読込みに成功したかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._LoadErrorFlag
	
	#削除
	def Delete(self,roomNum):
		deleteRoom = self._RoomList[roomNum]
		
		#モデルリストを更新
		newRoomLiset = []
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			if room != deleteRoom:
				newRoomLiset.append(room)
						
		self._RoomList = newRoomLiset
		
		#モデルを削除
		if deleteRoom != None:
			deleteRoom.Delete()
			del deleteRoom
			
		self._RoomCount = len(self._RoomList)
		
	#全モデル削除
	def DeleteAll(self):
		if self._Is3d:
			self.DeleteAllDoorHole()	#ダミードアを削除
			self.DeleteAllWindowHole()	#ダミー窓を削除
		
		modelCount = self.GetRoomCount()
		for x in range(modelCount):
			self.Delete(0)
			
		self._RoomIniFileName = ""
		
		#Undoの情報をクリア
		self._UndoClass.ClearList()
		self._RedoClass.ClearList()
		
	#部屋を取得
	def GetRoom(self,num):
		room = self._RoomList[num]
		
		return room
		
	#間取り数取得
	def GetRoomCount(self):
		count = len(self._RoomList)
		return count
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for room in self._RoomList:
			room.SetPickableForAllModel(state)
			
	#間取りの選択可能状態を設定
	def SetRoomPickable(self,num,state):
		room = self._RoomList[num]
		room.SetPickableForAllModel(state)
			
	#壁のラインモデルから番号を取得
	def GetWallLineNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWallLineNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#壁のラインモデルを取得
	def GetWallLineModel(self,roomNum,modelNum):
		room = self._RoomList[roomNum]
		model = room.GetWallLineModel(modelNum)
		
		return model
		
	#壁のラインの表示状態を設定
	def SetWallLineVisible(self,state):
		for room in self._RoomList:
			room.SetWallLineVisible(state)
		
	#壁のラインのハイライト表示
	def SetHighlightWallLine(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWallLine(modelNum,state)
		
	#壁のラインの位置設定
	def SetWallLinePosition(self,roomNum,num,position):
		room = self._RoomList[roomNum]
		room.SetWallLinePosition(num,position)
		
	#壁のラインの位置指定時のオフセット設定
	def SetWallLinePositionOffset(self,roomNum,num,position):
		room = self._RoomList[roomNum]
		room.SetWallLinePositionOffset(num,position)
		
	#壁の点モデルから番号を取得
	def GetWallPointNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWallPointNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#壁の点モデルを取得
	def GetWallPointModel(self,roomNum,wallPointNum):
		room = self._RoomList[roomNum]
		model = room.GetWallPointModel(wallPointNum)
		
		return model
		
	#壁の点の表示状態を設定
	def SetWallPointVisible(self,state):
		for Room in self._RoomList:
			Room.SetWallPointVisible(state)
		
	#壁の点のハイライト表示
	def SetHighlightWallPoint(self,roomNum,wallPointNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWallPoint(wallPointNum,state)
		
	#壁の点の位置設定
	def SetWallPointPosition(self,roomNum,wallPointNum,position):
		room = self._RoomList[roomNum]
		room.SetWallPointPosition(wallPointNum,position)
		
	#壁の点の位置指定時のオフセット設定
	def SetWallPointPositionOffset(self,roomNum,wallPointNum,position):
		room = self._RoomList[roomNum]
		room.SetWallPointPositionOffset(wallPointNum,position)
		
	#ドア追加
	def AddDoor(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDoor(fileName,wallNum,percentage)
		
		return num
		
	#ダミードア追加（穴を空ける）
	def AddDummyDoor(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDummyDoor(fileName,wallNum,percentage)
		
		return num
		
	#隣接する壁にドアの穴を空ける
	def AddDoorHole(self,roomNum,doorNum,fileName):
		state = False
		resRoom = None
		resWall = None
		resPerc = None
		
		#間取りを取得
		room = self._RoomList[roomNum]
		roomPos = room.GetFloorPosition()
		#print 'roomPos',roomPos
		
		#壁を取得
		doorList = room.GetDoorList()
		doorWallNum = doorList[doorNum][1]
		
		baseWallPointList = room.GetWallPointList()
		
		baseWallPointPosMin = baseWallPointList[doorWallNum]
		#print 'baseWallPointPosMin',baseWallPointPosMin
		
		baseWallPointGlobalPosMin = [0.0,0.0,0.0]
		baseWallPointGlobalPosMin[0] = roomPos[0] + baseWallPointPosMin[0]
		baseWallPointGlobalPosMin[2] = roomPos[2] + baseWallPointPosMin[1]
		
		doorWallNum2 = doorWallNum+1
		if doorWallNum2 == len(baseWallPointList):
			doorWallNum2 = 0
			
		baseWallPointPosMax = baseWallPointList[doorWallNum2]
		#print 'baseWallPointPosMax',baseWallPointPosMax
		
		baseWallPointGlobalPosMax = [0.0,0.0,0.0]
		baseWallPointGlobalPosMax[0] = roomPos[0] + baseWallPointPosMax[0]
		baseWallPointGlobalPosMax[2] = roomPos[2] + baseWallPointPosMax[1]
		
		#print 'baseWallPointGlobalPosMin',baseWallPointGlobalPosMin
		#print 'baseWallPointGlobalPosMax',baseWallPointGlobalPosMax
		
		baseWallVec = [baseWallPointGlobalPosMax[0]-baseWallPointGlobalPosMin[0],baseWallPointGlobalPosMax[2]-baseWallPointGlobalPosMin[2]]
		baseWallLong = room.GetWallSize(doorWallNum)
		if baseWallLong == 0.0:
			baseWallLong = 0.0001
		baseWallNorm = [baseWallVec[0]/baseWallLong,baseWallVec[1]/baseWallLong]
		
		#ドアを取得
		door = room.GetDoorModel(doorNum)
		doorPos = door.GetPosition()
		#print 'doorPos',doorPos
		
		doorGlobalPos = [0.0,0.0,0.0]
		doorGlobalPos[0] = doorPos[0]
		doorGlobalPos[1] = 0.0
		doorGlobalPos[2] = doorPos[2]
		#print 'doorGlobalPos',doorGlobalPos
		
		#部屋の数だけ繰り返す
		for x in range(len(self._RoomList)):
			#print 'x',x
			
			if x != roomNum:
				otherRoom = self._RoomList[x]
				otehrRoomPos = otherRoom.GetFloorPosition()
				#print 'otehrRoomPos',otehrRoomPos
				
				wallPointList = otherRoom.GetWallPointList()
				
				#壁の数だけ繰り返す
				for y in range(len(wallPointList)):
					#print 'y',y
					
					wallPointPosMin = wallPointList[y]
					#print 'wallPointPosMin',wallPointPosMin
					
					wallPointGlobalPosMin = [0.0,0.0,0.0]
					wallPointGlobalPosMin[0] = otehrRoomPos[0] + wallPointPosMin[0]
					wallPointGlobalPosMin[1] = 0.0
					wallPointGlobalPosMin[2] = otehrRoomPos[2] + wallPointPosMin[1]
					
					y2 = y+1
					if y2 == len(wallPointList):
						y2 = 0
						
					wallPointPosMax = wallPointList[y2]
					#print 'wallPointPosMax',wallPointPosMax
					
					wallPointGlobalPosMax = [0.0,0.0,0.0]
					wallPointGlobalPosMax[0] = otehrRoomPos[0] + wallPointPosMax[0]
					wallPointGlobalPosMax[1] = 0.0
					wallPointGlobalPosMax[2] = otehrRoomPos[2] + wallPointPosMax[1]
					
					wallVec = [wallPointGlobalPosMax[0]-wallPointGlobalPosMin[0],wallPointGlobalPosMax[2]-wallPointGlobalPosMin[2]]
					wallLong = otherRoom.GetWallSize(y)
					if wallLong == 0.0:
						wallLong = 0.0001
					wallNorm = [wallVec[0]/wallLong,wallVec[1]/wallLong]
					
					#内積を求める
					dot = baseWallNorm[0]*wallNorm[0] + baseWallNorm[1]*wallNorm[1]
					
					#壁同士の傾きが近い場合は距離を確認
					if dot < -0.99 or dot > 0.99:
						#壁の始点=A、壁の終点=B、ドア=P、面積=d、壁上のドアに近い位置=X
						vecAP = [doorGlobalPos[0]-wallPointGlobalPosMin[0],doorGlobalPos[2]-wallPointGlobalPosMin[2]]
						disAX = wallNorm[0]*vecAP[0] + wallNorm[1]*vecAP[1]
						
						d = math.fabs(wallVec[0]*vecAP[1] - wallVec[1]*vecAP[0])	#面積
						
						nearestDistance = d / wallLong
						nearestPerc = disAX / wallLong
						#print 'nearestDistance',nearestDistance
						#print 'nearestPerc',nearestPerc
						
						if nearestDistance < 0.16:	#15cm以内の場合はそこに穴を空ける
							if nearestPerc > 0.0005 and nearestPerc < 0.9995:
								#print 'CreateWindowHole'
								state = True
								resRoom = x
								resWall = y
								resPerc = nearestPerc
								#print 'distance',nearestDistance
								#print 'room',resRoom
								#print 'wall',resWall
								#print 'perc',resPerc
								
					#壁がドアの近くにあったら穴を空ける
					if state:
						dummyDoorNum = self.AddDummyDoor(resRoom,fileName,resWall,resPerc)
						
						dummyDoorData = [resRoom,dummyDoorNum,roomNum,doorNum]
						self._DummyDoorStateList.append(dummyDoorData)
						
						state = False
						
	#全てのドアについて隣接する壁にドアの穴を空ける
	def AddAllDoorHole(self):
		for roomNum in range(len(self._RoomList)):
			room = self._RoomList[roomNum]
			doorCount = room.GetDoorCount()
			for doorNum in range(doorCount):
				door = room.GetDoorModel(doorNum)
				if door.GetDummyFlag() == False:
					doorFileName = door.GetFileName()
					self.AddDoorHole(roomNum,doorNum,doorFileName)
		
		#print 'AddAllDoorHole',self._DummyDoorStateList
		
	#ドアの穴を削除
	def DeleteDoorHole(self,roomNum,doorNum):
		#ドアを削除
		if roomNum != None and doorNum != None:
			state = False
			dummyRoomNum = None
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[2] == roomNum and dummyDoorData[3] == doorNum:
					self.DeleteDummyDoor(dummyDoorData[0],dummyDoorData[1])
					dummyRoomNum = dummyDoorData[0]
					self._DummyDoorStateList[x] = None
					state = True
					break
		
			if state:
				room = self._RoomList[dummyRoomNum]
				room.ReCreateWall()
				
				for x in range(len(self._DummyDoorStateList)):
					dummyDoorData = self._DummyDoorStateList[x]
					if dummyDoorData != None:
						if dummyDoorData[0] == dummyRoomNum and dummyDoorData[1] > doorNum:
							dummyDoorData[1] = dummyDoorData[1] - 1
						
						if dummyDoorData[2] == roomNum and dummyDoorData[3] > doorNum:
							dummyDoorData[3] = dummyDoorData[3] - 1
			
		#間取りを削除
		else:
			state = False
			dummyRoomNumList = []
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[0] == roomNum or dummyDoorData[2] == roomNum:
					self.DeleteDummyDoor(dummyDoorData[0],0)
						
					dummyRoomNumList.append(dummyDoorData[0])
					self._DummyDoorStateList[x] = None
					state = True
					
			if state:
				for x in range(len(dummyRoomNumList)):
					room = self._RoomList[dummyRoomNumList[x]]
					room.ReCreateWall()
					
				for x in range(len(self._DummyDoorStateList)):
					dummyDoorData = self._DummyDoorStateList[x]
					if dummyDoorData != None:
						if dummyDoorData[0] > roomNum:
							dummyDoorData[0] = dummyDoorData[0] - 1
						if dummyDoorData[2] > roomNum:
							dummyDoorData[2] = dummyDoorData[2] - 1
					
		newList = []
		for x in range(len(self._DummyDoorStateList)):
			dummyDoorData = self._DummyDoorStateList[x]
			if dummyDoorData != None:
				newList.append(dummyDoorData)
		
		self._DummyDoorStateList = newList
		
	#ドアの穴を全て削除
	def DeleteAllDoorHole(self):
		#print 'DeleteAllDoorHole',self._DummyDoorStateList
		
		count = len(self._DummyDoorStateList)
		if count != 0:
			dummyRoomNumList = []
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData != None:
					self.DeleteDummyDoor(dummyDoorData[0],dummyDoorData[1])
					dummyRoomNumList.append(dummyDoorData[0])
					self._DummyDoorStateList[x] = None
		
					for y in range(len(self._DummyDoorStateList)):
						if y > x:
							dummyDoorData2 = self._DummyDoorStateList[y]
							if dummyDoorData2[0] == dummyDoorData[0] and dummyDoorData2[1] > dummyDoorData[1]:
								dummyDoorData2[1] = dummyDoorData2[1] - 1
								self._DummyDoorStateList[y] = dummyDoorData2
								
			for x in range(len(dummyRoomNumList)):
				room = self._RoomList[dummyRoomNumList[x]]
				room.ReCreateWall()
		
		self._DummyDoorStateList = []
		
	#ドア、窓の移動による、壁の再作成
	def ReCreateWall(self,roomNum):
		room = self._RoomList[roomNum]
		room.ReCreateWall()
	
	#ドアの削除
	def DeleteDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			self.DeleteDoorHole(roomNum,num)		#ダミードアを削除
			
			room.DeleteDoor(num)
			
			#ダミードアのリストを更新
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[0] == roomNum and dummyDoorData[1] > num:
					dummyDoorData[1] = dummyDoorData[1] - 1
					self._DummyDoorStateList[x] = dummyDoorData
					
	#ダミードアの削除
	def DeleteDummyDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.DeleteDoor(num)
		
	#ドアのコピー
	def CopyDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.CopyDoor(num)
			
	#ドアの位置設定
	def SetDoorPosition(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		if num != None and wallNum != None:	
			room.SetDoorPosition(num,wallNum,percentage)
			
	#壁の両端の位置を取得
	def GetWallPoint(self,roomNum,num):
		room = self._RoomList[roomNum]
		point0,point1 = room.GetWallPoint(num)
		
		return point0,point1
		
	#ドアの表示状態を設定
	def SetDoorVisible(self,roomNum,state):
		room = self._RoomList[roomNum]
		room.SetDoorVisible(state)
		
	#ドアのラインの表示状態を設定
	def SetDoorLineVisible(self,state):
		for Room in self._RoomList:
			Room.SetDoorLineVisible(state)
		
	#壁の高さ取得
	def GetWallHeight(self,roomNum):
		room = self._RoomList[roomNum]
		wallHeight = room.GetWallHeight()
		
		return wallHeight
		
	#一番高い壁の高さ取得
	def GetWallHeightTop(self):
		wallHeightTop = 0
		
		for room in self._RoomList:
			height = room.GetWallHeight()
			if height > wallHeightTop:
				wallHeightTop = height
				wallHeightTop = height
		
		return wallHeightTop
		
	#ドアのハイライト表示
	def SetHighlightDoor(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightDoor(modelNum,state)
		
	#ドアのハイライト状態の取得
	def GetHighlightDoorState(self,RoomNum,modelNum):
		room = self._RoomList[RoomNum]
		state = room.GetHighlightDoorState(modelNum)
		
		return state
		
	#ドアモデルから番号を取得
	def GetDoorNum(self,model):
		modelNum = None
		roomNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetDoorNum(model)
			if modelNum != None:
				roomNum = x
				break
				
		return roomNum,modelNum
		
	#ドアモデルを取得
	def GetDoorModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		model = room.GetDoorModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetDoorLineModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		lineModel = room.GetDoorLineModel(num)
		
		return lineModel
		
	#全ドア選択可能状態を設定
	def SetAllDoorPickable(self,state):
		for room in self._RoomList:
			room.SetDoorPickable(state)
			
	#ドア選択可能状態を設定
	def SetDoorPickable(self,roomNum,state):
		room = self._RoomList[roomNum]
		room.SetDoorPickable(state)
			
	#ドアのリストを更新
	def UpdateDoorList(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		room.UpdateDoorList(num,wallNum,percentage)
	
	#窓追加
	def AddWindow(self,roomNum,fileName,wallNum,percentage,windowHeight=None):
		room = self._RoomList[roomNum]
		num = room.AddWindow(fileName,wallNum,percentage)
		
		if windowHeight != None:
			room.SetWindowHeight(num,windowHeight)
		else:
			room.SetWindowHeight(num,self._DefaultWindowHeight)
			
		return num
	
	#ダミー窓追加（穴を空ける）
	def AddDummyWindow(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDummyWindow(fileName,wallNum,percentage)
		
		return num
		
	#隣接する壁に窓の穴を空ける
	def AddWindowHole(self,roomNum,windowNum,fileName):
		state = False
		resRoom = None
		resWall = None
		resPerc = None
		
		#間取りを取得
		room = self._RoomList[roomNum]
		roomPos = room.GetFloorPosition()
		#print 'roomPos',roomPos
		
		#壁を取得
		windowList = room.GetWindowList()
		windowWallNum = windowList[windowNum][2]
		
		baseWallPointList = room.GetWallPointList()
		
		baseWallPointPosMin = baseWallPointList[windowWallNum]
		#print 'baseWallPointPosMin',baseWallPointPosMin
		
		baseWallPointGlobalPosMin = [0.0,0.0,0.0]
		baseWallPointGlobalPosMin[0] = roomPos[0] + baseWallPointPosMin[0]
		baseWallPointGlobalPosMin[2] = roomPos[2] + baseWallPointPosMin[1]
		
		windowWallNum2 = windowWallNum+1
		if windowWallNum2 == len(baseWallPointList):
			windowWallNum2 = 0
			
		baseWallPointPosMax = baseWallPointList[windowWallNum2]
		#print 'baseWallPointPosMax',baseWallPointPosMax
		
		baseWallPointGlobalPosMax = [0.0,0.0,0.0]
		baseWallPointGlobalPosMax[0] = roomPos[0] + baseWallPointPosMax[0]
		baseWallPointGlobalPosMax[2] = roomPos[2] + baseWallPointPosMax[1]
		
		#print 'baseWallPointGlobalPosMin',baseWallPointGlobalPosMin
		#print 'baseWallPointGlobalPosMax',baseWallPointGlobalPosMax
		
		baseWallVec = [baseWallPointGlobalPosMax[0]-baseWallPointGlobalPosMin[0],baseWallPointGlobalPosMax[2]-baseWallPointGlobalPosMin[2]]		#ベクトル
		baseWallLong = room.GetWallSize(windowWallNum)		#長さ
		baseWallNorm = [baseWallVec[0]/baseWallLong,baseWallVec[1]/baseWallLong]	#単位ベクトル
		
		#窓を取得
		window = room.GetWindowModel(windowNum)
		windowPos = window.GetPosition()
		#print 'windowPos',windowPos
		windowHeight = room.GetWindowHeight(windowNum)
		#print 'windowPos',windowHeight
		
		windowGlobalPos = [0.0,0.0,0.0]
		windowGlobalPos[0] = windowPos[0]
		windowGlobalPos[1] = 0.0
		windowGlobalPos[2] = windowPos[2]
		#print 'windowGlobalPos',windowGlobalPos
		
		#部屋の数だけ繰り返す
		for x in range(len(self._RoomList)):
			#print 'x',x
			
			if x != roomNum:
				otherRoom = self._RoomList[x]
				otehrRoomPos = otherRoom.GetFloorPosition()
				#print 'otehrRoomPos',otehrRoomPos
				
				wallPointList = otherRoom.GetWallPointList()
				
				#壁の数だけ繰り返す
				for y in range(len(wallPointList)):
					#print 'y',y
					
					wallPointPosMin = wallPointList[y]
					#print 'wallPointPosMin',wallPointPosMin
					
					wallPointGlobalPosMin = [0.0,0.0,0.0]
					wallPointGlobalPosMin[0] = otehrRoomPos[0] + wallPointPosMin[0]
					wallPointGlobalPosMin[1] = 0.0
					wallPointGlobalPosMin[2] = otehrRoomPos[2] + wallPointPosMin[1]
					
					y2 = y+1
					if y2 == len(wallPointList):
						y2 = 0
						
					wallPointPosMax = wallPointList[y2]
					#print 'wallPointPosMax',wallPointPosMax
					
					wallPointGlobalPosMax = [0.0,0.0,0.0]
					wallPointGlobalPosMax[0] = otehrRoomPos[0] + wallPointPosMax[0]
					wallPointGlobalPosMax[1] = 0.0
					wallPointGlobalPosMax[2] = otehrRoomPos[2] + wallPointPosMax[1]
					
					wallVec = [wallPointGlobalPosMax[0]-wallPointGlobalPosMin[0],wallPointGlobalPosMax[2]-wallPointGlobalPosMin[2]]
					wallLong = otherRoom.GetWallSize(y)
					wallNorm = [wallVec[0]/wallLong,wallVec[1]/wallLong]
					
					#内積を求める
					dot = baseWallNorm[0]*wallNorm[0] + baseWallNorm[1]*wallNorm[1]
					
					#壁同士の傾きが近い場合は距離を確認
					if dot < -0.99 or dot > 0.99:
						#壁の始点=A、壁の終点=B、窓=P、面積=d、壁上の窓に近い位置=X
						vecAP = [windowGlobalPos[0]-wallPointGlobalPosMin[0],windowGlobalPos[2]-wallPointGlobalPosMin[2]]
						disAX = wallNorm[0]*vecAP[0] + wallNorm[1]*vecAP[1]
						
						d = math.fabs(wallVec[0]*vecAP[1] - wallVec[1]*vecAP[0])	#面積
						
						nearestDistance = d / wallLong
						nearestPerc = disAX / wallLong
						#print 'nearestDistance',nearestDistance
						#print 'nearestPerc',nearestPerc
						
						if nearestDistance < 0.16:	#15cm以内の場合はそこに穴を空ける
							if nearestPerc > 0.0005 and nearestPerc < 0.9995:
								#print 'CreateWindowHole'
								state = True
								resRoom = x
								resWall = y
								resPerc = nearestPerc
								#print 'distance',nearestDistance
								#print 'room',resRoom
								#print 'wall',resWall
								#print 'perc',resPerc
								
					#壁が窓の近くにあったら穴を空ける
					if state:
						dummyWindowNum = self.AddDummyWindow(resRoom,fileName,resWall,resPerc)
						self.SetDummyWindowHeight(resRoom,dummyWindowNum,windowHeight)
						
						dummyWindowData = [resRoom,dummyWindowNum,roomNum,windowNum,windowHeight]
						self._DummyWindowStateList.append(dummyWindowData)
						
						state = False
						
	#全てのに対して隣接する壁に窓の穴を空ける
	def AddAllWindowHole(self):
		for roomNum in range(len(self._RoomList)):
			room = self._RoomList[roomNum]
			windowCount = room.GetWindowCount()
			for windowNum in range(windowCount):
				window = room.GetWindowModel(windowNum)
				if window.GetDummyFlag() == False:
					windowFileName = window.GetFileName()
					self.AddWindowHole(roomNum,windowNum,windowFileName)
					
		#print 'AddAllWindowHole',self._DummyWindowStateList
		
	#窓の穴を削除
	def DeleteWindowHole(self,roomNum,windowNum):
		#窓を削除
		if roomNum != None and windowNum != None:
			state = False
			dummyRoomNum = None
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[2] == roomNum and dummyWindowData[3] == windowNum:
					self.DeleteDummyWindow(dummyWindowData[0],dummyWindowData[1])
					dummyRoomNum = dummyWindowData[0]
					self._DummyWindowStateList[x] = None
					state = True
					break
					
			if state:
				room = self._RoomList[dummyRoomNum]
				room.ReCreateWall()
				
				for x in range(len(self._DummyWindowStateList)):
					dummyWindowData = self._DummyWindowStateList[x]
					if dummyWindowData != None:
						if dummyWindowData[0] == dummyRoomNum and dummyWindowData[1] > windowNum:
							dummyWindowData[1] = dummyWindowData[1] - 1
							
						if dummyWindowData[2] == roomNum and dummyWindowData[3] > windowNum:
							dummyWindowData[3] = dummyWindowData[3] - 1
							
		#間取りを削除
		else:
			state = False
			dummyRoomNumList = []
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[0] == roomNum or dummyWindowData[2] == roomNum:
					self.DeleteDummyWindow(dummyWindowData[0],0)
					
					dummyRoomNumList.append(dummyWindowData[0])
					self._DummyWindowStateList[x] = None
					state = True
					
			if state:
				for x in range(len(dummyRoomNumList)):
					room = self._RoomList[dummyRoomNumList[x]]
					room.ReCreateWall()
					
				for x in range(len(self._DummyWindowStateList)):
					dummyWindowData = self._DummyWindowStateList[x]
					if dummyWindowData != None:
						if dummyWindowData[0] > roomNum:
							dummyWindowData[0] = dummyWindowData[0] - 1
						if dummyWindowData[2] > roomNum:
							dummyWindowData[2] = dummyWindowData[2] - 1
					
		newList = []
		for x in range(len(self._DummyWindowStateList)):
			dummyWindowData = self._DummyWindowStateList[x]
			if dummyWindowData != None:
				newList.append(dummyWindowData)
				
		self._DummyWindowStateList = newList
		
	#窓の穴を全て削除
	def DeleteAllWindowHole(self):
		#print 'DeleteAllWindowHole',self._DummyWindowStateList
		
		count = len(self._DummyWindowStateList)
		if count != 0:
			dummyRoomNumList = []
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData != None:
					self.DeleteDummyWindow(dummyWindowData[0],dummyWindowData[1])
					dummyRoomNumList.append(dummyWindowData[0])
					self._DummyWindowStateList[x] = None
					
					for y in range(len(self._DummyWindowStateList)):
						if y > x:
							dummyWindowData2 = self._DummyWindowStateList[y]
							if dummyWindowData2[0] == dummyWindowData[0] and dummyWindowData2[1] > dummyWindowData[1]:
								dummyWindowData2[1] = dummyWindowData2[1] - 1
								self._DummyWindowStateList[y] = dummyWindowData2
								
			for x in range(len(dummyRoomNumList)):
				room = self._RoomList[dummyRoomNumList[x]]
				room.ReCreateWall()
				
		self._DummyWindowStateList = []
		
	#窓の削除
	def DeleteWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			self.DeleteWindowHole(roomNum,num)		#ダミー窓を削除
			
			room.DeleteWindow(num)
			
			#ダミー窓のリストを更新
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[0] == roomNum and dummyWindowData[1] > num:
					dummyWindowData[1] = dummyWindowData[1] - 1
					self._DummyWindowStateList[x] = dummyWindowData
					
	#ダミー窓の削除
	def DeleteDummyWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.DeleteWindow(num)
		
	#窓のコピー
	def CopyWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.CopyWindow(num)
			
	#窓の位置設定
	def SetWindowPosition(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		if num != None and wallNum != None:	
			room.SetWindowPosition(num,wallNum,percentage)
			
	#窓の表示状態を設定
	def SetWindowVisible(self,roomNum,state):
		room = self._RoomList[roomNum]
		room.SetWindowVisible(state)
		
	#窓のラインの表示状態を設定
	def SetWindowLineVisible(self,state):
		for Room in self._RoomList:
			Room.SetWindowLineVisible(state)
			
	#窓のハイライト表示
	def SetHighlightWindow(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWindow(modelNum,state)
		
	#窓のハイライト状態の取得
	def GetHighlightWindowState(self,RoomNum,modelNum):
		room = self._RoomList[RoomNum]
		state = room.GetHighlightWindowState(modelNum)
		
		return state
		
	#窓モデルから番号を取得
	def GetWindowNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWindowNum(model)
			if modelNum != None:
				roomNum = x
				break
				
		return roomNum,modelNum
		
	#窓モデルを取得
	def GetWindowModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		model = room.GetWindowModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetWindowLineModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		lineModel = room.GetWindowLineModel(num)
		
		return lineModel
		
	#全窓選択可能状態を設定
	def SetAllWindowPickable(self,state):
		for room in self._RoomList:
			room.SetWindowPickable(state)
			
	#窓選択可能状態を設定
	def SetWindowPickable(self,roomNum,state):
		room = self._RoomList[roomNum]
		room.SetWindowPickable(state)
		
	#窓のリストを更新
	def UpdateWindowList(self,RoomNum,num,state,height,wallNum,percentage):
		room = self._RoomList[RoomNum]
		room.UpdateWindowList(num,state,height,wallNum,percentage)
		
	#天井の表示状態を設定
	def SetCeilingVisible(self,state):
		for room in self._RoomList:
			room.SetCeilingVisible(state)
			
	#床のハイライト表示
	def SetHighlightFloor(self,RoomNum,state):
		room = self._RoomList[RoomNum]
		room.SetHighlightFloor(state)
		
	#床のハイライト状態の取得
	def GetHighlightFloorState(self,RoomNum):
		room = self._RoomList[RoomNum]
		state = room.GetHighlightFloorState()
		
		return state
		
	#床モデルを取得
	def GetFloorModel(self,roomNum):
		room = self._RoomList[roomNum]
		model = room.GetFloorModel()
		
		return model
		
	#床のテクスチャを設定
	def SetFloorTexture(self,RoomNum,texture,filePath):
		room = self._RoomList[RoomNum]
		room.SetFloorTexture(texture,filePath)
		
		self._DefaultFloorTexPath = filePath
		
	#床のテクスチャを取得
	def GetFloorTexture(self,RoomNum):
		room = self._RoomList[RoomNum]
		texture = room.GetFloorTexture()
		
		return texture
		
	#床のテクスチャ名を取得
	def GetFloorTextureName(self,roomNum):
		room = self._RoomList[roomNum]
		textureName = room.GetFloorTextureName()
		
		return textureName
		
	#床のテクスチャのファイル名とサイズを取得
	def GetFloorTextureSetting(self,RoomNum):
		room = self._RoomList[RoomNum]
		textureFile,textureSize = room.GetFloorTextureSetting()
		
		return textureFile,textureSize
		
	#床モデルから部屋の番号を取得
	def GetRoomNum(self,model):
		roomNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			floorModel = room.GetFloorModel()
			if model == floorModel:
				roomNum = x
				break
				
		return roomNum
		
	#壁の高さを設定
	def SetWallHeight(self,roomNum,wallHeight):
		room = self._RoomList[roomNum]
		room.SetWallHeight(wallHeight)
		
		if room.GetOpRoomState != 2:
			self._DefaultWallHight = wallHeight
			
	#壁のテクスチャを設定
	def SetWallTexture(self,roomNum,texture,filePath):
		room = self._RoomList[roomNum]
		room.SetWallTexture(texture,filePath)
		
		if room.GetOpRoomState != 2:
			self._DefaultWallTexPath = filePath
			
	#壁のテクスチャを取得
	def GetWallTexture(self,roomNum):
		room = self._RoomList[roomNum]
		texture = room.GetWallTexture()
		
		return texture
		
	#壁のテクスチャ名を取得
	def GetWallTextureName(self,roomNum):
		room = self._RoomList[roomNum]
		textureName = room.GetWallTextureName()
		
		return textureName
		
	#壁のテクスチャのファイル名とサイズを取得
	def GetWallTextureSetting(self,roomNum):
		room = self._RoomList[roomNum]
		textureFile,textureSize = room.GetWallTextureSetting()
		
		return textureFile,textureSize
		
	#窓の高さを設定
	def SetWindowHeight(self,roomNum,num,height):
		room = self._RoomList[roomNum]
		room.SetWindowHeight(num,height)
		
		#ダミー窓の高さを設定
		for x in range(len(self._DummyWindowStateList)):
			dummyWindowData = self._DummyWindowStateList[x]
			if dummyWindowData[2] == roomNum and dummyWindowData[3] == num:
				self.SetDummyWindowHeight(dummyWindowData[0],dummyWindowData[1],height)
				dummyWindowData[4] = height
				self._DummyWindowStateList[x] = dummyWindowData
				
		self._DefaultWindowHeight = height
		
	#ダミー窓の高さを設定
	def SetDummyWindowHeight(self,roomNum,num,height):
		room = self._RoomList[roomNum]
		room.SetWindowHeight(num,height)
		
	#窓の高さを取得
	def GetWindowHeight(self,roomNum,num):
		room = self._RoomList[roomNum]
		height = room.GetWindowHeight(num)
		
		return height
		
	#壁のポイントを追加
	def AddWallPoint(self,roomNum,num,pointData=None,wallVisibleList=None,floorPos=None):
		room = self._RoomList[roomNum]
		room.AddWallPoint(num,pointData,wallVisibleList,floorPos)
		
	#壁のポイントを削除
	def RemoveWallPoint(self,roomNum,num):
		room = self._RoomList[roomNum]
		room.RemoveWallPoint(num)
		
	#壁のポイントの数を取得
	def GetWallPointCount(self,roomNum):
		room = self._RoomList[roomNum]
		pointCount = room.GetWallPointCount()
		return pointCount
		
	#床の位置設定
	def SetFloorPosition(self,roomNum,position,withWall=True):
		room = self._RoomList[roomNum]
		room.SetFloorPosition(position,withWall)
		
	#床の位置取得
	def GetFloorPosition(self,roomNum):
		room = self._RoomList[roomNum]
		position = room.GetFloorPosition()
		
		return position
		
	#天井のテクスチャを設定
	def SetCeilingTexture(self,RoomNum,texture,filePath):
		room = self._RoomList[RoomNum]
		room.SetCeilingTexture(texture,filePath)
		
		if room.GetOpRoomState != 2:
			self._DefaultCeilingTexPath = filePath
		
	#天井のテクスチャを取得
	def GetCeilingTexture(self,RoomNum):
		room = self._RoomList[RoomNum]
		texture = room.GetCeilingTexture()
		
		return texture
		
	#天井のテクスチャ名を取得
	def GetCeilingTextureName(self,roomNum):
		room = self._RoomList[roomNum]
		textureName = room.GetCeilingTextureName()
		
		return textureName
		
	#天井のテクスチャのファイル名とサイズを取得
	def GetCeilingTextureSetting(self,RoomNum):
		room = self._RoomList[RoomNum]
		textureFile,textureSize = room.GetCeilingTextureSetting()
		
		return textureFile,textureSize
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetDoorModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetCollisionModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#スタート位置を設定
	def SetStartPosition(self):
		startPosTemp = self._RoomIniFile.GetItemData('Define','satartposition')
		startPos = [0,0,0]
		if startPosTemp != None:
			startPos[0] = float(startPosTemp.split(',')[0])
			startPos[1] = float(startPosTemp.split(',')[1])
			startPos[2] = float(startPosTemp.split(',')[2])
		
		startAngleTemp = self._RoomIniFile.GetItemData('Define','satartangle')
		startAngle = [0,0,0]
		if startAngleTemp != None:
			startAngle[0] = float(startAngleTemp.split(',')[0])
			startAngle[1] = float(startAngleTemp.split(',')[1])
			startAngle[2] = float(startAngleTemp.split(',')[2])
		
		self._StartPosition = startPos
		self._StartAngle = startAngle
		
		#スタート位置のモデルを設定
		if startPos == [0,0,0]:
			self._StartPositionClass.Reset()
		else:
			self._StartPositionClass.SetPosition(startPos)
		self._StartPositionClass.SetOrientation(startAngle[0])
		
	#スタート位置をモデルから設定
	def SetStartPositionFromModel(self):
		pos = self._StartPositionClass.GetPosition()
		self._StartPosition = pos
		
		angle = [0,0,0]
		angle[0] = self._StartPositionClass.GetOrientation()
		self._StartAngle = angle
		
	#スタート位置を取得
	def GetStartPosition(self):
		startPos = self._StartPosition
		startAngle = self._StartAngle
		
		return startPos,startAngle
		
	#INIファイル名を取得
	def GetIniFileName(self):
		val = self._RoomIniFileName
		
		return val
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		for room in self._RoomList:
			room.SetViewState(state)
		
	#壁の表示状態設定
	def SetWallVisible(self,roomNum,wallNum,state):
		room = self._RoomList[roomNum]
		room.SetWallVisible(wallNum,state)
		
	#壁の表示状態取得
	def GetWallVisible(self,roomNum,wallNum):
		room = self._RoomList[roomNum]
		state = room.GetWallVisible(wallNum)
		
		return state
		
	#壁を非表示に設定
	def HideWall(self,state):
		for room in self._RoomList:
			room.HideWall(state)
			
	#追加時のテクスチャ、高さ設定情報を取得
	def GetDefaultSettings(self):
		return self._DefaultFloorTexPath,self._DefaultWallTexPath,self._DefaultCeilingTexPath,self._DefaultWallHight
		
	#図面を設定
	def SetDrawing(self):
		self._DrawingFile = None
		
		fileNameTemp = self._RoomIniFile.GetItemData('Define','drawingfile')
		if fileNameTemp != None:
			self._DrawingFile = fileNameTemp
		
		scaleTemp = self._RoomIniFile.GetItemData('Define','drawingscale')
		if scaleTemp != None:
			self._DrawingScale = float(scaleTemp)
		
		#図面を設定
		if self._DrawingFile != '' and self._DrawingFile != None:
			self._DrawingClass.Add(self._DrawingFile)
			self._DrawingClass.SetScale(self._DrawingScale)
			
	#図面を取得
	def GetDrawing(self):
		self._DrawingFile = self._DrawingClass.GetFileName()
		self._DrawingScale = self._DrawingClass.GetScale()
		
	#図面のモデルを取得
	def GetDrawingModel(self):
		return self._DrawingClass.GetDrawingModel()
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		scale = self._DrawingClass.GetDrawingScale()
		return scale
		
	"""
	#図面を削除
	def DeleteDrawing(self):
		if self._Drawing != None:
			self._Drawing.Delete()
			del self._Drawing
		
		self._Drawing = None	
			
	#図面の表示状態を設定
	def SetDrawingVisible(self,state):
		self._Drawing.SetVisible(state)
			
	#図面の表示状態を取得
	def GetDrawingVisible(self):
		return self._Drawing.GetVisible()
	"""
	
	#ドアの数を取得
	def GetDoorCount(self,roomNum):
		room = self._RoomList[roomNum]
		count = room.GetDoorCount()
		return count
		
	#窓の数を取得
	def GetWindowCount(self,roomNum):
		room = self._RoomList[roomNum]
		count = room.GetWindowCount()
		return count
		
	#壁の点のリストを設定
	def SetWallPointList(self,roomNum,list):
		room = self._RoomList[roomNum]
		room.SetWallPointList(list)
		return list
		
	#壁の点のリストを取得
	def GetWallPointList(self,roomNum):
		room = self._RoomList[roomNum]
		list = room.GetWallPointList()
		return list
		
	#壁の表示状態のリストを取得
	def GetWallVisibleList(self,roomNum):
		room = self._RoomList[roomNum]
		list = room.GetWallVisibleList()
		return list
		
	#ドアのリストを取得
	def GetDoorList(self,roomNum):
		room = self._RoomList[roomNum]
		list = room.GetDoorList()
		return list
		
	#窓のリストを取得
	def GetWindowList(self,roomNum):
		room = self._RoomList[roomNum]
		list = room.GetWindowList()
		return list
		
	#モデル名を取得
	def GetFileName(self,roomNum):
		room = self._RoomList[roomNum]
		
		fileName = ''
		roomName = ''
		if room != None:	
			fileName,roomName = room.GetFileName()
			
		return fileName,roomName
		
	#オペ室かどうかの設定
	def SetOpRoomState(self,roomNum,state):
		room = self._RoomList[roomNum]
		room.SetOpRoomState(state)
		
	#オペ室かどうかの取得
	def GetOpRoomState(self,roomNum):
		room = self._RoomList[roomNum]
		state = room.GetOpRoomState()
		return state
		
	#全部屋の壁を作成
	def CreateAllWallModel(self):
		#print 'CreateAllWallModel'
		
		for room in self._RoomList:
			room.SetAllWallModel()
		
	#全部屋の壁を削除
	def DeleteAllWallModel(self):
		#print 'DeleteAllWallModel'
		
		for room in self._RoomList:
			room.DelAllWallModel()
					
	#IDを入れ替え
	def ChangeId(self,num):
		#print 'changeID',num
		newRoomNum = len(self._RoomList)-1
		
		#リストを更新
		newRoomList = []
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			if x == num:
				newItem = self._RoomList[newRoomNum]
				newRoomList.append(newItem)
			if x != newRoomNum:
				newRoomList.append(room)
		self._RoomList = newRoomList
		
	#間取りの頂点位置のリスト取得
	def GetWallPointList(self,roomNum):
		room = self._RoomList[roomNum]
		pointList = room.GetWallPointList()
		return pointList
		
	#間取りのテクスチャ情報取得
	def GetTextureData(self,roomNum):
		room = self._RoomList[roomNum]
		floorTexFile,floorTexSize		= room.GetFloorTextureSetting()
		wallTexFile,wallTexSize			= room.GetWallTextureSetting()
		ceilingTexFile,ceilingTexSize	= room.GetCeilingTextureSetting()
		
		texData = [[floorTexFile,floorTexSize],[wallTexFile,wallTexSize],[ceilingTexFile,ceilingTexSize]]
		return texData
		
	#間取りのドア情報取得
	def GetDoorData(self,roomNum):
		doorData = []
		
		room = self._RoomList[roomNum]
		doorCount = room.GetDoorCount()
		for x in range(doorCount):
			data = room.GetDoorState(x)
			doorData.append(data)
		
		return doorData
		
	#間取りの窓情報取得
	def GetWindowData(self,roomNum):
		windowData = []
		
		room = self._RoomList[roomNum]
		windowCount = room.GetWindowCount()
		for x in range(windowCount):
			data = room.GetWindowState(x)
			windowData.append(data)
		
		return windowData
		
	#間取りのドアIDを入れ替え
	def ChangeDoorId(self,roomNum,num):
		#print 'changeDoorID',num
		room = self._RoomList[roomNum]
		room.ChangeDoorId(num)
		
	#間取りの窓IDを入れ替え
	def ChangeWindowId(self,roomNum,num):
		#print 'changeDoorID',num
		room = self._RoomList[roomNum]
		room.ChangeWindowId(num)
		
	#ドアの数を取得
	def GetDoorCount(self,roomNum):
		room = self._RoomList[roomNum]
		count = room.GetDoorCount()
		return count
		
	#窓の数を取得
	def GetWindowCount(self,roomNum):
		room = self._RoomList[roomNum]
		count = room.GetWindowCount()
		return count
		
	#壁の透過設定のリストを取得
	def GetWallVisibleList(self,roomNum):
		room = self._RoomList[roomNum]
		list = room.GetWallVisibleList()
		return list
		
	#オペ室かどうかのパラメータを取得
	def GetIsOpRoom(self,roomNum):
		room = self._RoomList[roomNum]
		isOpRoom = room.GetIsOpRoom()
		return isOpRoom
		
	#ドアの情報を取得
	def GetDoorState(self,roomNum,doorNum):
		room = self._RoomList[roomNum]
		state = room.GetDoorState(doorNum)
		return state
		
	#窓の情報を取得
	def GetWindowState(self,roomNum,windowNum):
		room = self._RoomList[roomNum]
		state = room.GetWindowState(windowNum)
		return state
		
	#2D、3Dの視点切り替え時に表示状態を設定
	def SetRoomVisibleState(self):
		if self._Is3d == False:
			self.SetWallLineVisible(True)
			self.SetWallPointVisible(True)
			self.SetDoorLineVisible(True)
			self.SetWindowLineVisible(True)
			self.SetCeilingVisible(False)
			
			self.DeleteAllDoorHole()		#ダミードアを削除
			self.DeleteAllWindowHole()		#ダミー窓を削除
			
			self.DeleteAllWallModel()		#壁モデルを削除
			
			self.HideWall(True)
			
		else:
			self.SetWallLineVisible(False)
			self.SetWallPointVisible(False)
			self.SetDoorLineVisible(False)
			self.SetWindowLineVisible(False)
			self.SetCeilingVisible(False)
			
			self.AddAllDoorHole()			#ダミードアを作成
			self.AddAllWindowHole()			#ダミー窓を作成
			
			self.CreateAllWallModel()		#壁モデルを作成
			
			self.HideWall(False)
			
	#---------------------------------------------------------
	def CeilingVisibleUpdate(self):
		"""
		パスシミュレータでの天井の表示切り替え
		"""
		viewPos = [0.0] * 3
		if self.__mViewVR and self.__mViewVR.GetVisibleState():
			viewPos = self.__mViewVR.GetViewPos(viz.ABS_GLOBAL)
		else:
			viewPos = viz.MainWindow.getView().getPosition()
		
		for room in self._RoomList:
			if viewPos[1] < room.GetWallHeight():
				room.SetCeilingVisible(True)
			else:
				room.SetCeilingVisible(False)
				
	#---------------------------------------------------------
	def GetDoorPosition(self,roomNum,num):
		"""
		ドアモデルの位置を取得
		"""
		room = self._RoomList[roomNum]
		door = room.GetDoorModel(num)
		doorModel = door.GetModel()
		doorPos = doorModel.getPosition(viz.ABS_GLOBAL)
		
		return doorPos
		
	#窓モデルの位置を取得
	def GetWindowPosition(self,roomNum,num):
		room = self._RoomList[roomNum]
		window = room.GetWindowModel(num)
		windowModel = window.GetModel()
		windowPos = windowModel.getPosition(viz.ABS_GLOBAL)
		
		return windowPos
		
	#ドアを別の間取りにコピー
	def CopyDoorToOtherRoom(self,roomNum,num,newRoomNum):
		room = self._RoomList[roomNum]
		newRoom = self._RoomList[newRoomNum]
		
		newNum = None
		if num != None:
			doorData = room.GetDoorState(num)
			newNum = newRoom.AddDoor(doorData[0],doorData[1],doorData[2])
			
		return newNum
		
	#窓を別の間取りにコピー
	def CopyWindowToOtherRoom(self,roomNum,num,newRoomNum):
		room = self._RoomList[roomNum]
		newRoom = self._RoomList[newRoomNum]
		
		newNum = None
		if num != None:
			windowData = room.GetWindowState(num)
			newNum = newRoom.AddWindow(windowData[0],windowData[2],windowData[3])
			newRoom.SetWindowHeight(newNum,windowData[1])
			
		return newNum
		