﻿# coding: utf-8

import viz
import vizmat
import vizinput
import vizshape
import vizact

import Interface
import Item

import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム制御のクラス定義
class ItemControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Init ItemControl')
		
		self._SelectModelClass = None
		self._ModelControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		self._PathControlClass = None
		
		self._ItemIniFileName = ""
		self._ItemIniFile = None
		self._ItemIniDataCount = 0
		self._ItemIniDataList = []
		
		self._SaveIniFile = None
		self._LoadErrorFlag=False	#takahashi 2014/8/1
		
		self._ModelList = []
		self._ModelNameList = []
		
		self._WalkSpeed		= 7
		
		self._AnimationState = 0
		self._PreAnimationState = 0
		
		self._ItemAnimationPathList  = []
		self._ItemAnimationDummyList = []
		
		self._Is3d = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Start ItemControl Update')
		pass
		
	#使用するクラスの追加
	def AddClass(self,selectModelClass,modelControlClass,undoClass,redoClass,pathControlClass):
		self._SelectModelClass = selectModelClass
		self._ModelControlClass = modelControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		self._PathControlClass = pathControlClass
		
	#INIファイルを基にアイテムを読み込む
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Load')
		
		self._LoadErrorFlag=False	#takahashi 2014/8/1
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		total = iniFile.GetItemData('Items','Total')

		if total == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはアイテム定義ファイルではありません。', title='RiMMInspector')
			self._LoadErrorFlag=True	#takahashi 2014/8/1

		else:
			#全アイテムを削除
			self.DeleteAll()
			self._PathControlClass.DeleteAllCharacter(True)
		
			self._ItemIniFileName = fileName
			
			#logファイルへのパスを取得
			fileNameData = fileName.split('\\')
			fName = ''
			fState = False
			for x in range(len(fileNameData)):
				data = fileNameData[x]
				if x == len(fileNameData)-1:
					pass
				elif fState:
					fName = fName + data + '\\' 
				if data == 'Settings':
					fState = True
					
			#読み込み用iniファイルの作成
			self._ItemIniFile = iniFile
			
			#情報をリストに設定
			total = self._ItemIniFile.GetItemData('Items','Total')
			self._ItemIniDataCount = int(total)
			
			self._ItemIniDataList = []
			for x in range(self._ItemIniDataCount):
				
				#name
				name = self._ItemIniFile.GetItemData('Items',str(x))
				
				#model
				model = self._ItemIniFile.GetItemData(name,'model')
				
				#collision
				collisionTemp = self._ItemIniFile.GetItemData(name,'collision')
				collision = [0,0,0,1,1,1]
				collision[0] = float(collisionTemp.split(',')[0])
				collision[1] = float(collisionTemp.split(',')[1])
				collision[2] = float(collisionTemp.split(',')[2])
				collision[3] = float(collisionTemp.split(',')[3])
				collision[4] = float(collisionTemp.split(',')[4])
				collision[5] = float(collisionTemp.split(',')[5])
				
				#position
				positionTemp = self._ItemIniFile.GetItemData(name,'position')
				position = [0,0,0]
				position[0] = float(positionTemp.split(',')[0])
				position[1] = float(positionTemp.split(',')[1])
				position[2] = float(positionTemp.split(',')[2])
				
				#angle
				angleTemp = self._ItemIniFile.GetItemData(name,'angle')
				angle = [0,0,0,0]
				angle[0] = float(angleTemp.split(',')[0])
				angle[1] = float(angleTemp.split(',')[1])
				angle[2] = float(angleTemp.split(',')[2])
				angle[3] = float(angleTemp.split(',')[3])
				
				#animation
				animationTemp = self._ItemIniFile.GetItemData(name,'animation')
				animation = ""
				if animationTemp != None:
					animation = animationTemp
					
				#in
				pInTemp = self._ItemIniFile.GetItemData(name,'in')
				pIn = ""
				if pInTemp != None:
					pIn = pInTemp
				
				#out
				pOutTemp = self._ItemIniFile.GetItemData(name,'out')
				pOut = ""
				if pOutTemp != None:
					pOut = pOutTemp
				
				#Motion
				motionTemp = self._ItemIniFile.GetItemData(name,'motion')
				motion = ""
				if motionTemp != None:
					motion = motionTemp
				
				#target
				targetTemp = self._ItemIniFile.GetItemData(name,'target')
				target = 0
				if targetTemp != None:
					target = int(targetTemp)
				
				#size
				sizeTemp = self._ItemIniFile.GetItemData(name,'size')
				size = [0.0,0.0,0.0]
				if sizeTemp != None:
					size[0] = float(sizeTemp.split(',')[0])
					size[1] = float(sizeTemp.split(',')[1])
					size[2] = float(sizeTemp.split(',')[2])
				
				#texture
				textureList = []
				for x in range(100):
					textureName = "Texture"+str(x)
					textureTemp = self._ItemIniFile.GetItemData(name,textureName)
					if textureTemp != None:
						nodeName = textureTemp.split(',')[0]
						textureFile = textureTemp.split(',')[1]
						texture = [nodeName,textureFile]
						textureList.append(texture)
					else:
						break
				
				#moveYState
				moveYStateTemp = self._ItemIniFile.GetItemData(name,'moveystate')
				moveYState = 1
				if moveYStateTemp != None:
					moveYState = int(moveYStateTemp)
				else:	#値がなかったら1にする（過去のもの同じように）
					moveYState = 1
				
				#sound
				soundTemp = self._ItemIniFile.GetItemData(name,'sound')
				sound = None
				if soundTemp != None:
					sound = soundTemp
					
				#light
				lightTemp = iniFile.GetItemData(name,'light')
				light = 0
				if lightTemp != None:
					light = int(lightTemp)
					
				#lightIntensity（照度）
				lightIntensityTemp = iniFile.GetItemData(name,'lightintensity')
				lightIntensity = 0
				if lightIntensityTemp != None:
					lightIntensity = float(lightIntensityTemp)
					
				#lightAttenuation（減衰）
				lightAttenuationTemp = iniFile.GetItemData(name,'lightattenuation')
				lightAttenuation = 0
				if lightAttenuationTemp != None:
					lightAttenuation = float(lightAttenuationTemp)
					
				#lightSpread（広がり）
				lightSpreadTemp = iniFile.GetItemData(name,'lightspread')
				lightSpread = 0
				if lightSpreadTemp != None:
					lightSpread = float(lightSpreadTemp)
					
				#モデルを読み込み、配置
				modelNum = self.Add(model)
				self.SetPosition(modelNum,position)
				self.SetAxisAngle(modelNum,angle)
				"""
				if model[-13:] == 'DummyBox.osgb': 		#ダミーボックスのみサイズとテクスチャを設定を反映
					self.SetSize(modelNum,size)
					for texture in textureList:
						nodeName = texture[0]
						textureFile = texture[1]
						self.SetTexture(modelNum,nodeName,textureFile)
				"""
				self.SetSize(modelNum,size)
				for texture in textureList:
					nodeName = texture[0]
					textureFile = texture[1]
					self.SetTexture(modelNum,nodeName,textureFile)
				self.SetMoveYState(modelNum,moveYState)
				
				#音源関係の設定
				if sound != None:
					self.SetSound(modelNum,sound)
					
				#ライト関係の設定
				if light != -1:
					self.SetIntensity(modelNum,lightIntensity)
					self.SetAttenuation(modelNum,lightAttenuation)
					self.SetSpread(modelNum,lightSpread)
					
				item = self.GetModel(modelNum)
				
				#情報をリストに追加
				data = [name,model,collision,position,angle,animation,pIn,pOut,motion,target,modelNum,item]
				self._ItemIniDataList.append(data)
				
				#ログを読み込む
				log = self._ItemIniFile.GetItemData(name,'log')
				if log != None:
					logPath = log
					logData = log.split('\\')
					if len(logData) == 1:
						logPath = fName + log
						
					logDataCountTemp = self._ItemIniFile.GetItemData(log,'total')
					if logDataCountTemp == None:
						self._PathControlClass.Load(modelNum,logPath,True,usePathFile=True)
					else:
						pData = []
						logDataCount = int(logDataCountTemp)
						for x in range(logDataCount):
							dataTemp = self._ItemIniFile.GetItemData(log,str(x))
							dataTemp = dataTemp.split(',')
							pData.append([float(dataTemp[0]),float(dataTemp[1]),float(dataTemp[2]),float(dataTemp[3])])
						#print 'pData',pData
						self._PathControlClass.Load(modelNum,pData,True)
						
			#Undoの情報をクリア
			self._UndoClass.ClearList()
			self._RedoClass.ClearList()
			
	#INIファイルにアイテムを保存する
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Save')
		
		#パスのファイル名を作成
		fileNameData = fileName.split('\\')
		
		fName = ''
		fState = False
		for x in range(len(fileNameData)):
			data = fileNameData[x]
			if x == len(fileNameData)-1:
				fName = fName + data[:-4]
			elif fState:
				fName = fName + data + '\\' 
			if data == 'Settings':
				fState = True
		
		#名前を取得
		pathName = fileNameData[len(fileNameData)-1][:-4]
		
		#保存用iniファイルの作成
		self._SaveIniFile = LoadIniFile.LoadIniFileClass()
		self._SaveIniFile.ResetIniData()
		
		self._SaveIniFile.AddSection('Items')
		self._SaveIniFile.AddItem('Items','Total',str(self._ItemIniDataCount))
		
		for x in range(self._ItemIniDataCount):
			data = self._ItemIniDataList[x]
			
			self._SaveIniFile.AddItem('Items',str(x),data[0])
			
			#各モデルの情報を追加
			modelNum = data[10]
			item = data[11]
			
			#name
			self._SaveIniFile.AddSection(data[0])
			
			#model
			self._SaveIniFile.AddItem(data[0],'model',data[1])
			
			#collision
			collisionTemp = data[2]
			collision = str(collisionTemp[0])
			for y in range(5):
				collision = collision + ',' + str(collisionTemp[y+1])
			self._SaveIniFile.AddItem(data[0],'collision',collision)
			
			#position
			positionTemp = item.GetPosition()
			position = str(positionTemp[0])
			for y in range(2):
				position = position + ',' + str(positionTemp[y+1])
			self._SaveIniFile.AddItem(data[0],'position',position)
			
			#angle
			angleTemp = item.GetAxisAngle()
			angle = str(angleTemp[0])
			for y in range(3):
				angle = angle + ',' + str(angleTemp[y+1])
			self._SaveIniFile.AddItem(data[0],'angle',angle)
			
			#animation
			self._SaveIniFile.AddItem(data[0],'animation',data[5])
		
			#in
			self._SaveIniFile.AddItem(data[0],'in',data[6])
			
			#out
			self._SaveIniFile.AddItem(data[0],'out',data[7])
			
			#motion
			self._SaveIniFile.AddItem(data[0],'motion',data[8])
			
			#target
			self._SaveIniFile.AddItem(data[0],'target',str(data[9]))
			
			#size
			sizeTemp = item.GetSize()
			size = str(sizeTemp[0])
			for y in range(2):
				size = size + ',' + str(sizeTemp[y+1])
			self._SaveIniFile.AddItem(data[0],'size',size)
			
			#texture
			textureList = item.GetTextureList()
			for y in range(len(textureList)):
				textureName = 'texture' + str(y)
				textureTemp = textureList[y]
				texture = str(textureTemp[0]) + ',' + str(textureTemp[1])
				self._SaveIniFile.AddItem(data[0],textureName,texture)
			
			#moveYState
			moveYStateTemp = item.GetMoveYState()
			moveYState = str(moveYStateTemp)
			self._SaveIniFile.AddItem(data[0],'moveystate',moveYState)
			
			#sound
			sound = item.GetSound()
			self._SaveIniFile.AddItem(data[0],'sound',sound)
			
			#ライト定義ファイルの場合
			if item.GetLightState():
				#light
				lightCategoryTemp,lightIntensityTemp,lightAttenuationTemp,lightSpreadTemp = item.GetLightData()
				lightCategory = str(lightCategoryTemp+1)
				self._SaveIniFile.AddItem(data[0],'light',lightCategory)
				
				lightIntensity = str(lightIntensityTemp)
				self._SaveIniFile.AddItem(data[0],'lightintensity',lightIntensity)
				
				lightAttenuation = str(lightAttenuationTemp)
				self._SaveIniFile.AddItem(data[0],'lightattenuation',lightAttenuation)
				
				lightSpread = str(lightSpreadTemp)
				self._SaveIniFile.AddItem(data[0],'lightspread',lightSpread)
				
			#path
			pathData = self._PathControlClass.GetPathData(x,True)
			if len(pathData) != 1:
				pathDataName = data[0]+'_PathData'
				
				#self._SaveIniFile.AddItem(data[0],'log',log)
				self._SaveIniFile.AddItem(data[0],'log',pathDataName)
				
				log = pathName + '_log_' + str(x+1) + '.log'; 
				angle = item.GetOrientation()
				logPath = fName + '_log_' + str(x+1) + '.log'; 
				pData = self._PathControlClass.Save(x,logPath,angle,True)
				
				self._SaveIniFile.AddSection(pathDataName)
				self._SaveIniFile.AddItem(pathDataName,'total',str(len(pData)))
				for y in range(len(pData)):
					pData2 = pData[y]
					dataStr = str(pData2[0]) + ',' + str(pData2[1]) + ',' + str(pData2[2]) + ',' + str(pData2[3]); 
					
					self._SaveIniFile.AddItem(pathDataName,str(y),dataStr)
					
		self._SaveIniFile.WriteIniData(fileName)
		self._ItemIniFileName = fileName
		
	#読込みに成功したかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._LoadErrorFlag
	
	#追加
	def Add(self,fileName):
		model = Item.Item(self._ModelControlClass)
		model.Add(fileName)	#モデルを追加
		
		self._ModelList.append(model)
		self._ModelNameList.append(fileName)
		
		num = len(self._ModelList) - 1
		
		model.SetId(num)
		model.SetViewState(self._Is3d)
		
		self._PathControlClass.AddCharacter(True)
		
		return num
	
	#新規モデルをリストに追加
	def AddNewItemToList(self,modelNum):
		item = self.GetModel(modelNum)
		
		name = 'NewItem' + str(modelNum)
		model = self._ModelNameList[modelNum]
		collision = [0,0,0,1,1,1]
		position = item.GetPosition()
		angle = item.GetAxisAngle()
		animation = ""
		pIn = ""
		pOut = ""
		motion = ""
		target = 0
		
		#情報をリストに追加
		data = [name,model,collision,position,angle,animation,pIn,pOut,motion,target,modelNum,item]
		self._ItemIniDataList.append(data)
		self._ItemIniDataCount = len(self._ItemIniDataList)
		
	#削除
	def Delete(self,num):
		deleteModel = self._ModelList[num]
		
		#モデルリストを更新
		newModelLiset = []
		newModelNameList = []
		
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			if model != deleteModel:
				fileName = self._ModelNameList[x]
				
				newModelLiset.append(model)
				newModelNameList.append(fileName)
						
		self._ModelList = newModelLiset
		self._ModelNameList = newModelNameList
		
		#データリストを更新
		newItemIniDataList = []
		changeRowState = False
		for x in range(len(self._ItemIniDataList)):
			data = self._ItemIniDataList[x]
			
			if data[11] == deleteModel:
				changeRowState = True
			else:
				if changeRowState == True:
					data[10] = data[10] - 1
					
					if data[0].startswith('NewItem'):
						name = data[0]
						newNum = int(name[7:])
						newName = 'NewItem' + str(newNum-1)
						data[0] = newName
						
				newItemIniDataList.append(data)
				
		self._ItemIniDataList = newItemIniDataList
		self._ItemIniDataCount = len(self._ItemIniDataList)
		
		#モデルを削除
		if deleteModel != None:
			deleteModel.Delete()
			del deleteModel
		
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
		self._PathControlClass.DeleteCharacter(num,True)
		
	#全モデル削除
	def DeleteAll(self):
		self._SelectModelClass.UnSelectItem()
		
		modelCount = self.GetItemCount()
		for x in range(modelCount):
			self.Delete(0)
			
		self._ItemIniFileName = ""
		
		#Undoの情報をクリア
		self._UndoClass.ClearList()
		self._RedoClass.ClearList()
		
		self._PathControlClass.DeleteAllCharacter(True)
		
	#コピー
	def Copy(self,num):
		model = self._ModelList[num]
		modelName = self._ModelNameList[num]
		
		pathPointList = []
		pathData	= self._PathControlClass.GetPathData(num,True)
		for x in range(len(pathData)):
			path = self._PathControlClass.GetPath(num,x,True)
			pathPos = path.GetPosition()
			pathPointList.append(pathPos)
				
		#追加
		modelNum = self.Add(modelName)
		
		#少しずらして配置
		originalPosition = model.GetPosition()
		position = [0,0,0]
		position[0] = originalPosition[0] + 1.0
		position[1] = originalPosition[1]
		position[2] = originalPosition[2] + 1.0
		
		self.SetPosition(modelNum,position)
		
		#角度設定
		originalOrientation = model.GetOrientation()
		self.SetOrientation(modelNum,originalOrientation)
		
		"""
		if modelName[-13:] == 'DummyBox.osgb': 		#ダミーボックスのみサイズ設定を反映
			#サイズ設定
			originalSize = model.GetSize()
			self.SetSize(modelNum,originalSize)
			
			#テクスチャ設定
			originalTextureList = model.GetTextureList()
			for texture in originalTextureList:
				node = texture[0]
				filePath = texture[1]
				self.SetTexture(modelNum,node,filePath)
		"""
		
		#サイズ設定
		originalSize = model.GetSize()
		self.SetSize(modelNum,originalSize)
		
		#テクスチャ設定
		originalTextureList = model.GetTextureList()
		for texture in originalTextureList:
			node = texture[0]
			filePath = texture[1]
			self.SetTexture(modelNum,node,filePath)
			
		#縦方向の移動制限の設定
		moveYState = model.GetMoveYState()
		self.SetMoveYState(modelNum,moveYState)
		
		#音源
		sound = model.GetSound()
		self.SetMoveYState(modelNum,sound)
		
		#パスの設定	
		for y in range(len(pathPointList)):
			if y != 0:
				path = self._PathControlClass.Add(modelNum,True)
				pos = pathPointList[y]	#位置を少しずらす
				newPos = [0,0,0]
				newPos[0] = pos[0] + 1.0
				newPos[1] = pos[1]
				newPos[2] = pos[2] + 1.0
				#self._CopyItemList[x][6][y] = newPos 
				self._PathControlClass.SetPosition(modelNum,path,newPos,True)
				
		self.AddNewItemToList(modelNum)
		
	#位置設定
	def SetPosition(self,num,position,withPath=True):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPosition(position)
			
			pathPos = [0,0,0]
			pathPos[0] = position[0]
			pathPos[1] = position[1]
			pathPos[2] = position[2]
			
			if withPath:
				self._PathControlClass.SetPosition(num,0,pathPos,True)
				
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
			
		return position
		
	#角度設定
	def SetOrientation(self,num,orientation,snap=True):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation,snap=snap)
			
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self,num):
		model = self._ModelList[num]
		
		if model != None:	
			model.ResetOrientationSnap()
		
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#角度設定(INIファイル用)
	def SetAxisAngle(self,num,axisAngle):
		model = self._ModelList[num]
		
		if model != None and axisAngle != None:	
			model.SetAxisAngle(axisAngle)
			
	#角度取得(INIファイル用)
	def GetAxisAngle(self,num):
		model = self._ModelList[num]
		axisAngle = [0,0,0,0]
		
		if model != None:
			axisAngle = model.GetAxisAngle()
		
		return axisAngle
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
	#ハイライト状態の取得
	def GetHighlightState(self,num):
		model = self._ModelList[num]
		state = model.GetHighlightState()
		
		return state
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model or self._ModelList[x]._PlaneModel == model:
				num =  x
				
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for model in self._ModelList:
			model.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#サイズ設定
	def SetSize(self,num,size):
		model = self._ModelList[num]
		if model != None:
			model.SetSize(size)
			
	#サイズ取得
	def GetSize(self,num):
		model = self._ModelList[num]
		size = [1,1,1]
		
		if model != None:
			size = model.GetSize()
			
		return size
		
	#テクスチャ設定
	def SetTexture(self,num,node,filePath):
		model = self._ModelList[num]
		if model != None:
			model.SetTexture(node,filePath)
			
	#テクスチャ取得
	def GetTexture(self,num,node):
		model = self._ModelList[num]
		texture = None
		
		if model != None:
			texture = model.GetTexture(node)
		
		return texture
		
	#テクスチャのファイルパス取得
	def GetTextureFilePath(self,num,node):
		model = self._ModelList[num]
		filePath = model.GetTextureFilePath(node)
		
		return filePath
		
	#テクスチャのリスト取得
	def GetTextureList(self,num):
		model = self._ModelList[num]
		texList = model.GetTextureList()
		
		return texList
		
	#テクスチャ再設定
	def ResetTexture(self,num,node,texture,filePath):
		model = self._ModelList[num]
		if model != None:
			model.ResetTexture(node,texture,filePath)
			
	#アイテム数を取得
	def GetItemCount(self):
		count = len(self._ModelList)
		return count
		
	#モデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			itemModel = item.GetModel()
			modelList.append(itemModel)
		
		return modelList
	
	#照明モデルのリストを取得
	def GetLightModelList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			fileName = item.GetFileName()
			if fileName[-15:-10] == 'light':	#ライトモデルかどうかファイル名で判定
				itemModel = item.GetModel()
				modelList.append(itemModel)
				
		return modelList
		
	#照明以外のモデルのリストを取得
	def GetModelWithoutLightList(self):
		modelList = []
		
		itemList = self._ModelList
		for item in itemList:
			fileName = item.GetFileName()
			if fileName[-15:-10] != 'light':	#ライトモデルかどうかファイル名で判定
				itemModel = item.GetModel()
				modelList.append(itemModel)
				
		return modelList
		
	#パスの削除
	def DeletePath(self,num):
		self._PathControlClass.DeleteCharacterPath(num,True)
		
	#パスのデータを取得
	def GetPathData(self,characterNum):
		return self._PathControlClass.GetPathData(characterNum,True)
		
	#パスの位置リストを取得
	def GetPathPositionList(self,characterNum):
		return self._PathControlClass.GetPathPositionList(characterNum,True)
		
	#パスの位置リストを設定
	def SetPathPositionList(self,characterNum,posList):
		self._PathControlClass.SetPathPositionList(characterNum,posList,True)
		
	#INIファイル名を取得
	def GetIniFileName(self):
		val = self._ItemIniFileName
		
		return val
		
	#縦方向の移動制限を設定
	def SetMoveYState(self,num,state):
		model = self._ModelList[num]
		
		if model != None and state != None:	
			model.SetMoveYState(state)
	
	#縦方向の移動制限を取得
	def GetMoveYState(self,num):
		model = self._ModelList[num]
		
		state = False
		if model != None:	
			state = model.GetMoveYState()
			
		return state
	
	#音源を設定
	def SetSound(self,num,sound):
		model = self._ModelList[num]
		
		if model != None and sound != None:	
			model.SetSound(sound)
	
	#音源を取得
	def GetSound(self,num):
		model = self._ModelList[num]
		
		sound = None
		if model != None:	
			sound = model.GetSound()
			
		return sound
	
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		for item in self._ModelList:
			item.SetViewState(state)
		
	#モデル名を取得
	def GetFileName(self,num):
		model = self._ModelList[num]
		
		fileName = ''
		if model != None:	
			fileName = model.GetFileName()
			
		return fileName
		
	#ライトの設定情報を取得
	def GetLightData(self,num):
		item = self._ModelList[num]
		category,intensity,attenuation,spread = item.GetLightData()
		
		return category,intensity,attenuation,spread
		
	#照度を設定
	def SetIntensity(self,num,intensity):
		item = self._ModelList[num]
		item.SetIntensity(intensity)
		
	#減衰を設定
	def SetAttenuation(self,num,attenuation):
		item = self._ModelList[num]
		item.SetAttenuation(attenuation)
		
	#広がりを設定
	def SetSpread(self,num,spread):
		item = self._ModelList[num]
		item.SetSpread(spread)
		
	#ライトかどうかの状態取得
	def GetLightState(self,num):
		item = self._ModelList[num]
		state = item.GetLightState()
		return state
		
	#ライトのアクティブ状態を設定
	def SetLightState(self,num,state):
		item = self._ModelList[num]
		item.SetLightState(state)
		
	#IDを入れ替え
	def ChangeId(self,num):
		#print 'changeID',num
		newItemNum = len(self._ModelList)-1
		
		#リストを更新
		newModelList = []
		newModelNameList = []
		newItemIniDataList = []
		
		for x in range(len(self._ModelList)):
			item		= self._ModelList[x]
			itemName	= self._ModelNameList[x]
			itemData	= self._ItemIniDataList[x]
			
			if x == num:
				newItem		= self._ModelList[newItemNum]
				newItemName	= self._ModelNameList[newItemNum]
				newItemData	= self._ItemIniDataList[newItemNum]
				
				newModelList.append(newItem)
				newModelNameList.append(newItemName)
				newItemIniDataList.append(newItemData)
				
			if x != newItemNum:
				newModelList.append(item)
				newModelNameList.append(itemName)
				newItemIniDataList.append(itemData)
				
		self._ModelList			= newModelList
		self._ModelNameList		= newModelNameList
		self._ItemIniDataList	= newItemIniDataList
		
		self.UpdateId()
		
		self._PathControlClass.ChangeId(num,True)
		
		#デバッグ用
		#for x in range(self._ItemIniDataCount):
		#	data = self._ItemIniDataList[x]
		#	print 'data(changeID)=',x,data[0],data[10]
		
	#IDを更新
	def UpdateId(self):
		#アイテムのIDを更新
		for x in range(len(self._ModelList)):
			item = self._ModelList[x]
			item.SetId(x)
			
			newName = 'NewItem' + str(x)
			self._ItemIniDataList[x][0] = newName
			self._ItemIniDataList[x][10] = x
			self._ItemIniDataList[x][11] = item
			
	#ダミーボックスのラインの表示を設定
	def SetDummyBoxLineVisible(self,state):
		for item in self._ModelList:
			item.SetDummyBoxLineVisible(state)
			
	#全アイテムのパスの表示状態設定
	def SetAllItemLineVisible(self,state):
		self._PathControlClass.SetAllLineVisible()
		
		for x in range(len(self._ModelList)):
			item = self._ModelList[x]
			itemModel = item.GetModel()
			if itemModel.getVisible():
				self._PathControlClass.SetLineVisible(x,state)
			else:
				self._PathControlClass.SetLineVisible(x,False)
				
	#全アイテムのパスの表示状態設定
	def SetAllItemLineColor(self,state):
		self._PathControlClass.SetLineColor(state)
			
	#パスシム用のパスの表示設定
	def ChangeLineVisible(self,flag):
		for x in range(len(self._ModelList)):
			item = self._ModelList[x]
			itemModel = item.GetModel()
			if itemModel.getVisible():
				self.SetAllItemLineVisible(flag)
				break
		
	#パスシム用パスを作成
	def CreateItemAnimationPath(self):
		if len(self._ItemAnimationPathList) != 0:
			self.DeleteItemAnimationPath()
			
		for x in range(len(self._ModelList)):
			pathData = self._PathControlClass.GetPathData(x,True)
			path,dummy = self.CreateAnimationPath(pathData)
			
			self._ItemAnimationPathList.append(path)
			self._ItemAnimationDummyList.append(dummy)
			
			
	#パスシム用パスを削除
	def DeleteItemAnimationPath(self):
		self.Reset()
		
		for x in range(len(self._ModelList)):
			item  = self._ModelList[x]
			itemModel = item.GetModel()
			if item.GetDummyBoxRoot() != None:
				itemModel = item.GetDummyBoxRoot()
				
			itemModel.setPosition(item.GetPosition())
			itemModel.setEuler(item.GetOrientation(),0,0)
		
		for x in range(len(self._ItemAnimationPathList)):
			animationPath = self._ItemAnimationPathList[x]
			animationPath.remove()
			del animationPath
			
		self._ItemAnimationPathList = []
		
		for x in range(len(self._ItemAnimationDummyList)):
			animationDummy = self._ItemAnimationDummyList[x]
			animationDummy.remove()
			del animationDummy
			
		self._ItemAnimationDummyList = []
		
	#アニメーションパスを作成
	def CreateAnimationPath(self,pathData):
		#位置のリストを作成
		positionList = []
		distanceList = []
		allDistance = 0.0
		allDistanceList = []
		
		#長さを確認
		distanceList.append(0.0)
		allDistanceList.append(0.0)
		for x in range(len(pathData)-1):
			path0 = pathData[x]
			pathPos0 = path0.GetPosition()
			pathPos0[1] = 0
			
			path1 = pathData[x+1]
			pathPos1 = path1.GetPosition()
			pathPos1[1] = 0
			
			distance = vizmat.Distance(pathPos0,pathPos1)
			
			distanceList.append(distance)
			allDistance = allDistance + distance
			allDistanceList.append(allDistance)
			
		#分割数は10cm
		deltaDistance = 0.1
		count = int(allDistance * 10)
		
		dist = 0.0
		for x in range(count):
			dist = dist + deltaDistance
					
			#最初の点を設定
			if x == 0:
				path = pathData[x]
				positionList.append(path.GetPosition())
				
			#最後の点を設定
			elif x == count-1:
				lastNum = len(pathData)-1
				path = pathData[lastNum]
				positionList.append(path.GetPosition())
				
			elif x < count-1:
				for y in range(len(allDistanceList)):
					p = allDistanceList[y]
					if dist < p or dist == p:
						
						#割合を確認
						distMin = allDistanceList[y-1]
						distMax = allDistanceList[y]
						
						pathMin = pathData[y-1]
						pathMax = pathData[y]
						
						perc = (dist - distMin) / (distMax - distMin)
						
						#位置設定
						pathPosMin = pathMin.GetPosition()
						pathPosMax = pathMax.GetPosition()
						
						pos = [0,0,0]
						pos[0] = pathPosMin[0]+(pathPosMax[0]-pathPosMin[0])*perc
						pos[1] = pathPosMax[1]
						pos[2] = pathPosMin[2]+(pathPosMax[2]-pathPosMin[2])*perc
						
						positionList.append(pos)
						
						break
						
		#パス作成
		path = viz.addAnimationPath()
		
		for x in range(len(positionList)):
			path.addControlPoint(x+1,pos=positionList[x])
		
		path.setLoopMode(viz.OFF)
		if len(positionList) > 2:
			path.computeTangents()
		#path.setTranslateMode(viz.CUBIC_BEZIER)
		path.setAutoRotate(viz.ON)
		path.setSpeed(self._WalkSpeed)
		
		path.addEventAtEnd('end')
		
		dummy = vizshape.addPlane([0.01,0.01])
		dummy.disable(viz.PICKING)
		dummy.disable(viz.RENDERING)
		dummy.visible(viz.OFF)
		dummy.alpha(0)
		dummy.setPosition(0,0,0)
		dummy.setEuler(0,0,0)
		
		viz.link(path,dummy)
		
		return path,dummy
		
	#移動の開始
	def Play(self):
		self._AnimationState = 1
		for animationPath in self._ItemAnimationPathList:
			animationPath.play()
			
	#移動の一時停止
	def Pause(self):
		self._AnimationState = 2
		for animationPath in self._ItemAnimationPathList:
			animationPath.pause()
			
	#移動の停止、リセット
	def Reset(self):
		self._AnimationState = 0
		for animationPath in self._ItemAnimationPathList:
			animationPath.pause()
			animationPath.reset()
			
	#移動の停止、最終地点にジャンプ
	def End(self):
		self._AnimationState = 3
		for x in range(len(self._ItemAnimationPathList)):
			animationPath = self._ItemAnimationPathList[x]
			animationPath.play()
			animationPath.setSpeed(1000000)
			
			vizact.onPathEvent(animationPath, 'end', self.End2,x)
			
	#移動の停止、最終地点にジャンプ2
	def End2(self,id):
		path = self._ItemAnimationPathList[id]
		path.pause()
		path.setSpeed(self._WalkSpeed)
			
	#アニメーション再生時の状態更新
	def AnimationUpdate(self):
		if self._AnimationState == 1 or self._AnimationState == 3:
			for x in range(len(self._ModelList)):
				pathData = self._PathControlClass.GetPathData(x,True)
				if len(pathData) > 1:
					item  = self._ModelList[x]
					dummy = self._ItemAnimationDummyList[x]
					itemModel = item.GetModel()
					if item.GetDummyBoxRoot() != None:
						itemModel = item.GetDummyBoxRoot()
					
					itemModel.setPosition(dummy.getPosition())
					euler = [0,0,0]
					dummyEuler = dummy.getEuler()
					euler[0] = dummyEuler[0] + 180.0
					itemModel.setEuler(euler)
					
		elif self._AnimationState == 2 and self._AnimationState != self._PreAnimationState:
			for x in range(len(self._ModelList)):
				pathData = self._PathControlClass.GetPathData(x,True)
				if len(pathData) > 1:
					item  = self._ModelList[x]
					dummy = self._ItemAnimationDummyList[x]
					itemModel = item.GetModel()
					if item.GetDummyBoxRoot() != None:
						itemModel = item.GetDummyBoxRoot()
					
					itemModel.setPosition(dummy.getPosition())
					euler = [0,0,0]
					dummyEuler = dummy.getEuler()
					euler[0] = dummyEuler[0] + 180.0
					itemModel.setEuler(euler)
					
		elif self._AnimationState == 0 and self._AnimationState != self._PreAnimationState:
			for x in range(len(self._ModelList)):
				pathData = self._PathControlClass.GetPathData(x,True)
				if len(pathData) > 1:
					item  = self._ModelList[x]
					itemModel = item.GetModel()
					if item.GetDummyBoxRoot() != None:
						itemModel = item.GetDummyBoxRoot()
					
					itemModel.setPosition(item.GetPosition())
					itemModel.setEuler(item.GetOrientation(),0,0)
					
		self._PreAnimationState = self._AnimationState
		
	#ラインモデルのリストを取得
	def GetLineModelList(self):
		lineModelList = self._PathControlClass.GetLineModelList()
		return lineModelList
		
	#パス付きのアイテムのリストを取得
	def GetItemWithPathList(self):
		list = []
		for x in range(len(self._ModelList)):
			pathData = self._PathControlClass.GetPathData(x,True)
			if len(pathData) > 1:
				item = self._ModelList[x]
				model = item.GetModel()
				list.append(model)
				
		return list
		
	#音源かどうかの状態取得
	def GetSoundState(self,num):
		item = self._ModelList[num]
		state = item.GetSoundState()
		return state
		
	#音の再生制御
	def PlaySound(self,state):
		for item in self._ModelList:
			item.PlaySound(state)
			
	#照明の制御
	def PlayLight(self,state):
		for item in self._ModelList:
			item.PlayLight(state)
			
	#照明のパラメータ取得
	def GetLightParam(self,num):
		item = self._ModelList[num]
		lightParam = item.GetLightParam()
		return lightParam
		
	#照明のパラメータ設定
	def SetLightParam(self,num,lightParam):
		item = self._ModelList[num]
		item.SetLightParam(lightParam)
		