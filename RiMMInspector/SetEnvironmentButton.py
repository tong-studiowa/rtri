﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizinput

import Object2d

import os.path
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#地形設定ボタン制御のクラス定義
class SetEnvironmentButton(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetEnvironmentButton] : Init SetEnvironmentButton')
		
		self._InputClass = None
		self._EnvironmentControlClass = None
		self._BackgroundClass = None
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		self._VisibleState = True
		
		self._IconCount = 2
		
		self._Frame = None
		self._TextList = []
		self._IconList = []
		
		self._Filter = [('INI Files','*.ini')]
		self._Directory = self.GetRootPath() + 'Settings\\.'
		
		#フレーム追加
		self._FramePosition = [0.593,0.148]
		self._FrameScale = [2.1,1.2]
		
		self._Frame = self.CreateFrame(self._FramePosition,self._FrameScale)
		
		#テキスト追加
		self._TextPositionX = 0.593
		self._TextPositionY = [0.176, 0.12]
		self._TextScale = [0.16875,0.3]
		self._TextMessage = ["読み込み","削除"]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._TextPositionX
			position[1] = self._TextPositionY[x]
			
			message = self._TextMessage[x]
			
			text = self.CreateText(message,position,self._TextScale)
			self._TextList.append(text)
			
		#アイコン追加
		self._IconPositionX = 0.593
		self._IconPositionY = [0.176, 0.12]
		self._IconScale = [2,0.5]
		
		for x in range(self._IconCount):
			
			position = [0.0,0.0]
			position[0] = self._IconPositionX
			position[1] = self._IconPositionY[x]
			
			icon = self.CreateIcon(position,self._IconScale)
			self._IconList.append(icon)
			
	#使用するクラスの追加
	def AddClass(self,inputClass,environmentControlClass,backgroundClass):
		self._InputClass = inputClass
		self._EnvironmentControlClass = environmentControlClass
		self._BackgroundClass = backgroundClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetEnvironmentButton] : Start SetEnvironmentButton Update')
		
		self._ClickState = False
		self._OverState = False
		self._OverIconState = None
		
		#マウスの重なり状態を確認
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			
			if mousePosition[0] > 0.515 and mousePosition[0] < 0.672:
				num = None
				if mousePosition[1] < 0.09:
					pass
				elif mousePosition[1] < 0.14:
					num = 1
				elif mousePosition[1] < 0.20:
					num = 0
					
				self._OverIconState = num
				
				if num != None:
					self._OverState = True
					
					#ボタン選択
					if self._InputClass.GetMouseClickState():
						self._ClickState = True
						if num == 0:
							self.LoadEnvironment()		#地形を追加
						elif num == 1:
							self.DeleteEnvironment()	#地形を削除
						
			#ロールオーバー
			self.Rollover(self._OverIconState)
				
	#リセット処理
	def Reset(self):
		self.SetVisible(False)
		self._ClickState = False
		self._OverState = False
	
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		
		for text in self._TextList:
			text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._VisibleState
		
		return state
		
	#フレームの作成
	def CreateFrame(self,position,scale):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.setPosition(position[0],position[1])
		frame.setScale(scale[0],scale[1])
		frame.color(viz.GRAY)
		frame.drawOrder(1)
		frame.alpha(0.75)
		
		return frame
		
	#テキストの作成
	def CreateText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		text.setPosition(position[0],position[1])
		text.setScale(scale[0],scale[1])
		text.color(viz.WHITE)
		text.drawOrder(2)
		
		return text
		
	#アイコンの作成
	def CreateIcon(self,position,scale):
		icon = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		icon.setPosition(position[0],position[1])
		icon.setScale(scale[0],scale[1])
		icon.color(viz.GRAY)
		icon.drawOrder(1)
		
		return icon
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
	
	#地形を読み込む
	def LoadEnvironment(self):
		filePath = vizinput.fileOpen(filter=self._Filter,file=self._Directory)
		
		if filePath != "":
			loadStaet = self._EnvironmentControlClass.Add(filePath)
			if loadStaet == True:
				#self._BackgroundClass.SetVisible(False)	#グリッドを非表示
				pass
			
		self.SetVisible(False)
			
	#地形を削除
	def DeleteEnvironment(self):
		self._EnvironmentControlClass.Delete()
		#self._BackgroundClass.SetVisible(True)	#グリッドを表示
		
		self.SetVisible(False)
			
	#ロールオーバー処理
	def Rollover(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			color = viz.GRAY
			if x == selectedIcon:
				color = [0.575,0.575,0.575]
			
			icon.color(color)
			
	#ルートフォルダの取得
	def GetRootPath(self):
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
			exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
			
		return rootDir
		