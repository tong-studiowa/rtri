﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import viz
import vizshape
import viztask

import oculus
import steamvr

import LoadIniFile
import Fove.FoveDll as Fove
import Kinect.KinectV2 as Kinect

import sys

#---------------------------------------------------------
#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

#---------------------------------------------------------
class ViewVR(object):
	
	#---------------------------------------------------------
	def __init__(self):
		"""
		コンストラクタ
		"""
		# ウィンドウセットアップ
		self.__mWindow = viz.addWindow()
		self.__mWindow.setPosition(0.0, 1.0)
		self.__mWindow.setSize(1.0, 1.0)
		self.__mWindow.clip(0.1, 3000.0)
		self.__mWindow.visible(viz.OFF)
		
		self.__mWindowMono = viz.addWindow()
		self.__mWindowMono.setPosition(0.0, 1.0)
		self.__mWindowMono.setSize(1.0, 1.0)
		self.__mWindowMono.clip(0.1, 3000.0)
		self.__mWindowMono.visible(viz.OFF)
		self.__mWindowMono.fov(70, 1280.0/960.0)
		
		# ビューセットアップ
		self.__mView = viz.addView()
		self.__mView.getHeadLight().disable()
		self.__mWindow.setView(self.__mView)
		self.__mWindowMono.setView(self.__mView)
		
		self.__mVisibleState = False
		
		self.__mWaitTask = None
			
		self.__mKinect = Kinect.KienctV2()		# Kinect
		
		self.__mNavi = viz.addGroup()
		self.__mViewLink = viz.link(self.__mNavi, self.__mView)
		
		self.__mFove = None
		self.__ballLRef = None
		self.__ballL = None
		
		viz.setListenerSound3D(self.__mView)	#3Dサウンドのリスナーを設定
		
		# 位置設定用のキューブ
		self.__mCube = vizshape.addCube()
		self.__mCube.disable(viz.RENDERING)
		self.__mCube.disable(viz.LIGHTING)
		self.__mCube.disable(viz.DEPTH_WRITE)
		self.__mCube.disable(viz.INTERSECTION)
		
		self.__mViewLink.preMultLinkable(self.__mCube)
		
		# 初期位置と角度を読み込む
		self.__mIniPos = viz.Vector([0.0] * 3)
		self.__mIniAngle = 0.0
		
		# HMDセットアップ
		self.__isSteamVr = False
		# まずOculus
		self.__mHMD = oculus.Rift(window = self.__mWindow)
		if self.__mHMD.getSensor() == None:
			self.__mHMD.remove()
			# 次はSteamVR
			self.__mHMD = steamvr.HMD(window = self.__mWindow)
			if self.__mHMD.getSensor() == None:
				raise SystemError
			else:
				self.__isSteamVr = True
		
		self.__mHMD.setMonoMirror(True)
		
		#FOVEの時のみ
		self.__mFove = None
		if self.__isSteamVr:
			self.__mFove = Fove.Fove()
		
		# 視線トラッキング用球モデル
		self.__ballLRef = vizshape.addPlane([0.00001,0.00001])
		self.__ballLRef.disable(viz.RENDERING)
		self.__ballLRef.disable(viz.LIGHTING)
		self.__ballLRef.disable(viz.DEPTH_WRITE)
		self.__ballLRef.disable(viz.INTERSECTION)
		
		self.__ballL = vizshape.addSphere(0.01)
		self.__ballL.ambient(viz.RED)
		self.__ballL.emissive(viz.RED)
		self.__ballL.color(viz.RED)
		self.__ballL.setPosition(0,0,1.0)
		self.__ballL.setParent(self.__ballLRef)
		self.__ballL.disable(viz.INTERSECTION)
		self.__ballL.renderOnlyToWindows([self.__mWindowMono])
		
	#---------------------------------------------------------
	def __del__(self):
		"""
		デストラクタ
		"""
		self.Finalize()
		
	#---------------------------------------------------------	
	def Finalize(self):
		"""
		終了化
		"""
		self.__mCallBackFunc = None
		
		if self.__mWaitTask:
			self.__mWaitTask.kill()
			self.__mWaitTask.remove()
			self.__mWaitTask = None
			
		if self.__mHMD:
			self.__mHMD.remove()
			self.__mHMD = None
		
		if self.__mCube:
			self.__mCube.remove()
			self.__mCube = None
		
		if self.__mViewLink:
			self.__mViewLink.remove()
			self.__mViewLink = None
			
		if self.__mNavi:
			self.__mNavi.remove()
			self.__mNavi = None
			
		if self.__mFove:
			self.__mFove.Finalize()
			self.__mFove = None
			
		if self.__mView:
			self.__mView.remove()
			self.__mView = None
			
		if self.__mWindow:
			self.__mWindow.remove()
			self.__mWindow = None
			
		if self.__mWindowMono:
			self.__mWindowMono.remove()
			self.__mWindowMono = None
			
		if self.__ballLRef:
			self.__ballLRef.remove()
			self.__ballLRef = None
			
		if self.__ballL:
			self.__ballL.remove()
			self.__ballL = None
			
	#---------------------------------------------------------
	def Update(self):
		"""
		毎フレーム呼び出されるための関数	
		"""
		self.__mKinect.SetJoint()
		
		for i in range(6):
			if self.__mKinect.CheckUserIDTracking(i):
				pos = self.__mKinect.GetUserIDJointPosition(i, 3)
				self.__mCube.setPosition(pos)
				break
				
		euler = self.__mHMD.getSensor().getEuler()
		self.__mCube.setEuler(euler)
		
		# 視線トラッキング用球モデル配置
		if self.__mFove != None:
			lPoint, rPoint = self.__mFove.GetEyePos()
			self.__ballLRef.setPosition(self.__mView.getPosition(mode=viz.ABS_GLOBAL))
			self.__ballLRef.setQuat(self.__mView.getQuat())
			self.__ballL.setPosition(lPoint,mode=viz.ABS_PARENT)
		
	#---------------------------------------------------------
	def SetCallBack(self, callBackFunc):
		"""
		リターン用のコールバック関数の設定
		"""
		self.__mCallBackFunc = callBackFunc
		
	#---------------------------------------------------------
	def SetWindowVisible(self, state):
		"""
		ウィンドウの表示・非表示を設定
		"""
		self.__mVisibleState = state
		if state:
			iniFile = LoadIniFile.LoadIniFileClass()
			iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH + 'RiMMInspector_Settings.ini'))
			
			iniPosTmp = iniFile.GetItemData('View', 'iniPos')
			if iniPosTmp:
				iniPosStr = iniPosTmp.split(',')
				self.__mIniPos = viz.Vector(float(iniPosStr[0]), float(iniPosStr[1]), float(iniPosStr[2]))
			
			iniAngleTmp =  iniFile.GetItemData('View', 'iniAngle')
			if iniAngleTmp:
				self.__mIniAngle = float(iniAngleTmp)
			
			"""
			# 目の高さがあれば追加
			if hasattr(self.__mHMD, "getProfile"):
				profile = self.__mHMD.getProfile()
				self.__mIniPos[1] += profile.eyeHeight
			"""
			
			self.Update()
			
			cubePos = self.__mCube.getPosition(viz.ABS_GLOBAL)
			self.__mIniPos -= cubePos
			
			self.__mNavi.setPosition(self.__mIniPos, viz.ABS_GLOBAL)
			self.__mNavi.setEuler([self.__mIniAngle, 0.0, 0.0], viz.ABS_GLOBAL)
			self.__mViewLink.enable()
			self.__mWindow.visible(True)
			self.__mWindowMono.visible(True)
		else:
			self.__mViewLink.disable()
			self.__mView.setPosition(0.0, 0.0, 10000.0)
			self.__mView.setEuler(0.0, 0.0, 0.0)
			
			self.__mWaitTask = viztask.schedule(self.WaitToDisappearWindow(1))
		
	#---------------------------------------------------------
	def WaitToDisappearWindow(self, frame):
		"""
		一定時間後ウィンドウを消す
		"""
		yield viztask.waitFrame(frame)
		self.__mWindow.visible(False)
		self.__mWindowMono.visible(False)
		
	#---------------------------------------------------------
	def GetVisibleState(self):
		"""
		ウィンドウの可視状態を取得
		"""
		return self.__mVisibleState
		
	#---------------------------------------------------------
	def GetViewPos(self, mode = viz.ABS_PARENT):
		"""
		視点の座標を取得
		"""
		return self.__mView.getPosition(mode)	
		
	#---------------------------------------------------------
	def GetViewState(self):
		"""
		ログ用に情報を取得
		"""
		headPos		= self.__mView.getPosition(mode=viz.ABS_GLOBAL)
		headEuler	= self.__mCube.getEuler()
		lPoint		= [0,0,1.0]
		rPoint		= [0,0,1.0]
		if self.__mFove != None:
			lPoint, rPoint = self.__mFove.GetEyePos()
		viewState	= [headPos, headEuler, lPoint, rPoint]
		return viewState
		