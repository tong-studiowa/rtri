﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object2d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム追加時の向き制御のクラス定義
class SetDirection(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDirection] : Init SetDirection')
		
		self._InputClass = None
		self._StencilClass = None
		self._AddLogClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._IsVisible = True
		self._Direction = 1			#0=上,1=下,2=左,3=右
		
		#フレーム、テキスト追加
		self._FrameCount = 4
		self._FrameList = []
		
		self._TextMessageList = ['↑','↓','←','→']
		self._TextList = []
		
		self._Position = [[0.049,0.827],[0.049,0.747],[0.0245,0.787],[0.0735,0.787]]
		self._Scale = [0.315,0.42]
		self._TextScale = [0.225,0.3]
		
		for x in range(self._FrameCount):
			frame = self.CreateFrame(self._Position[x],self._Scale)
			self._FrameList.append(frame)
			
			text = self.CreateText(self._TextMessageList[x],self._Position[x],self._TextScale)
			self._TextList.append(text)
		
		self.SetVisible(False)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,stencilClass,addLogClass):
		self._InputClass = inputClass
		self._StencilClass = stencilClass
		self._AddLogClass = addLogClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SetDirection] : Start SetDirection Update')
		
		self._ClickState = False
		self._OverState = -1
		
		if self._IsVisible:
			mousePosition = self._InputClass.GetMousePosition()
			
			if mousePosition[1] > 0.806 and mousePosition[1] < 0.846:
				if mousePosition[0] > 0.036 and mousePosition[0] < 0.06:
					self._OverState = 0
					
			if mousePosition[1] > 0.726 and mousePosition[1] < 0.766:
				if mousePosition[0] > 0.036 and mousePosition[0] < 0.06:
					self._OverState = 1
					
			if mousePosition[1] > 0.766 and mousePosition[1] < 0.806:
				if mousePosition[0] > 0.01 and mousePosition[0] < 0.036:
					self._OverState = 2
					
			if mousePosition[1] > 0.766 and mousePosition[1] < 0.806:
				if mousePosition[0] > 0.06 and mousePosition[0] < 0.086:
					self._OverState = 3
					
			if self._InputClass.GetMouseClickState() and self._OverState != -1:
				self._ClickState = True
				self._Direction = self._OverState
				self.SetItemAngle()

			#ロールオーバー
			self.Rollover(self._OverState)
		
	#リセット処理
	def Reset(self):
		self._Direction = 1
		self._ClickState = False
		self._OverState = -1
		self._IsVisible = True
		self.Rollover(self._OverState)
		self.SetItemAngle()
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		for x in range(self._FrameCount):
			frame = self._FrameList[x]
			frame.visible(val)
			
			text = self._TextList[x]
			text.visible(val)
			
	#フレームの作成
	def CreateFrame(self,position,scale):
		frame = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		frame.setPosition(position[0],position[1])
		frame.setScale(scale[0],scale[1])
		frame.color(viz.GRAY)
		
		return frame
		
	#テキストの作成
	def CreateText(self,message,position,scale):
		text = viz.addText(message,parent=viz.SCREEN,align=viz.ALIGN_CENTER_CENTER)
		text.setPosition(position[0],position[1])
		text.setScale(scale[0],scale[1])
		text.color(viz.WHITE)
		text.drawOrder(1)
		
		return text
		
	#向きの状態の取得
	def GetDirectionState(self):
		state = self._Direction
		
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		for x in range(self._FrameCount):
			color = viz.GRAY
			if x == self._Direction:
				color = [0.75,0.75,0.75]
			elif x == state:
				color = [0.575,0.575,0.575]
				
			frame = self._FrameList[x]
			frame.color(color)
			
	#アイテム追加時の角度を設定
	def SetItemAngle(self):
		angle = 0
		logAngle = 0
		if self._Direction == 0:
			angle = 180
			logAngle = 0
		elif self._Direction == 1:
			angle = 0
			logAngle = 180
		elif self._Direction == 2:
			angle = 90
			logAngle = 270
		elif self._Direction == 3:
			angle = 270
			logAngle = 90
			
		self._StencilClass.SetItemAngle(angle)
		self._AddLogClass.SetLogAngle(logAngle)
		