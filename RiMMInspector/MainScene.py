﻿# coding: utf-8
#PathSimulator

import viz
import viztask
import vizinput
import ntpath
import GUI
import Layout
import CameraHandle
import CameraControlClass
import os.path

#モード切り替え関係
mode = False
changeModeState = False
def SetMode(state,layoutData=None,buttonData=None):
	global mode
	global changeModeState
	global playMode
	global ClickList
	
	mode = state
	
	Layout.SetMode(mode)
	
	if mode:	#レイアウターからパスシムへ切り替え
		changeModeState = False
		SetCamera()
		
		#アイテム、間取り、キャラクターの設定
		Layout.StopCollide()
		LoadLayoutDataFromLayouter(layoutData,buttonData)
		
		#キャラクターの歩行を停止
		Layout.Stop()
		playMode=Layout.MODE_STOP
		GUI.HideGUIList([ButtonList[2]])
		GUI.ShowGUIList([ButtonList[1]])
		
	else:		#パスシムからレイアウターへ切り替え
		#表示状態をリセット
		if not Layout.lightFlag:
			Layout.SwitchLight()
		if not Layout.equipmentFlag:
			Layout.SwitchEquipment()
		if not Layout.roomFlag:
			Layout.SwitchBuilding()
		if Layout.collideFlag:
			Layout.SetCollide(ClickList)
		
		#キャラクターの歩行を停止
		Layout.Stop()
		Layout.ResetColorAllModel()
		if avatarCameraNum != -1:
			Layout.SetAvatarModelCamera(-1)
			
		#コリジョン判定のリセット
		Layout.ResetCollideModel()
		
		#データとインターフェイスをリセット
		Layout.ResetData(False)
		HideAllGUI()
		
def GetMode():
	global mode
	return mode
	
GUIList=[]
ButtonList=[]
CheckboxList=[]
CheckboxList2=[]
CheckboxList3=[]
TextList=[]
TextList2=[]
TextBoxList=[]
FilePathList=['','','']
ClickList=[	 False,False,False,False,False,False,False,False,False,False,
			 False,False,False,False,False,False,False,False,False,False]
AvatarList=[ False,False,False,False,False,False,False,False,False,False,
			 False,False,False,False,False,False,False,False,False,False]

menuFlag=False
OnGUIFlag=True
menuMode=None
playMode=Layout.MODE_STOP
avatarCameraNum=-1
drawingModel=None

#カメラの設定
#CameraHandleObject=CameraHandle.MyCameraHandler()
#viz.cam.setHandler(CameraHandleObject)

def SetCamera():
	viz.MainWindow.clip(0.1, 10000)
CameraHandleObject=CameraControlClass.CameraControlClass()
Layout.setCameraObject(CameraHandleObject)
#print viz.MainView.getPosition()

EXE_FOLDER_NAME		= 'RiMMInspector'
EXE_FOLDER_NAME2	= 'Central-uni'

#ルートフォルダの取得
def GetRootPath():
	exeDir = viz.res.getPublishedPath('')
	if os.path.exists(exeDir) == False or exeDir[-len(EXE_FOLDER_NAME2)-1:-1] == EXE_FOLDER_NAME2:
		exeDir = 'C:\\Users\\StudioWa\\Documents\\Vizard\\RIMMroot\\RiMMInspector\\'
	rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
		
	return rootDir
	
rootDir = GetRootPath()

#============
# メイン処理
#============
def Start():
	
	global GUIList
	global ButtonList
	global CheckboxList
	global CheckboxList2
	global CheckboxList3
	global TextList
	global TextList2
	global TextBoxList
	
	#GUIの生成
	GUIList=GUI.CreateGUIs()
	ButtonList=GUI.CreateButtons()
	CheckboxList=GUI.CreateCheckBoxes()
	CheckboxList2=GUI.CreateCheckBoxes2()
	CheckboxList3=GUI.CreateCheckBoxes3()
	TextList=GUI.CreateTexts()
	TextList2=GUI.CreateTexts2()
	TextBoxList=GUI.CreateTextBoxes()
	
	#マップへの非表示設定
	Layout.SetNotRenderToMapWindow(GUIList)
	Layout.SetNotRenderToMapWindow(ButtonList)
	Layout.SetNotRenderToMapWindow(CheckboxList)
	Layout.SetNotRenderToMapWindow(CheckboxList2)
	Layout.SetNotRenderToMapWindow(CheckboxList3)
	Layout.SetNotRenderToMapWindow(TextList)
	Layout.SetNotRenderToMapWindow(TextList2)
	Layout.SetNotRenderToMapWindow(TextBoxList)
	
	#GUIの初期設定
	InitializeGUI()
	
	#ボタン判定のアクティブ化
	'''
	viz.callback(viz.MOUSE_MOVE_EVENT,CheckCursor)
	viz.callback(viz.MOUSEDOWN_EVENT,CheckButton)
	'''

def HideAllGUI():
	global GUIList
	global ButtonList
	global CheckboxList
	global TextList
	global CheckboxList2
	global TextList2
	global CheckboxList3
	global TextBoxList
	global menuFlag
	
	GUI.HideGUIList(GUIList)
	GUI.HideGUIList(ButtonList)
	GUI.HideGUIList(CheckboxList)
	GUI.HideGUIList(TextList)
	GUI.HideGUIList(CheckboxList2)
	GUI.HideGUIList(TextList2)
	GUI.HideGUIList(CheckboxList3)
	GUI.HideGUIList(TextBoxList)
	
	menuFlag=False

def ShowAllGUI():
	global GUIList
	global ButtonList
	global TextList
	global playMode
	
	GUI.ShowGUIList([GUIList[0]])
	GUI.ShowGUIList([ButtonList[0],ButtonList[3],ButtonList[4],
					 ButtonList[5],ButtonList[6],ButtonList[11],ButtonList[13],ButtonList[14]])
	
	if playMode==Layout.MODE_PLAY:
		GUI.ShowGUIList([ButtonList[2]])
	else:
		GUI.ShowGUIList([ButtonList[1]])
	
	GUI.ShowGUIList([TextList[0],TextList[1],TextList[2],TextList[3],TextList[24]])
	
def InitializeGUI(buttonData=None):
#GUIの初期表示
	global CheckboxList
	global CheckboxList2
	global CheckboxList3
	global ClickList
	global avatarCameraNum
	
	HideAllGUI()
	ShowAllGUI()
	
	if Layout.mapClass:
		flag_5=Layout.mapClass.GetVisible()
	else:
		flag_5=False
		
	GUI.SetCheckBoxTexture(CheckboxList[1],True)
	GUI.SetCheckBoxTexture(CheckboxList[2],True)
	GUI.SetCheckBoxTexture(CheckboxList[3],True)
	GUI.SetCheckBoxTexture(CheckboxList[4],True)
	GUI.SetCheckBoxTexture(CheckboxList[5],flag_5)
	GUI.SetCheckBoxTexture(CheckboxList[6],Layout.drewingFlag)
	GUI.SetCheckBoxTexture(CheckboxList[7],Layout.collideFlag)
	GUI.SetCheckBoxTexture(CheckboxList[8],Layout.collideStopFlag)
	for index in range(20):
		GUI.SetCheckBoxTexture(CheckboxList2[index],ClickList[index])
		GUI.SetCheckBoxTexture(CheckboxList3[index],False)
	if avatarCameraNum>-1:
		GUI.SetCheckBoxTexture(CheckboxList3[avatarCameraNum],True)
	
	#レイアウターからの引き継ぎ
	if buttonData != None:
		#グリッド
		GUI.SetCheckBoxTexture(CheckboxList[0],buttonData[0])
		#図面
		if Layout.drewingFlag != buttonData[1]:
			Layout.SwitchDrawing()		
		GUI.SetCheckBoxTexture(CheckboxList[6],Layout.drewingFlag)
		#パス
		Layout.pathVisibleFlag = True
		if Layout.pathVisibleFlag != buttonData[2]:
			Layout.SwitchPath()
		GUI.SetCheckBoxTexture(CheckboxList[2],Layout.pathVisibleFlag)
		
		playMode=Layout.MODE_STOP
		GUI.HideGUIList([ButtonList[2]])
		GUI.ShowGUIList([ButtonList[1]])
		
def Update():
	global mode
	global playMode
	global ButtonList
	global CameraHandleObject
	global OnGUIFlag
	
	while True:
		if mode:
			CameraHandleObject.Update()
			CheckButton()
			
			#衝突判定のポーズ処理判定
			if Layout.CallPause():
				
				if playMode==Layout.MODE_PLAY:
					
					GUI.HideGUIList([ButtonList[2]])
					GUI.ShowGUIList([ButtonList[1]])
					Layout.Pause()
					playMode=Layout.MODE_PAUSE
			
			#メニューの表示切り替え
			if CameraHandleObject.IsDownSpaceKey():
				OnGUIFlag=not(OnGUIFlag)
				if OnGUIFlag:
					InitializeGUI()
				else:
					HideAllGUI()
			
			#メニューの表示切り替え
			if CameraHandleObject.IsDownKeyM():
				visible=Layout.SwitchMap()
				if visible:
					GUI.SetCheckBoxTexture(CheckboxList[5],True)
					GUI.Debug(' Set Visible Map')
				else:
					GUI.SetCheckBoxTexture(CheckboxList[5],False)
					GUI.Debug(' Set Invisible Map')
					
			#天井の表示切り替え
			if Layout.roomClass != None:
				Layout.roomClass.CeilingVisibleUpdate()
			
			#キャラクター視点の更新
			if Layout.pathData != None:
				Layout.pathData.CameraUpdate()
				
			#視点の高さ設定の更新
			for x in range(2):
				textBox = TextBoxList[x]
				state,val = textBox.Update()
				if state:
					CameraHandleObject.SetEyeHeight(x,val)
				
			#if Layout.selectMode == 1:
			if Layout.itemClass != None:
				Layout.itemClass.AnimationUpdate()
			
		yield viztask.waitFrame(1)
UpdateTask=viztask.schedule(Update())

def CheckButton():
	global menuFlag
	global menuMode
	global playMode
	global ButtonList
	global TextList
	global CheckboxList
	global TextList2
	global CheckboxList2
	global CheckboxList3
	global FilePathList
	global ClickList
	global AvatarList
	global avatarCameraNum
	global CameraHandleObject
	global changeModeState
	global TextBoxList
	
	if CameraHandleObject.CheckMouseButtonLeft():
		if changeModeState:
			roomPath	= 'Settings\\MedicalSim\\Room\\.'
			itemPath	= 'Settings\\MedicalSim\\Item\\.'
			logPath		= 'Settings\\MedicalSim\\Log\\.'
			filterData	= [('INI Files','*.ini')]
			
			#CameraHandleObject.MouseDownEvent()
			
			#ﾏｳｽｶｰｿﾙの接触判定
			pick_obj = viz.pick( pos=viz.mouse.getPosition(), mode=viz.SCREEN )
			
			#0-巻き戻し
			if pick_obj==ButtonList[0].GetModel():
				Layout.Stop()
				playMode=Layout.MODE_STOP
				GUI.HideGUIList([ButtonList[2]])
				GUI.ShowGUIList([ButtonList[1]])
			
			if playMode!=Layout.MODE_PLAY:
				#1-再生
				if pick_obj==ButtonList[1].GetModel():
					GUI.HideGUIList([ButtonList[1]])
					GUI.ShowGUIList([ButtonList[2]])
					Layout.Play(ClickList)
					playMode=Layout.MODE_PLAY
			else:
				#2-停止
				if pick_obj==ButtonList[2].GetModel():
					GUI.HideGUIList([ButtonList[2]])
					GUI.ShowGUIList([ButtonList[1]])
					Layout.Pause()
					playMode=Layout.MODE_PAUSE
			
			#3-終わり出し
			if pick_obj==ButtonList[3].GetModel():
				playMode=Layout.MODE_STOP
				Layout.SetFinPos()
			
			#4-読込
			if pick_obj==ButtonList[4].GetModel():
				if menuFlag==True and menuMode==GUI.LOADING_DIALOG:
					menuFlag=False
					SetLoadingDialog(menuFlag)
				else:
					SetShowHideDialog(False)
					SetCollideDialog(False)
					SetAvatarListDialog(False)
					SetCameraDialog(False)
					menuFlag=True
					menuMode=GUI.LOADING_DIALOG
					SetLoadingDialog(menuFlag)
			
			#5-表示/非表示
			if pick_obj==ButtonList[5].GetModel():
				if menuFlag==True and menuMode==GUI.VISIBLE_DIALOG:
					menuFlag=False
					SetShowHideDialog(menuFlag)
				else:
					SetLoadingDialog(False)
					SetCollideDialog(False)
					SetAvatarListDialog(False)
					SetCameraDialog(False)
					menuFlag=True
					menuMode=GUI.VISIBLE_DIALOG
					SetShowHideDialog(menuFlag)
			
			#6-衝突判定
			if pick_obj==ButtonList[6].GetModel():
				if menuFlag==True and menuMode==GUI.COLLIDE_DIALOG:
					menuFlag=False
					SetCollideDialog(menuFlag)
				else:
					SetLoadingDialog(False)
					SetShowHideDialog(False)
					SetAvatarListDialog(False)
					SetCameraDialog(False)
					menuFlag=True
					menuMode=GUI.COLLIDE_DIALOG
					SetCollideDialog(menuFlag)
			
			#11-アバターダイアログ
			if pick_obj==ButtonList[11].GetModel():
				if menuFlag==True and menuMode==GUI.AVATARLIST_DIALOG:
					menuFlag=False
					SetAvatarListDialog(menuFlag)
				else:
					SetLoadingDialog(False)
					SetShowHideDialog(False)
					SetCollideDialog(False)
					SetCameraDialog(False)
					menuFlag=True
					menuMode=GUI.AVATARLIST_DIALOG
					SetAvatarListDialog(menuFlag)
			
			#14-視点
			if pick_obj==ButtonList[14].GetModel():
				if menuFlag==True and menuMode==GUI.COLLIDE_DIALOG:
					menuFlag=False
					SetCameraDialog(menuFlag)
				else:
					SetLoadingDialog(False)
					SetShowHideDialog(False)
					SetCollideDialog(False)
					SetAvatarListDialog(False)
					menuFlag=True
					menuMode=GUI.COLLIDE_DIALOG
					SetCameraDialog(menuFlag)
			
			#ダイアログ表示時
			if menuFlag==True:
				
				if menuMode==GUI.LOADING_DIALOG:
					#exeDir = viz.res.getPublishedPath('')
					#rootDir = exeDir[:-len('PathSimulator')-1]
					
					#参照ボタン（間取り）
					if pick_obj==ButtonList[7].GetModel():
						directoryPath=rootDir+roomPath
						GUI.Debug(  directoryPath )
						filepath=vizinput.fileOpen(file=directoryPath,filter=filterData)
						FilePathList[0]=filepath
						TextList[11].SetText(ntpath.basename(filepath))
					#参照ボタン（アイテム）
					if pick_obj==ButtonList[8].GetModel():
						directoryPath=rootDir+itemPath
						GUI.Debug(  directoryPath )
						filepath=vizinput.fileOpen(file=directoryPath,filter=filterData)
						FilePathList[1]=filepath
						TextList[12].SetText(ntpath.basename(filepath))
					#参照ボタン（ログ）
					if pick_obj==ButtonList[9].GetModel():
						directoryPath=rootDir+logPath
						GUI.Debug(  directoryPath )
						filepath=vizinput.fileOpen(file=directoryPath,filter=filterData)
						FilePathList[2]=filepath
						TextList[13].SetText(ntpath.basename(filepath))
					#更新ボタン
					if pick_obj==ButtonList[10].GetModel():
						Layout.StopCollide()
						viztask.schedule(LoadLayoutData(FilePathList))
						#InitializeGUI()
						#menuFlag=False
				
				if menuMode==GUI.VISIBLE_DIALOG:
					#0-床グリッド
					if pick_obj==CheckboxList[0].GetModel():
						visible=Layout.SwitchGrid()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[0],True)
							GUI.Debug(' Set Visible Grid Model')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[0],False)
							GUI.Debug(' Set Invisible Grid Model')
					#1-照明の位置
					if pick_obj==CheckboxList[1].GetModel():
						visible=Layout.SwitchLight()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[1],True)
							GUI.Debug(' Set Visible Light Model')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[1],False)
							GUI.Debug(' Set Invisible Light Model')
					#2-パス
					if pick_obj==CheckboxList[2].GetModel():
						visible=Layout.SwitchPath()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[2],True)
							GUI.Debug(' Set Visible Path Model')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[2],False)
							GUI.Debug(' Set Invisible Path Model')
					#3-機械
					if pick_obj==CheckboxList[3].GetModel():
						visible=Layout.SwitchEquipment()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[3],True)
							GUI.Debug(' Set Visible Equipment Model')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[3],False)
							GUI.Debug(' Set Invisible Equipment Model')
					#4-建物
					if pick_obj==CheckboxList[4].GetModel():
						visible=Layout.SwitchBuilding()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[4],True)
							GUI.Debug(' Set Visible Building Model')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[4],False)
							GUI.Debug(' Set Invisible Building Model')
					#5-マップ
					if pick_obj==CheckboxList[5].GetModel():
						visible=Layout.SwitchMap()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[5],True)
							GUI.Debug(' Set Visible Map')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[5],False)
							GUI.Debug(' Set Invisible Map')
					#6-建物
					if pick_obj==CheckboxList[6].GetModel():
						visible=Layout.SwitchDrawing()
						if visible:
							GUI.SetCheckBoxTexture(CheckboxList[6],True)
							GUI.Debug(' Set Visible Drawing')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[6],False)
							GUI.Debug(' Set Invisible Drawing')
				
				if menuMode==GUI.COLLIDE_DIALOG:
					#7-衝突判定
					if pick_obj==CheckboxList[7].GetModel():
						collide,collideStopFlag=Layout.SetCollide(ClickList)
						if collide:
							GUI.SetCheckBoxTexture(CheckboxList[7],True)
							GUI.Debug(' Active Collide')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[7],False)
							GUI.Debug(' Deactive Collide')
							if collideStopFlag:
								Layout.SetCollideStop()
								GUI.SetCheckBoxTexture(CheckboxList[8],False)
								GUI.Debug(' Deactive Collide Stop')
					#8-衝突判定時の一時停止
					if pick_obj==CheckboxList[8].GetModel():
						collide,collideFlag=Layout.SetCollideStop()
						if collide:
							GUI.SetCheckBoxTexture(CheckboxList[8],True)
							GUI.Debug(' Active Collide Stop')
							if collideFlag == False:
								Layout.SetCollide(ClickList)
								GUI.SetCheckBoxTexture(CheckboxList[7],True)
								GUI.Debug(' Active Collide')
						else:
							GUI.SetCheckBoxTexture(CheckboxList[8],False)
							GUI.Debug(' Deactive Collide Stop')
				
				#アバターのリスト表示
				if menuMode==GUI.AVATARLIST_DIALOG:
					
					#視点の高さダイアログ
					if pick_obj==ButtonList[12].GetModel():
						while True:
							height=vizinput.input('視点の高さ[m]を入力してください')
							try:
								Layout.SetAvatarEyeHeight(float(height))
								break
							except:
								vizinput.message('入力エラー')
								break
					
					#パスの表示
					for index,checkbox in enumerate(CheckboxList2):
						if pick_obj==checkbox.GetModel():
							ClickList[index]=not(ClickList[index])
							if ClickList[index]:
								GUI.SetCheckBoxTexture(CheckboxList2[index],True)
								Layout.SetVisibleAvatarModelAndPath(index,True)
								GUI.Debug(' Set Active Path of Avatar'+str(index+1))
							else:
								GUI.SetCheckBoxTexture(CheckboxList2[index],False)
								GUI.SetCheckBoxTexture(CheckboxList3[index],False)
								#第三者視点に切り替え
								if index==avatarCameraNum:
									avatarCameraNum=-1
									Layout.SetAvatarModelCamera(avatarCameraNum)
								Layout.SetVisibleAvatarModelAndPath(index,False)
								GUI.Debug(' Set Deactive Path of Avatar'+str(index+1))
					#第三者視点
					checkFlag=False
					NextAvatarCameraNum=avatarCameraNum
					for index,checkbox in enumerate(CheckboxList3):
						#チェックボックスがクリックされた
						if pick_obj==checkbox.GetModel() and ClickList[index]:
							checkFlag=True
							#第三者視点に切り替え
							if index==avatarCameraNum:
								NextAvatarCameraNum=-1
								GUI.SetCheckBoxTexture(CheckboxList3[index],False)
							#アバター視点に切り替え
							else:
								if avatarCameraNum!=-1:
									GUI.SetCheckBoxTexture(CheckboxList3[avatarCameraNum],False)
								NextAvatarCameraNum=index
								GUI.SetCheckBoxTexture(CheckboxList3[index],True)
					
					#視点の変更処理
					if checkFlag:
						if NextAvatarCameraNum>-1:
							Layout.SwitchAvatarVisible(NextAvatarCameraNum,False)	#アバター非表示
						if avatarCameraNum>-1:
							Layout.SwitchAvatarVisible(avatarCameraNum,True)	#アバター表示
						avatarCameraNum=NextAvatarCameraNum
						Layout.SetAvatarModelCamera(avatarCameraNum)
						if avatarCameraNum>-1:
							CameraHandleObject.SetActiveInput(False)
						else:
							CameraHandleObject.SetActiveInput(True)
						GUI.Debug(' Set Camera View (Number='+str(index+1)+')')
						
			#モード切り替え
			if pick_obj==ButtonList[13].GetModel():
				SetMode(0)
				
		if changeModeState == False:
			changeModeState = True
			
def CheckCursor(e):
	return

def LoadLayoutData(FilePathList):
	global ClickList
	global AvatarList
	global menuFlag
	global avatarCameraNum
	
	while Layout.threadAliveFlag:
		yield viztask.waitFrame(1)
	
	Layout.SaveRemind()
	
	avatarCameraNum=-1
	CameraHandleObject.SetActiveInput(True)
	Layout.LoadDataInPathSim(FilePathList)
	
	ClickList=Layout.GetAvatarCheckBoxList()
	AvatarList=Layout.GetAvatarCheckBoxList()
	
	InitializeGUI()
	menuFlag=False

def LoadLayoutDataFromLayouter(layoutData,buttonData):
	global ClickList
	global AvatarList
	global menuFlag
	global avatarCameraNum
	
	#while Layout.threadAliveFlag:
	#	yield viztask.waitFrame(1)
	
	avatarCameraNum=-1
	CameraHandleObject.SetActiveInput(True)
	Layout.LoadDataFromLayouter(layoutData)
	
	ClickList=Layout.GetAvatarCheckBoxList()
	AvatarList=Layout.GetAvatarCheckBoxList()
	
	InitializeGUI(buttonData)
	menuFlag=False

def SetLoadingDialog(visible):
	global GUIList
	global ButtonList
	global TextList
	#読込ダイアログの表示
	if visible==True:
		GUI.ShowGUIList([GUIList[1],
						 ButtonList[7],ButtonList[8],ButtonList[9],ButtonList[10],
						 TextList[4],TextList[5],TextList[6],TextList[7],TextList[8],
						 TextList[9],TextList[10],TextList[11],TextList[12],TextList[13]])
		GUI.Debug(' Show Loading Dialog')
	elif visible==False:
		GUI.HideGUIList([GUIList[1],
						 ButtonList[7],ButtonList[8],ButtonList[9],ButtonList[10],
						 TextList[4],TextList[5],TextList[6],TextList[7],TextList[8],
						 TextList[9],TextList[10],TextList[11],TextList[12],TextList[13]])
		GUI.Debug(' Hide Loading Dialog')
	else:
		GUI.Debug(' -')

def SetShowHideDialog(visible):
	global GUIList
	global CheckboxList
	global TextList
	if visible:
		GUI.ShowGUIList([GUIList[2],
						 CheckboxList[0],CheckboxList[1],CheckboxList[2],
						 CheckboxList[3],CheckboxList[4],CheckboxList[5],CheckboxList[6],
						 TextList[14],TextList[15],TextList[16],
						 TextList[17],TextList[18],TextList[19],TextList[20]])
		GUI.Debug(' Show ShowHide Dialog')
	else:
		GUI.HideGUIList([GUIList[2],
						 CheckboxList[0],CheckboxList[1],CheckboxList[2],
						 CheckboxList[3],CheckboxList[4],CheckboxList[5],CheckboxList[6],
						 TextList[14],TextList[15],TextList[16],
						 TextList[17],TextList[18],TextList[19],TextList[20]])
		GUI.Debug(' Hide ShowHide Dialog')

def SetCollideDialog(visible):
	global GUIList
	global CheckboxList
	global TextList
	if visible:
		GUI.ShowGUIList([GUIList[3],CheckboxList[7],CheckboxList[8],
						 TextList[21],TextList[22]])
		GUI.Debug(' Show Collide Dialog')
	else:
		GUI.HideGUIList([GUIList[3],CheckboxList[7],CheckboxList[8],
						 TextList[21],TextList[22]])
		GUI.Debug(' Hide Collide Dialog')

def SetAvatarListDialog(visible):
	global GUIList
	global ButtonList
	global CheckboxList2
	global TextList
	global TextList2
	global AvatarList
	if visible:
		GUI.ShowGUIList([GUIList[4],ButtonList[12],TextList[23]])
		GUI.HideGUIList(CheckboxList2)
		GUI.HideGUIList(TextList2)
		for index,flag in enumerate(AvatarList):
			if flag:
				GUI.ShowGUIList([CheckboxList2[index]])
				GUI.ShowGUIList([CheckboxList3[index]])
				GUI.ShowGUIList([TextList2[index]])
		GUI.Debug(' Show AvatarList Dialog')
	else:
		GUI.HideGUIList([GUIList[4],ButtonList[12],TextList[23]])
		GUI.HideGUIList(CheckboxList2)
		GUI.HideGUIList(CheckboxList3)
		GUI.HideGUIList(TextList2)
		GUI.Debug(' Hide AvatarList Dialog')

def SetCameraDialog(visible):
	global GUIList
	global CheckboxList
	global TextList
	if visible:
		GUI.ShowGUIList([GUIList[5],TextList[25],TextList[26],TextBoxList[0],TextBoxList[1]])
		for x in range(2):
			TextBoxList[x].SetValue(CameraHandleObject.GetEyeHeight(x))
		GUI.Debug(' Show Camera Dialog')
	else:
		GUI.HideGUIList([GUIList[5],TextList[25],TextList[26],TextBoxList[0],TextBoxList[1]])
		GUI.Debug(' Hide Camera Dialog')

#GUIのリアルタイム位置調整
preSize=viz.window.getSize()
def ControlGUI(e):
	
	global preSize
	global GUIList
	global ButtonList
	global CheckboxList
	global CheckboxList2
	global CheckboxList3
	global TextList
	global TextList2
	global TextBoxList
	
	windowSize=viz.window.getSize()
	
	if windowSize[0]!=0 and windowSize[1]!=0:	#最小化の時は何もしない
		
		per = [ float(preSize[0])/windowSize[0],
				float(preSize[1])/windowSize[1] ]
		
		for data in GUIList:
			data.SetRatio(per)
		
		for data in ButtonList:
			data.SetRatio(per)
		
		for data in CheckboxList:
			data.SetRatio(per)
		
		for data in CheckboxList2:
			data.SetRatio(per)
		
		for data in CheckboxList3:
			data.SetRatio(per)
		
		for data in TextList:
			data.SetRatio(per)
		
		for data in TextList2:
			data.SetRatio(per)
		
		for data in TextBoxList:
			data.SetRatio(per)
			
		Layout.mapClass.SetRatio(per)
		
ControlGUI(preSize)
viz.callback(viz.WINDOW_SIZE_EVENT,ControlGUI)

"""
#ライト設定	#不要
import vizfx
def SetLight():
	
	Light = viz.addLight()
	Light.position(0,1,0)
	Light.direction(0,-1,0)
	Light.intensity(1)
	Light.setPosition([0,1000,0])
	
	viz.MainWindow.getView().getHeadLight().disable()
	viz.MainView.getHeadLight().disable()
	
	skyLight=vizfx.lighting.addDirectionalLight(euler=(40,15,0))
	skyLight.color([0.6,0.6,0.6])
	skyLight.ambient([0.1]*3)
	viz.setOption('viz.lightModel.ambient',[0]*3)
	
	skyLight2=vizfx.lighting.addDirectionalLight(euler=(240,30,0))
	skyLight2.color([0.4,0.4,0.4])
	
SetLight()		#一旦外す
"""

#表示状態を取得
def GetButtonState():
	gridState		= Layout.grid.GetGridVisible()
	drawingState	= Layout.drewingFlag
	pathState		= Layout.pathVisibleFlag
	
	return [gridState,drawingState,pathState]
	
def GetEyeHeightList():
	list = []
	for x in range(2):
		list.append(CameraHandleObject.GetEyeHeight(x))
	return list
		