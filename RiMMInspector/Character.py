﻿
import viz
import vizshape
#import vizfx

import Object3d
import LoadIniFile

from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

rotateSnapTemp = iniFile.GetItemData('Setting','RotateStep')
rotateSnap = int(rotateSnapTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#キャラクター制御のクラス定義
class Character(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,modelControlClass):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Character] : Init Character')
		
		self._ModelControlClass = modelControlClass
		
		self._Model = None
		self._FileName = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		self._Animation = 0
		
		self._Id = None
		
		self._RotateSnap = rotateSnap
		
		self._HighlightState = False
		
		self._IsVisible = True
		
		self._ViewCamera = None
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Character] : Start Character Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = None
		if path.exists(fileName) == True:		#絶対パスかどうか
			filePath = fileName
		else:
			filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
			
		model = self._ModelControlClass.AddCharacter(filePath)
		
		self._Model = model
		self._Model.color([1,1,1])
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		
		self.SetPickable(False)
		
		self.SetAnimation(1)
		
		self._FileName = fileName
		
		#カメラ追加
		self._ViewCamera = viz.addView()
		self._ViewCamera.getHeadLight().disable()
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		if self._ViewCamera:
			self._ViewCamera.remove()
				
		self._Model = None
		self._ViewCamera = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			position = iPosition
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = position[1] + self._PositionOffset[1]
			position[2] = position[2] + self._PositionOffset[2]
			
			self._Model.setPosition(position)
			self._Position = position
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
			
			#スナップ
			snapOrientation = 0
			count = 360 / self._RotateSnap
			for x in range(count):
				angle = self._RotateSnap * x + self._RotateSnap / 2
				if self._Orientation < angle:
					snapOrientation = self._RotateSnap * x
					break
			
			self._Model.setEuler(snapOrientation)
			
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self):
		snapOrientation = 0
		count = 360 / self._RotateSnap
		for x in range(count):
			angle = self._RotateSnap * x + self._RotateSnap / 2
			if self._Orientation < angle:
				snapOrientation = self._RotateSnap * x
				break
		
		self._Orientation = snapOrientation
		
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#角度設定(INIファイル用)
	def SetAxisAngle(self,axisAngle):
		if self._Model != None and axisAngle != None:
			self._Model.setAxisAngle(axisAngle)
			
			val = 0
			yVal = axisAngle[1]
			angleVal = axisAngle[3]
			if yVal < -0.5:
				val = angleVal * -1
			elif yVal > 0.5:
				val = angleVal
			if val < 0:
				val += 360
			self._Orientation = val
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self):
		axisAngle = self._Model.getAxisAngle(viz.ABS_GLOBAL)
		return axisAngle
	
	#ハイライト表示
	def SetHighlight(self,state):
		self._HighlightState = state
		
		if state == True:
			self._Model.enable(viz.LIGHTING)
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.specular(viz.RED)
		else:
			self._Model.disable(viz.LIGHTING)
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.specular(self._Specular)
		
	#ハイライト状態の取得
	def GetHighlightState(self):
		state = self._HighlightState
		return state
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
		
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		return model
		
	#アニメーションを設定
	def SetAnimation(self,animation):
		if self._Model != None:
			self._Animation = animation
			self._Model.state(self._Animation)
		
	#アニメーションを取得
	def GetAnimation(self):
		animation = self._Animation
		return animation
		
	#ファイル名を取得
	def GetFileName(self):
		fileName = self._FileName
		return fileName
		
	#表示状態設定
	def SetVisible(self,state):
		if state == True:
			self._IsVisible = True
		else:
			self._IsVisible = False
			
		self._Model.visible(self._IsVisible)
			
	#表示状態取得
	def GetVisible(self):
		return self._IsVisible
			
	#カメラ取得
	def GetViewCamera(self):
		return self._ViewCamera
		