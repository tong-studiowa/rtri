﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
#import vizfx

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'
LOCAL_SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(LOCAL_SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#キャラクター追加時のドラッグ用モデル制御のクラス定義
class AddCharacter(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddModel] : Init AddCharacter')
		
		self._ModelControlClass = None
		
		self._Model = None
		self._Picking = False
		
		self._TranslateSnap = translateSnap
		
	#使用するクラスの追加
	def AddClass(self,modelControlClass):
		self._ModelControlClass = modelControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][AddCharacter] : Start AddCharacter Update')
		pass
	
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = self._ModelControlClass.AddCharacter(filePath)
		model.state(1)
		
		self._Model = model
		self.SetPickable(False)
		self.SetHighlight(True)
		
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			position = iPosition
			
			#スナップ
			position[0] = self.Snap(position[0])
			position[2] = self.Snap(position[2])
			
			self._Model.setPosition(position)
			
			#高さを床面に揃える
			box = self._Model.getBoundingBox()
			ymin = box.getYmin()
			position[1] = position[1] - ymin + 0.01
			
			self._Model.setPosition(position)
			
	#位置取得
	def GetPosition(self):
		position = [0,0,0]
		
		if self._Model != None:
			position = self._Model.getPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
		
	#選択可能かの設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res
