﻿# GridControlClass

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath('Settings\\RiMMInspector_Settings.ini'))

gridStep = 1.0
gridStepTemp = iniFile.GetItemData('Setting','GridStep')
if gridStepTemp != None:
	gridStep = float(gridStepTemp)

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グリッド制御のクラス定義
class GridControlClass():
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][GridControlClass] : Init GridControlClass')
		
		self._GridSize = [50,50]	#グリッド全体のサイズ（50mx50m）
		self._GridStep = gridStep	#グリッドの間隔
		
		#グリッド作成
		self._Grid = vizshape.addGrid(self._GridSize,self._GridStep*10)
		
		self._Grid.disable(viz.PICKING)
		self._Grid.disable(viz.LIGHTING)
		self._Grid.color(viz.BLACK)
		self._Grid.alpha(0.9)
		
		self._Grid.disable(viz.DEPTH_WRITE)
		self._Grid.drawOrder(-30)
		self._Grid.setPosition([0,0.005,0])
		
		self._Grid2 = vizshape.addGrid(self._GridSize,self._GridStep)
		
		self._Grid2.disable(viz.PICKING)
		self._Grid2.disable(viz.LIGHTING)
		self._Grid2.color(viz.BLACK)
		self._Grid2.alpha(0.4)
		
		self._Grid.disable(viz.DEPTH_WRITE)
		self._Grid2.drawOrder(-29)
		
		self._Grid2.setParent(self._Grid)
		
		self.SetVisible(True)	#デフォルトは表示
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][GridControlClass] : Start GridControlClass Update')
		pass
		
	#表示状態設定
	def SetVisible(self,state):
		if state:
			self._Grid.visible(viz.ON)
		else:
			self._Grid.visible(viz.OFF)
		
	#表示状態の取得
	def GetVisible(self):
		state = self._Grid.getVisible()
		return state
		
	#グリッドモデルを取得
	def GetModel(self):
		model = self._Grid
		return model
		