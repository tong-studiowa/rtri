﻿# coding: utf-8

#========================
#ソフトライセンス、ドングル判定 2016/7/4
#========================
#import Security
#Security.SecurityCheck()
#========================

import viz
import viztask
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

#ローダーの取り込み
import Loader

# - - - - - - - - - - - - - - - - - - - - - - - - -
#基本設定
viz.setOption('viz.text.font','meiryo.ttc')				#日本語フォント設定(メイリオ)
viz.setOption('viz.window.icon','RiMMInspector.ico')	#アイコン設定

viz.setOption('viz.publish.load_message','Loading RiMMInspector Ver.1.0.0')		#ソフトのバージョン番号を記載

#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

dispSize = iniFile.GetItemData('Setting','DispSize')
dispSizeX = int(dispSize.split(',')[0])
dispSizeY = int(dispSize.split(',')[1])

ChangeWindowSetting = 0
#ChangeWindowSettingTemp = iniFile.GetItemData('Setting','ChangeWindowSetting')
#ChangeWindowSetting = int(ChangeWindowSettingTemp)
#print 'ChangeWindowSetting',ChangeWindowSetting

#レンダリング設定
viz.window.setSize(dispSizeX,dispSizeY)

#レンダリング処理の開始
viz.setMultiSample(8)
viz.setOption('viz.max_frame_rate','60')
viz.go()

viz.window.setSize(1024,768)

import CallbackContorl
import MainScene

viz.window.setSize(dispSizeX,dispSizeY)

import Scene

viz.clip(0.1, 3000)

#import SSAO	#SSAOの設定

mode = 0
preMode = 0

import WindowClose
close = WindowClose.WindowClose()	#終了キャンセルの処理

#ディスプレイの初期設定
import Display
displaySettingsPathSim = Display.GetDisplay('Settings\\RiMMInspector_Settings.ini')

displaySettings = [[[dispSizeX,dispSizeY],[0.0],1,0]] 	#0=レイアウター
displaySettings.append(displaySettingsPathSim)			#1=パスシミュレータ
#print 'displaySettings',displaySettings

#メインループ
def mainLoop():
	OutputDebugString("[STUDIOWA][INFO][RiMMInspector][RiMMInspector] : Start mainLoop")

	global mode
	global preMode
	global Display
	global displaySettings
	global ChangeWindowSetting
	
	while True:
		#OutputDebugString("[STUDIOWA][INFO][RiMMInspector][RiMMInspector] : Call Scene Update")
		
		#シーンの更新
		Display.CheckFullScreen()
		sceneClass.Update()
		
		#シーン切り替え関係
		mode1 = sceneClass.GetMode()
		
		if mode1 == 1 and preMode == 0:			#レイアウターからパスシミュレータへ切り替え
			print 'ChangeToPathSimMode'
			
			mode = 1
			layoutData = sceneClass.GetLayoutData()
			buttonState = sceneClass.GetButtonState()
			
			MainScene.SetMode(mode,layoutData,buttonState)
			
			#画面サイズを変更
			if ChangeWindowSetting:
				displaySettingsTemp = Display.SetDisplay(displaySettings,1)
				
		mode2 = 1
		if mode == 1:
			mode2 = MainScene.GetMode()
			
		if mode2 == 0 and preMode == 1:			#パスシミュレータからレイアウターへ切り替え
			print 'ChangeToLayouterMode'
			
			mode = 0
			viewPos = MainScene.CameraHandleObject.GetViewPos()
			buttonState = MainScene.GetButtonState()
			eyeHeightList = MainScene.GetEyeHeightList()
			viz.clip(0.1, 3000)
			
			#画面サイズを変更
			if ChangeWindowSetting:
				displaySettingsTemp = Display.SetDisplay(displaySettings,0)
				
			sceneClass.SetMode(mode,viewPos,buttonState,eyeHeightList)
			
		preMode = mode
		
		yield viztask.waitFrame(1)

#アプリケーションエントリ
if __name__ == '__main__':
	#シーンを生成
	callbackContorlClass = CallbackContorl.CallbackContorl()
	sceneClass = Scene.Scene()
	callbackContorlClass.SetTargetClass(sceneClass.GetInput(),MainScene.CameraHandleObject)
	
	MainScene.Layout.SetGrid(sceneClass.GetGrid())
	MainScene.Layout.SetBg(sceneClass.GetBg())
	
	#パスシミュレータのシーンを開始
	MainScene.Start()
	MainScene.HideAllGUI()
	
	#レイアウターのシーンを開始
	viztask.schedule(mainLoop)
	