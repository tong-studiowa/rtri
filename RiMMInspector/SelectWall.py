﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁選択制御のクラス定義
class SelectWall(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectWall] : Init SelectWall')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._StencilClass = None
		self._ManipulatorClass = None
		self._SelectModelClass = None
		self._CheckStateClass = None
		
		self._SelectedRoom = None
		self._SelectedModel = None
		self._SelectedModelState = 0
		
		self._SelectedModelList = []
		
		self._PreSelectedRoom = None
		self._PreSelectedRoomList = []
		
		self._ClickState = False
		self._Is3d = False
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,stencilClass,manipulatorClass,selectModelClass,checkStateClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._StencilClass = stencilClass
		self._ManipulatorClass = manipulatorClass
		self._SelectModelClass = selectModelClass
		self._CheckStateClass = checkStateClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][SelectWall] : Start SelectWall Update')
		
		self._ClickState = False
		
		roomNum = self._SelectedRoom
		modelNum = self._SelectedModel
		modelState = self._SelectedModelState
		
		modelList = self._SelectedModelList
		
		#シングルクリックによる選択（重なっている場合は全て選択、直前に間取りを選択していたらその間取りの壁のみ）
		if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() == False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True and self._Is3d == False:
			
			#部屋を選択していたら、その部屋限定で壁を選択
			#roomNum,modelNum,modelState = self.Select(True,onlyRoom=self._PreSelectedRoom)
			
			if len(self._PreSelectedRoomList) == 1:
				roomNum,modelNum,modelState = self.Select(True,onlyRoom=self._PreSelectedRoom)
			
			else:
				roomNum,modelNum,modelState = self.Select(True,onlyRoom=None)
					
			if modelNum == None or roomNum == None:
				if self._PreSelectedRoom != None:
					self._PreSelectedRoom = None
					
					#全ての部屋で壁を選択
					roomNum,modelNum,modelState = self.Select(True,onlyRoom=None)
					
			if modelNum != None and roomNum != None:
				#間取りを選択していなかった場合
				if self._PreSelectedRoom == None:
					modelList = self.Select2(True,roomNum,modelState)
				
				#複数の間取りを選択していた場合
				elif len(self._PreSelectedRoomList) > 1:
					modelList = self.Select2(True,roomNum,modelState)
					newModelList = []
					for room in self._PreSelectedRoomList:
						for data in modelList:
							if room == data[0]:
								newModelList.append(data)
					modelList = newModelList
					
					if len(modelList) == 0:
						#全ての部屋で壁を選択
						roomNum,modelNum,modelState = self.Select(True,onlyRoom=None)
						if modelNum != None and roomNum != None:
							modelList = self.Select2(True,roomNum,modelState)
						
					else:
						state = False
						for room in self._PreSelectedRoomList:
							if room == roomNum:
								state = True
								
						if state == False:
							roomState = False
							for data in modelList:
								room = self._RoomControlClass.GetRoom(data[0])
								opRoomState = room.GetOpRoomState()
								if opRoomState == False:
									roomNum = data[0]
									modelNum = data[1]
									roomState = True
							
							if roomState == False:
								data = modelList[0]
								roomNum = data[0]
								modelNum = data[1]
							
							newModelList = []
							for x in range(len(modelList)):
								data = modelList[x]
								if roomNum == data[0]:
									continue
								newModelList.append(data)
							modelList = newModelList
							
			else:
				modelList = []
				
			#マニピュレータを操作中
			if self._ManipulatorClass.GetState():
				pass
				
			#モデルをクリックし、選択
			elif modelNum != None:
				
				#選択中のモデルをクリックしたか確認
				selectState = False
				if modelNum != None and modelNum == self._SelectedModel:
					if roomNum == self._SelectedRoom:
						selectState = True
						
				if selectState == False:
					self._ClickState = True
					
					#選択していたモデルのハイライト表示をOFF
					if self._SelectedModel != None:
						if self._SelectedModelState == 1:
							self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,False)
						elif self._SelectedModelState == 2:
							self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,False)
					
					if self._SelectedModelList != []:
						for model in self._SelectedModelList:
							if self._SelectedModelState == 1:
								self._RoomControlClass.SetHighlightWallLine(model[0],model[1],False)
							elif self._SelectedModelState == 2:
								self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],False)
						
					#選択し、モデルをハイライト表示
					self._SelectedRoom = roomNum
					self._SelectedModel = modelNum
					self._SelectedModelState = modelState
					
					self._ManipulatorClass.SetCircle2Visible(0)
					
					if self._SelectedModelState == 1:
						self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,True)
						
						wallLine = self._RoomControlClass.GetWallLineModel(self._SelectedRoom,self._SelectedModel)
						self._ManipulatorClass.SetModel(wallLine,True)	#マニピュレータ設定
					
					elif self._SelectedModelState == 2:
						self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,True)
						
						wallPoint = self._RoomControlClass.GetWallPointModel(self._SelectedRoom,self._SelectedModel)
						self._ManipulatorClass.SetModel(wallPoint,False)	#マニピュレータ設定
					
					#2つ目の壁
					if modelList != []:
						self._SelectedModelList = modelList
						for model in self._SelectedModelList:
							if self._SelectedModelState == 1:
								self._RoomControlClass.SetHighlightWallLine(model[0],model[1],True)
							elif self._SelectedModelState == 2:
								self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],True)
					
					else:
						self._SelectedModelList = []
					
			#選択中にモデル以外をクリック
			else:
				if self._SelectedModel != None:
					self._ClickState = True
					
					selectedModel = None
					
					#選択を解除し、モデルのハイライト表示をOFF
					if self._SelectedModelState == 1:
						self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,False)
						selectedModel = self._RoomControlClass.GetWallLineModel(self._SelectedRoom,self._SelectedModel)
					elif self._SelectedModelState == 2:
						self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,False)
						selectedModel = self._RoomControlClass.GetWallPointModel(self._SelectedRoom,self._SelectedModel)
					
					if selectedModel == self._ManipulatorClass.GetModel():
						self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
					self._SelectedModel = None
					
					#2つ目の壁
					if self._SelectedModelList != []:
						for model in self._SelectedModelList:
							if self._SelectedModelState == 1:
								self._RoomControlClass.SetHighlightWallLine(model[0],model[1],False)
							elif self._SelectedModelState == 2:
								self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],False)
							
					self._SelectedModelList = []
					
		if self._StencilClass.GetClickState():
			if self._SelectedModel != None:
				
				#選択を解除し、モデルのハイライト表示をOFF
				if self._SelectedModelState == 1:
					self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,False)
				elif self._SelectedModelState == 2:
					self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,False)
				self._SelectedModel = None
				self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
			
			#2つ目の壁
			if self._SelectedModelList != []:
				for model in self._SelectedModelList:
					if self._SelectedModelState == 1:
						self._RoomControlClass.SetHighlightWallLine(model[0],model[1],False)
					elif self._SelectedModelState == 2:
						self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],False)
				self._SelectedModelList = []
					
		if self._SelectModelClass.GetDragState():
			if self._SelectedModel != None:
				
				#選択を解除し、モデルのハイライト表示をOFF
				if self._SelectedModelState == 1:
					self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,False)
				elif self._SelectedModelState == 2:
					self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,False)
				self._SelectedModel = None
				
			#2つ目の壁
			if self._SelectedModelList != []:
				for model in self._SelectedModelList:
					if self._SelectedModelState == 1:
						self._RoomControlClass.SetHighlightWallLine(model[0],model[1],False)
					elif self._SelectedModelState == 2:
						self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],False)
				self._SelectedModelList = []
				
		self._PreSelectedRoom = self._SelectModelClass.GetSelectedRoom()	#直前に選択していた間取りを取得
		self._PreSelectedRoomList = self._SelectModelClass.GetSelectedRoomList()

		#print 'select wall'
		#print 'room=',self._SelectedRoom,'wall=',self._SelectedModel,'state=',self._SelectedModelState
		#print 'roomList=',self._SelectedModelList
		
	#選択
	def Select(self,withDoorAndWindowState,onlyRoom=None,pos=[-1,-1]):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		roomNum = None
		modelNum = None
		
		if onlyRoom == None:
			self._RoomControlClass.SetPickableForAllModel(True)
			if withDoorAndWindowState == False:
				self._RoomControlClass.SetAllDoorPickable(False)
				self._RoomControlClass.SetAllWindowPickable(False)
				
		else:
			self._RoomControlClass.SetRoomPickable(onlyRoom,True)
			if withDoorAndWindowState == False:
				self._RoomControlClass.SetDoorPickable(onlyRoom,False)
				self._RoomControlClass.SetWindowPickable(onlyRoom,False)
				
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel(pos)
		
		if onlyRoom == None:
			self._RoomControlClass.SetPickableForAllModel(False)
		else:
			self._RoomControlClass.SetRoomPickable(onlyRoom,False)
		
		modelState = 0
		roomNum,modelNum = self._RoomControlClass.GetWallLineNum(pickedModel)
		if modelNum != None:
			modelState = 1
		else:
			roomNum,modelNum = self._RoomControlClass.GetWallPointNum(pickedModel)
			if modelNum != None:
				modelState = 2
				
		#print "roomNum=",roomNum,",modelNum=",modelNum,",modelState=",modelState
		return roomNum,modelNum,modelState
		
	#選択
	def Select2(self,withDoorAndWindowState,selectedRoom,modelState):
		#print 'selectwall2'
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		roomNum = None
		modelNum = None
		
		modelList = []
		
		while True:
			self._RoomControlClass.SetPickableForAllModel(True)
			self._RoomControlClass.SetRoomPickable(selectedRoom,False)
			for model in modelList:
				self._RoomControlClass.SetRoomPickable(model[0],False)
			
			if withDoorAndWindowState == False:
				self._RoomControlClass.SetDoorPickable(False)
				self._RoomControlClass.SetWindowPickable(False)
				
			state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
			
			self._RoomControlClass.SetPickableForAllModel(False)
			
			modelState2 = 0
			roomNum,modelNum = self._RoomControlClass.GetWallLineNum(pickedModel)
			if modelNum != None:
				modelState2 = 1
			else:
				roomNum,modelNum = self._RoomControlClass.GetWallPointNum(pickedModel)
				if modelNum != None:
					modelState2 = 2
			
			if modelState2 != modelState:
				roomNum = None
				modelNum = None
			
			if roomNum != None and modelNum != None:
				modelList.append([roomNum,modelNum])
			
			else:
				break
			
		#print "roomNum=",roomNum,",modelNum=",modelNum,",modelState=",modelState
		return modelList
		
	#選択解除
	def UnSelect(self):
		if self._SelectedModel != None:
			if self._SelectedModelState == 1:
				self._RoomControlClass.SetHighlightWallLine(self._SelectedRoom,self._SelectedModel,False)
			elif self._SelectedModelState == 2:
				self._RoomControlClass.SetHighlightWallPoint(self._SelectedRoom,self._SelectedModel,False)
			self._SelectedModel = None

			if self._SelectedModelList != []:
				for model in self._SelectedModelList:
					if self._SelectedModelState == 1:
						self._RoomControlClass.SetHighlightWallLine(model[0],model[1],False)
					elif self._SelectedModelState == 2:
						self._RoomControlClass.SetHighlightWallPoint(model[0],model[1],False)
				self._SelectedModelList = []
				
			self._ManipulatorClass.SetModel(None)	#マニピュレータ設定
		
	#選択しているモデルの取得
	def GetSelectedModel(self):
		selectedRoom = self._SelectedRoom
		selectedModel = self._SelectedModel
		selectedModelState = self._SelectedModelState
		return selectedRoom,selectedModel,selectedModelState
		
	#選択している2つ目のモデルの取得
	def GetSelectedModel2(self):
		selectedModelList = self._SelectedModelList
		return selectedModelList
		
	#クリック状態の取得
	def GetClickState(self):
		state = self._ClickState
		return state
	
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		if self._Is3d:
			self.UnSelect()