﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテムサイズ変更制御のクラス定義
class ScaleItem(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ScaleItem] : Init ScaleItem')
		
		self._InputClass = None
		self._ItemControlClass = None
		self._SelectModelClass = None
		self._ManipulatorClass = None
		self._CheckStateClass = None
		self._StencilClass = None
		self._SetItemSizeButtonClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._SelectedModel = None
		
		self._DragState = False
		self._PreDragState = False
		
		self._ScaleState = False
		self._PreScaleState = False
		
		self._PickState = -1
		self._PickedNodeNum = -1
		self._PickedPosition = [0,0,0]
		
		self._PreModelScale = [1,1,1]
		self._PreModelPosition = [0,0,0]
		
		self._DummyBoxNodeNameList = ['Top'
									 ,'Front'
									 ,'Right'
									 ,'Back'
									 ,'Left'
									 ,'Bottom']
									 
		self._DummyBoxItem = None
		self._DummyBoxModel = None
		self._DummyBoxRoot = None
		self._DummyBoxDummy1 = None
		self._DummyBoxDummy2 = None
		self._DummyBoxLineList = []
		
		self._DummyBoxLineSize = 0.1		#各辺のラインの幅
		
		self.InitScalingPlane()
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,selectModelClass,manipulatorClass,checkStateClass
				,stencilClass,setItemSizeButtonClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._SelectModelClass = selectModelClass
		self._ManipulatorClass = manipulatorClass
		self._CheckStateClass = checkStateClass
		self._StencilClass = stencilClass
		self._SetItemSizeButtonClass = setItemSizeButtonClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self._UndoData = []
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ScaleItem] : Start ScaleItem Update')
		
		self._ScaleState = False
		
		#ドラッグ開始
		if self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() == False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetChangeState() == False and self._CheckStateClass.GetOverAndModeState() != True:
			if self._InputClass.GetMouseStartDragState():
				manipulatorState = self._ManipulatorClass.GetState()
				#print 'manipulatorState',manipulatorState
				
				self._SelectedModel = self._SelectModelClass.GetSelectedModel()
				selectedModelList = self._SelectModelClass.GetSelectedModelList()
				#print 'self._SelectedModel',self._SelectedModel
				#print 'selectedModelList',selectedModelList
				
				if manipulatorState == 0 and self._SelectedModel != None and len(selectedModelList) == 1:
					modelNum,nodeName,roomNum,characterNum,doorNum,windowNum = self._SelectModelClass.Select()
					#print 'modelNum',modelNum
					
					itemName = self._ItemControlClass.GetFileName(self._SelectedModel)
					if modelNum == self._SelectedModel and itemName[-13:] == 'DummyBox.osgb': 
						self._PickState,self._PickedNodeNum = self.PickModel()
						self.SetScalingPlanePickable(True)
						
						if self._PickState > 0:
							self._DragState = True
							
							prePos  = self._ItemControlClass.GetPosition(self._SelectedModel)
							preSize = self._ItemControlClass.GetSize(self._SelectedModel)
							self._UndoData = []
							self._UndoData.append(23)
							self._UndoData.append([self._SelectedModel,prePos,preSize])
			
						#print 'self._PickState',self._PickState
						#print 'self._PickedNodeNum',self._PickedNodeNum
						
		#ドラッグ中	
		if self._DragState == True:
			#print 'ドラッグ中'
			self._PickedPosition = self.PickPosition()
			self.SetDummyPosition()
			self.SetScale(self._PickState,self._PickedNodeNum)
			
		#ドラッグ終了
		if self._InputClass.GetMouseDragState() == False:
			if self._DragState == True:
				#print 'ドラッグ終了'
				self._DragState = False
				self._PickState = -1
				self._PickedNodeNum = -1
				
				self.SetScalingPlanePickable(False)
				
				self._UndoClass.AddToList(self._UndoData)
				self._RedoClass.ClearList()

		self._PreDragState = self._DragState
		self._PreScaleState = self._ScaleState
		
	#マニピュレータを操作中か
	def GetState(self):
		state = self._DragState
		
		return state
		
	#スケール変更用板初期設定
	def InitScalingPlane(self):
		self._ScalingPlane = vizshape.addPlane()
		self._ScalingPlane.disable(viz.RENDERING)
		self._ScalingPlane.disable(viz.INTERSECTION)
		self._ScalingPlane.disable(viz.PICKING)

	#スケール変更用板の選択設定
	def SetScalingPlanePickable(self,state):
		if state:
			self._ScalingPlane.enable(viz.INTERSECTION)
			self._ScalingPlane.enable(viz.PICKING)
		else:
			self._ScalingPlane.disable(viz.INTERSECTION)
			self._ScalingPlane.disable(viz.PICKING)
			
	#ダミーボックスの選択設定
	def SetDummyBoxPickable(self,state):
		if state:
			self._DummyBoxModel.enable(viz.INTERSECTION)
			self._DummyBoxModel.enable(viz.PICKING)
		else:
			self._DummyBoxModel.disable(viz.INTERSECTION)
			self._DummyBoxModel.disable(viz.PICKING)
			
	#ダミーボックスのパーツを選択
	def PickModel(self):
		self._DummyBoxItem = self._ItemControlClass.GetModel(self._SelectedModel)
		self._DummyBoxModel,self._DummyBoxRoot,self._DummyBoxDummy1,self._DummyBoxDummy2,self._DummyBoxLineList = self._DummyBoxItem.GetDummyBoxModel()
		
		state = -1
		nodeNum = -1
		
		valid = False
		pickedModel = None
		pickedModelName = ""
		pickingPosition = [0.0,0.0,0.0]
		
		self.SetDummyBoxPickable(True)
		valid,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self.SetDummyBoxPickable(False)
		
		self._DummyBoxDummy1.setPosition([0,0,0],viz.ABS_PARENT)
		
		if valid == True:
			state = 0
			self._DummyBoxDummy1.setPosition(pickingPosition,viz.ABS_GLOBAL)
			
		if state == 0:
			box = self._DummyBoxModel.getBoundingBox()
			modelSize = box.size
			#print 'modelSize=',modelSize
			
			self._PreModelScale = [0,0,0]
			self._PreModelScale[0] = modelSize[0]
			self._PreModelScale[1] = modelSize[1]
			self._PreModelScale[2] = modelSize[2]
			
			self._PreModelPosition = self._DummyBoxRoot.getPosition()
			#print 'root.getPosition()	=',self._DummyBoxRoot.getPosition()
			
			stateX = 0
			stateY = 0
			stateZ = 0
			
			localPos = self._DummyBoxDummy1.getPosition(viz.ABS_PARENT)
			
			posXShl = (modelSize[0] * 0.5) - 0.1
			if localPos[0] > posXShl:
				stateX = 1
			elif localPos[0] < -posXShl:
				stateX = -1
				
			posYShlMin = 0.1
			posYShlMax = modelSize[1] - 0.1
			if localPos[1] > posYShlMax:
				stateY = 1
			elif localPos[1] < posYShlMin:
				stateY = -1
				
			posZShl = (modelSize[2] * 0.5) - 0.1
			if localPos[2] > posZShl:
				stateZ = 1
			elif localPos[2] < -posZShl:
				stateZ = -1
				
			#上面
			if pickedModelName == 'DummyBox_'+self._DummyBoxNodeNameList[0]:
				nodeNum = 0
				if   stateX == -1 and stateZ == -1:
					state = 1
				elif stateX ==  1 and stateZ == -1:
					state = 2
				elif stateX ==  1 and stateZ ==  1:
					state = 3
				elif stateX == -1 and stateZ ==  1:
					state = 4
				elif stateX ==  0 and stateZ == -1:
					state = 5
				elif stateX ==  1 and stateZ ==  0:
					state = 6
				elif stateX ==  0 and stateZ ==  1:
					state = 7
				elif stateX == -1 and stateZ ==  0:
					state = 8
				
			#正面
			elif pickedModelName == 'DummyBox_'+self._DummyBoxNodeNameList[1]:
				nodeNum = 1
				if   stateX == -1 and stateY == -1:
					state = 1
				elif stateX ==  1 and stateY == -1:
					state = 2
				elif stateX ==  1 and stateY ==  1:
					state = 3
				elif stateX == -1 and stateY ==  1:
					state = 4
				elif stateX ==  0 and stateY == -1:
					state = 5
				elif stateX ==  1 and stateY ==  0:
					state = 6
				elif stateX ==  0 and stateY ==  1:
					state = 7
				elif stateX == -1 and stateY ==  0:
					state = 8
					
			#右面
			elif pickedModelName == 'DummyBox_'+self._DummyBoxNodeNameList[2]:
				nodeNum = 2
				if   stateZ == -1 and stateY == -1:
					state = 1
				elif stateZ ==  1 and stateY == -1:
					state = 2
				elif stateZ ==  1 and stateY ==  1:
					state = 3
				elif stateZ == -1 and stateY ==  1:
					state = 4
				elif stateZ ==  0 and stateY == -1:
					state = 5
				elif stateZ ==  1 and stateY ==  0:
					state = 6
				elif stateZ ==  0 and stateY ==  1:
					state = 7
				elif stateZ == -1 and stateY ==  0:
					state = 8
					
			#裏面
			elif pickedModelName == 'DummyBox_'+self._DummyBoxNodeNameList[3]:
				nodeNum = 3
				if   stateX ==  1 and stateY == -1:
					state = 1
				elif stateX == -1 and stateY == -1:
					state = 2
				elif stateX == -1 and stateY ==  1:
					state = 3
				elif stateX ==  1 and stateY ==  1:
					state = 4
				elif stateX ==  0 and stateY == -1:
					state = 5
				elif stateX == -1 and stateY ==  0:
					state = 6
				elif stateX ==  0 and stateY ==  1:
					state = 7
				elif stateX ==  1 and stateY ==  0:
					state = 8
					
			#左面
			elif pickedModelName == 'DummyBox_'+self._DummyBoxNodeNameList[4]:
				nodeNum = 4
				if   stateZ ==  1 and stateY == -1:
					state = 1
				elif stateZ == -1 and stateY == -1:
					state = 2
				elif stateZ == -1 and stateY ==  1:
					state = 3
				elif stateZ ==  1 and stateY ==  1:
					state = 4
				elif stateZ ==  0 and stateY == -1:
					state = 5
				elif stateZ == -1 and stateY ==  0:
					state = 6
				elif stateZ ==  0 and stateY ==  1:
					state = 7
				elif stateZ ==  1 and stateY ==  0:
					state = 8
					
			else:
				state = -1
				nodeNum = -1
			
			scalingPlane = self._ScalingPlane
			
			if state == -1 or state == 0:
				scalingPlane.setPosition([0,0,0])
				scalingPlane.setEuler([0,0,0])
			else:
				scalingPlane.setPosition([0,0,0])
				scalingPlane.setEuler([0,0,0])
				
				scalingPlane.setParent(pickedModel)
				
				if nodeNum == 0:	#上面
					scalingPlane.setEuler([0,0,0])
				elif nodeNum == 1:	#正面
					scalingPlane.setEuler([0,-90,0])
				elif nodeNum == 2:	#右面
					scalingPlane.setEuler([0,0,-90])
				elif nodeNum == 3:	#裏面
					scalingPlane.setEuler([0,90,0])
				elif nodeNum == 4:	#左面
					scalingPlane.setEuler([0,0,90])
					
				newQuat = scalingPlane.getQuat(mode=viz.ABS_GLOBAL)
				#print 'newQuat=',newQuat
				
				scalingPlane.setParent(viz.WORLD)
				
				scalingPlane.setPosition(pickingPosition)
				#scalingPlane.setEuler([0,0,0])
				scalingPlane.setQuat(newQuat)
				
		#print 'finalstate=',state
		return state,nodeNum
		
	def PickPosition(self):
		self.SetDummyBoxPickable(False)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self.SetDummyBoxPickable(True)
		
		return pickingPosition
		
	def SetDummyPosition(self):
		self._DummyBoxItem.SetPosition(self._PreModelPosition)
		
		self.SetDummyBoxPickable(True)
		valid,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self.SetDummyBoxPickable(False)
		
		self._DummyBoxDummy1.setPosition([0,0,0],viz.ABS_PARENT)
		
		if valid == True:
			self._DummyBoxDummy1.setPosition(pickingPosition,viz.ABS_GLOBAL)
			#print 'pickedPos',pickingPosition
			
	def SetScale(self,state,nodeNum):
		pickedPosition = self._PickedPosition
		preModelScale = self._PreModelScale
		preModelPosition = self._PreModelPosition
		
		lineSize = self._DummyBoxLineSize
		
		dummyPos = self._DummyBoxDummy1.getPosition(viz.ABS_PARENT)
		#print 'dummyPos=',dummyPos
		
		self._DummyBoxDummy2.setPosition([0,0,0],viz.ABS_PARENT)
		#dummy2.setPosition(pickingPosition,viz.ABS_GLOBAL)
		
		newScale = [0,0,0]
		newScale[0] = self._PreModelScale[0]
		newScale[1] = self._PreModelScale[1]
		newScale[2] = self._PreModelScale[2]
		
		newPos = [0,0,0]
		newPos[0] = self._PreModelPosition[0]
		newPos[1] = self._PreModelPosition[1]
		newPos[2] = self._PreModelPosition[2]
		
		newDummy2Pos = [0,0,0]
		
		self._DummyBoxDummy2.setPosition(newPos,viz.ABS_GLOBAL)
		
		offset = [0,0,0]
		
		#print nodeNum
		if nodeNum == 0:	#上面
			if state == 1:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0]= (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				newDummy2Pos[2]= (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 2:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				newDummy2Pos[2] = (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 3:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 4:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 5:
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2]= (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 6:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				
			elif state == 7:
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 8:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				
		if nodeNum == 1:	#正面
			if state == 1 or state == 8:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				
			elif state == 2 or state == 6:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				
			elif state == 3:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				
			elif state == 4:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				
			elif state == 7:
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
		if nodeNum == 2:	#右面
			if state == 1 or state == 8:
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 2 or state == 6:
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 3:
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 4:
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 7:
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
		if nodeNum == 3:	#裏面
			if state == 1 or state == 8:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				
			elif state == 2 or state == 6:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				
			elif state == 3:
				newScale[0] = self._PreModelScale[0]*0.5 - dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] + self._PreModelScale[0]*0.5)*0.5 - offset[0]
				
			elif state == 4:
				newScale[0] = self._PreModelScale[0]*0.5 + dummyPos[0]
				if newScale[0] < self._DummyBoxLineSize*2:
					offset[0] = self._DummyBoxLineSize - newScale[0]*0.5
					newScale[0] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[0] = (dummyPos[0] - self._PreModelScale[0]*0.5)*0.5 + offset[0]
				
			elif state == 7:
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
		if nodeNum == 4:	#左面
			if state == 1 or state == 8:
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 2 or state == 6:
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 3:
				newScale[2] = self._PreModelScale[2]*0.5 - dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] + self._PreModelScale[2]*0.5)*0.5 - offset[2]
				
			elif state == 4:
				newScale[2] = self._PreModelScale[2]*0.5 + dummyPos[2]
				if newScale[2] < self._DummyBoxLineSize*2:
					offset[2] = self._DummyBoxLineSize - newScale[2]*0.5
					newScale[2] = self._DummyBoxLineSize*2
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
				
				newDummy2Pos[2] = (dummyPos[2] - self._PreModelScale[2]*0.5)*0.5 + offset[2]
				
			elif state == 7:
				newScale[1] = dummyPos[1]
				if newScale[1] < self._DummyBoxLineSize*2:
					newScale[1] = self._DummyBoxLineSize*2
					
		
		self._DummyBoxDummy2.setPosition(newDummy2Pos,viz.ABS_PARENT)
		newPos = self._DummyBoxDummy2.getPosition(viz.ABS_GLOBAL)
		
		self._ItemControlClass.SetPosition(self._SelectedModel,newPos)
		self._DummyBoxItem.SetSize(newScale)
		#self._DummyBoxItem.SetPosition(newPos)
		self._DummyBoxItem.SetDummyBoxLinePosition()
		
		#print 'scale=',newScale
		
		#テキストボックスが表示されていたら数値を更新
		if self._SetItemSizeButtonClass.GetVisible():
			size = self._ItemControlClass.GetSize(self._SelectedModel)
			self._SetItemSizeButtonClass.SetValue(self._SelectedModel,newScale)
			