﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Interface

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ログ関係のファンクション制御のクラス定義
class LogFunction(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogFunction] : Init LogFunction')
		
		self._InputClass = None
		self._LogControlClass = None
		self._SelectLogClass = None
		
		self._SelectedModel = None
		
	#使用するクラスの追加
	def AddClass(self,inputClass,logControlClass,selectLogClass):
		self._InputClass = inputClass
		self._LogControlClass = logControlClass
		self._SelectLogClass = selectLogClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LogFunction] : Start LogFunction Update')
		pass
	
	#クリック
	def Click(self,num):
		if num == 0:
			self.Delete()
	
	#選択が解除された
	def Hide(self):
		pass
		
	#削除
	def Delete(self):
		self._SelectedModel = self._SelectLogClass.GetSelectedModel()
		self._SelectLogClass.UnSelect()
		self._LogControlClass.Delete(self._SelectedModel)
		