﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import viz
import viztask
import vizinput
import os.path
import codecs
import datetime

#---------------------------------------------------------
#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'
EXE_FOLDER_NAME		= 'RiMMInspector'

#---------------------------------------------------------
class LogVR(object):
	
	#---------------------------------------------------------
	def __init__(self):
		"""
		コンストラクタ
		"""
		self.__state = False
		self.__LogList = ""
		self.__time = 0.0
		
		self.__preKey = [False, False, False]
		self.__directory = self.__getRootPath() + SETTING_FOLDER_PATH + 'Log\\'
		self.__fileName = '20170101-010101'		#年月日-時分秒
		
		viz.setOption('viz.AVIRecorder.maxWidth','1280')
		viz.setOption('viz.AVIRecorder.maxHeight','960')
		viz.setOption('viz.AVIRecorder.fps','30')
		
	#---------------------------------------------------------
	def __del__(self):
		"""
		デストラクタ
		"""
		pass
		
	#---------------------------------------------------------
	def Update(self,key,view):
		"""
		毎フレーム呼び出されるための関数	
		"""
		#Sキーボードで開始
		if key[0] == True and self.__preKey[0] == False:
			print 'start'
			self.__start()
			
		#Eキーボードで停止
		if key[1] == True and self.__preKey[1] == False:
			print 'end'
			self.__end()
			self.__saveFile()
			
		#ログをリストに追加
		if self.__state:
			#リストに追加
			self.__add(view)
			
			#経過時間を更新
			self.__time += viz.getFrameElapsed()
			
		self.__preKey = key
		
	#---------------------------------------------------------
	def __start(self):
		"""
		記録開始
		"""
		self.__state = True
		self.__LogList = ""
		self.__time = 0.0
		
		today = str(datetime.datetime.today())
		self.__fileName = today[0:4]+today[5:7]+today[8:10]+'-'+today[11:13]+today[14:16]+today[17:19]
		print 'LogFileName',self.__fileName
		
		viz.window.startRecording(self.__directory + self.__fileName + '.avi')
		
	#---------------------------------------------------------
	def __end(self):
		"""
		記録停止
		"""
		self.__state = False
		
		viz.window.stopRecording()
		
	#---------------------------------------------------------
	def __saveFile(self):
		"""
		ファイルに保存
		"""
		#CSVファイル保存
		filePath = self.__directory + self.__fileName + '.csv'
		f = codecs.open(filePath, 'w', 'shift-jis')	
		f.write(self.__LogList)
		f.close()

	#---------------------------------------------------------
	def __add(self,view):
		"""
		記録停止
		"""
		data = ""
		
		time	 = str(self.__time) + ','
		headPos	 = str(view[0][0]) + ',' + str(view[0][1]) + ',' + str(view[0][2]) + ','
		hadAngle = str(view[1][0]) + ',' + str(view[1][1]) + ',' + str(view[1][2]) + ','
		eyeDirL	 = str(view[2][0]) + ',' + str(view[2][1]) + ',' + str(view[2][2]) + '\n'
		data += time + headPos + hadAngle + eyeDirL
		self.__LogList += data
		
	#---------------------------------------------------------
	def __getRootPath(self):
		"""
		ルートフォルダの取得
		"""
		exeDir = viz.res.getPublishedPath('')
		if os.path.exists(exeDir) == False:
			exeDir = 'C:\\RiMMroot\\RiMMInspector\\'
			if os.path.exists(exeDir) == False:
				exeDir = 'E:\\Git\\rtri\\RiMMInspector\\'
		rootDir = exeDir[:-len(EXE_FOLDER_NAME)-1]
		
		return rootDir
		