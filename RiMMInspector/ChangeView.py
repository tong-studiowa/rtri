﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object2d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#2D/3D切り替え制御のクラス定義
class ChangeView(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ChangeView] : Init ChangeView')
		
		self._InputClass = None
		self._View2dClass = None
		self._View3dClass = None
		self._StencilClass = None
		self._RoomControlClass = None
		self._ManipulatorClass = None
		self._FunctionButtonAreaClass = None
		self._SelectWallClass = None
		self._SetDirectionClass = None
		self._AddLogClass = None
		self._ItemControlClass = None
		self._AddPathClass = None
		self._BackgroundClass = None
		self._ShowDrawingClass = None
		self._ShowPathClass = None
		self._PlayPathClass = None
		self._SelectModelClass = None
		self._SetFileterClass = None
		self._TranslateModelClass = None
		self._TranslateWallClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._ClickState = False
		self._OverState = False
		
		self._Is3d = False
		
		self._KeyState = False
		self._PreKeyState = False
		
		#フレーム追加
		#self._Position = [226,172]
		self._Position = [226,32]
		self._PositionX = [226,63]
		self._Scale = [100,36]
		self._Frame = self.CreateFrame(self._Position,self._Scale)
		
		#テキスト追加
		self._TextScale = [18.6,18.6]
		self._Text = self.CreateText("",self._Position,self._TextScale)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,view2dClass,view3dClass,stencilClass,roomControlClass
				,manipulatorClass,functionButtonAreaClass,selectWallClass,setDirectionClass
				,addLogClass,itemControlClass,addPathClass,backgroundClass,showDrawingClass
				,showPathClass,playPathClass,selectModelClass,setFileterClass,translateModelClass
				,translateWallClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._View2dClass = view2dClass
		self._View3dClass = view3dClass
		self._StencilClass = stencilClass
		self._RoomControlClass = roomControlClass
		self._ManipulatorClass = manipulatorClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._SelectWallClass = selectWallClass
		self._SetDirectionClass = setDirectionClass
		self._AddLogClass = addLogClass
		self._ItemControlClass = itemControlClass
		self._AddPathClass = addPathClass
		self._BackgroundClass = backgroundClass
		self._ShowDrawingClass = showDrawingClass
		self._ShowPathClass = showPathClass
		self._PlayPathClass = playPathClass
		self._SelectModelClass = selectModelClass
		self._SetFileterClass = setFileterClass
		self._TranslateModelClass = translateModelClass
		self._TranslateWallClass = translateWallClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self.Reset()
		
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ChangeView] : Start ChangeView Update')
		
		self._ClickState = False
		
		mousePosition = self._InputClass.GetMousePosition()
		self._OverState = self.CheckRollOver(mousePosition,self._Position,self._Scale)
		
		if self._OverState:
			if self._InputClass.GetMouseClickState() and self._InputClass.GetAltKeyState() == False and self._InputClass.GetShiftKeyState() ==False and self._StencilClass.GetChangeClickState() == False and self._ManipulatorClass.GetState() == False:
				self.Change()
			
		#ロールオーバー
		self.Rollover(self._OverState)
		
		#キー操作
		self._KeyState = self._InputClass.GetXKeyState()
		if self._KeyState == True and self._PreKeyState == False:
			if self._TranslateModelClass.GetTranslateState() == False and self._TranslateWallClass.GetTranslateState() == False:
				self._AddPathClass.Reset()
		if self._KeyState == False and self._PreKeyState == True:
			if self._TranslateModelClass.GetTranslateState() == False and self._TranslateWallClass.GetTranslateState() == False:
				self.Change()
		
		self._PreKeyState = self._KeyState
		
	#設定を反映
	def Change(self):
		if self._Is3d == True:
			self._Is3d = False
		else:
			self._Is3d = True
			
		self.Set()
		self._RoomControlClass.SetRoomVisibleState()
		
	#設定を反映
	def Set(self):
		if self._Is3d == False:
			self.SetText(False)
			camTargetPos = self._View3dClass.GetCameraTargetPos()
			self._View2dClass.SetViewPos(camTargetPos)
			self._View2dClass.Reset()
			self._View2dClass.SetWindowVisible(True)
			#self._SetDirectionClass.SetVisible(True)
			self._StencilClass.SetVisible(True)
			self._ManipulatorClass.SetViewState(True)
			self._SelectWallClass.SetViewState(False)
			#self._AddLogClass.SetVisible(True)
			self._InputClass.SetViewState(False)
			self._RoomControlClass.SetViewState(False)
			self._ItemControlClass.SetViewState(False)
			self._AddPathClass.SetAddState(False)
			self._AddPathClass.SetViewState(False)
			self.SetViewState(False)
			self._BackgroundClass.SetViewState(False)
			self._ShowDrawingClass.SetViewState(False)
			self._ShowPathClass.SetViewState(False)
			self._PlayPathClass.SetViewState(False)
			self._SelectModelClass.SetViewState(False)
			self._SetFileterClass.SetViewState(False)
			self._UndoClass.SetViewState(False)
			self._RedoClass.SetViewState(False)
			
		else:
			self.SetText(True)
			camTargetPos = self._View2dClass.GetViewPos()
			self._View3dClass.SetCameraTargetPos(camTargetPos)
			self._View3dClass.Reset()
			self._View2dClass.SetWindowVisible(False)
			#self._SetDirectionClass.SetVisible(False)
			self._StencilClass.SetVisible(False)
			self._ManipulatorClass.SetViewState(False)
			self._SelectWallClass.SetViewState(True)
			#self._AddLogClass.SetVisible(False)
			self._InputClass.SetViewState(True)
			self._RoomControlClass.SetViewState(True)
			self._ItemControlClass.SetViewState(True)
			self._AddPathClass.SetViewState(True)
			self.SetViewState(True)
			self._BackgroundClass.SetViewState(True)
			self._ShowDrawingClass.SetViewState(True)
			self._ShowPathClass.SetViewState(True)
			self._PlayPathClass.SetViewState(True)
			self._SelectModelClass.SetViewState(True)
			self._SetFileterClass.SetViewState(True)
			self._UndoClass.SetViewState(True)
			self._RedoClass.SetViewState(True)
			
		self.SetAspectRatio()
		self._BackgroundClass.SetAspectRatio()
		self._ShowDrawingClass.SetAspectRatio()
		self._ShowPathClass.SetAspectRatio()
		self._PlayPathClass.SetAspectRatio()
		self._SetFileterClass.SetAspectRatio()
		self._UndoClass.SetAspectRatio()
		self._RedoClass.SetAspectRatio()
		
	#リセット処理
	def Reset(self):
		self._ClickState = False
		self._Is3d = False
		self._View2dClass.Reset()
		self.SetText(False)
		
	#テキスト設定
	def SetText(self,state):
		if state == False:
			self._Text.message("３Ｄに切替")
		else:
			self._Text.message("２Ｄに切替")
	
	#表示状態の設定
	def SetViewState(self,state):
		if state:
			self._Position[0] = self._PositionX[1]
		else:
			self._Position[0] = self._PositionX[0]
		
	#視点の状態の取得
	def GetViewState(self):
		state = self._Is3d
		
		return state
		
	#表示状態設定
	def SetVisible(self,state):
		if state:
			self._Frame.visible(viz.ON)
			self._Text.visible(viz.ON)
		else:
			self._Frame.visible(viz.OFF)
			self._Text.visible(viz.OFF)
			
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		
		return state
		
	#ロールオーバー処理
	def Rollover(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Frame.color(color)
		
	#画角を設定
	def SetAspectRatio(self):
		self.SetIconPosition(self._Frame,self._Position,self._Scale)
		self.SetIconPosition(self._Text,self._Position,self._TextScale)
		