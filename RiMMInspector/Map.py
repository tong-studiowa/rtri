﻿# coding: utf-8
#PathSimulator

import viz
import viztask

class MapClass():
	
	def __init__(self):
		
		#窓とビューの設定
		self._window = viz.addWindow()
		self._viewcam = viz.addView()
		self._viewcam.getHeadLight().disable()
		self._window.setView(self._viewcam)
		self._window.fov(50, 1.333)	#パースペクティブ設定
		self._window.drawOrder(2)
		
		#矢印マーカーの設定
		self._marker = viz.addTexQuad(parent=viz.SCREEN,align=viz.ALIGN_CENTER,size=100)
		self._marker.texture(viz.addTexture(viz.res.getPublishedPath('Resource\\Interface\\arrow.tga')))
		self._marker.setPosition(0.5,0.5)
		self._marker.color(viz.RED)
		self._marker.renderOnlyToWindows([self._window])
		
		#パラメーターの宣言
		self._target = None
		self._link = None
		self._isVisible = True
		self._MapMode = 0		#0=進行方向が上向き、1=マップの向き固定
		
		#窓のデフォルト設定
		self._offset = [0.0,10.0,0.0]
		self._windowSize = viz.window.getSize()
		
		self._disppos	= 0
		self._size		= [0.3,0.3]
		self.SetSize(self._size)
		
		self.SetVisible(False,state=False)
		
	def SetVisible(self,visible,state=True):
		if state:
			self._isVisible = visible
		
		if visible:
			self._window.visible(True)
		else:
			self._window.visible(False)
	
	def GetVisible(self):
		return self._isVisible
	
	def SetOffset(self,offset):
		self._offset=offset
	
	def SetPosition(self,pos):
		try:
			self._disppos=int(pos)
		except:
			self._disppos=0
	'''
	def SetPosition(self,pos,pixel=False):
		
		pos_ratio=pos
		
		if pixel:
			windowsize=viz.MainWindow.getSize()
			pos_ratio=[pos[0]/windowsize[0],pos[1]/windowsize[1]]
		
		self._window.setPosition(pos_ratio)
		self._position=pos_ratio
	'''
	def SetSize(self,size,pixel=False):
		
		size_ratio=size
		
		if pixel:
			windowsize=viz.MainWindow.getSize()
			size_ratio=[size[0]/windowsize[0],size[1]/windowsize[1]]
		
		self._window.setSize(size_ratio)
		self._size=size_ratio
	
	def SetMode(self,mode):
		self._MapMode = mode
		
	def SetTarget(self,target):
		
		if target:
			self._target=target
			pos=self._target.getPosition()
			eul=self._target.getEuler()
			camerapos=[	pos[0]+self._offset[0],
						pos[1]+self._offset[1],
						pos[2]+self._offset[2]]
			self._viewcam.setPosition(camerapos)
			self._viewcam.setEuler([eul[0],90.0,0]) #真下を向く
			
			if self._link:	self._link.kill()
			self._link = viztask.schedule(self._KeepCamera)
	
	def _KeepCamera(self): 
		while True:
			if self._target:
				pos=self._target.getPosition()
				eul=self._target.getEuler()
				camerapos=[	pos[0]+self._offset[0],
							self._offset[1],
							pos[2]+self._offset[2]]
				self._viewcam.setPosition(camerapos)
							
				if self._MapMode == 1:
					self._viewcam.setEuler([0,90.0,0])
					self._marker.setEuler(0,0,-eul[0])
				else:
					self._viewcam.setEuler([eul[0],90.0,0])
					self._marker.setEuler(0,0,0)
				
			yield viztask.waitFrame(1)
	
	#マップ表示用ウィンドウにターゲットを描画する・しないの設定
	def SetRenderToMapWindow(self,targets=[]):
		for target in targets:
			if target:
				target.renderOnlyToWindows([self._window])
	def SetNotRenderToMapWindow(self,targets=[]):
		for target in targets:
			if target:
				#target.renderOnlyToWindows([viz.MainWindow])
				target.renderToAllWindowsExcept([self._window])
	
	def SetRatio(self,per):
		
		WINDOW_SIZE	= viz.MainWindow.getSize(mode=viz.WINDOW_PIXELS)
		OFFSET		= [	(WINDOW_SIZE[0]-self._windowSize[0])/WINDOW_SIZE[0],
						(WINDOW_SIZE[1]-self._windowSize[1])/WINDOW_SIZE[1] ]
		
		self._window.setSize(self._size[0]*per[0],self._size[1]*per[1])
		
		if self._disppos==0:
			self._window.setPosition([1.0-self._size[0]*per[0],1.0])
		else:
			self._window.setPosition([0.0,1.0])

