﻿# coding: utf-8
#PathSimulator

import viz
import vizmat
import vizshape
import vizinput
import math
import LoadIniFile
from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#間取り読み込み用クラス定義
class LoadRoomDataClass():
	#コンストラクタ
	def __init__(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][LoadRoomDataClass] : Init LoadRoomDataClass')
		
		self._RoomControlClass = RoomControl()
		#self._RoomControlClass.Load(SETTING_FOLDER_PATH + fileName)
		self._RoomControlClass.Load(fileName)							#絶対パスのみ
	
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][LoadRoomDataClass] : del LoadRoomDataClass')
		
		modelCount = self._RoomControlClass.GetRoomCount()
		for x in range(modelCount):
			self._RoomControlClass.Delete(0)
		
		self._RoomControlClass.DeleteDrawing()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadRoomDataClass] : Start LoadRoomDataClass Update')
		pass
	
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._RoomControlClass.GetDoorModelList()
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = self._RoomControlClass.GetModelList()
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = self._RoomControlClass.GetCollisionModelList()
		return modelList
		
	#スタート位置、角度を取得
	def GetStartPosition(self):
		startPos = [0,0,0]
		startAngle = [0,0,0]
		startPos,startAngle = self._RoomControlClass.GetStartPosition()
		return startPos,startAngle
	
	#読み込みに成功したかどうかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._RoomControlClass.IsSuccessLoading()
	
	#図面のモデルを取得
	def GetDrawingModel(self):
		model = self._RoomControlClass.GetDrawingModel()
		return model
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		scale = self._RoomControlClass.GetDrawingScale()
		return scale
		
	#図面の表示状態を設定
	def SetDrawingVisible(self,state):
		self._RoomControlClass.SetDrawingVisible(state)
		
	#天井の表示切り替え
	def CeilingVisibleUpdate(self):
		self._RoomControlClass.CeilingVisibleUpdate()
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Interface:
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass

# - - - - - - - - - - - - - - - - - - - - - - - - -
#インターフェイスのクラス定義
class Object3d(Interface):
	
	#毎フレーム呼び出されるための関数
	def Update(self):
		pass
	
	#オブジェクトを読み込むための関数
	def LoadObject(self):
		pass

# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋制御のクラス定義
class RoomControl(Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Init RoomControl')
		
		self._InputClass = None
		self._ChangeViewClass = None
		self._StartPositionClass = None
		self._DrawingClass = None
		
		self._DefaultRoomFile = SETTING_FOLDER_PATH + "Room_Default.ini"
		#self._DefaultRoomFile = SETTING_FOLDER_PATH + "Room_Default_Test.ini"
		
		self._RoomIniFileName = ""
		self._RoomIniFile = None
		self._LoadErrorFlag=False	#takahashi 2014/8/1
		
		self._RoomCount = 0
		self._RoomList = []
		
		self._StartPosition = [0,0,0]
		self._StartAngle = [0,0,0]
		
		self._Drawing = None
		self._DrawingFile = ''
		self._DrawingScale = 10
		
		self._Is3d = False
		self._ModifyState = False
		
		self._DummyDoorStateList = []
		self._DummyWindowStateList = []
		
		self._DefaultFloorTexPath = ''
		self._DefaultWallTexPath = ''
		self._DefaultCeilingTexPath = ''
		self._DefaultWallHight = 3.0
		
	#使用するクラスの追加
	def AddClass(self,inputClass,changeViewClass,startPositionClass,drawingClass):
		self._InputClass = inputClass
		self._ChangeViewClass = changeViewClass
		self._StartPositionClass = startPositionClass
		self._DrawingClass = drawingClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Start RoomControl Update')
		
		for room in self._RoomList:
			room.Update()
	
	#リセット
	def Reset(self):
		pass
	
	#読み込み
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Load')
		
		self.DeleteAllDoorHole()		#ダミードアを削除
		self.DeleteAllWindowHole()		#ダミー窓を削除
		
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		room0 = iniFile.GetItemData('Rooms','0')
		del iniFile
		
		if room0 == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルは間取り定義ファイルではありません。', title='RiMMInspector')
			self._LoadErrorFlag=True	#takahashi 2014/8/1
		
		else:
			#全部屋を削除
			self.DeleteAll()
			
			self._RoomIniFileName = fileName
			
			self.Create(fileName)
			
			self._ModifyState = False
		
		#表示関係を設定
		#self._ChangeViewClass.SetRoom()
		
		self.AddAllDoorHole()		#ダミードアを作成
		self.AddAllWindowHole()		#ダミー窓を作成
		
	#保存
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][RoomControl] : Save')
		
		self.DeleteAllDoorHole()		#ダミードアを削除
		self.DeleteAllWindowHole()		#ダミー窓を削除
		
		#保存用iniファイルの作成
		self._SaveIniFile = LoadIniFile.LoadIniFileClass()
		self._SaveIniFile.ResetIniData()
		
		self._SaveIniFile.AddSection('Define')
		
		#スタート位置関係
		self.SetStartPositionFromModel()
		
		satartPos = str(self._StartPosition[0]) + ',' + str(self._StartPosition[1]) + ',' + str(self._StartPosition[2])
		self._SaveIniFile.AddItem('Define','satartposition',satartPos)
		
		satartAngle= str(self._StartAngle[0]) + ',' + str(self._StartAngle[1]) + ',' + str(self._StartAngle[2])
		self._SaveIniFile.AddItem('Define','satartangle',satartAngle)
		
		#図面関係
		self.GetDrawing()
		print 'aaa',self._DrawingFile
		
		if self._DrawingFile != '' and self._DrawingFile != None:
			print 'bbb'
			self._SaveIniFile.AddItem('Define','drawingfile',self._DrawingFile)
			self._SaveIniFile.AddItem('Define','drawingscale',str(self._DrawingScale))
		
		#間取り関係
		self._SaveIniFile.AddSection('Rooms')
		
		for x in range(self._RoomCount):
			room = self._RoomList[x]
			roomName = 'Room'+str(x)
			
			self._SaveIniFile.AddItem('Rooms',str(x),roomName)
			
			#各部屋の情報を追加
			self._SaveIniFile.AddSection(roomName)
			
			#位置
			positionTemp = room.GetFloorPosition()
			position = str(positionTemp[0])
			for x in range(2):
				position = position + ',' + str(positionTemp[x+1])
			self._SaveIniFile.AddItem(roomName,'Position',position)
			
			#角度
			#angleTemp = room.GetAxisAngle()
			angleTemp = [0,0,0]
			angle = str(angleTemp[0])
			self._SaveIniFile.AddItem(roomName,'Angle',angle)
			
			#ポイント
			floorPointList = room.GetWallPointList()
			wallVisibleList = room.GetWallVisibleList()
			for x in range(len(floorPointList)):
				pointName = 'Point' + str(x)
				pointTemp = floorPointList[x]
				
				visibleState = 0
				if wallVisibleList[x]:
					visibleState = 1
					
				point = str(pointTemp[0]) + ',' + str(pointTemp[1]) + ',' + str(visibleState)
				self._SaveIniFile.AddItem(roomName,pointName,point)
			
			#壁の高さ
			wallHeightTemp = room.GetWallHeight()
			wallHeight = str(wallHeightTemp)
			self._SaveIniFile.AddItem(roomName,'WallHeight',wallHeight)
			
			#床のテクスチャ
			floorTextureFile,floorTextureSize = room.GetFloorTextureSetting()
			floorTexture = floorTextureFile + ',' + str(floorTextureSize)
			self._SaveIniFile.AddItem(roomName,'FloorTexture',floorTexture)
			
			#壁のテクスチャ
			wallTextureFile,wallTextureSize = room.GetWallTextureSetting()
			wallTexture = wallTextureFile + ',' + str(wallTextureSize)
			self._SaveIniFile.AddItem(roomName,'WallTexture',wallTexture)
			
			#天井のテクスチャ
			ceilingTextureFile,ceilingTextureSize = room.GetCeilingTextureSetting()
			ceilingTexture = ceilingTextureFile + ',' + str(ceilingTextureSize)
			self._SaveIniFile.AddItem(roomName,'CeilingTexture',ceilingTexture)
			
			#ドア
			doorCount = room.GetDoorCount()
			for x in range(doorCount):
				doorName = 'Door'+str(x)
				dataTemp = room.GetDoorState(x)
				data = str(dataTemp[0]) + ',' + str(dataTemp[1]) + ',' + str(dataTemp[2])
				self._SaveIniFile.AddItem(roomName,doorName,data)
			
			#窓
			windowCount = room.GetWindowCount()
			for x in range(windowCount):
				windowName = 'Window'+str(x)
				dataTemp = room.GetWindowState(x)
				data = str(dataTemp[0]) + ',' + str(dataTemp[1]) + ',' + str(dataTemp[2]) + ',' + str(dataTemp[3])
				self._SaveIniFile.AddItem(roomName,windowName,data)
			
		self._SaveIniFile.WriteIniData(fileName)
		self._RoomIniFileName = fileName
		
		self.AddAllDoorHole()		#ダミードアを作成
		self.AddAllWindowHole()		#ダミー窓を作成
		
		self._ModifyState = False
			
	#作成
	def Create(self,fileName):
		self.LoadIniFile(fileName)
		
		self._RoomCount = 1000
		
		total = self._RoomIniFile.GetItemData('Rooms','Total')
		if total != None:
			self._RoomCount = int(total)
			
		list = []
		for x in range(self._RoomCount):
			roomName = self._RoomIniFile.GetItemData('Rooms',str(x))
			if roomName !=None:
				room = Room()
				room.Create(self._RoomIniFile,roomName)	#レイアウトを作成
				room.SetViewState(self._Is3d)
				
				room.SetDoorLineVisible(False)
				room.SetWindowLineVisible(False)
				
				list.append(room)
				
			else:
				self._RoomCount = x
				break
				
		self._RoomList = list
		
		#スタート位置の設定
		self.SetStartPosition()
		
		#図面の設定
		self._Drawing = Drawing()
		self.SetDrawing()
		
	#追加
	def Add(self,fileName):
		self.LoadIniFile(fileName)
		
		roomName = self._RoomIniFile.GetItemData('Rooms',str('0'))
		room = Room()
		room.SetDefaultSettings(self._DefaultFloorTexPath,self._DefaultWallTexPath,self._DefaultCeilingTexPath,self._DefaultWallHight)
		room.Create(self._RoomIniFile,roomName,True)	#レイアウトを作成
		room.SetViewState(self._Is3d)
		
		self._RoomList.append(room)
		num = len(self._RoomList) - 1
		
		self._RoomCount = len(self._RoomList)
				
		self._ModifyState = True
		
		return num
	
	#新規間取りをリストに追加
	def AddNewRoomToList(self,roomNum):
		room = self.GetRoom(roomNum)
		
	#設定ファイルの読み込み
	def LoadIniFile(self,fileName):
		self._RoomIniFileName = fileName
		
		#iniファイルの読み込み
		self._RoomIniFile = LoadIniFile.LoadIniFileClass()
		self._RoomIniFile.LoadIniFile(viz.res.getPublishedPath(self._RoomIniFileName))
		
	#読込みに成功したかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._LoadErrorFlag
	
	#削除
	def Delete(self,roomNum):
		deleteRoom = self._RoomList[roomNum]
		
		#モデルリストを更新
		newRoomLiset = []
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			if room != deleteRoom:
				newRoomLiset.append(room)
						
		self._RoomList = newRoomLiset
		
		#モデルを削除
		if deleteRoom != None:
			deleteRoom.Delete()
			del deleteRoom
			
		self._ModifyState = True
		
		self._RoomCount = len(self._RoomList)
		
	#全モデル削除
	def DeleteAll(self):
		self.DeleteAllDoorHole()	#ダミードアを削除
		self.DeleteAllWindowHole()	#ダミー窓を削除
		
		modelCount = self.GetRoomCount()
		for x in range(modelCount):
			self.Delete(0)
			
		self._RoomIniFileName = ""
			
		self._ModifyState = False
		
	#部屋を取得
	def GetRoom(self,num):
		room = self._RoomList[num]
		
		return room
		
	#間取り数取得
	def GetRoomCount(self):
		count = len(self._RoomList)
		return count
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for room in self._RoomList:
			room.SetPickableForAllModel(state)
			
	#壁のラインモデルから番号を取得
	def GetWallLineNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWallLineNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#壁のラインモデルを取得
	def GetWallLineModel(self,roomNum,modelNum):
		room = self._RoomList[roomNum]
		model = room.GetWallLineModel(modelNum)
		
		return model
		
	#壁のラインの表示状態を設定
	def SetWallLineVisible(self,state):
		for room in self._RoomList:
			room.SetWallLineVisible(state)
		
	#壁のラインのハイライト表示
	def SetHighlightWallLine(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWallLine(modelNum,state)
		
	#壁のラインの位置設定
	def SetWallLinePosition(self,roomNum,num,position):
		room = self._RoomList[roomNum]
		room.SetWallLinePosition(num,position)
		
		self._ModifyState = True
		
	#壁のラインの位置指定時のオフセット設定
	def SetWallLinePositionOffset(self,roomNum,num,position):
		room = self._RoomList[roomNum]
		room.SetWallLinePositionOffset(num,position)
		
	#壁の点モデルから番号を取得
	def GetWallPointNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWallPointNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#壁の点モデルを取得
	def GetWallPointModel(self,roomNum,wallPointNum):
		room = self._RoomList[roomNum]
		model = room.GetWallPointModel(wallPointNum)
		
		return model
		
	#壁の点の表示状態を設定
	def SetWallPointVisible(self,state):
		for Room in self._RoomList:
			Room.SetWallPointVisible(state)
		
	#壁の点のハイライト表示
	def SetHighlightWallPoint(self,roomNum,wallPointNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWallPoint(wallPointNum,state)
		
	#壁の点の位置設定
	def SetWallPointPosition(self,roomNum,wallPointNum,position):
		room = self._RoomList[roomNum]
		room.SetWallPointPosition(wallPointNum,position)
		
		self._ModifyState = True
		
	#壁の点の位置指定時のオフセット設定
	def SetWallPointPositionOffset(self,roomNum,wallPointNum,position):
		room = self._RoomList[roomNum]
		room.SetWallPointPositionOffset(wallPointNum,position)
		
	#ドア追加
	def AddDoor(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDoor(fileName,wallNum,percentage)
		
		self._ModifyState = True
		
		return num
		
	#ダミードア追加（穴を空ける）
	def AddDummyDoor(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDummyDoor(fileName,wallNum,percentage)
		
		return num
		
	#隣接する壁にドアの穴を空ける
	def AddDoorHole(self,roomNum,doorNum,fileName):
		state = False
		resRoom = None
		resWall = None
		resPerc = None
		
		#間取りを取得
		room = self._RoomList[roomNum]
		roomPos = room.GetFloorPosition()
		#print 'roomPos',roomPos
		
		#壁を取得
		doorList = room.GetDoorList()
		doorWallNum = doorList[doorNum][1]
		
		baseWallPointList = room.GetWallPointList()
		
		baseWallPointPosMin = baseWallPointList[doorWallNum]
		#print 'baseWallPointPosMin',baseWallPointPosMin
		
		baseWallPointGlobalPosMin = [0.0,0.0,0.0]
		baseWallPointGlobalPosMin[0] = roomPos[0] + baseWallPointPosMin[0]
		baseWallPointGlobalPosMin[2] = roomPos[2] + baseWallPointPosMin[1]
		
		doorWallNum2 = doorWallNum+1
		if doorWallNum2 == len(baseWallPointList):
			doorWallNum2 = 0
			
		baseWallPointPosMax = baseWallPointList[doorWallNum2]
		#print 'baseWallPointPosMax',baseWallPointPosMax
		
		baseWallPointGlobalPosMax = [0.0,0.0,0.0]
		baseWallPointGlobalPosMax[0] = roomPos[0] + baseWallPointPosMax[0]
		baseWallPointGlobalPosMax[2] = roomPos[2] + baseWallPointPosMax[1]
		
		#print 'baseWallPointGlobalPosMin',baseWallPointGlobalPosMin
		#print 'baseWallPointGlobalPosMax',baseWallPointGlobalPosMax
		
		baseWallVec = [baseWallPointGlobalPosMax[0]-baseWallPointGlobalPosMin[0],baseWallPointGlobalPosMax[2]-baseWallPointGlobalPosMin[2]]
		baseWallLong = room.GetWallSize(doorWallNum)
		baseWallNorm = [baseWallVec[0]/baseWallLong,baseWallVec[1]/baseWallLong]
		
		#ドアを取得
		door = room.GetDoorModel(doorNum)
		doorPos = door.GetPosition()
		#print 'doorPos',doorPos
		
		doorGlobalPos = [0.0,0.0,0.0]
		doorGlobalPos[0] = doorPos[0]
		doorGlobalPos[1] = 0.0
		doorGlobalPos[2] = doorPos[2]
		#print 'doorGlobalPos',doorGlobalPos
		
		#部屋の数だけ繰り返す
		for x in range(len(self._RoomList)):
			#print 'x',x
			
			if x != roomNum:
				otherRoom = self._RoomList[x]
				otehrRoomPos = otherRoom.GetFloorPosition()
				#print 'otehrRoomPos',otehrRoomPos
				
				wallPointList = otherRoom.GetWallPointList()
				
				#壁の数だけ繰り返す
				for y in range(len(wallPointList)):
					#print 'y',y
					
					wallPointPosMin = wallPointList[y]
					#print 'wallPointPosMin',wallPointPosMin
					
					wallPointGlobalPosMin = [0.0,0.0,0.0]
					wallPointGlobalPosMin[0] = otehrRoomPos[0] + wallPointPosMin[0]
					wallPointGlobalPosMin[1] = 0.0
					wallPointGlobalPosMin[2] = otehrRoomPos[2] + wallPointPosMin[1]
					
					y2 = y+1
					if y2 == len(wallPointList):
						y2 = 0
						
					wallPointPosMax = wallPointList[y2]
					#print 'wallPointPosMax',wallPointPosMax
					
					wallPointGlobalPosMax = [0.0,0.0,0.0]
					wallPointGlobalPosMax[0] = otehrRoomPos[0] + wallPointPosMax[0]
					wallPointGlobalPosMax[1] = 0.0
					wallPointGlobalPosMax[2] = otehrRoomPos[2] + wallPointPosMax[1]
					
					wallVec = [wallPointGlobalPosMax[0]-wallPointGlobalPosMin[0],wallPointGlobalPosMax[2]-wallPointGlobalPosMin[2]]
					wallLong = otherRoom.GetWallSize(y)
					wallNorm = [wallVec[0]/wallLong,wallVec[1]/wallLong]
					
					#内積を求める
					dot = baseWallNorm[0] * wallNorm[0] + baseWallNorm[1] * wallNorm[1]
					
					#壁同士の傾きが近い場合は距離を確認
					if dot < -0.99 or dot > 0.99:
						#壁の始点=A、壁の終点=B、ドア=P、面積=d、壁上のドアに近い位置=X
						vecAP = [doorGlobalPos[0]-wallPointGlobalPosMin[0],doorGlobalPos[2]-wallPointGlobalPosMin[2]]
						disAX = wallNorm[0] * vecAP[0] + wallNorm[1] * vecAP[1]
						
						d = math.fabs(wallVec[0]*vecAP[1] - wallVec[1]*vecAP[0])	#面積
						
						nearestDistance = d / wallLong
						nearestPerc = disAX / wallLong
						#print 'nearestDistance',nearestDistance
						#print 'nearestPerc',nearestPerc
						
						if nearestDistance < 0.16:	#15cm以内の場合はそこに穴を空ける
							if nearestPerc > 0.0005 and nearestPerc < 0.9995:
								#print 'CreateWindowHole'
								state = True
								resRoom = x
								resWall = y
								resPerc = nearestPerc
								#print 'distance',nearestDistance
								#print 'room',resRoom
								#print 'wall',resWall
								#print 'perc',resPerc
								
					#壁がドアの近くにあったら穴を空ける
					if state:
						dummyDoorNum = self.AddDummyDoor(resRoom,fileName,resWall,resPerc)
						
						dummyDoorData = [resRoom,dummyDoorNum,roomNum,doorNum]
						self._DummyDoorStateList.append(dummyDoorData)
						
						state = False
						
	#全てのに対して隣接する壁にドアの穴を空ける
	def AddAllDoorHole(self):
		for roomNum in range(len(self._RoomList)):
			room = self._RoomList[roomNum]
			doorCount = room.GetDoorCount()
			for doorNum in range(doorCount):
				door = room.GetDoorModel(doorNum)
				if door.GetDummyFlag() == False:
					doorFileName = door.GetFileName()
					self.AddDoorHole(roomNum,doorNum,doorFileName)
		
		#print 'AddAllDoorHole',self._DummyDoorStateList
		
	#ドアの穴を削除
	def DeleteDoorHole(self,roomNum,doorNum):
		#ドアを削除
		if roomNum != None and doorNum != None:
			state = False
			dummyRoomNum = None
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[2] == roomNum and dummyDoorData[3] == doorNum:
					self.DeleteDummyDoor(dummyDoorData[0],dummyDoorData[1])
					dummyRoomNum = dummyDoorData[0]
					self._DummyDoorStateList[x] = None
					state = True
					break
		
			if state:
				room = self._RoomList[dummyRoomNum]
				room.ReCreateWall()
				
				for x in range(len(self._DummyDoorStateList)):
					dummyDoorData = self._DummyDoorStateList[x]
					if dummyDoorData != None:
						if dummyDoorData[0] == dummyRoomNum and dummyDoorData[1] > doorNum:
							dummyDoorData[1] = dummyDoorData[1] - 1
						
						if dummyDoorData[2] == roomNum and dummyDoorData[3] > doorNum:
							dummyDoorData[3] = dummyDoorData[3] - 1
			
		#間取りを削除				
		else:
			state = False
			dummyRoomNumList = []
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[0] == roomNum or dummyDoorData[2] == roomNum:
					self.DeleteDummyDoor(dummyDoorData[0],0)
						
					dummyRoomNumList.append(dummyDoorData[0])
					self._DummyDoorStateList[x] = None
					state = True
					
			if state:
				for x in range(len(dummyRoomNumList)):
					room = self._RoomList[dummyRoomNumList[x]]
					room.ReCreateWall()
					
				for x in range(len(self._DummyDoorStateList)):
					dummyDoorData = self._DummyDoorStateList[x]
					if dummyDoorData != None:
						if dummyDoorData[0] > roomNum:
							dummyDoorData[0] = dummyDoorData[0] - 1
						if dummyDoorData[2] > roomNum:
							dummyDoorData[2] = dummyDoorData[2] - 1
					
		newList = []
		for x in range(len(self._DummyDoorStateList)):
			dummyDoorData = self._DummyDoorStateList[x]
			if dummyDoorData != None:
				newList.append(dummyDoorData)
		
		self._DummyDoorStateList = newList
		
	#ドアの穴を全て削除
	def DeleteAllDoorHole(self):
		#print 'DeleteAllDoorHole',self._DummyDoorStateList
		
		count = len(self._DummyDoorStateList)
		if count != 0:
			dummyRoomNumList = []
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData != None:
					self.DeleteDummyDoor(dummyDoorData[0],dummyDoorData[1])
					dummyRoomNumList.append(dummyDoorData[0])
					self._DummyDoorStateList[x] = None
		
					for y in range(len(self._DummyDoorStateList)):
						if y > x:
							dummyDoorData2 = self._DummyDoorStateList[y]
							if dummyDoorData2[0] == dummyDoorData[0] and dummyDoorData2[1] > dummyDoorData[1]:
								dummyDoorData2[1] = dummyDoorData2[1] - 1
								self._DummyDoorStateList[y] = dummyDoorData2
								
			for x in range(len(dummyRoomNumList)):
				room = self._RoomList[dummyRoomNumList[x]]
				room.ReCreateWall()
		
		self._DummyDoorStateList = []
		
	#ドアの移動による、壁の再作成
	def ReCreateWall(self,roomNum):
		room = self._RoomList[roomNum]
		room.ReCreateWall()
	
	#ドアの削除
	def DeleteDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			self.DeleteDoorHole(roomNum,num)		#ダミードアを削除
			
			room.DeleteDoor(num)
			
			#ダミードアのリストを更新
			for x in range(len(self._DummyDoorStateList)):
				dummyDoorData = self._DummyDoorStateList[x]
				if dummyDoorData[0] == roomNum and dummyDoorData[1] > num:
					dummyDoorData[1] = dummyDoorData[1] - 1
					self._DummyDoorStateList[x] = dummyDoorData
				
			self._ModifyState = True
			
	#ダミードアの削除
	def DeleteDummyDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.DeleteDoor(num)
		
	#ドアのコピー
	def CopyDoor(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.CopyDoor(num)
			
			self._ModifyState = True
		
	#ドアの位置設定
	def SetDoorPosition(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		if num != None and wallNum != None:	
			room.SetDoorPosition(num,wallNum,percentage)
			
			self._ModifyState = True
		
	#壁の両端の位置を取得
	def GetWallPoint(self,roomNum,num):
		room = self._RoomList[roomNum]
		point0,point1 = room.GetWallPoint(num)
		
		return point0,point1
		
	#ドアのラインの表示状態を設定
	def SetDoorLineVisible(self,state):
		for Room in self._RoomList:
			Room.SetDoorLineVisible(state)
		
	#壁の高さ取得
	def GetWallHeight(self,roomNum):
		room = self._RoomList[roomNum]
		wallHeight = room.GetWallHeight()
		
		return wallHeight
		
	#一番高い壁の高さ取得
	def GetWallHeightTop(self):
		wallHeightTop = 0
		
		for room in self._RoomList:
			height = room.GetWallHeight()
			if height > wallHeightTop:
				wallHeightTop = height
		
		return wallHeightTop
		
	#ドアのハイライト表示
	def SetHighlightDoor(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightDoor(modelNum,state)
		
	#ドアモデルから番号を取得
	def GetDoorNum(self,model):
		modelNum = None
		roomNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetDoorNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#ドアモデルを取得
	def GetDoorModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		model = room.GetDoorModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetDoorLineModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		lineModel = room.GetDoorLineModel(num)
		
		return lineModel
		
	#ドア選択可能状態を設定
	def SetDoorPickable(self,state):
		for room in self._RoomList:
			room.SetDoorPickable(state)
			
	#ドアのリストを更新
	def UpdateDoorList(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		room.UpdateDoorList(num,wallNum,percentage)
	
	#窓追加
	def AddWindow(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddWindow(fileName,wallNum,percentage)
		
		self._ModifyState = True
		
		return num
	
	#ダミー窓追加（穴を空ける）
	def AddDummyWindow(self,roomNum,fileName,wallNum,percentage):
		room = self._RoomList[roomNum]
		num = room.AddDummyWindow(fileName,wallNum,percentage)
		
		return num
		
	#隣接する壁に窓の穴を空ける
	def AddWindowHole(self,roomNum,windowNum,fileName):
		state = False
		resRoom = None
		resWall = None
		resPerc = None
		
		#間取りを取得
		room = self._RoomList[roomNum]
		roomPos = room.GetFloorPosition()
		#print 'roomPos',roomPos
		
		#壁を取得
		windowList = room.GetWindowList()
		windowWallNum = windowList[windowNum][2]
		
		baseWallPointList = room.GetWallPointList()
		
		baseWallPointPosMin = baseWallPointList[windowWallNum]
		#print 'baseWallPointPosMin',baseWallPointPosMin
		
		baseWallPointGlobalPosMin = [0.0,0.0,0.0]
		baseWallPointGlobalPosMin[0] = roomPos[0] + baseWallPointPosMin[0]
		baseWallPointGlobalPosMin[2] = roomPos[2] + baseWallPointPosMin[1]
		
		windowWallNum2 = windowWallNum+1
		if windowWallNum2 == len(baseWallPointList):
			windowWallNum2 = 0
			
		baseWallPointPosMax = baseWallPointList[windowWallNum2]
		#print 'baseWallPointPosMax',baseWallPointPosMax
		
		baseWallPointGlobalPosMax = [0.0,0.0,0.0]
		baseWallPointGlobalPosMax[0] = roomPos[0] + baseWallPointPosMax[0]
		baseWallPointGlobalPosMax[2] = roomPos[2] + baseWallPointPosMax[1]
		
		#print 'baseWallPointGlobalPosMin',baseWallPointGlobalPosMin
		#print 'baseWallPointGlobalPosMax',baseWallPointGlobalPosMax
		
		baseWallVec = [baseWallPointGlobalPosMax[0]-baseWallPointGlobalPosMin[0],baseWallPointGlobalPosMax[2]-baseWallPointGlobalPosMin[2]]
		baseWallLong = room.GetWallSize(windowWallNum)
		baseWallNorm = [baseWallVec[0]/baseWallLong,baseWallVec[1]/baseWallLong]
		
		#窓を取得
		window = room.GetWindowModel(windowNum)
		windowPos = window.GetPosition()
		#print 'windowPos',windowPos
		windowHeight = room.GetWindowHeight(windowNum)
		#print 'windowPos',windowHeight
		
		windowGlobalPos = [0.0,0.0,0.0]
		windowGlobalPos[0] = windowPos[0]
		windowGlobalPos[1] = 0.0
		windowGlobalPos[2] = windowPos[2]
		#print 'windowGlobalPos',windowGlobalPos
		
		#部屋の数だけ繰り返す
		for x in range(len(self._RoomList)):
			#print 'x',x
			
			if x != roomNum:
				otherRoom = self._RoomList[x]
				otehrRoomPos = otherRoom.GetFloorPosition()
				#print 'otehrRoomPos',otehrRoomPos
				
				wallPointList = otherRoom.GetWallPointList()
				
				#壁の数だけ繰り返す
				for y in range(len(wallPointList)):
					#print 'y',y
					
					wallPointPosMin = wallPointList[y]
					#print 'wallPointPosMin',wallPointPosMin
					
					wallPointGlobalPosMin = [0.0,0.0,0.0]
					wallPointGlobalPosMin[0] = otehrRoomPos[0] + wallPointPosMin[0]
					wallPointGlobalPosMin[1] = 0.0
					wallPointGlobalPosMin[2] = otehrRoomPos[2] + wallPointPosMin[1]
					
					y2 = y+1
					if y2 == len(wallPointList):
						y2 = 0
						
					wallPointPosMax = wallPointList[y2]
					#print 'wallPointPosMax',wallPointPosMax
					
					wallPointGlobalPosMax = [0.0,0.0,0.0]
					wallPointGlobalPosMax[0] = otehrRoomPos[0] + wallPointPosMax[0]
					wallPointGlobalPosMax[1] = 0.0
					wallPointGlobalPosMax[2] = otehrRoomPos[2] + wallPointPosMax[1]
					
					wallVec = [wallPointGlobalPosMax[0]-wallPointGlobalPosMin[0],wallPointGlobalPosMax[2]-wallPointGlobalPosMin[2]]
					wallLong = otherRoom.GetWallSize(y)
					wallNorm = [wallVec[0]/wallLong,wallVec[1]/wallLong]
					
					#内積を求める
					dot = baseWallNorm[0] * wallNorm[0] + baseWallNorm[1] * wallNorm[1]
					
					#壁同士の傾きが近い場合は距離を確認
					if dot < -0.99 or dot > 0.99:
						#壁の始点=A、壁の終点=B、窓=P、面積=d、壁上の窓に近い位置=X
						vecAP = [windowGlobalPos[0]-wallPointGlobalPosMin[0],windowGlobalPos[2]-wallPointGlobalPosMin[2]]
						disAX = wallNorm[0] * vecAP[0] + wallNorm[1] * vecAP[1]
						
						d = math.fabs(wallVec[0]*vecAP[1] - wallVec[1]*vecAP[0])	#面積
						
						nearestDistance = d / wallLong
						nearestPerc = disAX / wallLong
						#print 'nearestDistance',nearestDistance
						#print 'nearestPerc',nearestPerc
						
						if nearestDistance < 0.16:	#15cm以内の場合はそこに穴を空ける
							if nearestPerc > 0.0005 and nearestPerc < 0.9995:
								#print 'CreateWindowHole'
								state = True
								resRoom = x
								resWall = y
								resPerc = nearestPerc
								#print 'distance',nearestDistance
								#print 'room',resRoom
								#print 'wall',resWall
								#print 'perc',resPerc
								
					#壁が窓の近くにあったら穴を空ける
					if state:
						dummyWindowNum = self.AddDummyWindow(resRoom,fileName,resWall,resPerc)
						self.SetDummyWindowHeight(resRoom,dummyWindowNum,windowHeight)
						
						dummyWindowData = [resRoom,dummyWindowNum,roomNum,windowNum,windowHeight]
						self._DummyWindowStateList.append(dummyWindowData)
						
						state = False
						
	#全てのに対して隣接する壁に窓の穴を空ける
	def AddAllWindowHole(self):
		for roomNum in range(len(self._RoomList)):
			room = self._RoomList[roomNum]
			windowCount = room.GetWindowCount()
			for windowNum in range(windowCount):
				window = room.GetWindowModel(windowNum)
				if window.GetDummyFlag() == False:
					windowFileName = window.GetFileName()
					self.AddWindowHole(roomNum,windowNum,windowFileName)
		
		#print 'AddAllWindowHole',self._DummyWindowStateList
		
	#窓の穴を削除
	def DeleteWindowHole(self,roomNum,windowNum):
		#ドアを削除
		if roomNum != None and windowNum != None:
			state = False
			dummyRoomNum = None
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[2] == roomNum and dummyWindowData[3] == windowNum:
					self.DeleteDummyWindow(dummyWindowData[0],dummyWindowData[1])
					dummyRoomNum = dummyWindowData[0]
					self._DummyWindowStateList[x] = None
					state = True
					break
		
			if state:
				room = self._RoomList[dummyRoomNum]
				room.ReCreateWall()
				
				for x in range(len(self._DummyWindowStateList)):
					dummyWindowData = self._DummyWindowStateList[x]
					if dummyWindowData != None:
						if dummyWindowData[0] == dummyRoomNum and dummyWindowData[1] > windowNum:
							dummyWindowData[1] = dummyWindowData[1] - 1
						
						if dummyWindowData[2] == roomNum and dummyWindowData[3] > windowNum:
							dummyWindowData[3] = dummyWindowData[3] - 1
			
		#間取りを削除				
		else:
			state = False
			dummyRoomNumList = []
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[0] == roomNum or dummyWindowData[2] == roomNum:
					self.DeleteDummyWindow(dummyWindowData[0],0)
						
					dummyRoomNumList.append(dummyWindowData[0])
					self._DummyWindowStateList[x] = None
					state = True
					
			if state:
				for x in range(len(dummyRoomNumList)):
					room = self._RoomList[dummyRoomNumList[x]]
					room.ReCreateWall()
					
				for x in range(len(self._DummyWindowStateList)):
					dummyWindowData = self._DummyWindowStateList[x]
					if dummyWindowData != None:
						if dummyWindowData[0] > roomNum:
							dummyWindowData[0] = dummyWindowData[0] - 1
						if dummyWindowData[2] > roomNum:
							dummyWindowData[2] = dummyWindowData[2] - 1
					
		newList = []
		for x in range(len(self._DummyWindowStateList)):
			dummyWindowData = self._DummyWindowStateList[x]
			if dummyWindowData != None:
				newList.append(dummyWindowData)
		
		self._DummyWindowStateList = newList
		
	#窓の穴を全て削除
	def DeleteAllWindowHole(self):
		#print 'DeleteAllWindowHole',self._DummyWindowStateList
		
		count = len(self._DummyWindowStateList)
		if count != 0:
			dummyRoomNumList = []
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData != None:
					self.DeleteDummyWindow(dummyWindowData[0],dummyWindowData[1])
					dummyRoomNumList.append(dummyWindowData[0])
					self._DummyWindowStateList[x] = None
		
					for y in range(len(self._DummyWindowStateList)):
						if y > x:
							dummyWindowData2 = self._DummyWindowStateList[y]
							if dummyWindowData2[0] == dummyWindowData[0] and dummyWindowData2[1] > dummyWindowData[1]:
								dummyWindowData2[1] = dummyWindowData2[1] - 1
								self._DummyWindowStateList[y] = dummyWindowData2
								
			for x in range(len(dummyRoomNumList)):
				room = self._RoomList[dummyRoomNumList[x]]
				room.ReCreateWall()
		
		self._DummyWindowStateList = []
		
	#窓の削除
	def DeleteWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			self.DeleteWindowHole(roomNum,num)		#ダミー窓を削除
			
			room.DeleteWindow(num)
		
			#ダミー窓のリストを更新
			for x in range(len(self._DummyWindowStateList)):
				dummyWindowData = self._DummyWindowStateList[x]
				if dummyWindowData[0] == roomNum and dummyWindowData[1] > num:
					dummyWindowData[1] = dummyWindowData[1] - 1
					self._DummyWindowStateList[x] = dummyWindowData
					
			self._ModifyState = True
		
	#ダミー窓の削除
	def DeleteDummyWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.DeleteWindow(num)
		
	#窓のコピー
	def CopyWindow(self,roomNum,num):
		room = self._RoomList[roomNum]
		if num != None:
			room.CopyWindow(num)
			
			self._ModifyState = True
		
	#窓の位置設定
	def SetWindowPosition(self,roomNum,num,wallNum,percentage):
		room = self._RoomList[roomNum]
		if num != None and wallNum != None:	
			room.SetWindowPosition(num,wallNum,percentage)
			
			self._ModifyState = True
		
	#窓のラインの表示状態を設定
	def SetWindowLineVisible(self,state):
		for Room in self._RoomList:
			Room.SetWindowLineVisible(state)
		
	#窓のハイライト表示
	def SetHighlightWindow(self,roomNum,modelNum,state):
		room = self._RoomList[roomNum]
		room.SetHighlightWindow(modelNum,state)
		
	#窓モデルから番号を取得
	def GetWindowNum(self,model):
		roomNum = None
		modelNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			modelNum = room.GetWindowNum(model)
			if modelNum != None:
				roomNum = x
				break
		
		return roomNum,modelNum
		
	#窓モデルを取得
	def GetWindowModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		model = room.GetWindowModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetWindowLineModel(self,roomNum,num):
		room = self._RoomList[roomNum]
		lineModel = room.GetWindowLineModel(num)
		
		return lineModel
		
	#窓選択可能状態を設定
	def SetWindowPickable(self,state):
		for room in self._RoomList:
			room.SetWindowPickable(state)
			
	#窓のリストを更新
	def UpdateWindowList(self,RoomNum,num,state,height,wallNum,percentage):
		room = self._RoomList[RoomNum]
		room.UpdateWindowList(num,state,height,wallNum,percentage)
	
	#天井の表示状態を設定
	def SetCeilingVisible(self,state):
		for room in self._RoomList:
			room.SetCeilingVisible(state)
	
	#床のハイライト表示
	def SetHighlightFloor(self,RoomNum,state):
		room = self._RoomList[RoomNum]
		room.SetHighlightFloor(state)
		
	#床モデルを取得
	def GetFloorModel(self,roomNum):
		room = self._RoomList[roomNum]
		model = room.GetFloorModel()
		
		return model
		
	#床のテクスチャを設定
	def SetFloorTexture(self,RoomNum,texture,filePath):
		room = self._RoomList[RoomNum]
		room.SetFloorTexture(texture,filePath)
		
		self._DefaultFloorTexPath = filePath
		
		self._ModifyState = True
		
	#床のテクスチャを取得
	def GetFloorTexture(self,RoomNum):
		room = self._RoomList[RoomNum]
		texture = room.GetFloorTexture()
		
		return texture
		
	#床モデルから部屋の番号を取得
	def GetRoomNum(self,model):
		roomNum = None
		
		for x in range(len(self._RoomList)):
			room = self._RoomList[x]
			floorModel = room.GetFloorModel()
			if model == floorModel:
				roomNum = x
				break
		
		return roomNum
		
	#壁の高さを設定
	def SetWallHeight(self,roomNum,wallHeight):
		room = self._RoomList[roomNum]
		room.SetWallHeight(wallHeight)
	
		self._DefaultWallHight = wallHeight
		
		self._ModifyState = True
		
	#壁のテクスチャを設定
	def SetWallTexture(self,roomNum,texture,filePath):
		room = self._RoomList[roomNum]
		room.SetWallTexture(texture,filePath)
		
		self._DefaultWallTexPath = filePath
		
		self._ModifyState = True
		
	#壁のテクスチャを取得
	def GetWallTexture(self,roomNum):
		room = self._RoomList[roomNum]
		texture = room.GetWallTexture()
		
		return texture
		
	#窓の高さを設定
	def SetWindowHeight(self,roomNum,num,height):
		room = self._RoomList[roomNum]
		room.SetWindowHeight(num,height)
		
		#ダミー窓の高さを設定
		for x in range(len(self._DummyWindowStateList)):
			dummyWindowData = self._DummyWindowStateList[x]
			if dummyWindowData[2] == roomNum and dummyWindowData[3] == num:
				self.SetDummyWindowHeight(dummyWindowData[0],dummyWindowData[1],height)
				dummyWindowData[4] = height
				self._DummyWindowStateList[x] = dummyWindowData
		
				self._ModifyState = True
		
	#ダミー窓の高さを設定
	def SetDummyWindowHeight(self,roomNum,num,height):
		room = self._RoomList[roomNum]
		room.SetWindowHeight(num,height)
		
	#窓の高さを取得
	def GetWindowHeight(self,roomNum,num):
		room = self._RoomList[roomNum]
		height = room.GetWindowHeight(num)
		
		return height
		
	#壁のポイントを追加
	def AddWallPoint(self,roomNum,num):
		room = self._RoomList[roomNum]
		room.AddWallPoint(num)
		
		self._ModifyState = True
		
	#壁のポイントを削除
	def RemoveWallPoint(self,roomNum,num):
		room = self._RoomList[roomNum]
		room.RemoveWallPoint(num)
		
		self._ModifyState = True
		
	#壁のポイントの数を取得
	def GetWallPointCount(self,roomNum):
		room = self._RoomList[roomNum]
		pointCount = room.GetWallPointCount()
		return pointCount
		
	#床の位置設定
	def SetFloorPosition(self,roomNum,position):
		room = self._RoomList[roomNum]
		room.SetFloorPosition(position)
		
		self._ModifyState = True
		
	#床の位置取得
	def GetFloorPosition(self,roomNum):
		room = self._RoomList[roomNum]
		position = room.GetFloorPosition()
		
		return position
		
	#天井のテクスチャを設定
	def SetCeilingTexture(self,RoomNum,texture,filePath):
		room = self._RoomList[RoomNum]
		room.SetCeilingTexture(texture,filePath)
		
		self._DefaultCeilingTexPath = filePath
		
		self._ModifyState = True
		
	#天井のテクスチャを取得
	def GetCeilingTexture(self,RoomNum):
		room = self._RoomList[RoomNum]
		texture = room.GetCeilingTexture()
		
		return texture
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetDoorModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = []
		
		for room in self._RoomList:
			list = room.GetCollisionModelList()
			
			for model in list:
				modelList.append(model)
		
		return modelList
		
	#スタート位置を設定
	def SetStartPosition(self):
		startPosTemp = self._RoomIniFile.GetItemData('Define','satartposition')
		startPos = [0,0,0]
		if startPosTemp != None:
			startPos[0] = float(startPosTemp.split(',')[0])
			startPos[1] = float(startPosTemp.split(',')[1])
			startPos[2] = float(startPosTemp.split(',')[2])
		
		startAngleTemp = self._RoomIniFile.GetItemData('Define','satartangle')
		startAngle = [0,0,0]
		if startAngleTemp != None:
			startAngle[0] = float(startAngleTemp.split(',')[0])
			startAngle[1] = float(startAngleTemp.split(',')[1])
			startAngle[2] = float(startAngleTemp.split(',')[2])
		
		self._StartPosition = startPos
		self._StartAngle = startAngle
		
		"""
		#スタート位置のモデルを設定
		if startPos == [0,0,0]:
			self._StartPositionClass.Reset()
		else:
			self._StartPositionClass.SetPosition(startPos)
		self._StartPositionClass.SetOrientation(startAngle[0])
		
		self._ModifyState = True
		"""
		
	#スタート位置をモデルから設定
	def SetStartPositionFromModel(self):
		pos = self._StartPositionClass.GetPosition()
		self._StartPosition = pos
		
		angle = [0,0,0]
		angle[0] = self._StartPositionClass.GetOrientation()
		self._StartAngle = angle
		
	#スタート位置を取得
	def GetStartPosition(self):
		startPos = self._StartPosition
		startAngle = self._StartAngle
		
		return startPos,startAngle
		
	#INIファイル名を取得
	def GetIniFileName(self):
		val = self._RoomIniFileName
		
		return val
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
		for room in self._RoomList:
			room.SetViewState(state)
		
	#壁の表示状態設定
	def SetWallVisible(self,roomNum,wallNum,state):
		room = self._RoomList[roomNum]
		room.SetWallVisible(wallNum,state)
		
		self._ModifyState = True
		
	#壁の表示状態取得
	def GetWallVisible(self,roomNum,wallNum):
		room = self._RoomList[roomNum]
		state = room.GetWallVisible(wallNum)
		
		return state
		
	#編集状態を取得
	def GetModifyState(self):
		state =	self._ModifyState
		return state
		
	#追加時のテクスチャ、高さ設定情報を取得
	def GetDefaultSettings(self):
		return self._DefaultFloorTexPath,self._DefaultWallTexPath,self._DefaultCeilingTexPath,self._DefaultWallHight
		
	#図面を設定
	def SetDrawing(self):
		fileNameTemp = self._RoomIniFile.GetItemData('Define','drawingfile')
		if fileNameTemp != None:
			self._DrawingFile = fileNameTemp
		
		scaleTemp = self._RoomIniFile.GetItemData('Define','drawingscale')
		if scaleTemp != None:
			self._DrawingScale = float(scaleTemp)
		
		#図面を設定
		if self._DrawingFile != '' and self._DrawingFile != None:
			self._Drawing.Add(self._DrawingFile)
			self._Drawing.SetScale(self._DrawingScale)
			
		self._ModifyState = True
		
	#図面を取得
	def GetDrawing(self):
		self._DrawingFile = self._Drawing.GetFileName()
		self._DrawingScale = self._Drawing.GetScale()
		
	#図面を削除
	def DeleteDrawing(self):
		if self._Drawing != None:
			self._Drawing.Delete()
			del self._Drawing
		
		self._Drawing = None	
			
	#図面の表示状態を設定
	def SetDrawingVisible(self,state):
		self._Drawing.SetVisible(state)
			
	#図面の表示状態を取得
	def GetDrawingVisible(self):
		return self._Drawing.GetVisible()
		
	#図面のモデルを取得
	def GetDrawingModel(self):
		return self._Drawing.GetDrawingModel()
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		scale = self._Drawing.GetDrawingScale()
		return scale
	
	#天井の表示切り替え
	def CeilingVisibleUpdate(self):
		viewPos = viz.MainWindow.getView().getPosition()
		
		for room in self._RoomList:
			if viewPos[1] < room.GetWallHeight():
				room.SetCeilingVisible(True)
			else:
				room.SetCeilingVisible(False)
				
# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋制御のクラス定義
class Room(Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Room] : Init Room')
		
		self._RoomIniFile = None
		
		#部屋の設定
		self._Name = ""
		self._Position = [0.0,0.0,0.0]
		self._Angle = 0.0
		self._WallHeight = 3.0
		
		#形状関係
		self._FloorPointList = []
		
		#床関係
		self._FloorTextureFile = ""
		self._FloorTextureSize = 1.0
		
		self._FloorClass = None
		
		#壁関係
		self._WallModelList = []
		self._WallTextureFile = ""
		self._WallTextureSize = 1.0
		self._WallVisibleList = []
		
		self._WallClass = None
		self._WallLineClass = None
		self._WallPointClass = None
		
		#天井関係
		self._CeilingTextureFile = ""
		self._CeilingTextureSize = 1.0
		
		self._CeilingClass = None
		
		#ドア関係
		self._DoorList = []
		
		self._DoorControlClass = None
		
		#ドア関係
		self._WindowList = []
		
		self._WindowControlClass = None
		
		#天井の表示切り替え用
		self._Is3d = False
		
		#新規追加時の設定
		self._DefaultFloorTexPath = ''
		self._DefaultWallTexPath = ''
		self._DefaultCeilingTexPath = ''
		self._DefaultWallHight = 3.0
		
		self._IsOpRoom = 0
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Room] : Start Room Update')
		self._Is3d = True
		#天井の表示切り替え
		if self._Is3d:
			#viewPos = viz.MainView.getPosition()
			viewPos = viz.MainWindow.getView().getPosition()
			if viewPos[1] < self._WallHeight:
				self.SetCeilingVisible(True)
			else:
				self.SetCeilingVisible(False)
		else:
			self.SetCeilingVisible(False)
		
	#作成
	def Create(self,iniFile,roomName,add=False):
		self._RoomIniFile = iniFile
		self._Name = roomName
		
		self.LoadIniFileValue()
		
		if add:
			if self._DefaultFloorTexPath != None and self._DefaultFloorTexPath != '':
				self._FloorTextureFile = self._DefaultFloorTexPath
			if self._DefaultWallTexPath != None and self._DefaultWallTexPath != '':
				self._WallTextureFile = self._DefaultWallTexPath
			if self._DefaultCeilingTexPath != None and self._DefaultCeilingTexPath != '':
				self._CeilingTextureFile = self._DefaultCeilingTexPath
			if self._DefaultWallHight != None:
				self._WallHeight = self._DefaultWallHight
			
		self._FloorClass = Floor(self._Position,self._FloorPointList,self._FloorTextureFile,self._FloorTextureSize)
		self._WallClass = Wall(self._Position,self._FloorPointList,self._DoorList,self._WindowList,self._WallTextureFile,self._WallTextureSize,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		#self._WallLineClass = WallLine(self._Position,self._FloorPointList,self._WallHeight,self._WallVisibleList)
		#self._WallPointClass = WallPoint(self._Position,self._FloorPointList,self._WallHeight)
		self._CeilingClass = Ceiling(self._Position,self._FloorPointList,self._CeilingTextureFile,self._CeilingTextureSize,self._WallHeight)
		self._DoorControlClass = DoorControl(self._Position,self._FloorPointList,self._DoorList,self._WallHeight)
		self._WindowControlClass = WindowControl(self._Position,self._FloorPointList,self._WindowList,self._WallHeight)
		
		self._DoorControlClass.AddClass(self._WallClass)
		self._DoorControlClass.SetDoor()
		
		self._WindowControlClass.AddClass(self._WallClass)
		self._WindowControlClass.SetWindow()
		
		self._WallClass.AddClass(self._DoorControlClass,self._WindowControlClass)
		self._WallClass.Create()
		
		self.SetCeilingVisible(True)
		
	#設定ファイルの読み込み
	def LoadIniFileValue(self):
		
		#全体のパラメータ設定
		positionTemp = self._RoomIniFile.GetItemData(self._Name,'Position')
		self._Position[0] = float(positionTemp.split(',')[0])
		self._Position[1] = float(positionTemp.split(',')[1])
		self._Position[2] = float(positionTemp.split(',')[2])
		
		self._Angle = float(self._RoomIniFile.GetItemData(self._Name,'Angle'))
		
		self._FloorPointList = []
		self._WallVisibleList = []
		for x in range(100):
			pointName = "Point"+str(x)
			pointTemp = self._RoomIniFile.GetItemData(self._Name,pointName)
			if pointTemp != None:
				pointX = float(pointTemp.split(',')[0])
				pointY = float(pointTemp.split(',')[1])
				point = [pointX,pointY]
				self._FloorPointList.append(point)
				
				visibleState = True
				if len(pointTemp.split(',')) == 3:
					visibleStateTemp = int(pointTemp.split(',')[2])
					if visibleStateTemp == 0:
						visibleState = False
					elif visibleStateTemp == 1:
						visibleState = True
						
				self._WallVisibleList.append(visibleState)
				
			else:
				break
				
		self._WallHeight = float(self._RoomIniFile.GetItemData(self._Name,'WallHeight'))
		
		#床関係のパラメータ設定
		floorTextureTemp = self._RoomIniFile.GetItemData(self._Name,'FloorTexture')
		self._FloorTextureFile = floorTextureTemp.split(',')[0]
		self._FloorTextureSize = float(floorTextureTemp.split(',')[1])
	
		#壁関係のパラメータ設定
		wallTextureTemp = self._RoomIniFile.GetItemData(self._Name,'WallTexture')
		self._WallTextureFile = wallTextureTemp.split(',')[0]
		self._WallTextureSize = float(wallTextureTemp.split(',')[1])
		
		#天井関係のパラメータ設定
		ceilingTextureTemp = self._RoomIniFile.GetItemData(self._Name,'CeilingTexture')
		self._CeilingTextureFile = ceilingTextureTemp.split(',')[0]
		self._CeilingTextureSize = float(ceilingTextureTemp.split(',')[1])
		
		#ドア関係のパラメータ設定
		self._DoorList = []
		for x in range(100):
			doorName = "Door"+str(x)
			doorTemp = self._RoomIniFile.GetItemData(self._Name,doorName)
			if doorTemp != None:
				doorModelName = doorTemp.split(',')[0]
				doorWallNum = int(doorTemp.split(',')[1])
				doorPercentage = float(doorTemp.split(',')[2])
				door = [doorModelName,doorWallNum,doorPercentage]
				self._DoorList.append(door)
			else:
				break
		
		#窓関係のパラメータ設定
		self._WindowList = []
		for x in range(100):
			windowName = "Window"+str(x)
			windowTemp = self._RoomIniFile.GetItemData(self._Name,windowName)
			if windowTemp != None:
				windowModelName = windowTemp.split(',')[0]
				windowHeight = float(windowTemp.split(',')[1])
				windowWallNum = int(windowTemp.split(',')[2])
				windowPercentage = float(windowTemp.split(',')[3])
				window = [windowModelName,windowHeight,windowWallNum,windowPercentage]
				self._WindowList.append(window)
			else:
				break
		
		#オペ室かどうのパラメータ設定
		opRoomStateTemp = self._RoomIniFile.GetItemData(self._Name,'OpRoom')
		self._IsOpRoom = 0
		if opRoomStateTemp != None and opRoomStateTemp != '':
			self._IsOpRoom = int(opRoomStateTemp)
		
	#削除
	def Delete(self):
		self._FloorClass.Delete()
		self._WallClass.Delete()
		#self._WallLineClass.Delete()
		#self._WallPointClass.Delete()
		self._CeilingClass.Delete()
		self._DoorControlClass.DeleteAll()
		self._WindowControlClass.DeleteAll()
		
		del self._RoomIniFile
		self._RoomIniFile = None
		
	#壁のラインの表示状態を設定
	def SetWallLineVisible(self,state):
		self._WallLineClass.SetVisible(state)
	
	#壁のラインのハイライト表示
	def SetHighlightWallLine(self,num,state):
		self._WallLineClass.SetHighlight(num,state)
		
	#壁のラインモデルから番号を取得
	def GetWallLineNum(self,model):
		num = self._WallLineClass.GetModelNum(model)	
		
		return num
		
	#壁のラインモデルを取得
	def GetWallLineModel(self,num):
		model = self._WallLineClass.GetModel(num)
		
		return model
		
	#壁の点の表示状態を設定
	def SetWallPointVisible(self,state):
		self._WallPointClass.SetVisible(state)
	
	#壁の点のハイライト表示
	def SetHighlightWallPoint(self,num,state):
		self._WallPointClass.SetHighlight(num,state)
		
	#壁の点モデルから番号を取得
	def GetWallPointNum(self,model):
		num = self._WallPointClass.GetModelNum(model)	
		
		return num
		
	#壁の点モデルを取得
	def GetWallPointModel(self,num):
		model = self._WallPointClass.GetModel(num)
		
		return model
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		self._FloorClass.SetPickable(state)
		self._WallClass.SetPickable(state)
		self._WallLineClass.SetPickable(state)
		self._WallPointClass.SetPickable(state)
		self._DoorControlClass.SetPickableForAllModel(state)
		self._WindowControlClass.SetPickableForAllModel(state)
	
	#壁のラインの位置設定
	def SetWallLinePosition(self,num,position):
		self._WallLineClass.SetPosition(num,position)
		self._FloorPointList = self._WallLineClass.GetFloorPointList()
		
		self.SetCenterPosition()
		
	#壁のラインの位置指定時のオフセット設定
	def SetWallLinePositionOffset(self,num,position):
		self._WallLineClass.SetPositionOffset(num,position)
		
	#壁の点の位置設定
	def SetWallPointPosition(self,num,position):
		state = True
		for x in range(len(self._FloorPointList)):
			point = self._FloorPointList[x]
			if x != num and position[0] == point[0] and position[2] == point[1]:
				state = False
		
		if state:
			self._WallPointClass.SetPosition(num,position)
			self._FloorPointList = self._WallPointClass.GetFloorPointList()
			
			self.SetCenterPosition()
			
	#壁の点の位置指定時のオフセット設定
	def SetWallPointPositionOffset(self,num,position):
		self._WallPointClass.SetPositionOffset(num,position)
		
	#ドア追加
	def AddDoor(self,fileName,wallNum,percentage):
		num,doorList = self._DoorControlClass.Add(fileName,wallNum,percentage)
		self._DoorList = doorList
		
		self._WallClass.SetDoor(self._DoorList)
		
		return num
	
	#ダミードア追加（穴を空ける）
	def AddDummyDoor(self,fileName,wallNum,percentage):
		num,doorList = self._DoorControlClass.AddDummy(fileName,wallNum,percentage)
		self._DoorList = doorList
		
		self._WallClass.SetDoor(self._DoorList)
		
		return num
	
	#ドアの移動による、壁の再作成
	def ReCreateWall(self):
		self._WallClass.SetDoor(self._DoorList)
	
	#ドアの削除
	def DeleteDoor(self,num):
		if num != None:
			doorList = self._DoorControlClass.Delete(num)
			self._DoorList = doorList
			
			self._WallClass.SetDoor(self._DoorList)
		
	#ドアの追加
	def CopyDoor(self,num):
		if num != None:
			doorList = self._DoorControlClass.Copy(num)
			self._DoorList = doorList
			
			self._WallClass.SetDoor(self._DoorList)
			
	#ドアの位置設定
	def SetDoorPosition(self,num,wallNum,percentage):
		if num != None and wallNum != None:	
			self._DoorControlClass.SetPosition(num,wallNum,percentage)
			
	#壁の両端の位置を取得
	def GetWallPoint(self,num):
		point0,point1 = self._WallClass.GetPoint(num)
		
		return point0,point1
		
	#ドアのラインの表示状態を設定
	def SetDoorLineVisible(self,state):
		self._DoorControlClass.SetLineVisible(state)
		
	#壁の高さ取得
	def GetWallHeight(self):
		wallHeight = self._WallHeight
		
		return wallHeight
	
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
	
		self._WallClass.SetWallHeight(self._WallHeight)
		self._WallLineClass.SetWallHeight(self._WallHeight)
		self._WallPointClass.SetWallHeight(self._WallHeight)
		self._CeilingClass.SetWallHeight(self._WallHeight)
		self._DoorControlClass.SetWallHeight(self._WallHeight)
		self._WindowControlClass.SetWallHeight(self._WallHeight)
		
		self._WallClass.Move(self._FloorPointList)
		self._WallLineClass.Move(self._FloorPointList)
		self._WallPointClass.Move(self._FloorPointList)
		self._DoorControlClass.Move(self._FloorPointList)
		self._WindowControlClass.Move(self._FloorPointList)
		
	#ドアのハイライト表示
	def SetHighlightDoor(self,num,state):
		self._DoorControlClass.SetHighlight(num,state)
		
	#ドアモデルから番号を取得
	def GetDoorNum(self,model):
		num = self._DoorControlClass.GetModelNum(model)	
		
		return num
		
	#ドアモデルを取得
	def GetDoorModel(self,num):
		model = self._DoorControlClass.GetModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetDoorLineModel(self,num):
		lineModel = self._DoorControlClass.GetLineModel(num)
		
		return lineModel
		
	#ドア選択可能状態を設定
	def SetDoorPickable(self,state):
		self._DoorControlClass.SetPickableForAllModel(state)
			
	#ドアのリストを更新
	def UpdateDoorList(self,num,wallNum,percentage):
		self._DoorList = self._DoorControlClass.UpdateList(num,wallNum,percentage)
	
	#窓追加
	def AddWindow(self,fileName,wallNum,percentage):
		num,windowList = self._WindowControlClass.Add(fileName,wallNum,percentage)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
		return num
	
	#ダミー窓追加（穴を空ける）
	def AddDummyWindow(self,fileName,wallNum,percentage):
		num,windowList = self._WindowControlClass.AddDummy(fileName,wallNum,percentage)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
		return num
	
	#窓の削除
	def DeleteWindow(self,num):
		if num != None:
			windowList = self._WindowControlClass.Delete(num)
			self._WindowList = windowList
			
			self._WallClass.SetWindow(self._WindowList)
		
	#窓の追加
	def CopyWindow(self,num):
		if num != None:
			windowList = self._WindowControlClass.Copy(num)
			self._WindowList = windowList
			
			self._WallClass.SetWindow(self._WindowList)
			
	#窓の位置設定
	def SetWindowPosition(self,num,wallNum,percentage):
		if num != None and wallNum != None:	
			self._WindowControlClass.SetPosition(num,wallNum,percentage)
		
	#窓のラインの表示状態を設定
	def SetWindowLineVisible(self,state):
		self._WindowControlClass.SetLineVisible(state)
		
	#窓のハイライト表示
	def SetHighlightWindow(self,num,state):
		self._WindowControlClass.SetHighlight(num,state)
		
	#窓モデルから番号を取得
	def GetWindowNum(self,model):
		num = self._WindowControlClass.GetModelNum(model)	
		
		return num
		
	#窓モデルを取得
	def GetWindowModel(self,num):
		model = self._WindowControlClass.GetModel(num)
		
		return model
		
	#窓のラインモデルを取得
	def GetWindowLineModel(self,num):
		lineModel = self._WindowControlClass.GetLineModel(num)
		
		return lineModel
		
	#窓選択可能状態を設定
	def SetWindowPickable(self,state):
		self._WindowControlClass.SetPickableForAllModel(state)
			
	#窓のリストを更新
	def UpdateWindowList(self,num,state,height,wallNum,percentage):
		self._WindowList = self._WindowControlClass.UpdateList(num,state,height,wallNum,percentage)
	
	#天井の表示状態を設定
	def SetCeilingVisible(self,state):
		self._CeilingClass.SetVisible(state)
	
	#床のハイライト表示
	def SetHighlightFloor(self,state):
		for x in range(len(self._FloorPointList)):
			self._WallLineClass.SetHighlight(x,state)
			self._WallPointClass.SetHighlight(x,state)
		
	#床モデルを取得
	def GetFloorModel(self):
		model = self._FloorClass.GetModel()
		
		return model
		
	#床の位置設定
	def SetFloorPosition(self,position):
		self._Position = position
		
		self._FloorClass.SetFloorPosition(self._Position)
		self._WallClass.SetFloorPosition(self._Position)
		self._WallLineClass.SetFloorPosition(self._Position)
		self._WallPointClass.SetFloorPosition(self._Position)
		self._CeilingClass.SetFloorPosition(self._Position)
		self._DoorControlClass.SetFloorPosition(self._Position)
		self._WindowControlClass.SetFloorPosition(self._Position)
		
	#床の位置取得
	def GetFloorPosition(self):
		position = self._Position
		
		return position
		
	#床のテクスチャを設定
	def SetFloorTexture(self,texture,filePath):
		self._FloorClass.SetTexture(texture)
		self._FloorTextureFile = filePath
		
	#床のテクスチャを取得
	def GetFloorTexture(self):
		texture = self._FloorClass.GetTexture()
		
		return texture
		
	#床のテクスチャのファイル名とサイズを取得
	def GetFloorTextureSetting(self):
		textureFile = self._FloorTextureFile
		textureSize = self._FloorTextureSize
		
		return textureFile,textureSize
		
	#壁のテクスチャを設定
	def SetWallTexture(self,texture,filePath):
		self._WallClass.SetTexture(texture)
		self._WallTextureFile = filePath
		
	#壁のテクスチャを取得
	def GetWallTexture(self):
		texture = self._WallClass.GetTexture()
		
		return texture
		
	#壁のテクスチャのファイル名とサイズを取得
	def GetWallTextureSetting(self):
		textureFile = self._WallTextureFile
		textureSize = self._WallTextureSize
		
		return textureFile,textureSize
		
	#天井のテクスチャを設定
	def SetCeilingTexture(self,texture,filePath):
		self._CeilingClass.SetTexture(texture)
		self._CeilingTextureFile = filePath
		
	#天井のテクスチャを取得
	def GetCeilingTexture(self):
		texture = self._CeilingClass.GetTexture()
		
		return texture
		
	#天井のテクスチャのファイル名とサイズを取得
	def GetCeilingTextureSetting(self):
		textureFile = self._CeilingTextureFile
		textureSize = self._CeilingTextureSize
		
		return textureFile,textureSize
		
	#窓の高さを設定
	def SetWindowHeight(self,num,height):
		windowList = self._WindowControlClass.SetWindowHeight(num,height)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
	#窓の高さを取得
	def GetWindowHeight(self,num):
		height = self._WindowControlClass.GetWindowHeight(num)
		
		return height
		
	#壁のポイントを追加
	def AddWallPoint(self,num):
		floorPointList,doorList,windowList = self._WallClass.AddPoint(num)
		wallVisibleList = self._WallClass.GetVisibleList()
		
		self._FloorPointList = floorPointList
		self._DoorList = doorList
		self._WindowList = windowList
		self._WallVisibleList = wallVisibleList
		
		self._WallLineClass.SetVisibleList(self._WallVisibleList)
		
		self._FloorClass.SetPointList(self._FloorPointList)
		self._WallLineClass.SetPointList(self._FloorPointList)
		self._WallPointClass.SetPointList(self._FloorPointList)
		self._CeilingClass.SetPointList(self._FloorPointList)
		self._DoorControlClass.SetPointList(self._FloorPointList,self._DoorList)
		self._WindowControlClass.SetPointList(self._FloorPointList,self._WindowList)
		
		self.SetCenterPosition()
		
	#壁のポイントを削除
	def RemoveWallPoint(self,num):
		floorPointList,doorList,windowList = self._WallClass.RemovePoint(num)
		wallVisibleList = self._WallClass.GetVisibleList()
		
		self._FloorPointList = floorPointList
		self._DoorList = doorList
		self._WindowList = windowList
		self._WallVisibleList = wallVisibleList
		
		self._WallLineClass.SetVisibleList(self._WallVisibleList)
		
		self._FloorClass.SetPointList(self._FloorPointList)
		self._WallLineClass.SetPointList(self._FloorPointList)
		self._WallPointClass.SetPointList(self._FloorPointList)
		self._CeilingClass.SetPointList(self._FloorPointList)
		self._DoorControlClass.SetPointList(self._FloorPointList,self._DoorList)
		self._WindowControlClass.SetPointList(self._FloorPointList,self._WindowList)
		
		self.SetCenterPosition()
		
	#壁のポイントの数を取得
	def GetWallPointCount(self):
		pointCount = len(self._FloorPointList)
		return pointCount
	
	#壁のポイントのリストを取得
	def GetWallPointList(self):
		pointList = self._FloorPointList
		return pointList
	
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._DoorControlClass.GetDoorModelList()
		return modelList
		
	#ドアの数を取得
	def GetDoorCount(self):
		count = self._DoorControlClass.GetCount()
		return count
		
	#窓の数を取得
	def GetWindowCount(self):
		count = self._WindowControlClass.GetCount()
		return count
		
	#ドアの情報を取得
	def GetDoorState(self,num):
		state = self._DoorControlClass.GetState(num)
		return state
		
	#窓の情報を取得
	def GetWindowState(self,num):
		state = self._WindowControlClass.GetState(num)
		return state
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._DoorControlClass.GetDoorModelList()
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		#床
		floorModel = self._FloorClass.GetModel()
		modelList.append(floorModel)
		
		#壁
		wallModelList = self._WallClass.GetAllModelList()
		for model in wallModelList:
			modelList.append(model)
		
		#天井
		ceilingModel = self._CeilingClass.GetModel()
		modelList.append(ceilingModel)
		
		#ドア
		doorModelList = self._DoorControlClass.GetDoorModelList()
		for model in doorModelList:
			modelList.append(model)
		
		#窓
		windowModelList = self._WindowControlClass.GetWindowModelList()
		for model in windowModelList:
			modelList.append(model)
		
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = []
		
		#壁
		wallModelList = self._WallClass.GetAllModelList()
		for model in wallModelList:
			modelList.append(model)
		
		#ドア
		doorModelList = self._DoorControlClass.GetDoorModelList()
		for model in doorModelList:
			modelList.append(model)
		
		#窓
		windowModelList = self._WindowControlClass.GetWindowModelList()
		for model in windowModelList:
			modelList.append(model)
		
		return modelList
		
	#部屋の中心位置を求める
	def CheckCenterPosition(self):
		position = [0,0,0]
		
		roomPosition = self.GetFloorPosition()
		wallPointCount = self.GetWallPointCount()
		pointList = self.GetWallPointList()
		
		for x in range(wallPointCount):
			pointPosition = pointList[x]
			position[0] = position[0] + pointPosition[0] + roomPosition[0]
			position[2] = position[2] + pointPosition[1] + roomPosition[2]
			
		position[0] = position[0] / wallPointCount
		position[2] = position[2] / wallPointCount
		
		return position
		
	#中心位置を再設定
	def SetCenterPosition(self):
		#部屋を再配置
		roomPos = self.GetFloorPosition()
		newRoomPos = self.CheckCenterPosition()
		self.SetFloorPosition(newRoomPos)
		
		#頂点を再配置
		disp = [0,0]
		disp[0] = newRoomPos[0] - roomPos[0]
		disp[1] = newRoomPos[2] - roomPos[2]
		
		for x in range(len(self._FloorPointList)):
			newPointPos = [0,0]
			pointPosition = self._FloorPointList[x]
			newPointPos[0] = pointPosition[0] - disp[0]
			newPointPos[1] = pointPosition[1] - disp[1]
			
			self._FloorPointList[x] = newPointPos
			
		self._FloorClass.Move(self._FloorPointList)
		self._WallClass.Move(self._FloorPointList)
		self._WallLineClass.Move(self._FloorPointList)
		self._WallPointClass.Move(self._FloorPointList)
		self._CeilingClass.Move(self._FloorPointList)
		self._DoorControlClass.Move(self._FloorPointList)
		self._WindowControlClass.Move(self._FloorPointList)
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
	#壁の表示状態設定
	def SetWallVisible(self,wallNum,state):
		self._WallVisibleList[wallNum] = state
		
		self._WallClass.SetVisibleList(self._WallVisibleList)
		self._WallLineClass.SetVisibleList(self._WallVisibleList)
		
	#壁の表示状態取得
	def GetWallVisible(self,wallNum):
		state = self._WallVisibleList[wallNum]
		return state
		
	#壁の表示状態のリスト取得
	def GetWallVisibleList(self):
		list = self._WallVisibleList
		return list
		
	#追加時のテクスチャ、高さ設定情報を設定
	def SetDefaultSettings(self,floorTex,WallTex,CeilingTex,WallHeight):
		self._DefaultFloorTexPath = floorTex
		self._DefaultWallTexPath = WallTex
		self._DefaultCeilingTexPath = CeilingTex
		self._DefaultWallHight = WallHeight
		
	#壁のサイズを取得
	def GetWallSize(self,num):
		wallSize = None
		if num != None:
			point0,point1 = self.GetWallPoint(num)
			wallSize = vizmat.Distance(point0,point1)
			
		return wallSize
		
	#ドアのリストを取得
	def GetDoorList(self):
		list = self._DoorList
		return list
		
	#窓のリストを取得
	def GetWindowList(self):
		list = self._WindowList
		return list
		
	#オペ室かどうかの設定
	def SetOpRoomState(self,state):
		self._IsOpRoom = state
		
	#オペ室かどうかの取得
	def GetOpRoomState(self):
		state = self._IsOpRoom
		return state
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#床制御のクラス定義
class Floor(Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,textureName,textureSize):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Floor] : Init Floor')
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._TextureFile = textureName
		self._TextureSize = textureSize
		
		self._Model = None
		self._Texture = None
		
		self._Height = 0.0
		
		self.Create()
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Floor] : Start Floor Update')
		pass
		
	#床モデルの作成
	def Create(self):
		self._Texture = self.LoadTexture(self._TextureFile)
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)

		self._Model = None
		
		#板ポリゴン作成
		viz.startLayer(viz.TRIANGLE_FAN)
		for position in self._PointList: 
			viz.texCoord(0.0,0.0)
			viz.vertex(position[0],0,position[1])
		model = viz.endLayer()
		
		model.drawOrder(-100)
		
		#model.enable(viz.LIGHTING)
		model.color(viz.WHITE)
		model.emissive(viz.GRAY)
		model.ambient(viz.GRAY)
		
		model.texture(self._Texture)
		
		self._Model = model
		
		self.SetPickable(False)
		self.SetFloor()
		
	#床モデルの削除
	def Delete(self):
		self._Model.remove()	#モデルを削除
		del self._Model
				
		self._Model = None
		
	#床モデルの配置
	def SetFloor(self):
		self._Model.setPosition([0.0,0.0,0.0])
		
		matrix = vizmat.Transform()
		matrix.setPosition([0.0,0.0,0.0])
		matrix.setScale([1/self._TextureSize,1/self._TextureSize,1])
			
		for x in range(len(self._PointList)):
			position = self._PointList[x]
			self._Model.setVertex(x,[position[0],0,position[1]],viz.ABS_GLOBAL)
			self._Model.setTexCoord(x,position[0],position[1])
		
		box = self._Model.getBoundingBox()
		scale = box.getSize()
		self._Height = 0.002-(scale[0]*scale[2]*0.000005)
		
		self._Model.setPosition(self._FloorPosition[0],self._Height,self._FloorPosition[2])
		self._Model.texmat(matrix)
		
	#テクスチャ画像の読み込み
	def LoadTexture(self,textureName):
		filePath = textureName
		"""
		if path.exists(textureName)!=True:		#絶対パスかどうか
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		"""
		if textureName[:2] == '..':
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		
		texture = viz.addTexture(filePath)
		
		return texture

	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.SetFloor()
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#テクスチャ設定
	def SetTexture(self,texture):
		self._Texture = texture
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		self._Model.texture(self._Texture)
		
	#テクスチャ取得
	def GetTexture(self):
		texture = self._Texture
		
		return texture
		
	#壁のポイントを再設定
	def SetPointList(self,pointList):
		self._PointList = pointList
		
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.SetFloor()
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁制御のクラス定義
class Wall(Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,doorList,windowList,textureName,textureSize,height,visibleList,isOpeRoom):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Wall] : Init Wall')
		
		self._FloorPosition = floorPositoin
		
		self._DoorControlClass = None
		self._WindowControlClass = None
		
		self._PointList = pointList
		self._DoorList = doorList
		self._WindowList = windowList
		self._TextureName = textureName
		self._TextureSize = textureSize
		self._Height = height
		self._VisibleList = visibleList
		
		self._ModelCount = 5
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		self._Texture = None
		self._TextureMatrix = None
		
		self.SetTextureMatrix()
		
		#壁の端は斜めに面を作成（正方形の部屋の四隅）
		self._OffsetVal = 0.075
		self._OpeRoomOffsetVal = 0.031
		
		#オペ室かどうか
		self._IsOpRoom = isOpeRoom
		
	#使用するクラスの追加
	def AddClass(self,doorControlClass,windowControlClass):
		self._DoorControlClass = doorControlClass
		self._WindowControlClass = windowControlClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Wall] : Start Wall Update')
		pass
		
	#壁モデルの作成
	def Create(self):
		self._Texture = self.LoadTexture(self._TextureName)
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		
		self.SetWall(True)
		self.SetPickable(False)
		self.SetVisible()
		
	#壁モデルの削除
	def Delete(self):
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				childModelList = model.getChildren()
				for childModel in childModelList:
					childModel.remove()	#モデルを削除
					del childModel
					
				model.remove()	#モデルを削除
				del model
				
		self._ModelList = []
		self._ModelBackSideList = []
		self._ModelTopSideList = []
		self._ModelLeftSideList = []
		self._ModelRightSideList = []
		
	#壁モデルの配置
	def SetWall(self,isCreate=False):
		#部屋にある壁の数だけループ
		for pointNum in range(len(self._PointList)):
			#print 'pointNum',pointNum
			
			#壁の両端の位置と距離を取得
			point0,point1 = self.GetPoint(pointNum)
			pointDistance = vizmat.Distance(point0,point1)
			
			#壁に配置されているドアと窓の情報を取得
			doorDataList	= self.GetDoorData(pointNum)
			windowDataList	= self.GetWindowData(pointNum)
			
			#ドアと窓が無い場合（壁のみ）
			if len(doorDataList) == 0 and len(windowDataList) == 0:
				for modelNum in range(self._ModelCount):		#5面作成（表裏左右上）
					rootModel = self.GetRootModel(isCreate,modelNum,pointNum)	#ルートモデルの設定
					wallModel = self.GetModel(isCreate,rootModel,modelNum,0)	#壁モデルの設定
					
					percentage0 = 0.0
					percentage1 = 1.0
					positionYMin = 0.0
					positionYMax = self._Height
					
					#モデルを配置
					self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
					
					#表示状態の設定
					visibleState = self._VisibleList[pointNum]
					self.SetModelPosition(rootModel,point0,point1,visibleState)
					
			#ドアと窓が有る場合
			else:
				#ドア、窓を位置が小さい順に並べる
				dataList = self.SortData(doorDataList,windowDataList)
				#print 'dataList',dataList
				#print 'doorDataList',doorDataList
				#print 'windowDataList',windowDataList
				
				for modelNum in range(self._ModelCount):		#5面作成（表裏左右上）
					rootModel = self.GetRootModel(isCreate,modelNum,pointNum)	#ルートモデルの設定
					
					modelCount = 0
					
					#ドア又は窓の数だけループ
					#print 'dataCount=',len(dataList)
					for count in range(len(dataList)):
						data = dataList[count]
						#print 'data',data
						
						#最初は壁を作成
						if count == 0:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage1 = posXMin
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#ドア又は窓の上に壁を作成
						wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
						modelCount = modelCount + 1
						
						percentage0 = 0.0
						percentage1 = 1.0
						positionYMin = 0.0
						positionYMax = self._Height
						
						posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
						percentage0 = posXMin
						percentage1 = posXMax
						positionYMin = posYMax
						
						#モデルを配置
						self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
						
						#ドア又は窓の上に壁を作成
						if data[0] == 1:	#窓の場合
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMin
							percentage1 = posXMax
							positionYMax = posYMin
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#最後は壁を作成
						if count == len(dataList) - 1:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMax
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
						#次のドア又は窓まで壁を作成
						else:
							wallModel = self.GetModel(isCreate,rootModel,modelNum,modelCount)	#壁モデルの設定
							modelCount = modelCount + 1
							
							percentage0 = 0.0
							percentage1 = 1.0
							positionYMin = 0.0
							positionYMax = self._Height
							
							posXMin,posXMax,posYMin,posYMax = self.GetPositionOnWall(data,pointNum,pointDistance)
							percentage0 = posXMax
							
							nextData = dataList[count+1]
							#print 'nextData',nextData
							posXMin2,posXMax2,posYMin2,posYMax2 = self.GetPositionOnWall(nextData,pointNum,pointDistance)
							percentage1 = posXMin2
							
							#モデルを配置
							self.SetWallModel(wallModel,modelNum,percentage0,percentage1,positionYMin,positionYMax,pointDistance)
							
							#隙間がない時は壁を非表示
							dist = pointDistance * (percentage1-percentage0)
							
							if dist > 0.01:	#今の窓、ドアの終端と次の窓、ドアの始端の位置関係が1cm未満の場合は非表示
								wallModel.visible(viz.ON)
							else:
								wallModel.visible(viz.OFF)
								
					#表示状態の設定
					visibleState = self._VisibleList[pointNum]
					self.SetModelPosition(rootModel,point0,point1,visibleState)
					
	#ルートモデルを作成
	def CreateRootModel(self):
		model = vizshape.addPlane([1,1])
		model.disable(viz.PICKING)
		model.alpha(0)
		
		return model
		
	#壁モデルを作成
	def CreateWallModel(self,rootModel):
		#板ポリゴン作成
		viz.startLayer(viz.QUADS)
		viz.texCoord(0.0,0.0)
		viz.vertex(0.0,0.0,0.0)
		viz.normal(0,1,0)
		viz.texCoord(1.0,0.0)
		viz.vertex(1.0,0.0,0.0)
		viz.normal(0,1,0)
		viz.texCoord(1.0,1.0)
		viz.vertex(1.0,0.0,1.0)
		viz.normal(0,1,0)
		viz.texCoord(0.0,1.0)
		viz.vertex(0.0,0.0,1.0)
		viz.normal(0,1,0)
		model = viz.endLayer()
		
		model.setParent(rootModel)
		model.texture(self._Texture)
		
		model.enable(viz.LIGHTING)
		model.color(viz.WHITE)
		model.emissive(viz.GRAY)
		model.ambient(viz.GRAY)
		
		return model
		
	#モデル位置をリセット
	def ResetModel(self,model):
		model.setPosition(0,0,0)
		model.setEuler(0,0,0)
		
	#ルートモデルを取得
	def GetRootModel(self,isCreate,modelNum,pointNum):
		rootModel = None
		
		if isCreate:	#作成
			rootModel = self.CreateRootModel()
			self.AddToWallList(modelNum,rootModel)
			
		else:			#取得
			rootModel = self.GetFromWallList(modelNum,pointNum)
			self.ResetModel(rootModel)
			
		return rootModel
		
	#モデルを取得
	def GetModel(self,isCreate,rootModel,modelNum,count):
		wallModel = None
		
		if isCreate:	#作成
			wallModel = self.CreateWallModel(rootModel)
			
		else:			#取得
			modelList = rootModel.getChildren()
			wallModel = modelList[count]
			self.ResetModel(wallModel)
			
		return wallModel
		
	#壁をリストに追加
	def AddToWallList(self,modelNum,rootModel):
		if modelNum == 0:
			self._ModelList.append(rootModel)
		elif modelNum == 1:
			self._ModelBackSideList.append(rootModel)
		elif modelNum == 2:
			self._ModelTopSideList.append(rootModel)
		elif modelNum == 3:
			self._ModelLeftSideList.append(rootModel)
		elif modelNum == 4:
			self._ModelRightSideList.append(rootModel)
			
	#壁をリストから取得
	def GetFromWallList(self,modelNum,wallNum):
		rootModel = None
		if modelNum == 0:
			rootModel = self._ModelList[wallNum]
		elif modelNum == 1:
			rootModel = self._ModelBackSideList[wallNum]
		elif modelNum == 2:
			rootModel = self._ModelTopSideList[wallNum]
		elif modelNum == 3:
			rootModel = self._ModelLeftSideList[wallNum]
		elif modelNum == 4:
			rootModel = self._ModelRightSideList[wallNum]
			
		return rootModel
		
	#壁にあるドアの情報を取得
	def GetDoorData(self,wallNum):
		dataList = []
		for x in range(len(self._DoorList)):
			data = self._DoorList[x]
			if data[1] == wallNum:
				if len(data) == 4:
					data[3] = x
				else:
					data.append(x)
					
				dataList.append(data)
				
		return dataList
		
	#壁にある窓の情報を取得
	def GetWindowData(self,wallNum):
		dataList = []
		for x in range(len(self._WindowList)):
			data = self._WindowList[x]
			if data[2] == wallNum:
				if len(data) == 5:
					data[4] = x
				else:
					data.append(x)
					
				dataList.append(data)
				
		return dataList
		
	#壁のモデルを配置
	def SetWallModel(self,wallModel,modelNum,percentage0,percentage1,posYMin,posYMax,pointDistance):
		position0 = (pointDistance * (1-percentage1)) - (pointDistance / 2)
		position1 = (pointDistance * (1-percentage0)) - (pointDistance / 2)
		
		positionYMin = posYMin
		positionYMax = posYMax
		
		posZ = 0.0
		if modelNum == 0:
			posZ = 0.075 + 0.001	#壁は内側に1mmずらす
		elif modelNum == 1:
			posZ = -0.075 + 0.001
			
		if percentage0 == 0:	#最初の位置
			if modelNum != 0:	
				if self._IsOpRoom == 1:
					position1 = position1 + self._OpeRoomOffsetVal
				elif self._IsOpRoom == 2:
					pass
				else:
					position1 = position1 + self._OffsetVal
				
		if percentage1 == 1:	#最後の位置
			if modelNum != 0:
				if self._IsOpRoom == 1:
					position0 = position0 - self._OpeRoomOffsetVal
				elif self._IsOpRoom == 2:
					pass
				else:
					position0 = position0 - self._OffsetVal
					
		#オペ室通路用間取りの壁は幅を縮める
		if self._IsOpRoom == 2:
			position1 = position1 - 0.035
			position0 = position0 + 0.035
			
		wallModel.setVertex(0,[position0,posZ,positionYMin])
		wallModel.setTexCoord(0,position0,positionYMin)
		wallModel.setVertex(1,[position1,posZ,positionYMin])
		wallModel.setTexCoord(1,position1,positionYMin)
		wallModel.setVertex(2,[position1,posZ,positionYMax])
		wallModel.setTexCoord(2,position1,positionYMax)
		wallModel.setVertex(3,[position0,posZ,positionYMax])
		wallModel.setTexCoord(3,position0,positionYMax)
		
		if modelNum == 2:	#天板
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position0,posZ,positionYMax])
			wallModel.setVertex(1,[position1,posZ,positionYMax])
			wallModel.setVertex(2,[position1,posZ2,positionYMax])
			wallModel.setVertex(3,[position0,posZ2,positionYMax])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		if modelNum == 3:	#左側面
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position0,posZ,positionYMin])
			wallModel.setVertex(1,[position0,posZ,positionYMax])
			wallModel.setVertex(2,[position0+self._OffsetVal,posZ2,positionYMax])
			wallModel.setVertex(3,[position0+self._OffsetVal,posZ2,positionYMin])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		if modelNum == 4:	#右側面
			posZ = -0.075 + 0.001
			posZ2 = 0.075 + 0.001
			wallModel.setVertex(0,[position1,posZ,positionYMin])
			wallModel.setVertex(1,[position1,posZ,positionYMax])
			wallModel.setVertex(2,[position1-self._OffsetVal,posZ2,positionYMax])
			wallModel.setVertex(3,[position1-self._OffsetVal,posZ2,positionYMin])
			
			wallModel.color(viz.BLACK)
			wallModel.emissive(viz.GRAY)
			wallModel.ambient(viz.GRAY)
			
		#テクスチャ設定
		wallModel.texmat(self._TextureMatrix)
		
	#ドア、窓を位置が小さい順に並べる
	def SortData(self,doorDataList,windowDataList):
		targetList = []		#比較対象
		for x in range(len(doorDataList)):
			targetList.append([0,x,doorDataList[x][2]])
		for x in range(len(windowDataList)):
			targetList.append([1,x,windowDataList[x][3]])
		#print 'targetList',targetList
		
		sortedList = sorted(targetList,key=lambda x: x[2])		#3つ目の値でソート
		
		resDataList = []	#並べ替えた結果
		for data in sortedList:
			resDataList.append([data[0],data[1]])
		#print 'resDataList',resDataList
		
		return resDataList
		
	#ドア、窓の上下左右の位置を取得
	def GetPositionOnWall(self,data,pointNum,pointDistance):
		percentage0 = 0.0
		percentage1 = 1.0
		posYMin = 0.0
		posYMax = 1.0
		
		if data[0] == 0:	#ドアの場合
			doorDataList = self.GetDoorData(pointNum)
			doorData = doorDataList[data[1]]
			#print 'doorData',doorData
			door = self._DoorControlClass.GetModel(doorData[3])
			doorSize = door.GetSize()
			doorSizePercentageX = doorSize[0] / pointDistance
			percentage0 = doorData[2] - doorSizePercentageX / 2
			percentage1 = doorData[2] + doorSizePercentageX / 2
			posYMax = doorSize[1]
			
		elif data[0] == 1:	#窓の場合
			windowDataList = self.GetWindowData(pointNum)
			windowData = windowDataList[data[1]]
			#print 'windowData',windowData
			window = self._WindowControlClass.GetModel(windowData[4])
			windowSize = window.GetSize()
			windowSizePercentageX = windowSize[0] / pointDistance
			percentage0 = windowData[3] - windowSizePercentageX / 2
			percentage1 = windowData[3] + windowSizePercentageX / 2
			posYMin = windowData[1]
			posYMax = windowData[1] + windowSize[1]
			
		return percentage0,percentage1,posYMin,posYMax
		
	#壁のUV設定
	def SetTextureMatrix(self):
		#テクスチャのUV設定
		self._TextureMatrix = vizmat.Transform()
		self._TextureMatrix.setPosition([0.0,0.0,0.0])
		self._TextureMatrix.setScale([1/self._TextureSize,1/self._TextureSize,1])
		
	#テクスチャ画像の読み込み
	def LoadTexture(self,textureName):
		filePath = textureName
		"""
		if path.exists(textureName)!=True:		#絶対パスかどうか
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		"""
		if textureName[:2] == '..':
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		
		texture = viz.addTexture(filePath)
		
		return texture
		
	#選択可能状態の設定
	def SetPickable(self,state):
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				childModelList = model.getChildren()
				for childModel in childModelList:
					if state == True:
						childModel.enable(viz.PICKING)
					elif state == False:
						childModel.disable(viz.PICKING)
						
	#ポイントを取得
	def GetPoint(self,num):
		point0 = self._PointList[num]
		num2 = num + 1
		if num2 == len(self._PointList):
			num2 = 0
		point1 = self._PointList[num2]
		
		return point0,point1
		
	#ポイント番号を取得
	def GetPointNum(self,num):
		num2 = num + 1
		if num2 == len(self._PointList):
			num2 = 0
			
		return num,num2
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.SetWall()
		
	#位置設定
	def SetModelPosition(self,model,point0,point1,visibleState):
		#位置
		position = [0.0,0.0,0.0]
		position[0] = (point0[0] + point1[0]) / 2 + self._FloorPosition[0]
		position[1] = 0.0 + self._FloorPosition[1]
		position[2] = (point0[1] + point1[1]) / 2 + self._FloorPosition[2]
		
		#非表示の時はずらす
		if visibleState == False:
			position[1] = -15.0
			
		model.setPosition(position)
		
		#角度
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		model.setEuler(wallAngle-90.0,-90.0,0.0)
		
	#ドアを設定
	def SetDoor(self,doorList):
		self._DoorList = doorList
		
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
	#窓を設定
	def SetWindow(self,windowList):
		self._WindowList = windowList
		
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
		
	#テクスチャ設定
	def SetTexture(self,texture):
		self._Texture = texture
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		for x in range(len(self._ModelList)):
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				childModelList = model.getChildren()
				for childModel in childModelList:
					childModel.texture(self._Texture)
					
	#テクスチャ取得
	def GetTexture(self):
		texture = self._Texture
		return texture
		
	#テクスチャ名取得
	def GetTextureName(self):
		textureName = self._TextureName
		return textureName
		
	#壁のポイントを追加
	def AddPoint(self,num):
		
		#ポイントリストを更新
		point0,point1 = self.GetPoint(num)
		newPoint = [0,0]
		newPoint[0] = (point0[0] + point1[0]) / 2
		newPoint[1] = (point0[1] + point1[1]) / 2
		
		self._PointList.insert(num+1,newPoint)
		
		#表示状態リストを更新
		visibleState = self._VisibleList[num]
		self._VisibleList.insert(num+1,visibleState)
		
		#ドアリストを更新
		for x in range(len(self._DoorList)):
			door = self._DoorList[x]
			if door[1] == num:
				if door[2] < 0.5:
					door[2] = door[2] * 2
				else:
					door[1] = door[1] + 1
					door[2] = door[2] * 2 - 1
				self._DoorList[x] = door
				
			elif door[1] > num:
				door[1] = door[1] + 1
				self._DoorList[x] = door
				
		#窓リストを更新
		for x in range(len(self._WindowList)):
			widnow = self._WindowList[x]
			if widnow[2] == num:
				if widnow[3] < 0.5:
					widnow[3] = widnow[3] * 2
				else:
					widnow[2] = widnow[2] + 1
					widnow[3] = widnow[3] * 2 - 1
				self._WindowList[x] = widnow
				
			elif widnow[2] > num:
				widnow[2] = widnow[2] + 1
				self._WindowList[x] = widnow
				
		preTexture = self.GetTexture()
		
		#再作成
		self.Delete()
		self.Create()
		
		self.SetTexture(preTexture)
		
		floorPointList = self._PointList
		doorList = self._DoorList
		windowList = self._WindowList
		
		return floorPointList,doorList,windowList
		
	#壁のポイントを削除
	def RemovePoint(self,num):
		
		#ポイントリストを更新
		point = self._PointList[num]
		self._PointList.remove(point)
		
		#表示状態リストを更新
		newVisibleList = []
		for x in range(len(self._VisibleList)):
			if x != num:
				newVisibleList.append(self._VisibleList[x])
				
		self._VisibleList = newVisibleList
		
		#ドアリストを更新
		for x in range(len(self._DoorList)):
			door = self._DoorList[x]
			if num == 0:
				if door[1] == len(self._PointList):
					door[1] = door[1] - 1
					door[2] = door[2] / 2
					self._DoorList[x] = door
				elif door[1] == num:
					door[1] = len(self._PointList) - 1
					door[2] = door[2] / 2 + 0.5
					self._DoorList[x] = door
				elif door[1] > num:
					door[1] = door[1] - 1
					self._DoorList[x] = door
			else:
				if door[1] == num - 1:
					door[2] = door[2] / 2
					self._DoorList[x] = door
				elif door[1] == num:
					door[1] = door[1] - 1
					door[2] = door[2] / 2 + 0.5
					self._DoorList[x] = door
				elif door[1] > num:
					door[1] = door[1] - 1
					self._DoorList[x] = door
					
		#窓リストを更新
		for x in range(len(self._WindowList)):
			widnow = self._WindowList[x]
			if num == 0:
				if widnow[2] == len(self._PointList):
					widnow[2] = widnow[2] - 1
					widnow[3] = widnow[3] / 2
					self._WindowList[x] = widnow
				elif widnow[2] == num:
					widnow[2] = len(self._PointList) - 1
					widnow[3] = widnow[3] / 2 + 0.5
					self._WindowList[x] = widnow
				elif widnow[2] > num:
					widnow[2] = widnow[2] - 1
					self._WindowList[x] = widnow
			else:
				if widnow[2] == num - 1:
					widnow[3] = widnow[3] / 2
					self._WindowList[x] = widnow
				elif widnow[2] == num:
					widnow[2] = widnow[2] - 1
					widnow[3] = widnow[3] / 2 + 0.5
					self._WindowList[x] = widnow
				elif widnow[2] > num:
					widnow[2] = widnow[2] - 1
					self._WindowList[x] = widnow
					
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
		floorPointList = self._PointList
		doorList = self._DoorList
		windowList = self._WindowList
		
		return floorPointList,doorList,windowList
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.SetWall()
		
	#モデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		for wallModel in self._ModelList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		return modelList
		
	#全モデルのリストを取得
	def GetAllModelList(self):
		modelList = []
		
		for wallModel in self._ModelList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelBackSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelTopSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelLeftSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		for wallModel in self._ModelRightSideList:
			childModelList = wallModel.getChildren()
			for childModel in childModelList:
				modelList.append(childModel)
				
		return modelList
		
	#モデルの表示状態を設定
	def SetVisible(self):
		for x in range(len(self._ModelList)):
			model = None
			for y in range(self._ModelCount):
				model = None
				if y == 0:
					model = self._ModelList[x]
				elif y == 1:
					model = self._ModelBackSideList[x]
				elif y == 2:
					model = self._ModelTopSideList[x]
				elif y == 3:
					model = self._ModelLeftSideList[x]
				elif y == 4:
					model = self._ModelRightSideList[x]
					
				visibleState = self._VisibleList[x]
				
				if visibleState:
					model.visible(viz.ON)
					
					position = model.getPosition()
					position[1] = 0.0 + self._FloorPosition[1]
					model.setPosition(position)
					
				else:
					model.visible(viz.OFF)
					
					position = model.getPosition()
					position[1] = -15.0
					model.setPosition(position)
					
	#モデルの表示状態のリストを設定
	def SetVisibleList(self,visibleList):
		self._VisibleList = visibleList
		
		self.SetVisible()
		
	#モデルの表示状態のリスト取得
	def GetVisibleList(self):
		list = self._VisibleList
		return list
		
	#壁を非表示に設定
	def Hide(self,state):
		if state:
			for x in range(len(self._ModelList)):
				for y in range(self._ModelCount):
					model = None
					if y == 0:
						model = self._ModelList[x]
					elif y == 1:
						model = self._ModelBackSideList[x]
					elif y == 2:
						model = self._ModelTopSideList[x]
					elif y == 3:
						model = self._ModelLeftSideList[x]
					elif y == 4:
						model = self._ModelRightSideList[x]
						
					model.visible(viz.OFF)
					
		else:
			self.SetVisible()
			
# - - - - - - - - - - - - - - - - - - - - - - - - -
#天井制御のクラス定義
class Ceiling(Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,textureName,textureSize,height):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Ceiling] : Init Ceiling')
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._TextureFile = textureName
		self._TextureSize = textureSize
		self._Height = height
		
		self._Model = None
		self._Texture = None
		
		self.Create()
		self.SetVisible(False)
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Ceiling] : Start Ceiling Ceiling')
		pass
		
	#天井モデルの作成
	def Create(self):
		self._Texture = self.LoadTexture(self._TextureFile)
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)

		self._Model = None
		
		#板ポリゴン作成
		viz.startLayer(viz.TRIANGLE_FAN)
		for position in self._PointList: 
			viz.texCoord(0.0,0.0)
			viz.vertex(position[0],0,position[1])
		model = viz.endLayer()
		
		model.drawOrder(-50)
		
		#model.enable(viz.LIGHTING)
		model.color(viz.WHITE)
		model.emissive(viz.GRAY)
		model.ambient(viz.GRAY)
		
		model.texture(self._Texture)
			
		self._Model = model
		
		self.SetPickable(False)
		self.SetCeiling()
		
	#天井モデルの削除
	def Delete(self):
		self._Model.remove()	#モデルを削除
		del self._Model
				
		self._Model = None
		
	#天井モデルの配置
	def SetCeiling(self):
		self._Model.setPosition([0.0,0.0,0.0])
		
		matrix = vizmat.Transform()
		matrix.setPosition([0.0,0.0,0.0])
		matrix.setScale([1/self._TextureSize,1/self._TextureSize,1])
		
		for x in range(len(self._PointList)):
			position = self._PointList[x]
			self._Model.setVertex(x,[position[0],0,position[1]],viz.ABS_GLOBAL)
			self._Model.setTexCoord(x,position[0],position[1])
		
		#サイズによって少しずらす（ちらつき防止）
		box = self._Model.getBoundingBox()
		scale = box.getSize()
		heightOffset = scale[0]*scale[2]*0.000005
		
		ceilingPosition = [0,0,0]
		ceilingPosition[0] = self._FloorPosition[0]
		ceilingPosition[1] = self._Height - 0.002 + heightOffset
		ceilingPosition[2] = self._FloorPosition[2]
		
		self._Model.setPosition(ceilingPosition)
		self._Model.texmat(matrix)

	#テクスチャ画像の読み込み
	def LoadTexture(self,textureName):
		filePath = textureName
		"""
		if path.exists(textureName)!=True:		#絶対パスかどうか
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		"""
		if textureName[:2] == '..':
			filePath = viz.res.getPublishedPath('../Settings'+"/"+textureName)
		
		texture = viz.addTexture(filePath)
		
		return texture

	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
				
	#表示状態を設定
	def SetVisible(self,state):
		if state == True:
			self._Model.visible(viz.ON)
		else:
			self._Model.visible(viz.OFF)
			
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
	
		self.SetCeiling()
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.SetCeiling()
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#テクスチャ設定
	def SetTexture(self,texture):
		self._Texture = texture
		self._Texture.wrap(viz.WRAP_S,viz.REPEAT)
		self._Texture.wrap(viz.WRAP_T,viz.REPEAT)
		
		self._Model.texture(self._Texture)
		
	#テクスチャ取得
	def GetTexture(self):
		texture = self._Texture
		
		return texture
		
	#壁のポイントを再設定
	def SetPointList(self,pointList):
		self._PointList = pointList
		
		preTexture = self.GetTexture()
		self.Delete()
		self.Create()
		self.SetTexture(preTexture)
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.SetCeiling()
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドア制御のクラス定義
class DoorControl(Interface):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,doorList,wallHeight):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorControl] : Init DoorControl')
		
		self._WallClass = None
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._DoorList = doorList
		self._WallHeight = wallHeight
		
		self._ModelList = []
		self._LineModelList = []
		
		self._DoorRef = vizshape.addBox([1,1,1])
		self._DoorRef.visible(viz.OFF)
		self._DoorRef.disable(viz.PICKING)
		
	#使用するクラスの追加
	def AddClass(self,wallClass):
		self._WallClass = wallClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorControl] : Start DoorControl Update')
		pass
		
	#iniファイルを基にドアを配置
	def SetDoor(self):
		for data in self._DoorList:
			num = self.Create(data[0],data[1],data[2])
	
	#作成
	def Create(self,fileName,wallNum,percentage):
		#モデル
		door = Door()
		door.Add(fileName)	#モデルを追加
		
		self._ModelList.append(door)
		
		#ラインモデル
		doorLine = DoorLine()
		doorLine.SetWallHeight(self._WallHeight)
		doorLine.Add(door)	#モデルを追加
		
		self._LineModelList.append(doorLine)
		
		#ID設定
		num = len(self._ModelList) - 1
		
		door.SetId(num)
		doorLine.SetId(num)
		
		#配置
		self.SetPosition(num,wallNum,percentage)
		
		return num
		
	#追加
	def Add(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,wallNum,percentage]
		self._DoorList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,wallNum,percentage)
		
		return num,self._DoorList
		
	#ダミーを追加
	def AddDummy(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,wallNum,percentage]
		self._DoorList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,wallNum,percentage)
		
		door = self._ModelList[num]
		door.SetDummyFlag(True)
		doorLine = self._LineModelList[num]
		doorLine.SetDummyFlag(True)
		
		return num,self._DoorList
		
	#削除
	def Delete(self,num):
		#モデル
		model = self._ModelList[num]
		
		self._ModelList.remove(model)
		
		if model != None:
			model.Delete()	#モデルを削除
			del model
		
		#ラインモデル
		lineModel = self._LineModelList[num]
		
		self._LineModelList.remove(lineModel)
		
		if lineModel != None:
			lineModel.Delete()	#モデルを削除
			del lineModel
			
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
			lineModel = self._LineModelList[x]
			lineModel.SetId(num)
			
		#ドアのリストを更新
		deletedDoor = self._DoorList[num]
		self._DoorList.remove(deletedDoor)
		
		return self._DoorList
			
	#全モデル削除
	def DeleteAll(self):
		#モデル
		for model in self._ModelList:
			model.Delete()	#モデルを削除
			del model
		
		self._ModelList = []
		
		#ラインモデル
		for lineModel in self._LineModelList:
			lineModel.Delete()	#モデルを削除
			del lineModel
		
		self._LineModelList = []
		
	#コピー
	def Copy(self,num):
		door = self._DoorList[num]
		
		fileName = door[0]
		wallNum = door[1]
		percentage = door[2]
		
		newPercentage = percentage + 0.1
		if newPercentage > 0.99:
			newPercentage = 0.99
		
		self.Add(fileName,wallNum,newPercentage)
		
		return self._DoorList
			
	#位置設定
	def SetPosition(self,num,wallNum,percentage):
		model = self._ModelList[num]
		lineModel = self._LineModelList[num]
		
		position = [0,0]
		orientation = 0
		
		if model != None and wallNum != None:	
			point0,point1 = self._WallClass.GetPoint(wallNum)
			position,orientation = self.GetModelPosition(model,point0,point1,percentage)
			
			#床の位置だけオフセット
			position [0] = position[0] + self._FloorPosition[0]
			position [1] = position[1] + self._FloorPosition[1]
			position [2] = position[2] + self._FloorPosition[2]
			
			#モデル
			model.SetOrientation(orientation)
			model.SetPosition(position,self._DoorRef)
	
			#ラインモデル
			lineModel.SetOrientation(orientation)
			lineModel.SetPosition(position)
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
	
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
		lineModel = self._LineModelList[num]
		lineModel.SetHighlight(state)
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model or self._LineModelList[x]._Model == model:
				num =  x
		
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetPickable(state)
			
			lineModel = self._LineModelList[x]
			lineModel.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#ラインモデル取得
	def GetLineModel(self,num):
		lineModel = self._LineModelList[num]
		
		return lineModel
		
	#モデルの位置取得
	def GetModelPosition(self,door,point0,point1,percentage):
		position = [0,0,0]
		orientation = 0
		
		position[0] = point0[0]*(1-percentage)+point1[0]*percentage
		position[1] = 0.0
		position[2] = point0[1]*(1-percentage)+point1[1]*percentage
		
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		
		orientation = wallAngle - 90.0
		
		return position,orientation
		
	#ラインの表示状態を設定
	def SetLineVisible(self,state):
		for lineModel in self._LineModelList:
			lineModel.SetVisible(state)
	
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
		for lineModel in self._LineModelList:
			lineModel.SetWallHeight(self._WallHeight)
			
	#壁のポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._DoorList)):
			data = self._DoorList[x]
			self.SetPosition(x,data[1],data[2])
		
	#リストを更新
	def UpdateList(self,num,wallNum,percentage):
		data = self._DoorList[num]
		data[1] = wallNum
		data[2] = percentage
		
		self._DoorList[num] = data
		
		return self._DoorList
		
	#壁のポイントを再設定
	def SetPointList(self,pointList,doorList):
		self._PointList = pointList
		self._DoorList = doorList
		
		self.ReCreate()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = []
		
		for door in self._ModelList:
			model = door.GetModel()
			modelList.append(model)
			
		return modelList
		
	#ドアの数を取得
	def GetCount(self):
		count = len(self._ModelList)
			
		return count
		
	#ドアの情報を取得
	def GetState(self,num):
		data = self._DoorList[num]
			
		return data
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドア制御のクラス定義
class Door(Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Door] : Init Door')
		
		self._FileName = ""
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		self._Texture = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		self._Size = [1,1,1]
		
		self._Id = None
		self._DummyFlag = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Door] : Start Door Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = vizfx.addChild(filePath)	#モデルを追加
		#model = viz.addChild(filePath)	#モデルを追加
		model.setPosition([0,0,0])
		model.setEuler([0,0,0])
		
		model.disable(viz.ANIMATIONS)
		model.setAnimationFrame(0)
		
		self._Model = model
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		self._Texture = self._Model.getTexture()
		
		#半透明モデルのオーダーを設定
		nodeNameList = self._Model.getNodeNames()
		for name in nodeNameList:
			if name[-2:] == '_A':
				self._Model.drawOrder(50,name)
		
		self.SetPositionOffset()
		self.SetSize()
		self.SetPickable(False)
		
		self._FileName = fileName
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
		self._FileName = ''
	
	#位置設定
	def SetPosition(self,position,ref):
		if self._Model != None and position != None:
			refModel = ref
			
			refModel.setPosition(position,mode=viz.ABS_GLOBAL)
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = self._Orientation
			refModel.setEuler(eulerAngle)
			
			self._Model.setParent(refModel)
			self._Model.setPosition(self._PositionOffset,mode=viz.ABS_PARENT)
			globalPosition = self._Model.getPosition(mode=viz.ABS_GLOBAL)
			self._Model.setParent(viz.WORLD)
			self._Model.setPosition(globalPosition,mode=viz.ABS_GLOBAL)

			self._Position = position
			
			
	#オフセット設定
	def SetPositionOffset(self):
		if self._Model != None:
			self._PositionOffset = [0.0,0.0,0.0]
			offset = [0.0,0.0,0.0]
			
			box = self._Model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()
			
			offset[0] = boxCenter[0] * -1
			offset[1] = (boxCenter[1] - boxSize[1] / 2) * -1
			offset[2] = boxCenter[2] * -1
			
			self._PositionOffset[0] = offset[0]
			self._PositionOffset[1] = offset[1]
			self._PositionOffset[2] = offset[2]
			
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#位置のオフセット値取得
	def GetPositionOffset(self):
		positionOffset = self._PositionOffset
		return positionOffset
	
	#角度設定
	def SetOrientation(self,orientation):
		if self._Model != None and orientation != None:
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.texture(None)
			self._Model.specular(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.texture(self._Texture)
			self._Model.specular(self._Specular)
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#モデルのサイズを設定
	def SetSize(self):
		box = self._Model.getBoundingBox()
		self._Size =  box.size
		
	#モデルのサイズを取得
	def GetSize(self):
		size = self._Size
		
		return size
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
		
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		
	#モデルのファイル名を取得
	def GetFileName(self):
		name = self._FileName
		return name
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#ドアのライン制御のクラス定義
class DoorLine(Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorLine] : Init DoorLine')
		
		self._DoorClass = None
		
		self._Model = None
		self._Picking = False
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		
		self._WallHeight = 3
		
		self._Width = 0.15
		self._Color = [0.50,0.75,0.50]
		
		self._Id = None
		self._DummyFlag = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][DoorLine] : Start DoorLine Update')
		pass
		
	#追加
	def Add(self,doorClass):
		self._DoorClass = doorClass
		
		if self._Model != None:
			self.Delete()
		
		doorModel = self._DoorClass.GetModel()
		box = doorModel.getBoundingBox()
		modelSize = box.size
		modelCenter = box.center
		
		model = vizshape.addPlane([1.0,1.0])
		model.setScale([modelSize[0],1.0,self._Width])
		model.setPosition(0,self._WallHeight+0.2,0)
		model.disable(viz.LIGHTING)
		model.color(self._Color)
		
		self._Model = model
		
		self.SetPickable(False)
		self.SetHighlight(False)
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			
			position = iPosition
			
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = self._WallHeight+0.2
			position[2] = position[2] + self._PositionOffset[2]
			
			self._Model.setPosition(position)
			self._Position = position
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		if self._DummyFlag:
			self._Model.visible(False)	#ダミーは非表示
		else:
			self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
		else:
			self._Model.color(self._Color)
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
	
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓制御のクラス定義
class WindowControl(Interface):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,windowList,wallHeight):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowControl] : Init WindowControl')
		
		self._WallClass = None
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._WindowList = windowList
		self._WallHeight = wallHeight
		
		self._ModelList = []
		self._LineModelList = []
		
		self._DefaultHeight  = 0.5
		
		self._WindowRef = vizshape.addBox([1,1,1])
		self._WindowRef.visible(viz.OFF)
		self._WindowRef.disable(viz.PICKING)
		
	#使用するクラスの追加
	def AddClass(self,wallClass):
		self._WallClass = wallClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowControl] : Start WindowControl Update')
		pass
		
	#iniファイルを基に窓を配置
	def SetWindow(self):
		for data in self._WindowList:
			num = self.Create(data[0],data[1],data[2],data[3])
	
	#追加
	def Add(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,self._DefaultHeight,wallNum,percentage]
		self._WindowList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,self._DefaultHeight,wallNum,percentage)
		
		return num,self._WindowList
		
	#ダミーを追加
	def AddDummy(self,fileName,wallNum,percentage):
		#リストに追加
		data = [fileName,self._DefaultHeight,wallNum,percentage]
		self._WindowList.append(data)
		
		#モデルを作成
		num = self.Create(fileName,self._DefaultHeight,wallNum,percentage)
		
		window = self._ModelList[num]
		window.SetDummyFlag(True)
		windowLine = self._LineModelList[num]
		windowLine.SetDummyFlag(True)
		
		return num,self._WindowList
		
	#作成
	def Create(self,fileName,height,wallNum,percentage):
		#モデル
		window = Window()
		window.Add(fileName)	#モデルを追加
		
		self._ModelList.append(window)
		
		#ラインモデル
		windowLine = WindowLine()
		windowLine.SetWallHeight(self._WallHeight)
		windowLine.Add(window)	#モデルを追加
		
		self._LineModelList.append(windowLine)
		
		#ID設定
		num = len(self._ModelList) - 1
		
		window.SetId(num)
		windowLine.SetId(num)
		
		#配置
		self.SetPosition(num,wallNum,percentage)
		
		return num
		
	#削除
	def Delete(self,num):
		#モデル
		model = self._ModelList[num]
		
		self._ModelList.remove(model)
		
		if model != None:
			model.Delete()	#モデルを削除
			del model
		
		#ラインモデル
		lineModel = self._LineModelList[num]
		
		self._LineModelList.remove(lineModel)
		
		if lineModel != None:
			lineModel.Delete()	#モデルを削除
			del lineModel
			
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
			lineModel = self._LineModelList[x]
			lineModel.SetId(num)
			
		#窓のリストを更新
		deletedWindow = self._WindowList[num]
		self._WindowList.remove(deletedWindow)
		
		return self._WindowList
		
	#全モデル削除
	def DeleteAll(self):
		#モデル
		for model in self._ModelList:
			model.Delete()	#モデルを削除
			del model
		
		self._ModelList = []
		
		#ラインモデル
		for lineModel in self._LineModelList:
			lineModel.Delete()	#モデルを削除
			del lineModel
		
		self._LineModelList = []
		
	#コピー
	def Copy(self,num):
		window = self._WindowList[num]
		
		fileName = window[0]
		wallNum = window[2]
		percentage = window[3]
		
		newPercentage = percentage + 0.1
		if newPercentage > 0.99:
			newPercentage = 0.99
		
		newNum,list = self.Add(fileName,wallNum,newPercentage)
		
		#高さ設定
		windowHeight = self.GetWindowHeight(num)
		self.SetWindowHeight(newNum,windowHeight)
		
		return self._WindowList
			
	#位置設定
	def SetPosition(self,num,wallNum,percentage):
		model = self._ModelList[num]
		lineModel = self._LineModelList[num]
		
		position = [0,0]
		orientation = 0
		
		if model != None and wallNum != None:	
			point0,point1 = self._WallClass.GetPoint(wallNum)
			position,orientation = self.GetModelPosition(model,point0,point1,percentage)
			
			window = self._WindowList[num]
			position[1] = window[1]
			
			#床の位置だけオフセット
			position [0] = position[0] + self._FloorPosition[0]
			position [1] = position[1] + self._FloorPosition[1]
			position [2] = position[2] + self._FloorPosition[2]
			
			#モデル
			model.SetOrientation(orientation)
			model.SetPosition(position,self._WindowRef)
	
			#ラインモデル
			lineModel.SetOrientation(orientation)
			lineModel.SetPosition(position)
			
	#高さ設定
	def SetHeight(self,num,height):
		self._WindowList[num][1] = height
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
	
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
		lineModel = self._LineModelList[num]
		lineModel.SetHighlight(state)
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model or self._LineModelList[x]._Model == model:
				num =  x
		
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetPickable(state)
			
			lineModel = self._LineModelList[x]
			lineModel.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#ラインモデル取得
	def GetLineModel(self,num):
		lineModel = self._LineModelList[num]
		
		return lineModel
		
	#モデルの位置取得
	def GetModelPosition(self,window,point0,point1,percentage):
		position = [0,0,0]
		orientation = 0
				
		position[0] = point0[0]*(1-percentage)+point1[0]*percentage
		position[1] = 0.0
		position[2] = point0[1]*(1-percentage)+point1[1]*percentage
		
		wallRadian = math.atan2(point0[0]-point1[0],point0[1]-point1[1])
		wallAngle = math.degrees(wallRadian)
		
		orientation = wallAngle - 90.0
		
		return position,orientation
		
	#ラインの表示状態を設定
	def SetLineVisible(self,state):
		for lineModel in self._LineModelList:
			lineModel.SetVisible(state)
	
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
		for lineModel in self._LineModelList:
			lineModel.SetWallHeight(self._WallHeight)
			
	#壁のポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._WindowList)):
			data = self._WindowList[x]
			self.SetPosition(x,data[2],data[3])
		
	#リストを更新
	def UpdateList(self,num,state,height,wallNum,percentage):
		data = self._WindowList[num]
		
		if state == 0:		#全部更新
			data[1] = height
			data[2] = wallNum
			data[3] = percentage
		elif state == 1:	#高さのみ更新
			data[1] = height
		elif state == 2:	#壁上の位置のみ更新
			data[2] = wallNum
			data[3] = percentage
			
		self._WindowList[num] = data
		
		return self._WindowList
		
	#窓の高さを設定
	def SetWindowHeight(self,num,height):
		window = self._WindowList[num]
		window[1] = height
		self._WindowList[num] = window
		
		self.ReCreate()
		
		return self._WindowList
		
	#窓の高さを取得
	def GetWindowHeight(self,num):
		window = self._WindowList[num]
		height = window[1]
		
		return height
		
	#壁のポイントを再設定
	def SetPointList(self,pointList,windowList):
		self._PointList = pointList
		self._WindowList = windowList
		
		self.ReCreate()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		
	#窓の数を取得
	def GetCount(self):
		count = len(self._ModelList)
			
		return count
		
	#窓の情報を取得
	def GetState(self,num):
		data = self._WindowList[num]
			
		return data
		
	#窓モデルのリストを取得
	def GetWindowModelList(self):
		modelList = []
		
		for window in self._ModelList:
			model = window.GetModel()
			modelList.append(model)
			
		return modelList
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓制御のクラス定義
class Window(Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Window] : Init Window')
		
		self._FileName = ""
		self._Model = None
		self._Picking = False
		
		self._Color = None
		self._Emissive = None
		self._Ambient = None
		self._Specular = None
		self._Texture = None
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		self._Size = [1,1,1]
		
		self._Id = None
		self._DummyFlag = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Window] : Start Window Update')
		pass
		
	#追加
	def Add(self,fileName):
		if self._Model != None:
			self.Delete()
		
		filePath = viz.res.getPublishedPath(SETTING_FOLDER_PATH + fileName)
		model = vizfx.addChild(filePath)	#モデルを追加
		#model = viz.addChild(filePath)	#モデルを追加
		model.setPosition([0,0,0])
		model.setEuler([0,0,0])
		
		self._Model = model
		self._Model.disable(viz.LIGHTING)
		
		self._Color = self._Model.getColor()
		self._Emissive = self._Model.getEmissive()
		self._Ambient = self._Model.getAmbient()
		self._Specular = self._Model.getSpecular()
		self._Texture = self._Model.getTexture()
		
		#半透明モデルのオーダーを設定
		nodeNameList = self._Model.getNodeNames()
		for name in nodeNameList:
			if name[-2:] == '_A':
				self._Model.drawOrder(50,name)
		
		self.SetPositionOffset()
		self.SetSize()
		self.SetPickable(False)
		
		self._FileName = fileName
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,position,ref):
		if self._Model != None and position != None:
			refModel = ref
			
			refModel.setPosition(position,mode=viz.ABS_GLOBAL)
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = self._Orientation
			refModel.setEuler(eulerAngle)
			
			self._Model.setParent(refModel)
			self._Model.setPosition(self._PositionOffset,mode=viz.ABS_PARENT)
			globalPosition = self._Model.getPosition(mode=viz.ABS_GLOBAL)
			self._Model.setParent(viz.WORLD)
			self._Model.setPosition(globalPosition,mode=viz.ABS_GLOBAL)

			self._Position = position
			
	#オフセット設定
	def SetPositionOffset(self):
		if self._Model != None:
			self._PositionOffset = [0.0,0.0,0.0]
			offset = [0.0,0.0,0.0]
			
			box = self._Model.getBoundingBox(mode=viz.ABS_GLOBAL)
			boxSize = box.getSize()
			boxCenter = box.getCenter()

			#offset = boxCenter

			offset[0] = boxCenter[0] * -1
			offset[1] = (boxCenter[1] - boxSize[1] / 2) * -1
			offset[2] = boxCenter[2] * -1
			
			self._PositionOffset[0] = offset[0]
			self._PositionOffset[1] = offset[1]
			self._PositionOffset[2] = offset[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#位置のオフセット値取得
	def GetPositionOffset(self):
		positionOffset = self._PositionOffset
		return positionOffset
	
	#角度設定
	def SetOrientation(self,orientation):
		if self._Model != None and orientation != None:
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
			self._Model.emissive(viz.RED)
			self._Model.ambient(viz.RED)
			self._Model.texture(None)
			self._Model.specular(viz.RED)
		else:
			self._Model.color(self._Color)
			self._Model.emissive(self._Emissive)
			self._Model.ambient(self._Ambient)
			self._Model.texture(self._Texture)
			self._Model.specular(self._Specular)
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#モデルのサイズを設定
	def SetSize(self):
		box = self._Model.getBoundingBox()
		self._Size =  box.size
		
	#モデルのサイズを取得
	def GetSize(self):
		size = self._Size
		
		return size
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
		
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		
	#モデルのファイル名を取得
	def GetFileName(self):
		name = self._FileName
		return name
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#窓のライン制御のクラス定義
class WindowLine(Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowLine] : Init WindowLine')
		
		self._WindowClass = None
		
		self._Model = None
		self._Picking = False
		
		self._Position = [0.0,0.0,0.0]
		self._PositionOffset = [0.0,0.0,0.0]
		self._Orientation = 0.0
		
		self._WallHeight = 3
		
		self._Width = 0.15
		self._Color = [0.50,0.50,0.75]
		
		self._Id = None
		self._DummyFlag = False
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WindowLine] : Start WindowLine Update')
		pass
		
	#追加
	def Add(self,windowClass):
		self._WindowClass = windowClass
		
		if self._Model != None:
			self.Delete()
		
		windowModel = self._WindowClass.GetModel()
		box = windowModel.getBoundingBox()
		modelSize = box.size
		modelCenter = box.center
		
		model = vizshape.addPlane([1.0,1.0])
		model.setScale([modelSize[0],1.0,self._Width])
		model.setPosition(0,self._WallHeight+0.2,0)
		model.disable(viz.LIGHTING)
		model.color(self._Color)
		
		self._Model = model
		
		self.SetPickable(False)
		self.SetHighlight(False)
		
		return model
		
	#削除
	def Delete(self):
		if self._Model != None:
			self._Model.remove()	#モデルを削除
		
		self._Model = None
	
	#位置設定
	def SetPosition(self,iPosition):
		if self._Model != None and iPosition != None:
			
			position = iPosition
			
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = self._WallHeight+0.2
			position[2] = position[2] + self._PositionOffset[2]
			
			self._Model.setPosition(position)
			self._Position = position
		
	#位置指定時のオフセット設定
	def SetPositionOffset(self,clickPosition):
		if self._Model != None and clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = self._Position[0] - clickPosition[0]
			self._PositionOffset[1] = self._Position[1] - clickPosition[1]
			self._PositionOffset[2] = self._Position[2] - clickPosition[2]
	
	#位置取得
	def GetPosition(self):
		position = self._Position
		return position
	
	#角度設定
	def SetOrientation(self,iOrientation):
		if self._Model != None and iOrientation != None:
			orientation = iOrientation
			
			eulerAngle = [0.0,0.0,0.0]
			eulerAngle[0] = orientation
			
			self._Model.setEuler(eulerAngle)
			self._Orientation = orientation
	
	#角度取得
	def GetOrientation(self):
		orientation = self._Orientation
		return orientation
	
	#表示状態を設定
	def SetVisible(self,state):
		if self._DummyFlag:
			self._Model.visible(False)	#ダミーは非表示
		else:
			self._Model.visible(state)
			
	#ハイライト表示
	def SetHighlight(self,state):
		if state == True:
			self._Model.color(viz.RED)
		else:
			self._Model.color(self._Color)
		
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
	
	#ID設定
	def SetId(self,id):
		self._Id = id
	
	#ID取得
	def GetId(self):
		id = self._Id
		
		return id
		
	#モデル取得
	def GetModel(self):
		model = self._Model
		
		return model
		
	#壁の高さ設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
		
	#ダミーフラグを設定
	def SetDummyFlag(self,state):
		self._DummyFlag = state
	
		if self._DummyFlag:
			self.SetVisible(False)
	
	#ダミーフラグを取得
	def GetDummyFlag(self):
		state = self._DummyFlag
		return state
		
# - - - - - - - - - - - - - - - - - - - - - - - - -
#図面制御のクラス定義
class Drawing(Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Init Drawing')
		
		self._Drawing = None
		self._DrawingName = ""
		
		self._TextureSize = [1.0,1.0]
		self._Scale = 1.0
		self._DrawingScale = [1.0,1.0,1.0]
		
		self._IsVisible = True
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Start Drawing Update')
		pass
		
	#追加
	def Add(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Add')
		
		loadState = False
		
		if fileName != '':
			#図面の読み込み
			tex = None
			
			if fileName[:2] == '..':
				fileName = viz.res.getPublishedPath(fileName)
				
			try:
				tex = viz.addTexture(fileName)
			except:
				tex = None
				
			if tex:
				#既にある図面を削除
				if self._DrawingName != "":
					self.Delete()
					
				#モデル作成
				texSize = tex.getSize()
				
				plane = vizshape.addPlane([1.0,1.0])
				plane.texture(tex)
				
				plane.disable(viz.PICKING)
				plane.disable(viz.LIGHTING)
				plane.alpha(0.5)
				
				plane.disable(viz.DEPTH_WRITE)
				plane.drawOrder(-10)
				plane.setPosition(0,0.1,0)
				
				self._Drawing = plane
				self._DrawingName = fileName
				self._TextureSize = [texSize[0],texSize[1]]
				
				self.SetScale(self._Scale)
				self.SetVisible(self._IsVisible)
				
				loadState = True
		
		return loadState
		
	#削除
	def Delete(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Drawing] : Delete')
		
		if self._DrawingName != "":
			self._Drawing.remove()
			del self._Drawing
			
			self._Drawing = None
			self._DrawingName = ""
			self._Scale = 1.0
			self._DrawingScale = [1.0,1.0,1.0]
	
	#サイズ設定
	def SetScale(self,scale):
		self._Scale = scale
		
		aspectratio = 1.0
		if self._TextureSize[0] != 0:
			aspectratio = float(self._TextureSize[1]) / float(self._TextureSize[0])
		
		if self._Drawing:
			self._DrawingScale = [self._Scale,1.0,self._Scale*aspectratio]
			self._Drawing.setScale(self._DrawingScale)
			
	#サイズ取得
	def GetScale(self):
		scale = self._Scale
		return scale
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.ON
		if state == True:
			self._IsVisible = True
			val = viz.ON
		else:
			self._IsVisible = False
			val = viz.OFF
			
		if self._Drawing != None:
			self._Drawing.visible(val)
			
	#表示状態取得
	def GetVisible(self):
		return self._IsVisible
		
	#ファイル名取得
	def GetFileName(self):
		fileName = self._DrawingName
		return fileName
		
	#図面のモデルを取得
	def GetDrawingModel(self):
		return self._Drawing
		
	#図面のスケールを取得
	def GetDrawingScale(self):
		return 	self._DrawingScale
		