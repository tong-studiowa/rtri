﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

#---------------------------------------------------------
from ctypes import *

#---------------------------------------------------------
class KienctV2():
	"""
	Kinectを制御くするクラス
	"""
	__mkinect = cdll.LoadLibrary("kinect/Kinectv2SharedMemory.dll")
	
	#---------------------------------------------------------
	def __init__(self):
		"""
		コンストラクタ
		"""		
		self.__mkinect.KinectV2SharedMemoryInit.restype	= c_int
		self.__mkinect.GetJointState.restype			= c_int
		self.__mkinect.GetRightHand.restype				= c_int
		self.__mkinect.GetLeftHand.restype				= c_int
		self.__mkinect.GetUserIDJointState.restype		= c_int
		self.__mkinect.GetUserIDRightHand.restype		= c_int
		self.__mkinect.GetUserIDLeftHand.restype		= c_int

		# データの読み込み状況を確認
		self.__mkinect.GetRefreshState.restype			= c_int
		self.__mkinect.GetUserNumber.restype			= c_int
		self.__mkinect.GetFirstUserID.restype			= c_int
		self.__mkinect.CheckUserIDTracking.restype		= c_int
		
		#以下初期化
		self.__mkinect.KinectV2SharedMemoryInit()
		
		#jointData
		self.__mJoint			= [[[0  for k in range(3)] for i in range(25)] for j in range(6)]
		self.__mJointNone		= [0 for i in range(6)]
		self.__mJointState		= [[0 for i in range(25)] for j in range(6)]
		self.__mBodyIdList		= [0 for i in range(6)]
		self.__mLeftHandList	= [0 for i in range(6)]
		self.__mRightHandList	= [0 for i in range(6)]
	
	#---------------------------------------------------------	
	def CheckUserIDTracking(self, id):
		return self.__mkinect.CheckUserIDTracking(c_int(id))
	
	#---------------------------------------------------------
	def GetJointPositionFromKinect(self, jointType, point):
		buf = POINTER(c_float) * 3
		self.__mkinect.GetJointPosition(c_int(jointType), buf)
		for i in range(3):
			point[i] = buf[i]
	
	#---------------------------------------------------------
	def GetJointState(self):
		return self.__mkinect.GetJointState()
	
	#---------------------------------------------------------
	def GetRightHand(self):
		return self.__mkinect.GetRightHand()
		
	#---------------------------------------------------------
	def GetLeftHand(self):
		return self.__mkinect.GetLeftHand()
	
	#---------------------------------------------------------	
	def GetUserIDJointPositionFromKinect(self, id, jointType, point):
		buf = cast((c_float * 4)(), POINTER(c_float))
		self.__mkinect.GetUserIDJointPosition(c_int(id), c_int(jointType), buf)
		for i in range(3):
			point[i] = buf[i]
		point[2] = -point[2]
		
	#---------------------------------------------------------
	def GetUserIDJointPosition(self, id, jointType):
		return self.__mJoint[id][jointType]
	
	#---------------------------------------------------------
	def GetUserIDJointPosGetUserIDJointStateition(self, id, jointType):
		return self.__mJointState[id][jointType]
	
	#---------------------------------------------------------
	def GetUserIDJointState(self, id, jointType):
		return self.__mkinect.GetUserIDJointState(c_int(id), c_int(jointType))
		
	#---------------------------------------------------------
	def GetUserIDRightHand(self, id):
		return self.__mkinect.GetUserIDRightHand(c_int(id))
		
	#---------------------------------------------------------
	def GetUserIDLeftHand(self, id):
		return self.__mkinect.GetUserIDLeftHand(c_int(id))
	
	#---------------------------------------------------------
	def GetUserNumber(self):
		self.__mkinect.GetUserNumber()
		
	#---------------------------------------------------------
	def GetFirstUserID(self):
		self.__mkinect.GetFirstUserID()
		
	#---------------------------------------------------------
	def CheckUserIDTrackingFromKinect(self, id):
		return self.__mkinect.CheckUserIDTracking(c_int(id))
		
	#---------------------------------------------------------
	def CheckUserIDTracking(self, id):
		return self.__mJointNone[id]

	#---------------------------------------------------------
	def GetTrakingBodyIdList(self):
		return self.__mBodyIdList
		
	#---------------------------------------------------------	
	def GetLeftHandList(self):
		return self.__mLeftHandList
		
	#---------------------------------------------------------	
	def GetRightHandList(self):
		return self.__mRightHandList
	
	#---------------------------------------------------------
	def GetFinedJointList(self):
		"""
		見つけた関節データを詰めて取得
		"""
		jointList = []
		count = 0
		for i in range(0, 6):
			if self.__mJointNone[i]:
				jointList.append(self.__mJoint[i])
		return jointList
		
	#---------------------------------------------------------
	def GetFinedJointStateList(self):
		"""
		見つけた関節状態データを詰めて取得
		"""
		stateList = []
		count = 0
		for i in range(0, 6):
			if self.__mJointNone[i]:
				stateList.append(self.__mJointState[i])
		return stateList
		
	#---------------------------------------------------------
	def GetFinedRightHandList(self):
		"""
		見つけた右手データを詰めて取得
		"""
		rightHandList = []
		count = 0
		for i in range(0, 6):
			if self.__mJointNone[i]:
				rightHandList.append(self.__mRightHandList[i])
		return rightHandList
		
	#---------------------------------------------------------
	def GetFinedLeftHandList(self):
		"""
		見つけた左手データを詰めて取得
		"""
		leftHandList = []
		count = 0
		for i in range(0, 6):
			if self.__mJointNone[i]:
				leftHandList.append(self.__mLeftHandList[i])
		return leftHandList
		
	#---------------------------------------------------------
	def GetFinedBodyIdList(self):
		"""
		見つけたボディIDデータを詰めて取得
		"""
		bodyIdList = []
		count = 0
		for i in range(0, 6):
			if self.__mJointNone[i]:
				bodyIdList.append(i)
		return bodyIdList
		
	#---------------------------------------------------------
	def SetJoint(self):
		if self.__mkinect.GetRefreshState():
			for i in range(0, 6):
				if self.CheckUserIDTrackingFromKinect(i):
					self.__mJointNone[i]		= 1
					self.__mBodyIdList[i]		= i#トラッキングされている関節のリストを作成
					self.__mLeftHandList[i]		= self.GetUserIDLeftHand(i)
					self.__mRightHandList[i]	= self.GetUserIDRightHand(i)
					
					for k in range(25):
						if(self.GetUserIDJointState(i, k)):
							self.__mJointState[i][k] = self.GetUserIDJointState(i, k)# 1,推測値; 2,計測値
							self.GetUserIDJointPositionFromKinect(i, k, self.__mJoint[i][k])
						else:
							self.__mJointState[i][k] = 0
				else:
					self.__mJointNone[i] = 0
					self.__mBodyIdList[i] = -1
			return True
		else:
			return False

