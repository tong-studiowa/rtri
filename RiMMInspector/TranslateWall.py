﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object3d
import LoadIniFile

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

translateSnapTemp = iniFile.GetItemData('Setting','GridStep')
translateSnap = float(translateSnapTemp) * 0.001	#入力時はミリメートル → メートル

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁移動制御のクラス定義
class TranslateWall(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateWall] : Init TranslateWall')
		
		self._InputClass = None
		self._RoomControlClass = None
		self._SelectWallClass = None
		self._ManipulatorClass = None
		self._UndoClass = None
		self._RedoClass = None

		self._SelectedRoom = None
		self._SelectedModel = None
		self._SelectedModelState = 0
		
		self._SelectedRoom2 = None
		self._SelectedModel2 = None
		
		self._SelectedModelList = []
		
		self._ManipulatorState = 0
		
		self._TranslateState = False
		self._PreTranslateState = False
		
		self._TranslateSnap = translateSnap
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,roomControlClass,selectWallClass,manipulatorClass,undoClass
				,redoClass):
		self._InputClass = inputClass
		self._RoomControlClass = roomControlClass
		self._SelectWallClass = selectWallClass
		self._ManipulatorClass = manipulatorClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][TranslateWall] : Start TranslateWall Update')
		
		self._TranslateState = False
		
		#マニピュレータの状態を取得
		manipulatorState = self._ManipulatorClass.GetState()
		
		if manipulatorState != 0 and manipulatorState != 2 and manipulatorState != 4 :
			self._SelectedRoom,self._SelectedModel,self._SelectedModelState = self._SelectWallClass.GetSelectedModel()
			self._SelectedModelList = self._SelectWallClass.GetSelectedModel2()
			
			if self._SelectedModel != None:
				self._TranslateState = True
				self.Translate(manipulatorState)
			
		#移動完了時
		if self._PreTranslateState == True and self._TranslateState == False:
			self._UndoClass.AddToList(self._UndoData)
			self._RedoClass.ClearList()
			
		self._PreTranslateState = self._TranslateState
		
	#移動
	def Translate(self,manipulatorState):
		position1 = [0.0,0.0,0.0]
		prePosition1 = [0.0,0.0,0.0]
		prePosition12 = [0.0,0.0,0.0]
		pickingPosition1 = [0.0,0.0,0.0]
		
		if self._SelectedModelState == 1:
			model = self._RoomControlClass.GetWallLineModel(self._SelectedRoom,self._SelectedModel)
			prePosition1 = model.getPosition(mode=viz.ABS_GLOBAL)
			
		elif self._SelectedModelState == 2:
			model = self._RoomControlClass.GetWallPointModel(self._SelectedRoom,self._SelectedModel)
			prePosition1 = model.getPosition(mode=viz.ABS_GLOBAL)
		
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		self._ManipulatorClass.SetPlanePickable(True)
		state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
		self._ManipulatorClass.SetPlanePickable(False)
		
		#床の位置だけオフセット
		floorPosition = self._RoomControlClass.GetFloorPosition(self._SelectedRoom)
		
		#スナップ
		pickingPosition[0] = self.Snap(pickingPosition[0])
		pickingPosition[1] = self.Snap(pickingPosition[1])
		pickingPosition[2] = self.Snap(pickingPosition[2])
		
		#移動
		pickingPosition1[0] = pickingPosition[0] - floorPosition[0]
		pickingPosition1[1] = pickingPosition[1] - floorPosition[1]
		pickingPosition1[2] = pickingPosition[2] - floorPosition[2]
		prePosition12[0] = prePosition1[0] - floorPosition[0]
		prePosition12[1] = prePosition1[1] - floorPosition[1]
		prePosition12[2] = prePosition1[2] - floorPosition[2]
		
		if manipulatorState == 1:	#XZ平面で移動
			position1[0] = pickingPosition1[0]
			position1[1] = prePosition12[1]
			position1[2] = pickingPosition1[2]
			
		elif manipulatorState == 3:	#X軸で移動
			position1[0] = pickingPosition1[0]
			position1[1] = prePosition12[1]
			position1[2] = prePosition12[2]
			
		elif manipulatorState == 5:	#Z軸で移動
			position1[0] = prePosition12[0]
			position1[1] = prePosition12[1]
			position1[2] = pickingPosition1[2]
			
		if self._TranslateState == True and self._PreTranslateState == False:
			self._UndoData = []
			self._UndoData.append(9)
			self._UndoData.append([self._SelectedModelState,self._SelectedRoom,self._SelectedModel,prePosition1])
			#print 'undoData=',9,self._SelectedModelState,self._SelectedRoom,self._SelectedModel,prePosition
			
		if self._SelectedModelState == 1:
			self._RoomControlClass.SetWallLinePosition(self._SelectedRoom,self._SelectedModel,position1)
			
		elif self._SelectedModelState == 2:
			self._RoomControlClass.SetWallPointPosition(self._SelectedRoom,self._SelectedModel,position1)
		
		#2つ目
		if self._SelectedModelList != []:
			for x in range(len(self._SelectedModelList)):
				position2 = [0.0,0.0,0.0]
				prePosition2 = [0.0,0.0,0.0]
				prePosition22 = [0.0,0.0,0.0]
				pickingPosition2 = [0.0,0.0,0.0]
				
				modelData = self._SelectedModelList[x]
				
				if self._SelectedModelState == 1:
					model2 = self._RoomControlClass.GetWallLineModel(modelData[0],modelData[1])
					prePosition2 = model2.getPosition(mode=viz.ABS_GLOBAL)
					
				elif self._SelectedModelState == 2:
					model2 = self._RoomControlClass.GetWallPointModel(modelData[0],modelData[1])
					prePosition2 = model2.getPosition(mode=viz.ABS_GLOBAL)
				
				offset = [0.0,0.0,0.0]
				offset[0] = prePosition2[0] - prePosition1[0]
				offset[1] = prePosition2[1] - prePosition1[1]
				offset[2] = prePosition2[2] - prePosition1[2]
				
				floorPosition2 = self._RoomControlClass.GetFloorPosition(modelData[0])
				
				#移動
				pickingPosition2[0] = pickingPosition[0] + offset[0] - floorPosition2[0]
				pickingPosition2[1] = pickingPosition[1] + offset[1] - floorPosition2[1]
				pickingPosition2[2] = pickingPosition[2] + offset[2] - floorPosition2[2]
				prePosition22[0] = prePosition2[0] - floorPosition2[0]
				prePosition22[1] = prePosition2[1] - floorPosition2[1]
				prePosition22[2] = prePosition2[2] - floorPosition2[2]
			
				if manipulatorState == 1:	#XZ平面で移動
					position2[0] = pickingPosition2[0]
					position2[1] = prePosition22[1]
					position2[2] = pickingPosition2[2]
					
				elif manipulatorState == 3:	#X軸で移動
					position2[0] = pickingPosition2[0]
					position2[1] = prePosition22[1]
					position2[2] = prePosition22[2]
					
				elif manipulatorState == 5:	#Z軸で移動
					position2[0] = prePosition22[0]
					position2[1] = prePosition22[1]
					position2[2] = pickingPosition2[2]
					
				if self._TranslateState == True and self._PreTranslateState == False:
					self._UndoData.append([self._SelectedModelState,modelData[0],modelData[1],prePosition2])
					#print 'undoData=',9,self._SelectedModelState,self._SelectedRoom,self._SelectedModel,prePosition
					
				if self._SelectedModelState == 1:
					self._RoomControlClass.SetWallLinePosition(modelData[0],modelData[1],position2)
					
				elif self._SelectedModelState == 2:
					self._RoomControlClass.SetWallPointPosition(modelData[0],modelData[1],position2)
			
	#スナップ
	def Snap(self,val):
		inputVal = val
		snapVal = self._TranslateSnap 
		res = 0.0
		
		a = abs(inputVal)%snapVal
		b = int(inputVal/snapVal)

		if a < snapVal/2:
			res = snapVal*b
		else:
			if inputVal > 0:
				res = snapVal*(b+1)
			else:
				res = snapVal*(b-1)

		return res

	#移動状態の確認
	def GetTranslateState(self):
		state = self._TranslateState
		return state
		