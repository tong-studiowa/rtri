﻿# coding: utf-8
#PathSimulator

import viz
import vizact
import vizshape
import viztask
import vizmat
import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

zoomModeTemp = iniFile.GetItemData('View','ZoomMode')
zoomMode = 1
try:
	zoomMode = int(zoomModeTemp)
except:
	zoomMode = 1
	
zoomWheelSpeedTemp = iniFile.GetItemData('View','ZoomWheelSpeed')
zoomWheelSpeed = 1.0
try:
	zoomWheelSpeed = float(zoomWheelSpeedTemp)
except:
	zoomWheelSpeed = 1.0
	
zoomDrugSpeedTemp = iniFile.GetItemData('View','ZoomDrugSpeed')
zoomDrugSpeed = 1.0
try:
	zoomDrugSpeed = float(zoomDrugSpeedTemp)
except:
	zoomDrugSpeed = 1.0
	
zoomDistVal = 1.0
"""
zoomDistValTemp = iniFile.GetItemData('View','ZoomDistVal')
try:
	zoomDistVal = float(zoomDistValTemp)
except:
	zoomDistVal = 1.0
"""

# - - - - - - - - - - - - - - - - - - - - - - - - -
#3D視点制御のクラス定義
class CameraControlClass():
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CameraControlClass] : Init CameraControlClass')
		
		#マウス、キー入力関係
		self._AltKeyState	= False
		
		self._IsActiveFlag			= True
		self._IsButtonDown_Left		= False
		self._IsButtonDown_Middle	= False
		self._IsButtonDown_Right	= False
		self._IsButtonDownFlag_Left	= False		# add takahashi 2014/10/10
		self._IsWheelMove			= 0
		self._IsWheelMoveTemp		= 0
		self._MousePosition			= [0.0,0.0]
		self._PreMousePosition		= [0.0,0.0]
		self._MouseDisplacement		= [0.0,0.0]
		
		#キーボード入力の処理開始
		#viz.callback(viz.KEYDOWN_EVENT,self.OnKeyDown)
		#viz.callback(viz.KEYUP_EVENT,self.OnKeyUp)
		
		#マウス入力の処理開始
		viztask.schedule(self.CheckMousePosition)				#マウスの位置取得
		viztask.schedule(self.CheckMouseWheelState)				#マウスホイールの状態取得
		
		#viz.callback(viz.MOUSEDOWN_EVENT,self.MouseDownEvent)	#マウスのボタンが押された
		#viz.callback(viz.MOUSEUP_EVENT,self.MouseUpEvent)		#マウスのボタンが離された
		#viz.callback(viz.MOUSEWHEEL_EVENT,self.MouseWheelEvent)	#マウスのホイールが操作された
		
		#視点操作関係
		self._TranslateSpeed = 10.0
		self._SpinSpeed = 100.0
		
		self._ZoomMode			= zoomMode
		self._ZoomWheelSpeed	= zoomWheelSpeed * 0.1
		self._ZoomDrugSpeed		= zoomDrugSpeed * -1
		self._ZoomDistVal		= zoomDistVal
		
		self._EyeHeight = [1.5,25.0]	#ウォークスルーの高さ、上空から見下ろす高さ
		
		#ターゲットの作成
		self._CameraTarget = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget.visible(viz.OFF)
		self._CameraTarget.disable(viz.PICKING)
		
		self._CameraTarget2 = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget2.visible(viz.OFF)
		self._CameraTarget2.disable(viz.PICKING)
		self._CameraTarget2.setParent(self._CameraTarget)
		
		self._CameraTarget3 = vizshape.addPlane([0.001,0.001,0.001])
		self._CameraTarget3.visible(viz.OFF)
		self._CameraTarget3.disable(viz.PICKING)
		self._CameraTarget3.setParent(self._CameraTarget2)
		self._CameraTarget3.setPosition([0.0,0.0,-10.0])
		
		self._CameraTarget2.setEuler([0,45,0])
		
		self._CameraPos = self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL)
		self._CameraTargetPos = [0,0,0]
		
		self._MoveViewKeyState	 = [False,False,False,False,False,False]
		self._RotateViewKeyState = [False,False,False,False]
		self._ResetViewKeyState	 = False
		
		self._MoveViewJoyState = [0.0,0.0,0.0,0.0,False,False]
		self._RotateViewJoyState = [0.0,0.0,0.0,0.0]
		self._ResetViewJoyState = False
		
		self.Reset()
		
		#ジョイスティックの設定
		self._mKey=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self._mPadKey=[0,0,0,0,0,0,0,0,0,0]
		self._mDeviceType=1
		
		self._mMoveVal	 = 2.0
		self._mRotateVal = 20.0
		
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(viz.res.getPublishedPath('Settings\\RiMMInspector_Settings.ini'))
		movespeed	= iniFile.GetItemData('Walkthrough','movespeed')
		rotatespeed	= iniFile.GetItemData('Walkthrough','rotatespeed')
		
		try:	self._mMoveVal = float(movespeed)
		except:	pass
		try:	self._mRotateVal = float(rotatespeed)
		except:	pass
		
		dinput = viz.add('DirectInput.dle')
		devices = dinput.getJoystickDevices()
		self._mJoy=None
		try:
			self._mJoy=dinput.addJoystick(devices[0])
			self._mJoy.setDeadZone(0.2)
		except:
			self._mJoy=None
	
	#毎フレーム呼び出されるための関数	
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CameraControlClass] : Start CameraControlClass Update')
		
		#マウスの移動量の確認
		self._MouseDisplacement[0] = self._MousePosition[0] - self._PreMousePosition[0]
		self._MouseDisplacement[1] = self._MousePosition[1] - self._PreMousePosition[1]
		
		self._PreMousePosition[0] = self._MousePosition[0]
		self._PreMousePosition[1] = self._MousePosition[1]
		
		#視点操作
		moveState = False
		
		if self._AltKeyState:
			
			#マウス左ボタン操作（回転）
			if self._IsButtonDown_Left:
				self.Spin(self._MouseDisplacement)
				moveState = True
			
			#マウス中ボタン操作（移動）
			if self._IsButtonDown_Middle:
				self.Translate(self._MouseDisplacement)
				moveState = True
				
			#マウス右ボタン操作（ズーム）
			if self._IsButtonDown_Right:
				self.Zoom(self._MouseDisplacement[1] * self._ZoomDrugSpeed)
				moveState = True
			
		#マウスホイール操作
		if self._IsWheelMove != 0:
			self.Zoom(self._IsWheelMove * self._ZoomWheelSpeed)
			moveState = True
			
		if self._mKey[0]==1:	#W
			self._Move(0,0,self._mMoveVal*1.0,True)
		if self._mKey[1]==1:	#d
			self._Move(self._mMoveVal*1.0,0,0)
		if self._mKey[2]==1:	#s
			self._Move(0,0,self._mMoveVal*-1.0,True)
		if self._mKey[3]==1:	#a
			self._Move(self._mMoveVal*-1.0,0,0)
		if self._mKey[8]==1:	#PAGE_UP
			self._Move(0,self._mMoveVal*1.0,0)
		if self._mKey[9]==1:	#PAGE_DOWN
			self._Move(0,self._mMoveVal*-1.0,0)
		if self._mKey[12]==1:	#HOME
			self.ResetRotationOfEyeView()
		
		if self._mKey[4]==1:
			self._Pan(self._mRotateVal*-1.0,0,0)
		if self._mKey[5]==1:
			self._Rotate(0,self._mRotateVal*1.0,0)
		if self._mKey[6]==1:
			self._Pan(self._mRotateVal*1.0,0,0)
		if self._mKey[7]==1:
			self._Rotate(0,self._mRotateVal*-1.0,0)
		
		#=================
		# ジョイパッド
		#=================
		
		if self._mJoy:
			
			pos = self._mJoy.getPosition()
			rot = self._mJoy.getRotation()
			hat = self._mJoy.getHat()
			
			if self._mDeviceType==1:
				rot=[pos[2],rot[2],0]
			
			#移動
			if abs(pos[1])>0.2:
				self._Move(0,0,self._mMoveVal*pos[1],True)
			if abs(pos[0])>0.2:
				self._Move(self._mMoveVal*pos[0],0,0)
			
			#視点操作
			if abs(rot[0])>0.2:
				self._Rotate(0,self._mRotateVal*rot[0],0)
				self._Pan(0,self._mRotateVal*rot[0],0)
			if abs(rot[1])>0.2:
				self._Pan(self._mRotateVal*rot[1],0,0)
			
			#視点リセット
			if self._mJoy.isButtonDown(2):	# Xbox360 -> X
				self._mPadKey[2]=1
				self.ResetRotationOfEyeView()
			
			#画面切り替え
			if self._mJoy.isButtonDown(6):	# Xbox360 -> X
				self._mPadKey[9]=1
			
			#スタート位置ジャンプ
			if self._mJoy.isButtonDown(7):	# Xbox360 -> X
				self._mPadKey[8]=1
				#self.InitPositionAndAngle()
			
			#視点位置移動
			if self._mJoy.isButtonDown(4):	# RB
				self._SetEyePos(-1)
			if self._mJoy.isButtonDown(5):	# LB
				self._SetEyePos(1)
	
		#カメラ位置を更新
		if moveState:
			viz.MainView.setPosition(self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL))
			viz.MainView.setEuler(self._CameraTarget3.getEuler(mode=viz.ABS_GLOBAL))
		
	#リセットするための関数
	def Reset(self):
		self._CameraTarget.setPosition(self._CameraTargetPos[0],self._CameraTargetPos[1],self._CameraTargetPos[2])
		viz.MainView.setPosition(self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL))
		viz.MainView.setEuler(self._CameraTarget3.getEuler(mode=viz.ABS_GLOBAL))
		
	#スタート位置に設定する為の関数
	def SetStartPos(self,pos,euler):
		self._CameraTarget.setPosition(pos)
		self._CameraTarget.setEuler([euler[0],0,0])
		self._CameraTarget2.setEuler([0,euler[1],0])
		self._CameraTarget3.setPosition([0.0,0.0,-10.0])
		viz.MainView.setPosition(self._CameraTarget3.getPosition(mode=viz.ABS_GLOBAL))
		viz.MainView.setEuler(self._CameraTarget3.getEuler(mode=viz.ABS_GLOBAL))
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		self.SetCameraTargetPos()
		
	#レイアウターから受け取った情報を設定
	def SetViewPos(self,data):
		self._CameraTarget.setPosition(data[0][0])
		self._CameraTarget.setEuler(data[0][1])
		
		self._CameraTarget2.setPosition(data[1][0])
		self._CameraTarget2.setEuler(data[1][1])
		
		self._CameraTarget3.setPosition(data[2][0])
		self._CameraTarget3.setEuler(data[2][1])
		
		viz.MainView.setPosition(data[3][0])
		viz.MainView.setEuler(data[3][1])
		
		self._CameraPos = data[3][0]
		self._CameraTargetPos = data[0][0]
		
	#レイアウターへ渡す情報を設定
	def GetViewPos(self):
		target1Pos	 = self._CameraTarget.getPosition()
		target1Euler = self._CameraTarget.getEuler()
		
		target2Pos	 = self._CameraTarget2.getPosition()
		target2Euler = self._CameraTarget2.getEuler()
		
		target3Pos	 = self._CameraTarget3.getPosition()
		target3Euler = self._CameraTarget3.getEuler()
		
		viewPos	 	 = viz.MainView.getPosition()
		viewEuler	 = viz.MainView.getEuler()
		
		return [[target1Pos,target1Euler],[target2Pos,target2Euler],[target3Pos,target3Euler],[viewPos,viewEuler]]
		
	#移動
	def Translate(self,displacementVal):
		dist = self.GetDistance()
		
		x = displacementVal[0] * -self._TranslateSpeed
		y = displacementVal[1] * -self._TranslateSpeed
		
		viz.MainView.move(x,y,dist)
		newPos = viz.MainView.getPosition()
		self._CameraTarget.setPosition(newPos)
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#回転
	def Spin(self,displacementVal):
		x = displacementVal[0] * self._SpinSpeed
		y = displacementVal[1] * -self._SpinSpeed
		
		#横方向
		angleX = self._CameraTarget.getEuler()
		angleX[0] = angleX[0] + x
		self._CameraTarget.setEuler(angleX)
		
		#縦方向
		angleY = self._CameraTarget2.getEuler()
		angleY[0] = 0.0
		angleY[1] = angleY[1] + y
		angleY[2] = 0.0
		
		#真上、真下を超えないよう制限
		if angleY[1] > 89.0:
			angleY[1] = 89.0
		elif angleY[1] < -89.0:
			angleY[1] = -89.0
			
		self._CameraTarget2.setEuler(angleY)
		
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#ズーム
	def Zoom(self,wheelVal):
		dist = self.GetDistance()
		if self._ZoomMode == 0:
			dist = 1.0
			wheelVal *= 10.0
			
		z = wheelVal * (dist * self._ZoomDistVal)
		
		pos = self._CameraTarget3.getPosition()
		pos[2] = pos[2] + z
		
		self._CameraTarget3.setPosition(pos)
		
		#回転の中心からの距離が1m以下の場合、中心をずらす
		dist = self.GetDistance()
		if dist < 1.0:
			offset = 1.0 - dist
			pos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
			
			self._CameraTarget3.setPosition([0.0,0.0,offset])
			newPos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
			
			self._CameraTarget.setPosition(newPos)
			self._CameraTarget3.setPosition([0.0,0.0,-1.0])
			
		self._CameraPos = viz.MainView.getPosition()
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
	#回転の中心から視点までの距離を取得
	def GetDistance(self):
		pos = self._CameraTarget3.getPosition()
		distance = pos[2] * -1
		return distance
		
	#カメラターゲットの位置を設定
	def SetCameraTargetPos(self):
		#位置設定
		pos = viz.MainView.getPosition()
		
		targetpos = self._CameraTarget.getPosition(viz.ABS_GLOBAL)
		target3pos = self._CameraTarget3.getPosition(viz.ABS_GLOBAL)
		
		offsetPos = [0,0,0]
		offsetPos[0] = targetpos[0] - target3pos[0]
		offsetPos[1] = targetpos[1] - target3pos[1]
		offsetPos[2] = targetpos[2] - target3pos[2]
		
		newPos = [0,0,0]
		newPos[0] = pos[0] + offsetPos[0]
		newPos[1] = pos[1] + offsetPos[1]
		newPos[2] = pos[2] + offsetPos[2]
		
		self._CameraTarget.setPosition(newPos)
		self._CameraTargetPos = self._CameraTarget.getPosition()
		
		#角度設定
		angle = viz.MainView.getEuler()
		targetAngle = [0,0,0]
		targetAngle[0] = angle[0]
		self._CameraTarget.setEuler(targetAngle)
		
		targetAngle2 = [0,0,0]
		targetAngle2[1] = angle[1]
		self._CameraTarget2.setEuler(targetAngle2)
		
	def OnKeyDown(self,key): 
		if key == viz.KEY_ALT_L or key == viz.KEY_ALT_R:	#Altキー
			self._AltKeyState = True
		
		#視点操作
		if key=='w':
			self._mKey[0]=1
		elif key=='d':
			self._mKey[1]=1
		elif key=='s':
			self._mKey[2]=1
		elif key=='a':
			self._mKey[3]=1
		if key==viz.KEY_UP:
			self._mKey[4]=1
		elif key==viz.KEY_RIGHT:
			self._mKey[5]=1
		elif key==viz.KEY_DOWN:
			self._mKey[6]=1
		elif key==viz.KEY_LEFT:
			self._mKey[7]=1
		elif key==viz.KEY_PAGE_UP:
			self._mKey[8]=1
		elif key==viz.KEY_PAGE_DOWN:
			self._mKey[9]=1
		elif key==' ':
			self._mKey[11]=1
		elif key==viz.KEY_HOME:
			self._mKey[12]=1
		elif key=='m':
			self._mKey[16]=1
		elif key==viz.KEY_SHIFT_L or key==viz.KEY_SHIFT_R:
			self._mKey[17]=1
	
	def OnKeyUp(self,key): 
		if key == viz.KEY_ALT_L or key == viz.KEY_ALT_R:			#Altキー
			self._AltKeyState = False
		
		if key==viz.KEY_UP:
			self._mKey[4]=0
		elif key==viz.KEY_RIGHT:
			self._mKey[5]=0
		elif key==viz.KEY_DOWN:
			self._mKey[6]=0
		elif key==viz.KEY_LEFT:
			self._mKey[7]=0
		elif key=='w':
			self._mKey[0]=0
		elif key=='d':
			self._mKey[1]=0
		elif key=='s':
			self._mKey[2]=0
		elif key=='a':
			self._mKey[3]=0
		elif key==viz.KEY_PAGE_UP:
			self._mKey[8]=0
		elif key==viz.KEY_PAGE_DOWN:
			self._mKey[9]=0
		elif key==viz.KEY_BACKSPACE:
			self._mKey[10]=0
		elif key==' ':
			self._mKey[11]=0
		elif key==viz.KEY_HOME:
			self._mKey[12]=0
		elif key=='y':
			self._mKey[13]=0
		elif key=='n':
			self._mKey[14]=0
		elif key==viz.KEY_RETURN:
			self._mKey[15]=0
		elif key=='m':
			self._mKey[16]=0
		elif key==viz.KEY_SHIFT_L or key==viz.KEY_SHIFT_R:
			self._mKey[17]=0
		else:
			pass
	
	#マウスのボタンが離された状態から押された状態に切り替わったタイミングで呼び出される関数	
	def MouseDownEvent(self,button):
		if button == viz.MOUSEBUTTON_LEFT:
			self._IsButtonDown_Left = True
			self._IsButtonDownFlag_Left = True	# add takahashi 2014/10/10
		if button == viz.MOUSEBUTTON_RIGHT:
			self._IsButtonDown_Right = True
		if button == viz.MOUSEBUTTON_MIDDLE:
			self._IsButtonDown_Middle = True
	
	#マウスのボタンが押された状態から離された状態に切り替わったタイミングで呼び出される関数	
	def MouseUpEvent(self,button):
		if button == viz.MOUSEBUTTON_LEFT:
			self._IsButtonDown_Left = False
			self._IsButtonDownFlag_Left=False	# add takahashi 2014/10/10
		if button == viz.MOUSEBUTTON_RIGHT:
			self._IsButtonDown_Right = False
		if button == viz.MOUSEBUTTON_MIDDLE:
			self._IsButtonDown_Middle = False
	
	#マウスのホイールが操作されたときに呼び出される関数
	def MouseWheelEvent(self,val):
		self._IsWheelMoveTemp = val
	
	#マウス位置を確認するための関数
	def CheckMousePosition(self):
		while True:
			x,y = viz.mouse.getPosition()	#マウス位置を取得
			self._MousePosition[0] = x
			self._MousePosition[1] = y
			
			yield viztask.waitFrame(1)
	
	#マウスホイールの状態を確認するための関数
	def CheckMouseWheelState(self):
		while True:
			self._IsWheelMove = self._IsWheelMoveTemp
			if self._IsWheelMoveTemp != 0:
				self._IsWheelMoveTemp = 0
				
			yield viztask.waitFrame(1)
	
	#追加（高橋）
	#マウスクリック時の立ち上がりを取得する為の関数
	def CheckMouseButtonLeft(self):
		"""
		if self._IsButtonDownFlag_Left:
			self._IsButtonDownFlag_Left=False
			return True
		return False
		"""
		if self._IsButtonDownFlag_Left == False:
			self._IsButtonDownFlag_Left=True
			return True
		return False
		
	def IsDownBackSpaceKey(self):
		if self._mKey[10]!=0:
			self._mKey[10]=0
			return True
		self._mKey[10]=0
		return False	
	def IsDownSpaceKey(self):
		if self._mKey[11]!=0:
			self._mKey[11]=0
			return True
		self._mKey[11]=0
		return False
	def IsDownHomeKey(self):
		if self._mKey[12]!=0:
			self._mKey[12]=0
			return True
		self._mKey[12]=0
		return False
	def IsDownPadButtonA(self):
		if self._mPadKey[0]!=0:
			self._mPadKey[0]=0
			return True
		self._mPadKey[0]=0
		return False
	def IsDownPadButtonB(self):
		if self._mPadKey[1]!=0:
			self._mPadKey[1]=0
			return True
		self._mPadKey[1]=0
		return False
	def IsDownPadButtonX(self):
		if self._mPadKey[2]!=0:
			self._mPadKey[2]=0
			return True
		self._mPadKey[2]=0
		return False
	def IsDownPadButtonY(self):
		if self._mPadKey[3]!=0:
			self._mPadKey[3]=0
			return True
		self._mPadKey[3]=0
		return False
	def IsDownPadButtonBack(self):
		if self._mPadKey[9]!=0:
			self._mPadKey[9]=0
			return True
		self._mPadKey[9]=0
		return False
	def IsDownPadStartButton(self):
		if self._mPadKey[8]!=0:
			self._mPadKey[8]=0
			return True
		self._mPadKey[8]=0
		return False
	def IsDownKeyY(self):
		if self._mKey[13]!=0:
			self._mKey[13]=0
			return True
		self._mKey[13]=0
		return False
	def IsDownKeyN(self):
		if self._mKey[14]!=0:
			self._mKey[14]=0
			return True
		self._mKey[14]=0
		return False
	def IsDownKeyEnter(self):
		if self._mKey[15]==1:
			self._mKey[15]=2
			return True
		#self._mKey[15]=0
		return False
	#def IsDownKeyC(self):
	#	if self._mKey[16]==1:
	#		self._mKey[16]=2
	#		return True
	#	#self._mKey[16]=0
	#	return False
	def IsDownKeyM(self):
		if self._mKey[16]==1:
			self._mKey[16]=2
			return True
		#self._mKey[16]=0
		return False
	
	def _Move(self,iX,iY,iZ,keepHeight=False):
		if not(self._IsActiveFlag):
			return
		prePos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
		elapsedTime = viz.getFrameElapsed()
		moveVal = [iX*elapsedTime,iY*elapsedTime,iZ*elapsedTime]
		viz.MainView.move(moveVal)
		if keepHeight:
			newPos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			newPos[1] = prePos[1]
			viz.MainView.setPosition(newPos,mode=viz.ABS_GLOBAL)
		self.SetCameraTargetPos()
	def _Rotate(self,iX,iY,iZ):
		if not(self._IsActiveFlag):
			return
		currentEuler = viz.MainView.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,currentEuler[1],currentEuler[2]]
		viz.MainView.setEuler(rotateVal)
		self.SetCameraTargetPos()
	def _Pan(self,iX,iY,iZ):
		if not(self._IsActiveFlag):
			return
		currentEuler = viz.MainView.getEuler()
		elapsedTime = viz.getFrameElapsed()
		rotateVal = [currentEuler[0]+iY*elapsedTime,currentEuler[1]+iX*elapsedTime,currentEuler[2]]
		viz.MainView.setEuler(rotateVal)
		self.SetCameraTargetPos()
	def _SetEyePos(self,val=1):
		if not(self._IsActiveFlag):
			return
		currentPos = viz.MainView.getPosition()
		nextHeight = currentPos[1] + 1.0*val*viz.getFrameElapsed()
		if nextHeight>10.0 :
			nextHeight=10.0 
		elif nextHeight<0.2:
			nextHeight=0.2
		viz.MainView.setPosition([currentPos[0],nextHeight,currentPos[2]])
		self.SetCameraTargetPos()
	def ResetRotationOfEyeView(self):
		if not(self._IsActiveFlag):
			return
		shiftKeyState = self._mKey[17]
		#print 'shiftKeyState',shiftKeyState
		if shiftKeyState:
			ang1=viz.MainView.getEuler()
			ang2=viz.MainView.getEuler()
			viz.MainView.setEuler(0,90,0)
			viz.MainView.setEuler(0,90,0)
			pos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			viz.MainView.setPosition([pos[0],self._EyeHeight[1],pos[2]],mode=viz.ABS_GLOBAL)
		else:
			ang1=viz.MainView.getEuler()
			ang2=viz.MainView.getEuler()
			viz.MainView.setEuler(ang1[0],0,0)
			viz.MainView.setEuler(ang2[0],0,0)
			pos = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			viz.MainView.setPosition([pos[0],self._EyeHeight[0],pos[2]],mode=viz.ABS_GLOBAL)
		self.SetCameraTargetPos()
		
	def SetActiveInput(self,isactive):
		self._IsActiveFlag=isactive
		
	def SetEyeHeight(self,num,val):
		self._EyeHeight[num] = val * 0.001
		
	def GetEyeHeight(self,num):
		val = self._EyeHeight[num] * 1000.0
		return val
		