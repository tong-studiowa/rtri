﻿# WallPoint
# Develop		: Python v2.7.2 / Vizard4.06.0138
# CharacterCode	: utf-8
# LastUpdate	: 2014/4/1
# Programer		: nishida

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#壁の点制御のクラス定義
class WallPoint(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self,floorPositoin,pointList,height,isOpeRoom):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallPoint] : Init WallPoint')
		
		self._FloorPosition = floorPositoin
		
		self._PointList = pointList
		self._Height = height
		
		self._Radius = 0.2
		self._ModelList = []
		
		self._Color = viz.BLACK
		
		self._PositionOffset = [0.0,0.0,0.0]
		
		self._IsOpRoom = isOpeRoom
		
		self.Create()
		
		#壁の点を表示
		self.SetVisible(True)
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][WallPoint] : Start WallPoint Update')
		pass
		
	#壁モデルの作成
	def Create(self):
		self._ModelList = []
		
		for x in range(len(self._PointList)):
			point = self._PointList[x]
			
			#球作成
			sphere = vizshape.addSphere(self._Radius)
	#		sphere.disable(viz.LIGHTING)
			sphere.color(self._Color)
		
			self.SetModelPosition(sphere,point)
			self.SetPickable(False)
			
			self._ModelList.append(sphere)
		
	#壁の点モデルの削除
	def Delete(self):
		for model in self._ModelList:
			model.remove()	#モデルを削除
			del model
				
		self._ModelList = []
		
	#壁の点の表示状態を設定
	def SetVisible(self,state):
		visibleState = viz.OFF
		if state == True:
			visibleState = viz.ON
		else:
			visibleState = viz.OFF
			
		for line in self._ModelList:
			line.visible(visibleState)
			
		self.ReCreate()
		
	#壁のハイライト表示
	def SetHighlight(self,num,state):
		color = self._Color
		if state == True:
			color = viz.RED
		else:
			color = self._Color

		point =  self._ModelList[num]
		point.color(color)
		
		self.ReCreate()
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x] == model:
				num =  x
		
		return num
		
	#モデルを取得
	def GetModel(self,num):
		model = self._ModelList[num] 
		
		return model
		
	#選択可能状態の設定
	def SetPickable(self,state):
		for model in self._ModelList:
			if state == True:
				model.enable(viz.PICKING)
			elif state == False:
				model.disable(viz.PICKING)
				
	#位置設定
	def SetPosition(self,num,iPosition):
		model = self._ModelList[num]
		
		if model != None and iPosition != None:
			position = iPosition
			position[0] = position[0] + self._PositionOffset[0]
			position[1] = 80.0
			position[2] = position[2] + self._PositionOffset[2]
			
			model.setPosition(position)
			
			point = [0.0,0.0]
			point[0] = position[0]
			point[1] = position[2]
			
			self.SetPoint(num,point)
			
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,clickPosition):
		point = self.GetPoint(num)
		
		if clickPosition != None:
			self._PositionOffset = [0.0,0.0,0.0]
			self._PositionOffset[0] = point[0] - clickPosition[0]
			self._PositionOffset[1] = self._Height+0.1
			self._PositionOffset[2] = point[1] - clickPosition[2]
	
	#位置取得
	def GetPosition(self,num):
		position = [0.0,0.0,0.0]
		point = self.GetPoint(num)
		
		position[0] = point[0]
		position[1] = self._Height+0.1
		position[2] = point[1]
		
		return position
		
	#ポイントを取得
	def GetPoint(self,num):
		point = self._PointList[num]
		
		return point
		
	#ポイントを設定
	def SetPoint(self,num,point):
		self._PointList[num] = point
		
	#フロアのポイントリストを取得
	def GetFloorPointList(self):
		list = self._PointList
		
		return list
		
	#ポイントの移動
	def Move(self,pointList):
		self._PointList = pointList
		
		self.ReCreate()
		
	#再配置
	def ReCreate(self):
		for x in range(len(self._PointList)):
			point = self._PointList[x]
			model = self._ModelList[x]
			
			self.SetModelPosition(model,point)
			
	#位置再設定
	def SetModelPosition(self,model,point):
		#位置
		position = [0.0,0.0,0.0]
		position[0] = point[0] + self._FloorPosition[0]
		position[1] = 80.0
		position[2] = point[1] + self._FloorPosition[2]
		
		#選択中は上にずらす
		color = model.getColor()
		if color[0] == 1.0 and color[1] == 0.0 and color[2] == 0.0:
			position[1] = 85.0
			
		if self._IsOpRoom == 1:
			position[1] = position[1] - 1.0
			
		model.setPosition(position)
		
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._Height = wallHeight
	
	#壁のポイントを再設定
	def SetPointList(self,pointList):
		self._PointList = pointList
		
		self.Delete()
		self.Create()
		
	#床の位置を再設定
	def SetFloorPosition(self,floorPosition):
		self._FloorPosition = floorPosition
		
		self.ReCreate()
		