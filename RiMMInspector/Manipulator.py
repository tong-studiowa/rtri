﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizshape
import vizmat

import Object3d

import math
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

# - - - - - - - - - - - - - - - - - - - - - - - - -
#マニピュレータ制御のクラス定義
class Manipulator(Object3d.Object3d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Manipulator] : Init Manipulator')
		
		self._InputClass = None
		self._FunctionButtonAreaClass = None
		self._View2dClass = None
		self._BackgroundClass = None
		self._StencilClass = None
		self._CheckStateClass = None
		
		self._SelectedModel = None
		self._State = 0
		self._PreState = 0
		self._ChangeState = False
		self._PivotState = False
		
		self._ViewState = True
		self._ArrowYVisibleState = 1
		self._Circle2VisibleState = 1
		
		self._AlphaVal = 0.5
		self._AlphaValArrow = 0.3
		
		#3D用モデル
		self._Model = viz.addChild(viz.res.getPublishedPath('Resource\\Interface\\Manipulator.osgb'))

		self._Model.drawOrder(100)
		
		self._Model.drawOrder(106,node='Plane')
		self._Model.disable(viz.DEPTH_TEST,node='Plane')
		
		self._Model.drawOrder(105,node='ArrowX')
		self._Model.disable(viz.DEPTH_TEST,node='ArrowX')

		self._Model.drawOrder(104,node='ArrowY')
		self._Model.disable(viz.DEPTH_TEST,node='ArrowY')

		self._Model.drawOrder(103,node='ArrowZ')
		self._Model.disable(viz.DEPTH_TEST,node='ArrowZ')

		self._Model.drawOrder(102,node='Circle1')
		self._Model.disable(viz.DEPTH_TEST,node='Circle1')

		self._Model.drawOrder(101,node='Circle2')
		self._Model.disable(viz.DEPTH_TEST,node='Circle2')
		
		self._Model.alpha(self._AlphaVal)
		self._Model.alpha(self._AlphaValArrow,node='ArrowX')
		self._Model.alpha(self._AlphaValArrow,node='ArrowY')
		self._Model.alpha(self._AlphaValArrow,node='ArrowZ')
		self._Model.alpha(0.0,node='Plane')
	
		#2D用モデル
		self._Model2d = viz.addChild(viz.res.getPublishedPath('Resource\\Interface\\Manipulator2d.osgb'))
		
		self._Model2d.drawOrder(100)
		
		self._Model2d.drawOrder(106,node='Plane')
		self._Model2d.disable(viz.DEPTH_TEST,node='Plane')
		
		self._Model2d.drawOrder(105,node='ArrowX')
		self._Model2d.disable(viz.DEPTH_TEST,node='ArrowX')
		
		self._Model2d.drawOrder(103,node='ArrowZ')
		self._Model2d.disable(viz.DEPTH_TEST,node='ArrowZ')

		self._Model2d.drawOrder(102,node='Circle1')
		self._Model2d.disable(viz.DEPTH_TEST,node='Circle1')

		self._Model2d.drawOrder(101,node='Circle2')
		self._Model2d.disable(viz.DEPTH_TEST,node='Circle2')
		
		self._Model2d.alpha(self._AlphaVal)
		self._Model2d.alpha(self._AlphaValArrow,node='ArrowX')
		self._Model2d.alpha(self._AlphaValArrow,node='ArrowZ')
		self._Model2d.alpha(0.0,node='Plane')
	
		#移動用板モデル
		self._PlaneModel = vizshape.addPlane([100,100])
		self._PlaneModel.alpha(0)
		
		#初期設定
		self.SetPickable(False)
		self.SetVisible(False)
		self.SetPlanePickable(False)
		
	#使用するクラスの追加
	def AddClass(self,inputClass,functionButtonAreaClass,view2dClass,backgroundClass,stencilClass
				,checkStateClass):
		self._InputClass = inputClass
		self._FunctionButtonAreaClass = functionButtonAreaClass
		self._View2dClass = view2dClass
		self._BackgroundClass = backgroundClass
		self._StencilClass = stencilClass
		self._CheckStateClass = checkStateClass
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Manipulator] : Start Manipulator Update')
		
		#モデルを選択中
		if self._SelectedModel != None:
			self.SetVisible(True)
			
			self.SetPosition()
		
			if self._InputClass.GetMouseStartDragState() and self._FunctionButtonAreaClass.GetOverState() != True and self._InputClass.GetShiftKeyState() == False and self._StencilClass.GetChangeClickState() == False and self._CheckStateClass.GetOverAndModeState() != True:
				self._State = self.Select()
				
				self.SetPlaneAngle(self._State)
				
				#マニピュレータ操作開始
				if self._State != 0:
					pass
				
			#マニピュレータ操作停止
			elif self._InputClass.GetMouseFinDragState():
				self._State = 0
			
			#マニピュレータ操作中
			elif self._State != 0:
				pass
			
			#マニピュレータのサイズを一定にする
			self.SetScale()
			
		#モデル未選択
		else:
			self.SetVisible(False)
		
		self._ChangeState = False
		if self._State != self._PreState:
			self._ChangeState = True
			
		self._PreState = self._State
		
	#選択
	def Select(self):
		state = False
		pickedModel = None
		pickingPosition = [0.0,0.0,0.0]
		pickedModelName = ""
		
		res = 0		#State=0 : 未選択
		
		if self._InputClass.GetAltKeyState() == False:		#Altキーを押していない場合
			self._BackgroundClass.SetPickable(False)
			self.SetPickable(True)
			
			state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
			
			self._BackgroundClass.SetPickable(True)
			self.SetPickable(False)
			
			if self.IsManipulaterModel(pickedModel):
				if pickedModelName == "Circle1" or pickedModelName == "Plane" :		#State=1 : XZ平面で移動
					res = 1
				elif pickedModelName == "Circle2":									#State=2 : Y軸で回転
					res = 2
				elif pickedModelName == "ArrowX" or pickedModelName == "ArrowX2":	#State=3 : X軸で移動
					res = 3
				elif pickedModelName == "ArrowY" or pickedModelName == "ArrowY2":	#State=4 : Y軸で移動
					res = 4
				elif pickedModelName == "ArrowZ" or pickedModelName == "ArrowZ2":	#State=5 : Z軸で移動
					res = 5
		
		return res
		
	#表示状態を設定
	def SetVisible(self,state):
		if state == True:
			if self._ViewState == True:
				self._Model.visible(viz.OFF)
				self._Model2d.visible(viz.ON)
				
				"""
				#2Dの回転用円モデルの表示設定
				if self._Circle2VisibleState == 1:
					self._Model2d.alpha(self._AlphaVal,node='Circle2')
					self._Model2d.enable(viz.PICKING,node='Circle2')
				elif self._Circle2VisibleState == 0:
					self._Model2d.alpha(0,node='Circle2')
					self._Model2d.disable(viz.PICKING,node='Circle2')
				"""
				
			else:
				self._Model.visible(viz.ON)
				self._Model2d.visible(viz.OFF)
				
				#3DのY軸モデルの表示設定
				if self._ArrowYVisibleState == 1:
					self._Model.alpha(self._AlphaVal,node='ArrowY')
					self._Model.enable(viz.PICKING,node='ArrowY')
				elif self._ArrowYVisibleState == 0:
					self._Model.alpha(0,node='ArrowY')
					self._Model.disable(viz.PICKING,node='ArrowY')
				
		elif state == False:
			self._Model.visible(viz.OFF)
			self._Model2d.visible(viz.OFF)
			
	#選択可能状態の設定
	def SetPickable(self,state):
		if state == True:
			self._Model.enable(viz.PICKING)
			self._Model2d.enable(viz.PICKING)
		elif state == False:
			self._Model.disable(viz.PICKING)
			self._Model2d.disable(viz.PICKING)

	#移動用板モデルの選択可能状態の設定
	def SetPlanePickable(self,state):
		if state == True:
			self._PlaneModel.enable(viz.PICKING)
		elif state == False:
			self._PlaneModel.disable(viz.PICKING)
		
	#位置を設定
	def SetPosition(self):
		position = [0.0,0.0,0.0]
		
		modelPosition = self._SelectedModel.getPosition(mode=viz.ABS_GLOBAL)
		
		box = self._SelectedModel.getBoundingBox(viz.ABS_GLOBAL)
		modelSize = box.getSize()
		modelCenter = box.getCenter()
		
		if self._PivotState == False:
			position = modelPosition
		else:
			position = modelCenter
			
		self._Model.setPosition(position)
		self._PlaneModel.setPosition(position)
		self._Model2d.setPosition(position)
		
	#サイズを一定に設定
	def SetScale(self):
		modelPosition = self._Model.getPosition(mode=viz.ABS_GLOBAL)
		viewPosition = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
		
		distance = vizmat.Distance(modelPosition,viewPosition)
		
		scaleVal = distance * 0.15
		scale = [1,1,1]
		scale[0] = scaleVal
		scale[1] = scaleVal
		scale[2] = scaleVal
		
		self._Model.setScale(scale)
		
		scale = self._View2dClass.GetViewScale()
		scaleVal2d = scale * 0.15
		scale2d = [1,1,1]
		scale2d[0] = scaleVal2d
		scale2d[2] = scaleVal2d
		
		self._Model2d.setScale(scale2d)
		
	#モデルを設定
	def SetModel(self,model,state=False):
		self._SelectedModel = model
		self._PivotState = state
			
	#モデルを取得
	def GetModel(self):
		model = self._SelectedModel
		
		return model
		
	#マニピュレータ用モデルかどうか
	def IsManipulaterModel(self,model):
		res = False
		if model == self._Model or model == self._Model2d:
			res = True
		
		return res
			
	#視点の状態を設定
	def SetViewState(self,state):
		self._ViewState = state
		
	#マニピュレータを操作中か
	def GetState(self):
		state = self._State
		
		return state
		
	#直前の操作中かを取得
	def GetChangeState(self):
		state = self._ChangeState
		return state
		
	#移動用板モデルの角度設定
	def SetPlaneAngle(self,state):
		if state == 4:	#Y軸方向
			planePosition = self._PlaneModel.getPosition(mode=viz.ABS_GLOBAL)
			viewPosition = viz.MainView.getPosition(mode=viz.ABS_GLOBAL)
			
			planeRadian = math.atan2(planePosition[0]-viewPosition[0],planePosition[2]-viewPosition[2])
			planeAngle = math.degrees(planeRadian)
			self._PlaneModel.setEuler(planeAngle,-90.0,0.0)
			
		else:						#XZ軸方向
			self._PlaneModel.setEuler(0,0,0)
			
	#縦方向の移動用矢印モデルの表示設定
	def SetArrowYVisible(self,state):
		self._ArrowYVisibleState = state
		
	#回転用円モデルの表示設定
	def SetCircle2Visible(self,state):
		self._Circle2VisibleState = state
		