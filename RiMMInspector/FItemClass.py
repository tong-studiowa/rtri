﻿# FItemClass
# Develop       : Python v2.7.2 / Vizard4.05.0095
# LastUpdate    : 2012/12/5
# Programer     : nishida

import viz
import F3DObjectClass
from win32api import OutputDebugString

class FItemClass(F3DObjectClass.F3DObjectClass):

# member
	#mStyle         = None
	#mState         = None
	#mPhysicsType   = None
	#mAnimation     = None
	#mMaterial      = None
	#mTexture       = None

# method
	# called method at first
	def __init__(self):
		F3DObjectClass.F3DObjectClass.__init__(self)
		self.mStyle         = None
		self.mState         = None
		self.mPhysicsType   = None
		self.mAnimation     = None
		self.mMaterial      = None
		self.mTexture       = None

	# called method at delete
	def __del__(self):
		#OutputDebugString("[StudioWa][INFO] Delete FItemClass")
		if self.mAnimation!=None:
			self.mAnimation	= None
		if self.mMaterial!=None:
			self.mMaterial	= None
		if self.mTexture!=None:
			self.mTexture	= None
		F3DObjectClass.F3DObjectClass.__del__(self)

	# set style
	def SetStyle(self,style):
		self.mStyle = style

	# return style
	def GetStyle(self):
		return self.mStyle

	# set state
	def SetState(self,state):						#temp(未完)
		self.mState = state

	# return state
	def GetState(self):
		return self.mState

	# set physics type
	def SetPhysicsType(self,physicsType):			#temp(未完)
		self.mPhysicsType = physicsType

	# return physics type
	def GetPhysicsType(self):
		return self.mPhysicsType

	# set animation
	def SetAnimation(self,animation):
		animationParameter = vizact.animation(animation)
		self.GetModel().addAction(animationParameter)
		self.mAnimation = animation

	# return animation
	def GetAnimation(self):
		return self.mAnimation

	# set material
	def SetMaterial(self,material):					#temp(未完)
		self.mMaterial = material

	# return material
	def GetMaterial(self):
		return self.mMaterial
