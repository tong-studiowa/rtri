﻿# coding: utf-8

import viz
import vizinput
import vizfx
import LoadIniFile
from os import path
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = ''

# - - - - - - - - - - - - - - - - - - - - - - - - -
#アイテム読み込み用クラス定義
class ItemData():
	#コンストラクタ
	def __init__(self,itemControl):
		OutputDebugString('[STUDIOWA][INFO][LoadItemDataClass] : Init LoadItemDataClass')
		self._ItemControlClass = itemControl
		
		"""
		self._ItemControlClass = ItemControl()
		self._ItemControlClass.Load(SETTING_FOLDER_PATH + fileName)
		"""
	
	#デストラクタ
	def __del__(self):
		OutputDebugString('[STUDIOWA][INFO][LoadItemDataClass] : del LoadItemDataClass')
		"""
		modelCount = self._ItemControlClass.GetItemCount()
		for x in range(modelCount):
			self._ItemControlClass.Delete(0)
		"""
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadItemDataClass] : Start LoadItemDataClass Update')
		pass
		
	#アイテムの読み込み
	def Load(self,filePath):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][LoadRoomDataClass] : Start LoadRoomDataClass Load')
		self._ItemControlClass.Load(filePath)
	
	#INIファイルにアイテムを保存する
	def Save(self,filePath):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][ItemControl] : Save')
		self._ItemControlClass.Save(filePath)
		
	#アイテム数を取得
	def GetItemCount(self):
		count = self._ItemControlClass.GetItemCount()
		return count
		
	#アイテムモデルのリストを取得
	def GetModelList(self):
		modelList = self._ItemControlClass.GetModelList()
		return modelList
		
	#読み込みに成功したかどうかを判定する --- takahashi 2014/8/1
	def IsSuccessLoading(self):
		return self._ItemControlClass.IsSuccessLoading()
		
	#照明モデルのリストを取得
	def GetLightModelList(self):
		modelList = self._ItemControlClass.GetLightModelList()
		return modelList
		
	#照明以外のモデルのリストを取得
	def GetModelWithoutLightList(self):
		modelList = self._ItemControlClass.GetModelWithoutLightList()
		return modelList
		
	#ダミーボックスのラインの表示を設定
	def SetDummyBoxLineVisible(self,state):
		self._ItemControlClass.SetDummyBoxLineVisible(state)
		
	#アイテムの総数を取得
	def GetItemCount(self):
		count = self._ItemControlClass.GetItemCount()
		return count
		
	#パスシム用のパスの表示設定
	def ChangeLineVisible(self,flag):
		self._ItemControlClass.ChangeLineVisible(flag)
		
	#パスシム用パスを作成
	def CreateItemAnimationPath(self):
		self._ItemControlClass.CreateItemAnimationPath()
		
	#全アイテムのパスの表示状態設定
	def SetAllItemLineVisible(self,state):
		self._ItemControlClass.SetAllItemLineVisible(state)
			
	#全アイテムのパスの表示状態設定
	def SetAllItemLineColor(self,state):
		self._ItemControlClass.SetAllItemLineColor(state)
			
	#移動の開始
	def Play(self):
		self._ItemControlClass.Play()
		
	#移動の一時停止
	def Pause(self):
		self._ItemControlClass.Pause()
		
	#移動の一時停止
	def Pause(self):
		self._ItemControlClass.Pause()
		
	#移動の停止、リセット
	def Reset(self):
		self._ItemControlClass.Reset()
		
	#移動の停止、最終地点にジャンプ
	def End(self):
		self._ItemControlClass.End()
		
	#アニメーション再生時の状態更新
	def AnimationUpdate(self):
		self._ItemControlClass.AnimationUpdate()
		
	#ラインモデルのリストを取得
	def GetLineModelList(self):
		return self._ItemControlClass.GetLineModelList()
		
	#パス付きのアイテムのリストを取得
	def GetItemWithPathList(self):
		count = self._ItemControlClass.GetItemWithPathList()
		return count
		