﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz

import Object2d
import AddItem
import AddDoor
import AddWindow
import AddRoom
import AddCharacter

import LoadIniFile
from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = 'Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#設定ファイルの読み込み
iniFile = LoadIniFile.LoadIniFileClass()
iniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+'RiMMInspector_Settings.ini'))

stencilIniFilePath = iniFile.GetItemData('Setting','Stencil')
stencilIniFile = LoadIniFile.LoadIniFileClass()
stencilIniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+stencilIniFilePath))

soundsIniFilePath = iniFile.GetItemData('Setting','Sounds')
soundsIniFile = LoadIniFile.LoadIniFileClass()
soundsIniFile.LoadIniFile(viz.res.getPublishedPath(SETTING_FOLDER_PATH+soundsIniFilePath))

# - - - - - - - - - - - - - - - - - - - - - - - - -
#ステンシル制御のクラス定義
class Stencil(Object2d.Object2d):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Stencil] : Init Stencil')
		
		self._AddItemClass = AddItem.AddItem()
		self._AddDoorClass = AddDoor.AddDoor()
		self._AddWindowClass = AddWindow.AddWindow()
		self._AddRoomClass = AddRoom.AddRoom()
		self._AddCharacterClass = AddCharacter.AddCharacter()
		
		self._InputClass = None
		self._ItemControlClass = None
		self._RoomControlClass = None
		self._SelectWallClass = None
		self._CharacterControlClass = None
		self._SelectModelClass = None
		self._ModelControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._ClickState = False
		self._PreClickState = False
		self._ChangeClickState = False
		self._ClickSliderState = False
		self._OverState = False
		self._OverTitleState = False
		self._OverIconState = None
		self._OverSliderState = False
		
		self._VisibleState = True
		
		self._KeyState = False
		self._PreKeyState = False
		
		self._CategoryCount = 0
		for x in range(100):
			data = stencilIniFile.GetItemData('Category',str(x))
			if data == None:
				self._CategoryCount = x
				break
			
		self._CategoryNum = 0
		self._CategoryType = 0
		self._CategoryName = ""
		
		self._IconCount = 9
		self._ShowIconCount = 9
		self._IconList = []
		self._IconFrameList = []
		self._IconTextureList = []
		self._IconTextList = []
		self._ModelNameList = []
		self._ModelMoveYStateList = []
		
		self._ModelNum = None
		
		windowSize  = viz.window.getSize()
		
		self._SliderPositionMin = 71
		self._SliderPositionMax = windowSize[1]-89
		
		self._CategoryModelCount = 5
		self._IconOffset = 0
		self._PreIconOffset = 0
		
		self._ItemAngle = 0
		self._CharacterAngle = 180
		
		#フレーム追加
		self._FramePosition = [88,windowSize[1]/2-30]
		self._FrameScale = [150,windowSize[1]-97]
		
		self._Frame = self.CreateBgFrame(self._FramePosition,self._FrameScale)
		
		#タイトル追加
		self._TitleFramePosition = [88,37]
		self._TitleFrameScale = [140,35]
		
		self._TitleFrame = self.CreateFrame(self._TitleFramePosition,self._TitleFrameScale)
		
		self._TextPosition = [88,37]
		self._TextScale = [18.6,18.6]
		
		self._Text = self.CreateText("",self._TextPosition,self._TextScale)
		
		#アイコン、テキスト追加
		self._IconPosition = [76,119]
		self._IconScale = [115,115]
		self._IconPositionList = []
		self._TextPositionList = []
		
		for x in range(self._IconCount):
			
			iconPos = [0.0,0.0]
			iconPos[0] = self._IconPosition[0]
			iconPos[1] = self._IconPosition[1] + (121 * x)
			self._IconPositionList.append(iconPos)
			
			icon = self.CreateIcon(iconPos,self._IconScale)
			self._IconList.append(icon)
			
			iconFrame = self.CreateFrame(iconPos,self._IconScale)
			self._IconFrameList.append(iconFrame)
		
			textPos = [0.0,0.0]
			textPos[0] = self._IconPosition[0]
			textPos[1] = self._IconPosition[1] + (121 * x) + 45
			self._TextPositionList.append(textPos)
			
			iconText = self.CreateText("",textPos,self._TextScale)
			self._IconTextList.append(iconText)
			
		#スライダー追加
		self._SliderFramePosition = [148,(windowSize[1]-89)/2+36]
		self._SliderFrameScale = [20,windowSize[1]-149]
	
		self._SliderFrame = self.CreateSliderFrame(self._SliderFramePosition,self._SliderFrameScale)
		
		self._SliderPosition = [148,self._SliderPositionMin]
		self._SliderScale = [20,20]
		
		self._Slider = self.CreateSlider(self._SliderPosition,self._SliderScale)
		
		self._UndoData = []
		
	#使用するクラスの追加
	def AddClass(self,inputClass,itemControlClass,roomControlClass,selectWallClass,characterControlClass
				,selectModelClass,modelControlClass,undoClass,redoClass):
		self._InputClass = inputClass
		self._ItemControlClass = itemControlClass
		self._RoomControlClass = roomControlClass
		self._SelectWallClass = selectWallClass
		self._CharacterControlClass = characterControlClass
		self._SelectModelClass = selectModelClass
		self._ModelControlClass = modelControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
		self.AddClassToFunction()
		self.Reset()
		
	#各ファンクションへの使用するクラスの追加
	def AddClassToFunction(self):
		self._AddItemClass.AddClass(self._ModelControlClass)
		self._AddRoomClass.AddClass(self._RoomControlClass)
		self._AddDoorClass.AddClass(self._RoomControlClass,self._ModelControlClass)
		self._AddWindowClass.AddClass(self._RoomControlClass,self._ModelControlClass)
		self._AddCharacterClass.AddClass(self._ModelControlClass)
			
	#毎フレーム呼び出されるための関数
	def Update(self):
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Stencil] : Start Stencil Update')
		
		self._OverState = False
		self._OverTitleState = False
		self._OverIconState = None
		self._OverSliderState = False
		
		if self._VisibleState == True:
			mousePosition = self._InputClass.GetMousePosition()
			self._OverState = self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale)
			
			if self._OverState:
				self._OverTitleState = self.CheckRollOver(mousePosition,self._TitleFramePosition,self._TitleFrameScale)
				self._OverSliderState = self.CheckRollOver(mousePosition,self._SliderFramePosition,self._SliderFrameScale)
				
				#ホイール操作
				mouseWheelState = self._InputClass.GetMouseWheelState()
				if mouseWheelState != 0 and self._ClickState == False and self._ClickSliderState == False:
					self._IconOffset,newPercentage = self.GetOffsetNumByWheel(mouseWheelState)
					if self._IconOffset != self._PreIconOffset:
						self.SetSliderState(newPercentage)
						self.SetIcon()
						self._PreIconOffset = self._IconOffset
						
				if self._OverSliderState:
					#スライダーのドラッグ開始
					if self._InputClass.GetMouseStartDragState():
						self._ClickSliderState = True
						
				else:
					num = None
					for x in range(self._IconCount):
						if self._IconList[x].getVisible():
							if self.CheckRollOver(mousePosition,self._IconPositionList[x],self._IconScale):
								num = x
								
					self._OverIconState = num
					
					if self._OverIconState != None:
						#ドラッグ開始
						if self._InputClass.GetMouseStartDragState():
							self._ClickState = True
								
							#ドラッグ用モデル選択
							self._ModelNum = self._OverIconState
							modelName = self.GetModelName(self._ModelNum)
							
							if self._CategoryType == 0:		#アイテム
								self._AddItemClass.Add(modelName)
								self._AddItemClass.SetOrientation(self._ItemAngle)
							elif self._CategoryType == 1:	#ドア
								wallHeight = self._RoomControlClass.GetWallHeightTop()
								self._AddDoorClass.SetWallHeight(wallHeight)
								self._AddDoorClass.Add(modelName)
							elif self._CategoryType == 2:	#窓
								wallHeight = self._RoomControlClass.GetWallHeightTop()
								self._AddWindowClass.SetWallHeight(wallHeight)
								self._AddWindowClass.Add(modelName)
							elif self._CategoryType == 3:	#部屋
								self._AddRoomClass.Add(modelName)
							elif self._CategoryType == 4:	#キャラクター
								self._AddCharacterClass.Add(modelName)
								self._AddCharacterClass.SetOrientation(self._CharacterAngle)
				
			#ドラッグ終了
			if self._InputClass.GetMouseFinDragState():
				#モデルを追加して配置
				if self.CheckRollOver(mousePosition,self._FramePosition,self._FrameScale) == False:
					if self._ModelNum != None:
						modelName = self.GetModelName(self._ModelNum)
						moveYState = self.GetMoveYState(self._ModelNum)
						
						if self._CategoryType == 0:		#アイテム
							positionTemp = self._AddItemClass.GetPosition()
							position = [0,0,0]
							position[0] = positionTemp[0]
							position[1] = positionTemp[1]
							position[2] = positionTemp[2]
							
							modelNum = self._ItemControlClass.Add(modelName)
							self._ItemControlClass.SetPosition(modelNum,position)
							self._ItemControlClass.SetOrientation(modelNum,self._ItemAngle)
							self._ItemControlClass.SetMoveYState(modelNum,moveYState)
							self._ItemControlClass.AddNewItemToList(modelNum)
							
							if self._CategoryName ==  soundsIniFile.GetItemData('Sounds','Category'):	#音源の場合
								sound = soundsIniFile.GetItemData('Sounds',str(self._ModelNum))
								self._ItemControlClass.SetSound(modelNum,sound)
								
							self._UndoData = []
							self._UndoData.append(13)
							self._UndoData.append([modelNum])
							self._UndoClass.AddToList(self._UndoData)
							self._RedoClass.ClearList()
							
						elif self._CategoryType == 1:	#ドア
							roomNum = None
							wallNum = None
							wallState = 0
							
							roomNum,wallNum,wallState = self._SelectWallClass.Select(True)
							
							if wallState == 1:
								percentage = self._AddDoorClass.GetPercentage()
								modelNum = self._RoomControlClass.AddDoor(roomNum,modelName,wallNum,percentage)
								
								self._UndoData = []
								self._UndoData.append(19)
								self._UndoData.append([roomNum,modelNum])
								self._UndoClass.AddToList(self._UndoData)
								self._RedoClass.ClearList()
								
								self._RoomControlClass.SetDoorVisible(roomNum,False)
								
						elif self._CategoryType == 2:	#窓
							roomNum = None
							wallNum = None
							wallState = 0
							
							roomNum,wallNum,wallState = self._SelectWallClass.Select(True)
							
							if wallState == 1:
								percentage = self._AddWindowClass.GetPercentage()
								modelNum = self._RoomControlClass.AddWindow(roomNum,modelName,wallNum,percentage)
								
								self._UndoData = []
								self._UndoData.append(21)
								self._UndoData.append([roomNum,modelNum])
								self._UndoClass.AddToList(self._UndoData)
								self._RedoClass.ClearList()
								
								self._RoomControlClass.SetWindowVisible(roomNum,False)
								
						elif self._CategoryType == 3:	#部屋
							positionTemp = self._AddRoomClass.GetPosition()
							position = [0,0,0]
							position[0] = positionTemp[0]
							position[1] = positionTemp[1]
							position[2] = positionTemp[2]
							
							roomNum = self._RoomControlClass.Add(modelName)
							self._RoomControlClass.SetFloorPosition(roomNum,position)
							self._RoomControlClass.AddNewRoomToList(roomNum)
							
							#追加した間取りを選択
							roomCount = self._RoomControlClass.GetRoomCount()
							newRoomList = [roomCount-1]
							self._SelectModelClass.ReSelectRoom(newRoomList)
							
							self._UndoData = []
							self._UndoData.append(15)
							self._UndoData.append([roomNum,modelName,position])
							self._UndoClass.AddToList(self._UndoData)
							self._RedoClass.ClearList()
							
						elif self._CategoryType == 4:	#キャラクター
							positionTemp = self._AddCharacterClass.GetPosition()
							position = [0,0,0]
							position[0] = positionTemp[0]
							position[1] = positionTemp[1]
							position[2] = positionTemp[2]
							
							modelNum = self._CharacterControlClass.Add(modelName)
							self._CharacterControlClass.SetPosition(modelNum,position)
							self._CharacterControlClass.SetOrientation(modelNum,self._CharacterAngle)
							
							self._UndoData = []
							self._UndoData.append(26)
							self._UndoData.append([modelNum])
							self._UndoClass.AddToList(self._UndoData)
							self._RedoClass.ClearList()
							
				#ドラッグ用モデル削除
				if self._CategoryType == 0:		#アイテム
					self._AddItemClass.Delete()
				elif self._CategoryType == 1:	#ドア
					self._AddDoorClass.Delete()
				elif self._CategoryType == 2:	#窓
					self._AddWindowClass.Delete()
				elif self._CategoryType == 3:	#部屋
					self._AddRoomClass.Delete()
				elif self._CategoryType == 4:	#キャラクター
					self._AddCharacterClass.Delete()
					
				self._ModelNum = None
		
				self._ClickState = False
				self._ClickSliderState = False
				
			#ドラッグ中
			if self._ModelNum != None:
				
				#ドラッグ用モデル配置
				state = False
				pickedModel = None
				pickingPosition = [0.0,0.0,0.0]
				pickedModelName = ""
				
				state,pickedModel,pickingPosition,pickedModelName = self._InputClass.PickModel()
				
				roomNum = None
				wallNum = None
				wallState = 0
				
				roomNum,wallNum,wallState = self._SelectWallClass.Select(True)
				
				point0 = [0,0]
				point1 = [0,0]
				
				if wallState == 1:
					point0,point1 = self._RoomControlClass.GetWallPoint(roomNum,wallNum)
				
				if self._CategoryType == 0:		#アイテム
					self._AddItemClass.SetPosition(pickingPosition)
				elif self._CategoryType == 1:	#ドア
					self._AddDoorClass.SetPosition(pickingPosition,wallNum,wallState,point0,point1,roomNum)
				elif self._CategoryType == 2:	#窓
					self._AddWindowClass.SetPosition(pickingPosition,wallNum,wallState,point0,point1,roomNum)
				elif self._CategoryType == 3:	#部屋
					self._AddRoomClass.SetPosition(pickingPosition)
				elif self._CategoryType == 4:	#キャラクター
					self._AddCharacterClass.SetPosition(pickingPosition)
			
			#スライダー関係
			if self._ClickSliderState == True:
				sliderPercentage = self.SetSliderPosition(mousePosition)
				self._IconOffset = self.GetOffsetNum(sliderPercentage)
				if self._IconOffset != self._PreIconOffset:
					self.SetIcon()
					self._PreIconOffset = self._IconOffset
				
			#ジャンル選択
			if self._OverTitleState == True:
				if self._InputClass.GetMouseClickState():
					if self._InputClass.GetShiftKeyState():
						self.ChangeCategory(True)
					else:
						self.ChangeCategory()
			
			#ロールオーバー
			if self._ClickState == False and self._ClickSliderState == False:
				self.RolloverTitle(self._OverTitleState)
				self.RolloverIcon(self._OverIconState)
				self.RolloverSlider(self._OverSliderState)
			
			#キー操作
			self._KeyState = self._InputClass.GetTabKeyState()
			if self._KeyState == False and self._PreKeyState == True:
				if self._InputClass.GetShiftKeyState():
					self.ChangeCategory(True)
				else:
					self.ChangeCategory()
			
			self._PreKeyState = self._KeyState
		
		self._ChangeClickState = False
		if self._ClickState != self._PreClickState:
			self._ChangeClickState = True
			
		self._PreClickState = self._ClickState
		
	#リセット処理
	def Reset(self):
		self._CategoryNum = 0
		self._VisibleState = True
		self._ClickState = False
		self._PreClickState = False
		self._ChangeClickState = False
		self._ClickSliderState = False
		
		self.SetCategory(self._CategoryNum)
		self.SetAspectRatio()
		
	#表示状態設定
	def SetVisible(self,state):
		val = viz.OFF
		if state == True:
			self._VisibleState = True
			val = viz.ON
		else:
			self._VisibleState = False
			val = viz.OFF
			
		self._Frame.visible(val)
		self._TitleFrame.visible(val)
		self._Text.visible(val)
		
		for icon in self._IconList:
			icon.visible(val)
			
		for iconFrame in self._IconFrameList:
			iconFrame.visible(val)
		
		for iconText in self._IconTextList:
			iconText.visible(val)
			
		self._Slider.visible(val)
		self._SliderFrame.visible(val)
		
		if state == True:
			self.SetCategory(self._CategoryNum)
		
	#カテゴリ設定
	def SetCategory(self,num):
		if num > self._CategoryCount-1:
			return
			
		self._CategoryNum = num
		
		categorySetting = stencilIniFile.GetItemData('Category',str(num))
		self._CategoryName = categorySetting.split(',')[0]
		self._CategoryType = int(categorySetting.split(',')[1])
		
		self._Text.message(self._CategoryName)
		
		categoryModelCount = 0
		for x in range(100):
			data = stencilIniFile.GetItemData(self._CategoryName,str(x))
			if data == None:
				categoryModelCount = x
				break
				
		self._CategoryModelCount = categoryModelCount
		
		self.ResetSlider()
		self.SetIcon()
		
	#アイコン設定
	def SetIcon(self):
		self.SetShowIconCount()
		
		self._ModelNameList = []
		self._IconTextureList = []
		self._ModelMoveYStateList = []
		
		for x in range(self._IconCount):
			num = int(x + self._IconOffset)
			data = stencilIniFile.GetItemData(self._CategoryName,str(num))
			
			if data != None and x < self._ShowIconCount:
				modelName = data.split(',')[0]
				textureName = data.split(',')[1]
				text = data.split(',')[2]
				moveY = 1
				try:
					moveYTemp = data.split(',')[3]
					moveY = int(moveYTemp)
				except:	#値がなかったら1にする（過去のもの同じように）
					moveY = 1
					
				self._ModelNameList.append(modelName)
				self._IconTextureList.append(textureName)
				self._ModelMoveYStateList.append(moveY)
				
				texture = viz.addTexture(viz.res.getPublishedPath(textureName))
				
				icon = self._IconList[x]
				icon.texture(texture)
				icon.visible(viz.ON)
				
				iconFrame = self._IconFrameList[x]
				iconFrame.visible(viz.ON)
				
				iconText = self._IconTextList[x]
				iconText.message(text)
				iconText.visible(viz.ON)
				
			else:
				icon = self._IconList[x]
				icon.visible(viz.OFF)
				
				iconFrame = self._IconFrameList[x]
				iconFrame.visible(viz.OFF)
				
				iconText = self._IconTextList[x]
				iconText.visible(viz.OFF)
				
	#モデル名の取得
	def GetModelName(self,num):
		modelName = self._ModelNameList[num]
		
		return modelName
		
	#モデルの縦方向の移動可能情報の取得
	def GetMoveYState(self,num):
		moveYState = self._ModelMoveYStateList[num]
		
		return moveYState
		
	#クリック状態の取得
	def GetClickState(self):
		state = False
		if self._ClickState or self._ClickSliderState:
			state = True
			
		return state
		
	#直前のクリック状態の取得
	def GetChangeClickState(self):
		state = self._ChangeClickState
		return state
		
	#重なり状態の取得
	def GetOverState(self):
		state = self._OverState
		return state
		
	#カテゴリ変更
	def ChangeCategory(self,state=False):
		num = self._CategoryNum
		if state:
			num = num - 1
			data = stencilIniFile.GetItemData('Category',str(num))
			if data == None:
				num = self._CategoryCount - 1
		else:
			num = num + 1
			data = stencilIniFile.GetItemData('Category',str(num))
			if data == None:
				num = 0
				
		self._CategoryNum = num
		
		self.SetCategory(self._CategoryNum)
		
	#カテゴリ数の取得
	def GetCategoryCount(self):
		return self._CategoryCount
		
	#タイトルのロールオーバー処理
	def RolloverTitle(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._TitleFrame.color(color)
		
	#アイコンのロールオーバー処理
	def RolloverIcon(self,selectedIcon):
		for x in range(len(self._IconList)):
			icon = self._IconList[x]
			
			alpha = 0.85
			if x == selectedIcon:
				alpha = 1.0
			
			icon.alpha(alpha)
		
	#スライダーのロールオーバー処理
	def RolloverSlider(self,state):
		color = viz.GRAY
		if state == True:
			color = [0.575,0.575,0.575]
			
		self._Slider.color(color)
	
	#スライダーの位置設定
	def SetSliderPosition(self,mousePosition):
		prePosition = self._Slider.getPosition()
		windowSize  = viz.window.getSize()
		
		position = [0,0]
		position[0] = prePosition[0]*windowSize[0]
		position[1] = (1.0-mousePosition[1])*windowSize[1]
		
		if position[1] < self._SliderPositionMin:
			position[1] = self._SliderPositionMin
		elif position[1] > self._SliderPositionMax:
			position[1] = self._SliderPositionMax
		
		self._SliderPosition[1] = position[1]
		self.SetIconPosition(self._Slider,self._SliderPosition,self._SliderScale)
		
		percentage = self.GetSliderState()
		
		return percentage
		
	#スライダーをリセット
	def ResetSlider(self):
		self.SetSliderPosition([1,1])
		self._IconOffset = self.GetSliderState()
		self._PreIconOffset = self._IconOffset
		
	#スライダーの数値を取得
	def GetSliderState(self):
		windowSize  = viz.window.getSize()
		positionTemp = self._Slider.getPosition()
		sliderPosition = (1.0-positionTemp[1])*windowSize[1]
		
		percentage = 0.0
		if (self._SliderPositionMax - self._SliderPositionMin) != 0:
			percentage = (sliderPosition - self._SliderPositionMin) / (self._SliderPositionMax - self._SliderPositionMin)
		
		if percentage < 0.002:
			percentage = 0.0
		elif percentage > 0.998:
			percentage = 1.0
			
		return percentage
		
	#スライダーの位置を設定
	def SetSliderState(self,percentage):
		windowSize  = viz.window.getSize()
		position = self._Slider.getPosition()
		position[1] = 1.0 - (((self._SliderPositionMax - self._SliderPositionMin)*percentage) + self._SliderPositionMin) / windowSize[1]
		self._Slider.setPosition(position)
		
	#スライダーによるリストのオフセット値を取得
	def GetOffsetNum(self,percentage):
		self.SetShowIconCount()

		iconOffset = 0
		if self._CategoryModelCount > self._ShowIconCount:
			over = self._CategoryModelCount - self._ShowIconCount + 1
			iconOffset = over - 1
			
			a = 1.0 / over	
			for x in range(over):
				b = a * (x + 1)
				if percentage < b:
					iconOffset = x
					break
					
		return iconOffset
		
	#表示するアイコンの数を設定
	def SetShowIconCount(self):
		windowSize  = viz.window.getSize()
		
		iconCount = 9
		for x in range(self._IconCount):
			scaleLimt = self._IconPositionList[x][1] + 140
			if windowSize[1] < scaleLimt:
				iconCount = x
				break
		
		self._ShowIconCount = iconCount
		
	#ホイールによるリストのオフセット値を取得
	def GetOffsetNumByWheel(self,wheelStata):
		newOffset = self._IconOffset - wheelStata
		if newOffset < 0:
			newOffset = 0
		
		percentage = 0.0
		
		self.SetShowIconCount()

		iconOffset = 0
		if self._CategoryModelCount > self._ShowIconCount:
			over = self._CategoryModelCount - self._ShowIconCount + 1
			iconOffset = over - 1
			
			a = 1.0 / over	
			for x in range(over):
				b = a * (x + 1)
				if x == newOffset:
					iconOffset = x
					if x == 0:
						percentage = 0.0
					else:
						percentage = b
					break
					
		return iconOffset,percentage
		
	#アイテムの向き設定
	def SetItemAngle(self,angle):
		self._ItemAngle = angle
		
	#画角を設定
	def SetAspectRatio(self):
		windowSize  = viz.window.getSize()
		
		self._SliderPositionMax = windowSize[1]-89
		if self._SliderPositionMax < self._SliderPositionMin:
			self._SliderPositionMax = self._SliderPositionMin + 1
		
		self._FramePosition = [88,windowSize[1]/2-30]
		self._FrameScale = [150,windowSize[1]-97]
		self.SetIconPosition(self._Frame,self._FramePosition,self._FrameScale)
		
		self.SetIconPosition(self._TitleFrame,self._TitleFramePosition,self._TitleFrameScale)
		self.SetIconPosition(self._Text,self._TextPosition,self._TextScale)
		
		for x in range(self._IconCount):
			self.SetIconPosition(self._IconList[x],self._IconPositionList[x],self._IconScale)
			self.SetIconPosition(self._IconFrameList[x],self._IconPositionList[x],self._IconScale)
			self.SetIconPosition(self._IconTextList[x],self._TextPositionList[x],self._TextScale)
			
		self._SliderFramePosition = [148,(windowSize[1]-89)/2+36]
		self._SliderFrameScale = [20,windowSize[1]-149]
		self.SetIconPosition(self._SliderFrame,self._SliderFramePosition,self._SliderFrameScale)
		
		self._SliderPosition = [148,self._SliderPositionMin]
		self.SetIconPosition(self._Slider,self._SliderPosition,self._SliderScale)
		
		self.ResetSlider()
		self.SetIcon()
		self.SetVisible(self._VisibleState)
		