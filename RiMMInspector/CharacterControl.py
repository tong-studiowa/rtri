﻿# coding: utf-8

import viz
import vizinput
import vizmat

import Interface
import Character

import LoadIniFile
from win32api import OutputDebugString

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#キャラクター制御のクラス定義
class CharacterControl(Interface.Interface):
	
	#コンストラクタ
	def __init__(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : Init CharacterControl')
		
		self._SelectModelClass = None
		self._PathControlClass = None
		self._ModelControlClass = None
		self._UndoClass = None
		self._RedoClass = None
		
		self._IniFileName = ""
		self._IniFile = None
		self._IniDataCount = 0
		
		self._SaveIniFile = None
		
		self._ModelList = []
		self._ModelNameList = []
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : Start CharacterControl Update')
		pass
		
	#使用するクラスの追加
	def AddClass(self,selectModelClass,pathControlClass,modelControlClass,undoClass,redoClass):
		self._SelectModelClass = selectModelClass
		self._PathControlClass = pathControlClass
		self._ModelControlClass = modelControlClass
		self._UndoClass = undoClass
		self._RedoClass = redoClass
		
	#INIファイルを基にアイテムを読み込む
	def Load(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : Load')
		
		filePath = viz.res.getPublishedPath(fileName)
		
		#読み込み用iniファイルの作成
		iniFile = LoadIniFile.LoadIniFileClass()
		iniFile.LoadIniFile(filePath)
		
		total = iniFile.GetItemData('Log','Total')
		
		if total == None:
			vizinput.message('ファイル読み込みエラー!\n指定のINIファイルはパス定義ファイルではありません。', title='RiMMInspector')
			del iniFile
		
		else:
			#全キャラクターとパスを削除
			self.DeleteAll()
			self._PathControlClass.DeleteAllCharacter()
		
			self._IniFileName = fileName
			
			#logファイルへのパスを取得
			fileNameData = fileName.split('\\')
			fName = ''
			fState = False
			for x in range(len(fileNameData)):
				data = fileNameData[x]
				if x == len(fileNameData)-1:
					pass
				elif fState:
					fName = fName + data + '\\' 
				if data == 'Settings':
					fState = True
					
			#読み込み用iniファイルの作成
			self._IniFile = iniFile
			
			#情報をリストに設定
			total = self._IniFile.GetItemData('Log','Total')
			self._IniDataCount = int(total)
			
			for x in range(self._IniDataCount):
				name = self._IniFile.GetItemData('Log',str(x))
				avatar = self._IniFile.GetItemData(name,'avatar')
				log = self._IniFile.GetItemData(name,'log')
				
				#モデルを読み込み、配置
				modelNum = self.Add(avatar)
				self.SetOrientation(modelNum,180)
				
				#ログを読み込む
				logPath = log
				logData = log.split('\\')
				if len(logData) == 1:
					logPath = fName + log
					
				logDataCountTemp = self._IniFile.GetItemData(log,'total')
				if logDataCountTemp == None:
					self._PathControlClass.Load(modelNum,logPath,usePathFile=True)
				else:
					pData = []
					logDataCount = int(logDataCountTemp)
					for x in range(logDataCount):
						dataTemp = self._IniFile.GetItemData(log,str(x))
						dataTemp = dataTemp.split(',')
						pData.append([float(dataTemp[0]),float(dataTemp[1]),float(dataTemp[2]),float(dataTemp[3])])
					#print 'pData',pData
					self._PathControlClass.Load(modelNum,pData)
				
			#Undoの情報をクリア
			self._UndoClass.ClearList()
			self._RedoClass.ClearList()
			
	#INIファイルにキャラクターとパスを保存する
	def Save(self,fileName):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : Save')
		
		#パスのファイル名を作成
		fileNameData = fileName.split('\\')
		
		fName = ''
		fState = False
		for x in range(len(fileNameData)):
			data = fileNameData[x]
			if x == len(fileNameData)-1:
				fName = fName + data[:-4]
			elif fState:
				fName = fName + data + '\\' 
			if data == 'Settings':
				fState = True
		
		#名前を取得
		pathName = fileNameData[len(fileNameData)-1][:-4]
		
		#保存用iniファイルの作成
		self._SaveIniFile = LoadIniFile.LoadIniFileClass()
		self._SaveIniFile.ResetIniData()
		
		self._SaveIniFile.AddSection('Log')
		self._SaveIniFile.AddItem('Log','Total',str(len(self._ModelList)))
		
		for x in range(len(self._ModelList)):
			name = 'Path' + str(x)
			self._SaveIniFile.AddItem('Log',str(x),name)
			
			self._SaveIniFile.AddSection(name)
			
			avatar = self._ModelNameList[x]
			self._SaveIniFile.AddItem(name,'avatar',avatar)
			
			log = pathName + '_log_' + str(x+1) + '.log'; 
			angle = self.GetOrientation(x)
			
			pathDataName = name+'_PathData'
			#self._SaveIniFile.AddItem(name,'log',log)
			self._SaveIniFile.AddItem(name,'log',pathDataName)
			
			logPath = fName + '_log_' + str(x+1) + '.log'; 
			pData = self._PathControlClass.Save(x,logPath,angle)
			
			self._SaveIniFile.AddSection(pathDataName)
			self._SaveIniFile.AddItem(pathDataName,'total',str(len(pData)))
			for y in range(len(pData)):
				pData2 = pData[y]
				dataStr = str(pData2[0]) + ',' + str(pData2[1]) + ',' + str(pData2[2]) + ',' + str(pData2[3]); 
				
				self._SaveIniFile.AddItem(pathDataName,str(y),dataStr)
					
		self._SaveIniFile.WriteIniData(fileName)
		self._IniFileName = fileName
		
	#追加
	def Add(self,fileName):
		model = Character.Character(self._ModelControlClass)
		model.Add(fileName)	#モデルを追加
		
		self._ModelList.append(model)
		self._ModelNameList.append(fileName)
		
		num = len(self._ModelList) - 1
		
		model.SetId(num)
		
		self._PathControlClass.AddCharacter()
		
		return num
	
	#削除
	def Delete(self,num):
		deleteModel = self._ModelList[num]
		
		#モデルリストを更新
		newModelLiset = []
		newModelNameList = []
		
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			if model != deleteModel:
				fileName = self._ModelNameList[x]
				
				newModelLiset.append(model)
				newModelNameList.append(fileName)
						
		self._ModelList = newModelLiset
		self._ModelNameList = newModelNameList
		
		#モデルを削除
		if deleteModel != None:
			deleteModel.Delete()
			del deleteModel
		
		#IDを再設定
		for x in range(len(self._ModelList)):
			model = self._ModelList[x]
			model.SetId(num)
			
		self._PathControlClass.DeleteCharacter(num)
		
	#全モデル削除
	def DeleteAll(self):
		self._SelectModelClass.UnSelectCharacter()
		
		modelCount = self.GetCharacterCount()
		for x in range(modelCount):
			self.Delete(0)
			
		self._PathControlClass.DeleteAllCharacter()
		
	#位置設定
	def SetPosition(self,num,position,withPath=True):
		model = self._ModelList[num]
		
		if model != None and position != None:
			model.SetPosition(position)
			
			if withPath:
				self._PathControlClass.SetPosition(num,0,position)
				
	#位置指定時のオフセット設定
	def SetPositionOffset(self,num,position):
		model = self._ModelList[num]
		
		if model != None and position != None:	
			model.SetPositionOffset(position)
			
	#位置取得
	def GetPosition(self,num):
		model = self._ModelList[num]
		position = [0,0,0]
		
		if model != None:
			position = model.GetPosition()
		
		return position
	
	#角度設定
	def SetOrientation(self,num,orientation):
		model = self._ModelList[num]
		
		if model != None and orientation != None:	
			model.SetOrientation(orientation)
			
	#角度のスナップ状態をリセット
	def ResetOrientationSnap(self,num):
		model = self._ModelList[num]
		
		if model != None:	
			model.ResetOrientationSnap()
		
	#角度取得
	def GetOrientation(self,num):
		model = self._ModelList[num]
		orientation = 0.0
		
		if model != None:
			orientation = model.GetOrientation()
		
		return orientation
		
	#角度設定(INIファイル用)
	def SetAxisAngle(self,num,axisAngle):
		model = self._ModelList[num]
		
		if model != None and axisAngle != None:	
			model.SetAxisAngle(axisAngle)
	
	#角度取得(INIファイル用)
	def GetAxisAngle(self,num):
		model = self._ModelList[num]
		axisAngle = [0,0,0,0]
		
		if model != None:
			axisAngle = model.GetAxisAngle()
		
		return axisAngle
		
	#ハイライト表示
	def SetHighlight(self,num,state):
		model = self._ModelList[num]
		model.SetHighlight(state)
		
	#ハイライト状態の取得
	def GetHighlightState(self,num):
		model = self._ModelList[num]
		state = model.GetHighlightState()
		
		return state
		
	#モデルから番号を取得
	def GetModelNum(self,model):
		num = None
		
		for x in range(len(self._ModelList)):
			if self._ModelList[x]._Model == model:
				num =  x
				
		return num
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		for model in self._ModelList:
			model.SetPickable(state)
			
	#モデル取得
	def GetModel(self,num):
		model = self._ModelList[num]
		
		return model
		
	#キャラクター数を取得
	def GetCharacterCount(self):
		count = len(self._ModelList)
		return count
		
	#モデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		CharacterList = self._ModelList
		for Character in CharacterList:
			CharacterModel = Character.GetModel()
			modelList.append(CharacterModel)
		
		return modelList
		
	#パスの削除
	def DeletePath(self,num):
		self._PathControlClass.DeleteCharacterPath(num)
		
	#パスのデータを取得
	def GetPathData(self,characterNum):
		return self._PathControlClass.GetPathData(characterNum)
		
	#パスの位置リストを取得
	def GetPathPositionList(self,characterNum):
		return self._PathControlClass.GetPathPositionList(characterNum)
		
	#パスの位置リストを設定
	def SetPathPositionList(self,characterNum,posList):
		self._PathControlClass.SetPathPositionList(characterNum,posList)
		
	#アニメーションを設定
	def SetAnimation(self,num,state):
		model = self._ModelList[num]
		model.SetAnimation(state)
		
	#アニメーションを取得
	def GetAnimation(self,num):
		model = self._ModelList[num]
		state = model.GetAnimation()
		return state
		
	#ファイル名を取得
	def GetFileName(self,num):
		model = self._ModelList[num]
		fileName = model.GetFileName()
		return fileName
		
	#IDを入れ替え
	def ChangeId(self,num):
		#print 'changeID',num
		newCharaNum = len(self._ModelList)-1
		
		#リストを更新
		newModelList = []
		newModelNameList = []
		
		for x in range(len(self._ModelList)):
			chara		= self._ModelList[x]
			charaName	= self._ModelNameList[x]
			
			if x == num:
				newChara		= self._ModelList[newCharaNum]
				newCharaName	= self._ModelNameList[newCharaNum]
				
				newModelList.append(newChara)
				newModelNameList.append(newCharaName)
				
			if x != newCharaNum:
				newModelList.append(chara)
				newModelNameList.append(charaName)
				
		self._ModelList			= newModelList
		self._ModelNameList		= newModelNameList
		
		self.UpdateId()
		
		self._PathControlClass.ChangeId(num)
		
		#デバッグ用
		#for x in range(self._ItemIniDataCount):
		#	data = self._ItemIniDataList[x]
		#	print 'data(changeID)=',x,data[0],data[10]
		
	#IDを更新
	def UpdateId(self):
		#キャラクターのIDを更新
		for x in range(len(self._ModelList)):
			chara = self._ModelList[x]
			chara.SetId(x)
			
	#キャラクターの総数を取得
	def GetAvatarCount(self):
		count = len(self._ModelList)
		return count
		
	#キャラクターのリストを取得
	def GetAvatarList(self):
		list = []
		for character in self._ModelList:
			list.append(character.GetModel())
			
		return list
		
	#パスのリストを取得
	def GetPathList(self):
		list = self._PathControlClass.GetPathList()
		return list
		
	#キャラクターの表示を設定
	def SetAvatarVisible(self,id,state):
		chara = self._ModelList[id]
		if state:
			chara.SetVisible(viz.ON)
		else:
			chara.SetVisible(viz.OFF)
			
	#パスの表示を設定
	def SetPathVisible(self,id,state):
		self._PathControlClass.SetPathVisible(id,state)
		
	#全キャラクターとパスの表示状態設定
	def SetVisibleCharacterAndPath(self,state):
		for x in range(len(self._ModelList)):
			self.SetAvatarVisible(x,state)
			self.SetPathVisible(x,state)
			
	#INIファイルにアイテムを保存する
	def GetSaveData(self):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][CharacterControl] : GetSaveData')
		
		#キャラクターの個数
		count = self.GetAvatarCount()
		
		#キャラクターモデルのリスト
		characterList = self.GetAvatarList()
		
		#パスデータのリスト
		pathDataList = []
		for x in range(len(self._ModelList)):
			angle = self.GetOrientation(x)
			pathData = self._PathControlClass.GetSaveData(x,angle)
			pathDataList.append(pathData)
			
		data = [count,characterList,pathDataList]
			
		return data
		