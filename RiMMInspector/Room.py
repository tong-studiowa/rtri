﻿# RiMMInspector

# - - - - - - - - - - - - - - - - - - - - - - - - -
#モジュールのインポート
import viz
import vizmat

import Interface
import Floor
import Wall
import WallLine
import WallPoint
import Ceiling
import DoorControl
import WindowControl

from win32api import OutputDebugString

# - - - - - - - - - - - - - - - - - - - - - - - - -
#グローバル変数

#ファイルへのパス
SETTING_FOLDER_PATH = '..\\Settings\\'

# - - - - - - - - - - - - - - - - - - - - - - - - -
#部屋制御のクラス定義
class Room(Interface.Interface):
	
	#コンストラクタ
	def __init__(self,modelControlClass):
		OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Room] : Init Room')
		
		self._ModelControlClass = modelControlClass
		
		self._RoomIniFile = None
		
		#部屋の設定
		self._FileName = ''
		self._Name = ''
		self._Position = [0.0,0.0,0.0]
		self._Angle = 0.0
		self._WallHeight = 3.0
		
		#形状関係
		self._FloorPointList = []
		
		#床関係
		self._FloorTextureFile = ""
		self._FloorTextureSize = 1.0
		
		self._FloorClass = None
		
		#壁関係
		self._WallModelList = []
		self._WallTextureFile = ""
		self._WallTextureSize = 1.0
		self._WallVisibleList = []
		
		self._WallClass = None
		self._WallLineClass = None
		self._WallPointClass = None
		
		#天井関係
		self._CeilingTextureFile = ""
		self._CeilingTextureSize = 1.0
		
		self._CeilingClass = None
		
		#ドア関係
		self._DoorList = []
		
		self._DoorControlClass = None
		
		#ドア関係
		self._WindowList = []
		
		self._WindowControlClass = None
		
		#天井の表示切り替え用
		self._Is3d = False
		
		#新規追加時の設定
		self._DefaultFloorTexPath = ''
		self._DefaultWallTexPath = ''
		self._DefaultCeilingTexPath = ''
		self._DefaultWallHight = 3.0
		
		self._HighlightFloorState = False
		
		self._IsOpRoom = 0
		
	#毎フレーム呼び出されるための関数
	def Update(self):		
		#OutputDebugString('[STUDIOWA][INFO][RiMMInspector][Room] : Start Room Update')
		
		#天井の表示切り替え
		if self._Is3d:
			viewPos = viz.MainView.getPosition()
			if viewPos[1] < self._WallHeight:
				self.SetCeilingVisible(True)
			else:
				self.SetCeilingVisible(False)
		else:
			self.SetCeilingVisible(False)
		
	#作成
	def Create(self,iniFile,fileName,roomName,roomData=None,add=False,copy=False):
		self._RoomIniFile = iniFile
		self._FileName = fileName
		self._Name = roomName
		
		self.LoadIniFileValue()
		
		if add:
			if self._DefaultFloorTexPath != None and self._DefaultFloorTexPath != '':
				self._FloorTextureFile = self._DefaultFloorTexPath
			
			if self._IsOpRoom != 2:
				if self._DefaultWallTexPath != None and self._DefaultWallTexPath != '':
					self._WallTextureFile = self._DefaultWallTexPath
				if self._DefaultCeilingTexPath != None and self._DefaultCeilingTexPath != '':
					self._CeilingTextureFile = self._DefaultCeilingTexPath
				if self._DefaultWallHight != None:
					self._WallHeight = self._DefaultWallHight
			
		if copy:
			self._Position = roomData[2]
			self._FloorPointList = roomData[3]
			self._WallVisibleList = roomData[4]
			self._WallHeight = roomData[5]
			self._FloorTextureFile = roomData[6]
			self._WallTextureFile = roomData[7]
			self._CeilingTextureFile = roomData[8]
			self._IsOpRoom = roomData[11]
			
		if copy:
			self._DoorList = []
			self._WindowList = []
			
		self._FloorClass = Floor.Floor(self._Position,self._FloorPointList,self._FloorTextureFile,self._FloorTextureSize)
		self._WallClass = Wall.Wall(self._Position,self._FloorPointList,self._DoorList,self._WindowList,self._WallTextureFile,self._WallTextureSize,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		self._WallLineClass = WallLine.WallLine(self._Position,self._FloorPointList,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		self._WallPointClass = WallPoint.WallPoint(self._Position,self._FloorPointList,self._WallHeight,self._IsOpRoom)
		self._CeilingClass = Ceiling.Ceiling(self._Position,self._FloorPointList,self._CeilingTextureFile,self._CeilingTextureSize,self._WallHeight)
		self._DoorControlClass = DoorControl.DoorControl(self._Position,self._FloorPointList,self._DoorList,self._WallHeight)
		self._WindowControlClass = WindowControl.WindowControl(self._Position,self._FloorPointList,self._WindowList,self._WallHeight)
		
		self._DoorControlClass.AddClass(self._WallClass,self._ModelControlClass)
		self._DoorControlClass.SetDoor()
		
		self._WindowControlClass.AddClass(self._WallClass,self._ModelControlClass)
		self._WindowControlClass.SetWindow()
		
		self._WallClass.AddClass(self._DoorControlClass,self._WindowControlClass)
		self._WallClass.Create()
		
		if copy:
			doorList = roomData[9]
			for doorData in doorList:
				num = self.AddDoor(doorData[0],doorData[1],doorData[2])
				
			windowList = roomData[10]
			for windowData in windowList:
				num = self.AddWindow(windowData[0],windowData[2],windowData[3])
				self.SetWindowHeight(num,windowData[1])
				
		if self._Is3d == False:
			self.HideWall(True)
			
	#設定ファイルの読み込み
	def LoadIniFileValue(self):
		#全体のパラメータ設定
		positionTemp = self._RoomIniFile.GetItemData(self._Name,'Position')
		self._Position[0] = float(positionTemp.split(',')[0])
		self._Position[1] = float(positionTemp.split(',')[1])
		self._Position[2] = float(positionTemp.split(',')[2])
		
		self._Angle = float(self._RoomIniFile.GetItemData(self._Name,'Angle'))
		
		self._FloorPointList = []
		self._WallVisibleList = []
		for x in range(100):
			pointName = "Point"+str(x)
			pointTemp = self._RoomIniFile.GetItemData(self._Name,pointName)
			if pointTemp != None:
				pointX = float(pointTemp.split(',')[0])
				pointY = float(pointTemp.split(',')[1])
				point = [pointX,pointY]
				self._FloorPointList.append(point)
				
				visibleState = True
				if len(pointTemp.split(',')) == 3:
					visibleStateTemp = int(pointTemp.split(',')[2])
					if visibleStateTemp == 0:
						visibleState = False
					elif visibleStateTemp == 1:
						visibleState = True
						
				self._WallVisibleList.append(visibleState)
				
			else:
				break
				
		self._WallHeight = float(self._RoomIniFile.GetItemData(self._Name,'WallHeight'))
		
		#床関係のパラメータ設定
		floorTextureTemp = self._RoomIniFile.GetItemData(self._Name,'FloorTexture')
		self._FloorTextureFile = floorTextureTemp.split(',')[0]
		self._FloorTextureSize = float(floorTextureTemp.split(',')[1])
	
		#壁関係のパラメータ設定
		wallTextureTemp = self._RoomIniFile.GetItemData(self._Name,'WallTexture')
		self._WallTextureFile = wallTextureTemp.split(',')[0]
		self._WallTextureSize = float(wallTextureTemp.split(',')[1])
		
		#天井関係のパラメータ設定
		ceilingTextureTemp = self._RoomIniFile.GetItemData(self._Name,'CeilingTexture')
		self._CeilingTextureFile = ceilingTextureTemp.split(',')[0]
		self._CeilingTextureSize = float(ceilingTextureTemp.split(',')[1])
		
		#ドア関係のパラメータ設定
		self._DoorList = []
		for x in range(100):
			doorName = "Door"+str(x)
			doorTemp = self._RoomIniFile.GetItemData(self._Name,doorName)
			if doorTemp != None:
				doorModelName	= doorTemp.split(',')[0]
				doorWallNum		= int(doorTemp.split(',')[1])
				doorPercentage	= float(doorTemp.split(',')[2])
				door = [doorModelName,doorWallNum,doorPercentage]
				self._DoorList.append(door)
			else:
				break
		
		#窓関係のパラメータ設定
		self._WindowList = []
		for x in range(100):
			windowName = "Window"+str(x)
			windowTemp = self._RoomIniFile.GetItemData(self._Name,windowName)
			if windowTemp != None:
				windowModelName		= windowTemp.split(',')[0]
				windowHeight		= float(windowTemp.split(',')[1])
				windowWallNum		= int(windowTemp.split(',')[2])
				windowPercentage	= float(windowTemp.split(',')[3])
				window = [windowModelName,windowHeight,windowWallNum,windowPercentage]
				self._WindowList.append(window)
			else:
				break
		
		#オペ室かどうのパラメータ設定
		opRoomStateTemp = self._RoomIniFile.GetItemData(self._Name,'OpRoom')
		self._IsOpRoom = 0
		if opRoomStateTemp != None and opRoomStateTemp != '':
			self._IsOpRoom = int(opRoomStateTemp)
			
	#Undoで再作成
	def ReCreate(self,iniFile,roomName,data):
		self._RoomIniFile = iniFile
		self._Name = roomName
		self._FileName = data[1]
		
		self.GetParamFromUndoData(data)

		self._FloorClass = Floor.Floor(self._Position,self._FloorPointList,self._FloorTextureFile,self._FloorTextureSize)
		self._WallClass = Wall.Wall(self._Position,self._FloorPointList,self._DoorList,self._WindowList,self._WallTextureFile,self._WallTextureSize,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		self._WallLineClass = WallLine.WallLine(self._Position,self._FloorPointList,self._WallHeight,self._WallVisibleList,self._IsOpRoom)
		self._WallPointClass = WallPoint.WallPoint(self._Position,self._FloorPointList,self._WallHeight,self._IsOpRoom)
		self._CeilingClass = Ceiling.Ceiling(self._Position,self._FloorPointList,self._CeilingTextureFile,self._CeilingTextureSize,self._WallHeight)
		self._DoorControlClass = DoorControl.DoorControl(self._Position,self._FloorPointList,self._DoorList,self._WallHeight)
		self._WindowControlClass = WindowControl.WindowControl(self._Position,self._FloorPointList,self._WindowList,self._WallHeight)
		
		self._DoorControlClass.AddClass(self._WallClass,self._ModelControlClass)
		self._DoorControlClass.SetDoor()
		
		self._WindowControlClass.AddClass(self._WallClass,self._ModelControlClass)
		self._WindowControlClass.SetWindow()
		
		self._WallClass.AddClass(self._DoorControlClass,self._WindowControlClass)
		self._WallClass.Create()
		
	#Undo時のから間取りの情報を設定
	def GetParamFromUndoData(self,data):
		
		#全体のパラメータ設定
		self._Position = data[2]
		self._Angle = 0.0
		
		self._FloorPointList = data[3]
		
		self._WallHeight = data[4]
		
		#床関係のパラメータ設定
		floorTextureData = data[5][0]
		self._FloorTextureFile = floorTextureData[0]
		self._FloorTextureSize = floorTextureData[1]
	
		#壁関係のパラメータ設定
		wallTextureData = data[5][1]
		self._WallTextureFile = wallTextureData[0]
		self._WallTextureSize = wallTextureData[1]
		
		#天井関係のパラメータ設定
		ceilingTextureData = data[5][2]
		self._CeilingTextureFile = ceilingTextureData[0]
		self._CeilingTextureSize = ceilingTextureData[1]
		
		#ドア関係のパラメータ設定
		self._DoorList = data[6]
		
		#窓関係のパラメータ設定
		self._WindowList = data[7]
		
		#壁の透過のパラメータ設定
		self._WallVisibleList = data[8]
		
		#オペ室のパラメータ設定
		self._IsOpRoom = data[9]
			
	#削除
	def Delete(self):
		self._FloorClass.Delete()
		self._WallClass.Delete()
		self._WallLineClass.Delete()
		self._WallPointClass.Delete()
		self._CeilingClass.Delete()
		self._DoorControlClass.DeleteAll()
		self._WindowControlClass.DeleteAll()
		
		del self._RoomIniFile
		self._RoomIniFile = None
		
	#壁のラインの表示状態を設定
	def SetWallLineVisible(self,state):
		self._WallLineClass.SetVisible(state)
	
	#壁のラインのハイライト表示
	def SetHighlightWallLine(self,num,state):
		if self._HighlightFloorState == False:
			self._WallLineClass.SetHighlight(num,state)
		
	#壁のラインモデルから番号を取得
	def GetWallLineNum(self,model):
		num = self._WallLineClass.GetModelNum(model)	
		
		return num
		
	#壁のラインモデルを取得
	def GetWallLineModel(self,num):
		model = self._WallLineClass.GetModel(num)
		
		return model
		
	#壁の点の表示状態を設定
	def SetWallPointVisible(self,state):
		self._WallPointClass.SetVisible(state)
	
	#壁の点のハイライト表示
	def SetHighlightWallPoint(self,num,state):
		if self._HighlightFloorState == False:
			self._WallPointClass.SetHighlight(num,state)
		
	#壁の点モデルから番号を取得
	def GetWallPointNum(self,model):
		num = self._WallPointClass.GetModelNum(model)	
		
		return num
		
	#壁の点モデルを取得
	def GetWallPointModel(self,num):
		model = self._WallPointClass.GetModel(num)
		
		return model
		
	#全モデルの選択可能状態を設定
	def SetPickableForAllModel(self,state):
		self._FloorClass.SetPickable(state)
		#self._WallClass.SetPickable(state)
		self._WallLineClass.SetPickable(state)
		self._WallPointClass.SetPickable(state)
		self._DoorControlClass.SetPickableForAllModel(state)
		self._WindowControlClass.SetPickableForAllModel(state)
	
	#壁のラインの位置設定
	def SetWallLinePosition(self,num,position):
		self._WallLineClass.SetPosition(num,position)
		self._FloorPointList = self._WallLineClass.GetFloorPointList()
		
		if self._IsOpRoom == 1:
			self.SetOpRoomShape(num,False)
			
		self.SetCenterPosition()
		
	#壁のラインの位置指定時のオフセット設定
	def SetWallLinePositionOffset(self,num,position):
		self._WallLineClass.SetPositionOffset(num,position)
		
	#壁の点の位置設定
	def SetWallPointPosition(self,num,position):
		state = True
		for x in range(len(self._FloorPointList)):
			point = self._FloorPointList[x]
			if x != num and position[0] == point[0] and position[2] == point[1]:
				state = False
		
		if state:
			self._WallPointClass.SetPosition(num,position)
			self._FloorPointList = self._WallPointClass.GetFloorPointList()
			
			if self._IsOpRoom == 1:
				self.SetOpRoomShape(num,True)
			
			self.SetCenterPosition()
			
	#壁の点の位置指定時のオフセット設定
	def SetWallPointPositionOffset(self,num,position):
		self._WallPointClass.SetPositionOffset(num,position)
		
	#ドア追加
	def AddDoor(self,fileName,wallNum,percentage):
		num,doorList = self._DoorControlClass.Add(fileName,wallNum,percentage)
		self._DoorList = doorList
		
		self._WallClass.SetDoor(self._DoorList)
		
		return num
	
	#ダミードア追加（穴を空ける）
	def AddDummyDoor(self,fileName,wallNum,percentage):
		num,doorList = self._DoorControlClass.AddDummy(fileName,wallNum,percentage)
		self._DoorList = doorList
		
		self._WallClass.SetDoor(self._DoorList)
		
		return num
	
	#ドア、窓の移動による、壁の再作成
	def ReCreateWall(self):
		self._WallClass.SetDoor(self._DoorList)
		self._WallClass.SetWindow(self._WindowList)
	
	#ドアの削除
	def DeleteDoor(self,num):
		if num != None:
			doorList = self._DoorControlClass.Delete(num)
			self._DoorList = doorList
			
			self._WallClass.SetDoor(self._DoorList)
		
	#ドアのコピー
	def CopyDoor(self,num):
		if num != None:
			#壁のサイズを取得
			doorData = self._DoorList[num]
			wallSize = self.GetWallSize(doorData[1])
			
			#ドアをコピー
			doorList = self._DoorControlClass.Copy(num,wallSize)
			self._DoorList = doorList
			
			self._WallClass.SetDoor(self._DoorList)
			
	#ドアの位置設定
	def SetDoorPosition(self,num,wallNum,percentage):
		if num != None and wallNum != None:	
			self._DoorControlClass.SetPosition(num,wallNum,percentage)
			
	#壁の両端の位置を取得
	def GetWallPoint(self,num):
		point0,point1 = self._WallClass.GetPoint(num)
		
		return point0,point1
		
	#ドアの表示状態を設定
	def SetDoorVisible(self,state):
		self._DoorControlClass.SetVisible(state)
		
	#ドアのラインの表示状態を設定
	def SetDoorLineVisible(self,state):
		self._DoorControlClass.SetLineVisible(state)
		
	#壁の高さ取得
	def GetWallHeight(self):
		wallHeight = self._WallHeight
		
		return wallHeight
	
	#壁の高さを設定
	def SetWallHeight(self,wallHeight):
		self._WallHeight = wallHeight
	
		self._WallClass.SetWallHeight(self._WallHeight)
		self._WallLineClass.SetWallHeight(self._WallHeight)
		self._WallPointClass.SetWallHeight(self._WallHeight)
		self._CeilingClass.SetWallHeight(self._WallHeight)
		self._DoorControlClass.SetWallHeight(self._WallHeight)
		self._WindowControlClass.SetWallHeight(self._WallHeight)
		
		self._WallClass.Move(self._FloorPointList)
		self._WallLineClass.Move(self._FloorPointList)
		self._WallPointClass.Move(self._FloorPointList)
		self._DoorControlClass.Move(self._FloorPointList)
		self._WindowControlClass.Move(self._FloorPointList)
		
	#ドアのハイライト表示
	def SetHighlightDoor(self,num,state):
		self._DoorControlClass.SetHighlight(num,state)
		
	#ドアのハイライト状態の取得
	def GetHighlightDoorState(self,modelNum):
		state = self._DoorControlClass.GetHighlight(modelNum)
		return state
		
	#ドアモデルから番号を取得
	def GetDoorNum(self,model):
		num = self._DoorControlClass.GetModelNum(model)	
		
		return num
		
	#ドアモデルを取得
	def GetDoorModel(self,num):
		model = self._DoorControlClass.GetModel(num)
		
		return model
		
	#ドアのラインモデルを取得
	def GetDoorLineModel(self,num):
		lineModel = self._DoorControlClass.GetLineModel(num)
		
		return lineModel
		
	#ドア選択可能状態を設定
	def SetDoorPickable(self,state):
		self._DoorControlClass.SetPickableForAllModel(state)
			
	#ドアのリストを更新
	def UpdateDoorList(self,num,wallNum,percentage):
		self._DoorList = self._DoorControlClass.UpdateList(num,wallNum,percentage)
	
	#窓追加
	def AddWindow(self,fileName,wallNum,percentage):
		num,windowList = self._WindowControlClass.Add(fileName,wallNum,percentage)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
		return num
	
	#ダミー窓追加（穴を空ける）
	def AddDummyWindow(self,fileName,wallNum,percentage):
		num,windowList = self._WindowControlClass.AddDummy(fileName,wallNum,percentage)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
		return num
	
	#窓の削除
	def DeleteWindow(self,num):
		if num != None:
			windowList = self._WindowControlClass.Delete(num)
			self._WindowList = windowList
			
			self._WallClass.SetWindow(self._WindowList)
		
	#窓の追加
	def CopyWindow(self,num):
		if num != None:
			#壁のサイズを取得
			windowData = self._WindowList[num]
			wallSize = self.GetWallSize(windowData[2])
			
			windowList = self._WindowControlClass.Copy(num,wallSize)
			self._WindowList = windowList
			
			self._WallClass.SetWindow(self._WindowList)
			
	#窓の位置設定
	def SetWindowPosition(self,num,wallNum,percentage):
		if num != None and wallNum != None:	
			self._WindowControlClass.SetPosition(num,wallNum,percentage)
		
	#窓の表示状態を設定
	def SetWindowVisible(self,state):
		self._WindowControlClass.SetVisible(state)
		
	#窓のラインの表示状態を設定
	def SetWindowLineVisible(self,state):
		self._WindowControlClass.SetLineVisible(state)
		
	#窓のハイライト表示
	def SetHighlightWindow(self,num,state):
		self._WindowControlClass.SetHighlight(num,state)
		
	#窓のハイライト状態の取得
	def GetHighlightWindowState(self,num):
		state = self._WindowControlClass.GetHighlight(num)
		return state
		
	#窓モデルから番号を取得
	def GetWindowNum(self,model):
		num = self._WindowControlClass.GetModelNum(model)	
		
		return num
		
	#窓モデルを取得
	def GetWindowModel(self,num):
		model = self._WindowControlClass.GetModel(num)
		
		return model
		
	#窓のラインモデルを取得
	def GetWindowLineModel(self,num):
		lineModel = self._WindowControlClass.GetLineModel(num)
		
		return lineModel
		
	#窓選択可能状態を設定
	def SetWindowPickable(self,state):
		self._WindowControlClass.SetPickableForAllModel(state)
			
	#窓のリストを更新
	def UpdateWindowList(self,num,state,height,wallNum,percentage):
		self._WindowList = self._WindowControlClass.UpdateList(num,state,height,wallNum,percentage)
	
	#天井の表示状態を設定
	def SetCeilingVisible(self,state):
		self._CeilingClass.SetVisible(state)
	
	#床のハイライト表示
	def SetHighlightFloor(self,state):
		self._HighlightFloorState = state
		
		for x in range(len(self._FloorPointList)):
			self._WallLineClass.SetHighlight(x,state)
			self._WallPointClass.SetHighlight(x,state)
			
	#床のハイライト状態の取得
	def GetHighlightFloorState(self):
		state = self._HighlightFloorState
		return state
		
	#床モデルを取得
	def GetFloorModel(self):
		model = self._FloorClass.GetModel()
		
		return model
		
	#床の位置設定
	def SetFloorPosition(self,position,withWall=True):
		self._Position = position
		
		self._FloorClass.SetFloorPosition(self._Position)
		if withWall:
			self._WallClass.SetFloorPosition(self._Position)
		self._WallLineClass.SetFloorPosition(self._Position)
		self._WallPointClass.SetFloorPosition(self._Position)
		self._CeilingClass.SetFloorPosition(self._Position)
		self._DoorControlClass.SetFloorPosition(self._Position)
		self._WindowControlClass.SetFloorPosition(self._Position)
		
	#床の位置取得
	def GetFloorPosition(self):
		position = self._Position
		return position
		
	#床のテクスチャを設定
	def SetFloorTexture(self,texture,filePath):
		self._FloorClass.SetTexture(texture)
		self._FloorTextureFile = filePath
		
	#床のテクスチャを取得
	def GetFloorTexture(self):
		texture = self._FloorClass.GetTexture()
		return texture
		
	#床のテクスチャ名を取得
	def GetFloorTextureName(self):
		textureName = self._FloorTextureFile
		return textureName
		
	#床のテクスチャのファイル名とサイズを取得
	def GetFloorTextureSetting(self):
		textureFile = self._FloorTextureFile
		textureSize = self._FloorTextureSize
		
		return textureFile,textureSize
		
	#壁のテクスチャを設定
	def SetWallTexture(self,texture,filePath):
		self._WallClass.SetTexture(texture)
		self._WallTextureFile = filePath
		
	#壁のテクスチャを取得
	def GetWallTexture(self):
		texture = self._WallClass.GetTexture()
		return texture
		
	#壁のテクスチャ名を取得
	def GetWallTextureName(self):
		textureName = self._WallTextureFile
		return textureName
		
	#壁のテクスチャのファイル名とサイズを取得
	def GetWallTextureSetting(self):
		textureFile = self._WallTextureFile
		textureSize = self._WallTextureSize
		
		return textureFile,textureSize
		
	#天井のテクスチャを設定
	def SetCeilingTexture(self,texture,filePath):
		self._CeilingClass.SetTexture(texture)
		self._CeilingTextureFile = filePath
		
	#天井のテクスチャを取得
	def GetCeilingTexture(self):
		texture = self._CeilingClass.GetTexture()
		return texture
		
	#天井のテクスチャ名を取得
	def GetCeilingTextureName(self):
		textureName = self._CeilingTextureFile
		return textureName
		
	#天井のテクスチャのファイル名とサイズを取得
	def GetCeilingTextureSetting(self):
		textureFile = self._CeilingTextureFile
		textureSize = self._CeilingTextureSize
		
		return textureFile,textureSize
		
	#窓の高さを設定
	def SetWindowHeight(self,num,height):
		windowList = self._WindowControlClass.SetWindowHeight(num,height)
		self._WindowList = windowList
		
		self._WallClass.SetWindow(self._WindowList)
		
	#窓の高さを取得
	def GetWindowHeight(self,num):
		height = self._WindowControlClass.GetWindowHeight(num)
		
		return height
		
	#壁のポイントを追加
	def AddWallPoint(self,num,pointData=None,wallVisibleList=None,floorPos=None):
		if self._IsOpRoom == 0:
			floorPointList,doorList,windowList = self._WallClass.AddPoint(num,pointData,wallVisibleList)
			wallVisibleList = self._WallClass.GetVisibleList()
			
			self._FloorPointList = floorPointList
			self._DoorList = doorList
			self._WindowList = windowList
			self._WallVisibleList = wallVisibleList
			
			self._FloorClass.SetPointList(self._FloorPointList)
			self._WallLineClass.SetPointList(self._FloorPointList)
			self._WallPointClass.SetPointList(self._FloorPointList)
			self._CeilingClass.SetPointList(self._FloorPointList)
			self._DoorControlClass.SetPointList(self._FloorPointList,self._DoorList)
			self._WindowControlClass.SetPointList(self._FloorPointList,self._WindowList)
			
			self._WallLineClass.SetVisibleList(self._WallVisibleList)
			
			if floorPos != None:
				self.SetFloorPosition(floorPos)
			else:
				self.SetCenterPosition()
				
	#壁のポイントを削除
	def RemoveWallPoint(self,num):
		if self._IsOpRoom == 0:
			floorPointList,doorList,windowList = self._WallClass.RemovePoint(num)
			wallVisibleList = self._WallClass.GetVisibleList()
			
			self._FloorPointList = floorPointList
			self._DoorList = doorList
			self._WindowList = windowList
			self._WallVisibleList = wallVisibleList
			
			self._FloorClass.SetPointList(self._FloorPointList)
			self._WallLineClass.SetPointList(self._FloorPointList)
			self._WallPointClass.SetPointList(self._FloorPointList)
			self._CeilingClass.SetPointList(self._FloorPointList)
			self._DoorControlClass.SetPointList(self._FloorPointList,self._DoorList)
			self._WindowControlClass.SetPointList(self._FloorPointList,self._WindowList)
			
			self._WallLineClass.SetVisibleList(self._WallVisibleList)
			
			self.SetCenterPosition()
		
	#壁のポイントの数を取得
	def GetWallPointCount(self):
		pointCount = len(self._FloorPointList)
		return pointCount
	
	#壁のポイントのリストを設定
	def SetWallPointList(self,list):
		self._FloorPointList = list
	
	#壁のポイントのリストを取得
	def GetWallPointList(self):
		pointList = self._FloorPointList
		return pointList
	
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._DoorControlClass.GetDoorModelList()
		return modelList
		
	#ドアの数を取得
	def GetDoorCount(self):
		count = self._DoorControlClass.GetCount()
		return count
		
	#窓の数を取得
	def GetWindowCount(self):
		count = self._WindowControlClass.GetCount()
		return count
		
	#ドアの情報を取得
	def GetDoorState(self,num):
		state = self._DoorControlClass.GetState(num)
		return state
		
	#窓の情報を取得
	def GetWindowState(self,num):
		state = self._WindowControlClass.GetState(num)
		return state
		
	#ドアモデルのリストを取得
	def GetDoorModelList(self):
		modelList = self._DoorControlClass.GetDoorModelList()
		return modelList
		
	#床、壁、天井、ドア、窓のモデルのリストを取得
	def GetModelList(self):
		modelList = []
		
		#床
		floorModel = self._FloorClass.GetModel()
		modelList.append(floorModel)
		
		#壁
		wallModelList = self._WallClass.GetAllModelList()
		for model in wallModelList:
			modelList.append(model)
		
		#天井
		ceilingModel = self._CeilingClass.GetModel()
		modelList.append(ceilingModel)
		
		#ドア
		doorModelList = self._DoorControlClass.GetDoorModelList()
		for model in doorModelList:
			modelList.append(model)
		
		#窓
		windowModelList = self._WindowControlClass.GetWindowModelList()
		for model in windowModelList:
			modelList.append(model)
		
		return modelList
		
	#コリジョン判定用モデル（壁、ドア、窓）のモデルのリストを取得
	def GetCollisionModelList(self):
		modelList = []
		
		#壁
		wallModelList = self._WallClass.GetAllModelList()
		for model in wallModelList:
			modelList.append(model)
		
		#ドア
		doorModelList = self._DoorControlClass.GetDoorModelList()
		for model in doorModelList:
			modelList.append(model)
		
		#窓
		windowModelList = self._WindowControlClass.GetWindowModelList()
		for model in windowModelList:
			modelList.append(model)
		
		return modelList
		
	#部屋の中心位置を求める
	def CheckCenterPosition(self):
		position = [0,0,0]
		
		roomPosition = self.GetFloorPosition()
		wallPointCount = self.GetWallPointCount()
		pointList = self.GetWallPointList()
		
		for x in range(wallPointCount):
			pointPosition = pointList[x]
			position[0] = position[0] + pointPosition[0] + roomPosition[0]
			position[2] = position[2] + pointPosition[1] + roomPosition[2]
			
		position[0] = position[0] / wallPointCount
		position[2] = position[2] / wallPointCount
		
		return position
		
	#中心位置を再設定
	def SetCenterPosition(self):
		#部屋を再配置
		roomPos = self.GetFloorPosition()
		newRoomPos = self.CheckCenterPosition()
		self.SetFloorPosition(newRoomPos)
		
		#頂点を再配置
		disp = [0,0]
		disp[0] = newRoomPos[0] - roomPos[0]
		disp[1] = newRoomPos[2] - roomPos[2]
		
		for x in range(len(self._FloorPointList)):
			newPointPos = [0,0]
			pointPosition = self._FloorPointList[x]
			newPointPos[0] = pointPosition[0] - disp[0]
			newPointPos[1] = pointPosition[1] - disp[1]
			
			self._FloorPointList[x] = newPointPos
			
		self._FloorClass.Move(self._FloorPointList)
		self._WallClass.Move(self._FloorPointList)
		self._WallLineClass.Move(self._FloorPointList)
		self._WallPointClass.Move(self._FloorPointList)
		self._CeilingClass.Move(self._FloorPointList)
		self._DoorControlClass.Move(self._FloorPointList)
		self._WindowControlClass.Move(self._FloorPointList)
		
	#表示状態の設定
	def SetViewState(self,state):
		self._Is3d = state
		
	#壁の表示状態設定
	def SetWallVisible(self,wallNum,state):
		#print 'SetWallVisible'
		self._WallVisibleList[wallNum] = state
		
		self._WallClass.SetVisibleList(self._WallVisibleList)
		self._WallLineClass.SetVisibleList(self._WallVisibleList)
		
	#壁の表示状態取得
	def GetWallVisible(self,wallNum):
		state = self._WallVisibleList[wallNum]
		return state
		
	#壁の表示状態のリスト取得
	def GetWallVisibleList(self):
		list = self._WallVisibleList
		return list
		
	#壁を非表示に設定
	def HideWall(self,state):
		if state:	#2D視点時
			self._WallClass.Hide(True)
			self._WindowControlClass.SetVisible(False)
			self._DoorControlClass.SetVisible(False)
			
		else:		#3D視点時
			self._WallClass.Hide(False)
			self._WindowControlClass.SetVisible(True)
			self._DoorControlClass.SetVisible(True)
		
	#追加時のテクスチャ、高さ設定情報を設定
	def SetDefaultSettings(self,floorTex,WallTex,CeilingTex,WallHeight):
		self._DefaultFloorTexPath = floorTex
		self._DefaultWallTexPath = WallTex
		self._DefaultCeilingTexPath = CeilingTex
		self._DefaultWallHight = WallHeight
		
	#壁のサイズを取得
	def GetWallSize(self,num):
		wallSize = None
		if num != None:
			point0,point1 = self.GetWallPoint(num)
			wallSize = vizmat.Distance(point0,point1)
			
		return wallSize
		
	#ドアのリストを取得
	def GetDoorList(self):
		list = self._DoorList
		return list
		
	#窓のリストを取得
	def GetWindowList(self):
		list = self._WindowList
		return list
		
	#モデル名を取得
	def GetFileName(self):
		fileName = self._FileName
		roomName = self._Name
		return fileName,roomName
		
	#オペ室かどうかの設定
	def SetOpRoomState(self,state):
		self._IsOpRoom = state
		
	#オペ室かどうかの取得
	def GetOpRoomState(self):
		state = self._IsOpRoom
		return state
		
	#オペ室の形状を設定
	def SetOpRoomShape(self,num,isPoint):
		posMin = [self._FloorPointList[6][0],self._FloorPointList[0][1]]
		posMax = [self._FloorPointList[2][0],self._FloorPointList[4][1]]
		
		if isPoint:
			if num == 1:
				posMin[1] = self._FloorPointList[1][1]
			elif num == 3:
				posMax[0] = self._FloorPointList[3][0]
			elif num == 5:
				posMax[1] = self._FloorPointList[5][1]
			elif num == 7:
				posMin[0] = self._FloorPointList[7][0]
			
		else:
			if num == 1:
				posMin[1] = self._FloorPointList[1][1]
			elif num == 3:
				posMax[0] = self._FloorPointList[3][0]
			elif num == 5:
				posMax[1] = self._FloorPointList[5][1]
			elif num == 7:
				posMin[0] = self._FloorPointList[7][0]
		
		self._FloorPointList[0] = [posMin[0]+0.6,posMin[1]]
		self._FloorPointList[1] = [posMax[0]-0.6,posMin[1]]
		self._FloorPointList[2] = [posMax[0],posMin[1]+0.6]
		self._FloorPointList[3] = [posMax[0],posMax[1]-0.6]
		self._FloorPointList[4] = [posMax[0]-0.6,posMax[1]]
		self._FloorPointList[5] = [posMin[0]+0.6,posMax[1]]
		self._FloorPointList[6] = [posMin[0],posMax[1]-0.6]
		self._FloorPointList[7] = [posMin[0],posMin[1]+0.6]
		
	#壁モデルを作成
	def SetAllWallModel(self):
		self._WallClass.SetAllWallModel()
		
	#壁モデルを削除
	def DelAllWallModel(self):
		self._WallClass.Delete()
		
	#間取りのドアIDを入れ替え
	def ChangeDoorId(self,num):
		#print 'changeDoorID',num
		self._DoorList = self._DoorControlClass.ChangeId(num)
		self._WallClass.SetDoor(self._DoorList)
		
	#間取りの窓IDを入れ替え
	def ChangeWindowId(self,num):
		#print 'ChangeWindowId',num
		self._WindowList = self._WindowControlClass.ChangeId(num)
		self._WallClass.SetWindow(self._WindowList)
		
	#間取り名を取得
	def GetName(self):
		return self._Name
		
	#読み込みに失敗したかどうかを取得
	def GetLoadErrorState(self):
		return self._LoadErrorState
		
	#壁の透過設定のリストを取得
	def GetWallVisibleList(self):
		return self._WallVisibleList
		
	#オペ室かどうかのパラメータを取得
	def GetIsOpRoom(self):
		return self._IsOpRoom